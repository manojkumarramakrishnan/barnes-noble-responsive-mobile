package commonFiles;

import java.io.File;
import java.util.ArrayList;
import org.testng.annotations.BeforeSuite;

import bnConfig.BNConstants;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.NetworkMode;

public class ExtentRptManager 
{
	public static ExtentReports extent;
	public static ExtentTest parent,child;
	public ArrayList<String> log = new ArrayList<>();
	
	@BeforeSuite
	public static ExtentReports completeReport()
	{
	 if(extent == null)
	 {
		 String rptPath = BNConstants.repFileLoc;
		 extent = new ExtentReports(rptPath, false, NetworkMode.ONLINE);
		 extent.loadConfig(new File(BNConstants.extentRepConfigfile));
	 }
	 return extent;
	}
	
	/*@AfterSuite
	public void closeReporter() 
	{
		extent.close();
	}
	
	@BeforeMethod
	public void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}*/
}
