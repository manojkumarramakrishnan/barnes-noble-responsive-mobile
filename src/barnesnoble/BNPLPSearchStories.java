package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import bnPageObjects.BNPLPSearchObj;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNPLPSearchStories 
{
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNPLPSearchObj searchplp;
	
	@BeforeClass
	public void beforeTest() throws Exception 
	{
		//Thread.sleep(25000);
		driver = BNBasicfeature.setMobileView();
	}
	
	  @Test(priority = 1)
	  public void BRM_1817_Search_Result_Page_Implementation() throws Exception 
	  {
		  HSSFSheet sheet = BNBasicfeature.excelsetUp("Search Implementation");
		  searchplp = new BNPLPSearchObj(driver,sheet);
		  
		  searchplp.searchPageNoResult();
		  searchplp.searchPageFilterShowMore();
		  searchplp.searchpageNarrowFilterSelection();
		  searchplp.searchFieldContainer();
		  searchplp.searchSuggestionResults();
		  searchplp.searchSuggestionSelection();
		  searchplp.searchPriceSearchResultView();
		  searchplp.searchPageFilterSort();
		  searchplp.searchPageSortVisible();
		  searchplp.searchPageSortOptions();
		  searchplp.searchPageCurrentPageUnderline();
		  searchplp.searchPaginationPageNavigation();
		  searchplp.searchPageFilteSortIconElements();
		  searchplp.searchResultHeaderValidation();
		  searchplp.searchMisspellA();
		  searchplp.searchPageFilterApplyRemove();
		  searchplp.searchMisspellSuggestion();
		  searchplp.searchPageMisspellSuggestionSelection();
		  searchplp.searchPageGridListDetails();
		  //searchplp.searchSuggestionSelectionPageView("BRM-1849");
		  searchplp.searchpdpPageNavigation(); //--> Enable
		  searchplp.searchPageViewSelection();
		  searchplp.searchPageSorting("BRM-1878");
		  searchplp.searchPageSorting("BRM-1879");
		  searchplp.searchPageSorting("BRM-1880");
		  searchplp.searchPageSorting("BRM-1881");
		  searchplp.searchPageSorting("BRM-1885");
		  searchplp.searchPageSorting("BRM-1886");
		  searchplp.searchPageSorting("BRM-1887");
		  //searchplp.searchPageSort(); //--> Enable 
		  searchplp.searchViewChange();
		  searchplp.searchSavePriceDisplayListView("BRM-892");
		  searchplp.searchNoReviewCount();
		  searchplp.searchFilterOption();
		  searchplp.searchFilterBackground();
		  searchplp.searchImagepdpPageNavigation();
		  searchplp.searchpageRemoveFilterExpandClose();
		  searchplp.searchFilterNoCollapse();
		  searchplp.searchPageMisspellSuggestion();
		  searchplp.searchResultGridListOptions();
		  searchplp.searchSavePriceDisplayListView("BRM-1866");
		  searchplp.searchFilterReviewCountClickPDPPageNavigation();
		  searchplp.searchPageFilterAccordians();
		  searchplp.searchPageHavePagination();
		  searchplp.searchPageBacktoTop();
		  searchplp.searchGridViewPersistent(); //--> Need to Include
		  searchplp.searchPageAuthorNameFilter();
	  }
	
	@Test(priority = 2)
	public void BRM_1833_Product_list_page_implementation() throws Exception 
	{
		HSSFSheet sheet = BNBasicfeature.excelsetUp("PLP Implementation");
		searchplp = new BNPLPSearchObj(driver,sheet);
		searchplp.pancakeCatSubcatToPLP();
		searchplp.respectivePlp();
		searchplp.plpFilterSort();
		searchplp.categoryTitle();
		searchplp.productsTotal();
		searchplp.sortBold();		
		searchplp.productPrice();
		searchplp.regular_sale_price();		  
		searchplp.productRatingReviews();
		searchplp.category_Count_Title();
		searchplp.productPerPage();
		searchplp.ProductDisplayed();
		searchplp.ItemsFound();
		searchplp.grid_List_View();
		searchplp.prodTitleImage();
		searchplp.plp_To_pdp();
		searchplp.SubCategory_To_PLP(); 
		searchplp.PLP_Creative();
		searchplp.product_to_pdp();
		searchplp.review_to_pdp();
		searchplp.sortOverlay();
		searchplp.sort_new_To_old();
		searchplp.sort_old_To_new();
		searchplp.sort_high_To_low();
		searchplp.sort_low_To_high();
		searchplp.sort_highlyRated();
		searchplp.sort_A_Z();
		searchplp.sort_Z_A();	
		searchplp.navigate_back();
		searchplp.filterCreative();
		searchplp.filterAccord();		
		searchplp.filterAccordExpand();
		searchplp.filterClear();
		searchplp.filterRefresh();
		searchplp.filterApply();
		searchplp.filterClose();		  
		searchplp.flterSortColor();
		searchplp.filterToPDP();	 
	}
	
	@AfterClass
	public void afterClass() 
	{
	  driver.close();
	}
	  
	@BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
		
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
		
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
		
	public static void EndTest()
	{
		extent.endTest(child);
	}
		
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
		
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
		
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
		
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
		
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
		
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
		
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
}
