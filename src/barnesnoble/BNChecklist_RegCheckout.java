package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import bnPageObjects.BNRegChkListObj;
import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNChecklist_RegCheckout 
{
	WebDriver driver;
	HSSFSheet sheet;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNRegChkListObj chk;
	
  @Test(priority=1)
  public void BN_Checklist_Checkout() throws Exception
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Checklist");
	  chk = new BNRegChkListObj(driver,sheet);
	  
	  //******************** New Shipping *************************
	  chk.checkoutExistingshippingDetails();
	  chk.checkoutOrderShipAddressLoyaltyContainer();
	  chk.checkoutExistingshippingEditButton();
	  chk.checkoutOrderShippingSavedAddressList();
	  chk.checkoutAddaShippingAddressPage();
	  chk.EditShipInfoUseExistingShippingAdd();
	  chk.EditShipToMAddAddressUseExistingAddressLinkNavigation();
	  chk.ShipToMAddAddressChooseFromAddressList();
	  chk.ShipToMAddAddressButtonsEnableFromAddressList();
	  chk.EditShipAddressChangeToAddressFromList();
	  chk.AddressDoctorAddShippingAddress();
	  
	  //******************** Edit Shipping *************************

	  chk.EditShipShippingEditDetails();
	  chk.EditShipShippingEditDetailsSaveAddressVerfication();
	  chk.EditShipShippingEditAddressSuggestionList();
	  chk.EditShipShippingEditAddressYouEntered();
	  chk.EditShippingAddressListEditButtonSelection();
	  chk.EditShippingAddressListModifyAddressReverfication();
	  chk.checkoutAddShippingAddAddress();
	  chk.ShipToMAddAddressEditAddressUsethisAddressNavigation(); 
	  
	  //******************** Billing Address *************************
	   
	  chk.checkoutAddBillingSavedAddressListContents();
	  chk.checkoutOrderAddBillingExistingShippingRecords();
	  chk.checkoutOrderAddBillingExistingShippingRecordsSelection();
	  chk.checkoutOrderAddBillingExistingShippingRecordsSelectionSave();
	  chk.EditShipEditBillingCCardCreateNew();
	  chk.checkoutOrderLoyaltyContainer();
	  
	  //******************** Delivery Option and Delivery Preference *************************
	  chk.regUserCheckoutDeliveryPreference();
	  chk.regUserCheckoutDeliveryPresent();
	  chk.shipFrmBarnesAndNoble();
	  chk.electronicDeliveryPref();
	  chk.deliveredElectronicProducsts();
	  chk.shipFrmMarketSellerPlace();
	  chk.deliveryContainerSize();
	  chk.regUserDeliveryPreferenceView();
	  chk.regUserDeliveryOptionSelect();
	  chk.regUserDeliveryPreferenceDetails();
	  chk.regUserDeliveryPreferenceSelection();
	  chk.regUserEditOption();
	  chk.regUserNookPrdtAvailableText();
	  chk.regUserGiftMessage();
	  chk.regUserMakeitasGiftContainerDefaultSelection();
	  chk.regUserMakeitasGiftContainerExpandDetails();
	  chk.regUserMakeitasGiftContainerDefaultText();
	  chk.regUserMakeitasGiftContainerDefaultTextLengthValidation();
	  chk.regUserMakeitasGiftContainerApplyButton();
	  chk.regUserMakeitasGiftContainerCheckboxEnableDisable();
	  chk.regUserMakeitasGiftContainerApplyGiftCard();
	  chk.regUserMakeitasGiftContainerGiftCardEdit(); 
	  
	  //*************** Gift Card and  Membership Coupon Book Fair Id *********************
	  chk.regUserCheckoutBNMembershipPgmDrpDown();
	  chk.regUserCheckoutCouponContainerContainerDetails();
	  chk.regUserCheckoutCouponDetails2();
	  chk.regUserCheckoutBNMembershipPgmDrpDownDefault();
	  chk.regUserCheckoutGiftCouponEmptyValidation();
	  chk.regUserCheckoutCouponGftCpnBkfairDetails();
	  chk.regUserCheckoutCouponInvalidValidation();
	  chk.regUserCheckoutBookFair();
	  chk.regUserCheckoutBookFairHighlight();
	  chk.regUserCheckoutBookFairEmptyValidation();
	  chk.regUserCheckoutBookFairInvalidValidation();
	  chk.regUserCheckoutBookFairInvalidValidation1(); 
	  
	  chk.signOut(); 
	  //*************** Empty Billing and Shipping Address *********************
	  
	  chk.checkoutNoDeliveryPreference();
	  chk.checkoutAddNewInfo();
	  chk.checkoutSubmitOrderAlert();
	  chk.checkoutShippingAddressAddBtn();
	  chk.checkoutAddShippingAddressElements();
	  chk.checkoutOrderAddBillingInfo();
	  chk.checkoutAddBillingAddNewPageElements();
	  chk.checkoutAddBillingAddNewPageValidation();
	  chk.checkoutOrderAddBillingCCBillingiiconExpand();
	  chk.checkoutOrderAddBillingCCBillingiiconCollapse();
	  chk.BillingCCardCancelBtnNavigation();
  }
	  
	  @BeforeClass
	  public void beforeClass() 
	  {
		  driver = BNBasicfeature.setMobileView();  
	  }

	  @AfterClass
	  public void afterClass() 
	  {
		  driver.close();
	  }

   @BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
}
