package barnesnoble;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import bnPageObjects.BNCompleteSprintStories;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNSprintStories {
  
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNCompleteSprintStories bnss;
	  
  @BeforeClass
  public void beforeClass() throws Exception 
  {
	  //Thread.sleep(10000);
	  driver = BNBasicfeature.setMobileView();
  }
  
  @Test(priority = 1)
  public void BRM_55_Header_Rotating_Global_Promo_Message() throws IOException, JSONException, InterruptedException 
  {
	bnss = new BNCompleteSprintStories(driver, sheet);
	bnss.promobannerpresent();
	bnss.promoredirect();
	bnss.promocomparison();
	bnss.promochangetime();
  }
  
  @Test(priority = 2)
  public void BRM_56_Pancake_Menu_Display() throws IOException, JSONException, InterruptedException 
  {
	HSSFSheet sheet = BNBasicfeature.excelsetUp("Header - Pancake Menu");
	bnss = new BNCompleteSprintStories(driver,sheet);
	bnss.elementpresent();
	bnss.menuopencollapse();
	bnss.menusubmenuclick();
	bnss.menuscroll();
	bnss.catenavigation();  
	bnss.menusubmenucolor();
	//bnss.subcatenavigation(); 
	bnss.menuComparision();
  }
  
  @Test(priority = 3)
  public void BRM_57_Search_Functionality() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Search Function");
	  bnss = new BNCompleteSprintStories(driver,sheet);
	  bnss.searchIcon();
	  bnss.searchCancel();
	  bnss.searchCount();
	  //bnss.searchhighlight();
	  //bnss.searchfieldcontentverification();
	  bnss.searchresultnavigation();  
	  bnss.resultnavigation();
	  bnss.htmlsearch();
	  //bnss.searchcloseicon();
	  bnss.inlinepopupclose();
	  bnss.nosearchresult();
	  bnss.searchCaseSensitive();
	  //bnss.searchvalidationmsg();
	  bnss.searchDataVal();
	  //bnss.searchClose();
	  //bnss.searchTxt();
	  bnss.searchBackspace();
	  //bnss.searchiconStatic();
	  bnss.searchResCount();
	  bnss.pancakeNavigation();
	  bnss.srchcomparision();
  }
  
  @Test (priority = 4)
  public void BRM_306_Sign_In() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Sign In");
	  bnss = new BNCompleteSprintStories(driver,sheet);
	  
	  bnss.signInnav();
	  bnss.pagescroll();
	  bnss.signInbtnchk();
	  bnss.emailpasswrdfldchk();
	  bnss.frgtpasswordlinkvisiblity();
	  bnss.SecureSignInalert();
	  bnss.emaillengthValidation();
	  bnss.forgetPasswordLinkVerify(); 
	  bnss.nonregisterdAlrt();
	  bnss.cancelbtnnav();       
	  bnss.invalidPasswordAlrt();
	  bnss.invalidEmailAlrt();
	  bnss.htmltagValidation(); 
	  bnss.termsOfUseverify(); 
	  bnss.policyPage(); 
	  bnss.rememberMeCheckboxstate();
	  bnss.passwordFieldTextChk();
	  bnss.createAccNav(); 
	  bnss.emaillengthchk();
	  bnss.pwdlengthchk(); 
	  //bnss.cancelbtnnav2();
	  bnss.invalidemailformat();
	  bnss.loginlimit();
	  //bnss.successfulLogin();
	  //bnss.httpscallchk();
  }
  
  //@Test(priority = 5)
  public void BRM_291_Forgot_Password() throws IOException 
  {
  	HSSFSheet sheet = BNBasicfeature.excelsetUp("Forgot Password");
  	bnss = new BNCompleteSprintStories(driver,sheet);
	
  	bnss.forgotPasswordnavigation();
  	bnss.forgorpasswordEmailandContinuefield();
  	bnss.forgorpasswordelementpresence();
  	bnss.forgotpasswordEmptyEmailAlrt();
	bnss.forgotpasswordEmailLength();
	bnss.forgotpasswordNonRegisterEmailAlrt();
	bnss.forgotpasswordHtmlEmail();
	bnss.forgotpasswordContinuebtn(); //
	bnss.forgotpasswordpageelements();
	bnss.forgotpasswordEmailVerify();
	bnss.forgotpasswordRecoveryOptionVerify();
	bnss.forgotpasswordRecoveryDefaultSelect();
	bnss.forgotpasswordRecoverySecurityQuestionCheck();
	bnss.forgotpasswordSecurityAnswerLimit();
	bnss.forgotpasswordInvalidSecurityAnswer();
	bnss.forgotpasswordInvalidSecurityLimitExceed();
	bnss.forgotpasswordResetByEmailLink();
	bnss.forgotpasswordContinueShoppingNav();	
	bnss.forgotpasswordSecurityQuestionVerification();
	bnss.forgotpasswordBlankPassAlrt();
	bnss.forgotpasswordResetRulesEnable();
	bnss.forgotpasswordResetRulesDisable();
	bnss.forgotpasswordResetSuccess();
	bnss.forgotpasswordRecoveryCustomerService();
  }
  
  @Test(priority = 6)
  public void BRM_59_Logo_Display() throws IOException 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Logo");
	  bnss = new BNCompleteSprintStories(driver,sheet);
	  bnss.bnlogo();
	  bnss.logonavigation();
  }
  
  @Test(priority = 7)
  public void BRM_308_Register_Process() throws Exception 
  {
	HSSFSheet sheet = BNBasicfeature.excelsetUp("Create Account");
	bnss = new BNCompleteSprintStories(driver,sheet);
	bnss.createAcntfieldchk();
	bnss.signInNavigation();
	bnss.fieldtxtchk();
	bnss.createAccVal();
	bnss.checkisvalenterable();
	bnss.namevalidation();
	bnss.namevalidationalert();
	bnss.validemailchk();
	bnss.invalidemailchk();
	bnss.validcnfmailchk();
	bnss.invaliddatacoloralert();
	bnss.diffemailchk();
	bnss.passwordfieldschk();
	bnss.passlengthchk();
	bnss.blankpasschk();
	bnss.noconfirmpasschk();
	bnss.passmismatch();
	bnss.samepasswordOkval();
	bnss.dropdownselection();
	bnss.securityAnsVal();
	bnss.cancelbtnnavigation();
	bnss.existingEmail();
	bnss.maxcharval();
	bnss.secquesdropdown();
	bnss.createNewAccount();
	bnss.createAccwelcomeMsg();
	bnss.signOut();
  }
  
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }
  
    @BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
  
}
