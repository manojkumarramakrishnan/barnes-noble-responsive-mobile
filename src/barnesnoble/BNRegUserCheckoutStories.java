package barnesnoble;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNRegUserCheckoutStories {
  
	WebDriver driver;
	HSSFSheet sheet;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	bnPageObjects.BNCompleteRegisteredCheckoutStories bnreg;
	  
  @BeforeClass
  public void beforeClass() 
  {
	  driver = BNBasicfeature.setMobileView();
  }
  
  @Test(priority = 1)
  public void BRM_60_Login_Link() throws IOException 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Login Link");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  bnreg.signInnvaigation();
	  bnreg.checkoutnonRegistered();
	  bnreg.checkoutInvaliloginalrt();
	  bnreg.loggedinVerifcation();
  }
  
  @Test(priority = 2)
  public void BRM_292_Address_Doctor() throws IOException 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Address Doctor");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  bnreg.AddressDoctorAddShippingAddress();
	  bnreg.AddressDoctorShippingAddressAddressYouEntered();
	  bnreg.AddressDoctorSuggestedShippingAddressListDisplay();
	  bnreg.AddressDoctorSuggestedShippingAddressListUseThisAddressButton();
	  bnreg.AddressDoctorSuggestedShippingAddressList();
	  bnreg.AddressDoctorSuggestedShippingAddressListAddressSelection();
	  bnreg.AddressDoctorSuggestedShippingAddressListEditButtonSelection();
	  bnreg.AddressDoctorSuggestedShippingAddressListModifyAddressReverfication();
	  bnreg.AddressDoctorAddressAddAddressFromList();
	  bnreg.AddressDoctorAddressSelectFromList();
	  
	  bnreg.AddressDoctorEditShippingAddress();
	  bnreg.AddressDoctorEditShippingAddressAddressYouEntered();
	  bnreg.AddressDoctorSuggestedEditShippingAddressListDisplay();
	  bnreg.AddressDoctorSuggestedEditShippingAddressListUseThisAddressButton();
	  bnreg.AddressDoctorSuggestedEditShippingAddressList();
	  bnreg.AddressDoctorSuggestedEditShippingAddressListAddressSelection();
	  bnreg.AddressDoctorSuggestedEditShippingAddressListEditButtonSelection();
	  bnreg.AddressDoctorSuggestedEditShippingAddressListModifyAddressReverfication();
	  bnreg.AddressDoctorEditAddressAddAddressFromList();
	  bnreg.AddressDoctorEditAddressSelectFromList();
  }
  
  @Test(priority = 3)
  public void BRM_294_Edit_Shipping_Address_and_Edit_Billing_Address() throws IOException, Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Edit ShipandBill Address");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  
	  bnreg.EditShipInfoAddressLists();
	  bnreg.EditShipAddShippingExistingAddressInShippingInformation();
	  bnreg.EditShipInfoAddressListButton();
	  bnreg.EditShipSavedAddressListContents(); 
	  bnreg.EditShipAddShippingEditButton();
	  bnreg.EditShipInfoUseExistingShippingAdd();
	  bnreg.EditShipAddShippingAddressDefaultCountrySelection();
	  bnreg.EditShipInfoAddShippingAddressEmptySpaceValidation();
	  bnreg.EditShipInfoAddAddressSpacesInShippingInfo();
	  bnreg.EditShipAddShippingFieldsHTMLTagValidation();
	  bnreg.EditShipInfoAddShippingAddressInvalidFirstName();
	  bnreg.EditShipAddShippingFirstNameBlankAlert();
	  bnreg.EditShipAddShippingAddAddressFirstNameLengthValidation();
	  bnreg.EditShipAddShippingFirstNameHTMLTags();
	  bnreg.EditShipAddShippingLastNameBlankAlert();
	  bnreg.EditShipAddShippingAddAddressLastNameLengthValidation();
	  bnreg.EditShipInfoAddShippingAddressInvalidLastName();
	  bnreg.EditShipAddShippingLastNameHTMLTags();
	  bnreg.EditShipAddShippingStreetAddressBlankAlert();
	  bnreg.EditShipAddShippingAddressLengthValidation();
	  bnreg.EditShipInfoAddShippingAddressAddressFieldValidation();
	  bnreg.EditShipAddShippingAptSuiteLengthValidation();
	  bnreg.EditShipInfoAddShippingAddressCityValidation();
	  bnreg.EditShipAddShippingCityBlankAlert();
	  bnreg.EditShipAddShippingCityLengthValidation();
	  bnreg.EditShipAddShippingStateBlankAlert();
	  bnreg.EditShipAddShippingZipcodeBlankAlert();
	  bnreg.EditShipInfoAddShippingAddressPhoneNoLengthValidation();
	  bnreg.EditShipInfoAddShippingAddressZipcodeValidation();
	  bnreg.EditShipInfoAddShippingAddressZipcodeValidationAlert();
	  bnreg.EditShipAddShippingContactNumberBlankAlert();
	  bnreg.EditShipInfoAddShippingAddressPhoneNoValidation();
	  bnreg.EditShipAddShippingCompanyLengthValidation();
	  bnreg.EditShipInfoAddShippingAddressAlertRemovalOnValidData();
	  bnreg.EditShipAddShippingStateListingValidation();
	  bnreg.EditShipToMAddAddressUseExistingAddressLinkNavigation();
	  bnreg.EditShipShippingEditDetails();
	  bnreg.EditShipShippingEditDetailsDataVerification();
	  bnreg.EditShipAddressEditPagePrefilledDetails();
	  bnreg.EditShipShippingEditDetailsSaveAddressVerfication();
	  bnreg.EditShipShippingEditAddressSuggestionList();
	  bnreg.EditShipShippingEditAddressYouEntered();
	  bnreg.EditShippingAddressShippingAddressListDisplay();
	  bnreg.EditShippingAddressSuggestedShippingAddressListUseThisAddressButton();
	  bnreg.EditShippingAddressListAddressSelection();
	  bnreg.EditShippingAddressListEditButtonSelection();
	  bnreg.EditShippingAddressListModifyAddressReverfication();
	  bnreg.EditShipAddressAddaShippingAddressPage(); 
	  bnreg.EditShipAddressAddNewAddressDetails(); 
	  bnreg.EditShipAddShippingAddressSuggestionList();
	  bnreg.EditShipAddShippingAddressSuggestedShippingAddressListDisplay();
	  bnreg.EditShipAddShippingAddressSuggestedShippingAddressListUseThisAddressButton();
	  bnreg.EditShipAddShippingAddressAddressYouEntered();
	  bnreg.EditShipAddNewAddressReEditAddress();
	  bnreg.EditShipAddShippingAddressListEditButtonSelection();
	  bnreg.EditShipAddShippingAddressListModifyAddressReverfication();
	  bnreg.EditShipAddShippingAddressListAddressSelection();
	  bnreg.EditShipAddressAddAddressFromList(); 
	  bnreg.EditShippingAddressSave();
	  bnreg.EditShipAddressChangeToAddressFromList();
	  bnreg.EditShipAddressAddressSelectFromList(); 
	  bnreg.EditShipInfoUseExistingShippingLinkNavigation(); 
	  bnreg.EditShipAddShippingAddAddressCancelBtn();

	  //Billing Address 
	 
	  bnreg.EditShipEditBillingCCardSavedAddressList(); // Saved Payment List Display
	  bnreg.EditShipAddBillingEditButton();
	  bnreg.EditShipAddBillingButtonsLists();
	  bnreg.EditShipBillingAddBillingCreditCardfields();
	  bnreg.EditShipAddBillingAddInvalidCCNumberValidations(); // New Credit Card Entering
	  bnreg.EditShipAddBillingCCNumLengthValidation();
	  bnreg.EditShipAddBillingAddInvalidCCNameAlertValidation();
	  bnreg.EditShipAddBillingCCardNameNumberValidation();
	  bnreg.EditShipAddBillingCCardNameLengthValidation();
	  bnreg.EditShipAddBillingCCardExpiryMonthValidation();
	  bnreg.EditShipAddBillingCCardMonthList();
	  bnreg.EditShipAddBillingCCardYearList();
	  bnreg.EditShipAddBillingCCardExpiryYearValidation();
	  bnreg.EditShipAddBillingCCardMnthYearDefaultTextSelection(); // Default Text and Selection
	  bnreg.EditShipAddBillingCCardExpiryYearSelection();
	  bnreg.EditShipAddBillingAddInvalidCCCsv();
	  bnreg.EditShipAddBillingCCardCsvLengthValidation();
	  bnreg.EditShipBillingAddressCountrySelection();
	  bnreg.EditShipBillingAddCCBillingInvalidFirstNamealert();
	  bnreg.EditShipBillingAddCCBillingInvalidLastName();
	  bnreg.EditShipAddBillingAddCCLastNameValidationAlert();
	  bnreg.EditShipBillingAddCCBillingStreetAddressValidation();
	  bnreg.EditShipBillingAddCCBillingCityValidation();
	  bnreg.EditShipBillingAddCCBillingZipcodeValidation();
	  bnreg.EditShipAddBillingCCBillingZipcodeValidationAlert();
	  bnreg.EditShipBillingAddCCBillingPhoneNoValidation();
	  bnreg.EditShipBillingAddCCBillingPhoneNoValidation2();
	  bnreg.EditShipBillingAddCCBillingPhoneNoLengthValidation();
	  bnreg.EditShipBillingAddCCBillingPhoneNoLengthValidationAlert();
	  bnreg.EditShipAddBillingCCBillingContactNumberValidationAlert();
	  bnreg.EditShipAddBillingCCardAddressVerficationNavigation(); // New Billing Info Add
	  bnreg.EditShipAddBillingCCardExpiryCardAlert();
	  bnreg.EditShipAddBillingCCCancelBtnNavigation();
	  bnreg.EditShipBillingEditDetails();
	  bnreg.EditShipBillingEditDetailsDataVerification();
	  bnreg.EditShipBillingAddressCardEditSelectSavedShippingAddress();
	  bnreg.EditShipBillingAddressCardEditSelectSavedShippingAddressList();
	  bnreg.EditShipBillingAddressCardEditCancelBtnNavigation();
	  bnreg.EditShipBillingAddressCardEditPrefilledDataDisplay(); // Edit Prefilled Record Datas
	  bnreg.EditShipBillingAddressCardEditPrefilledDataModify();
	  bnreg.EditShipBillingEditDetailsSaveAddressVerfication();
	  bnreg.EditShipEditBillingCCardUpdateandSave();
	  bnreg.EditShipEditBillingCCardExistingSavedAddressList(); // Saved CC List Display 
	  bnreg.EditShipEditBillingAddressCardListDisplay();
	  bnreg.EditShipEditBillingAddressCardListDisplay1();
	  bnreg.EditShipBillingAddressCardDetailsA();
	  bnreg.EditShipBillingAddressCreditCardSelect();
	  bnreg.EditShipEditBillingCCardCreateNew();
  } 
  
  @Test(priority = 4)
  public void BRM_293_Ship_To_Multiple_Address() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Ship To Multiple Add");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  
	  bnreg.ShipToMAddAddressListButton(); 
	  bnreg.ShipToMAddAddressList(); 
	  bnreg.ShipToMAddAddressUseExistingAddressLinkNavigation();
	  bnreg.ShipToMAddAddressButtonsEnableFromAddressList();
	  bnreg.ShipToMAddAddressPageScrollInAddressList();
	  bnreg.ShipToMAddAddressChooseFromAddressList();
	  bnreg.ShipToMAddAddressSelectFromAddressList();
	  bnreg.ShipToMAddAddressChangeToAddressFromList();
	  bnreg.ShipToMAddAddressAddaShippingAddressPage();
	  bnreg.ShipToMAddAddressAddAddressDetails();
	  bnreg.ShipToMAddAddressAddAddressUsethisAddressNavigation();
	  bnreg.regUserCheckoutDeliveredElectronically();
	  bnreg.ShipToMAddAddressEditFromAddressList();
	  bnreg.ShipToMAddAddressEditPageDetails();
	  bnreg.ShipToMAddAddressEditAddressDetails();
	  bnreg.ShipToMAddAddressReEditAddress();
	  bnreg.ShipToMAddAddressEditAddressUsethisAddressNavigation();
	  bnreg.ShipToMAddAddressBillingAddress();
	  bnreg.ShipToMAddAddressBillingAddressSelectCard();
	  bnreg.ShipToMAddAddressBillingAddressCardDetails();
	  bnreg.ShipToMAddAddressBillingAddressCardDetailsA();
	  bnreg.ShipToMAddAddressBillingAddressCardListDisplay();
	  bnreg.ShipToMAddAddressBillingAddressCardBtnEnable();
	  bnreg.ShipToMAddAddressBillingAddressCreditCardSelect();
	  bnreg.ShipToMAddAddressBillingAddressAddCreditCardCardBtn();
  }
  
  @Test(priority = 5)
  public void BRM_296_Delivery_Options_Registered_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Guest Delivery Options");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  
	  bnreg.regUserCheckoutDeliveryPreference();
	  bnreg.regUserDeliveryPreferenceEditButton();
	  bnreg.regUserCheckoutDeliveryPresent();
	  bnreg.regUserDeliveryPreferenceView();
	  bnreg.regUserDeliveryOptionSelect();
	  bnreg.regUserDeliveryPreferenceSelectHighlight();
	  bnreg.regUserDeliveryPreferenceDetails();
	  bnreg.regUserDeliveryPreferenceSelection();
	  bnreg.regUserMakeitasGiftContainerDefaultSelection();
	  bnreg.regUserMakeitasGiftContainerOpen();
	  bnreg.regUserMakeitasGiftContainerExpandDetails();
	  bnreg.regUserMakeitasGiftContainerDefaultText();
	  bnreg.regUserMakeitasGiftContainerDefaultTextLengthValidation();
	  bnreg.regUserMakeitasGiftContainerRemainingCharacter();
	  bnreg.regUserMakeitasGiftContainerValidation();
	  bnreg.regUserMakeitasGiftContainerApplyButton();
	  bnreg.regUserMakeitasGiftContainerDisclaimerMessage();
	  bnreg.regUserMakeitasGiftContainerCheckboxEnableDisable();
	  bnreg.regUserMakeitasGiftContainerApplyGiftCard();
	  bnreg.regUserMakeitasGiftContainerGiftCardEdit();
  }
  
  @Test(priority = 6)
  public void BRM_298_Payment_Info_Registered_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Payment Info");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  // With Billing Info
	  bnreg.payInfoBillingAddressListContents();
	  bnreg.payInfoBillingAddressCardListDisplay();
	  bnreg.payInfoBillingCCardListDisplay();
	  bnreg.payInfoBillingCreditCardDetailsA();
	  bnreg.payInfoAddBillingAddress();
	  bnreg.payInfoAddNewBillingCardElements();
	  bnreg.payInfoAddNewBillingPageElements();
	  bnreg.payInfoAddNewBillingPageValidation();
	  bnreg.payInfoAddNewBillingCCNumLengthValidation();
	  bnreg.payInfoAddNewBillingInvalidCCNumberValidations();
	  bnreg.payInfoAddNewBillingCCNameLengthValidation();
	  bnreg.payInfoAddNewBillingCCardNameNumberValidation();
	  bnreg.payInfoAddNewBillingInvalidCCNameAlertValidation();
	  bnreg.payInfoAddNewBillingCCardExpiryMonthValidation();
	  bnreg.payInfoAddNewBillingCCardExpiryYearValidation();
	  bnreg.payInfoAddNewBillingCCardMnthYearDefaultTextSelection();
	  bnreg.payInfoAddNewBillingCCCardMonthList();
	  bnreg.payInfoAddNewBillingCCardYearList();
	  bnreg.payInfoAddNewBillingCCardExpiryYearSelection();
	  bnreg.payInfoAddNewBillingCCCsvLengthValidation();
	  bnreg.payInfoAddNewBillingInvalidCCCsv();
	  bnreg.payInfoAddNewBillingLastNameValidationAlert();
	  bnreg.payInfoAddNewBillingZipcodeValidationAlert();
	  bnreg.payInfoAddNewBillingContactNumberValidationAlert();
	  bnreg.payInfoBillingCCCancelBtnNavigation();
	  bnreg.payInfoBillingAddresCCardCreateNew();
	  bnreg.payInfoEditShipBillingAddressCardEditPrefilledDataDisplay();
	  bnreg.payInfoEditShipBillingAddressCardEditPrefilledDataModify();
	  bnreg.payInfoEditShipBillingAddressCardEditSelectSavedShippingAddress();
	  bnreg.payInfoEditShipBillingAddressCardEditSelectSavedShippingAddressList();
	  bnreg.payInfoBillingAddressCardEditCancelBtnNavigation();
	  bnreg.payInfoBillingCCardAddressVerficationNavigation();
	  bnreg.payInfoEditShipAddressModificationSave();
	  bnreg.payInfoBillingCCardSavedAddressList();
	  bnreg.payInfoBillingCCardExistingSavedAddressList();
	  bnreg.payInfoBillingCreditCardSelect();
  }
  
  @Test(priority = 7)
  public void BRM_301_Membership_Gift_Card_and_Coupons_Registered_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("MemberShipGft");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  
	  bnreg.regUserCheckoutCouponContainer();
	  bnreg.regUserCheckoutCouponContainerContainerDetails();
	  bnreg.regUserCheckoutCouponDetails2();
	  bnreg.regUserCheckoutBNMembershipPgmDrpDown();
	  bnreg.regUserCheckoutBNMembershipPgmDrpDownDefault();
	  bnreg.regUserCheckoutBNMembershipPgmDrpEmptyValidation();
	  bnreg.regUserCheckoutBNMembershipPgmDrpInvalidValidation();
	  bnreg.regUserCheckoutBNMembershipPgmDrpInvalidValidation2();
	  bnreg.regUserCheckoutBNMembershipPgmDrpIncorrectValidation();
	  bnreg.regUserCheckoutCouponGftCpnBkfairDetails();
	  bnreg.regUserCheckoutGiftCouponEmptyValidation();
	  bnreg.regUserCheckoutGiftCouponInvalidCharValidation();
	  bnreg.regUserCheckoutGiftCouponInvalidValidation();
	  bnreg.regUserCheckoutGiftCouponPinValidation();
	  bnreg.regUserCheckoutGiftCouponInvalidCCValidation();
	  bnreg.regUserCheckoutCouponEmptyValidation();
	  bnreg.regUserCheckoutCouponInvalidValidation();
	  bnreg.regUserCheckoutBookFairEmptyValidation();
	  bnreg.regUserCheckoutBookFairInvalidValidation();
	  bnreg.regUserCheckoutTaxExemptFields();
	  bnreg.regUserCheckoutTaxExemptClose();
	  bnreg.regUserCheckoutTaxExemptEnable();
  } 
  
  @Test(priority = 8)
  public void BRM_289_Shipping_and_Billing_Address_Entry_and_Returning_Customer_Login() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Checkout ExistingInfo");
	  bnreg = new bnPageObjects.BNCompleteRegisteredCheckoutStories(driver,sheet);
	  
	  bnreg.checkoutElements();
	  bnreg.checkoutShippingInfo(); 
	  bnreg.checkoutTopTotal();
	  bnreg.checkoutSecureLayer();
	  bnreg.checkoutSubmitbtnColor();
	  bnreg.checkoutExistingshippingDetails();
	  bnreg.checkoutExistingshippingEditButton();
	  bnreg.checkoutOrderShippingSavedAddressList();
	  bnreg.checkoutAddShippingSavedAddressListContents();  
	  bnreg.checkoutAddShippingAddAddress(); 
	  bnreg.checkoutAddShippingAddAddressSuggestionList();
	  bnreg.checkoutAddShippingAddAddressUseThisAddressBtnVisibility();
	  bnreg.checkoutAddShippingAddAddressAddressYouEnteredDetailsVisibility();
	  bnreg.checkoutAddShippingAddAddressAddressYouEnteredDetails();
	  bnreg.checkoutAddShippingAddAddressAddressEditFeature();
	  
	  bnreg.checkoutAddShippingAddAddressAddressEditandUpdateNavigation();
	  bnreg.checkoutAddShippingAddAddressAddressSelectionfromList();
	  bnreg.checkoutAddBillingSavedAddressListContents();
	  bnreg.checkoutAddBillingPageNavigation(); 
	  bnreg.checkoutAddBillingAddNewPageElements();  
	  bnreg.checkoutAddBillingAddNewPageValidation(); 
	  bnreg.checkoutAddBillingAddInvalidCCNumberValidations(); 
	  bnreg.checkoutOrderAddBillingCCNumLengthValidation(); 
	  bnreg.checkoutOrderAddBillingCCardNameLengthValidation(); 
	  bnreg.checkoutOrderAddBillingCCardMonthList(); 
	  bnreg.checkoutOrderAddBillingCCardYearList(); 
	  bnreg.checkoutAddBillingAddInvalidCCNameAlertValidation(); 
	  bnreg.checkoutOrderAddBillingCCardNameNumberValidation(); 
	  bnreg.checkoutOrderAddBillingCCardExpiryMonthValidation(); 
	  bnreg.checkoutOrderAddBillingCCardExpiryYearValidation(); 
	  bnreg.checkoutOrderAddBillingCCardMnthYearDefaultTextSelection(); 
	  bnreg.checkoutOrderAddBillingCCardExpiryYearSelection();  
	  bnreg.checkoutOrderAddBillingCCardCsvLengthValidation(); 
	  bnreg.checkoutAddBillingAddInvalidCCCsv(); 
	  bnreg.checkoutAddBillingAddCCLastNameValidationAlert(); 
	  bnreg.checkoutOrderAddBillingCCBillingZipcodeValidationAlert(); 
	  bnreg.checkoutOrderAddBillingCCBillingContactNumberValidationAlert(); 
	  bnreg.checkoutOrderAddBillingCCardAddressVerficationNavigation(); 
	  bnreg.checkoutOrderAddBillingCCardExpiryCardAlert(); 
	  bnreg.checkoutAddBillingCCCancelBtnNavigation(); 
	  bnreg.checkoutOrderAddBillingCreditCardSelectBillingLink();
	  bnreg.checkoutOrderAddBillingExistingShippingRecords();
	  bnreg.checkoutOrderAddBillingExistingShippingRecordsSelection();
	  bnreg.checkoutOrderAddBillingExistingShippingRecordsSelectionSave();
	  bnreg.checkoutOrderAddBillingExistingShippingRecordsSelectionCancel();
	  bnreg.checkoutOrderAddBillingInvalidCCExpiredMonth();
	  bnreg.checkoutOrderAddBillingCCBillingCreateNew(); 
	  
	  bnreg.signOut();
	  
	  bnreg.checkoutAddNewButton();
	  bnreg.checkoutAddNewInfo();
	  bnreg.checkoutAdd();
	  bnreg.checkoutOrderTotalPrice();
	  bnreg.checkoutOrderSubmitOrderBtn();
	  bnreg.checkoutNoDeliveryPreference();
	  bnreg.checkoutOrderLoyaltyContainer();
	  bnreg.checkoutOrderComparison();
	  bnreg.checkoutSubmitOrderAlert();
	  bnreg.checkoutShippingAddressAddBtn();
	  bnreg.checkoutAddShippingAddressElements();
	  bnreg.checkoutAddShippingAddressFieldsInlineTxt();
	  bnreg.checkoutAddShippingDefaultCountrySelection();
	  bnreg.checkoutAddShippingAddressBtns();
	  bnreg.checkoutAddShippingAddressFieldsHighlight();
	  bnreg.checkoutAddShippingEmptySpaceValidation();
	  bnreg.checkoutAddShippingAddressFieldsMissingAlert();
	  bnreg.checkoutAddShippingAddAddressFirstNameLengthValidation();
	  bnreg.checkoutAddShippingFirstNameBlankAlert();
	  bnreg.checkoutspacesInShippingInfo();
	  bnreg.checkoutAddShippingFirstNameHTMLTags();
	  bnreg.checkoutAddShippingAddressFirstNameValidation();
	  bnreg.checkoutAddShippingLastNameHTMLTags();
	  bnreg.checkoutAddShippingLastNameBlankAlert();
	  bnreg.checkoutAddShippingAddAddressLastNameLengthValidation();
	  bnreg.checkoutAddShippingAddressLastNameValidation();
	  bnreg.checkoutAddShippingAddressLengthValidation(); 
	  bnreg.checkoutAddShippingStreetAddressBlankAlert();
	  bnreg.checkoutAddShippingAddressAddressValidation();
	  bnreg.checkoutAddShippingAptSuiteLengthValidation();
	  bnreg.checkoutAddShippingCityBlankAlert(); 
	  bnreg.checkoutAddShippingCityLengthValidation(); 
	  bnreg.checkoutAddShippingCityValidation();
	  bnreg.checkoutAddShippingZipcodeBlankAlert();
	  bnreg.checkoutAddShippingAddressZipcodeValidation();
	  bnreg.checkoutAddShippingAddressZipcodeValidationAlert();
	  bnreg.checkoutAddShippingStateBlankAlert(); 
	  bnreg.checkoutAddShippingContactNumberBlankAlert(); 
	  bnreg.checkoutAddShippingPhoneNoValidation();    // Please Check
	  bnreg.checkoutAddShippingPhoneNoValidation2();
	  bnreg.checkoutAddShippingPhoneNoLengthValidation();
	  bnreg.checkoutAddShippingCompanyLengthValidation();  
	  bnreg.checkoutAddShippingOtherFieldHTMLTagValidation();
	  bnreg.checkoutAddShippingAlertRemovalOnValidData();
	  bnreg.checkoutAddShippingStateLists();
	  bnreg.checkoutAddShippingStateListingValidation();
	  bnreg.checkoutAddShippingAddAddressCancelBtn();
	  
	  // Billing Information
	  
	  bnreg.checkoutOrderAddBillingInfo(); // Uncomment the line to run continuously
	  bnreg.checkoutOrderAddBillingCreditCardfields();
	  bnreg.checkoutOrderAddBillingCreditCardAddBillOption();
	  bnreg.checkoutOrderAddBillingCCBillingTabFocus();
	  bnreg.checkoutOrderAddBillingCCNumberLengthValidation();
	  bnreg.checkoutOrderAddBillingCCMonthYearDefaultText();
	  bnreg.checkoutOrderAddBillingCCMonthList();
	  bnreg.checkoutOrderAddBillingCCYearList();
	  bnreg.checkoutOrderAddBillingCCInvalidNumberValidation();
	  bnreg.checkoutOrderAddBillingCCNumberLengthAlert();
	  bnreg.checkoutOrderAddBillingCCNameValidation();
	  bnreg.checkoutOrderAddBillingInvalidCCNameAlert();
	  bnreg.checkoutOrderAddBillingCCBlankAlert();
	  bnreg.checkoutOrderAddBillingCCBillingBlankSpaceValidation();
	  bnreg.checkoutOrderAddBillingCCBillingBlankAddressAlert();
	  bnreg.checkoutOrderAddBillingCCBillingCountrySelection(); // Need to test
	  bnreg.checkoutOrderAddBillingCCBillingInvalidFirstNamealert();
	  bnreg.checkoutOrderAddBillingCCBillingInvalidLastName();
	  bnreg.checkoutOrderAddBillingCCBillingStreetAddressValidation(); /// Need to test
	  bnreg.checkoutOrderAddBillingCCBillingZipcodeValidation();
	  bnreg.checkoutOrderAddBillingCCBillingZipcodeLengthValidation();   // Need to test
	  bnreg.checkoutOrderAddBillingCCBillingCityValidation();
	  bnreg.checkoutOrderAddBillingCCBillingPhoneNoValidation();
	  bnreg.checkoutOrderAddBillingCCBillingPhoneNoLengthValidation();
	  bnreg.checkoutOrderAddBillingCCBillingPhoneNoLengthValidationAlert();
	  bnreg.checkoutOrderAddBillingCCBillingiicon();
  }
  
  //@Test(priority = 9)
  public void removeAddress()
  {
	  
  }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }
  
    @BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
  

}
