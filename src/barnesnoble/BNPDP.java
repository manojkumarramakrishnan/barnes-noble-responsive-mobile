package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import bnPageObjects.BNPDPObject;
import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNPDP
{
  
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNPDPObject pdp; 
  
  @BeforeClass
  public void beforeClass() throws Exception 
  {
	  //Thread.sleep(8000);
	  driver = BNBasicfeature.setMobileView();
  }
  
  @Test(priority = 1)
  public void BRM_1434_PDP_Page_Implementation() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("PDP");  
	  pdp = new BNPDPObject(driver,sheet);
	  
	  /*pdp.PDPPageNavigationPLPSingleImage();
	  
	   Print Magazine PDP  
	  pdp.pdpPagePrintMagazineFormatOption(); 
	  pdp.pdpPagePrintMagazineSubscriptionDetails();
	  pdp.pdpPagePrintMagazineSubscriptionAddtoBag();
	  
	   Gift Card 
	  
	  pdp.pdpPageGiftCardCreative(); 
	  //pdp.pdpPageGiftCardImageClick(); 
	  pdp.pdpPageGiftCardTitleSubtitle();
	  pdp.pdpPageGiftCardValue();
	  pdp.pdpPageGiftCardValueDefaultText();
	  pdp.pdpPageGiftCardNoOfCardsDropDown();
	  pdp.pdpPageGiftCardNoOfCardsDropDownDefaultText();
	  pdp.pdpPageGiftCardNoOfCardsDropDownRange();
	  pdp.pdpPageGiftCardNoOfCardsSelection();
	  pdp.pdpPageGiftCardTOField();
	  pdp.pdpPageGiftCardTOFieldDefaultText();
	  pdp.pdpPageGiftCardToFieldLengthValidation();
	  pdp.pdpPageGiftCardFromField();
	  pdp.pdpPageGiftCardFromFieldDefaultText();
	  pdp.pdpPageGiftCardFromFieldLengthValidation();
	  pdp.pdpPageGiftCardGiftMessage();
	  pdp.pdpPageGiftCardGiftDefaultText();
	  pdp.pdpPageGiftCardGiftLengthValidation();
	  pdp.pdpPageGiftCardTotalPrice();
	  pdp.pdpPageGiftCardAddtoBag();
	  pdp.pdpPageGiftCardShippingInfo();
	  pdp.pdpPageProductGiftCardPageCustomerBoughtSection();
	  pdp.pdpPageProductGiftCardCustomerBoughtSectionPageDetails();
	  pdp.pdpPageProductGiftCardCustomerBoughtSectionPageScroll();
	  pdp.pdpPageGiftCardValueInvalidValidation();
	  pdp.pdpPageGiftCardValueValidation();
	  pdp.pdpPageGiftCardNoOfCardsAlert();
	  pdp.pdpPageGiftCardValueAmountValidation();
	  pdp.pdpPageGiftCardValueLowAmountValidation();
	  pdp.pdpPageGiftCardValueHighAmountValidation();
	  pdp.pdpPageGiftCardValueValidationHighlight();
	  pdp.pdpPageGiftCardTOFieldValidation();
	  pdp.pdpPageGiftCardToFieldValidationHighlightAlert();
	  pdp.pdpPageGiftCardFromFieldValidation();
	  pdp.pdpPageGiftCardFromFieldValidationHighlightAlert();
	  pdp.pdpPageGiftCardAlphaNumericAlert();
	  pdp.pdpPageGiftCardValueExceedAlert();
	  pdp.pdpPageGiftCardGiftFieldValidation();
	  pdp.pdpPageGiftCardGiftFieldRemainingCharacter(); 
	  pdp.pdpPageGiftCardTotalPriceCalculation();
	  pdp.pdpPageProductGiftCardCustomerBoughtNavigation();
	  pdp.pdpPageGiftCardAddGifttoBagSuccessMessage();
	  pdp.pdpPageGiftCardShopBagClick(); 
	  //pdp.checkoutBtnNavigation();  --> To be Included
	  
	   Music PDP   
	  pdp.pdpPageProductMusicTrackAccordian();
	  pdp.pdpPageProductMusicTrackAccordianShowMore();
	  pdp.pdpPageProductMusicTrackAccordianCollapse();
	  pdp.pdpPageProductRelatedSubjectAccordian();
	  
	   Celebrity CookBooks PDP  
	  pdp.pdpPageCookBookPromoBadge();
	  pdp.pdpPageProductPromoMsg();
	  pdp.pdpPageProductBookPromoMsg();
	  
	   Movie PDP  
	  
	  pdp.pdpPageMovieProductDetails();
	  pdp.pdpPageProductBookPickUpInStore();
	  pdp.pdpPageProductBookPickUpInStoreClick();
	  pdp.pdpPageMovieProductDropDown();
	  pdp.pdpPageMovieProductDropDownDetails();
	  pdp.pdpPageMovieProductBluRayPriceDetails();
	  pdp.pdpPageMovieCastCrewAccordianExpand();
	  
	  /* Nook Magazine PDP   
	  
	  pdp.pdpPageMagazineProduct();
	  pdp.pdpPageMagazineCurrentIssue();
	  pdp.pdpPageMagazineProduct14DaysTrial();
	  pdp.pdpPageMagazineProduct14DaysTrialDetails();
	  pdp.pdpPageMagazineProduct14DaysManageSubscription();
	  pdp.pdpPageMagazineProduct14DaysManageDefaultPayment();
	  pdp.pdpPageMagazineProduct14DaysFAQ();
	  pdp.pdpPageMagazineProduct14DaysOverlayClose();
	  pdp.pdpPageMagazineNookDeviceToolTip();
	  pdp.pdpPageMagazineNookAnnualSubscription();
	  pdp.pdpPageMagazineNookAnnualSubscriptionAddToBag();
	  pdp.pdpPageMagazineNookAnnualSubscriptionInstantPurchase();
	  pdp.pdpPageMagazineNookAnnualSubscriptionDeliverdWeeklyText();
	  pdp.pdpPageMagazineNookAnnualSubscriptionMonthlySubscriberText();
	  pdp.pdpPageMagazineNookAnnualSubscriptionHelpOverlay();
	  pdp.pdpPageMagazineNookAnnualSubscriptionHelpOverlayClose();
	  pdp.pdpPageMagazineNookMonthlySubscription();
	  pdp.pdpPageMagazineCurrentIssueDetails();
	  pdp.pdpPageMagazineNookMonthlyAddToBag();
	  pdp.pdpPageMagazineNookAnnualSubscriptionInstantPurchaseLoginNavigation();
	  
	  /* Textbook PDP  */
	  
	  pdp.pdpPageProductISBNandEdition();
	  pdp.pdpPageProductISBNExpand();
	  pdp.pdpPageISBNCollapse();
	  pdp.pdpPageProductSavepercentage();
	  pdp.pdpPageBuyTypeDropDown();
	  pdp.pdpPageBuyTypeDropDownValues();
	  pdp.pdpPageEstReturnDate();
	  pdp.pdpPageRentalBookMessage();
	  pdp.pdpPageRentalPopUpClose();
	  pdp.pdpPageTextBooxShipInfo();
	  pdp.pdpPageBuyTypeLists();
	  pdp.pdpPageBuyUsed();
	  pdp.pdpPageMarketPlace();
	  pdp.pdpPageProductTableOfContentsExpand(); 
	  pdp.pdpPageProductTableOfContentsCollapse(); 
	  
	  /* Nook Device PDP 
	  pdp.pdpPageNookDeviceOfferMessage();
	  pdp.pdpPageNookDeviceOfferMessageLinkValidation();
	  pdp.pdpPageNookDevicePrice(); 
	  pdp.pdpPageProductNookDevicePickUpInStore(); 
	  pdp.pdpPageProductNookDevicePickUpInStoreClick();
	  pdp.pdpPageNookDeviceShippingInfo(); //--> Nook Shipping Info
	  pdp.pdpPageNookDeviceShippingDetailsInfo();
	  pdp.pdpPageNookDeviceVideoContainer();
	  pdp.pdpPageNookDeviceVideoTitle();
	  pdp.pdpPageNookDeviceVideoOptions();
	  pdp.pdpPageNookFeatureContent();
	  pdp.pdpPageNookFeatureContentDetails();
	  pdp.pdpPageNookSpecs();
	  pdp.pdpPageNookSpecsDetails();
	  //pdp.pdpPageNookSpecsOtherDetails(); //Partial Implementation*
	  pdp.pdpPageNookSpecsAccessories();
	  pdp.pdpPageNookSpecsAccessoriesDetails();
	  pdp.pdpPageNookSpecsAccessoriesScroll();
	  pdp.pdpPageNookSpecsAccessoryPageNavigation();
	  
	   Toys PDP  
	  pdp.pdpPageFreeShipMessage();
	  pdp.pdpPageProductBookPickUpInStore();
	  pdp.pdpPageProductBookPickUpInStoreClick();
	  pdp.pdpPageFreeShipMessageClickHere();
	  
	   E - Gift Card 
	  
	  pdp.pdpPageEGiftCardCreative();
	  //pdp.pdpPageEGiftCardImageClick(); 
	  pdp.pdpPageEGiftTitleSubtitle();
	  pdp.pdpPageEGiftCardValue();
	  pdp.pdpPageEGiftCardReceipentFieldDefaultText();
	  pdp.pdpPageEGiftCardConfirmEmailDefaulttext();
	  pdp.pdpPageEGiftCardToFieldDefaulttext();
	  pdp.pdpPageEGiftCardFromFieldDefaulttext();
	  pdp.pdpPageEGiftCardFieldsHighlight();
	  pdp.pdpPageEGiftCardReceipentField();
	  pdp.pdpPageEGiftCardReceipentFieldValidationAlert();
	  pdp.pdpPageEGiftCardReceipentFieldInvalidEmailValidation();
	  pdp.pdpPageEGiftCardReceipentFieldValidEmailValidation();
	  pdp.pdpPageEGiftCardConfirmEmail();
	  pdp.pdpPageEGiftCardConfirmEmailValidationAlert();
	  pdp.pdpPageEGiftCardConfirmEmailInvalidEmailValidation();
	  pdp.pdpPageEGiftCardConfirmEmailValidEmailValidation();
	  pdp.pdpPageEGiftCardReceipentEmailConfirmEmailLenghtValidation();
	  pdp.pdpPageEGiftCardToField();
	  pdp.pdpPageEGiftCardToFieldLenghtValidation();
	  pdp.pdpPageEGiftCardToFieldInvalidValidationAlertHighlight();
	  pdp.pdpPageEGiftCardToFieldValidation();
	  pdp.pdpPageEGiftCardFromField();
	  pdp.pdpPageEGiftCardFromFieldLenghtValidation();
	  pdp.pdpPageEGiftCardFromFieldValidation();
	  pdp.pdpPageEGiftCardFromFieldInvalidValidationAlertHighlight();
	  pdp.pdpPageEGiftCardGiftMessage();
	  pdp.pdpPageEGiftCardPrice(); 
	  
	   Book PDP  
	  
	  pdp.PDPPageNavigationfromPLP();
	  pdp.PDPPageProductImage();
	  pdp.PDPPageDefaultImage();
	  pdp.PDPPageAdditionalImage();
	  pdp.PDPPageImageSwipe();
	  pdp.PDPPageImageCarouselHighlight();
	  pdp.PDPPageImageCarouselCyclicRotation();
	  pdp.PDPPageProductandAuthorName();
	  pdp.PDPPageRating();
	  pdp.PDPPageAddtoWishList();
	  pdp.PDPPageAddtoWishList1();
	  pdp.pdpPageProductDesc();
	  pdp.pdpPageFormatOptions();
	  pdp.pdpPageFormatOptionsOverview();
	  pdp.pdpPageProductInstantPurchase();
	  pdp.pdpPageProductPrice();
	  //pdp.pdpPageProductBookPickUpInStore();
	  //pdp.pdpPageProductBookPickUpInStoreClick();
	  pdp.pdpPageProductPickUpInStore();
	  pdp.pdpPagePickUpInstoreInValidZip();
	  pdp.pdpPagePickUpInstoreNoStoreZipCode();
	  pdp.pdpPagePickUpInstoreValidZip();
	  pdp.pdpPageProductProductAccordion();
	  pdp.PDPPageReviewPanel();
	  pdp.pdpPageProductOverviewExpand();
	  pdp.pdpPageProductOverViewAccordian();
	  pdp.pdpPageProductOverviewCollapse();
	  pdp.pdpPageProductDescReadMore();
	  pdp.pdpPageReadMoreOverview();
	  pdp.pdpPageProductOverviewCollapse();
	  pdp.pdpPageEditorialReviewExpand();
	  pdp.pdpPageProductEditorialReviewReadMore();
	  pdp.pdpPageProductEditorialReviewCollapse();
	  pdp.pdpPageProductProductDetailsExpand();
	  pdp.pdpPageProductProductDetailsReviewReadMore();
	  pdp.pdpPageProductProductDetailsCollapse();
	  pdp.pdpPageProductRelatedSubjectsExpand();
	  pdp.pdpPageProductRelatedSubjectsCollapse();
	  pdp.pdpPageProductRelatedSubjectsDetails();
	  pdp.pdpPageProductReadTheExcerptExpand();
	  pdp.pdpPageProductReadTheExcerptReadMore();
	  pdp.pdpPageProductReadTheExcerptCollapse();
	  pdp.pdpPageProductMeetTheAuthorExpand();
	  pdp.pdpPageProductMeetTheAuthorReadMore();
	  pdp.pdpPageProductMeetTheAuthorCollapse();
	  pdp.pdpPageProductCustomerReviewExpand();
	  pdp.pdpPageProductCustomerReviewDetails();
	  pdp.pdpPageLoadMoreReview(); 
	  pdp.pdpPageCustomerReviewCollapse();
	  pdp.pdpPageBookShipFreeInfo();
	  pdp.pdpPageBookShipSameDayInfo();
	  pdp.pdpPageProductSocialMediaIcons();
	  pdp.pdpPageProductSocialMediaNavigation();
	  pdp.pdpPageProductCustomerBoughtSection();
	  pdp.pdpPageProductCustomerBoughtSectionDetails();
	  pdp.pdpPageProductCustomerBoughtSectionScroll();
	  pdp.pdpPageProductCustomerBoughtNavigation();
	  //pdp.pdpPageNookBookPDPImageClick();
	  //pdp.pdpPageNookBookPDPImageClose();
	  pdp.pdpPageNookBookInstantPurchase(); 
	  pdp.pdpPageFormatOptionsValues();
	  pdp.pdpPageFormatOptionsSelection();
	  pdp.pdpPageFormatOptionsSelectionPrice();
	  pdp.pdpPageProductAddtoBagSuccessOverlay();
	  pdp.pdpPageProductAddtoBag();
	  pdp.AddtoWishListSignInNavigation(); 
	  
	   WishList 
	  pdp.navigationToWishlist();
	  pdp.wishListOverlay();
	  pdp.wishlistOptions();
	  pdp.wishlistAddItemButton();
	  pdp.wishlistTextSelectWishlistNotClickable();
	  pdp.createWishList();
	  pdp.wishlistTextCreateWishListNotClickable();
	  pdp.createWishListOverlay();
	  pdp.wishlistClose();
	  pdp.wishListAddItem();
	  pdp.wishListAddedclose();
	  
	   Instant Purchase Credit Card   
	  pdp.pdpPageInstantPurchasePromptCreditCard();
	  pdp.pdpPageInstantPurchasePromptCreditCardToolTip();
	  pdp.pdpPageInstantPurchasePromptCreditCardCVVLength();
	  //pdp.pdpPageInstantPurchaseInvalidSecurityAlert();
	  pdp.pdpPageInstantPurchasePopUpClose();
	  pdp.miniCart();
	  pdp.miniCartEmptyBagSignedInUserBtns();
	  
	   Instant Purchase Empty  
	  
	  pdp.miniCartSigoutBagCount();
	  pdp.pdpPageInstantPurchase();
	  pdp.pdpPageInstantPurchaseClose();
	  pdp.pdpPageInstantPurchaseClose1();
	  pdp.pdpPageInstantPurchaseManageDefaultAddress();
	  pdp.pdpPageInstantPurchaseManageDefaultSpeed();
	  pdp.pdpPageInstantPurchaseManageDefaultPayment();
	  pdp.pdpPageInstantPurchaseManageEditPayment();
	  //pdp.pdpPageNavigationFromMenu(); 
*/  }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }
  
    @BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
  
  
}
