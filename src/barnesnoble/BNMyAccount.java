package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;
import bnPageObjects.BNMyAccObj;

public class BNMyAccount extends ExtentRptManager
{
  
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNMyAccObj myAcc;
  
  @BeforeClass
  public void beforeTest() throws Exception 
  {
	//Thread.sleep(10000);
	driver = BNBasicfeature.setMobileView();
  }
  
  @Test(priority = 1)
  public void BRM_2487_My_Account_Landing_Page_Implementation() throws Exception 
  {
	  sheet = BNBasicfeature.excelsetUp("My Account");
	  myAcc = new BNMyAccObj(driver,sheet);
	  myAcc.myAccountBefLogin();
	  myAcc.myAccountPageHeaderFooterDet();
	  myAcc.myAccountOrderBefLoginDet(); //
	  myAcc.myAccountOrderEmailLimit();
	  myAcc.myAccountEmptyValidation();
	  myAcc.myAccountInvalidDataValidation();
	  myAcc.myAccountSpecialCharEmailAdd();
	  myAcc.myAccountInvalidEmailId();
	  myAcc.myAccountInvalidEmailId1();
	  myAcc.myAccountInvalidOrderNumber();
	  myAcc.myAccountNoResOrderNumber();
	  myAcc.myAccountSignInLink();
	  //myAcc.myAccountOrderExpand();
	  //myAcc.myAccounExpandCollapse();  
	  myAcc.myAccountsignInUserName();
	  myAcc.myAccountLandingPageDetails();
	  myAcc.myAccountLandingPageOrderDetails();
	  myAcc.myAccountLandingPageCaseSensitive();
	  myAcc.myAccountLandingPageScroll();
	  myAcc.myAccountLandingPageWishlist();
	  myAcc.myAccountLandingPageOrderHistoryDisplay();
	  myAcc.myAccountLandingPageTextColor();
	  myAcc.myAccountLandinPageSideArrow();
	  myAcc.myAccountLandingPageAddressOverflow();
	  myAcc.myAccoundLandingPageGiftCards();
	  myAcc.myAccoundLandingPageMembershipCards();
	  myAcc.myAccountLandingPageMembeshipTextColor();
	  //myAcc.myAccountPaymentExpand();
	  //myAcc.myAccountPaymentExpandIcon();
	  //myAcc.myAccountPaymentCollapse();
	  //myAcc.myAccountSettingsExpand();
	  //myAcc.myAccountWishListExpand();
  }
  
  @Test(priority = 2)
  public void BRM_2488_My_Account_Order_Implementation() throws Exception 
  {
	  sheet = BNBasicfeature.excelsetUp("My Account");
	  BNMyAccObj myAcc = new BNMyAccObj(driver,sheet);
	  myAcc.myAccountMyOrderNavigation();
	  myAcc.myAccountOrderPageOrderDetails();
	  myAcc.myAccountSearchDefaultText();
	  myAcc.myAccountOrderHistoryFields();
	  myAcc.myAccountOrderDropDown("BRM2693");
	  myAcc.myAccountOrderDDDefault();
	  myAcc.myAccountOrderDDDefaultValue();
	  myAcc.myAccountOrderDropDown("BRM5451");
	  myAcc.myAccountNOOrderSrch();
	  myAcc.myAccountInvalidONOSrch1();
	  myAcc.myAccountOrderNoFilter();
	  myAcc.myAccountInvalidONOSrch();
  }
  
  @Test (priority = 3)
  public void BRM_2490_My_Account_Setting_Implementation() throws Exception
  {
	  sheet = BNBasicfeature.excelsetUp("My Account");
	  BNMyAccObj myAcc = new BNMyAccObj(driver,sheet);
	  //myAcc.orderStatusNavigation();
	  // Account Settings Section
	  myAcc.myAccountNamePageNavigation();
	  //myAcc.myAccountAccountSettingsContainerDetails();
	  //myAcc.myAccountEmailPreferenceLinkNavigation();
	  //myAcc.myAccountNewsStoresLinkNavigation();
	  //myAcc.myAccountAddressBookContainer();
	  //myAcc.myAccountAddressBookNavigation();
	  //myAcc.myAccountMembershipContainer();
	  
	  // Manage Accounts Name and Email Update 
	  myAcc.myAccountManageAccountSettingsDetails();
	  //myAcc.myAccountMangeAccountNav();
	  myAcc.myAccountFLNameLimit();
	  myAcc.myAccountFLInvalidValidation();
	  myAcc.myAccountChangeNameCancelButton();
	  myAcc.myAccountUpdatefname();
	  myAcc.myAccountNameSuccessCloseButton();
	  myAcc.myAccountNameSuccessOverlayClose();
	  
	  // Change Name
	  myAcc.myAccountEmailPageNavigation();
	  myAcc.myAccountChangeEmail();
	  myAcc.myAccountEmailDetails();
	  myAcc.myAccEmailLengthValidation();
	  myAcc.myAccPassLengthValidation();
	  myAcc.myAccountNewEmailValidation();
	  
	  myAcc.myAccountManageAddressBookNavigation();
	  
	  // Change Pass
	  myAcc.myAccountPasswordPageNavigation();
	  myAcc.myAccountChangePass();
	  myAcc.myAccountChangePassDet();
	  myAcc.myAccNewPassLimitValidation();
	  myAcc.myAccountEmptyPassValidation();
	  myAcc.myAccountPassStrucValidation();
	  myAcc.myAccountChangePassCancel();
	  
	  // Change Security Question
	  myAcc.myAccountChangeSecQuestion();
	  myAcc.myAccountSecurityQuesDetails();
	  myAcc.myAccountSecQuesLimit();
	  myAcc.myAccountSecQuesLimitValidation();
	  myAcc.myAccountSecQuesCancel();
	  
	  myAcc.myAccountDeliverySpeed(); 
	  myAcc.myAccountManageDefPayNavigation();
	  
	  // Content Settings
	  myAcc.myAccountContentPageNavigation();
	  myAcc.myAccountContentSettings();
	  myAcc.myAccountContentSettingDetails();
	  myAcc.myAccountContentSettChangeCancel();
	  myAcc.myAccountContentSettingUpdate();
	  myAcc.myAccountContentSettingSuccessCloseButton();
	  myAcc.myAccountContentSettSuccessOverlayClose();
	  
	  // Instant Purchase
	  myAcc.myAccountInstantPurchasePageNavigation();
	  myAcc.myAccountInstantPurchaseSettings();
	  myAcc.myAccountInstantPurchaseRadio();
	  myAcc.myAccountInstantPurchaselabel();
	  myAcc.myAccountInstantPurchaseDefSelect();
	  myAcc.myAccountInstantPurchaseDetails();
	  myAcc.myAccountInstantPurchaseChangeCancel();
	  myAcc.myAccountInstantPurchaseUpdate();
	  myAcc.myAccountInstantPurchaseSuccessCloseButton();
	  myAcc.myAccountInsantPurchaseSuccessOverlayClose();
	  
	  // Default Delivery Speed
	  myAcc.myAccountPaymentsPageNavigation();
	  //myAcc.myAccountDeliverySpeed(); --> Included
	  //myAcc.myAccountManageDefPayNavigation(); --> Included
	  myAcc.myAccountManagePayPageDet();
	  myAcc.myAccountRemovePayOverlay();
	  myAcc.myAccountNewPayDet();
	  myAcc.myAccountAddShippingDefaultCountrySelection("BRM2630");
	  myAcc.myAccountStateDefSelection("BRM2637"); 
	  myAcc.myAccountBillingSectionDet();
	  myAcc.myAccountAddpayEmptyValidation();
	  myAcc.myAccountAddPayCCLimit();
	  myAcc.myAccountAddPayMonth();
	  myAcc.myAccountAddPayYearList();
	  myAcc.myAccountAddPayCurrYear();
	  myAcc.myAccountCountryTextBox("BRM2632");
	  myAcc.myAccountAddShippingStateListingValidation("BRM2631");
	  myAcc.myAccountBillingCanadaZipValidation();
	  myAcc.myAccountBillValidation();
	  myAcc.myAccountBillFNLNValidation();
	  myAcc.myAccountAddBillingZipCodeLimit("BRM2638");
	  myAcc.myAccountAddBillingZipCodeLimit("BRM2641");
	  myAcc.myAccountAddBillingCCBillingZipcodeValidation();
	  myAcc.myAccountAddBillingCCBillingPhoneNoValidation("BRM2633");
	  myAcc.myAccountAddBillingSpecialCharPhoneNo();
	  myAcc.myAccountAddBillingCCBillingPhoneNoValidation1();
	  myAcc.myAccountPhoneNumberLengthValidation("BRM2635");
	  myAcc.myAccountAddPayAddBillingAddress();
	  myAcc.myAccountSelectSavedbillingAdd();
	  myAcc.myAccountSavedAddressDet();
	  myAcc.myAccountAddBillingCancelNavigation();
	  myAcc.myAccountpaymentPageEditPayment();
	  //myAcc.navigatetoAccount();
	  //myAcc.navigatetoAccount();
	  //myAcc.settingsContainerOpen();
	  //myAcc.myAccountManageAccSettingsLinkNavigation();
	  myAcc.myAccountShippingAddressPageNavigation();
	  //myAcc.myAccountManageAddressBookNavigation(); - > Linked
	  myAcc.myAccountDefaultAddressDetails();
	  myAcc.myAccountDefaultAddressRemoveLink();
	  myAcc.myAccountAddShippingAddressNavigation();
	  myAcc.myAccountAddShippingDetails();
	  myAcc.myAccountAddShippingEmptyValidation();
	  myAcc.myAccountAddShipFNLNValidation();
	  myAcc.myAccountAddShippingDefaultCountrySelection("BRM2657");
	  myAcc.myAccountStateDefSelection("BRM2665"); 
	  myAcc.myAccountCountryTextBox("BRM2660");
	  myAcc.myAccountAddShipPhoneNoValidation();
	  myAcc.myAccountAddShippingSpecialCharPhoneNo();
	  myAcc.myAccountAddBillingZipCodeLimit("BRM2666");
	  myAcc.myAccountAddBillingZipCodeLimit("BRM2669");
	  myAcc.myAccountAddShippingPhoneNoValidation1();
	  myAcc.myAccountAddShippingCntNoValidation();
	  myAcc.myAccountPhoneNumberLengthValidation("BRM2663");
	  myAcc.myAccountAddShippingStateListingValidation("BRM2659");
	  myAcc.myAccountShippingCanadaZipValidation();
	  myAcc.myAccountAddShippingCancelNavigation();
	  myAcc.myAccountPaymentPageEditShipping();
  }
  
  @Test(priority = 4)
  public void BRM_2489_My_Account_Payments_Implementation() throws Exception
  {
	  sheet = BNBasicfeature.excelsetUp("My Account");
	  BNMyAccObj myAcc = new BNMyAccObj(driver,sheet);
	  myAcc.myAccPaymentExpandDet("BRM-2701");
	  myAcc.myAccPaymentExpandDet("BRM-2702");
	  myAcc.myAccountPaymentCCDetails();
	  //myAcc.myAccountManageDefNavigation();
	  //myAcc.navigatetoAccount();
	  //myAcc.myAccountManageDefPaymentNavigation();
	  //myAcc.navigatetoAccount();
	  //myAcc.myAccountAddNewPayNav();
	  //myAcc.navigatetoAccount();
	  myAcc.myAccountNavigatetoGiftCardsNav();
	  myAcc.myAccAddGftCardDet();
	  myAcc.myAccInvalidGftCard();
	  myAcc.myAccAddGiftPinLimit();
	  //myAcc.navigatetoAccount();
	  myAcc.myAccChkBalNavigation();
	  myAcc.myAccCheckBalDet();
	  myAcc.myAccountChkBalEmptyVal();
	  myAcc.myAccountChkBalEmptyPinVal();
	  myAcc.myAccGiftCardLimit();
	  myAcc.myAccGiftPinLimit();
	  //myAcc.navigatetoAccount();
	  //myAcc.myAccGftCardNavigation();
	  myAcc.myAccountLandinFooterLinksNav();
	  myAcc.myAccountLandinTextBookRentalNav();
  }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }
  
    @BeforeMethod
	public void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
 }
