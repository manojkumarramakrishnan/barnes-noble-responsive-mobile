package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNGuestUserCheckoutStories {
  
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	bnPageObjects.BNCompleteGuestCheckoutStories bnguest;
	  
  @BeforeClass
  public void beforeClass() throws Exception 
  {
	  //Thread.sleep(10000);
	  driver = BNBasicfeature.setMobileView();
  }
  
  @Test(priority = 1)
  public void BRM_290_Guest_Shipping_and_Billing_Address_Entry_For_New_Customers() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Guest New Entry");
	  bnguest = new bnPageObjects.BNCompleteGuestCheckoutStories(driver,sheet);
	  
	  bnguest.signInorCheckoutoverlay(false);
	  //bnguest.guestSignInOverlayNavigation();
	  bnguest.guestsignInOverlayDetails();
	  bnguest.guestCheckoutoverlay();
	  bnguest.guestCheckoutfields();
	  bnguest.guestNoDeliveryNavigation();
	  bnguest.guestCheckoutTabKeyUse();
	  bnguest.guestCheckoutCustomerServiceNavigation("BRM651");
	  bnguest.guestCheckoutTermsOfUseNavigation("BRM652");
	  bnguest.guestCheckoutTermsOfUseNavigation2("BRM655");
	  bnguest.guestCheckoutCopyrightsPageNavigation("BRM653");
	  bnguest.guestCheckoutPolicyPageNavigation("BRM654");
	  bnguest.guestCheckoutBlankSaveAlertValidation();
	  bnguest.guestCheckoutDefaultCountry();
	  bnguest.guestCheckoutDefaultText();
	  bnguest.guestCheckoutFirstNameLengthValidation();
	  bnguest.guestCheckoutLastNameLengthValidation();
	  bnguest.guestCheckoutStreetAddressLengthValidation();
	  bnguest.guestCheckoutAptSuiteAddressLengthValidation();
	  bnguest.guestCheckoutCityLengthValidation();
	  bnguest.guestCheckoutStateSelectionValidation();
	  bnguest.guestCheckoutZipCodeLengthValidation();
	  bnguest.guestCheckoutPhoneNumberLengthValidation();
	  bnguest.guestCheckoutCompanyLengthValidation();
	  bnguest.guestCheckoutInvalidFieldDataValidation();
	  bnguest.guestCheckoutEnterdatas();
	  bnguest.guestCheckoutAddressVerification();
	  bnguest.guestCheckoutAddressVerificationEdit();
	  bnguest.guestCheckoutDeliveryPageNavigation();
	  bnguest.guestCheckoutBottomSummarySectionDetails();
	  bnguest.guestCheckoutBreadcrumHighlight();
	  bnguest.guestCheckoutOtherwaysToPay();
	  bnguest.guestCheckoutOtherwaysToPayNavigation();
	  bnguest.guestCheckoutOtherwaysToPayNavigationDet();
  }
  
  @Test(priority = 2)
  public void BRM_296_Delivery_Options_Guest_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Guest Delivery Options");
	  bnguest = new bnPageObjects.BNCompleteGuestCheckoutStories(driver,sheet);
	  
	  bnguest.guestDeliveryOptions();
	  bnguest.guestCheckoutDeliveryPage();
	  bnguest.guestCheckoutDeliveryPageSummaryContainer();
	  bnguest.guestCheckoutDeliveryPreferenceOptions();
	  bnguest.guestCheckoutDeliveryPreferenceDefaultSelection();
	  bnguest.guestCheckoutDeliveryPreferenceHighlight();
	  bnguest.guestCheckoutDeliveryShippingMethods();
	  bnguest.guestCheckoutDeliveryShippingMethodsDefaultSelection();
	  bnguest.guestCheckoutShippingMethodsHighlight();
	  bnguest.guestCheckoutCartItemDetails();
	  bnguest.guestCheckoutSummaryContainerPriceDetails();
	  bnguest.guestCheckoutDeliveryEstimatedTax();
	  bnguest.guestCheckoutDeliverySummaryContainerEstimatedShipping();
	  bnguest.guestCheckoutDeliveryOrderTotal();
	  bnguest.guestCheckoutCustomerServiceNavigation("BRM673");
	  bnguest.guestCheckoutTermsOfUseNavigation("BRM674");
	  bnguest.guestCheckoutCopyrightsPageNavigation("BRM675");
	  bnguest.guestCheckoutPolicyPageNavigation("BRM676");
	  bnguest.guestCheckoutTermsOfUseNavigation2("BRM677");
	  bnguest.guestCheckoutDeliveryBreadcrumHighlight();
	  bnguest.guestCheckoutPaymentsPageNavigation();
  }
  
  @Test(priority = 3)
  public void BRM_301_Membership_Gift_Card_and_Coupons_Guest_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("MemberShipGft");
	  bnguest = new bnPageObjects.BNCompleteGuestCheckoutStories(driver,sheet);
	  
	  bnguest.guestCheckooutCouponVerification(false);
	  bnguest.guestCheckooutGiftCardFields();
	  bnguest.guestCheckooutCouponFieldVerification();
	  bnguest.guestCheckooutGiftCardImage();
	  bnguest.guestCheckooutCouponFieldPresent(); 
	  bnguest.guestCheckooutGiftCardFieldEmptyValidation();
	  bnguest.guestCheckooutGiftCardFieldEmptyHighlight();
	  bnguest.guestCheckooutGiftCreditCardFieldHighlight();
	  bnguest.guestCheckooutGiftCreditCardFieldClick();
	  bnguest.guestCheckooutGiftCreditCardFieldValidation();
	  bnguest.guestCheckooutGiftCreditCardFieldSpecialCharValidation();
	  bnguest.guestCheckooutGiftCreditCardPinValidation();
	  bnguest.guestCheckooutGiftInvalidCreditCardValidation();
	  bnguest.guestCheckooutCouponField();
	  bnguest.guestCheckooutCouponFieldClick();
	  bnguest.guestCheckooutCouponEmptyValidation();
	  bnguest.guestCheckooutCouponInvalidValidation();
	  bnguest.guestCheckooutCouponInvalidValidation2();
	  bnguest.guestCheckooutBookFairField();
	  bnguest.guestCheckooutBookFairFieldClick();
	  bnguest.guestCheckooutBookFairEmptyValidation();
	  bnguest.guestCheckoutBookFairIDInvalidValidation();
	  bnguest.guestCheckoutBookFairIDInvalidValidation2();
  }
  
  @Test(priority = 4)
  public void BRM_298_Payment_Info_Guest_User() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Payment Info");
	  bnguest = new bnPageObjects.BNCompleteGuestCheckoutStories(driver,sheet);
	  
	  bnguest.payInfoGuestPaymentDefaultSelection();
	  bnguest.payInfoCCTabHighlight();
	  bnguest.payInfoGuestPaymentPageElements();
	  bnguest.payInfoCCBreadCrumHighlight();
	  bnguest.payInfoGuestCCElements();
	  bnguest.payInfoGuestCCEmptyValidation();
	  bnguest.payInfoCCInvalidCardValidation();
	  bnguest.payInfoGuestCCNumberLengthValidation();
	  bnguest.payInfoGuestCCNameLengthValidation();
	  bnguest.payInfoCCInvalidNameValidation();
	  bnguest.payInfoCCCsvLengthValidation();
	  bnguest.payInfoGuestCCMonthYearDefaultText();
	  bnguest.payInfoGuestCCardMonthList();
	  bnguest.payInfoGuestCCardYearList();
	  bnguest.payInfoCCiicon();
	  bnguest.payInfoCCShipsAddressInvalidEmailIdValidation();
	  bnguest.payInfoCCShipsAddressEmailId();
	  bnguest.payInfoCCShipsameasdefault();
	  bnguest.payInfoCCShipsAddressOverlay();
	  bnguest.payInfoCCShipAddressDetails();
	  bnguest.payInfoCCCouponConatainer();
	  //pay.payInfoCCCouponContainerSectionDetails();
	  bnguest.payInfoCCGiftCardConatainer();
	  bnguest.payInfoCCGiftCardLengthValidationandDefaultText();
	  bnguest.payInfoCCGiftCardInvalidValidation();
	  bnguest.payInfoCCGiftCardClose();
	  bnguest.payInfoCCGiftCouponCodeFields();
	  bnguest.payInfoCCGiftCouponCodeInvalidValidation();
	  bnguest.payInfoCCGiftCouponCodeClose();
	  bnguest.payInfoCCBookfairContainer();
	  bnguest.payInfoCCBookfairValidation();
	  bnguest.payInfoCCBookfairLengthValidation();
	  bnguest.payInfoCCGiftBookFairClose();
	  bnguest.payInfoCCGiftSummaryContainerSubtotal();
	  bnguest.payInfoCCGiftSummaryContainerEstimatedShip();
	  bnguest.payInfoCCGiftSummaryContainerEstimatedTax();
	  bnguest.payInfoCCGiftSummaryContainerOrderTotal();
	  bnguest.payInfoCCPaypalLink();
	  bnguest.payInfoCCOtherPayTabHighlight();
	  bnguest.payInfoCCPaypalLinkNavigation();
	  bnguest.guestCheckoutCustomerServiceNavigation("BRM774");
	  bnguest.guestCheckoutTermsOfUseNavigation("BRM775");
	  bnguest.guestCheckoutTermsOfUseNavigation2("BRM778");
	  bnguest.guestCheckoutCopyrightsPageNavigation("BRM776");
	  bnguest.guestCheckoutPolicyPageNavigation("BRM777");
	  bnguest.guestCheckoutCustomerServiceNavigation("BRM965");
	  bnguest.guestCheckoutTermsOfUseNavigation("BRM966");
	  bnguest.guestCheckoutTermsOfUseNavigation2("BRM969");
	  bnguest.guestCheckoutPolicyPageNavigation("BRM968");
	  bnguest.guestCheckoutCopyrightsPageNavigation("BRM967");
	  bnguest.payInfoCCReviewOrderPageNavigation();
  }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }
  
	@BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
  
}
