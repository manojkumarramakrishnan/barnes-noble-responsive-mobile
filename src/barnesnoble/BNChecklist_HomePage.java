package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import bnPageObjects.BNChkListHPObj;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNChecklist_HomePage 
{
	WebDriver driver;
	HSSFSheet sheet;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNChkListHPObj chk;
	
  @Test(priority=1)
  public void BN_Checkist_HeaderHomePage() throws Exception 
  {
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Checklist");
	  chk = new BNChkListHPObj(driver,sheet);
	  chk.homepage();
	  chk.hpelementpresent();
	  chk.menuopencollapse();
	  chk.menusubmenuclick();
	  chk.menuscroll();
	  chk.catenavigation();
	  chk.menuComparision();
	  chk.bnlogo();
	  chk.promobannerpresent();
	  chk.promoredirect();
	  /*chk.menuopencollapse();
	  chk.menusubmenuclick();
	  chk.menuscroll();
	  chk.catenavigation();
	  chk.menuComparision();*/
	  chk.searchField();
	  chk.nosearchresult();
	  chk.searchClose();
	  chk.searchCancel();
	  chk.signInnav();
	  chk.rememberMeCheckboxstate();
	  chk.createAccountPge();
	  chk.SecureSignInalert();
	  chk.frgtpasswordlinkNavigation();
	  chk.forgotpasswordpageelements();
	  chk.forgotpasswordResetByEmailLink();
	  chk.forgotpasswordSecurityQuestionVerification();
	  chk.forgotpasswordResetSuccess();
	  chk.successfulLogin();
	  chk.minicartShoppingBagSignInViewCount();
	  chk.signOut();
	  chk.storeLocator();
	  chk.searchresultnavigation();
  }
  
  @BeforeClass
  public void beforeClass() throws Exception 
  {
	  Thread.sleep(5000);
	  driver = BNBasicfeature.setMobileView();  
  }

  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }

   @BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
	
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
	
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
	
	public static void EndTest()
	{
		extent.endTest(child);
	}
	
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
	
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
	
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
	
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
	
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
	
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
}
