package barnesnoble;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import bnPageObjects.BNMiniCartObj;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import commonFiles.BNBasicfeature;
import commonFiles.ExtentRptManager;

public class BNMiniCart 
{
	WebDriver driver;
	HSSFSheet sheet;
	boolean loggedIn = false;
	public static ExtentReports extent = ExtentRptManager.completeReport();
	public static ExtentTest parent, child;
	public static ArrayList<String> log = new ArrayList<>();
	BNMiniCartObj minicart;
	
	@BeforeClass
	public void beforeTest() throws Exception 
	{
		driver = BNBasicfeature.setMobileView();
	}
	
	@Test(priority = 1)
	public void BRM_62_Mini_Cart_Display() throws Exception 
	{
	  HSSFSheet sheet = BNBasicfeature.excelsetUp("Mini Cart");
	  minicart = new BNMiniCartObj(driver,sheet);
	  
	  minicart.miniCartShopBagPresent();
	  minicart.miniCartShoppingBagIcon();
	  minicart.miniCartShoppingBagCount();
	  minicart.mincartShoppingBagOpenClose();
	  minicart.miniCartShoppingBagSection();
	  minicart.mincartShoppingBagEmptyDetails();
	  minicart.mincartShoppingBagEmptyItemCount();
	  //minicart.miniCartOfferDetails();
	  //minicart.mincartShoppingBagFreeShipDetails();
	  minicart.minicartShoppingBagContinueBtnNavigation();
	  minicart.minicartShoppingBagSignInNavigation();
	  
	  minicart.minicartShoppingBagSignInViewCount();
	  //minicart.miniCartHTTPSCheck();
	  minicart.miniCartBagCount();
	  minicart.miniCartQtyField();
	  minicart.miniCartProductDetails();
	  minicart.miniCartAvailableImmediately();
	  minicart.miniCartTotalPriceContainer();
	  minicart.miniCartSubTotal();
	  minicart.miniCartNoUpdateOption();
	  minicart.miniCartNookNoUpdateOption();
	  minicart.miniCartCouponSection();
	  minicart.minicartShoppingBagSavedItemsList();
	  minicart.minicartShoppingBagSavedItemsDetails();
	  minicart.minicartShoppingBagSavedItemsDetailsLists();
	  minicart.miniCartSignedInUserCheckoutNavigation();
	  minicart.miniCartCheckoutPageShipDet();
	  minicart.miniCartHomeNavigationFromRegisteredCheckout();
	  minicart.miniCartSigoutBagCount();
	  
	  minicart.miniCartGuestCheckoutNavigation();
	  minicart.miniCartCheckoutAsGuestNavigation();
	  minicart.miniCartCheckoutAsGuestBackNavigation();
	  //minicart.miniCartLoadAndClose(); 
	  minicart.miniCartCheckoutPageNavigationFromSignIn();
	  minicart.miniCartHomeNavigationFromRegisteredCheckout();
	  minicart.miniCartInvalidCouponCode();
	  minicart.miniCartShippingMessageDetails();
	  minicart.miniCartAddedPrdtDetails();
	  minicart.miniCartProductAddedCount();
	  minicart.miniCartDifferentCategorySameProduct();
	  minicart.miniCartSaveforLater();
	  minicart.minicartShoppingBagSavedItemAddtoBag();
	  minicart.miniCartUpdateButton();
	  minicart.miniCartZeroQtyProductRemove();
	  minicart.miniCartUpdateQtyLimit();
	  minicart.miniCartShoppinBagCountOver100();
	  minicart.miniCartAvailableLimitCheck();
	  //minicart.miniCartUpdateQtyOnPurchaseMore(false);
	  minicart.miniCartEmptyBagSignedInUserBtns();
	  minicart.signOut();
	  //minicart.miniCartValidCouponCodeApply(); ==> Yet to implement
		 
	  }
	
	@AfterClass
	public void afterClass() 
	{
	  driver.close();
	}
	  
	@BeforeMethod
	public static void beforeMethod(Method name)
	{
		parent = extent.startTest(name.getName().replace("_", " "));
	}
		
	@AfterMethod
	public static void afterMethod(Method name)
	{
		extent.endTest(parent);
		extent.flush();
	}
		
	public static void ChildCreation(String name)
	{
		child = extent.startTest(name);
		parent.appendChild(child);
	}
		
	public static void EndTest()
	{
		extent.endTest(child);
	}
		
	public static void Createlog(String val)
	{
		child.log(LogStatus.INFO, val);
	}
		
	public static void Pass(String val)
	{
		child.log(LogStatus.PASS, val);
		EndTest();
	}
		
	public static void Skip(String val)
	{
		child.log(LogStatus.SKIP, val);
		EndTest();
	}
		
	public static void Pass(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.PASS, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
		
	public static void Fail(String val)
	{
		child.log(LogStatus.FAIL, val);
		EndTest();
	}
		
	public static void Fail(String val,ArrayList<String> logval)
	{
		child.log(LogStatus.FAIL, val);
		for(String lval:logval)
		{
			Createlog(lval);
		}
		logval.removeAll(logval);
		EndTest();
	}
		
	public static void Exception(String val)
	{
		child.log(LogStatus.FATAL,val);
		EndTest();
	}
}
