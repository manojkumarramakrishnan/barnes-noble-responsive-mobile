package bnStreamCalls;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import barnesnoble.BNSprintStories;
import bnConfig.BNConstants;

public class BNPromoMessageCall extends BNSprintStories
{
	public static ArrayList<String> promoCall(ArrayList<String> log) throws Exception
	{
		ArrayList<String> promoarrayList = new ArrayList<>();
		try 
		{
			URL url = new URL(BNConstants.topCampaignURL);
			/*String user = "bnuser";
			String pass = "bn123";
			//String pass = "bn321";
			String authStr = user + ":" + pass;*/
			try
			{
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				/*byte[] authEncBytes = Base64.encodeBase64(authStr.getBytes());
				String utf8 = new String(authEncBytes,"UTF-8");
				conn.setRequestProperty("Authorization", "Basic " + utf8);*/
				if(conn.getResponseCode()==200)
				{
					InputStream is = conn.getInputStream();
					@SuppressWarnings("deprecation")
					String jsonData = IOUtils.toString(is);
					JSONObject properties = new JSONObject(jsonData);
					JSONArray promomsg = properties.getJSONObject("properties").getJSONObject("buyinfo").getJSONArray("promomessages");
					for(int i = 0; i<promomsg.length();i++)
					{
						String identifier = promomsg.getJSONObject(i).getString("identifier");
						if(identifier.equals("upsellPromo"))
						{
							String promomessage = promomsg.getJSONObject(i).getString("name");
							//String response = promomessage;
							String response = promomessage.replaceAll("[^\\x00-\\x7F]","").replaceAll("\\s+", "");
							promoarrayList.add(response);
						}
						else
						{
							continue;
						}
					}
					log.add("The JSON values from the Category Call is successfully fetched.");	
				}
				else
				{
					Fail("The User is not authorized to access the URL.The returnes error code was : " + conn.getResponseCode());
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong with the URL.Please Check the response." + e.getMessage());
			}
		} 
		catch (MalformedURLException e) 
		{
			System.out.println(" Error in getting the Promo Message to compare." + e.getMessage());
			Exception(" Error in getting the Promo Message to compare." + e.getMessage());
		}
		return promoarrayList;
	}
}
