package bnStreamCalls;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import barnesnoble.BNSprintStories;
import bnConfig.BNConstants;

public class BNTopCategoryCampaignCall extends BNSprintStories 
{
	static HttpURLConnection request;
	public static List<List<String>> menudetails(ArrayList<String> log)
	{
		ArrayList<String> menuName = new ArrayList<>();
		ArrayList<String> categoryName = new ArrayList<>();
		ArrayList<String> subCategoryName = new ArrayList<>();
		try 
		{
			URL url = new URL(BNConstants.topCampaignURL);
			try
			{
				HttpURLConnection request = (HttpURLConnection) url.openConnection();
				if(request.getResponseCode()==200)
				{
					String baseURl = BNConstants.topCampaignURL;
					@SuppressWarnings("deprecation")
					String jsonData = IOUtils.toString(new URL(baseURl));
					JSONArray children = new JSONObject(jsonData).getJSONObject("children").getJSONArray("categories");
					for(int i=0; i<children.length();i++)
					{
						String mName = children.getJSONObject(i).getString("name");
						menuName.add(mName);
						/*JSONObject prop = mName.getJSONObject(i).getJSONObject("properties").getJSONObject("state").getJSONArray("additionallinks").getJSONObject(0);
						String shpall = prop.getString("name");
						String resshpall = WordUtils.capitalizeFully(shpall);
						categoryName.add(resshpall.concat(" "+mName));*/
						JSONArray mcategories = children.getJSONObject(i).getJSONObject("children").getJSONArray("categories");
						for(int j=0;j<mcategories.length();j++)
						{
							String mcatename = mcategories.getJSONObject(j).getString("name");
							categoryName.add(mcatename);
							JSONArray scategories = mcategories.getJSONObject(j).getJSONObject("children").getJSONArray("categories");
							for(int k=0;k<scategories.length();k++)
							{
								String subcatename = scategories.getJSONObject(k).getString("name");
								String response = new String(subcatename.getBytes("Windows-1252"),"UTF-8");
								//String response = subcatename.replaceAll("[^\\x00-\\x7F]","");
								if(response.equals("My NOOK Library"))
								{
									continue;
								}
								else
								{
									subCategoryName.add(response);	
								}
							}
						}
					}
					log.add("The JSON values from the Category Call is successfully fetched.");
				}
				else
				{
					Fail("There is something wrong with the URL.Please Check the response." + request.getResponseMessage());
					System.out.println(request.getResponseMessage());
				}
			}
			catch (Exception e)
			{
				Exception("There is something wrong with the URL.Please Check." + e.getMessage());
			}
		} 
		catch (Exception e) 
		{
			Exception("There is something wrong with the URL.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
		
		List<List<String>> result = new ArrayList<List<String>>(); 
		result.add(menuName);
		result.add(categoryName);
		result.add(subCategoryName);
		return result;
	}
	
  /* public static List<List<String>> menudetails(ArrayList<String> log)
	{
		ArrayList<String> menuName = new ArrayList<>();
		ArrayList<String> categoryName = new ArrayList<>();
		ArrayList<String> subCategoryName = new ArrayList<>();
		String jsonData = "";
		JSONArray children,mcategories,scategories = null; 
		request = null;
		try 
		{
			//URL url = new URL(BNConstants.topCampaignURL);
			if(request==null)
			{
				getauth();
			}
			try
			{
				if(request.getResponseCode()==200)
				{
					//String baseURl = BNConstants.topCampaignURL;
					InputStream is = request.getInputStream();
					jsonData = IOUtils.toString(is);
					children = new JSONObject(jsonData).getJSONObject("children").getJSONArray("categories");
					for(int i=0; i<children.length();i++)
					{
						String mName = children.getJSONObject(i).getString("name");
						menuName.add(mName);
						/*JSONObject prop = mName.getJSONObject(i).getJSONObject("properties").getJSONObject("state").getJSONArray("additionallinks").getJSONObject(0);
						String shpall = prop.getString("name");
						String resshpall = WordUtils.capitalizeFully(shpall);
						categoryName.add(resshpall.concat(" "+mName));*/
						/*mcategories = children.getJSONObject(i).getJSONObject("children").getJSONArray("categories");
						for(int j=0;j<mcategories.length();j++)
						{
							String mcatename = mcategories.getJSONObject(j).getString("name");
							categoryName.add(mcatename);
							scategories = mcategories.getJSONObject(j).getJSONObject("children").getJSONArray("categories");
							for(int k=0;k<scategories.length();k++)
							{
								String subcatename = scategories.getJSONObject(k).getString("name");
								String response = new String(subcatename.getBytes("Windows-1252"),"UTF-8");
								//String response = subcatename.replaceAll("[^\\x00-\\x7F]","");
								if(response.equals("My NOOK Library"))
								{
									continue;
								}
								else
								{
									subCategoryName.add(response);	
								}
							}
						}
					}
					log.add("The JSON values from the Category Call is successfully fetched.");
				}
				else
				{
					Exception("There is something wrong with the URL.Please Check the response." + request.getResponseMessage());
				}
			}
			catch (Exception e)
			{
				Exception("There is something wrong with the URL.Please Check the response." + e.getMessage());
			}
		} 
		catch (Exception e) 
		{
			Exception("There is something wrong with the URL.Please Check." + e.getMessage());
		}
		
		List<List<String>> result = new ArrayList<List<String>>(); 
		result.add(menuName);
		result.add(categoryName);
		result.add(subCategoryName);
		return result;
	}*/
	
	@SuppressWarnings("deprecation")
	public static String link(ArrayList<String> log,int sel1,int sel2,int sel3)
	{
		String cateLink="",jsonData = "";
		request = null;
		try 
		{
			//URL url = new URL(BNConstants.topCampaignURL);
			/*if(request==null)
			{
				getauth();
			}*/
			try
			{
				/*if(request.getResponseCode()==200)
				{
					*///String baseURl = BNConstants.topCampaignURL;
					String baseURl = BNConstants.topCampaignURL;
					jsonData = IOUtils.toString(new URL(baseURl));
					/*InputStream is = request.getInputStream();
					jsonData = IOUtils.toString(is);*/
					JSONArray children = new JSONObject(jsonData).getJSONObject("children").getJSONArray("categories");
					JSONArray categories = children.getJSONObject(sel1).getJSONObject("children").getJSONArray("categories");
					JSONArray subcategories = categories.getJSONObject(sel2-1).getJSONObject("children").getJSONArray("categories");
					cateLink = subcategories.getJSONObject(sel3-1).getString("link");
					log.add("The JSON values from the Category Call is successfully fetched.");
				/*}
				else
				{
					Exception("There is something wrong with the URL.Please Check the response." + request.getResponseMessage());
				}*/
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				Exception("There is something wrong with the URL.Please Check the response." + e.getMessage());
			}
		} 
		catch (Exception e) 
		{
			Exception("There is something wrong with the URL.Please Check." + e.getMessage());
		}
		
		return cateLink;
	}
	
	/*public static void getauth() throws Exception
	{
		URL url = new URL(BNConstants.topCampaignURL);
		String user = "bnuser";
		String pass = "bn123";
		//String pass = "bn321";
		String authStr = user + ":" + pass;
		request = (HttpURLConnection) url.openConnection();
		byte[] authEncBytes = Base64.encodeBase64(authStr.getBytes());
		String utf8 = new String(authEncBytes,"UTF-8");
		request.setRequestProperty("Authorization", "Basic " + utf8);
	}*/
}
