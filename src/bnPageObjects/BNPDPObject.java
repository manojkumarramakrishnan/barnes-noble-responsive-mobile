package bnPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNPDP;
import bnConfig.BNConstants;
import bnConfig.BNXpaths;

public class BNPDPObject extends BNPDP implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	public boolean bkval=false;
	boolean lodMin = false;
	boolean ptmagzine = false;
	boolean musicPdp = false;
	boolean moviePdp = false;
	boolean textbookPdp = false;
	boolean textbookPdpBuytype = false;
	boolean cbPdp = false;
	boolean bookpromoBadge = false;
	boolean nookPdp = false;
	boolean giftPdp = false;
	boolean cardAdded = false;
	boolean egiftPdp = false;
	boolean nookMagazinePdp = false;
	boolean nookMagazinepopUp = false;
	boolean bookPdp = false;
	boolean toysPdp = false;
	public int val;
	static int bgCount = 0, currVal = 0;
	static int accSel = 0;
	static String accName = "";
	
	public BNPDPObject(WebDriver driver,HSSFSheet sheet) 
	{
		this.driver = driver;
		this.sheet = sheet;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, BNXpaths.timeout);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+plpSearchPageFilterContainerr+"")
	WebElement plpSearchPageFilterContainer;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy (xpath = ""+pdpPageAddToWishListContainerr+"")
	WebElement pdpPageAddToWishListContainer;
	
	@FindBy (xpath = ""+pdpPageImageCarousell+"")
	WebElement pdpPageImageCarousel;
	
	@FindBy(xpath = ""+productIdd+"")
	WebElement productId;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+pdpPageProductNamee+"")
	WebElement pdpPageProductName;
	
	@FindBy(xpath = ""+pdpPageByy+"")
	WebElement pdpPageBy;
	
	@FindBy(xpath = ""+pdpPageProductNameContainerr+"")
	WebElement pdpPageProductNameContainer;
	
	@FindBy(xpath = ""+pdpPageDropDownContainerr+"")
	WebElement pdpPageDropDownContainer;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualLabelPerIssuee+"")
	WebElement pdpPageMagazineAnnualLabelPerIssue;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualLabelPerYearr+"")
	WebElement pdpPageMagazineAnnualLabelPerYear;
	
	@FindBy(xpath = ""+pdpPageATBBtnn+"")
	WebElement pdpPageATBBtn;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+bagCountt+"")
	WebElement bagCount;
	
	@FindBy(xpath = ""+pdpPageMiniCartflyy+"")
	WebElement pdpPageMiniCartfly;
	
	@FindBy(xpath = ""+pdpPageATBErrorr+"")
	WebElement pdpPageATBError;
	
	@FindBy(xpath = ""+productAddedSuccessIdd+"")
	WebElement productAddedSuccessId;
	
	@FindBy(xpath = ""+pdpPageGiftCardValueFieldd+"")
	WebElement pdpPageGiftCardValueField;
	
	@FindBy(xpath = ""+pdpPageGiftCardNoOfCardsDDD+"")
	WebElement pdpPageGiftCardNoOfCardsDD;
	
	@FindBy(xpath = ""+pdpPageGiftCardNoOfCardsDDDefaultTextt+"")
	WebElement pdpPageGiftCardNoOfCardsDDDefaultText;
	
	@FindBy(xpath = ""+pdpPageGiftCardToo+"")
	WebElement pdpPageGiftCardTo;
	
	@FindBy(xpath = ""+pdpPageGiftCardToDefaultTextt+"")
	WebElement pdpPageGiftCardToDefaultText;
	
	@FindBy(xpath = ""+pdpPageGiftCardFromm+"")
	WebElement pdpPageGiftCardFrom;
	
	@FindBy(xpath = ""+pdpPageGiftCardFromDefaultTextt+"")
	WebElement pdpPageGiftCardFromDefaultText;
	
	@FindBy(xpath = ""+pdpPageGiftCardMsgg+"")
	WebElement pdpPageGiftCardMsg;
	
	@FindBy(xpath = ""+pdpPageGiftCardMsgDefaultTextt+"")
	WebElement pdpPageGiftCardMsgDefaultText;
	
	@FindBy(xpath = ""+pdpPageGiftCardTotalPricee+"")
	WebElement pdpPageGiftCardTotalPrice;
	
	@FindBy(xpath = ""+pdpPageGiftCardLabell+"")
	WebElement pdpPageGiftCardLabel;
	
	@FindBy(xpath = ""+pdpPageGiftCardShippingDetailss+"")
	WebElement pdpPageGiftCardShippingDetails;
	
	@FindBy(xpath = ""+pdpPageCustomerBoughtSectionn+"")
	WebElement pdpPageCustomerBoughtSection;
	
	@FindBy(xpath = ""+pdpPageCustomerBoughtCaptionn+"")
	WebElement pdpPageCustomerBoughtCaption;
	
	@FindBy(xpath = ""+pdpPageCustomerBoughtSectionn1+"")
	WebElement pdpPageCustomerBoughtSection1;
	
	@FindBy(xpath = ""+pdpPageGiftCardErrorr+"")
	WebElement pdpPageGiftCardError;
	
	@FindBy(xpath = ""+pdpPageGiftCardRemainingCharr+"")
	WebElement pdpPageGiftCardRemainingChar;
	
	@FindBy(xpath = ""+miniCartLoadingGaugee+"")
	WebElement miniCartLoadingGauge;
	
	@FindBy(xpath=""+mcContinueShoppingBtnn+"")
	WebElement mcContinueShoppingBtn;
	
	@FindBy(xpath = ""+miniCartShoppingBagMaskIdd+"")
	WebElement miniCartShoppingBagMaskId;
	
	@FindBy(xpath = ""+checkoutPageFramee+"") 
	WebElement checkoutPageFrame;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+checkoutAsGuestbtnn+"")
	WebElement checkoutAsGuestbtn;
	
	@FindBy(xpath = ""+pdpPageDropDownProductTypee+"")
	WebElement pdpPageDropDownProductType;
	
	@FindBy(xpath = ""+pdpPageSignInInstantPurchasee+"")
	WebElement pdpPageSignInInstantPurchase;
	
	@FindBy(xpath = ""+pdpPageProductReadMoree+"")
	WebElement pdpPageProductReadMore;
	
	@FindBy(xpath = ""+pdpPageExpandedAccordionn+"")
	WebElement pdpPageExpandedAccordion;
	
	@FindBy(xpath = ""+searchPageSearchTermm+"")
	WebElement searchPageSearchTerm;
	
	@FindBy(xpath = ""+pdpPageWantitTodayy+"")
	WebElement pdpPageWantitToday;
	
	@FindBy(xpath = ""+pdpPagePickInstoreContainerr+"")
	WebElement pdpPagePickInstoreContainer;
	
	@FindBy(xpath = ""+pdpPagePickInstorePickUpTitlee+"")
	WebElement pdpPagePickInstorePickUpTitle;
	
	@FindBy(xpath = ""+pdpPagePickInstorePickUpDescc+"")
	WebElement pdpPagePickInstorePickUpDesc;
	
	@FindBy(xpath = ""+pdpPageNookDevicePickUpZipCodee+"")
	WebElement pdpPageNookDevicePickUpZipCode;
	
	@FindBy(xpath = ""+pdpPagePickUpFindStoreBtnn+"")
	WebElement pdpPagePickUpFindStoreBtn;
	
	@FindBy(xpath = ""+pdpPagePickInstorePopUpClosee+"")
	WebElement pdpPagePickInstorePopUpClose;
	
	@FindBy(xpath = ""+pdpPageProductDropDownn+"")
	WebElement pdpPageProductDropDown;
	
	@FindBy(xpath = ""+pdpPageTrialMsgg+"")
	WebElement pdpPageTrialMsg;
	
	@FindBy(xpath = ""+pdpPageTrialMsgLearnMoree+"")
	WebElement pdpPageTrialMsgLearnMore;
	
	@FindBy(xpath = ""+pdpPagePopUpp+"")
	WebElement pdpPagePopUp;
	
	@FindBy(xpath = ""+pdpPagePopUpClosee+"")
	WebElement pdpPagePopUpClose;
	
	@FindBy(xpath = ""+pdpPageNookToolTipp+"")
	WebElement pdpPageNookToolTip;
	
	@FindBy(xpath = ""+pdpPageNookPopUpp+"")
	WebElement pdpPageNookPopUp;
	
	@FindBy(xpath = ""+pdpPageNookPopUpClosee+"")
	WebElement pdpPageNookPopUpClose;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualIssuePricee+"")
	WebElement pdpPageMagazineAnnualIssuePrice;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualPricee+"")
	WebElement pdpPageMagazineAnnualPrice;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualAddToBagg+"")
	WebElement pdpPageMagazineAnnualAddToBag;
	
	@FindBy(xpath = ""+pdpPageMagazineAnnualInstantPurchasee+"")
	WebElement pdpPageMagazineAnnualInstantPurchase;
	
	@FindBy(xpath = ""+pdpPageMagazineMonthlyIssuePricee+"")
	WebElement pdpPageMagazineMonthlyIssuePrice;
	
	@FindBy(xpath = ""+pdpPageMagazineMonthlyLabelPerIssuee+"")
	WebElement pdpPageMagazineMonthlyLabelPerIssue;
	
	@FindBy(xpath = ""+pdpPageMagazineMonthlyPricee+"")
	WebElement pdpPageMagazineMonthlyPrice;
	
	@FindBy(xpath = ""+pdpPageMagazineMonthlyLabelPerYearr+"")
	WebElement pdpPageMagazineMonthlyLabelPerYear;
	
	@FindBy(xpath = ""+pdpPageMagazineCurrentIssuePricee+"")
	WebElement pdpPageMagazineCurrentIssuePrice;
	
	@FindBy(xpath = ""+pdpPageMagazineCurrentIssuePriceLabell+"")
	WebElement pdpPageMagazineCurrentIssuePriceLabel;
	
	@FindBy(xpath = ""+pdpPageMagazineMonthlyAddToBagg+"")
	WebElement pdpPageMagazineMonthlyAddToBag;
	
	@FindBy(xpath = ""+signInpageframee+"")
	WebElement signInpageframe;
	
	@FindBy(xpath = ""+pdpPageProductShortDescReadMoree+"")
	WebElement pdpPageProductShortDescReadMore;
	
	@FindBy(xpath = ""+pdpPageOverviewContainerr+"")
	WebElement pdpPageOverviewContainer;
	
	@FindBy(xpath = ""+pdpPageBuyTypeDDD+"")
	WebElement pdpPageBuyTypeDD;
	
	@FindBy(xpath = ""+pdpPageRetDateContainerr+"")
	WebElement pdpPageRetDateContainer;
	
	@FindBy(xpath = ""+pdpPageRetDatee+"")
	WebElement pdpPageRetDate;
	
	@FindBy(xpath = ""+pdpPageRentalStepss+"")
	WebElement pdpPageRentalSteps;
	
	@FindBy(xpath = ""+pdpPageRentalFAQQ+"")
	WebElement pdpPageRentalFAQ;
	
	
	
	
	@FindBy(how = How.XPATH, using = ""+plpPageProductListss+"")
	List<WebElement> plpPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageSalePriceListt+"")
	List<WebElement> pdpPageSalePriceList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageActualPriceListt+"")
	List<WebElement> pdpPageActualPriceList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageActualSavePercentt+"")
	List<WebElement> pdpPageActualSavePercent;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerImageListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerImageList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerImageLinkListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerImageLinkList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerTitleLinkListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerTitleLinkList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerRatingListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerRatingList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageCustomerBoughtContainerRatingCountListt+"")
	List<WebElement> pdpPageCustomerBoughtContainerRatingCountList;
	
	@FindBy(how = How.XPATH, using = ""+mcContinueToCheckOutBtnn+"")
	List<WebElement> mcContinueToCheckOutBtn;
	
	@FindBy(how = How.XPATH, using = ""+miniCartAddedItemListss+"")
	List<WebElement> miniCartAddedItemLists;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductDDListt+"")
	List<WebElement> pdpPageProductDDList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageAccordionListt+"")
	List<WebElement> pdpPageAccordionList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageAccordionLinkk+"")
	List<WebElement> pdpPageAccordionLink;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageAccExpp+"")
	List<WebElement> pdpPageAccExp;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductReadLesss+"")
	List<WebElement> pdpPageProductReadLess;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductRelatedSubjectListt+"")
	List<WebElement> pdpPageProductRelatedSubjectList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductPromoMsgListt+"")
	List<WebElement> pdpPageProductPromoMsgList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductPromoLinkListt+"")
	List<WebElement> pdpPageProductPromoLinkList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageMovieDirectorDetailss+"")
	List<WebElement> pdpPageMovieDirectorDetails;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageMovieCastDetailss+"")
	List<WebElement> pdpPageMovieCastDetails;
	
	@FindBy(how = How.XPATH, using = ""+pdpPagePickUpInStoreLabell+"")
	List<WebElement> pdpPagePickUpInStoreLabel;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductTypess+"")
	List<WebElement> pdpPageProductTypes;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageBuyTypeListt+"")
	List<WebElement> pdpPageBuyTypeList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPagePopUpDetailss+"")
	List<WebElement> pdpPagePopUpDetails;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageMagazineAnnualDeliveredTextt+"")
	List<WebElement> pdpPageMagazineAnnualDeliveredText;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageMagazineAnnualMonthlySubscriberLinkTextt+"")
	List<WebElement> pdpPageMagazineAnnualMonthlySubscriberLinkText;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageISBNListt+"")
	List<WebElement> pdpPageISBNList;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageATBBtnss+"")
	List<WebElement> pdpPageATBBtns;
	
	
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	// Gift Card Clear
	public void giftfieldClear()
	{
		try
		{
			if((BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))&&(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))&&(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))&&(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg)))
			{
				BNBasicfeature.scrollup(pdpPageGiftCardValueField, driver);
				pdpPageGiftCardValueField.clear();
				pdpPageGiftCardTo.clear();
				BNBasicfeature.scrollup(pdpPageGiftCardFrom, driver);
				pdpPageGiftCardFrom.clear();
				pdpPageGiftCardMsg.clear();
				BNBasicfeature.scrollup(pdpPageGiftCardValueField, driver);
			}
			else
			{
				Fail( "The Gift Fields are missing.");
				System.out.println("Field is missing to clear in gift card fields.");
			}
		}
		catch(Exception e)
		{
			System.out.println("CLEAR ALL EXCEPTION");
		}
	}
		
	//Mini Cart Get Identifier
	public String getprdtIdentifier(int sel)
	{
		WebElement prdtIdentifier = driver.findElement(By.xpath("(//*[@class='sk_mobShoppingBagContainer']//*[contains(@id,'PdtWrapper')]//*[contains(@class,'PdtItemContainer')])["+(sel+1)+"]"));
		String prdtId = prdtIdentifier.getAttribute("identifier");
		//System.out.println(prdtId);
		return prdtId;
	}
	
	// Get Bag Count Value
	public boolean getBagCount()
	{
		boolean bagCountOk = false;
		int count = 1;
		int refCount = 1;
		do
		{
			for( int i = 0; i<100; i++)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(bagCount));
					wait.until(ExpectedConditions.elementToBeClickable(bagCount));
					//String bagCnt = bagCount.getText();
					String bagCnt = bagCount.getText().replace("+", "");
					if(bagCnt.isEmpty())
					{
						count++;
						continue;
					}
					else
					{
						bgCount = Integer.parseInt(bagCnt);
						bagCountOk = true;
						break;
					}
				}
				catch(Exception e)
				{
					count++;
				}
			}
			if(count>=100)
			{
				driver.navigate().refresh();
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(bagCount));
				count = 1;
				refCount++;
			}
		}while((bagCountOk==false)&&(refCount<=3));
		return bagCountOk;
	}
	
	// Load the Mini Cart Feature
	public boolean loadMiniCart() throws Exception
	{
		boolean pgok = false;
		int count = 1;
		do
		{
			Thread.sleep(100);
			wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
			Thread.sleep(900);
			Actions act = new Actions(driver);
			act.moveToElement(bagIcon).click().build().perform();
			//jsclick(bagIcon);
			//Thread.sleep(1000);
			try
			{
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block;"));
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
				//wait.until(ExpectedConditions.visibilityOf(mcContinueToCheckOutBtn));
				Thread.sleep(1000);
				if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(0)))
				{
					pgok = true;
					break;
				}
				else if((BNBasicfeature.isElementPresent(mcContinueShoppingBtn)))
				{
					jsclick(miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					pgok = false;
					count++;
					continue;
				}
				else
				{
					jsclick(miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					pgok = false;
					count++;
					continue;
				}
			}
			catch(Exception e)
			{
				jsclick(miniCartShoppingBagMaskId);
				driver.navigate().refresh();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				count++;
				continue;
			}
		}
		while(pgok==false||count<10);
		return pgok;
	}
	
	// Add to Bag
	public boolean clickaddtoBag(int prevVal, WebElement ATBBtn) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			prdadded = false;
			int count = 1;
			//Thread.sleep(1000);
			do
			{
				WebDriverWait newWait = new WebDriverWait(driver, 5);
				try
				{
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						newWait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(500);
						currVal = Integer.parseInt(bagCount.getText());
						if(currVal>prevVal)
						{
							prdadded = true;
							//break;
						}
						
						jsclick(ATBBtn);
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
						//BNBasicfeature.scrollup(promoCarousel, driver);
						String error = pdpPageATBError.getAttribute("style");
						if(error.contains("block"))
						{
							driver.navigate().refresh();
							wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOf(bagCount)
									)
								);
							wait.until(ExpectedConditions.visibilityOf(header));
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
						}
						
						if(BNBasicfeature.isElementPresent(productAddedSuccessId))
						{
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
							else
							{
								Thread.sleep(1000);
								jsclick(ATBBtn);
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
								count++;
								continue;
							}
						}
						else
						{
							count++;
							jsclick(ATBBtn);
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
							continue;
						}
					}
					else
					{
						continue;
					}
				}
				catch(Exception e)
				{
					driver.navigate().refresh();
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(bagCount)
							)
						);
					wait.until(ExpectedConditions.visibilityOf(header));
					count++;
					boolean bgCnt1 = getBagCount();
					if(bgCnt1==true)
					{
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(100);
						currVal = Integer.parseInt(bagCount.getText());
					}
					else
					{
						continue;
					}
					
					if(currVal>prevVal)
					{
						prdadded = true;
						//break;
					}
				}
			}while((prdadded==false)&&(count <= 5));
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}
	
	
	// GET ACCORDIAN LIST
	public boolean getAccordian(String getAccordionName)
	{
		boolean accordianFound = false;
		accName = "";
		try
		{
			if((BNBasicfeature.isListElementPresent(pdpPageAccordionList))&&(BNBasicfeature.isListElementPresent(pdpPageAccordionLink)))
			{
				log.add("The Accordion list is displayed in the PDP page for the selected product.");
				log.add("The Expected Accordion : " + getAccordionName);
				for(accSel = 1; accSel<=pdpPageAccordionList.size();accSel++)
				{
					accName = pdpPageAccordionList.get(accSel-1).getText();
					if(accName.contains(getAccordionName))
					{
						log.add("The Accordion " + accName + " is found");
						accordianFound = true;
						break;
					}
					else
					{
						accordianFound = false;
						continue;
					}
				}
			}
			else
			{
				Fail( "The Accordian list is not found.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" ISSUE IN GETTING ACCORDIAN LIST.");
			Exception("ISSUE IN GETTING ACCORDIAN LIST.");
		}
		return accordianFound;
	}
	
	/* Search EAN Numbers */
	public boolean searchEanNumbers(String ean) throws Exception
	{
		boolean pdpPageloaded = false;
		try
		{
			do
			{
				pdpPageloaded = false;
				try
				{
					wait.until(ExpectedConditions.visibilityOf(header));
					//BNBasicfeature.scrollup(header, driver);
					if(BNBasicfeature.isElementPresent(searchIcon))
					{
						Actions act = new Actions(driver);
						jsclick(searchIcon);
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
						act.moveToElement(searchContainer).click().sendKeys(ean).sendKeys(Keys.ENTER).build().perform();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(homepageHeader));
						//driver.manage().deleteAllCookies();
						//driver.navigate().refresh();
						if(BNBasicfeature.isElementPresent(plpNoProductsFound))
						{
							pdpPageloaded = false;
							break;
						}
						else
						{
							wait.until(ExpectedConditions.visibilityOf(productId));
							wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
							if(BNBasicfeature.isElementPresent(pdpPageProductName))
							{
								pdpPageloaded = true;
							}
							else if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
							{
								pdpPageloaded = true;	
							}
							else if(BNBasicfeature.isElementPresent(plpNoProductsFound))
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
							else
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
						}
					}
					else
					{
						System.out.println("The Search Icon is not found.Please Check.");
					}
				}
				catch(Exception e)
				{
					System.out.println("In Catch Block 1");
					System.out.println(e.getMessage());
					//driver.navigate().refresh();
				}
			}while(pdpPageloaded==false);
		}
		catch (Exception e) 
		{
			System.out.println("In Outer Catch Block 2");
			System.out.println(e.getMessage());
		}
		return pdpPageloaded;
	}
		
	/* BRM - 1444 Verify that if there is only one primary image for the product in the PDP page,the carousel icon should not be displayed .*/
	/* BRM - 1436 Verify that while selecting the product image/product title from the product list page,the corresponding PDP page should be displayed.*/
	public void PDPPageNavigationPLPSingleImage()
	{
		ChildCreation("BRM - 1444 Verify that if there is only one primary image for the product in the PDP page,the carousel icon should not be displayed .");
		String selprdtname = "",selPrdtid="",currId="";
		try
		{
			//int i = 0, j = 1, k = 11; 
			int i = 7, j = 4, k = 1, sel = 0;
			//Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			//wait.until(ExpectedConditions.visibilityOf(prdtBagCount));
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				Pass("The Menu is successfully clicked and the Submenus are displayed.");
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				if(BNBasicfeature.isElementPresent(menuLists))
				{
					WebElement mainMenu = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItem']//*[@id='sk_mobCategoryItemCont_id_"+i+"_0']"));
					jsclick(mainMenu);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]")));
					WebElement cName = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]"));
					jsclick(cName);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]")));
					WebElement subcName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]"));
					BNBasicfeature.scrolldown(subcName, driver);
					Thread.sleep(1000);
					jsclick(subcName);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(homepageHeader),
							ExpectedConditions.visibilityOf(plpSearchPageFilterContainer)
							)
					);
					
					Thread.sleep(500);
					
					boolean atbfound = false, validrNo = false;
					if(BNBasicfeature.isListElementPresent(plpPageProductLists))
					{
						for(int l = 1; l<=plpPageProductLists.size();l++)
						{
							Thread.sleep(100);
							int count=1;
							atbfound = false;
							Random r = new Random();
							ArrayList<Integer> rNo = new ArrayList<>();
							do
							{
								do
								{
									validrNo = false;
									sel = r.nextInt(plpPageProductLists.size());
									if(sel<1)
									{
										sel = 1;
									}
									if(rNo.contains(sel))
									{
										validrNo = false;
									}
									else
									{
										rNo.add(sel);
										validrNo = true;
									}
									
								}while(validrNo==false);
								
								BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
								WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
								selprdtname = prdtlist.getText();
								log.add("The Selected Product Name is : " + selprdtname);
								selPrdtid = prdtlist.getAttribute("identifier").toString();
								log.add("Selected Product ID : " + selPrdtid);
								try
								{
									atbfound = false;
									jsclick(prdtlist);
									WebDriverWait w1 = new WebDriverWait(driver, 10);
									w1.until(ExpectedConditions.and(
											ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer),
											ExpectedConditions.visibilityOf(productId)
											)
									);
									wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
									if(BNBasicfeature.isElementPresent(pdpPageImageCarousel))
									{
										driver.navigate().back();
										count++;
										continue;
									}
									else
									{
										atbfound = true;
										break;
									}
								}
								catch(Exception e)
								{
									driver.navigate().back();
									count++;
									continue;
								}
							} while((atbfound==true)&&(count<10));
						}
						
						if(BNBasicfeature.isElementPresent(pdpPageImageCarousel))
						{
							Fail("The Image Carousel is available for the Selected Product.");
						}
						else
						{
							Pass("The Image Carousel is not available for the Selected Product.");
						}
					}
					else
					{
						Fail("The Product List page is empty.");
					}
				}
				else
				{
					Fail("The Menu list is not displayed / visible.");
				}
			}
			else
			{
				Fail("The pancake menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1444 Issue ." + e.getMessage());
			Exception(" BRM - 1444 Issue." + e.getMessage());
		}
		
		/* BRM - 1436 Verify that while selecting the product image/product title from the product list page,the corresponding PDP page should be displayed. */
		ChildCreation("BRM - 1436 Verify that while selecting the product image/product title from the product list page,the corresponding PDP page should be displayed.");
		try
		{
			if(BNBasicfeature.isElementPresent(productId))
			{
				currId = productId.getAttribute("identifier").toString();
			}
			else
			{
				Fail( "The prodcut id is not displayed.");
			}
			
			if(currId.equals(selPrdtid))
			{
				Pass("The User is Navigated to the Correct PDP Page.", log);
			}
			else
			{
				Fail("The User is not navigated to the Correct PDP Page.", log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1436 Issue ." + e.getMessage());
			Exception(" BRM - 1436 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2180 Verify that Format drop down is displayed below the Add to wishlist in Print Magazine PDP page*/
	public void pdpPagePrintMagazineFormatOption()
	{
		ChildCreation(" BRM - 2180 Verify that Format drop down is displayed below the Add to wishlist in Print Magazine PDP page.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			/*String cellVal = BNBasicfeature.getExcelVal("BRM2180", sheet, 3);
			searchProduct(sheet, false, "BRM2180");
			Thread.sleep(1000);
			mvfound = selectProduct("Time");*/
			
			String ean = BNBasicfeature.getExcelVal("BRM2180", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isElementPresent(pdpPageProductName))
					 {
						 if(pdpPageProductName.getText().contains("Subscription"))
						 {
							 ptmagzine = true;
							 break;
						 }
						 else
						 {
							 ptmagzine = false;
							 continue;
						 }
					 }
					 else
					 {
						 ptmagzine = false;
						 continue;
					 }
				 }
			}
			
			if(ptmagzine == true)
			{
				if(BNBasicfeature.isElementPresent(pdpPageDropDownContainer))
				{
					Pass("The Buying Type Drop Down is displayed.");
				}
				else
				{
					Fail("The Buying Type drop down is not displayed.");
				}
			}
			else
			{
				Fail("The user is not navigated to the print magazine page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2180 Issue ." + e.getMessage());
			Exception(" BRM - 2180 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2181 Verify that Print Magazine - Annual subscription, $ amount per issue and $ amount per year are displayed below the format drop down in Print Magazine PDP page.*/
	public void pdpPagePrintMagazineSubscriptionDetails()
	{
		ChildCreation(" BRM - 2181 Verify that Print Magazine - Annual subscription, $ amount per issue and $ amount per year are displayed below the format drop down in Print Magazine PDP page.");
		if(ptmagzine==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageDropDownContainer))
				{
					Pass("The Product Type Container is found.The found product type is : " + pdpPageDropDownContainer.getText());
				}
				else
				{
					Skip("The Product Type Container is not displayed.");
				}
				
				if(BNBasicfeature.isListElementPresent(pdpPageSalePriceList))
				{
					for(WebElement price : pdpPageSalePriceList)
					{
						//BNBasicfeature.scrolldown(price, driver);
						if(price.getText().isEmpty())
						{
							Fail("The Sale Price is empty.");
						}
						else
						{
							Pass("The Sale Price is displayed.The displayed sale price is " + price.getText());
						}
					}
				}
				else
				{
					Fail("The Sale Price is not displayed.");
				}
				
				if(BNBasicfeature.isListElementPresent(pdpPageActualPriceList))
				{
					for(WebElement price : pdpPageActualPriceList)
					{
						//System.out.println(price.getText());
						if(price.getText().isEmpty())
						{
							Fail("The Actual Price is empty.");
						}
						else
						{
							Pass("The Actual Price is displayed.The displayed Actual price is " + price.getText());
						}
					}
				}
				else
				{
					Fail("The Actual Price is not displayed.");
				}
				
				if(BNBasicfeature.isListElementPresent(pdpPageActualSavePercent))
				{
					for(WebElement price : pdpPageActualSavePercent)
					{
						if(price.getText().isEmpty())
						{
							Fail("The Save Percent is empty.");
						}
						else
						{
							Pass("The Save Percent is displayed.The displayed Save Percent is " + price.getText());
						}
					}
				}
				else
				{
					Fail("The Save Percent is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualLabelPerIssue))
				{
					String text = pdpPageMagazineAnnualLabelPerIssue.getText();
					if(text.isEmpty())
					{
						Fail("The Sale Price is empty.");
					}
					else
					{
						Pass("The Per Issue label is displayed for the annual subscription. The dispalyed ");
					}
				}
				else
				{
					Fail("The Per Issue label is not displayed for the annual subscription.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualLabelPerYear))
				{
					Pass("The Per Year label is displayed for the annual subscription.");
				}
				else
				{
					Fail("The Per Year label is not displayed for the annual subscription.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2181 Issue ." + e.getMessage());
				Exception(" BRM - 2181 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The User was unable to find the requested Product.");
		}
	}
	
	/* BRM - 2182 Verify that clicking on add to bag adds the product to the cart in Print Magazine PDP page*/
	public void pdpPagePrintMagazineSubscriptionAddtoBag()
	{
		ChildCreation(" BRM - 2182 Verify that clicking on add to bag adds the product to the cart in Print Magazine PDP page.");
		if(ptmagzine==true)
		{
			try
			{
				int prevVal = 0;
				if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
				{
					log.add("The Add to Bag button is displayed.");
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						prevVal = Integer.parseInt(bagCount.getText());
						boolean prdtAdded = clickaddtoBag(prevVal, pdpPageATBBtn);
						if(prdtAdded == true)
						{
							Pass("The Product is added to the Shopping Cart.");
						}
						else
						{
							Fail("The Product is not added to the Shopping Cart.");
						}
					}
					else
					{
						Fail( "The User failed to get the product count");
					}
				}
				else
				{
					Fail("The Add to Bag button is not present.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2182 Issue ." + e.getMessage());
				Exception(" BRM - 2182 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The user was unable to find the Print Magazine Product.");
		}
	}
	
	/* BRM - 1537 Verify that Gift Cards PDP page should be displayed as per the creative*/
	public void pdpPageGiftCardCreative()
	{
		ChildCreation(" BRM - 1537 Verify that Gift Cards PDP page should be displayed as per the creative.");
		try
		{
			/*wait.until(ExpectedConditions.visibilityOf(promoCarousel));
			String cellVal = BNBasicfeature.getExcelVal("BRM1537", sheet, 3);
			searchProduct(sheet, false, "BRM1537");
			Thread.sleep(3000);
			mvfound = selectProduct("Mom Gift Card");*/
			
			String ean = BNBasicfeature.getExcelVal("BRM1537", sheet, 8);
			String expColor = BNBasicfeature.getExcelVal("BRM1537", sheet, 3);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isElementPresent(pdpPageProductName))
					 {
						 if(pdpPageProductName.getText().contains("Gift"))
						 {
							 giftPdp = true;
							 break;
						 }
						 else
						 {
							 giftPdp = false;
							 continue;
						 }
					 }
					 else
					 {
						 giftPdp = false;
						 continue;
					 }
				 }
			}
			
			if(giftPdp==true)
			{
				wait.until(ExpectedConditions.visibilityOf(pdpPageProductNameContainer));
				if(BNBasicfeature.isElementPresent(pdpPageProductNameContainer))
				{
					Pass("The Title for the Gift Card Container is displayed.");
					if(BNBasicfeature.isElementPresent(pdpPageProductName))
					{
						String title = pdpPageProductName.getText();
						if(title.isEmpty())
						{
							Fail("The Gift Card Name is not displayed / empty.");
						}
						else
						{
							Pass("The Gift Card Name is displayed. The Selected Gift Card Name is : " + title);
						}
						
						if(BNBasicfeature.isElementPresent(pdpPageBy))
						{
							String titleby = pdpPageBy.getText();
							if(titleby.isEmpty())
							{
								Fail("The Gift Card Provider Name is not displayed / empty.");
							}
							else
							{
								Pass("The Gift Card Provider Name is displayed. The Selected Gift Card Name is : " + title);
							}
						}
						else
						{
							Fail("The Gift Card Provider name is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Name is not displayed.");
					}
				}
				else
				{
					Fail("The Title for the Gift Card Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					Pass("The Gift Card Value Text field is displayed.");
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
				{
					Pass("The Gift Card Number of Cards drop down field is displayed.");
				}
				else
				{
					Fail("The Gift Card Number of Cards drop down is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
				{
					Pass("The Gift Card To field is displayed.");
				}
				else
				{
					Fail("The Gift Card To field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
				{
					BNBasicfeature.scrolldown(pdpPageGiftCardFrom, driver);
					Pass("The Gift Card From field is displayed.");
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
				
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg))
				{
					Pass("The Gift Card Message field is displayed.");
				}
				else
				{
					Fail("The Gift Card Message field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTotalPrice))
				{
					Pass("The Gift Card Total price is displayed.");
				}
				else
				{
					Fail("The Gift Card Total price is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
				{
					Pass("The Gift Card Add to Bag Button is displayed.");
					String csvalue = pdpPageATBBtn.getCssValue("background-color");
					//String ColorName = csvalue.replace("2px solid", "");
					Color colorhxcnvt = Color.fromString(csvalue);
					//System.out.println(colorhxcnvt);
					String hexCode = colorhxcnvt.asHex();
					log.add("The Expected Color was : " + expColor);
					log.add("The Actual Button Color is : " + hexCode);
					//System.out.println(hexCode);
					if(hexCode.equals(expColor))
					{
						Pass("The button color matches the expected value.",log);
					}
					else
					{
						Fail("The button color does not matches the expected value.",log);
					}
				}
				else
				{
					Fail("The Gift Card Add to Bag Button is not displayed.");
				}
			}
			else
			{
				Fail("The Gift Card is not found.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1537 Issue ." + e.getMessage());
			Exception(" BRM - 1537 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1539 Verify that Gift card title "Barnes & Noble Green Gift Card by Barnes & Noble" should be displayed below the Gift card image in the Gift Cards PDP page*/
	public void pdpPageGiftCardTitleSubtitle()
	{
		ChildCreation(" BRM - 1539 Verify that Gift card title Barnes & Noble Green Gift Card by Barnes & Noble should be displayed below the Gift card image in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(header))
				{
					BNBasicfeature.scrollup(header, driver);
				}
				
				if(BNBasicfeature.isElementPresent(pdpPageProductNameContainer))
				{
					Pass("The Title for the Gift Card Container is displayed.");
					if(BNBasicfeature.isElementPresent(pdpPageProductName))
					{
						String title = pdpPageProductName.getText();
						if(title.isEmpty())
						{
							Fail("The Gift Card Name is not displayed / empty.");
						}
						else
						{
							Pass("The Gift Card Name is displayed. The Selected Gift Card Name is : " + title);
						}
						
						if(BNBasicfeature.isElementPresent(pdpPageBy))
						{
							String titleby = pdpPageBy.getText();
							if(titleby.isEmpty())
							{
								Fail("The Gift Card Provider Name is not displayed / empty.");
							}
							else
							{
								Pass("The Gift Card Provider Name is displayed. The Selected Gift Card Name is : " + title);
							}
						}
						else
						{
							Fail("The Gift Card Provider name is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Name is not displayed.");
					}
				}
				else
				{
					Fail("The Title for the Gift Card Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1539 Issue ." + e.getMessage());
				Exception(" BRM - 1539 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1541 Verify that Card value field should be displayed below the Gift card title in the Gift Cards PDP page*/
	public void pdpPageGiftCardValue()
	{
		ChildCreation(" BRM - 1541 Verify that Card value field should be displayed below the Gift card title in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					Pass("The Gift Card Value Text field is displayed.");
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1541 Issue ." + e.getMessage());
				Exception(" BRM - 1541 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1543 Verify that default text "Card Value" should be displayed in the card value field in the Gift Cards PDP page*/
	public void pdpPageGiftCardValueDefaultText()
	{
		ChildCreation(" BRM - 1543 Verify that default text Card Value should be displayed in the card value field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1543", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardLabel))
				{
					log.add("The Gift Card Value Text field is displayed.");
					String currVal = pdpPageGiftCardLabel.getText();
					log.add("The Expected Default Text was : " + cellVal);
					log.add("The Current Default Text is : " + currVal);
					if(currVal.equals(cellVal))
					{
						Pass("The Value of Default Text matches the expected content.",log);
					}
					else
					{
						Fail("The Value of Default Text does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1543 Issue ." + e.getMessage());
				Exception(" BRM - 1543 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1550 Verify that Number of Cards drop down field should be displayed below the Card Value field in the Gift Cards PDP page*/
	public void pdpPageGiftCardNoOfCardsDropDown()
	{
		ChildCreation(" BRM - 1550 Verify that Number of Cards drop down field should be displayed below the Card Value field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
				{
					Pass("The Gift Card Number of Cards drop down field is displayed.");
				}
				else
				{
					Fail("The Gift Card Number of Cards drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1550 Issue ." + e.getMessage());
				Exception(" BRM - 1550 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1551 Verify that default text "Number of Cards" should be displayed in the Number of Cards field in the Gift Cards PDP page*/
	public void pdpPageGiftCardNoOfCardsDropDownDefaultText()
	{
		ChildCreation(" BRM - 1551 Verify that default text Number of Cards should be displayed in the Number of Cards field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1551", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
				{
					log.add("The Gift Card Number of Cards drop down field is displayed.");
					log.add("The expected value was : "  + cellVal);
					if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDDDefaultText))
					{
						String actval = pdpPageGiftCardNoOfCardsDDDefaultText.getText();
						log.add("The Actual Value is : " + actval);
						if(pdpPageGiftCardNoOfCardsDDDefaultText.getText().equals(cellVal))
						{
							Pass("The default text matches with the expected value.",log);
						}
						else
						{
							Fail("The default text does not match with the expected value.",log);
						}
					}
					else
					{
						Fail("The default text is not displayed.",log);
					}
				}
				else
				{
					Fail("The Gift Card Number of Cards drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1551 Issue ." + e.getMessage());
				Exception(" BRM - 1551 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1552 Verify that Number of cards drop down field has values in the range of 1 to 99 in the Gift Cards PDP page*/
	public void pdpPageGiftCardNoOfCardsDropDownRange()
	{
		ChildCreation(" BRM - 1552 Verify that Number of cards drop down field has values in the range of 1 to 99 in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1552", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
				{
					log.add("The Gift Card Number of Cards drop down field is displayed.");
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					int size = sel.getOptions().size();
					//System.out.println(size);
					String firstOption = sel.getOptions().get(0).getText();
					//System.out.println(firstOption);
					log.add("The First Option Value in the Drop Down is : " + firstOption);
					String lastOption = sel.getOptions().get(size-1).getText();
					//System.out.println(lastOption);
					log.add("The Last Option Value in the Drop Down is : " + lastOption);
					if((firstOption.equals(Val[0]))&&(lastOption.equals(Val[1])))
					{
						Pass("The First Option and Last Option Matches the expected Value.");
					}
					else
					{
						Fail("There is some mismatch in the First and Last Option selection.");
					}
				}
				else
				{
					Fail("The Gift Card Number of Cards drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1552 Issue ." + e.getMessage());
				Exception(" BRM - 1552 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1553 Verify that we can able to select any values (1 to 99) in the Number of Cards drop down in the Gift Cards PDP page*/
	public void pdpPageGiftCardNoOfCardsSelection()
	{
		ChildCreation(" BRM - 1553 Verify that we can able to select any values (1 to 99) in the Number of Cards drop down in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
				{
					log.add("The Gift Card Number of Cards drop down field is displayed.");
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					Random r = new Random();
					int size = sel.getOptions().size();
					//System.out.println(size);
					int opt = r.nextInt(size);
					//System.out.println(opt);
					log.add("The Randomly Selected value was : " + (opt+1));
					sel.selectByIndex(opt);
					Integer currVal = Integer.parseInt(sel.getFirstSelectedOption().getText());
					log.add("The Current Value is : " + currVal);
					if(currVal.equals(opt+1))
					{
						Pass("The user is able to select the value from the drop down randomly.",log);
					}
					else
					{
						Fail("There is some mismatch and the selected value was not set.Please Check.",log);
					}
				}
				else
				{
					Fail("The Gift Card Number of Cards drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1553 Issue ." + e.getMessage());
				Exception(" BRM - 1553 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1555 Verify that "To (Optional)" field should be displayed below the Number of Cards drop down in the Gift Cards PDP page*/
	public void pdpPageGiftCardTOField()
	{
		ChildCreation(" BRM - 1555 Verify that To (Optional) field should be displayed below the Number of Cards drop down in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
				{
					Pass("The Gift Card To field is displayed.");
				}
				else
				{
					Fail("The Gift Card To field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1555 Issue ." + e.getMessage());
				Exception(" BRM - 1555 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1556 Verify that default text "To (Optional)" should be displayed in the To (Optional) field in the Gift Cards PDP page	*/
	public void pdpPageGiftCardTOFieldDefaultText()
	{
		ChildCreation(" BRM - 1556 Verify that default text To (Optional) should be displayed in the To (Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1556", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardToDefaultText))
				{
					log.add("The Gift Card To field is displayed.");
					String currVal = pdpPageGiftCardToDefaultText.getText();
					log.add("The Expected Default Text was : " + cellVal);
					log.add("The Current Default Text is : " + currVal);
					if(currVal.equals(cellVal))
					{
						Pass("The Value of Default Text matches the expected content.",log);
					}
					else
					{
						Fail("The Value of Default Text does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card To field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1556 Issue ." + e.getMessage());
				Exception(" BRM - 1556 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1563 Verify that we can able to enter maximum of 50 characters and minimum of 0 characters in To(Optional) field in the Gift Cards PDP page*/
	public void pdpPageGiftCardToFieldLengthValidation()
	{
		ChildCreation(" BRM - 1563 Verify that we can able to enter maximum of 50 characters and minimum of 0 characters in To(Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1563", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
				{
					log.add("The Gift Card To field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						pdpPageGiftCardTo.clear();
						pdpPageGiftCardTo.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Value from the excel file was : " + Val[i]);
						log.add("The Value length from the excel field is : " + Val[i].length());
						int size = pdpPageGiftCardTo.getAttribute("value").length();
						log.add("The Current value in the To Field is : " + pdpPageGiftCardTo.getText());
						log.add("The Current length in the To Field is : " + size);
						if(size<=50)
						{
							Pass("The To Feild in the Gift Card page accepts character less than 50.",log);
						}
						else
						{
							Fail("The To Feild in the Gift Card page accepts character more than 50.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card To field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1563 Issue ." + e.getMessage());
				Exception(" BRM - 1563 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1564 Verify that "From (Optional)" field should be displayed below the To(Optional) field in the Gift Cards PDP page*/
	public void pdpPageGiftCardFromField()
	{
		ChildCreation(" BRM - 1564 Verify that From (Optional) field should be displayed below the To(Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
				{
					Pass("The Gift Card From field is displayed.");
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1564 Issue ." + e.getMessage());
				Exception(" BRM - 1564 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1565 Verify that default text "From (Optional)" should be displayed in the From (Optional) field in the Gift Cards PDP page */
	public void pdpPageGiftCardFromFieldDefaultText()
	{
		ChildCreation(" BRM - 1565 Verify that default text From (Optional) should be displayed in the From (Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1565", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFromDefaultText))
				{
					log.add("The Gift Card From field is displayed.");
					String currVal = pdpPageGiftCardFromDefaultText.getText();
					log.add("The Expected Default Text was : " + cellVal);
					log.add("The Current Default Text is : " + currVal);
					if(currVal.equals(cellVal))
					{
						Pass("The Value of Default Text matches the expected content.",log);
					}
					else
					{
						Fail("The Value of Default Text does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1565 Issue ." + e.getMessage());
				Exception(" BRM - 1565 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1568 Verify that we can able to enter maximum of 50 characters and minimum of 0 characters in From(Optional) field in the Gift Cards PDP page*/
	public void pdpPageGiftCardFromFieldLengthValidation()
	{
		ChildCreation(" BRM - 1568 Verify that we can able to enter maximum of 50 characters and minimum of 0 characters in From(Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1568", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
				{
					log.add("The Gift Card To field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						pdpPageGiftCardFrom.clear();
						pdpPageGiftCardFrom.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Value from the excel file was : " + Val[i]);
						log.add("The Value length from the excel field is : " + Val[i].length());
						int size = pdpPageGiftCardFrom.getAttribute("value").length();
						log.add("The Current value in the From Field is : " + pdpPageGiftCardFrom.getText());
						log.add("The Current length in the From Field is : " + size);
						if(size<=50)
						{
							Pass("The From Feild in the Gift Card page accepts character less than 50.",log);
						}
						else
						{
							Fail("The From Feild in the Gift Card page accepts character more than 50.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1568 Issue ." + e.getMessage());
				Exception(" BRM - 1568 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1569 Verify that "Gift Message(250/250)" field should be displayed below the From(Optional) field in the Gift Cards PDP page */
	public void pdpPageGiftCardGiftMessage()
	{
		ChildCreation(" BRM - 1569 Verify that Gift Message(250/250) field should be displayed below the From(Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg))
				{
					Pass("The Gift Card Message field is displayed.");
				}
				else
				{
					Fail("The Gift Card Message field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1569 Issue ." + e.getMessage());
				Exception(" BRM - 1569 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/*BRM - 1570 Verify that default text "Gift Message(250/250)" should be displayed in the Gift Message(250/250) field in the Gift Cards PDP page*/
	public void pdpPageGiftCardGiftDefaultText()
	{
		ChildCreation(" BRM - 1570 Verify that default text Gift Message(250/250) should be displayed in the Gift Message(250/250) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1570", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsgDefaultText))
				{
					log.add("The Gift Card Message field is displayed.");
					String currVal = pdpPageGiftCardMsgDefaultText.getText();
					log.add("The Expected Default Text was : " + cellVal);
					log.add("The Current Default Text is : " + currVal);
					if(currVal.equals(cellVal))
					{
						Pass("The Value of Default Text matches the expected content.",log);
					}
					else
					{
						Fail("The Value of Default Text does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card Gift Message field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1570 Issue ." + e.getMessage());
				Exception(" BRM - 1570 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1571 Verify that we can able to enter maximum of 250 characters and minimum of 0 characters in Gift Message(250/250) field in the Gift Cards PDP page*/
	public void pdpPageGiftCardGiftLengthValidation()
	{
		ChildCreation(" BRM - 1571 Verify that we can able to enter maximum of 50 characters and minimum of 0 characters in From(Optional) field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1571", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg))
				{
					log.add("The Gift Card Message field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						pdpPageGiftCardMsg.clear();
						pdpPageGiftCardMsg.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Value from the excel file was : " + Val[i]);
						log.add("The Value length from the excel field is : " + Val[i].length());
						int size = pdpPageGiftCardMsg.getAttribute("value").length();
						log.add("The Current value in the Gift Message Field is : " + pdpPageGiftCardMsg.getText());
						log.add("The Current length in the Gift Message Field is : " + size);
						if(size<=250)
						{
							Pass("The Gift Message Feild in the Gift Card page accepts character less than 250.",log);
						}
						else
						{
							Fail("The Gift Messagein the Gift Card page accepts character more than 250.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1571 Issue ." + e.getMessage());
				Exception(" BRM - 1571 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1574 Verify that default amount "$0.00" should be displayed below the Gift message field in the Gift Cards PDP page */
	public void pdpPageGiftCardTotalPrice()
	{
		ChildCreation(" BRM - 1574 Verify that default amount $0.00 should be displayed below the Gift message field in the Gift Cards PDP page .");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1574", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTotalPrice))
				{
					Pass("The Gift Card Total price is displayed.");
					log.add("The expected content was : " + cellVal);
					String price = pdpPageGiftCardTotalPrice.getText();
					log.add("The Actual Content is :  " + price);
					if(cellVal.equals(price))
					{
						Pass("The value match the expected content.",log);
					}
					else
					{
						Fail("The value does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card Total price is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1574 Issue ." + e.getMessage());
				Exception(" BRM - 1574 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1576 Verify that Add to Bag is displayed below the Amount value in the Gift Cards PDP page*/
	public void pdpPageGiftCardAddtoBag()
	{
		ChildCreation(" BRM - 1576 Verify that Add to Bag is displayed below the Amount value in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1576", sheet, 3);
				if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
				{
					Pass("The Gift Card Add to Bag Button is displayed.");
					String csvalue = pdpPageATBBtn.getCssValue("background-color");
					//String ColorName = csvalue.replace("2px solid", "");
					Color colorhxcnvt = Color.fromString(csvalue);
					//System.out.println(colorhxcnvt);
					String hexCode = colorhxcnvt.asHex();
					log.add("The Expected Color was : " + cellVal);
					log.add("The Actual Button Color is : " + hexCode);
					//System.out.println(hexCode);
					if(hexCode.equals(cellVal))
					{
						Pass("The button color matches the expected value.",log);
					}
					else
					{
						Fail("The button color does not matches the expected value.",log);
					}
				}
				else
				{
					Fail("The Gift Card Add to Bag Button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1576 Issue ." + e.getMessage());
				Exception(" BRM - 1576 Issue." + e.getMessage());
			}	
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1579 Verify that Shipping information should be displayed below the Add to Bag button in the Gift Cards PDP page*/
	public void pdpPageGiftCardShippingInfo()
	{
		ChildCreation(" BRM - 1579 Verify that Shipping information should be displayed below the Add to Bag button in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardShippingDetails))
				{
					Pass("The Shipping information is displayed.");
				}
				else
				{
					Fail("The Shipping information is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1579 Issue ." + e.getMessage());
				Exception(" BRM - 1579 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/*BRM - 2165 Verify that "Customers who bought this also bought" section should be available in the Gift card page*/ 
	public void pdpPageProductGiftCardPageCustomerBoughtSection()
	{
		ChildCreation(" BRM - 2165 Verify that Customers who bought this also bought section should be available in the Gift card page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2165", sheet, 4);
				log.add("The Expected Caption : " + cellVal);
				if(BNBasicfeature.isElementPresent(pdpPageCustomerBoughtSection))
				{
					Pass("The Customer Bought Section is present / displayed.");
					if(BNBasicfeature.isElementPresent(pdpPageCustomerBoughtCaption))
					{
						log.add("The Actual Caption : " + pdpPageCustomerBoughtCaption.getText());
						if(pdpPageCustomerBoughtCaption.getText().contains(cellVal))
						{
							Pass("The Caption matches the expected content : " , log);
						}
						else
						{
							Fail("The Caption does not match the expected content : " , log);
						}
					}
					else
					{
						Fail("The caption field is not present.");
					}
				}
				else if(BNBasicfeature.isElementPresent(pdpPageCustomerBoughtSection1))
				{
					Pass("The Customer Bought Section is present / displayed.");
					if(BNBasicfeature.isElementPresent(pdpPageCustomerBoughtCaption))
					{
						log.add("The Actual Caption : " + pdpPageCustomerBoughtCaption.getText());
						if(pdpPageCustomerBoughtCaption.getText().contains(cellVal))
						{
							Pass("The Caption matches the expected content : " , log);
						}
						else
						{
							Fail("The Caption does not match the expected content : " , log);
						}
					}
					else
					{
						Fail("The caption field is not present.");
					}
				}
				else
				{
					Skip("The Customer Bought Section is not present / displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2165 Issue ." + e.getMessage());
				Exception(" BRM - 2165 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 2166 Verify that product image, product title, author name and star ratings should be displayed in the "Customer who bought this also bought" section in Gift card page.*/
	public void pdpPageProductGiftCardCustomerBoughtSectionPageDetails()
	{
		ChildCreation(" BRM - 2166 Verify that product image, product title, author name and star ratings should be displayed in the Customer who bought this also bought section in Gift card page.");
		if(giftPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageCustomerBoughtSection))
				{
					log.add("The Customer Who Bought product Container is found.");
					if(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerList)&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerImageList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerImageLinkList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerTitleLinkList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerRatingList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerRatingCountList)))
					{
						log.add("The Product list are available in the Product Container.");
						for(int i = 1; i<pdpPageCustomerBoughtContainerList.size();i++)
						{
							//WebElement prdtImage = driver.findElement(By.xpath("(//*[contains(@id,'GridItem')]//img)["+i+"]"));
							WebElement prdtImage = pdpPageCustomerBoughtContainerImageList.get(i-1);
							BNBasicfeature.scrolldown(prdtImage, driver);
							//WebElement prdtImageLink = driver.findElement(By.xpath("(//*[contains(@id,'GridItem')]//a)["+i+"]"));
							WebElement prdtImageLink = pdpPageCustomerBoughtContainerImageLinkList.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtImage))
							{
								int imagestatus = BNBasicfeature.imageBroken(prdtImage,log);
								//System.out.println(imagestatus);
								log.add("The Image Status is : " + imagestatus);
								log.add("The Product Image is found.");
								if(imagestatus == 200)
								{
									Pass("The Image is not broken and it is displayed",log);
								}
								else
								{
									Fail("The Image is broken and it is not displayed.",log);
								}
								
								int linkStatus = BNBasicfeature.linkBroken(prdtImageLink,log);
								log.add("The Link Status from the Product Name is : " + linkStatus);
								if(linkStatus == 200)
								{
									Pass("The link is not broken and it is valid. ",log);
								}
								else
								{
									Fail("The link is not broken and it is not valid. .",log);
								}
							}
							else
							{
								System.out.println("The Product Image is not found.");
								Fail("The Product Image is not found.",log);
								log.add("The Product Image is not found.");
							}
							
							//WebElement prdtName = driver.findElement(By.xpath("(//*[contains(@id,'GridItem')]//img)["+i+"]"));
							WebElement prdtName = pdpPageCustomerBoughtContainerImageList.get(i-1);
							//WebElement prdtNameLink = driver.findElement(By.xpath("(//*[contains(@id,'GridItem')]//*[contains(@class,'Title')]//a)["+i+"]"));
							WebElement prdtNameLink = pdpPageCustomerBoughtContainerTitleLinkList.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtName))
							{
								String pName = prdtName.getAttribute("name");
								//System.out.println(pName);
								log.add("The Product Name is displayed. The Current Product Name : " + pName);
								int linkStatus = BNBasicfeature.linkBroken(prdtNameLink,log);
								log.add("The Link Status from the Product Name is : " + linkStatus);
								if(linkStatus == 200)
								{
									log.add("The Product Link Status is Valid.");
									Pass("The link is not broken and it is valid. ",log);
								}
								else
								{
									log.add("The Product Link Status is not Valid.");
									Fail("The link is not broken and it is not valid. .",log);
								}
							}
							else
							{
								Fail("The Product Name is not displayed for the Product.");
							}
							
							/*WebElement authorName = driver.findElement(By.xpath("(//*[@id='id_pdtGridItemContainer']//*[@class='sktab_pdtGridItemAuthorTitle'])["+i+"]"));
							if(BNBasicfeature.isElementPresent(authorName))
							{
								System.out.println(authorName.getText());
								Pass("The author name is displayed. The author name is : " + authorName.getText());
							}
							else
							{
								System.out.println(authorName.getText());
								Fail("The author name is not displayed");
							}*/
							
							//WebElement starRating = driver.findElement(By.xpath("(//*[@id='id_sktab_pdtGrid_zeroRatingStar']//div)["+i+"]"));
							WebElement starRating = pdpPageCustomerBoughtContainerRatingList.get(i-1);
							//WebElement starRatingCount = driver.findElement(By.xpath("(//*[@id='id_sktab_pdtGrid_zeroRatingStar']//span)["+i+"]"));
							WebElement starRatingCount = pdpPageCustomerBoughtContainerRatingCountList.get(i-1);
							if(BNBasicfeature.isElementPresent(starRating))
							{
								//System.out.println(starRatingCount.getText());
								log.add("The Star Rating for the Product is : " + starRatingCount);
								Pass("The Star rating for the product is displayed.");
							}
							else
							{
								System.out.println(starRating);
								Fail("The Star Rating is not displayed for the product.");
							}
						}
					}
					else
					{
						Fail("The Customer who bought container is not displayed.");
					}
				}
				else
				{
					Skip("The Customer Who Bought Product Container is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2166 Issue ." + e.getMessage());
				Exception(" BRM - 2166 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 2167 Verify that products should be scrolled horizontally to view the other products in the Gift card page*/
	public void pdpPageProductGiftCardCustomerBoughtSectionPageScroll()
	{
		ChildCreation(" BRM - 2167 Verify that products should be scrolled horizontally to view the other products in the Gift card page.");
		if(giftPdp==true)
		{
			try
			{
				if((BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerTitleLinkList)))
				{
					log.add("The Product list are available in the Product Container.");
					for(int i = 1; i<pdpPageCustomerBoughtContainerList.size();i++)
					{
						//WebElement accPrdtName = driver.findElement(By.xpath("(//*[@id='id_pdtGridItemContainer'])["+i+"]//*[@class='sktab_pdtGridItemTitle']//a"));
						WebElement accPrdtName = pdpPageCustomerBoughtContainerTitleLinkList.get(i-1);
						BNBasicfeature.scrolldown(accPrdtName, driver);
						if(BNBasicfeature.isElementPresent(accPrdtName))
						{
							if(accPrdtName.getText().isEmpty())
							{
								Fail("The Product Name is empty.");
							}
							else
							{
								Pass("The Product Name is displayed.");
								//System.out.println(accPrdtName.getText());
								log.add("The Product Name is : " + accPrdtName.getText());
								int prdtLinkResp = BNBasicfeature.linkBroken(accPrdtName, log);
								if(prdtLinkResp==200)
								{
									Pass("The Product Link is not broken.",log);
								}
								else
								{
									Fail("The Product Link is broken.",log);
								}
							}
						}
						else
						{
							Fail("The Product Name is not displayed.",log);
						}
					}
				}
				else
				{
					Skip("The Customer Bought List Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2167 Issue ." + e.getMessage());
				Exception(" BRM - 2167 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1544 Verify that Card Value field should not accept alphabets and special characters in the Gift Cards PDP page*/
	public void pdpPageGiftCardValueInvalidValidation()
	{
		ChildCreation(" BRM - 1544 Verify that Card Value field should not accept alphabets and special characters in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1544", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					BNBasicfeature.scrollup(pdpPageGiftCardValueField, driver);
					log.add("The Gift Card Value Text field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardValueField.clear();
						pdpPageGiftCardValueField.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						if((pdpPageGiftCardValueField.getAttribute("value").isEmpty())||(pdpPageGiftCardValueField.getAttribute("value").contains(Val[i])))
						{
							log.add("The Current value in the Card Field is : " + pdpPageGiftCardValueField.getAttribute("value"));
							Pass("The Card Value does not accept any other value other than the numeric.",log);
						}
						else
						{
							log.add("The Current value in the Card Field is : " + pdpPageGiftCardValueField.getAttribute("value"));
							Fail("The Card Value accepts not only the numeric value but also the other entered datas.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1544 Issue ." + e.getMessage());
				Exception(" BRM - 1544 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1542 Verify that we should be able to enter the card value in the Card value field and it should accept only numbers in the Gift Cards PDP page */
	public void pdpPageGiftCardValueValidation()
	{
		ChildCreation(" BRM - 1542 Verify that we should be able to enter the card value in the Card value field and it should accept only numbers in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1542", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardValueField.clear();
						pdpPageGiftCardValueField.sendKeys(Val[i]);
						Thread.sleep(500);
						log.add("The Entered Value was : " + Val[i]);
						String currVal = pdpPageGiftCardValueField.getAttribute("value");
						if((pdpPageGiftCardValueField.getAttribute("value").isEmpty())||(Val[i].contains(currVal)))
						{
							log.add("The Current value in the Card Field is : " + pdpPageGiftCardValueField.getAttribute("value"));
							Pass("The Card Value does not accept any other value other than the numeric.",log);
						}
						else
						{
							log.add("The Current value in the Card Field is : " + pdpPageGiftCardValueField.getAttribute("value"));
							Fail("The Card Value accepts not only the numeric value but also the other entered datas.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1542 Issue ." + e.getMessage());
				Exception(" BRM - 1542 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1549 Verify that if we leave card value as blank and clicking on Add to Bag should display "Card Value field cannot be left blank" alert message in the Gift Cards PDP page */
	public void pdpPageGiftCardNoOfCardsAlert()
	{
		ChildCreation(" BRM - 1549 Verify that if we leave card value as blank and clicking on Add to Bag should display Card Value field cannot be left blank alert message in the Gift Cards PDP page");
		if(giftPdp==true)
		{
			try
			{
				//String cellVal = BNBasicfeature.getExcelNumericVal("BRM1549", sheet, 4);
				String cellError = BNBasicfeature.getExcelVal("BRM1549", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField)&&(BNBasicfeature.isElementPresent(pdpPageATBBtn)))
				{
					log.add("The Gift Card Value Text field is displayed.");
					pdpPageGiftCardValueField.clear();
					//pdpPageGiftCardValueField.sendKeys(cellVal);
					jsclick(pdpPageATBBtn);
					Thread.sleep(100);
					log.add("The Expected alert was : " + cellError);
					wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
					wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
					Thread.sleep(100);
					log.add("The Actual alert was : " + pdpPageGiftCardError.getText());
					if(pdpPageGiftCardError.getText().equals(cellError))
					{
						Pass("The Expected alert was raised.",log);
					}
					else
					{
						Fail("The raised alert does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1549 Issue ." + e.getMessage());
				Exception(" BRM - 1549 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1545 Verify that card value field accept minimum value of $10 and maximum value of $350 in the Gift Cards PDP page*/
	public void pdpPageGiftCardValueAmountValidation()
	{
		ChildCreation(" BRM - 1545 Verify that card value field accept minimum value of $10 and maximum value of $350 in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1545", sheet, 4);
				String[] Amt = cellVal.split("\n");
				String cellAlrt = BNBasicfeature.getExcelVal("BRM1545", sheet, 5);
				String[] Alert = cellAlrt.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					for(int i= 0; i<Amt.length;i++)
					{
						pdpPageGiftCardValueField.clear();
						pdpPageGiftCardValueField.sendKeys(Amt[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Amt[i]);
						Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
						sel.selectByIndex(1);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(pdpPageGiftCardError))
						{
							log.add("The error is raised when user entered the " + Amt[i] + " and proceeded to Add to Bag the card.");
							String finalAlert = Amt[i].concat(" " +Alert[i]);
							//System.out.println(finalAlert);
							log.add("The Expected alert was : " + finalAlert);
							String actualAlert = pdpPageGiftCardError.getText();
							log.add("The Actual Alert was : " + pdpPageGiftCardError.getText());
							if(finalAlert.contains(actualAlert))
							{
								Pass("The raised alert matches the expected value",log);
							}
							else
							{
								Fail("The raised alert does not matches the expected value.",log);
							}
						}
						else
						{
							Fail("The error message is not raised when user enterd the invalid amount.");
						}
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1545 Issue ." + e.getMessage());
				Exception(" BRM - 1545 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1546 Verify that if we enter card value less than $10 and clicking on add to bag should display "2 is too low. The lowest value you can use is 10." alert message in the Gift Cards PDP page*/
	public void pdpPageGiftCardValueLowAmountValidation()
	{
		ChildCreation(" BRM -1546 Verify that if we enter card value less than $10 and clicking on add to bag should display 3 is too low. The lowest value you can use is 10. alert message in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM1546", sheet, 4);
				String cellAlrt = BNBasicfeature.getExcelVal("BRM1546", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					pdpPageGiftCardValueField.clear();
					pdpPageGiftCardValueField.sendKeys(cellVal);
					Thread.sleep(100);
					log.add("The Entered Value was : " + cellVal);
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					sel.selectByIndex(1);
					jsclick(pdpPageATBBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
					wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(pdpPageGiftCardError))
					{
						log.add("The error is raised when user entered the " + cellVal + " and proceeded to Add to Bag the card.");
						log.add("The Expected alert was : " + cellAlrt);
						String actualAlert = pdpPageGiftCardError.getText();
						log.add("The Actual Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlrt.contains(actualAlert))
						{
							Pass("The raised alert matches the expected value",log);
						}
						else
						{
							Fail("The raised alert does not matches the expected value.",log);
						}
					}
					else
					{
						Fail("The error message is not raised when user enterd the invalid amount.");
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1546 Issue ." + e.getMessage());
				Exception(" BRM - 1546 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1547 Verify that if we enter card value greater than $350 and clicking on add to bag should display "999 is too high. The highest value you can use is 350." alert message in the Gift Cards PDP page */
	public void pdpPageGiftCardValueHighAmountValidation()
	{
		ChildCreation(" BRM - 1547 Verify that if we enter card value greater than $350 and clicking on add to bag should display 999 is too high. The highest value you can use is 350. alert message in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM1547", sheet, 4);
				String cellAlrt = BNBasicfeature.getExcelVal("BRM1547", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					pdpPageGiftCardValueField.clear();
					pdpPageGiftCardValueField.sendKeys(cellVal);
					Thread.sleep(100);
					log.add("The Entered Value was : " + cellVal);
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					sel.selectByIndex(1);
					jsclick(pdpPageATBBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
					wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(pdpPageGiftCardError))
					{
						log.add("The error is raised when user entered the " + cellVal + " and proceeded to Add to Bag the card.");
						log.add("The Expected alert was : " + cellAlrt);
						String actualAlert = pdpPageGiftCardError.getText();
						log.add("The Actual Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlrt.contains(actualAlert))
						{
							Pass("The raised alert matches the expected value",log);
						}
						else
						{
							Fail("The raised alert does not matches the expected value.",log);
						}
					}
					else
					{
						Fail("The error message is not raised when user enterd the invalid amount.");
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1547 Issue ." + e.getMessage());
				Exception(" BRM - 1547 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1548 Verify that if we enter card value less than $10 and greater than $350, then the card value field should be highlighted in the Gift Cards PDP page.*/
	public void pdpPageGiftCardValueValidationHighlight()
	{
		ChildCreation(" BRM - 1548 Verify that if we enter card value less than $10 and greater than $350, then the card value field should be highlighted in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1548", sheet, 4);
				String[] Amt = cellVal.split("\n");
				String cellColor = BNBasicfeature.getExcelVal("BRM1548", sheet, 3);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					for(int i= 0; i<Amt.length;i++)
					{
						pdpPageGiftCardValueField.clear();
						pdpPageGiftCardValueField.sendKeys(Amt[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Amt[i]);
						Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
						sel.selectByIndex(1);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						String cssVal = pdpPageGiftCardValueField.getCssValue("border");
						//System.out.println(cssVal);
						String actualColor = BNBasicfeature.colorfinder(cssVal);
						//System.out.println(actualColor);
						log.add("The Expected Color was : " + cellColor);
						log.add("The Actual Color is : " + actualColor);
						if(actualColor.equals(cellColor))
						{
							Pass("The Gift Card Value field is highlighted with the expected color.",log);
						}
						else
						{
							Fail("The Gift Card Value field is not highlighted with the expected color.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1548 Issue ." + e.getMessage());
				Exception(" BRM - 1548 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1557 Verify that To (Optional) accepts only letters, spaces, commas, periods, dashes, and single quotes in the Gift Cards PDP page*/
	public void pdpPageGiftCardTOFieldValidation()
	{
		ChildCreation(" BRM - 1557 Verify that To (Optional) accepts only letters, spaces, commas, periods, dashes, and single quotes in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1557", sheet, 4);
				String[] Val = cellVal.split("\n");
				String cellAlert = BNBasicfeature.getExcelVal("BRM1557", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
				{
					pdpPageGiftCardValueField.clear();
					pdpPageGiftCardValueField.sendKeys(Val[0]);
					Thread.sleep(100);
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					sel.selectByIndex(1);
					log.add("The Gift Card To field is displayed.");
					for(int i= 1; i<Val.length;i++)
					{
						pdpPageGiftCardTo.clear();
						pdpPageGiftCardTo.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						log.add("The Expected Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlert.equals(pdpPageGiftCardError.getText()))
						{
							Pass("The Gift Card To field alert matches the expected value. " ,log);
						}
						else
						{
							Fail("The Gift Card To field alert does not matches the expected value. " ,log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value To field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1557 Issue ." + e.getMessage());
				Exception(" BRM - 1557 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1562 Verify that if we enter anything other than letters, spaces, commas, periods, dashes, and single quotes in To (Optional) field should display,the alert message and the field should be highlighted in the Gift Cards PDP page */
	public void pdpPageGiftCardToFieldValidationHighlightAlert()
	{
		ChildCreation(" BRM - 1562 Verify that if we enter anything other than letters, spaces, commas, periods, dashes, and single quotes in To (Optional) field should display,the alert message and the field should be highlighted in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM1562", sheet, 3);
				String cellVal = BNBasicfeature.getExcelVal("BRM1562", sheet, 4);
				String[] Val = cellVal.split("\n");
				String cellAlert = BNBasicfeature.getExcelVal("BRM1562", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
				{
					log.add("The Gift Card To field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardTo.clear();
						pdpPageGiftCardTo.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						log.add("The Expected Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlert.equals(pdpPageGiftCardError.getText()))
						{
							Pass("The Gift Card To field alert matches the expected value. " ,log);
						}
						else
						{
							Fail("The Gift Card To field alert does not matches the expected value. " ,log);
						}
						
						String cssVal = pdpPageGiftCardTo.getCssValue("border");
						//System.out.println(cssVal);
						String actualColor = BNBasicfeature.colorfinder(cssVal);
						//System.out.println(actualColor);
						log.add("The Expected Color was : " + expColor);
						log.add("The Actual Color is : " + actualColor);
						if(actualColor.equals(expColor))
						{
							Pass("The Gift Card To Field is highlighted with the expected color.",log);
						}
						else
						{
							Fail("The Gift Card To Field is not highlighted with the expected color.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value To field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1562 Issue ." + e.getMessage());
				Exception(" BRM - 1562 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1566 Verify that From (Optional) accepts only letters, spaces, commas, periods, dashes, and single quotes in the Gift Cards PDP page*/
	public void pdpPageGiftCardFromFieldValidation()
	{
		ChildCreation(" BRM - 1566 Verify that From (Optional) accepts only letters, spaces, commas, periods, dashes, and single quotes in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1566", sheet, 4);
				String[] Val = cellVal.split("\n");
				String cellAlert = BNBasicfeature.getExcelVal("BRM1566", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
				{
					pdpPageGiftCardTo.clear();
					log.add("The Gift Card From field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardFrom.clear();
						pdpPageGiftCardFrom.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						log.add("The Expected Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlert.equals(pdpPageGiftCardError.getText()))
						{
							Pass("The Gift Card From field alert matches the expected value. " ,log);
						}
						else
						{
							Fail("The Gift Card From field alert does not matches the expected value. " ,log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Value From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1566 Issue ." + e.getMessage());
				Exception(" BRM - 1566 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1567 Verify that if we enter anything other than letters, spaces, commas, periods, dashes, and single quotes in From (Optional) field should display ,the alert message and the field should be highlighted in the Gift Cards PDP page*/
	public void pdpPageGiftCardFromFieldValidationHighlightAlert()
	{
		ChildCreation(" BRM - 1567 Verify that if we enter anything other than letters, spaces, commas, periods, dashes, and single quotes in From (Optional) field should display ,the alert message and the field should be highlighted in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM1567", sheet, 3);
				String cellVal = BNBasicfeature.getExcelVal("BRM1567", sheet, 4);
				String[] Val = cellVal.split("\n");
				String cellAlert = BNBasicfeature.getExcelVal("BRM1567", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
				{
					log.add("The Gift Card From field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardFrom.clear();
						pdpPageGiftCardFrom.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						jsclick(pdpPageATBBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
						wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
						Thread.sleep(100);
						log.add("The Expected Alert was : " + pdpPageGiftCardError.getText());
						if(cellAlert.equals(pdpPageGiftCardError.getText()))
						{
							Pass("The Gift Card From field alert matches the expected value. " ,log);
						}
						else
						{
							Fail("The Gift Card From field alert does not matches the expected value. " ,log);
						}
						
						String cssVal = pdpPageGiftCardFrom.getCssValue("border");
						//System.out.println(cssVal);
						String actualColor = BNBasicfeature.colorfinder(cssVal);
						//System.out.println(actualColor);
						log.add("The Expected Color was : " + expColor);
						log.add("The Actual Color is : " + actualColor);
						if(actualColor.equals(expColor))
						{
							Pass("The Gift Card From Field is highlighted with the expected color.",log);
						}
						else
						{
							Fail("The Gift Card From Field is not highlighted with the expected color.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card From field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1567 Issue ." + e.getMessage());
				Exception(" BRM - 1567 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1702 Verify whether Clicking on Add to Bag, after entering alpha numeric characters(say $, %, etc) generates corresponding alert message*/
	public void pdpPageGiftCardAlphaNumericAlert()
	{
		ChildCreation(" BRM - 1702 Verify whether Clicking on Add to Bag, after entering alpha numeric characters(say $, %, etc) generates corresponding alert message.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1702", sheet, 4);
				String[] Val =  cellVal.split("\n");
				String alrtMsg = BNBasicfeature.getExcelVal("BRM1702", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					giftfieldClear();
					pdpPageGiftCardValueField.sendKeys(Val[0]);
					Thread.sleep(100);
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					sel.selectByIndex(1);
					pdpPageGiftCardTo.sendKeys(Val[1]);
					jsclick(pdpPageATBBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
					wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
					Thread.sleep(100);
					String currAlrt = pdpPageGiftCardError.getText();
					log.add("The Expected Alert was : " + alrtMsg);
					log.add("The Current Alert is : " + currAlrt);
					if(alrtMsg.contains(currAlrt))
					{
						Pass("The Alert matches the expected content,",log);
					}
					else
					{
						Fail("The Alert does not matches the expected content,",log);
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1702 Issue ." + e.getMessage());
				Exception(" BRM - 1702 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1703 Verify Whether alert message is displayed when total amount for gift card exceeds 1000$*/
	public void pdpPageGiftCardValueExceedAlert()
	{
		ChildCreation(" BRM - 1703 Verify Whether alert message is displayed when total amount for gift card exceeds 1000$.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1702", sheet, 4);
				String[] Val =  cellVal.split("\n");
				String alrtMsg = BNBasicfeature.getExcelVal("BRM1703", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
				{
					log.add("The Gift Card Value Text field is displayed.");
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					giftfieldClear();
					Thread.sleep(100);
					pdpPageGiftCardValueField.sendKeys(Val[0]);
					Thread.sleep(100);
					sel.selectByIndex(7);
					//pdpPageGiftCardTo.sendKeys(Val[1]);
					jsclick(pdpPageATBBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(pdpPageGiftCardError));
					wait.until(ExpectedConditions.attributeContains(pdpPageGiftCardError, "style", "block"));
					Thread.sleep(500);
					String currAlrt = pdpPageGiftCardError.getText();
					log.add("The Expected Alert was : " + alrtMsg);
					log.add("The Current Alert is : " + currAlrt);
					if(alrtMsg.contains(currAlrt))
					{
						Pass("The Alert matches the expected content,",log);
					}
					else
					{
						Fail("The Alert does not matches the expected content,",log);
					}
				}
				else
				{
					Fail("The Gift Card Value Text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1703 Issue ." + e.getMessage());
				Exception(" BRM - 1703 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1572 Verify that Gift Message(250/250) field accepts alphabets, numbers and special characters in the Gift Cards PDP page*/
	public void pdpPageGiftCardGiftFieldValidation()
	{
		ChildCreation(" BRM - 1572 Verify that Gift Message(250/250) field accepts alphabets, numbers and special characters in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1572", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg))
				{
					BNBasicfeature.scrolldown(pdpPageGiftCardFrom, driver);
					pdpPageGiftCardFrom.clear();
					log.add("The Gift Card Message field is displayed.");
					for(int i= 0; i<Val.length;i++)
					{
						pdpPageGiftCardMsg.clear();
						pdpPageGiftCardMsg.sendKeys(Val[i]);
						Thread.sleep(100);
						log.add("The Entered Value was : " + Val[i]);
						String currVal = pdpPageGiftCardMsg.getAttribute("value");
						if(Val[i].equals(currVal))
						{
							Pass("The Gift Message field accepts the entered value and it matches the expected content.",log);
						}
						else
						{
							Fail("The Gift Message field does not accepts the entered value or and there is mis-matches in the expected content.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Message field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1572 Issue ." + e.getMessage());
				Exception(" BRM - 1572 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1573 Verify that after entering some text in the Gift Message(250/250) field, remaining number of letters are displayed in the bottom right side of the field in the Gift Cards PDP page*/
	public void pdpPageGiftCardGiftFieldRemainingCharacter()
	{
		ChildCreation(" BRM - 1573 Verify that after entering some text in the Gift Message(250/250) field, remaining number of letters are displayed in the bottom right side of the field in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1573", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageGiftCardMsg))
				{
					BNBasicfeature.scrolldown(pdpPageGiftCardFrom, driver);
					pdpPageGiftCardFrom.clear();
					log.add("The Gift Card Message field is displayed.");
					pdpPageGiftCardMsg.clear();
					pdpPageGiftCardMsg.sendKeys(cellVal);
					Thread.sleep(100);
					Integer excelValLength = cellVal.length();
					log.add("The Entered Value was : " + cellVal);
					if(BNBasicfeature.isElementPresent(pdpPageGiftCardRemainingChar))
					{
						Pass("The Remaining Character notifier is dispalyed.");
						Integer currcount = Integer.parseInt(pdpPageGiftCardRemainingChar.getText());
						log.add("The Current length remaining out of 250 character is : " + (250-currcount));
						if((250-currcount)==excelValLength)
						{
							Pass("The expected and the current length matches.",log);
						}
						else
						{
							Fail("There is mismatch in the expected and the current length.",log);
						}
					}
					else
					{
						Fail("The Remaining Character notifier is not dispalyed.",log);
					}
				}
				else
				{
					Fail("The Gift Card Message field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1573 Issue ." + e.getMessage());
				Exception(" BRM - 1573 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1575 Verify that after entering amount in the card value field and selecting a value from the Number of cards drop down, amount value should be updated by "Card value * Number of cards" in the Gift Cards PDP page*/
	public void pdpPageGiftCardTotalPriceCalculation()
	{
		ChildCreation(" BRM - 1575 Verify that after entering amount in the card value field and selecting a value from the Number of cards drop down, amount value should be updated by Card value * Number of cards in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				giftfieldClear();
				Thread.sleep(100);
				int low = 10;
				int high = 350;
				Random r = new Random();
				int sel = r.nextInt(high - low) + low;
				String val = String.valueOf(sel);
				pdpPageGiftCardValueField.sendKeys(val);
				Thread.sleep(100);
				log.add("The randomly selected amount was : " + val);
				Select s = new Select(pdpPageGiftCardNoOfCardsDD);
				s.selectByIndex(0);
				BNBasicfeature.scrolldown(pdpPageGiftCardFrom, driver);
				Thread.sleep(100);
				int calc = (sel * 1);
				log.add("The actual price is : " + pdpPageGiftCardTotalPrice.getText());
				if(pdpPageGiftCardTotalPrice.getText().contains(String.valueOf(calc)))
				{
					Pass("The total price calculation works as expected.",log);
				}
				else
				{
					Fail("There is some mismatch in the total price calculation. Please Check.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1575 Issue ." + e.getMessage());
				Exception(" BRM - 1575 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 2168 Verify that on selecting the product image or product title the corresponding PDP page must be displayed in the Gift card page.*/
	public void pdpPageProductGiftCardCustomerBoughtNavigation()
	{
		ChildCreation(" BRM - 2168 Verify that on selecting the product image or product title the corresponding PDP page must be displayed in the Gift card page.");
		if(giftPdp==true)
		{
			String urlBefNavigation = "";
			try
			{
				if((BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerList))&&(BNBasicfeature.isListElementPresent(pdpPageCustomerBoughtContainerImageList)))
				{
					log.add("The PDP page displays the Other Product bought by this Customer also.");
					//WebElement prdtImage = driver.findElement(By.xpath("(//*[@id='id_pdtGridItemContainer']//img)[1]"));
					WebElement prdtImage = pdpPageCustomerBoughtContainerImageList.get(0);
					BNBasicfeature.scrolldown(prdtImage, driver);
					Random r = new Random();
					int sel = r.nextInt(pdpPageCustomerBoughtContainerList.size());
					//System.out.println(sel);
					//prdtImage = driver.findElement(By.xpath("(//*[@id='id_pdtGridItemContainer']//img)["+(sel+1)+"]"));
					prdtImage = pdpPageCustomerBoughtContainerImageList.get(sel);
					BNBasicfeature.scrolldown(prdtImage, driver);
					//WebElement prdtName = driver.findElement(By.xpath("(//*[@id='id_pdtGridItemContainer']//img)["+(sel+1)+"]"));
					WebElement prdtName = pdpPageCustomerBoughtContainerImageList.get(sel);
					String pName = prdtName.getAttribute("name");
					System.out.println(pName);
					log.add("The Selected Product from the Customer Who also bought this section is : " + prdtName.getText());
					urlBefNavigation = driver.getCurrentUrl();
					//System.out.println(urlBefNavigation);
					log.add("The URL Before Navigation : " + urlBefNavigation);
					jsclick(prdtName);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					wait.until(ExpectedConditions.visibilityOf(pdpPageATBBtn));
					if(BNBasicfeature.isElementPresent(pdpPageProductName))
					{
						System.out.println(pdpPageProductName.getText());
						String urlAftNavigation = driver.getCurrentUrl();
						//System.out.println(urlAftNavigation);
						log.add("The URL After Navigation : " + urlAftNavigation);
						log.add("The Prdocut Name is displayed in the PDP Page. The Current Name in the PDP Page is : " + pdpPageProductName.getText());
						if(pName.contains(pdpPageProductName.getText())||(urlBefNavigation!=(urlAftNavigation)))
						{
							Pass("The user is redirected to the respective PDP Page and the product name matches.",log);
						}
						else
						{
							Fail("The user is not redirected to the respective PDP page.",log);
							driver.get(urlBefNavigation);
						}
					}
					else
					{
						Fail("The Prodcut name is not displayed in the PDP Page.",log);
					}
					
					String currUrl = driver.getCurrentUrl();
					if(!currUrl.equals(urlBefNavigation))
					{
						driver.get(urlBefNavigation);
					}
				}
				else
				{
					Skip("The Customer Who bought Container list is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2168 Issue ." + e.getMessage());
				Exception(" BRM - 2168 Issue." + e.getMessage());
				String currUrl = driver.getCurrentUrl();
				if(!currUrl.equals(urlBefNavigation))
				{
					driver.get(urlBefNavigation);
				}
				wait.until(ExpectedConditions.visibilityOf(header));
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1577 Verify that clicking on Add to Bag displays a overlay with message "Item(s) successfully added to your shopping bag" in the Gift Cards PDP page*/
	public void pdpPageGiftCardAddGifttoBagSuccessMessage()
	{
		ChildCreation(" BRM - 1577 Verify that clicking on Add to Bag displays a overlay with message Item(s) successfully added to your shopping bag in the Gift Cards PDP page.");
		if(giftPdp==true)
		{
			try
			{
				giftfieldClear();
				pdpPageGiftCardValueField.clear();
				pdpPageGiftCardValueField.sendKeys("10");
				Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
				sel.selectByIndex(0);
				Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
				{
					log.add("The Add to Bag icon is displayed.");
					jsclick(pdpPageATBBtn);
					wait.until(ExpectedConditions.visibilityOf(pdpPageMiniCartfly)).getAttribute("style").contains("block");
					wait.until(ExpectedConditions.visibilityOf(pdpPageMiniCartfly)).getAttribute("style").contains("none");
					if(BNBasicfeature.isElementPresent(pdpPageMiniCartfly))
					{
						Pass("The Mini Cart Fly is displayed.");
						cardAdded = true;
					}
					else
					{
						Fail("The Mini Cart Fly is not displayed.");
						cardAdded = false;
					}
				}
				else
				{
					Fail("The Add to Bag Button is not displayed in the Gift Card page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2168 Issue ." + e.getMessage());
				Exception(" BRM - 2168 Issue." + e.getMessage());
				cardAdded = false;
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found.");
		}
	}
	
	/* BRM - 1578 Verify that clicking on View shopping bag in the overlay displays the checkout page and the gift card should be added to the shopping bag in the Gift Cards PDP page */
	public void pdpPageGiftCardShopBagClick() throws Exception
	{
		ChildCreation(" BRM - 1578 Verify that clicking on View shopping bag in the overlay displays the checkout page and the gift card should be added to the shopping bag in the Gift Cards PDP page .");
		boolean prdtIdadded = false;
		String addedId = "";
		if((giftPdp==true)&&(cardAdded==true))
		{
			boolean added = false;
			try
			{
				if(BNBasicfeature.isElementPresent(bagCount))
				{
					BNBasicfeature.scrollup(bagCount, driver);
				}
				
				if(BNBasicfeature.isElementPresent(productId))
				{
					addedId = productId.getAttribute("identifier");
					log.add("The Added Product id is : " + addedId);
				}
				else
				{
					Fail("The Product id is not displayed.");
				}
				if(addedId.isEmpty())
				{
					Fail("The Product id is not displayed so product was not added.");
				}
				else
				{
					giftfieldClear();
					pdpPageGiftCardValueField.clear();
					pdpPageGiftCardValueField.sendKeys("10");
					Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
					sel.selectByIndex(1);
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(bagIcon))
					{
						boolean getCount = getBagCount();
						if(getCount==true)
						{
							int prdtval = Integer.parseInt(bagCount.getText());
							/*int count = 1;
							do
							{
								jsclick(pdpPageATBBtn);
								Thread.sleep(3500);
								BNBasicfeature.scrollup(bagCount, driver);
								Thread.sleep(1000);
								int currVal = Integer.parseInt(bagCount.getText());
								if(BNBasicfeature.isElementPresent(productAddedSuccessId))
								{
									if(currVal>prdtval)
									{
										added = true;
										break;
									}
									else
									{
										jsclick(pdpPageATBBtn);
										Thread.sleep(3000);
										count++;
										continue;
									}
								}
								else
								{
									count++;
									continue;
								}
							}while(count<= 5 || added == true);*/
							boolean prdtAdded = clickaddtoBag(prdtval, pdpPageATBBtn);
							if(prdtAdded == true)
							{
								Pass("The Product is added to the Shopping Cart.");
								added = true;
							}
							else
							{
								Fail("The Product is not added to the Shopping Cart.");
							}
						}
					}
					else
					{
						Fail("The Shopping Bag is not displayed.");
					}
					
					if(added == true)
					{
						lodMin = loadMiniCart();
						if(lodMin==true)
						{
							if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
							{
								for(int i = 0; i<miniCartAddedItemLists.size();i++)
								{
									String getProductId = getprdtIdentifier(i);
									if(getProductId.equals(addedId))
									{
										prdtIdadded=true;
										log.add("The Found Product id is : " + getProductId);
										break;
									}
									else
									{
										prdtIdadded=false;
										continue;
									}
								}
								
								if(prdtIdadded==true)
								{
									Pass("The Product is added.",log);
									Thread.sleep(100);
								}
								else
								{
									Fail("The added product is not displayed in the Mini-Cart.",log);
								}
							}
							else
							{
								Fail("The Mini - Cart Seems to be empty.The added product is nod dispalyed.");
							}
						}
						else
						{
							Fail( "The User failed to load the Mini Cart.");
						}
					}
					else
					{
						Fail("The Product is not added.Please Check.");
					}
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 1578 Issue ." + e.getMessage());
				Exception(" BRM - 1578 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Gift Card was not found or the Card was not added successfully.");
		}
	}
	
	/* BRM - 465 Verify whether "CHECKOUT" button is present after items are shopped in shopping bag page and clicking on it displays a overlay with "Sign in" and "Checkout as Guest" if not signed in already.*/
	public void checkoutBtnNavigation()
	{
		ChildCreation("BRM - 465 Verify whether CHECKOUT button is present after items are shopped in shopping bag page and clicking on it displays a overlay with Sign in and Checkout as Guest if not signed in already.");
		try
		{
			String pgeTitle = BNBasicfeature.getExcelVal("BRM465", sheet, 2);
			String guestButtonCaption = BNBasicfeature.getExcelVal("BRM465", sheet, 4);
			if((cardAdded==true)&&(lodMin==true))
			{
				if(BNBasicfeature.isListElementPresent(mcContinueToCheckOutBtn))
				{
					Pass("The Secure Checkout Button is displayed.");
					jsclick(mcContinueToCheckOutBtn.get(0));
					Thread.sleep(500);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageFrame));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(pgetitle))
					{
						Pass("The Page title is displayed.");
						log.add("The Expected page title was : " + pgeTitle);
						log.add("The Actual title is : " + pgetitle.getText());
						if(pgeTitle.contains(pgetitle.getText()))
						{
							Pass("The Page title matches the expected value and the user is navigated to the Sign In Or Checkout as Guest page.",log);
						}
						else
						{
							Fail("There is some mismatch in the expected value.",log);
						}
						
						if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
						{
							log.add("The Checkout as Guest button is displayed.");
							log.add("The Expected button caption was : " + guestButtonCaption);
							log.add("The Actual button caption is : " + checkoutAsGuestbtn.getText());
							if(guestButtonCaption.contains(checkoutAsGuestbtn.getText()))
							{
								Pass("There button caption matches the expected content.",log);
							}
							else
							{
								Fail("There is mismatch in the expected button caption.",log);
							}
						}
						else
						{
							Fail("The Checkout as Guest button is not displayed.");
						}
					}
					else
					{
						Fail("The user is not navigated to the Sign In Page.",log);
					}
					driver.navigate().back();
					wait.until(ExpectedConditions.visibilityOf(header));
				}
				else
				{
					Fail("The Secure Checkout Button is not displayed.");
				}
			}
			else
			{
				Skip("The Gift Card was not added successfully.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 465 Issue ." + e.getMessage());
			Exception(" BRM - 465 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2188 Verify that on selecting the "+" in the Track accordian,the track section should be expanded*/
	public void pdpPageProductMusicTrackAccordian()
	{
		ChildCreation(" BRM - 2188 Verify that Overview accordian is displayed as per the creative.");
		accName = "";
		boolean AccordionFound = false;
		try
		{
			//wait.until(ExpectedConditions.visibilityOf(promoCarousel));
			driver.get(BNConstants.mobileSiteURL);
			wait.until(ExpectedConditions.visibilityOf(header));
			/*String cellVal = BNBasicfeature.getExcelVal("BRM2188", sheet, 3);
			searchProduct(sheet, false, "BRM2188");
			Thread.sleep(1000);
			musicPdp = selectProduct("Stages");*/
			
			String ean = BNBasicfeature.getExcelVal("BRM2188", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isElementPresent(pdpPageDropDownProductType))
					 {
						if(BNBasicfeature.isListElementPresent(pdpPageProductDDList))
						{
							for(int lstSize = 0; lstSize<pdpPageProductDDList.size();lstSize++)
							{
								String defSel = pdpPageProductDDList.get(lstSize).getText();
								if(defSel.contains("CD"))
								 {
									 musicPdp = true;
									 break;
								 }
								 else
								 {
									 musicPdp = false;
									 continue;
								 }
							}
							
							if(musicPdp==true)
							{
								musicPdp = true;
								break;
							}
							else
							{
								musicPdp = false;
								continue;
							}
						}
						else
						{
							 musicPdp = false;
							 continue;
						}
					 }
					 else
					 {
						 musicPdp = false;
						 continue;
					 }
				 }
			}
			
			if(musicPdp == true)
			{
				if(BNBasicfeature.isElementPresent(pdpPageSignInInstantPurchase))
				{
					BNBasicfeature.scrolldown(pdpPageSignInInstantPurchase, driver);
				}
				
				String getAccordionName = BNBasicfeature.getExcelVal("BRM2188", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				if(AccordionFound==true)
				{
					log.add("The Expected Accordion : " + getAccordionName);
					log.add("The Actual Accordion : " + accName);
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
					WebElement ele = pdpPageAccordionLink.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to expand the " + accName + " Accordion is found");
						jsclick(ele);
						WebElement contExpan = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contExpan, "style", "block"));
						//WebElement contExpan = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						String contExpandet = contExpan.getAttribute("style");
						if(contExpandet.contains("display: block;"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is displayed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.",log);
						}
					}
					else
					{
						Fail("The Link to expand the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Track Accordion is not found.",log);
				}
			}
			else
			{
				Fail("The Product is not found.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2188 Issue ." + e.getMessage());
			Exception(" BRM - 2188 Issue." + e.getMessage());
		}
	}
	
	/* BRM 2190 - Verify that on selecting the "Show More" option in the track section,it should expand the track details.*/
	public void pdpPageProductMusicTrackAccordianShowMore()
	{
		ChildCreation(" BRM - 2190 Verify that on selecting the Show More option in the track section,it should expand the track details.");
		boolean AccordionFound = false;
		if(musicPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2190", sheet, 4);
				String[] Val = cellVal.split("\n");
				AccordionFound = getAccordian(Val[0]);
				if(AccordionFound==true)
				{
					Pass("The Track Accordian is found.");
					if(BNBasicfeature.isElementPresent(pdpPageProductReadMore))
					{
						log.add("The Read More link in the Track Accordian Container is found and it is displayed.");
						log.add("The Expected Label for Read More : " + Val[1]);
						log.add("The Actual Label : " + pdpPageProductReadMore.getText());
						if(Val[1].contains(pdpPageProductReadMore.getText()))
						{
							Pass("The Read More Label Matches the Expected Content.",log);
						}
						else
						{
							Fail("The Read More Label does not match the expected content",log);
						}
						
						jsclick(pdpPageProductReadMore);
						boolean shMore = false;
						WebDriverWait w1 = new WebDriverWait(driver, 3);
						try
						{
							w1.until(ExpectedConditions.visibilityOf(pdpPageProductReadLess.get(accSel-1)));
							//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='exShow']//span)["+j+"]")));
							shMore = true;
						}
						catch(Exception e)
						{
							shMore = false;
						}
						if(shMore==true)
						{
							//WebElement ele = driver.findElement(By.xpath("(//*[@class='exShow']//span)["+j+"]"));
							WebElement ele = pdpPageProductReadLess.get(accSel-1);
							if(BNBasicfeature.isElementPresent(ele))
							{
								log.add("The Show Less link is found.");
								BNBasicfeature.scrolldown(ele, driver);
								log.add("The Expected Content : " + Val[2]);
								log.add("The Actual Content : " + ele.getText());
								if(Val[2].contains(ele.getText()))
								{
									Pass("The Show Less link is displayed and it mathces the expected content.",log);
								}
								else
								{
									Fail("The Show Less link is displayed and it does not mathces the expected content.",log);
								}
							}
							else
							{
								Fail("The Show Less Link is not found.");
							}
						}
						else
						{
							Fail( "The Show More is not displayed for the Selected Product.");
						}
					}
					else
					{
						Fail("The Read More link is not found.");
					}
				}
				else
				{
					Fail("The Track Accordian is not found.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2190 Issue ." + e.getMessage());
				Exception(" BRM - 2190 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Music PDP page was not found.");
		}
	}
	
	/* BRM - 2191 Verify that on clicking the "close" icon in the "Track" section,it should collapse the track details of the product*/
	public void pdpPageProductMusicTrackAccordianCollapse()
	{
		ChildCreation(" BRM - 2191 Verify that on clicking the Close icon in the Track section,it should collapse the track details of the product.");
		accName = "";
		boolean AccordionFound = false;
		if(musicPdp==true)
		{
			try
			{
				String getAccordionName = BNBasicfeature.getExcelVal("BRM2188", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				
				if(BNBasicfeature.isElementPresent(pdpPageExpandedAccordion))
				{
					log.add("The Accordion " + accName + " is expanded / opened");
					AccordionFound = true;
				}
				else
				{
					log.add("The Accordion " + accName + " is not expanded / opened");
					AccordionFound = false;
				}
				
				if(AccordionFound == true)
				{
					//System.out.println(accName);
					//System.out.println(j);
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
					WebElement ele = pdpPageAccordionList.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to collapse the " + accName + " Accordion is found");
						jsclick(ele);
						//ele.click();
						WebElement contCollapse = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contCollapse, "style", "none"));
						//Thread.sleep(1500);
						//WebElement contCollapse = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						String contExpandet = contCollapse.getAttribute("style");
						if(contExpandet.contains("none"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is closed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.", log);
						}
					}
					else
					{
						Fail("The Link to collapse the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Accordion is not found on the Product Page / it is not in opened state.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2191 Issue ." + e.getMessage());
				Exception(" BRM - 2191 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Music PDP page was not found.");
		}
	}
	
	/* BRM - 2192 Verify that clicking on related subjects link buttons in the Related subjects accordian should redirect to the respective page*/
	public void pdpPageProductRelatedSubjectAccordian()
	{
		ChildCreation(" BRM - 2192 Verify that clicking on related subjects link buttons in the Related subjects accordian should redirect to the respective page.");
		accName = "";
		boolean AccordionFound = false;
		if(musicPdp==true)
		{
			try
			{
				String getAccordionName = BNBasicfeature.getExcelVal("BRM2192", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
					
				if(AccordionFound==true)
				{
					///System.out.println(accName);
					//System.out.println(j);
					log.add("The Expected Accordion : " + getAccordionName);
					log.add("The Actual Accordion : " + accName);
					//WebElement contExpan = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
					WebElement contExpan = pdpPageAccExp.get(accSel-1);
					String contExpandet = contExpan.getAttribute("style");
					if(contExpandet.contains("none"))
					{
						//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
						WebElement ele = pdpPageAccordionLink.get(accSel-1);
						BNBasicfeature.scrolldown(ele, driver);
						jsclick(ele);
						Thread.sleep(1000);
					}
					wait.until(ExpectedConditions.attributeContains(pdpPageAccExp.get(accSel-1), "style", "block"));
					contExpandet = contExpan.getAttribute("style");
					if(contExpandet.contains("display: block;"))
					{
						if (BNBasicfeature.isListElementPresent(pdpPageProductRelatedSubjectList))
						{
							for(WebElement sub : pdpPageProductRelatedSubjectList)
							{
								log.add("The Related Product Details Contents : " + sub.getText());
								//System.out.println(sub.getText());
							}
							//List<WebElement> lstele = driver.findElements(By.xpath("(//*[@class='sk_relasubj']//a)"));
							Random r = new Random();
							int sel = r.nextInt(pdpPageProductRelatedSubjectList.size());
							if(sel<1)
							{
								sel = 1;
							}
							//WebElement ele = driver.findElement(By.xpath("(//*[@class='sk_relasubj']//a)[1]"));
							//WebElement ele = driver.findElement(By.xpath("(//*[@class='sk_relasubj']//a)["+sel+"]"));
							WebElement ele = pdpPageProductRelatedSubjectList.get(sel-1);
							String prdtfiltername = ele.getText();
							log.add("The Product filtered details to view is : " + prdtfiltername );
							jsclick(ele);
							Thread.sleep(1000);
							
							//wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@class='productlistTab']//*[@class='searchterm']")));
							wait.until(ExpectedConditions.visibilityOf(searchPageSearchTerm));
							//String filterRec = driver.findElement(By.xpath("//*[@class='productlistTab']//*[@class='searchterm']")).getText();
							String filterRec = searchPageSearchTerm.getText();
							log.add("The Product filtered details to in the Search Page  is : " + filterRec );
							if(filterRec.contains(prdtfiltername))
							{
								Pass("The " + prdtfiltername + " details is clicked and the results is displayed successfully.", log);
							}
							else
							{
								Fail("The " + prdtfiltername + " details is clicked and the results is not filtered as expected.", log);
							}
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is displayed but the list seems to be empty.",log);
						}
					}
					else
					{
						Fail("The " + accName + " Accordion is clicked and the container is not displayed.",log);
					}
				}
				else
				{
					Fail("The Accordion is not found",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2192 Issue ." + e.getMessage());
				Exception(" BRM - 2192 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Music PDP page was not found.");
		}
	}
	
	/* BRM - 2152 Verify that "Celebrity Cookbooks" are displayed below the "Get it same day" in book PDP page*/
	public void pdpPageCookBookPromoBadge()
	{
		ChildCreation("BRM - 2152 Verify that Celebrity Cookbooks are displayed below the Get it same day in book PDP page.");
		try
		{
			/*String name = BNBasicfeature.getExcelVal("BRM2154", sheet, 4);
			searchProduct(sheet, false, "BRM2154");
			cbPdp = selectProduct("Cooking");*/
			
			String ean = BNBasicfeature.getExcelVal("BRM2154", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isListElementPresent(pdpPageProductPromoMsgList))
					 {
						 cbPdp = true;
						 break;
					 }
					 else
					 {
						 cbPdp = false;
						 continue;
					 }
				 }
			}
			if(cbPdp == true)
			{
				wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
				if(BNBasicfeature.isListElementPresent(pdpPageProductPromoMsgList)&&(BNBasicfeature.isListElementPresent(pdpPageProductPromoLinkList)))
				{
					//List<WebElement> promoList = driver.findElements(By.xpath("//*[@class='sk_promoMsg_lst']//li"));
					List<WebElement> promoList = pdpPageProductPromoMsgList;
					log.add("The Product Promo List details is displayed for the Product.");
					for(int i = 0;i<promoList.size();i++)
					{
						BNBasicfeature.scrolldown(promoList.get(i), driver);
						//WebElement proLink = driver.findElement(By.xpath("(//*[@class='sk_promoMsg_lst']//a)["+(i+1)+"]"));
						WebElement proLink = pdpPageProductPromoLinkList.get(i);
						int response = BNBasicfeature.linkBroken(proLink, log);
						String promoUrl = proLink.getAttribute("href");
						//System.out.println(promoUrl);
						log.add("The Promo URL is : " + promoUrl);
						
						if(response==200)
						{
							Pass("The link is valid.",log);
						}
						else
						{
							Fail("The link is not valid.",log);
						}
					}
				}
				else
				{
					Fail("The Promo Message List is not found.");
				}
			}
			else
			{
				Fail("The Book is not found in the PLP Page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2152 Issue ." + e.getMessage());
			Exception(" BRM - 2152 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2154 Verify that on selecting "Celebrity Cookbooks" should redirect to the respective page in book PDP page.*/
	public void pdpPageProductPromoMsg()
	{
		ChildCreation(" BRM - 2154 Verify that on selecting Celebrity Cookbooks should redirect to the respective page in book PDP page.");
		if(cbPdp==true)
		{
			try
			{
				if((BNBasicfeature.isListElementPresent(pdpPageProductPromoMsgList))&&(BNBasicfeature.isListElementPresent(pdpPageProductPromoLinkList)))
				{
					//List<WebElement> promoList = driver.findElements(By.xpath("//*[@class='sk_promoMsg_lst']//li"));
					List<WebElement> promoList = pdpPageProductPromoMsgList;
					log.add("The Product Promo List details is displayed for the Product.");
					for(int i = 1;i<=promoList.size();i++)
					{
						//WebElement proLink = driver.findElement(By.xpath("(//*[@class='sk_promoMsg_lst']//a)["+i+"]"));
						WebElement proLink = pdpPageProductPromoLinkList.get(i-1);
						if(bookpromoBadge==false)
						{
							String promoTitle = proLink.getText();
							if(promoTitle.contains("Books"))
							{
								bookpromoBadge  = true;
							}
						}
						int response = BNBasicfeature.linkBroken(proLink, log);
						String promoUrl = proLink.getAttribute("href");
						//System.out.println(promoUrl);
						log.add("The Promo URL is : " + promoUrl);
						if(response==200)
						{
							String prevUrl = driver.getCurrentUrl();
							//Thread.sleep(2000);
							jsclick(proLink);
							//Thread.sleep(2000);
							wait.until(ExpectedConditions.visibilityOf(header));
							//wait.until(ExpectedConditions.visibilityOf(prdtBagCount));
							Thread.sleep(500);
							String currUrl = driver.getCurrentUrl();
							log.add("The Promo Link URL is : " + currUrl);
							//System.out.println(currUrl);
							if((currUrl.contains(promoUrl))||(!prevUrl.contains(currUrl)))
							{
								Pass("The User is navigated to the Current URL.",log);
								driver.navigate().back();
							}
							else
							{
								Fail("The User is not navigated to the Current URL.",log);
								driver.navigate().back();
							}
						}
						else
						{
							Fail("The link is not valid.");
						}
					}
				}
				else
				{
					Fail("The Promo Message List is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2154 Issue ." + e.getMessage());
				Exception(" BRM - 2154 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested Cookbook was not found.");
		}
	}
	
	/* BRM - 2156 Verify that "Books We're Talking About" should be displayed below the "Back to School" in book PDP page*/
	public void pdpPageProductBookPromoMsg()
	{
		ChildCreation("BRM - 2156 Verify that Books We're Talking About should be displayed below the Back to School in book PDP page");
		cbPdp = false;
		try
		{
			/*String name = BNBasicfeature.getExcelVal("BRM2156", sheet, 4);
			searchProduct(sheet, false, "BRM2156");
			mvfound = selectProduct("Delicious");*/
			if(bookpromoBadge==false)
			{
				String ean = BNBasicfeature.getExcelVal("BRM2156", sheet, 8);
				String cellVal = ean;
				String[] eanNum = cellVal.split("\n");
				boolean bkfound = false;
				for(int i = 0; i<eanNum.length;i++)
				{
					bkfound = searchEanNumbers(eanNum[i]);
					 if(bkfound == true)
					 {
						 if(BNBasicfeature.isListElementPresent(pdpPageProductPromoMsgList))
						 {
							 cbPdp = true;
							 break;
						 }
						 else
						 {
							 cbPdp = false;
							 continue;
						 }
					 }
				}
				if(cbPdp == true)
				{
					wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
					if((BNBasicfeature.isListElementPresent(pdpPageProductPromoMsgList))&&(BNBasicfeature.isListElementPresent(pdpPageProductPromoLinkList)))
					{
						List<WebElement> promoList = pdpPageProductPromoMsgList;
						//List<WebElement> promoList = driver.findElements(By.xpath("//*[@class='sk_promoMsg_lst']//li"));
						log.add("The Product Promo List details is displayed for the Product.");
						for(int i = 1;i<=promoList.size();i++)
						{
							//WebElement proLink = driver.findElement(By.xpath("(//*[@class='sk_promoMsg_lst']//a)["+i+"]"));
							WebElement proLink = pdpPageProductPromoLinkList.get(i-1);
							int response = BNBasicfeature.linkBroken(proLink, log);
							String promoUrl = proLink.getAttribute("href");
							System.out.println(promoUrl);
							log.add("The Promo URL is : " + promoUrl);
							if(response==200)
							{
								String prevUrl = driver.getCurrentUrl();
								jsclick(proLink);
								Thread.sleep(2000);
								wait.until(ExpectedConditions.visibilityOf(header));
								Thread.sleep(2000);
								//wait.until(ExpectedConditions.visibilityOf(prdtBagCount));
								String currUrl = driver.getCurrentUrl();
								log.add("The Promo Link URL is : " + currUrl);
								System.out.println(currUrl);
								if((currUrl.contains(promoUrl))||(!prevUrl.contains(currUrl)))
								{
									Pass("The User is navigated to the Current URL.",log);
									driver.navigate().back();
								}
								else
								{
									Fail("The User is not navigated to the Current URL.",log);
									driver.navigate().back();
								}
							}
							else
							{
								Fail("The link is not valid.");
							}
						}
					}
					else
					{
						Fail("The Promo Message List is not found.");
					}
				}
				else
				{
					Fail("The Book is not found in the PLP Page.",log);
				}
			}
			else
			{
				Pass( " The Book Promo Badge is found for the Link.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2156 Issue ." + e.getMessage());
			Exception(" BRM - 2156 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1517 Verify that Director and Cast details should be displayed below the product title in the Movie & TV PDP page*/
	public void pdpPageMovieProductDetails()
	{
		ChildCreation(" BRM - 1517 Verify that Director and Cast details should be displayed below the product title in the Movie & TV PDP page.");
		try
		{
			//String name = BNBasicfeature.getExcelVal("BRM1517", sheet, 4);
			//searchProduct(sheet, false, "BRM1517");
			//mvfound = selectProduct(name);
			String ean = BNBasicfeature.getExcelVal("BRM1517", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isListElementPresent(pdpPageMovieDirectorDetails))
					 {
						 moviePdp = true;
						 break;
					 }
					 else
					 {
						 moviePdp = false;
						 continue;
					 }
				 }
			}
			if(moviePdp == true)
			{
				if(BNBasicfeature.isListElementPresent(pdpPageMovieDirectorDetails))
				{
					log.add("The Director List details is displayed for the Selected Movie");
					for(WebElement dirname : pdpPageMovieDirectorDetails)
					{
						if(dirname.getText().isEmpty())
						{
							Fail("The Director Details seems to be empty.");
						}
						else
						{
							log.add("The Director Name for the Selected Movie : " + dirname.getText());
						}
					}
					Pass("The Director Name for the selcted Movie are displayed. ",log);
				}
				else
				{
					Fail("The Director Detais for the Movie is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(pdpPageMovieCastDetails))
				{
					log.add("The Cast List details is displayed for the Selected Movie");
					for(WebElement castname : pdpPageMovieDirectorDetails)
					{
						if(castname.getText().isEmpty())
						{
							Fail("The Cast Details seems to be empty.");
						}
						else
						{
							log.add("The Director Name for the Selected Movie : " + castname.getText());
						}
					}
					Pass("The Cast Name for the selcted Movie are displayed. ",log);
				}
				else
				{
					Fail("The Cast Detais for the Movie is not found.");
				}
			}
			else
			{
				Fail("The movie is not found in the PLP Page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1517 Issue ." + e.getMessage());
			Exception(" BRM - 1517 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2150 Verify that "Want it today? Check store availability" is displayed below the Marketplace section in Book PDP*/
	public void pdpPageProductBookPickUpInStore()
	{
		ChildCreation(" BRM - 2150 Verify that Want it today? Check store availability is displayed below the Marketplace section in Book PDP");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM2150", sheet, 4);
			String[] Val = cellVal.split("\n");
			if(BNBasicfeature.isElementPresent(pdpPageWantitToday))
			{
				BNBasicfeature.scrolldown(pdpPageWantitToday, driver);
				
				Pass("The Want it today? Check store availability Container is displayed.");
				// Modify and Insert for the Want It Today...
				
				if(BNBasicfeature.isListElementPresent(pdpPagePickUpInStoreLabel))
				{
					for(int i = 0; i<pdpPagePickUpInStoreLabel.size(); i++)
					{
						if(BNBasicfeature.isElementPresent(pdpPagePickUpInStoreLabel.get(i)))
						{
							String txt = pdpPagePickUpInStoreLabel.get(i).getText();
							log.add("The "+ Val[i] +" is displayed.");
							log.add("The Actual Value is : " + txt);
							if(txt.equalsIgnoreCase((Val[i])))
							{
								Pass("The label value match the expected content.",log);
							}
							else
							{
								Fail("The label value does not match the expected content.",log);
							}
						}
						else
						{
							Fail( "The " + Val[0] + "label is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Check Store Availability Label is not displayed.");
				}
			}
			else
			{
				Fail("The Want it today? Check store availability container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2150 Issue ." + e.getMessage());
			Exception(" BRM - 2150 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2151 Verify that on selecting "Want it today? Check store availability" should redirect to the respective page in Book PDP pages*/
	public void pdpPageProductBookPickUpInStoreClick()
	{
		ChildCreation(" BRM - 2151 Verify that on selecting Want it today? Check store availability should redirect to the respective page in Book PDP pages.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM2151", sheet, 4);
			String[] Val = cellVal.split("\n");
			String expColor = BNBasicfeature.getExcelVal("BRM2170", sheet, 3);
			if(BNBasicfeature.isElementPresent(pdpPageWantitToday))
			{
				log.add("The Want it today? Check store availability Container is displayed.");
				Actions act = new Actions(driver);
				act.moveToElement(pdpPageWantitToday).click().build().perform();
				log.add("The Check Availability is clicked.");
				wait.until(ExpectedConditions.attributeContains(pdpPagePickInstoreContainer, "style", "block"));
				//wait.until(ExpectedConditions.attributeContains(pdpPagePickInstorePopUpMask, "style", "display: block;"));
				Thread.sleep(100);
				//if(BNBasicfeature.isElementPresent(pdpPagePickInstorePopUpMask))
				if(BNBasicfeature.isElementPresent(pdpPagePickInstoreContainer))
				{
					if(pdpPagePickInstoreContainer.getAttribute("style").contains("block"))
					{
						Pass("The Pick Up In Store Pop Up is displayed.");
						if(BNBasicfeature.isElementPresent(pdpPagePickInstorePickUpTitle))
						{
							String tit = pdpPagePickInstorePickUpTitle.getText();
							log.add("The Expected title was : " + Val[0]);
							log.add("The Actual title is : " + tit);
							if(Val[0].contains(tit))
							{
								Pass("The Pick Up Title is displayed." + pdpPagePickInstorePickUpTitle.getText());
							}
							else
							{
								Fail("There is some mismatch in the expected and the displayed valuye.",log);
							}
							
							if(BNBasicfeature.isElementPresent(pdpPagePickInstorePickUpDesc))
							{
								if(pdpPagePickInstorePickUpDesc.getText().isEmpty())
								{
									Fail("The Pick Up In Store Desc is not displayed.");
								}
								else
								{
									Pass("The Pick Up Title is displayed." + pdpPagePickInstorePickUpDesc.getText());
								}
								
								if(BNBasicfeature.isElementPresent(pdpPageNookDevicePickUpZipCode))
								{
									Pass("The Pick Up Store Zipcode is displayed.");
									log.add("The Expected Placeholder value is : "  + Val[1]);
									String placeholder = pdpPageNookDevicePickUpZipCode.getAttribute("placeholder");
									log.add("The actual placeholder value is : " + placeholder);
									if(Val[1].contains(placeholder))
									{
										Pass("The Placeholder value matches for the Zip Code field.");
									}
									else
									{
										Fail("The Placeholder value mismatches for the Zip Code field.");
									}
								}
								else
								{
									Fail("The Pick Up Store Zipcode is not displayed.");
								}
								
								if(BNBasicfeature.isElementPresent(pdpPagePickUpFindStoreBtn))
								{
									Pass("The Pick Up Store Find button is displayed.");
									String btnColor = pdpPagePickUpFindStoreBtn.getCssValue("background-color");
									//System.out.println(btnColor);
									
									Color colorhxcnvt = Color.fromString(btnColor);
									String hexCode = colorhxcnvt.asHex();
									//System.out.println(hexCode);
									log.add("Expected Color : " + expColor);
									log.add("Actual Color : " + hexCode);
									if(expColor.contains(hexCode))
									{
										Pass("The Find In Store button is matched with the expected Color.", log);
									}
									else
									{
										Fail("The The Find In Store button is not matched with the expected Color.");
									}
								}
								else
								{
									Fail("The Pick Up Store Find button is not displayed.");
								}
							}
							else
							{
								Fail("The Pick Up Desc is not displayed.");
							}
						}
						else
						{
							Fail("The Pick Up Title is not displayed.");
						}
					}
					else
					{
						Fail("The Pick Up In Store Pop Up is not displayed",log);
					}
				}
				else
				{
					Fail("The Pick Up In Store Pop Up is not displayed.",log);
				}
				
				if(BNBasicfeature.isElementPresent(pdpPagePickInstorePopUpClose))
				{
					Pass("The Find In Store POP Up Close Button is displayed.");
					jsclick(pdpPagePickInstorePopUpClose);
					
					wait.until(ExpectedConditions.attributeContains(pdpPagePickInstoreContainer, "style", "display: none;"));
					//wait.until(ExpectedConditions.attributeContains(pdpPagePickInstorePopUpMask, "style", "display: none;"));
					Thread.sleep(100);
					boolean closed = pdpPagePickInstoreContainer.getAttribute("style").contains("display: none;");
					if(closed==true)
					{
						Pass("The Find In Store POP Up Close Button is clicked and the Pop Up is closed.");
					}
					else
					{
						Fail("The Find In Store POP Up Close Button is clicked and the Pop Up is not closed.");
					}
					
				}
				else
				{
					Fail("The Find In Store POP Up Close Button is not displayed.");
				}
			}
			else
			{
				Fail("The Want it today? Check store availability container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2151 Issue ." + e.getMessage());
			Exception(" BRM - 2151 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1518 Verify that "Format: Blu-ray" drop down should be displayed below the Add to Wishlist and it should be selected as default in the Movie & TV PDP page*/
	public void pdpPageMovieProductDropDown()
	{
		ChildCreation(" BRM - 1518 Verify that Format: Blu-ray drop down should be displayed below the Add to Wishlist and it should be selected as default in the Movie & TV PDP page.");
		if(moviePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1518", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageProductDropDown)&&(BNBasicfeature.isListElementPresent(pdpPageProductTypes)))
				{
					BNBasicfeature.scrolldown(pdpPageProductDropDown, driver);
					log.add("The Product type Format drop down is displayed.");
					/*Select sel = new Select(pdpPageProductDropDown);
					String formatOption = sel.getFirstSelectedOption().getText();*/
					List<WebElement> ele = pdpPageProductTypes;
					String formatOption = ele.get(0).getText();
					log.add("The Expected Format Option was : " + cellVal);
					log.add("The Actual Format Option is : " + formatOption);
					if(formatOption.equals(cellVal))
					{
						Pass("The Format Option Matches the expected content.",log);
					}
					else
					{
						Fail("The Format Option is not displayed or there is some mismatch.",log);
					}
				}
				else
				{
					Fail("The Product Type Format Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1518 Issue ." + e.getMessage());
				Exception(" BRM - 1518 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Requested Movie was not found.");
		}
	}
	
	/* BRM - 1519 Verify that the drop down has "Blu-ray" and "DVD" options in the Movie & TV PDP page*/
	public void pdpPageMovieProductDropDownDetails()
	{
		ChildCreation(" BRM - 1519 Verify that the drop down has Blu-ray and DVD options in the Movie & TV PDP page.");
		if(moviePdp==true)
		{
			String formatType = "";
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1519", sheet, 4);
				String[] Val = cellVal.split("\n");
				//System.out.println(Val.length);
				if(BNBasicfeature.isListElementPresent(pdpPageProductTypes))
				{
					log.add("The Product type Format drop down is displayed.");
					/*Select sel = new Select(pdpPageProductDropDown);
					int formatOptionSize = sel.getOptions().size();*/
					List<WebElement> ele = pdpPageProductTypes;
					int formatOptionSize = ele.size();
					for(int i = 0;i<formatOptionSize;i++)
					{
						log.add("The Expected Format Option was : " + Val[i]);
						boolean formatFound = false;
						int cnt = 0;
						do
						{
							formatType = ele.get(i).getText();
							log.add("The Actual Format Option was : " + formatType);
							//System.out.println(Val[cnt]);
							if(formatType.contains(Val[cnt]))
							{
								formatFound = true;
								break;
							}
							else
							{
								formatFound = false;
								cnt++;
								continue;
							}
						}while(cnt<Val.length);
						
						if(formatFound==true)
						{
							Pass("The Expected Format is found.",log);
						}
						else
						{
							Fail("The Expected Format is not found.",log);
						}
					}
				}
				else
				{
					Fail("The Product Type Format Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1519 Issue ." + e.getMessage());
				Exception(" BRM - 1519 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Requested Movie was not found.");
		}
	}
	
	/* BRM - 1520 Verify that Blu-ray, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed below the Format drop down in the Movie & TV PDP page*/
	public void pdpPageMovieProductBluRayPriceDetails()
	{
		ChildCreation(" BRM - 1520 Verify that Blu-ray, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed below the Format drop down in the Movie & TV PDP page.");
		if(moviePdp==true)
		{
			try
			{
				//if(BNBasicfeature.isElementPresent(pdpPageProductType))
				if(BNBasicfeature.isElementPresent(pdpPageProductDropDown))
				{
					//System.out.println(pdpPageProductType.getText());
					//log.add("The Product Type is displayed for the selected product. The Selected Product type is : " + pdpPageProductType.getText());
					if(BNBasicfeature.isListElementPresent(pdpPageSalePriceList))
					{
						for(int i = 0; i<pdpPageSalePriceList.size(); i++)
						{
							String movieSalePrice = pdpPageSalePriceList.get(i).getText();
							if(movieSalePrice.isEmpty())
							{
								Fail("The Movie Sale Price is empty.");
							}
							else
							{
								log.add("The Movie Sale Price is displayed.The displayed price is : " + movieSalePrice);
								Pass("The Movie Sale Price is displayed for the Selected product.",log);
							}
						}
						
						if(BNBasicfeature.isListElementPresent(pdpPageActualPriceList))
						{
							for(int i = 0; i<pdpPageSalePriceList.size(); i++)
							{
								String movieactualPrice = pdpPageActualPriceList.get(i).getText();
								if(movieactualPrice.isEmpty())
								{
									Fail("The Movie Actual Price is empty.");
								}
								else
								{
									log.add("The Movie Original Price is displayed. The Original Price displayed was : " + movieactualPrice);
									Pass("The Movie Original Price is displayed for the Selected product.",log);
								}
							}
						}
						else
						{
							Fail("The Orignial Price is not displayed.",log);
						}
						
						if(BNBasicfeature.isListElementPresent(pdpPageActualSavePercent))
						{
							for(int i = 0; i<pdpPageActualSavePercent.size(); i++)
							{
								String movieSavePercent = pdpPageActualSavePercent.get(i).getText();
								if(movieSavePercent.isEmpty())
								{
									Fail("The Movie Actual Price is empty.");
								}
								else
								{
									log.add("The Save Percent is displayed. The Save Percent for the Selcted Product is : " + movieSavePercent);
									Pass("The Product type,Sale Price, Actual Price, and Save Percent is displayed for the Selected product.",log);
								}
							}
						}
						else
						{
							Fail("The Save Percent is not displayed for the selected movie product.",log);
						}
					}
					else
					{
						Fail("The Sale price is not displayed.");
					}
				}
				else
				{
					Fail("The Product Format text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1520 Issue ." + e.getMessage());
				Exception(" BRM - 1520 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Requested Movie was not found.");
		}
	}
	
	/* BRM - 1716 Verify that while selecting the "+" in the "Cast and Crew" accordian,the related section should be expanded*/
	public void pdpPageMovieCastCrewAccordianExpand()
	{
		ChildCreation(" BRM - 1716 Verify that while selecting the + in the Editorial Reviews Accordion,it should expand expand the editorial reviews of the product");
		if(moviePdp==true)
		{
			String accName = "";
			boolean AccordionFound = false;
			try
			{
				//BNBasicfeature.scrolldown(pdpPageInstantPurchase, driver);
				String getAccordionName = BNBasicfeature.getExcelVal("BRM1716", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				
				if(AccordionFound==true)
				{
					//System.out.println(accName);
					//System.out.println(j);
					log.add("The Expected Accordion : " + getAccordionName);
					log.add("The Actual Accordion : " + accName);
					//WebElement ele = driver.findElement(By.xpath("(//*[@class='descriptionEle pdp_DeatilsItem']//*[@class='pdp_DetailsLink'])["+j+"]"));
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+(accSel-1)+"]"));
					WebElement ele = pdpPageAccordionLink.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to expand the " + accName + " Accordion is found");
						jsclick(ele);
						WebElement contExpan = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contExpan, "style", "block"));
						String contExpandet = contExpan.getAttribute("style");
						if(contExpandet.contains("display: block;"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is displayed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.",log);
						}
						
						jsclick(ele);
						WebElement contCollapse = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contCollapse, "style", "none"));
						//Thread.sleep(1500);
						//WebElement contCollapse = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						contExpandet = contCollapse.getAttribute("style");
						if(contExpandet.contains("none"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is closed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.", log);
						}
					}
					else
					{
						Fail("The Link to expand the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Accordion is not found",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1716 Issue ." + e.getMessage());
				Exception(" BRM - 1716 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The Requested Movie was not found.");
		}
	}
	
	/* BRM - 1521 Verify that publisher name should be displayed below the product title in the Digital Magazines PDP page */
	public void pdpPageMagazineProduct()
	{
		ChildCreation(" BRM - 1521 Verify that publisher name should be displayed below the product title in the Digital Magazines PDP page.");
		try
		{
			/*String name = BNBasicfeature.getExcelVal("BRM1521", sheet, 4);
			searchProduct(sheet, false, "BRM1521");
			nookMagazinePdp = selectProduct(name);*/
			String ean = BNBasicfeature.getExcelVal("BRM1521", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isListElementPresent(pdpPageMovieDirectorDetails))
					 {
						 nookMagazinePdp = true;
						 break;
					 }
					 else
					 {
						 nookMagazinePdp = false;
						 continue;
					 }
				 }
			}
			if(nookMagazinePdp == true)
			{
				if(BNBasicfeature.isListElementPresent(pdpPageMovieDirectorDetails))
				{
					log.add("The Publisher name details is displayed for the Selected Magazine.");
					for(WebElement publisher : pdpPageMovieDirectorDetails)
					{
						System.out.println(publisher.getText());
						if(publisher.getText().isEmpty())
						{
							Fail("The Publisher name is empty.");
						}
						else
						{
							log.add("The Publisher Name for the Selected Magazine : " + publisher.getText());
						}
					}
					Pass("The Publisher Name for the selcted digital magazine are displayed. ",log);
				}
				else
				{
					Fail("The Digital Magazine publisher details is not found.");
				}
			}
			else
			{
				Fail("The magazine is not found in the PLP Page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1521 Issue ." + e.getMessage());
			Exception(" BRM - 1521 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1522 Verify that Current issue section should be displayed below the Add to Wishlist in the Digital Magazines PDP page.*/
	public void pdpPageMagazineCurrentIssue()
	{
		ChildCreation("BRM - 1522 Verify that Current issue section should be displayed below the Add to Wishlist in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1522", sheet, 4);
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					log.add("The Expected content was : " + cellVal);
					for(int i = 0 ; i<pdpPageBuyTypeList.size(); i++)
					{
						String txt = pdpPageBuyTypeList.get(i).getText();
						log.add("The Actual content is : " + txt);
						if(txt.contains(cellVal))
						{
							Pass("The Current Issue is displayed for the Product and the value matches.",log);
						}
						else
						{
							Fail("The Value does not match the expected content.",log);
						}
					}
				}
				else
				{
					Fail("The Current Issue is not dispalyed for the Products.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1522 Issue ." + e.getMessage());
				Exception(" BRM - 1522 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1523 Verify that "Try it FREE for 14 days Learn more" should be displayed below the Current issue in the Digital Magazines PDP page*/
	public void pdpPageMagazineProduct14DaysTrial()
	{
		ChildCreation(" BRM - 1523 Verify that Try it FREE for 14 days Learn more should be displayed below the Current issue in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1523", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageTrialMsg))
				{
					log.add("The Try it Free for 14 Days message is displayed.");
					log.add("The Expected Val : " + cellVal);
					String actualVal = pdpPageTrialMsg.getText();
					//System.out.println(actualVal);
					log.add("The Actual Val : " + pdpPageTrialMsg.getText());
					if(actualVal.contains(cellVal))
					{
						Pass("The Content matches the expected value.",log);
					}
					else
					{
						Fail("The content does not match the expected value.",log);
					}
				}
				else
				{
					Fail("The Message is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1523 Issue ." + e.getMessage());
				Exception(" BRM - 1523 Issue." + e.getMessage());
			}	
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1524 Verify that while selecting the "Learn more",it should displays a overlay with Free 14 day free trail description in the Digital Magazines PDP page*/
	public void pdpPageMagazineProduct14DaysTrialDetails()
	{
		ChildCreation(" BRM - 1524 Verify that while selecting the Learn more,it should displays a overlay with Free 14 day free trail description in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageTrialMsgLearnMore))
				{
					log.add("The Learn More link is found and it is displayed.");
					//Actions act = new Actions(driver);
					jsclick(pdpPageTrialMsgLearnMore);
					//act.moveToElement(pdpPageTrialMsgLearnMore).click().build().perform();
					//pdpPageTrialMsgLearnMore.click();
					wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "block"));
					nookMagazinepopUp = true;
					wait.until(ExpectedConditions.visibilityOf(pdpPagePopUp));
					if(BNBasicfeature.isElementPresent(pdpPagePopUp))
					{
						Pass("The Try it Free for 14 days pop up is displayed.",log);
					}
					else
					{
						Fail("The Try it Free for 14 Days details pop up is not displayed.",log);
					}
				}
				else
				{
					Fail("The Learn More link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1524 Issue ." + e.getMessage());
				Exception(" BRM - 1524 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 2262 PDP Magazine - Free trial - Verify that clicking on "Manage my subscription" should redirect to the respective page*/
	public void pdpPageMagazineProduct14DaysManageSubscription()
	{
		ChildCreation(" BRM - 2262 PDP Magazine - Free trial - Verify that clicking on Manage my subscription should redirect to the respective page.");
		if((nookMagazinePdp==true)&&(nookMagazinepopUp==true))
		{
			String ele="";
			WebElement link=null;
			boolean lnkFound = false;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2262", sheet, 4);
				//String signIntitle = BNBasicfeature.getExcelVal("BRM2262", sheet, 5);
				if(BNBasicfeature.isElementPresent(pdpPagePopUp))
				{
					log.add("The Pop Up is displayed.");
					if(BNBasicfeature.isListElementPresent(pdpPagePopUpDetails))
					{
						Pass("The Pop Up details are displayed.");
						for(int i = 0;i<pdpPagePopUpDetails.size();i++)
						{
							ele = pdpPagePopUpDetails.get(i).getText();
							link = pdpPagePopUpDetails.get(i);
							log.add("The Expected Value is : " + cellVal);
							if(cellVal.contains(ele))
							{
								log.add("The Actual Value was : " + link.getText());
								lnkFound = true;
								break;
							}
							else
							{
								lnkFound = false;
								continue;
							}
						}
						
						if(lnkFound==true)
						{
							Pass("The Expected Link Manage my subscription is found.",log);
							/*String handle = driver.getWindowHandle();
							link.click();
							for(String newWin: driver.getWindowHandles())
							{
								if(!newWin.equals(handle))
								{
									driver.switchTo().window(newWin);
									//driver.switchTo().frame(signInpageframe);
									String title = pgetitle.getText();
									log.add("The Expected title was " + signIntitle);
									if(title.equals(signIntitle))
									{
										log.add("The Actual Title is : " + title);
										Pass("The user is navigated to the Sign In Page when Add to WishList was clicked.", log);
									}
									else
									{
										log.add("The Actual title is " + title);
										Fail("The user was not navigated to the Sign In Page when Add to WishList was clicked.",log);
									}
									driver.close();
								}
							}*/
						}
						else
						{
							Fail("The Expected Link Manage my Subscription is not found. ");
						}
					}
					else
					{
						Fail("The PopUp details are not displayed.");
					}
				}
				else
				{
					Fail("The Pop Up is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2262 Issue ." + e.getMessage());
				Exception(" BRM - 2262 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found or the pop up is not displayed.");
		}
	}
	
	/* BRM - 2263 PDP Magazine - Free trial - Verify that clicking on "Set default payment method" should redirect to the respective page*/
	public void pdpPageMagazineProduct14DaysManageDefaultPayment()
	{
		ChildCreation(" BRM - 2263 PDP Magazine - Free trial - Verify that clicking on Set default payment method should redirect to the respective page.");
		if((nookMagazinePdp==true)&&(nookMagazinepopUp==true))
		{
			String ele="";
			WebElement link=null;
			boolean lnkFound = false;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2263", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPagePopUp))
				{
					log.add("The Pop Up is displayed.");
					if(BNBasicfeature.isListElementPresent(pdpPagePopUpDetails))
					{
						Pass("The Pop Up details are displayed.");
						for(int i = 0;i<pdpPagePopUpDetails.size();i++)
						{
							ele = pdpPagePopUpDetails.get(i).getText();
							link = driver.findElement(By.xpath("//*[contains(@id,'modalContainer')]//p"));
							log.add("The Expected Value is : " + cellVal);
							if(cellVal.contains(ele))
							{
								log.add("The Actual Value was : " + link.getText());
								lnkFound = true;
								break;
							}
							else
							{
								lnkFound = false;
								continue;
							}
						}
						
						if(lnkFound==true)
						{
							Pass("The Expected Link Set Default Payment is found.",log);
							//link.click();
						}
						else
						{
							Fail("The Expected Link Set Default Payment is not found. ");
						}
					}
					else
					{
						Fail("The PopUp details are not displayed.");
					}
				}
				else
				{
					Fail("The Pop Up is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2263 Issue ." + e.getMessage());
				Exception(" BRM - 2263 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found or the pop up is not displayed.");
		}
	}
	
	/* BRM - 2264 PDP Magazine - Free trial - Verify that clicking on "FAQs" should redirect to the respective page*/
	public void pdpPageMagazineProduct14DaysFAQ()
	{
		ChildCreation(" BRM - 2264 PDP Magazine - Free trial - Verify that clicking on FAQs should redirect to the respective page.");
		if(nookMagazinePdp==true)
		{
			String ele="";
			WebElement link=null;
			boolean lnkFound = false;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2264", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPagePopUp))
				{
					log.add("The Pop Up is displayed.");
					if(BNBasicfeature.isListElementPresent(pdpPagePopUpDetails))
					{
						Pass("The Pop Up details are displayed.");
						for(int i = 1;i<=pdpPagePopUpDetails.size();i++)
						{
							link = driver.findElement(By.xpath("//*[contains(@id,'modalContainer')]//p"));
							log.add("The Expected Value is : " + cellVal);
							if(cellVal.contains(ele))
							{
								log.add("The Actual Value was : " + link.getText());
								lnkFound = true;
								break;
							}
							else
							{
								lnkFound = false;
								continue;
							}
						}
						
						if(lnkFound==true)
						{
							Pass("The Expected Link FAQ is found.",log);
							//link.click();
						}
						else
						{
							Fail("The Expected Link FAQ is not found. ");
						}
					}
					else
					{
						Fail("The PopUp details are not displayed.");
					}
				}
				else
				{
					Fail("The Pop Up is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2264 Issue ." + e.getMessage());
				Exception(" BRM - 2264 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found or the pop up is not displayed.");
		}
	}
	
	/* BRM - 2265 PDP Magazine - Free trial - Verify that clicking on close icon in "Free 14-day free trial" overlay should close the overlay*/
	public void pdpPageMagazineProduct14DaysOverlayClose()
	{
		ChildCreation("BRM - 2265 PDP Magazine - Free trial - Verify that clicking on close icon in Free 14-day free trial overlay should close the overlay.");
		if(nookMagazinePdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPagePopUp))
				{
					log.add("The Pop Up is displayed.");
					if(BNBasicfeature.isElementPresent(pdpPagePopUpClose))
					{
						log.add("The Close icon is displayed.");
						jsclick(pdpPagePopUpClose);
						wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "none"));
						if(pdpPagePopUp.getAttribute("style").contains("none"))
						{
							Pass("The Pop Up is closed.");
						}
						else
						{
							Fail("The Pop up is not closed.");
						}
					}
					else
					{
						Fail("The Close icon is not displayed.",log);
					}
				}
				else
				{
					Fail("The Pop Up is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2265 Issue ." + e.getMessage());
				Exception(" BRM - 2265 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found or the pop up is not displayed.");
		}
	}
	
	/* BRM - 1525 Verify that clicking on tool tip in Available on Nook Devices should display a pop up with nook devices and Free NOOK reading apps and again clicking on tool tip should close the pop up in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookDeviceToolTip()
	{
		ChildCreation(" BRM - 1525 Verify that clicking on tool tip in Available on Nook Devices should display a pop up with nook devices and Free NOOK reading apps and again clicking on tool tip should close the pop up in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				/*if(BNBasicfeature.isElementPresent(pdpPagePopUpClose))
				{
					log.add("The Close button is found in the pop up.");
					pdpPagePopUpClose.click();*/
					if(BNBasicfeature.isElementPresent(pdpPageNookToolTip))
					{
						log.add("The PDP Page Nook Tool Tip icon is displayed.");
						jsclick(pdpPageNookToolTip);
						wait.until(ExpectedConditions.attributeContains(pdpPageNookPopUp, "style", "block"));
						if(BNBasicfeature.isElementPresent(pdpPageNookPopUp))
						{
							if(pdpPageNookPopUp.getAttribute("style").contains("block"))
							{
								Pass("The Nook Tool Tip is displayed.",log);
								if(BNBasicfeature.isElementPresent(pdpPageNookPopUpClose))
								{
									log.add("The Nook Pop Up close icon is displayed / present.");
									jsclick(pdpPageNookPopUpClose);
									wait.until(ExpectedConditions.attributeContains(pdpPageNookPopUp, "style", "none"));
									if(pdpPageNookPopUp.getAttribute("style").contains("none"))
									{
										Pass("The Nook Pop Up is closed.",log);
									}
									else
									{
										Fail("The Nook Pop Up is not closed.",log);
									}
								}
								else
								{
									Fail("The Nook Pop up Close icon is not displayed.");
								}
							}
							else
							{
								Fail("The Nook Tool Tip is not displayed.");
							}
						}
						else
						{
							Fail("The Nook Pop Up is not displayed.",log);
						}
					}
					else
					{
						Fail("The Nook Tool Tip icon is not displayed.");
					}
				/*}
				else
				{
					Fail("The Try it for 14 days free trial is not closed.");
				}*/
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1525 Issue ." + e.getMessage());
				Exception(" BRM - 1525 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1526 Verify that Annual subscription, Amount per issue and Amount per year should be displayed in the Annual subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookAnnualSubscription()
	{
		ChildCreation(" BRM - 1526 Verify that Annual subscription, Amount per issue and Amount per year should be displayed in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			String subscription = ""; 
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1526", sheet, 4);
				String[] Val = cellVal.split("\n");
				boolean subScriptionfound = false;
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					log.add("The Subscription list is found.");
					log.add("The Expected Subscription was : " + Val[0]);
					for(int i = 0; i<pdpPageBuyTypeList.size();i++)
					{
						subscription = pdpPageBuyTypeList.get(i).getText();
						if(pdpPageBuyTypeList.get(i).getText().contains(Val[0]))
						{
							BNBasicfeature.scrolldown(pdpPageBuyTypeList.get(i), driver);
							subscription = pdpPageBuyTypeList.get(i).getText();
							log.add("The subscription list is found. The actual value is : " + subscription);
							subScriptionfound = true;
							break;
						}
						else
						{
							subScriptionfound = false;
							continue;
						}
					}
					
					if(subScriptionfound == true)
					{
						if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualIssuePrice))
						{
							String annPrice = pdpPageMagazineAnnualIssuePrice.getText();
							if(annPrice.isEmpty())
							{
								Fail("The Annual Issue Price is empty. ");
							}
							else
							{
								log.add("The Issue Per Price is displayed. The displayed price was " + pdpPageMagazineAnnualIssuePrice.getText());
							}
							
							if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualLabelPerIssue))
							{
								log.add("The Per Issue label is displayed for the annual subscription.");
								log.add("The Expected content was : " + Val[1]);
								if(pdpPageMagazineAnnualLabelPerIssue.getText().contains(Val[1]))
								{
									log.add("The Actual content was : " + pdpPageMagazineAnnualLabelPerIssue.getText());
									if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualPrice))
									{
										String annPerIssuePrice = pdpPageMagazineAnnualPrice.getText();
										if(annPerIssuePrice.isEmpty())
										{
											Fail("The Per issue Price is empty.");
										}
										else
										{
											log.add("The Annual Price for the Magazine is displayed.The displayed price was : " + pdpPageMagazineAnnualPrice.getText());
										}
										if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualLabelPerYear))
										{
											log.add("The per year label is displayed.");
											log.add("The Expected Value is : " + Val[2]);
											if(pdpPageMagazineAnnualLabelPerYear.getText().contains(Val[2]))
											{
												log.add("The Actual Content was : " + pdpPageMagazineAnnualLabelPerYear.getText());
												Pass("The Value matches the expected content.",log);
											}
											else
											{
												log.add("The Actual Content was : " + pdpPageMagazineAnnualLabelPerYear.getText());
												Fail("The Value does not match the expected Content.",log);
											}
										}
										else
										{
											Fail("The Annual Per Year label is not displayed.",log);
										}
									}
									else
									{
										Fail("The Annual Price for the Magazine is not displayed.",log);
									}
								}
								else
								{
									log.add("The Actual content was : " + pdpPageMagazineAnnualLabelPerIssue.getText());
									Fail("The Value does not match the expected content.",log);
								}
							}
							else
							{
								Fail("The Per Issue label is not displayed.",log);
							}
						}
						else
						{
							Fail("The Monthly Issue Price is not displayed.");
						}
					}
					else
					{
						Fail("The Subscription list type is not found.",log);
					}
				}
				else
				{
					Fail("The Subscription list is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1526 Issue ." + e.getMessage());
				Exception(" BRM - 1526 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1527 Verify that Add to Bag should be displayed below the Amount per issue and Amount per year in the Annual subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookAnnualSubscriptionAddToBag()
	{
		ChildCreation(" BRM - 1527 Verify that Add to Bag should be displayed below the Amount per issue and Amount per year in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1527", sheet, 3);
				if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualAddToBag))
				{
					Pass("The Add to Bag button is displayed.");
					String csvalue = pdpPageMagazineAnnualAddToBag.getCssValue("background-color");
					//String ColorName = csvalue.replace("2px solid", "");
					Color colorhxcnvt = Color.fromString(csvalue);
					//System.out.println(colorhxcnvt);
					String hexCode = colorhxcnvt.asHex();
					//System.out.println(hexCode);
					log.add("The expected button color was : " + cellVal);
					log.add("The Actual button color is : " + hexCode);
					if(hexCode.equals(cellVal))
					{
						Pass("The button color matches the expected content.",log);
					}
					else
					{
						Fail("The Button color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Add to Bag is not found for the Annual Subscription Container.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1527 Issue ." + e.getMessage());
				Exception(" BRM - 1527 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1528 Verify that Instant purchase is displayed below the Add to Bag in the Annual subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookAnnualSubscriptionInstantPurchase()
	{
		ChildCreation(" BRM - 1528 Verify that Instant purchase is displayed below the Add to Bag in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualInstantPurchase))
				{
					Pass("The Instant Purchase link is found.");
				}
				else
				{
					Fail("The Instant Purchase linkis not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1528 Issue ." + e.getMessage());
				Exception(" BRM - 1528 Issue." + e.getMessage());
			}	
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1530 Verify that Delivered weekly number of issues / year should be displayed below the Instant purchase in the Annual subscription section in the Digital Magazines PDP page.*/
	public void pdpPageMagazineNookAnnualSubscriptionDeliverdWeeklyText()
	{
		ChildCreation(" BRM - 1530 Verify that Delivered weekly number of issues / year should be displayed below the Instant purchase in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1530", sheet, 4);
				if(BNBasicfeature.isListElementPresent(pdpPageMagazineAnnualDeliveredText))
				{
					log.add("The Expected Value is : " + cellVal);
					boolean lnkFound = false;
					int i = 0;
					String acttxt = "";
					for(i=0;i<pdpPageMagazineAnnualDeliveredText.size();i++)
					{
						acttxt = pdpPageMagazineAnnualDeliveredText.get(i).getText();
						if(acttxt.contains(cellVal))
						{
							log.add("The Actual Value is : " + acttxt);
							lnkFound = true;
							break;
						}
						else
						{
							lnkFound = false;
							continue;
						}
					}
					
					if(lnkFound==true)
					{
						if(acttxt.contains(cellVal))
						{
							Pass("The Delivered Weekly Issue label is found and it is displayed.",log);
						}
						else
						{
							Fail("There is some mismatch in the expected value.",log);
						}
					}
					else
					{
						Fail("The expected value is not dispalyed.");
					}
				}
				else
				{
					Fail("The Delivered Weekly issue label is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1530 Issue ." + e.getMessage());
				Exception(" BRM - 1530 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1531 Verify that "Monthly Subscribers: how to upgrade to an annual subscription" should be displayed below the Delivered weekly number of issues / year in the Annual subscription section in the Digital Magazines PDP page.*/
	public void pdpPageMagazineNookAnnualSubscriptionMonthlySubscriberText()
	{
		ChildCreation(" BRM - 1531 Verify that Monthly Subscribers: how to upgrade to an annual subscription should be displayed below the Delivered weekly number of issues / year in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1531", sheet, 4);
				log.add("The Expected value was : " + cellVal);
				if(BNBasicfeature.isListElementPresent(pdpPageMagazineAnnualDeliveredText))
				{
					boolean lnkFound = false;
					int i = 0;
					String acttxt = "";
					for(i = 0; i<pdpPageMagazineAnnualDeliveredText.size();i++)
					{
						acttxt = pdpPageMagazineAnnualDeliveredText.get(i).getText();
						if(acttxt.contains(cellVal))
						{
							log.add("The Actual Value is : " + acttxt);
							lnkFound = true;
							break;
						}
						else
						{
							lnkFound = false;
							continue;
						}
					}
					
					if(lnkFound==true)
					{
						log.add("The Monthly Subscriber text is found.");
						WebElement ele = pdpPageMagazineAnnualDeliveredText.get(i);
						if(BNBasicfeature.isElementPresent(ele))
						{
							log.add("The Link text is found along with the Monthly Subscription text.");
							//System.out.println(ele.getText());
							String finaltext = pdpPageMagazineAnnualDeliveredText.get(i).getText().concat(ele.getText());
							//System.out.println(finaltext);
							//System.out.println(finaltext.length());
							log.add("The expected content was : " + cellVal);
							log.add("The actual content is : " + finaltext);
							if(finaltext.contains(cellVal))
							{
								Pass("The content matches the expected value.",log);
							}
							else
							{
								Fail("There is mismatch in the value.",log);
							}
						}
						else
						{
							Fail("The Link Text is not found along with the Monthly Subscription text.");
						}
					}
					else
					{
						Fail("The Expected link text is not found.");
					}
				}
				else
				{
					Fail("The Monthly Suubscrbers text is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1531 Issue ." + e.getMessage());
				Exception(" BRM - 1531 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1532 Verify that clicking on "Monthly Subscribers: how to upgrade to an annual subscription" displays a "Upgrade to Annual Subscription" overlay in the Annual subscription section in the Digital Magazines PDP page. */
	public void pdpPageMagazineNookAnnualSubscriptionHelpOverlay()
	{
		ChildCreation(" BRM - 1532 Verify that clicking on Monthly Subscribers: how to upgrade to an annual subscription displays a Upgrade to Annual Subscription overlay in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1531", sheet, 4);
				log.add("The Expected value was : " + cellVal);
				if(BNBasicfeature.isListElementPresent(pdpPageMagazineAnnualDeliveredText))
				{
					boolean lnkFound = false;
					int i = 0;
					String acttxt = "";
					for(i = 0; i<pdpPageMagazineAnnualDeliveredText.size();i++)
					{
						acttxt = pdpPageMagazineAnnualDeliveredText.get(i).getText();
						if(acttxt.contains(cellVal))
						{
							log.add("The Actual Value is : " + acttxt);
							lnkFound = true;
							break;
						}
						else
						{
							lnkFound = false;
							continue;
						}
					}
				
					if(lnkFound==true)
					{
						if(BNBasicfeature.isListElementPresent(pdpPageMagazineAnnualDeliveredText))
						{
							int responseCode = BNBasicfeature.linkBroken(pdpPageMagazineAnnualMonthlySubscriberLinkText.get(i), log);
							if(responseCode == 200)
							{
								log.add("The link is valid. The response code thrown for the link is : " + responseCode);
								jsclick(pdpPageMagazineAnnualMonthlySubscriberLinkText.get(i));
								wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "block"));
								if(BNBasicfeature.isElementPresent(pdpPagePopUp))
								{
									log.add("The How to Upgrade to an annual subsciption link was clicked and the Pop up is displayed.");
									if(pdpPagePopUp.getAttribute("style").contains("block"))
									{
										Pass("The How to Upgrade to an annual subsciption link was clicked and the Pop up is displayed.",log);
									}
									else
									{
										Fail("The How to Upgrade to an annual subsciption link was clicked and the Pop up is not displayed.",log);
									}
								}
								else
								{
									Fail("The How to Upgrade to an annual subsciption link was clicked and the Pop up is not displayed.",log);
								}
							}
							else
							{
								Fail("The link is not valid. The Response code thrown for the link was : " + responseCode);
							}
						}
						else
						{
							Fail("The Monthly Subscriber link text is not found.");
						}
					}
					else
					{
						Fail("The Monthly Subscriber link text is not found.");
					}
				}
				else
				{
					Fail("The Monthly Subscriber text is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1532 Issue ." + e.getMessage());
				Exception(" BRM - 1532 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1533 Verify that clicking on close in "Upgrade to Annual Subscription" overlay closes the overlay in the Annual subscription section in the Digital Magazines PDP page.*/
	public void pdpPageMagazineNookAnnualSubscriptionHelpOverlayClose()
	{
		ChildCreation(" BRM - 1533 Verify that clicking on close in Upgrade to Annual Subscription overlay closes the overlay in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPagePopUpClose))
				{
					log.add("The Close icon is displayed in the Opened pop Up.");
					jsclick(pdpPagePopUpClose);
					wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "none"));
					if(pdpPagePopUp.getAttribute("style").contains("display: none;"))
					{
						Pass("The How to Upgrade to an annual subsciption close was clicked and the Pop up is closed.",log);
					}
					else
					{
						Fail("The How to Upgrade to an annual subsciption close was clicked and the Pop up is not closed.",log);
					}
				}
				else
				{
					Fail("The Close icon is not displayed in the Pop up.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1533 Issue ." + e.getMessage());
				Exception(" BRM - 1533 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1534 Verify that Monthly subscription, Amount per issue and Amount per month should be displayed in the Monthly subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookMonthlySubscription()
	{
		ChildCreation(" BRM - 1534 Verify that Monthly subscription, Amount per issue and Amount per month should be displayed in the Monthly subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			String subscription = ""; 
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1534", sheet, 4);
				String[] Val = cellVal.split("\n");
				boolean subScriptionfound = false;
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					log.add("The Subscription list is found.");
					log.add("The Expected Subscription was : " + Val[0]);
					for(int i = 1; i<=pdpPageBuyTypeList.size();i++)
					{
						subscription = pdpPageBuyTypeList.get(i).getText();
						if(pdpPageBuyTypeList.get(i).getText().contains(Val[0]))
						{
							subscription = pdpPageBuyTypeList.get(i).getText();
							log.add("The subscription list is found. The actual value is : " + subscription);
							subScriptionfound = true;
							break;
						}
						else
						{
							subScriptionfound = false;
							continue;
						}
					}
					
					if(subScriptionfound == true)
					{
						if(BNBasicfeature.isElementPresent(pdpPageMagazineMonthlyIssuePrice))
						{
							String issPrice = pdpPageMagazineMonthlyIssuePrice.getText();
							if(issPrice.isEmpty())
							{
								Fail("The issue price is empty.");
							}
							else
							{
								log.add("The Issue Per Price is displayed. The displayed price was " + pdpPageMagazineMonthlyIssuePrice.getText());
							}
							
							if(BNBasicfeature.isElementPresent(pdpPageMagazineMonthlyLabelPerIssue))
							{
								log.add("The Per Issue label is displayed for the annual subscription.");
								log.add("The Expected content was : " + Val[1]);
								if(pdpPageMagazineMonthlyLabelPerIssue.getText().contains(Val[1]))
								{
									log.add("The Actual content was : " + pdpPageMagazineMonthlyLabelPerIssue.getText());
									if(BNBasicfeature.isElementPresent(pdpPageMagazineMonthlyPrice))
									{
										String monPrice = pdpPageMagazineMonthlyPrice.getText();
										if(monPrice.isEmpty())
										{
											Fail("The issue price is empty.");
										}
										else
										{
											log.add("The Monthly Price for the Magazine is displayed.The displayed price was : " + pdpPageMagazineMonthlyPrice.getText());
										}
										
										if(BNBasicfeature.isElementPresent(pdpPageMagazineMonthlyLabelPerYear))
										{
											log.add("The per month  label is displayed.");
											log.add("The Expected Value is : " + Val[2]);
											if(pdpPageMagazineMonthlyLabelPerYear.getText().contains(Val[2]))
											{
												log.add("The Actual Content was : " + pdpPageMagazineMonthlyLabelPerYear.getText());
												Pass("The Value matches the expected content.",log);
											}
											else
											{
												log.add("The Actual Content was : " + pdpPageMagazineMonthlyLabelPerYear.getText());
												Fail("The Value does not match the expected Content.",log);
											}
										}
										else
										{
											Fail("The Annual Per Year label is not displayed.",log);
										}
									}
									else
									{
										Fail("The Annual Price for the Magazine is not displayed.",log);
									}
								}
								else
								{
									log.add("The Actual content was : " + pdpPageMagazineMonthlyLabelPerIssue.getText());
									Fail("The Value does not match the expected content.",log);
								}
							}
							else
							{
								Fail("The Per Issue label is not displayed.",log);
							}
						}
						else
						{
							Fail("The Monthly Issue Price is not displayed.");
						}
					}
					else
					{
						Fail("The Subscription list type is not found.",log);
					}
				}
				else
				{
					Fail("The Subscription list is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1534 Issue ." + e.getMessage());
				Exception(" BRM - 1534 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1536 Verify that Current issue and Amount per issue should be displayed in the Current subscription section in the Digital Magazines PDP page.*/
	public void pdpPageMagazineCurrentIssueDetails()
	{
		ChildCreation("BRM - 1536 Verify that Current issue and Amount per issue should be displayed in the Current subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				boolean currentSubScription = false;
				int j = 0;
				String cellVal = BNBasicfeature.getExcelVal("BRM1536", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					for(j = 0; j<pdpPageBuyTypeList.size(); j++)
					{
						String txt = pdpPageBuyTypeList.get(j).getText();
						if(txt.contains(Val[0]))
						{
							currentSubScription = true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(currentSubScription==true)
					{
						BNBasicfeature.scrolldown(pdpPageBuyTypeList.get(j), driver);
						Pass("The Current Issue is displayed for the Product.");
						log.add("The Expected content was : " + Val[0]);
						String txt = pdpPageBuyTypeList.get(j).getText();
						log.add("The Actual content is : " + txt);
						if(txt.contains(Val[0]))
						{
							Pass("The Value match the expected content.",log);
							if(BNBasicfeature.isElementPresent(pdpPageMagazineCurrentIssuePrice))
							{
								String val = pdpPageMagazineCurrentIssuePrice.getText();
								if(val.isEmpty())
								{
									Fail("The Current Issue Price is not displayed.");
								}
								else
								{
									log.add("The Displayed price is : " + val);
									Pass("The Current Issue Price is not displayed.");
								}
							}
							else
							{
								Fail("The Current Issue Price is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(pdpPageMagazineCurrentIssuePriceLabel))
							{
								String val = pdpPageMagazineCurrentIssuePriceLabel.getText();
								if(val.contains(Val[1]))
								{
									Pass("The Per Issue is displayed and it matches the expected value.",log);
								}
								else
								{
									Fail("The Per Issue is displayed but there is some mismatach in the expected value.",log);
								}
							}
							else
							{
								Fail("The Current Issue Price is not displayed.");
							}
							
						}
						else
						{
							Fail("The Value does not match the expected content.",log);
						}
					}
					else
					{
						Fail( "The Current Subscription list is not dispalyed.");
					}
				}
				else
				{
					Fail("The Current Issue is not dispalyed for the Products.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1536 Issue ." + e.getMessage());
				Exception(" BRM - 1536 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1535 Verify that Add to Bag should be displayed below the Amount per issue and Amount per month in the Monthly subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookMonthlyAddToBag()
	{
		ChildCreation(" BRM - 1535 Verify that Add to Bag should be displayed below the Amount per issue and Amount per month in the Monthly subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1535", sheet, 3);
				if(BNBasicfeature.isElementPresent(pdpPageMagazineMonthlyAddToBag))
				{
					Pass("The Add to Bag button is displayed.");
					String csvalue = pdpPageMagazineMonthlyAddToBag.getCssValue("background-color");
					//String ColorName = csvalue.replace("2px solid", "");
					Color colorhxcnvt = Color.fromString(csvalue);
					//System.out.println(colorhxcnvt);
					String hexCode = colorhxcnvt.asHex();
					//System.out.println(hexCode);
					log.add("The expected button color was : " + cellVal);
					log.add("The Actual button color is : " + hexCode);
					if(hexCode.equals(cellVal))
					{
						Pass("The button color matches the expected content.",log);
					}
					else
					{
						Fail("The Button color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Add to Bag is not found for the Annual Subscription Container.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1535 Issue ." + e.getMessage());
				Exception(" BRM - 1535 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
	}
	
	/* BRM - 1529 Verify that while selecting the Instant purchase displays the login page in the Annual subscription section in the Digital Magazines PDP page*/
	public void pdpPageMagazineNookAnnualSubscriptionInstantPurchaseLoginNavigation()
	{
		ChildCreation(" BRM - 1529 Verify that while selecting the Instant purchase displays the login page in the Annual subscription section in the Digital Magazines PDP page.");
		if(nookMagazinePdp==true)
		{
			try
			{
				String signIntitle = BNBasicfeature.getExcelVal("BRM1450", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageMagazineAnnualInstantPurchase))
				{
					BNBasicfeature.scrollup(pdpPageMagazineAnnualInstantPurchase, driver);
					Thread.sleep(1000);
					log.add("The instant Purchase button is displayed.");
					jsclick(pdpPageMagazineAnnualInstantPurchase);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					String title = pgetitle.getText();
					log.add("The Expected title was " + signIntitle);
					if(title.equals(signIntitle))
					{
						log.add("The Actual Title is : " + title);
						Pass("The user is navigated to the Sign In Page when Add to WishList was clicked.", log);
						driver.navigate().back();
					}
					else
					{
						log.add("The Actual title is " + title);
						Fail("The user was not navigated to the Sign In Page when Add to WishList was clicked.",log);
						driver.navigate().back();
					}
				}
				else
				{
					Fail("The Add to Wishlist Icon text is not displayed");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1529 Issue ." + e.getMessage());
				Exception(" BRM - 1529 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip(" The specified Nook Magazine is not found.");
		}
		wait.until(ExpectedConditions.visibilityOf(header));
		//wait.until(ExpectedConditions.visibilityOf(prdtBagCount));
	}
	
	/* BRM - 1501 Verify that "ISBN" and "Edition" details should be displayed in the Textbooks PDP page*/
	public void pdpPageProductISBNandEdition()
	{
		ChildCreation(" BRM - 1501 Verify that ISBN and Edition details should be displayed in the Textbooks PDP page.");
		try
		{
			//String bookName = BNBasicfeature.getExcelVal("BRM1501", sheet, 2);
			//searchProduct(sheet, false,"BRM1501");
			//boolean textbookPdp = selectProduct(bookName);
			String ean = BNBasicfeature.getExcelVal("BRM1501", sheet, 8);
			String cellVal = ean;
			String[] eanNum = cellVal.split("\n");
			boolean bkfound = false;
			for(int i = 0; i<eanNum.length;i++)
			{
				bkfound = searchEanNumbers(eanNum[i]);
				 if(bkfound == true)
				 {
					 if(BNBasicfeature.isListElementPresent(pdpPageISBNList))
					 {
						 textbookPdp = true;
						 break;
					 }
					 else
					 {
						 textbookPdp = false;
						 continue;
					 }
				 }
			}
			
			if(textbookPdp == true)
			{
				wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
				BNBasicfeature.scrolldown(pdpPageProductName, driver);
				if(BNBasicfeature.isListElementPresent(pdpPageISBNList))
				{
					log.add("The ISBN is listed in the PDP page for the selected product.");
					for(int i = 1;i<=pdpPageISBNList.size();i++)
					{
						WebElement isbndet = driver.findElement(By.xpath("(//*[@class='skMob_shortDescCont']//p)["+i+"]"));
						String isbnNum = isbndet.getText();
						if(isbnNum.isEmpty())
						{
							Fail("The ISBN Container is displayed but the value is empty.");
						}
						else
						{
							log.add("The displayed ISBN details is : " + isbnNum);
							//System.out.println(isbndet.getText());
						}
					}
					Pass("The ISBN Details is displayed for the product.",log);
				}
				else
				{
					Fail("The ISBN is not displayed in the PDP Page.",log);
				}
			}
			else
			{
				Fail("The book is not found in the PLP Page.",log);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1501 Issue ." + e.getMessage());
			Exception(" BRM - 1501 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1502 Verify that while selecting the down arrow option in the "Edition", the "ISBN Edition" section should be enabled in the Textbooks PDP page */
	public void pdpPageProductISBNExpand()
	{
		ChildCreation(" BRM - 1502 Verify that while selecting the down arrow option in the Edition, the ISBN Edition section should be enabled in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageISBNList.get(0)))
				{
					log.add("The ISBN and Edition details are displayed in the PDP Page.");
					if(BNBasicfeature.isElementPresent(pdpPageProductShortDescReadMore))
					{
						log.add("The Read More section is found in the PDP Page for the selected product.");
						jsclick(pdpPageProductShortDescReadMore);
						if(BNBasicfeature.isElementPresent(pdpPageOverviewContainer))
						{
							log.add("The Overview Container is found.");
							wait.until(ExpectedConditions.attributeContains(pdpPageOverviewContainer, "class", "expand"));
							String attr = pdpPageOverviewContainer.getAttribute("class");
							if(attr.contains("pdp_Description expandDetails"))
							{
								Pass("The Overview Section is displayed when Read More is clicked on the PDP Page.",log);
							}
							else
							{
								Fail("The Overview Section is not displayed when Read More icon is clicked on the PDP Page.",log);
							}
						}
						else
						{
							Fail("The overview Container is not found.",log);
						}
					}
					else
					{
						Fail("The Read More link is not found in the PDP page.");
					}
				}
				else
				{
					Fail("The Shordt Description is not displayed in the PDP page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1502 Issue ." + e.getMessage());
				Exception(" BRM - 1502 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1503 Verify that while selecting the Up arrow option in the "Edition",the "ISBN Edition" section should be collapsed in the Textbooks PDP page */
	public void pdpPageISBNCollapse()
	{
		ChildCreation("BRM - 1503 Verify that while selecting the Up arrow option in the Edition,the ISBN Edition section should be collapsed in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			String accName = "";
			boolean AccordionFound = false;
			try
			{
				//BNBasicfeature.scrolldown(pdpPageInstantPurchase, driver);
				String getAccordionName = BNBasicfeature.getExcelVal("BRM1467", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				if(BNBasicfeature.isElementPresent(pdpPageExpandedAccordion))
				{
					log.add("The Accordion " + accName + " is expanded / opened");
					AccordionFound = true;
				}
				else
				{
					log.add("The Accordion " + accName + " is not expanded / opened");
					AccordionFound = false;
				}
				
				if(AccordionFound == true)
				{
					//System.out.println(accName);
					//System.out.println(j);
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
					WebElement ele = pdpPageAccordionList.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to collapse the " + accName + " Accordion is found");
						jsclick(ele);
						//ele.click();
						WebElement contCollapse = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contCollapse, "style", "none"));
						//Thread.sleep(1500);
						//WebElement contCollapse = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						String contExpandet = contCollapse.getAttribute("style");
						if(contExpandet.contains("none"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is closed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.", log);
						}
					}
					else
					{
						Fail("The Link to collapse the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Accordion is not found on the Product Page / it is not in opened state.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1503 Issue ." + e.getMessage());
				Exception(" BRM - 1503 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1504 Verify that Rent Paperback, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page*/
	public void pdpPageProductSavepercentage()
	{
		ChildCreation(" BRM - 1504 Verify that Rent Paperback, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					BNBasicfeature.scrollup(pdpPageProductDropDown, driver);
					log.add("The Add to Bag Container is found.");
					if(BNBasicfeature.isListElementPresent(pdpPageActualPriceList))
					{
						log.add("The Sale price list is displayed.");
						if(BNBasicfeature.isListElementPresent(pdpPageSalePriceList))
						{
							log.add("The Actual Price list is displayed.");
							if(BNBasicfeature.isListElementPresent(pdpPageActualSavePercent))
							{
								log.add("The Save Percentage for the Prodcut is displayed.");

								for(int i = 1;i<=pdpPageBuyTypeList.size();i++)
								{
									String buyType = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]")).getText();
									if(buyType.isEmpty())
									{
										Fail("The Buy Type displayed is empty.");
									}
									else
									{
										log.add("The Buy Type displayed is : " + buyType);
									}
									
									//WebElement salePrice = driver.findElement(By.xpath("(//*[@class='priceEle skmob_Sale skMob_price_sale '])["+i+"]"));
									if(!buyType.contains("Market"))
									{
										WebElement salePrice = driver.findElement(By.xpath("(//*[contains(@pricetype,'Sale')])["+i+"]"));
										String sPrice = salePrice.getAttribute("price");
										if(sPrice.isEmpty())
										{
											Fail("The Sale Price displayed is empty.");
										}
										else
										{
											log.add("The Sale Price displayed is : " + sPrice);
										}
										
										String originalPrice = driver.findElement(By.xpath("(//*[contains(@class,'saleAvail')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
										if(originalPrice.isEmpty())
										{
											Fail("The Original Price displayed is empty.");
										}
										else
										{
											log.add("The Original Price displayed is : " + originalPrice);
										}
										
										String savePercent = driver.findElement(By.xpath("(//*[contains(@pricetype,'Save')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
										if(originalPrice.isEmpty())
										{
											Fail("The Save Percent displayed is empty.");
										}
										else
										{
											log.add("The Save Percentage displayed for the Product is : " + savePercent);
										}
									}
								}
								Pass("The Buy Type, Sale Price,Actual Price and Save percentage is displayed.",log);
							}
							else
							{
								Fail("The Save Percentage List is not displayed.",log);
							}
						}
						else
						{
							Fail("The Actual Price List is not displayed.",log);
						}
					}
					else
					{
						Fail("The Sale Price List is not displayed.");
					}
				}
				else
				{
					Fail("The Add to Bag Container is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1504 Issue ." + e.getMessage());
				Exception(" BRM - 1504 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1505 Verify that Rent Paperback drop down should be displayed with "60 days - $Amount" selected by default in the Textbooks PDP page*/
	public void pdpPageBuyTypeDropDown()
	{
		ChildCreation(" BRM - 1505 Verify that Rent Paperback drop down should be displayed with 60 days - $Amount selected by default in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1505", sheet, 4);
				if(BNBasicfeature.isElementPresent(pdpPageBuyTypeDD))
				{
					Pass("The Buy Type Drop Down is displayed.");
					/*Select sel = new Select(pdpPageBuyTypeDD);
					String defSelVal = sel.getFirstSelectedOption().getText();*/
					List<WebElement> ele = driver.findElements(By.xpath("//*[@class='sk_pdtBuyType']//*"));
					String defSelVal = ele.get(0).getText();
					log.add("The Expected Selected Default Value : "  + cellVal);
					log.add("The Current Value selected in the Drop Down : " + defSelVal);
					if(defSelVal.contains(cellVal))
					{
						Pass("The Default Selected in the Buy Type matches the expected content.",log);
					}
					else
					{
						Fail("The Default Selected in the Buy Type does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Buy Type Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1505 Issue ." + e.getMessage());
				Exception(" BRM - 1505 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1506 Verify that drop down has "60 days - $Amount", "90 days - $Amount" and "130 days - $Amount" options in the Textbooks PDP page*/
	public void pdpPageBuyTypeDropDownValues()
	{
		ChildCreation(" BRM - 1506 Verify that drop down has 60 days - $Amount, 90 days - $Amount and 130 days - $Amount options in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1506", sheet, 4);
				String[] val = cellVal.split("\n"); 
				if(BNBasicfeature.isElementPresent(pdpPageBuyTypeDD))
				{
					Pass("The Buy Type Drop Down is displayed.");
					//Select sel = new Select(pdpPageBuyTypeDD);
					//int size = sel.getOptions().size();
					List<WebElement> ele = driver.findElements(By.xpath("//*[@class='sk_pdtBuyType']//*"));
					int size = ele.size();
					//String defSelVal = ele.get(0).getText();
					for(int i = 0; i<size;i++)
					{
						String defSelVal = ele.get(i).getText();
						log.add("The Expected Selected Default Value : "  + val[i]);
						if(defSelVal.contains(val[i]))
						{
							log.add("The Current Value selected in the Drop Down : " + defSelVal);
							Pass("The Buy Type matches the expected content.",log);
						}
						else
						{
							log.add("The Current Value selected in the Drop Down : " + defSelVal);
							Fail("The Buy Type does not matches the expected content.",log);
						}
					}
				}
				else
				{
					Fail("The Buy Type Drop Down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1506 Issue ." + e.getMessage());
				Exception(" BRM - 1506 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1507 Verify that estimated return date should be displayed below the Add to Bag option in the Textbooks PDP page*/
	public void pdpPageEstReturnDate()
	{
		ChildCreation(" BRM - 1507 Verify that estimated return date should be displayed below the Add to Bag option in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageRetDateContainer))
				{
					//System.out.println(pdpPageRetDateContainer.getText());
					log.add("The Estimated Return Date Container is found and displayed.");
					if(BNBasicfeature.isElementPresent(pdpPageRetDate))
					{
						if(pdpPageRetDate.getText().isEmpty())
						{
							Fail("The Estimated return Date is not displayed and it is empty.");
						}
						else
						{
							Pass("The Estimated return Date is displayed." + pdpPageRetDate.getText() , log);
						}
					}
					else
					{
						Fail("The Estimated return Date is not displayed." + pdpPageRetDate.getText() , log);
					}
				}
				else
				{
					Fail("The Estimated Return date container is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1507 Issue ." + e.getMessage());
				Exception(" BRM - 1507 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1508 Verify that while selecting the "How Textbook Rental Works" displays a overlay with the required information in the Textbooks PDP page*/
	public void pdpPageRentalBookMessage()
	{
		ChildCreation(" BRM - 1508 Verify that while selecting the How Textbook Rental Works displays a overlay with the required information in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageRentalSteps))
				{
					log.add("The Rental steps links is found.");
					jsclick(pdpPageRentalSteps);
					wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "block"));
					if(BNBasicfeature.isElementPresent(pdpPagePopUp))
					{
						Pass("The Rental pop up is displayed when Rental Steps detials link was clicked.",log);
					}
					else
					{
						Fail("The Rental Pop up is not displayed.",log);
					}
				}
				else
				{
					Fail("The Rental Steps link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1508 Issue ." + e.getMessage());
				Exception(" BRM - 1508 Issue." + e.getMessage());
			}	
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1509 Verify that clicking on close in the overlay , the "How Textbook Rental Works" section should be closed in the Textbooks PDP page*/
	public void pdpPageRentalPopUpClose()
	{
		ChildCreation(" BRM - 1509 Verify that clicking on close in the overlay , the How Textbook Rental Works section should be closed in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPagePopUp))
				{
					log.add("The Rental pop up is displayed when Rental Steps is clicked.");
					if(BNBasicfeature.isElementPresent(pdpPagePopUpClose))
					{
						log.add("The Rental Pop Up Close icon is displayed.");
						jsclick(pdpPagePopUpClose);
						wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "none"));
						if(pdpPagePopUp.getAttribute("style").contains("none"))
						{
							Pass("The Rental Pop up is closed.",log);
						}
						else
						{
							Fail("The Rental Pop Up is not closed.",log);
						}
					}
					else
					{
						Fail("The Rental Pop Up icon is not visible / present.");
					}
				}
				else
				{
					Fail("The Rental Pop up is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1509 Issue ." + e.getMessage());
				Exception(" BRM - 1509 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1510 Verify that Shipping information should be displayed below "How Textbook Rental Works" in the Textbooks PDP page.*/
	public void pdpPageTextBooxShipInfo()
	{
		ChildCreation(" BRM - 1510 Verify that Shipping information should be displayed below How Textbook Rental Works in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pdpPageRentalFAQ))
				{
					log.add("The Rental steps links is found.");
					jsclick(pdpPageRentalFAQ);
					wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "block"));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(pdpPagePopUp))
					{
						Pass("The Rental FAQ pop up is displayed when Rental Steps detials link was clicked.",log);
						if(BNBasicfeature.isElementPresent(pdpPagePopUp))
						{
							log.add("The Rental pop up is displayed when Rental Steps is clicked.");
							if(BNBasicfeature.isElementPresent(pdpPagePopUpClose))
							{
								log.add("The Rental Pop Up Close icon is displayed.");
								jsclick(pdpPagePopUpClose);
								wait.until(ExpectedConditions.attributeContains(pdpPagePopUp, "style", "none"));
								Thread.sleep(100);
								if(pdpPagePopUp.getAttribute("style").contains("none"))
								{
									Pass("The Rental FAQ Pop up is closed.",log);
								}
								else
								{
									Fail("The Rental FAQ Pop Up is not closed.",log);
								}
							}
							else
							{
								Fail("The Rental Pop Up icon is not visible / present.");
							}
						}
						else
						{
							Fail("The Rental Pop up is not displayed.");
						}
					}
					else
					{
						Fail("The Rental FAQ Pop up is not displayed.",log);
					}
				}
				else
				{
					Fail("The Rental Steps link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1510 Issue ." + e.getMessage());
				Exception(" BRM - 1510 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1511 Verify that Rent / Buy New, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page*/
	public void pdpPageBuyTypeLists()
	{
		ChildCreation(" BRM - 1511 Verify that Rent / Buy New, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM1511", sheet, 4);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					log.add("The Add to Bag Container is found.");
					if(BNBasicfeature.isListElementPresent(pdpPageActualPriceList))
					{
						log.add("The Sale price list is displayed.");
						if(BNBasicfeature.isListElementPresent(pdpPageSalePriceList))
						{
							log.add("The Actual Price list is displayed.");
							if(BNBasicfeature.isListElementPresent(pdpPageActualSavePercent))
							{
								log.add("The Save Percentage for the Prodcut is displayed.");
								for(int i = 1;i<=val.length;i++)
								{
									String buyType = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]")).getText();
									log.add("The Buy Type displayed is : " + buyType);
									log.add("The Expected Buy Type is : " + val[i-1]);
									if(buyType.contains(val[i-1]))
									{
										//WebElement salePrice = driver.findElement(By.xpath("(//*[@class='priceEle skmob_Sale skMob_price_sale '])["+i+"]"));
										WebElement salePrice = driver.findElement(By.xpath("(//*[@pricetype='Sale'])["+i+"]"));
										String sPrice = salePrice.getAttribute("price");
										if(sPrice.isEmpty())
										{
											Fail("The Sale Price is empty.");
										}
										else
										{
											log.add("The Sale Price displayed is : " + sPrice);
										}
										if(!buyType.contains("Market"))
										{
											String originalPrice = driver.findElement(By.xpath("(//*[contains(@class,'saleAvail')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
											if(originalPrice.isEmpty())
											{
												Fail("The Original Price displayed is empty.");
											}
											else
											{
												log.add("The Original Price displayed is : " + originalPrice);
											}
											
											String savePercent = driver.findElement(By.xpath("(//*[contains(@pricetype,'Save')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
											if(originalPrice.isEmpty())
											{
												Fail("The Save Percent displayed is empty.");
											}
											else
											{
												log.add("The Save Percentage displayed for the Product is : " + savePercent);
											}
										}
									}
									else
									{
										Fail("The Buying Type : " + buyType + " is displayed and it does not matches the expected content. ", log);
									}
								}
							}
							else
							{
								Fail("The Save Percentage List is not displayed.",log);
							}
						}
						else
						{
							Fail("The Actual Price List is not displayed.",log);
						}
					}
					else
					{
						Fail("The Sale Price List is not displayed.");
					}
				}
				else
				{
					Fail("The Add to Bag Container is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1511 Issue ." + e.getMessage());
				Exception(" BRM - 1511 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1513 Verify that Buy Used, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page*/
	public void pdpPageBuyUsed()
	{
		ChildCreation(" BRM - 1513 Verify that Buy Used, Sale amount, Actual amount (Should be striked off) and Save percentage should be displayed in the Textbooks PDP page.");
		if(textbookPdp==true)
		{
			int i;
			try
			{
				String buyTypeName = BNBasicfeature.getExcelVal("BRM1513", sheet, 4);
				/*String bookName = BNBasicfeature.getExcelVal("BRM1513", sheet, 2);
				String buyTypeName = BNBasicfeature.getExcelVal("BRM1513", sheet, 4);
				searchProduct(sheet, false, "BRM1513");
				boolean textbookPdpBuytype = selectProduct(bookName);
				if(textbookPdpBuytype == true)
				{*/
					//wait.until(ExpectedConditions.visibilityOf(prdtshortDesc));
					BNBasicfeature.scrolldown(pdpPageAddToWishListContainer, driver);
					if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
					{
						log.add("The Buy Type list is displayed in the PDP page for the selected product.");
						for(i = 1;i<=pdpPageBuyTypeList.size();i++)
						{
							String buytype = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]")).getText();
							if(buyTypeName.equals(buytype))
							{
								textbookPdpBuytype = true;
								break;
							}
							{
								textbookPdpBuytype = false;
								continue;
							}
						}
						
						if(textbookPdpBuytype == true)
						{
							//System.out.println(i);
							String buyType = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]")).getText();
							//WebElement salePrice = driver.findElement(By.xpath("(//*[@class='priceEle skmob_Sale skMob_price_sale '])["+i+"]"));
							WebElement salePrice = driver.findElement(By.xpath("(//*[@pricetype='Sale'])["+i+"]"));
							String sPrice = salePrice.getAttribute("price");
							if(sPrice.isEmpty())
							{
								Fail("The Sale Price is empty.");
							}
							else
							{
								log.add("The Sale Price displayed is : " + sPrice);
							}
							
							String originalPrice = driver.findElement(By.xpath("(//*[contains(@class,'saleAvail')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
							if(originalPrice.isEmpty())
							{
								Fail("The Original Price displayed is empty.");
							}
							else
							{
								log.add("The Original Price displayed is : " + originalPrice);
							}
							
							String savePercent = driver.findElement(By.xpath("(//*[contains(@pricetype,'Save')]//*[contains(@class,'priceValue')])["+i+"]")).getText();
							if(originalPrice.isEmpty())
							{
								Fail("The Save Percent displayed is empty.");
							}
							else
							{
								log.add("The Save Percentage displayed for the Product is : " + savePercent);
							}
							Pass("The Buying Type : " + buyType + " is displayed and it matches the expected content. ", log);
						}
						else
						{
							Fail("The Buy Type List is not found.",log);
						}
					}
					else
					{
						Fail("The ISBN is not displayed in the PDP Page.",log);
					}
				/*}
				else
				{
					Fail("The book is not found in the PLP Page.",log);
				}*/
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1513 Issue ." + e.getMessage());
				Exception(" BRM - 1513 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was not found.");
		}
	}
	
	/* BRM - 1516 Verify that "Marketplace from $amount" is displayed below the Buy New & Used From Sale amount in the Textbooks PDP page*/
	public void pdpPageMarketPlace()
	{
		ChildCreation(" BRM - 1516 Verify that Marketplace from $amount is displayed below the Buy New & Used From Sale amount in the Textbooks PDP page.");
		if(textbookPdpBuytype==true)
		{
			boolean buyTypeFound = false;
			int i;
			try
			{
				String buyTypeName = BNBasicfeature.getExcelVal("BRM1516", sheet, 4);
				//wait.until(ExpectedConditions.visibilityOf(prdtshortDesc));
				BNBasicfeature.scrolldown(pdpPageAddToWishListContainer, driver);
				if(BNBasicfeature.isListElementPresent(pdpPageBuyTypeList))
				{
					log.add("The Buy Type list is displayed in the PDP page for the selected product.");
					for(i = 1;i<=pdpPageBuyTypeList.size();i++)
					{
						String buytype = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]")).getText();
						//System.out.println(buytype.getText());
						if(buyTypeName.equals(buytype))
						{
							buyTypeFound = true;
							break;
						}
						{
							buyTypeFound = false;
							continue;
						}
					}
					
					if(buyTypeFound == true)
					{
						//System.out.println(i);
						WebElement buyType = driver.findElement(By.xpath("(//*[@class='sk_buyingType'])["+i+"]"));
						//WebElement salePrice = driver.findElement(By.xpath("//*[@class='priceEle skmob_Sale skMob_price_reg ']//*[@class='skmob_priceValue ']"));
						WebElement salePrice = driver.findElement(By.xpath("(//*[@pricetype='Sale'])["+i+"]"));
						//System.out.println(salePrice.getText());
						log.add("The Sale Price displayed is : " + salePrice.getText());
						Pass("The Buying Type : " + buyType.getText() + " is displayed and it matches the expected content. ", log);
					}
					else
					{
						Fail("The Buy Type List is not found.",log);
					}
				}
				else
				{
					Fail("The ISBN is not displayed in the PDP Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1516 Issue ." + e.getMessage());
				Exception(" BRM - 1516 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was the buy type is not found.");
		}
	}
	
	/* BRM - 1488 Verify that while selecting the "+" in the "Table of Contents" accordian,the table of contents section should be expanded*/
	public void pdpPageProductTableOfContentsExpand()
	{
		ChildCreation(" BRM - 1488 Verify that while selecting the + in the Related Subjects Accordion,the related subjects section should be expanded.");
		if(textbookPdp==true)
		{
			String accName = "";
			boolean AccordionFound = false;
			try
			{
				//BNBasicfeature.scrolldown(pdpPageInstantPurchase, driver);
				String getAccordionName = BNBasicfeature.getExcelVal("BRM1488", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				if(AccordionFound==true)
				{
					log.add("The Expected Accordion : " + getAccordionName);
					log.add("The Actual Accordion : " + accName);
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
					WebElement ele = pdpPageAccordionLink.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to expand the " + accName + " Accordion is found");
						jsclick(ele);
						WebElement contExpan = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contExpan, "style", "block"));
						//WebElement contExpan = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						String contExpandet = contExpan.getAttribute("style");
						if(contExpandet.contains("display: block;"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is displayed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.",log);
						}
					}
					else
					{
						Fail("The Link to expand the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Accordion is not found",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1488 Issue ." + e.getMessage());
				Exception(" BRM - 1488 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was the buy type is not found.");
		}
	}
	
	/* BRM - 1489 Verify that while selecting the close button in the "Table of contents",it should collapse the Table of Contents details*/
	public void pdpPageProductTableOfContentsCollapse()
	{
		ChildCreation(" BRM - 1489 Verify that while selecting the close button in the Table of contents,it should collapse the Table of Contents details.");
		if(textbookPdp==true)
		{
			String accName = "";
			boolean AccordionFound = false;
			try
			{
				//BNBasicfeature.scrolldown(pdpPageInstantPurchase, driver);
				String getAccordionName = BNBasicfeature.getExcelVal("BRM1488", sheet, 4);
				AccordionFound = getAccordian(getAccordionName);
				
				if(BNBasicfeature.isElementPresent(pdpPageExpandedAccordion))
				{
					log.add("The Accordion " + accName + " is expanded / opened");
					AccordionFound = true;
				}
				else
				{
					log.add("The Accordion " + accName + " is not expanded / opened");
					AccordionFound = false;
				}
				
				if(AccordionFound == true)
				{
					//System.out.println(accName);
					//System.out.println(j);
					//WebElement ele = driver.findElement(By.xpath("(//*[@id='id_pdpContainer']//*[@class='pdp_DetailsLink'])["+j+"]"));
					WebElement ele = pdpPageAccordionList.get(accSel-1);
					if(BNBasicfeature.isElementPresent(ele))
					{
						log.add("The Link to collapse the " + accName + " Accordion is found");
						jsclick(ele);
						//ele.click();
						WebElement contCollapse = pdpPageAccExp.get(accSel-1);
						wait.until(ExpectedConditions.attributeContains(contCollapse, "style", "none"));
						//Thread.sleep(1500);
						//WebElement contCollapse = driver.findElement(By.xpath("(//*[@class='descriptionDetails pdp_DetailsCont'])["+j+"]"));
						String contExpandet = contCollapse.getAttribute("style");
						if(contExpandet.contains("none"))
						{
							Pass("The " + accName + " Accordion is clicked and the Container is closed successfully.", log);
						}
						else
						{
							Fail("The " + accName + " Accordion is clicked and the container is not displayed.", log);
						}
					}
					else
					{
						Fail("The Link to collapse the " + accName + " Accordion is not found",log);
					}
				}
				else
				{
					Fail("The Accordion is not found on the Product Page / it is not in opened state.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1489 Issue ." + e.getMessage());
				Exception(" BRM - 1489 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip("The requested textbook was the buy type is not found.");
		}
	}
}
