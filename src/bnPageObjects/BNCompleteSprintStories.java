package bnPageObjects;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNSprintStories;
import bnConfig.BNConstants;
import bnConfig.BNXpaths;
import bnStreamCalls.BNPromoMessageCall;
import bnStreamCalls.BNTopCategoryCampaignCall;

public class BNCompleteSprintStories extends BNSprintStories implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static boolean signedIn = false, frgtPassPage = false, createAccPage = false, BRM357 = false, fieldClear = false, accntCreated = false;
	static int sel = 0, mrno = 0, cmrno = 0, scno = 0;
	ArrayList<WebElement> webEle = new ArrayList<>();
	
	public BNCompleteSprintStories(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+loggedInAccntNamee+"")
	WebElement loggedInAccntName;
	
	@FindBy(xpath = ""+footerSignInn+"")
	WebElement footerSignIn;
	
	@FindBy(xpath = ""+signInn+"")
	WebElement signIn;
	
	@FindBy(xpath = ""+signInpageframee+"") 
	WebElement signInpageframe;
	
	@FindBy(xpath = ""+forgotPasspageframee+"") 
	WebElement forgotPasspageframe;
	
	@FindBy(xpath = ""+checkoutLogoo+"")
	WebElement checkoutLogo;
	
	@FindBy(xpath = ""+createAccbtnn+"")
	WebElement createAccbtn;
	
	@FindBy(xpath = ""+createAccountCreateBtnn+"")
	WebElement createAccountCreateBtn;
	
	@FindBy(xpath = ""+createAccountCancelBtnn+"")
	WebElement createAccountCancelBtn;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;
	
	@FindBy(xpath = ""+panCakeMenuMaskk+"")
	WebElement panCakeMenuMask;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+searchClosee+"")
	WebElement searchClose;
	
	@FindBy(xpath = ""+searchResultss+"")
	WebElement searchResults;
	
	@FindBy(xpath = ""+searchPagePopUpMaskk+"")
	WebElement searchPagePopUpMask;
	
	@FindBy(xpath = ""+searchResultsContainerr+"")
	WebElement searchResultsContainer;
	
	@FindBy(xpath = ""+searchContainerCancell+"")
	WebElement searchContainerCancel;
	
	@FindBy(xpath = ""+bnLogoo+"")
	WebElement bnLogo;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath =""+storeLocatorr+"")
	WebElement storeLocator;
	
	@FindBy(xpath = ""+promoCarousell+"")
	WebElement promoCarousel;
	
	@FindBy(xpath = ""+activePromoo+"")
	WebElement activePromo;
	
	@FindBy(xpath = ""+myAccountt+"")
	WebElement myAccount;
	
	@FindBy(xpath = ""+additionalLinkss+"")
	WebElement additionalLinks;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy (xpath = ""+pdpPageAddToWishListContainerr+"")
	WebElement pdpPageAddToWishListContainer;
	
	@FindBy(xpath = ""+plpSearchPageFilterContainerr+"")
	WebElement plpSearchPageFilterContainer;
	
	@FindBy (xpath = ""+searchPageSearchTermm+"")
	WebElement searchPageSearchTerm;
	
	@FindBy (xpath = ""+productListPagee+"")
	WebElement productListPage;
	
	@FindBy(xpath = ""+noSearchResultss+"")
	WebElement noSearchResults;
	
	@FindBy(xpath = ""+createAccFramee+"")
	WebElement createAccFrame;
	
	@FindBy(xpath = ""+signInlnkk+"")
	WebElement signInlnk;
	
	@FindBy(xpath = ""+forgotPasswordd+"")
	WebElement forgotPassword;
	
	@FindBy(xpath = ""+forgotContinueBtnn+"")
	WebElement forgotContinueBtn;
	
	@FindBy(xpath = ""+orderStatuss+"")
	WebElement orderStatus;
	
	@FindBy(xpath = ""+manageAccountt+"")
	WebElement manageAccount;
	
	@FindBy(xpath = ""+addressBookk+"")
	WebElement addressBook;
	
	@FindBy(xpath = ""+paymentss+"")
	WebElement payments;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+emailIdd+"")
	WebElement emailId;
	
	@FindBy(xpath = ""+passwordd+"")
	WebElement password;
	
	@FindBy(xpath = ""+forgotPasswordInfoo+"")
	WebElement forgotPasswordInfo;
	
	@FindBy(xpath = ""+forgotPasswordErrorr+"")
	WebElement forgotPasswordError;
	
	@FindBy(xpath = ""+forgotPasswordErrorr1+"")
	WebElement forgotPasswordError1;
	
	@FindBy(xpath = ""+forgotPasswordErrorr2+"")
	WebElement forgotPasswordError2;
	
	@FindBy(xpath = ""+forgotEmailDisplayy+"")
	WebElement forgotEmailDisplay;
	
	@FindBy(xpath = ""+forgotEmailEditt+"")
	WebElement forgotEmailEdit;
	
	@FindBy(xpath = ""+forgotEmailRecoveryOptionn+"")
	WebElement forgotEmailRecoveryOption;
	
	@FindBy(xpath = ""+forgotEmailRecoveryOptionTxtt+"")
	WebElement forgotEmailRecoveryOptionTxt;
	
	@FindBy(xpath = ""+forgotSecurityRecoveryOptionn+"")
	WebElement forgotSecurityRecoveryOption;
	
	@FindBy(xpath = ""+forgotSecurityRecoveryOptionTxtt+"")
	WebElement forgotSecurityRecoveryOptionTxt;
	
	@FindBy(xpath = ""+forgotSecurityAnswerFieldss+"")
	WebElement forgotSecurityAnswerFields;
	
	@FindBy(xpath = ""+forgotCancell+"")
	WebElement forgotCancel;
	
	@FindBy(xpath = ""+forgotSecurityAnswerTextBoxx+"")
	WebElement forgotSecurityAnswerTextBox;
	
	@FindBy(xpath = ""+forgotResetSuccessContainerr+"")
	WebElement forgotResetSuccessContainer;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss1+"")
	WebElement forgotSecurityResetSuccess1;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss2+"")
	WebElement forgotSecurityResetSuccess2;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss3+"")
	WebElement forgotSecurityResetSuccess3;
	
	@FindBy(xpath = ""+continueShopBtnn+"")
	WebElement continueShopBtn;
	
	@FindBy(xpath = ""+bagCountt+"")
	WebElement bagCount;
	
	@FindBy(xpath = ""+forgotPasswordNeww+"")
	WebElement forgotPasswordNew;
	
	@FindBy(xpath = ""+forgotPasswordNewCnfmm+"")
	WebElement forgotPasswordNewCnfm;
	
	@FindBy(xpath = ""+forgotPasswordruless+"")
	WebElement forgotPasswordrules;
	
	@FindBy(xpath = ""+forgotPasswordrulesiiconShoww+"")
	WebElement forgotPasswordrulesiiconShow;
	
	@FindBy(xpath = ""+forgotPasswordruleslistt+"")
	WebElement forgotPasswordruleslist;
	
	@FindBy(xpath = ""+forgotPasswordrulesiiconHidee+"")
	WebElement forgotPasswordrulesiiconHide;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertHeaderr+"")
	WebElement forgotPasswordResetSuccessAlertHeader;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertConfirmationn+"")
	WebElement forgotPasswordResetSuccessAlertConfirmation;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertCloseBtnn+"")
	WebElement forgotPasswordResetSuccessAlertCloseBtn;
	
	@FindBy(xpath = ""+customerServiceLinkk+"")
	WebElement customerServiceLink;
	
	@FindBy(xpath = ""+loginDisclaimerr+"")
	WebElement loginDisclaimer;
	
	@FindBy(xpath = ""+loginDisclaimerpolicyy+"")
	WebElement loginDisclaimerpolicy;
	
	@FindBy(xpath = ""+loginDisclaimerTermsofUsee+"")
	WebElement loginDisclaimerTermsofUse;
	
	@FindBy(xpath = ""+secureSignInn+"")
	WebElement secureSignIn;
	
	@FindBy(xpath = ""+signInErralrtt+"")
	WebElement signInErralrt;
	
	@FindBy(xpath = ""+miniCartLoadingGaugee+"")
	WebElement miniCartLoadingGauge;
	
	@FindBy(xpath = ""+TermsofUsePrivacyPolicyTitlee+"")
	WebElement TermsofUsePrivacyPolicyTitle;
	
	@FindBy(xpath = ""+rememberMee+"")
	WebElement rememberMe;
	
	@FindBy(xpath = ""+emailtxtt+"")
	WebElement emailtxt;
	
	@FindBy(xpath = ""+passwordtxtt+"")
	WebElement passwordtxt;
	
	@FindBy(xpath = ""+ccFNamee+"")
	WebElement ccFName;
	
	@FindBy(xpath = ""+ccFNametxtt+"")
	WebElement ccFNametxt;
	
	@FindBy(xpath = ""+ccLNamee+"")
	WebElement ccLName;
	
	@FindBy(xpath = ""+ccLNametxtt+"")
	WebElement ccLNametxt;
	
	@FindBy(xpath = ""+ccEmailIdd+"")
	WebElement ccEmailId;
	
	@FindBy(xpath = ""+ccEmailIdtxtt+"")
	WebElement ccEmailIdtxt;
		
	@FindBy(xpath = ""+ccCnfEmailIdd+"")
	WebElement ccCnfEmailId;
	
	@FindBy(xpath = ""+ccCnfEmailIdtxtt+"")
	WebElement ccCnfEmailIdtxt;
	
	@FindBy(xpath = ""+ccPasswordd+"")
	WebElement ccPassword;
	
	@FindBy(xpath = ""+ccPasswordtxtt+"")
	WebElement ccPasswordtxt;
	
	@FindBy(xpath = ""+ccCnfPasss+"")
	WebElement ccCnfPass;
	
	@FindBy(xpath = ""+ccCnfPasstxtt+"")
	WebElement ccCnfPasstxt;
	
	@FindBy(xpath = ""+ccSecurityQuestionn+"")
	WebElement ccSecurityQuestion;
	
	@FindBy(xpath = ""+ccSecurityQuestiontxtt+"")
	WebElement ccSecurityQuestiontxt;
	
	@FindBy(xpath = ""+ccSecurityAnswerr+"")
	WebElement ccSecurityAnswer;
	
	@FindBy(xpath = ""+ccSecurityAnswertxtt+"")
	WebElement ccSecurityAnswertxt;
	
	@FindBy(xpath = ""+erromsgg+"")
	WebElement erromsg;
	
	@FindBy(xpath = ""+signOutt+"")
	WebElement signOut;
	
	@FindBy(xpath = ""+signOutTempp+"")
	WebElement signOutTemp;
	
	@FindBy(xpath = ""+signOutIndicatorr+"")
	WebElement signOutIndicator;
	
	//aa
	
	@FindBy(how = How.XPATH,using = ""+promoCarousell+"")
	List<WebElement> promoCarouselList;
	
	@FindBy(how = How.XPATH,using = ""+displayedSearchResultss+"")
	List<WebElement> displayedSearchResults;
	
	@FindBy(how = How.XPATH,using = ""+mainMenuListss+"")
	List<WebElement> mainMenuLists;
	
	@FindBy(how = How.XPATH, using = ""+searchResultsListss+"")
	List<WebElement> searchResultsLists;
	
	@FindBy(how = How.XPATH, using = ""+searchSuggestedItemHighlightt+"")
	List<WebElement> searchSuggestedItemHighlight;
	
	@FindBy(how = How.XPATH, using = ""+searchPageProductListss+"")
	List<WebElement> searchPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+ccIndividualErroMsgg+"")
	List<WebElement> ccIndividualErroMsg;
	
	//aa
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	// Navigate to URL
	public void navigatetoURL() throws IOException
	{
		try
		{
			driver.get(BNConstants.mobileSiteURL);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	//Clear all the fields in the createAccountPage
	public void clearAll()
	{
		try
		{
			if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password)))
			{
				emailId.clear();
				password.clear();
			}
			else
			{
				Fail( "The Email and Password field is not displayed to clear.");
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in clearing the Email and Password.");
			Exception("Error in clearing the Email and Password.");
		}
	}
	
	/*//Clear all the fields in the createAccountPage
	public void ccclearAll()
	{
		fieldClear = false;
		try
		{
			if((BNBasicfeature.isElementPresent(ccFName))&&(BNBasicfeature.isElementPresent(ccLName))&&(BNBasicfeature.isElementPresent(ccEmailId))&&(BNBasicfeature.isElementPresent(ccCnfEmailId))&&(BNBasicfeature.isElementPresent(ccPassword))&&(BNBasicfeature.isElementPresent(ccCnfPass))&&(BNBasicfeature.isElementPresent(ccSecurityAnswer)))
			{
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);", pgetitle);
				BNBasicfeature.scrollup(ccFName, driver);
				Thread.sleep(500);
				ccFName.clear();
				ccLName.clear();
				BNBasicfeature.scrollup(emailId, driver);
				//Thread.sleep(500);
				ccEmailId.clear();
				ccCnfEmailId.clear();
				ccPassword.clear();
				ccCnfPass.clear();
				ccSecurityAnswer.clear();
				//Thread.sleep(500);
				js.executeScript("arguments[0].scrollIntoView(true);", pgetitle);
				Thread.sleep(500);
				BNBasicfeature.scrollup(ccFName, driver);
				Thread.sleep(500);
				ccFName.click();
				fieldClear = true;
			}
			else
			{
				Fail(" One or More of the fields are missing to clear the records.");
			}
		}
		catch (Exception e)
		{
			System.out.println("CLEAR ALL EXCEPTION");
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}*/
	
	//Clear all the fields in the createAccountPage
	public void ccclearAll(ArrayList<WebElement> ele)
	{
		fieldClear = false;
		try
		{
			int cnt = ele.size();
			BNBasicfeature.scrollup(checkoutLogo, driver);
			Thread.sleep(500);
			BNBasicfeature.scrollup(signInlnk, driver);
			for(int i = 0; i<cnt;i++)
			{
				BNBasicfeature.scrolldown(ele.get(i), driver);
				//Thread.sleep(50);
				ele.get(i).clear();
				fieldClear=true;
			}
			Thread.sleep(500);
			BNBasicfeature.scrollup(pgetitle, driver);
			Thread.sleep(100);
			webEle.removeAll(webEle);
		}
		catch (Exception e)
		{
			System.out.println("CLEAR ALL EXCEPTION");
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	// Sign In to the User Account
	public boolean signInExistingCustomer() throws IOException
	{
		Actions act = new Actions(driver);
		signedIn = false;
		try
		{
			if(!BNBasicfeature.isElementPresent(header))
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			if(BNBasicfeature.isElementPresent(menu))
			{
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				Thread.sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(signIn));
				Thread.sleep(100);
				act.moveToElement(signIn).click().build().perform();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				signedIn = true;
			}
		}
		catch (Exception e)
		{
			System.out.println(" Issue In Navigating to the Sign In Page.");
			Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
		return signedIn;
	}
	
	// Get Category
	public WebElement getMenuCategory(int mrno)
	{
		WebElement cat = null;
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category']")));
			List<WebElement> ele1 = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category']"));
			//List<WebElement> ele1 = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobSubCategoryCnt']//*[@level='level_']"));
			Random r = new Random();
			cmrno = r.nextInt(ele1.size());
			//cmrno = r.nextInt(ele1.size());
			if(cmrno<1||mrno==ele1.size())
			{
				cmrno = 1;
			}
			//scrolldown(driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]")));
			cat = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting the Menu Category Name.");
			Exception("Error in getting Menu Category Name");
		}
		return cat;
	}
	
	// Get Category
	public WebElement getMenuSubCategory()
	{
		WebElement cat = null;
		try
		{
			List<WebElement> subcateele = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@class='sk_mobSubCategory'])["+cmrno+"]//*[@level]"));
			Random r = new Random();
			scno = r.nextInt(subcateele.size());
			if(scno<=1)
			{
				scno = 1;
			}
			//System.out.println(scno);
			cat = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel__1'])["+cmrno+"]//*[@class='sk_mobCategoryItemTxt'])["+scno+"]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting the SubCategory Name.");
			Exception("Error in getting SubCategory Name");
		}
		return cat;
	}
	
	// Get Search Results
	public void getSearchResults(String srchtxt)
	{
		String srchTxt = srchtxt;
		try
		{
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				if(!BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
				}
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					 act.moveToElement(searchContainer).click().sendKeys(srchTxt).build().perform();
					 Thread.sleep(500);
					 int count = 0;
					 do
					 {
						 Thread.sleep(150);
						 wait.until(ExpectedConditions.and(
								 	ExpectedConditions.visibilityOf(searchResults),
								 	ExpectedConditions.invisibilityOfAllElements(displayedSearchResults),
								 	ExpectedConditions.visibilityOf(searchResultsContainer),
									ExpectedConditions.visibilityOfAllElements(searchResultsLists)
										)
									);
						 count++;
						 Thread.sleep(150);
					 }while(count<3);
					 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				}
				else
				{
					Fail("The Search Results are not displayed.");
				}
			}
			else
			{
				Fail("The Search icon is Clicked and the Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Error in displaying the search results.");
			Exception("Error in displaying the search results.");
		}
	}
	
	// Load Billing Or Shipping Page
	public boolean ldPage(WebElement ele, WebElement ele1, WebElement ele2)
	{
		boolean createAccloaded = false;
		WebDriverWait w1 = new WebDriverWait(driver, 5);
		int count = 1;
		try
		{
			do
			{
				if(BNBasicfeature.isElementPresent(ele))
				{
					createAccloaded = true;
				}
				else
				{
					try
					{
						jsclick(ele1);
						w1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(ele2));
						if(BNBasicfeature.isElementPresent(ele))
						{
							createAccloaded = true;
						}
						count ++;
					}
					catch(Exception e)
					{
						createAccloaded = false;
					}
				}
			}while((createAccloaded==false)&&(count<5));
		}
		catch (Exception e) 
		{
			createAccloaded = false;
		}
		return createAccloaded;
	}
	
	/****************************************** Header Rotating Global Promo Message ***********************************************/
	
	/* BRM 47 - Verify whether the promo messages are displayed is top of the banner as per the creative*/
	public void promobannerpresent() throws InterruptedException
	{
		ChildCreation(" BRM 47 - Verify whether the promo messages are displayed is top of the banner as per the creative.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo message banner is present at the top of the page.");
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the pages.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 47 Issue ." + e.getMessage());
			Exception(" BRM - 47 Issue." + e.getMessage());
		}
	}
	
	/* BRM 48 - Verify whether clicking on the promo messages redirects to the respective page*/
	public void promoredirect()
	{
		ChildCreation(" BRM 48 - Verify whether clicking on the promo messages redirects to the respective page.");
		try
		{
			log.add("The browser is launched and the URL is successfully loaded.");
			wait.until(ExpectedConditions.visibilityOf(header));
			String currUrl = driver.getCurrentUrl();
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo message banner is present at the top of the page.",log);
				wait.until(ExpectedConditions.visibilityOf(activePromo));
				if(BNBasicfeature.isElementPresent(activePromo))
				{
					wait.until(ExpectedConditions.elementToBeClickable(activePromo));
					String promotxt = activePromo.getText();
					activePromo.click();
					//jsclick(activePromo);
					//Thread.sleep(5000);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.or(
							ExpectedConditions.visibilityOf(menu)
								)
						);
					String navUrl = driver.getCurrentUrl().toString();
					log.add( "The URL before navigation was : " + currUrl);
					log.add( "The URL after navigation is : " + navUrl);
					if(currUrl.equals(navUrl))
					{
						Fail( "The User is not navigated to the expected url.",log);
					}
					else
					{
						log.add(" The user is navigated to the " +  navUrl + " page after clicking the " + promotxt.toString());
						Pass("The user is redirected to the Page successfully.",log);	
						driver.navigate().back();
						wait.until(ExpectedConditions.visibilityOf(header));
					}
				}
				else
				{
					Fail("The user is not redirected to the Page successfully.",log);
				}
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 48 Issue ." + e.getMessage());
			Exception(" BRM - 48 Issue." + e.getMessage());
		}
	}

	/* BRM 49 - Promo : The promo messages should match with the classic site and should be rotate based on time interval */
	public void promocomparison() throws MalformedURLException, JSONException, InterruptedException
	{
		ChildCreation(" BRM 49 - Promo : The promo messages should match with the classic site and should be rotate based on time interval.");
		try 
		{
			//driver.get(BNConstants.mobileSiteURL);
			wait.until(ExpectedConditions.visibilityOf(header));
			ArrayList<String> urlprmoresp = new ArrayList<>();
			ArrayList<String> mobileresp = new ArrayList<>();
			urlprmoresp = BNPromoMessageCall.promoCall(log);
			wait.until(ExpectedConditions.visibilityOf(header));
			//List<WebElement> promoMessageSize = promoCarouselList;
			//System.out.println(promoMessageSize.size());
			if(BNBasicfeature.isElementPresent(promoCarousel)&&(BNBasicfeature.isListElementPresent(promoCarouselList)))
			{
				for(int j = 0; j<urlprmoresp.size(); j++)
				{
					String status;
					String promoName="";
					try
					{
						do
						{
							wait.until(ExpectedConditions.attributeContains(promoCarouselList.get(j), "class", "active"));
							//status = driver.findElement(By.xpath("//*[@class='sk_hdr_prmBnrCarousel']//li["+(j+1)+"]")).getAttribute("class");
							status = promoCarouselList.get(j).getAttribute("class");
							//Thread.sleep(5000);
							//promoName= driver.findElement(By.xpath("//*[@class='sk_hdr_prmBnrCarousel']//li["+(j+1)+"]")).getText().replaceAll("[^\\x00-\\x7F]","").replaceAll("\\s+", "");
							promoName= promoCarouselList.get(j).getText().replaceAll("[^\\x00-\\x7F]","").replaceAll("\\s+", "");
							//System.out.println(" Promo Name : " + promoName);
						}
						while(status.equals(""));
						mobileresp.add(promoName);
					}
					catch (Exception e) 
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong. Please Check." + e.getMessage());
						break;
					}
					/*wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//*[@class='sk_hdr_prmBnrCarousel']/ul)//li["+(j+1)+"][@class='active']")));
					String Name=driver.findElement(By.xpath("(//*[@class='sk_hdr_prmBnrCarousel']/ul)//li["+(j+1)+"][@class='active']")).getText();
					String promoname = promoresarray.getJSONObject(j).getString("name");
					if(Name.equals(promoname))
					{
						System.out.println("Pass");
					}
					else
					{
						System.out.println("Fail");
					}*/
				}
						
				if((urlprmoresp.size()>0)&&(mobileresp.size()>0))
				{
					log.add("The promo name are succesfully fetched.");
					if(mobileresp.equals(urlprmoresp))
					{
						log.add("The value fetched from the JSON " + urlprmoresp + " and the value from the mobile webpage banner is " + mobileresp);
						Pass("The Promo message content in the banner matches.",log);
					}
					else
					{
						log.add("The value fetched from the JSON " + urlprmoresp + " and the value from the mobile webpage banner is " + mobileresp);
						Fail("The Promo message content in the banner does not matches.",log);
					}
				}
				else
				{
					log.add("The Value from the JSON " + urlprmoresp);
					log.add("The Value from the Mobile " + mobileresp);
					Fail("The array seems to be empty.",log);
				}
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the page.");
			}
		} 
		catch (Exception e) 
		{
			System.out.println(" BRM - 49 Issue ." + e.getMessage());
			Exception(" BRM - 49 Issue." + e.getMessage());
		}
	}
	
	/* BRM 120 - Verify that promo banner at the header should be rotate with the periodical interval of time*/
	public void promochangetime()
	{
		ChildCreation(" BRM 120 - Verify that promo banner at the header should be rotate with the periodical interval of time.");
		try 
		{
			log.add("The browser is launched and the URL is successfully loaded.");
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				List<WebElement> listlen = driver.findElements(By.xpath("//*[@class='sk_hdr_prmBnrCarousel']//li"));
				if(listlen.size()>0)
				{
					for(int j = 0; j<listlen.size(); j++)
					{
						String status;
						String promoName;
						long startTime ;
						do
						{
							startTime = System.currentTimeMillis();
							wait.until(ExpectedConditions.attributeContains(promoCarouselList.get(j), "class", "active"));
							//status = driver.findElement(By.xpath("//*[@class='sk_hdr_prmBnrCarousel']//li["+(j+1)+"]")).getAttribute("class");
							status = promoCarouselList.get(j).getAttribute("class");
							//Thread.sleep(5000);
							//promoName=driver.findElement(By.xpath("//*[@class='sk_hdr_prmBnrCarousel']//li["+(j+1)+"]")).getText();
							promoName = promoCarouselList.get(j).getText();
							
						} 	
						while(status.equals(""));
							
						long endTime = System.currentTimeMillis();
						long milliseconds = (endTime - startTime);
						long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime - startTime);
						log.add("The banner promo " + promoName + " visible for " + timeSeconds + " Second / " + milliseconds +" milliseconds." );
					}
				Pass("The banner changes promo rotates frequently",log);
				}
				else
				{
					Fail("The promo content list seems to be empty. Please Check.");
				}
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the page.",log);
			}
		}
		catch (Exception e) 
		{
			System.out.println(" BRM - 120 Issue ." + e.getMessage());
			Exception(" BRM - 120 Issue." + e.getMessage());
		}
	}
	
	/*************************************************************BRM - 56 Pancake Stories *********************************************************************************/
	
	/* BRM - 3 Verify that "Hamburger Menu","Search","Logo","Store Locator","Shopping Bag" icons should be displayed at the header along with the promo banner */
	public void elementpresent()
	{
		ChildCreation(" BRM - 3 Verify that Hamburger Menu,Search,Logo,Store Locator,Shopping Bag icons should be displayed at the header along with the promo banner .");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				Pass("The Pancake Menu is displayed / present.");
			}
			else
			{
				Fail("The Pancake Menu is not displayed / present.");
			}
			
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				Pass("The Search Icon is displayed / present.");
			}
			else
			{
				Fail("The Search Icon is displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(bnLogo))
			{
				Pass("The Barnes and Noble logo is displayed / present.");
			}
			else
			{
				Fail("The Barnes and Noble logo is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(storeLocator))
			{
				Pass("The Store Locator icon is displayed / present.");
			}
			else
			{
				Fail("The Store Locator icon is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				Pass("The Shopping Bag icon is displayed / present.");	
			}
			else
			{
				Fail("The Shopping Bag icon is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo Carousel icon is displayed / present.");
			}
			else
			{
				Fail("The Promo Carousel is not displayed / present.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 3 Issue ." + e.getMessage());
			Exception(" BRM - 3 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 4 Verify whether clicking on hamburger menu displays the sub menus and again clicking on hamburger menu collapses the sub menus*/
	public void menuopencollapse()
	{
		ChildCreation(" BRM - 4 Verify whether clicking on hamburger menu displays the sub menus and again clicking on hamburger menu collapses the sub menus.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				log.add("The browser is launched and the user is navigated to the URL");
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(panCakeMenuMask));
				if(BNBasicfeature.isElementPresent(panCakeMenuMask))
				{
					Pass("The Menu is successfully clicked and the Submenus are displayed.",log);
					jsclick(panCakeMenuMask);
					log.add("The Menu Mask area is successfully clicked.");
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					wait.until(ExpectedConditions.visibilityOf(bagIcon));
					if(BNBasicfeature.isElementPresent(menu))
					{
						Pass("The Submenu is successfully collapsed and the Menu icon is once again displayed / visible.",log);
					}
					else
					{
						Fail("The Submenu is not collapsed and the Menu icon is not displayed / visible .",log);
					}
				}
				else
				{
					Fail("The Menu Mask area is not visible to click.");
				}
			}
			else
			{
				Fail("The pancake menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 4 Issue ." + e.getMessage());
			Exception(" BRM - 4 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 5 Verify whether clicking on hamburger menu displays the sub menus and clicking on �+� or Item name can expand the menu [Sub menus too] and clicking on �-� or item name collapses the menu [ Sub menus too].*/
	public void menusubmenuclick()
	{
		ChildCreation(" BRM - 5 Verify whether clicking on hamburger menu displays the sub menus and clicking on + or Item name can expand the menu [Sub menus too] and clicking on - or item name collapses the menu [ Sub menus too].");
		//System.out.println(3);
		try
		{
			mrno = 0; cmrno = 0;
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
			}
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				log.add("The browser is launched and the user is navigated to the URL");
				jsclick(menu);
				Pass("The Menu is successfully clicked and the Submenus are displayed.");
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				if(BNBasicfeature.isElementPresent(menuLists))
				{
					List<WebElement> ele = mainMenuLists;
					Random r = new Random();
					mrno = r.nextInt(ele.size());
					BNBasicfeature.scrolldown(mainMenuLists.get(mrno), driver);
					WebElement subMenu = mainMenuLists.get(mrno);
					//WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobCategoryItemLink '])"));
					WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[contains(@class,'sk_mobCategoryItemLink ')])"));
					log.add("The Selected Category from the list is " + subMenu.getText().toString());
					act.moveToElement(subMenu).click().build().perform();
					Pass("The Category is successfully clicked and the clicked Category is " + subMenu.getText().toString(),log);
					BNBasicfeature.scrollup(subMenu, driver);
					WebElement category = getMenuCategory(mrno);
					log.add("The Selected Sub Category from the list is " + category.getText().toString());
					Thread.sleep(100);
					act.moveToElement(category).click().build().perform();
					//Thread.sleep(2000);
					Pass("The Sub Category is successfully clicked and the clicked Subcategory is " + category.getText().toString(),log);
					
					String cat = category.getAttribute("class");
					if(!cat.isEmpty())
					{
						WebElement icon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]//*[@class='sk_mobCategoryItemLink ']"));
						act.moveToElement(icon).click().build().perform();
						//jsclick(icon);
						act.moveToElement(sicon).click().build().perform();
						Pass("The Sub Category are collapsed successfully when clicked.");
					}
					
					BNBasicfeature.scrolldown(subMenu, driver);
					Thread.sleep(100);
					//jsclick(subMenu);
					Pass("The Category are collapsed successfully when clicked.");
					BNBasicfeature.scrollup(myAccount, driver);
					Thread.sleep(100);
					jsclick(panCakeMenuMask);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					Pass("The User is able to close the menu successfully.");
				}
				else
				{
					Fail("The Menu list is not displayed / visible.");
				}
			}
			else
			{
				Fail("The pancake menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 5 Issue ." + e.getMessage());
			Exception(" BRM - 5 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 6 Verify whether the scrolling of categories and sub categories in hamburger menu are displayed properly.*/
	public void menuscroll()
	{
		ChildCreation(" BRM - 6 Verify whether the scrolling of categories and sub categories in hamburger menu are displayed properly.");
		//System.out.println(4);
		try
		{
			mrno = 0;
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
			}
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			BNBasicfeature.scrollup(header, driver);
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				log.add("The Menu is successfully clicked and the Submenus are displayed.");
				if(BNBasicfeature.isElementPresent(additionalLinks)&&(BNBasicfeature.isListElementPresent(mainMenuLists)))
				{
					BNBasicfeature.scrolldown(additionalLinks, driver);
					Pass("The Menu list is scrolled Down successfully.",log);
					BNBasicfeature.scrollup(myAccount, driver);
					Pass("The Menu list is scrolled Up successfully.");
					wait.until(ExpectedConditions.visibilityOf(myAccount));
				
					List<WebElement> ele = mainMenuLists;
					Random r = new Random();
					mrno = r.nextInt(ele.size());
					WebElement subMenu = mainMenuLists.get(mrno);
					WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobCategoryItemLink '])"));
					log.add("The Selected Category from the list is " + subMenu.getText().toString());
					act.moveToElement(subMenu).click().build().perform();
					Thread.sleep(100);
					BNBasicfeature.scrolldown(subMenu, driver);
					Pass("The User is scrolled down to the Category successfully.",log);
					log.add("The Category is successfully clicked and the clicked Category is " + subMenu.getText().toString());
					WebElement category = getMenuCategory(mrno);
					log.add("The Selected Sub Category from the list is " + category.getText().toString());
					act.moveToElement(category).click().build().perform();
					BNBasicfeature.scrolldown(category, driver);
					Pass("The User is scrolled down to the SubCategories successfully.",log);
					BNBasicfeature.scrollup(myAccount, driver);
					act.moveToElement(sicon).click().build().perform();
					jsclick(panCakeMenuMask);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					Pass("The User is able to close the menu successfully.");
				}
				else
				{
					Fail("The Menu list is not displayed / visible.");
				}
			}
			else
			{
				Fail("The Menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 6 Issue ." + e.getMessage());
			Exception(" BRM - 6 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 7 Verify whether clicking on categories are forwarding to the appropriate links.*/
	public void catenavigation()
	{
		ChildCreation(" BRM - 7 Verify whether clicking on categories are forwarding to the appropriate links.");
		try
		{
			String befUrl = driver.getCurrentUrl();
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			mrno = 0; cmrno = 0;
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				List<WebElement> menuele = mainMenuLists;
				Random r = new Random();
				mrno = r.nextInt(menuele.size());
				WebElement mName = mainMenuLists.get(mrno);
				BNBasicfeature.scrolldown(mName, driver);
				act.moveToElement(mName).click().build().perform();
				Thread.sleep(100);
				BNBasicfeature.scrollup(mName, driver);
				List<WebElement> catele = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@type='category']"));
				cmrno = r.nextInt(catele.size());
				if(cmrno<=1)
				{
					cmrno = 1;
				}
				//System.out.println(cmrno);
				WebElement cName = getMenuCategory(mrno);
				BNBasicfeature.scrolldown(cName, driver);
				act.moveToElement(cName).click().build().perform();
				Thread.sleep(100);
				WebElement subcName = getMenuSubCategory();
				BNBasicfeature.scrolldown(subcName, driver);
				String subCatName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel__1'])["+cmrno+"]//*[@class='sk_mobCategoryItemTxt'])["+scno+"]")).getText();
				System.out.println(subCatName);
				act.moveToElement(subcName).click().build().perform();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				String currUrl = driver.getCurrentUrl();
				//System.out.println(currUrl);
				String link = BNTopCategoryCampaignCall.link(log, mrno, cmrno, scno);
				String[] brkCurLink = null;
				@SuppressWarnings("unused")
				boolean helpPage = false;
				@SuppressWarnings("unused")
				boolean deptPlp = false;
				@SuppressWarnings("unused")
				boolean ean = false;
				if(currUrl.contains("/h/"))
				{
					brkCurLink = link.split("/h/");
					helpPage  = true;
				}
				else if(currUrl.contains("nookpress"))
				{
					
				}
				else if(currUrl.contains("ean"))
				{
					brkCurLink = link.split("ean=");
					ean = true;
				}
				else 
				{
					brkCurLink = link.split("_/");
					deptPlp = true;
				}
				
				//System.out.println(link);
				boolean linkTrue = false; 
				for(int i = 0; i<=brkCurLink.length;i++)
				{
					if(currUrl.contains(brkCurLink[i]))
					{
						linkTrue = true;
						break;
					}
					else
					{
						linkTrue = false;
						continue;
					}
				}
				
				/*if(linkTrue==true)
				{
					Pass("The User is redirected to the expected url link.");
					
					if(deptPlp==true)
					{
						if(BNBasicfeature.isElementPresent(productListTab))
						{
							System.out.println("P");
							if(BNBasicfeature.isElementPresent(searchTerm))
							{
								String title = searchTerm.getText();
								if(title.isEmpty())
								{
									System.out.println("F");
								}
								else
								{
									if(subCatName.contains(title))
									{
										System.out.println("P");
									}
									else
									{
										System.out.println("F");
									}
								}
							}
						}
						else if(BNBasicfeature.isElementPresent(storeTitle))
						{
							String title = storeTitle.getText();
							if(title.isEmpty())
							{
								System.out.println("F");
							}
							else
							{
								if(title.contains(subCatName))
								//if(subCatName.contains(title))
								{
									System.out.println("P");
								}
								else
								{
									System.out.println("F");
								}
							}
						}
					}
					else if(helpPage==true)
					{
						System.out.println("P");
					}
					else if(ean == true)
					{
						String title = storeTitle.getText();
						if(title.isEmpty())
						{
							System.out.println("F");
						}
						else
						{
							if(title.contains(subCatName))
							//if(subCatName.contains(title))
							{
								System.out.println("P");
							}
							else
							{
								System.out.println("F");
							}
						}
					}
					else
					{
						System.out.println("F");
					}
					
						
				}
				else
				{
					if(BNBasicfeature.isElementPresent(productListTab))
					{
						System.out.println("P");
					}
					else if(BNBasicfeature.isElementPresent(storeTitle))
					{
						System.out.println("P");
					}
					else
					{
						System.out.println("F");
					}
				}*/
				
				if(linkTrue==true)
				{
					Pass("The user is navigated to the expected Url.");
				}
				else
				{
					Fail("The user is not navigated to the expected Url.");
				}
				
				if(!befUrl.equals(currUrl))
				{
					driver.navigate().back();
				}
				
			}
			else
			{
				Fail("The Menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 7 Issue ." + e.getMessage());
			Exception(" BRM - 7 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 8 The subcategory sections in the Pancake should be highlighted in different color */
	public void menusubmenucolor()
	{
		ChildCreation(" BRM - 8 Verify that the subcategory sections in the Pancake should be highlighted in different color.");
		//System.out.println(5);
		int mrno,cmrno; 
		try
		{
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			String csvalue;
			//String menutxtColor = sheet.getRow(i).getCell(2).getStringCellValue();
			String menutxtColor = BNBasicfeature.getExcelVal("BRM8", sheet, 2);
			//String cattxtColor = sheet.getRow(i).getCell(3).getStringCellValue();
			String cattxtColor = BNBasicfeature.getExcelVal("BRM8", sheet, 3);
			//String subcattxtColor = sheet.getRow(i).getCell(4).getStringCellValue();
			String subcattxtColor = BNBasicfeature.getExcelVal("BRM8", sheet, 4);
			Thread.sleep(100);
			if(BNBasicfeature.isElementPresent(menu))
			{
			    log.add("The browser is launched and the user is navigated to the URL");
			    jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				List<WebElement> ele = mainMenuLists;
				Random r = new Random();
				mrno = r.nextInt(ele.size());
				//System.out.println("Menu Selected : " + mrno);	
				WebElement subMenu = driver.findElement(By.xpath("//*[@id='sk_mobCategoryItemCont_id_"+(mrno)+"_0']/div[1]"));
				WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobCategoryItemLink '])"));
				csvalue = subMenu.getCssValue("color");
				String menuhex = BNBasicfeature.colorfinder(csvalue);
				act.moveToElement(subMenu).click().build().perform();
				BNBasicfeature.scrollup(subMenu, driver);
				List<WebElement> ele1 = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category']"));
				cmrno = r.nextInt(ele1.size());
				if(cmrno<1)
				{
					cmrno = 1;
				}
				
				//System.out.println("Submenu Selected : " + cmrno);	
				WebElement category = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]//*[@class='sk_mobCategoryItemTxt']"));
				act.moveToElement(category).click().build().perform();
				Thread.sleep(100);
				csvalue = subMenu.getCssValue("color");
				String catehex = BNBasicfeature.colorfinder(csvalue);
				
				WebElement subcategory = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@class='sk_mobSubCategory'])["+(cmrno)+"]//*[@level='level_2']//*[@class='sk_mobCategoryItemTxt'])[1]"));
				WebElement icon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]//*[@class='sk_mobCategoryItemLink ']"));
				csvalue = subcategory.getCssValue("color");
				String subcatehex = BNBasicfeature.colorfinder(csvalue);
				act.moveToElement(icon).click().build().perform();
				act.moveToElement(sicon).click().build().perform();
				log.add("The expected color  : " + menutxtColor);
				log.add("The actual color of the menu : " + menuhex);
				if(menuhex.equals(menutxtColor))
				{
					Pass("The Menu Name is in Expected Color.",log);
				}
				else
				{
					Fail("The Menu Name is not in Expected Color.The expected color was " + menutxtColor + " but the current menu color is : " + menuhex,log);
				}
				
				log.add("The expected color  : " + cattxtColor);
				log.add("The actual color of the menu : " + catehex);
				if(catehex.equals(cattxtColor))
				{
					Pass("The Category Name is in Expected Color.",log);
				}
				else
				{
					Fail("The Category Name is not in Expected Color.The expected color was " + cattxtColor + " but the current category color is : " + catehex,log);
				}
							
				log.add("The expected color  : " + subcattxtColor);
				log.add("The actual color of the menu : " + subcatehex);
				if(subcatehex.equals(subcattxtColor))
				{
					Pass("The Sub Category is in Expected Color.",log);
				}
				else
				{
					Fail("The Sub Category is not in Expected Color.The expected color was " + subcattxtColor + " but the current sub Category color is : " + subcatehex,log);
				}
				BNBasicfeature.scrollup(myAccount, driver);
				jsclick(panCakeMenuMask);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
			}
			else
			{
				Fail("The Menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 8 Issue ." + e.getMessage());
			Exception(" BRM - 8 Issue." + e.getMessage());
		}
	}

	/* BRM - 10 Hamburger Menu : All the categories available in the classic site, should be present in the menu*/
	public void menuComparision() throws IOException, JSONException, InterruptedException
	{
		ChildCreation(" BRM - 10 Hamburger Menu : All the categories available in the classic site, should be present in the menu");
		ArrayList<String> Menu = new ArrayList<>();
		ArrayList<String> Category = new ArrayList<>();
		ArrayList<String> SubCategory = new ArrayList<>();
		
		ArrayList<String> MMenu = new ArrayList<>();
		ArrayList<String> MCategory = new ArrayList<>();
		ArrayList<String> MSubCategory = new ArrayList<>();
		
		List<List<String>> res = BNTopCategoryCampaignCall.menudetails(log);
		Menu = (ArrayList<String>) res.get(0);
		Category = (ArrayList<String>) res.get(1);
		SubCategory = (ArrayList<String>) res.get(2);
		
		try
		{
			//navigatetoURL();
			driver.get(BNConstants.mobileSiteURL);
			jsclick(menu);
			wait.until(ExpectedConditions.visibilityOf(myAccount));
			wait.until(ExpectedConditions.visibilityOf(menuLists));
			List<WebElement> menuele = mainMenuLists;
			Actions act = new Actions(driver);
			for(int i = 0; i<menuele.size(); i++)
			{
				try
				{
					WebElement mName = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItem']//*[@id='sk_mobCategoryItemCont_id_"+i+"_0']"));
					String menuName = mName.getText();
					if(!menuName.isEmpty())
					{
						MMenu.add(menuName);
						jsclick(mName);
						wait.until(ExpectedConditions.attributeContains(By.xpath("((//*[@class='sk_mobCategoryItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory'])[1]"), "style", "block"));
						Thread.sleep(100);
						BNBasicfeature.scrollup(mName, driver);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id='id_skCtgryItmShopCnt_"+i+"']//*[@class='sk_ctgryItmShopTxt']"))));
						/*WebElement shopall = driver.findElement(By.xpath("//*[@id='id_skCtgryItmShopCnt_"+i+"']//*[@class='sk_ctgryItmShopTxt']"));
						MCategory.add(shopall.getText());*/
						List<WebElement> catele = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category']"));
						for(int j = 1; j <= catele.size(); j++)
						{
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]")));
							WebElement cName = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]"));
							BNBasicfeature.scrollup(cName, driver);
							MCategory.add(cName.getText());
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]"))));
							act.moveToElement(cName).click().build().perform();
							//Thread.sleep(2000);
							BNBasicfeature.scrollup(cName, driver);
							//Thread.sleep(100);
							List<WebElement> subcateele = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@class='sk_mobSubCategory'])["+j+"]//*[@level]"));
							for(int k = 1; k<=subcateele.size(); k++)
							{
								String nook = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@class='sk_mobSubCategory'])["+j+"]//*[@level])["+k+"]")).getAttribute("aria-label");
								if(nook.equals("My NOOK Library"))
								{
									continue;
								}
								else
								{
									wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]")));
									WebElement subcName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]"));
									BNBasicfeature.scrollup(subcName, driver);
									//Thread.sleep(100);
									MSubCategory.add(subcName.getText());
								}
							}
						}
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something missing." + e.getMessage());
					continue;
				}
			}
			
			if((Menu.size()>0) && (MMenu.size()>0))
			{
				log.add("Comparison results of the Classic site and Mobile Site Menu Names.");
				/*System.out.println(Menu.size());
				System.out.println(Menu);
				System.out.println(MMenu.size());
				System.out.println(MMenu);*/
				String MenuName = MMenu.toString();
				Menu.removeAll(MMenu);
				if(Menu.isEmpty())
				{
					log.add("The Menu Contents from the Site are : " + MenuName);
					Pass("The Menu Contents of both the Classic and Mobile Site Matches.",log);
				}
				else
				{
					log.add("The following menu is Missing/Mismatching : " + MMenu.toString());
					Fail("The Menu Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
			
			if((Category.size()>0) && (MCategory.size()>0))
			{
				log.add("Comparison results of the Classic site and Mobile Site Category Names.");
				/*System.out.println(Category.size());
				System.out.println(Category);
				System.out.println(MCategory.size());
				System.out.println(MCategory);*/
				String CategoryName = MCategory.toString();
				Category.removeAll(MCategory);
				if(Category.isEmpty())
				{
					log.add("The Category Contents from the Site are : " + CategoryName);
					Pass("The Category Contents of both the Classic and Mobile Site Matches.");
				}
				else
				{
					log.add("The following Category are Missing/Mismatching : " + MCategory.toString());
					Fail("The Category Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
			
			if((SubCategory.size()>0) && (MSubCategory.size()>0))
			{
				log.add("Comparison results of Classic site and Mobile Site Sub-Category Menu Names.");
				/*System.out.println(SubCategory.size());
				System.out.println(SubCategory);
				System.out.println(MSubCategory.size());
				System.out.println(MSubCategory);*/
				String SubCategoryName = MSubCategory.toString();
				SubCategory.removeAll(MSubCategory);
				if(SubCategory.isEmpty())
				{
					log.add("The Sub-Category Contents from the Site are : " + SubCategoryName);
					Pass("The Sub-Category Menu Contents of both the Classic and Mobile Site Matches.",log);
				}
				else
				{
					log.add("The following Sub-Category Menu are Missing/Mismatching : " + MSubCategory.toString());
					Fail("The Sub-Category Menu Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 10 Issue ." + e.getMessage());
			Exception(" BRM - 10 Issue." + e.getMessage());
		}
	}
	
	
	/*********************************************************** Search Functionality *****************************************************************/
	
	/* BRM - 13 Verify whether clicking on search icon displays a search field with cancel option */
	/* BRM - 29 Verify that the default text is displayed in the search box. */
	public void searchIcon()
	{
		ChildCreation(" BRM - 13 Verify whether clicking on search icon displays a search field with cancel option .");
		try
		{
			String canceltxt = "";
			/*if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]"))
						)
						);
				driver.get(BNConstants.mobileSiteURL);
			}*/
			int count = 1;
			do
			{
				try
				{
					driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
					driver.get(BNConstants.mobileSiteURL);
					break;
				}
				catch (Exception e)
				{
					driver.navigate().refresh();
					System.out.print("There is something wrong with the URL Please Check." + e.getMessage());
					count++;
				}
			}while(count<5);
			wait.until(ExpectedConditions.visibilityOf(header));
			canceltxt = BNBasicfeature.getExcelVal("BRM13", sheet, 2); 
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				log.add("The Search icon is displayed.");
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					log.add("The Search icon is Clicked and the Search Field is displayed.");
					if(BNBasicfeature.isElementPresent(searchContainerCancel))
					{
						Pass("The Cancel Button is displayed.");
						log.add( "The Expected Value was : " + canceltxt);
						String val = searchContainerCancel.getText();
						log.add( "The actual Value is : " + val);
						if(val.equals(canceltxt))
						{
							Pass("The Cancel Button text matches.",log);
						}
						else
						{
							Fail("The Cancel Button text does not matches.",log);
						}
					}
					else
					{
						Fail("The Cancel Button is not displayed.",log);
					}
				}
				else
				{
					Fail("The Search icon is Clicked and the Search Container is not displayed.",log);
				}
			}
			else
			{
				Fail("The Search icon is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 13 Issue ." + e.getMessage());
			Exception(" BRM - 13 Issue." + e.getMessage());
		}
		
		/* BRM - 29 Verify that the default text is displayed in the search box. */
		ChildCreation(" BRM - 29 Verify that the default text is displayed in the search box.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM29", sheet, 2); 
			log.add( "The expected value was : " + srchtxt);
			String val = searchContainer.getAttribute("placeholder");
			log.add( "The actual value is : " + srchtxt);
			if(val.equals(srchtxt))
			{
				Pass("The Search text box placeholder with the Search Caption is present.",log);
			}
			else
			{
				Fail("The Search text box placeholder with the Search Caption is not visible.",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 29 Issue ." + e.getMessage());
			Exception(" BRM - 29 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 12 Verify whether clicking on cancel clears the text in the search field*/
	public void searchCancel()
	{
		ChildCreation(" BRM - 12 Verify whether clicking on cancel clears the text in the search field.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				String srchtxt =  BNBasicfeature.getExcelVal("BRM12", sheet, 3);
				getSearchResults(srchtxt);
				wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				wait.until(ExpectedConditions.visibilityOf(searchResultsContainer));
				jsclick(searchContainerCancel);
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					Fail("The Search Container is not closed and it is still Open.",log);
				}
				else
				{
					Pass("The Search Container is closed and it is not Open.",log);
				}
			}
			else
			{
				Fail( "The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 12 Issue ." + e.getMessage());
			Exception(" BRM - 12 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 14 Verify whether the search field provides inline pop up with correctly 8 matching results*/
	/* BRM - 15 Enter some characters in search field and verify whether the characters are highlighted in inline pop up*/
	/* BRM - 16 Verify whether clicking on search icon displays only search field, Promo messages,Cross and Cancel options in the screen*/
	/* BRM - 20 On entering any values the cross mark should be displayed*/
	/* BRM - 88 Verify whether the search icon is static in empty search text box.*/
	public void searchCount() throws InterruptedException
	{
		ChildCreation(" BRM - 14 Verify whether the search field provides inline pop up with correctly 8 matching results.");
		try
		{
			ArrayList<String> mobileSiteSrch= new ArrayList<>();
			String srchtxt =  BNBasicfeature.getExcelVal("BRM14", sheet, 3);
			wait.until(ExpectedConditions.visibilityOf(header));
			getSearchResults(srchtxt);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			if(BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				 for(WebElement srchsugg : searchResultsLists)
				 {
					 mobileSiteSrch.add(srchsugg.getText());
				 }
				 if((mobileSiteSrch.size()>0) && (mobileSiteSrch.size()<=8))
				 {	
					log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					Pass("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion of only 8 and not more than 8. ");
			     }
				 else
			 	 {
			 		log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
			 		Fail("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion more than 8 or the list is empty. ");
			 	 }
			}
			else
			{
				Fail("The Search Results is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 14 Issue ." + e.getMessage());
			Exception(" BRM - 14 Issue." + e.getMessage());
		}
		
		/* BRM - 15 Enter some characters in search field and verify whether the characters are highlighted in inline pop up*/
		ChildCreation(" BRM - 15 Enter some characters in search field and verify whether the characters are highlighted in inline pop up.");
		try
		{
			String font = BNBasicfeature.getExcelVal("BRM15", sheet, 5);
			String srchtxt =  BNBasicfeature.getExcelVal("BRM15", sheet, 3);
			if(searchContainer.getAttribute("value").isEmpty())
			{
				getSearchResults(srchtxt);
			}
			
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			if(!searchContainer.getAttribute("value").equals(""))
			{
				if(BNBasicfeature.isListElementPresent(searchResultsLists))
				{
					log.add("The Search Results is displayed.");
					for(int j = 1 ; j<searchResultsLists.size(); j++)
					{
						WebElement txt = searchSuggestedItemHighlight.get(j-1);
						String txthighlight = txt.getCssValue("font-weight");
						if(txthighlight.equals(font))
						{
							Pass("The entered Search Keyword " + txt.getText() + " is highlighted. The font weight is " + txthighlight,log);
						}
						else
						{
							Fail("The entered Search Keyword " + txt.getText() + " is highlighted. The font weight is  " + txthighlight,log);
						}
					}
				}
				else
				{
					Fail("The Search Results is not displayed.");
				}
			}
			else
			{
				Fail( "The Search Container is empty.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 15 Issue ." + e.getMessage());
			Exception(" BRM - 15 Issue." + e.getMessage());
		}
		
		/* BRM - 16 Verify whether clicking on search icon displays only search field, Promo messages,Cross and Cancel options in the screen*/
		ChildCreation(" BRM - 16 Verify whether clicking on search icon displays only search field, Promo messages,Cross and Cancel options in the screen.");
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			boolean res = (promoCarousel)!=null;
			if(res==true)
			{
				Pass("The Promo Banner message at the top of the page is displayed.",log);
			}
			else
			{
				Fail("The Promo Banner message at the top of the page is not displayed.",log);
			}
			
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				Pass("The Search Container is displayed.");
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(searchContainerCancel))
			{
				Pass("The Cancel button  in the Search Container is displayed.");
			}
			else
			{
				Fail("The Cancel button in the Search Container is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				Pass("The Close icon button in the Search Container is displayed.");
			}
			else
			{
				Fail("The Close icon button in the Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 15 Issue ." + e.getMessage());
			Exception(" BRM - 15 Issue." + e.getMessage());
		}
		
		/* BRM - 20 On entering any values the cross mark should be displayed*/
		ChildCreation(" BRM - 20 On entering any values the cross mark should be displayed.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				Pass("The Close icon is displayed in the Search Container box.");
			}
			else
			{
				Fail("The Close icon is not displayed in the Search Container box.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 20 Issue ." + e.getMessage());
			Exception(" BRM - 20 Issue." + e.getMessage());
		}
		
		/* BRM - 27 On selecting the cross mark, the entered values should be cleared from the search box*/
		ChildCreation(" BRM - 27 On selecting the cross mark, the entered values should be cleared from the search box.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM27", sheet, 3);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				if(searchContainer.getAttribute("value").isEmpty())
				{
				   Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
				}
				else
				{
				   Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
				}
			}
			else
			{
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					getSearchResults(srchtxt);
					 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
					 if(BNBasicfeature.isElementPresent(searchClose))
					 {
						jsclick(searchClose);
						if(searchContainer.getAttribute("value").isEmpty())
						{
						   Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
						}
						else
						{
						   Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
						}
					 }
					 else
					 {
						Fail( "The Search Close button is not displayed."); 
					 }
				}
				else
				{
					Fail( "The Search Icon is not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 27 Issue ." + e.getMessage());
			Exception(" BRM - 27 Issue." + e.getMessage());
		}
		
		/* BRM - 88 Verify whether the search icon is static in empty search text box.*/
		ChildCreation(" BRM - 88 Verify whether the search icon is static in empty search text box.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			else if(BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchContainerCancel);
			}
			wait.until(ExpectedConditions.visibilityOf(menu));
			//if(isElementPresent(searchIcon))
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				log.add("The Search Icon is displayed.");
				jsclick(searchIcon);
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					Pass("The Search Icon is present and it is static.",log);
				}
				else
				{
					Fail("The Search Icon is present and it is static.",log);
				}
			}
			else
			{
				 Fail("The Search icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 88 Issue ." + e.getMessage());
			Exception(" BRM - 88 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 15 Enter some characters in search field and verify whether the characters are highlighted in inline pop up*/
	public void searchhighlight() throws InterruptedException
	{
		ChildCreation(" BRM - 15 Enter some characters in search field and verify whether the characters are highlighted in inline pop up.");
		try
		{
			Actions act = new Actions(driver);
			String srchtxt =  BNBasicfeature.getExcelVal("BRM15", sheet, 3);
			String font = BNBasicfeature.getExcelVal("BRM15", sheet, 5);
			wait.until(ExpectedConditions.visibilityOf(searchContainer));
			if(searchContainer.getAttribute("value").isEmpty())
			{
				if(!BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
				}
				else
				{
					act.moveToElement(searchContainer).sendKeys(srchtxt).build().perform();
					Thread.sleep(1000);
					int count = 0;
					 do
					 {
						 wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(searchResultsContainer),
									ExpectedConditions.visibilityOfAllElements(searchResultsLists)
										)
									);
						 count++;
					 }while(count<2);
					wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				}
			}
			
			if(!searchContainer.getAttribute("value").equals(""))
			{
				if(BNBasicfeature.isListElementPresent(searchResultsLists))
				{
					log.add("The Search Results is displayed.");
					for(int j = 1 ; j<searchResultsLists.size(); j++)
					{
						WebElement txt = searchSuggestedItemHighlight.get(j-1);
						String txthighlight = txt.getCssValue("font-weight");
						if(txthighlight.equals(font))
						{
							Pass("The entered Search Keyword " + txt.getText() + " is highlighted. The font weight is " + txthighlight,log);
						}
						else
						{
							Fail("The entered Search Keyword " + txt.getText() + " is highlighted. The font weight is  " + txthighlight,log);
						}
					}
				}
				else
				{
					Fail("The Search Results is not displayed.");
				}
			}
			else
			{
				Fail( "The Search Container is empty.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 16 Issue ." + e.getMessage());
			Exception(" BRM - 16 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 16 Verify whether clicking on search icon displays only search field, Promo messages,Cross and Cancel options in the screen*/
	public void searchfieldcontentverification() throws InterruptedException
	{
		ChildCreation(" BRM - 16 Verify whether clicking on search icon displays only search field, Promo messages,Cross and Cancel options in the screen.");
		try
		{
			String srchtxt =  BNBasicfeature.getExcelVal("BRM16", sheet, 3);
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
			}
			
			wait.until(ExpectedConditions.visibilityOf(searchContainer));
			act.moveToElement(searchContainer).click().sendKeys(srchtxt).build().perform();
			Thread.sleep(1000);
			int count = 0;
			 do
			 {
				 wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(searchResultsContainer),
							ExpectedConditions.visibilityOfAllElements(searchResultsLists)
								)
							);
				 count++;
			 }while(count<2);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			boolean res = (promoCarousel)!=null;
			if(res==true)
			{
				Pass("The Promo Banner message at the top of the page is displayed.",log);
			}
			else
			{
				Fail("The Promo Banner message at the top of the page is not displayed.",log);
			}
			
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				Pass("The Search Container is displayed.");
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(searchContainerCancel))
			{
				Pass("The Cancel button  in the Search Container is displayed.");
			}
			else
			{
				Fail("The Cancel button in the Search Container is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				Pass("The Close icon button in the Search Container is displayed.");
			}
			else
			{
				Fail("The Close icon button in the Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 16 Issue ." + e.getMessage());
			Exception(" BRM - 16 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 17 Verify that while selecting the search option after entering search keywords,the search results page should be displayed*/
	public void searchresultnavigation() throws InterruptedException
	{
		ChildCreation(" BRM - 17 Verify that while selecting the search option after entering search keywords,the search results page should be displayed .");
		try
		{
			Actions act = new Actions(driver);
			String srchText = BNBasicfeature.getExcelVal("BRM17", sheet, 3);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
			}
			
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				if(!BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchIcon);
				}
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					act.moveToElement(searchContainer).click().sendKeys(srchText).build().perform();
					Thread.sleep(100);
					act.sendKeys(Keys.ENTER).build().perform();
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(1000);
					
					wait.until(ExpectedConditions.visibilityOf(homepageHeader));
					if(BNBasicfeature.isElementPresent(plpNoProductsFound))
					{
						Fail("The Product List is not dispalyed.");
					}
					else if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
					{
						Skip("The Product List is not dispalyed and the user is navigated to the PDP Page.");
					}
					else if(BNBasicfeature.isListElementPresent(searchPageProductLists))
					{
						Pass("The Product List is dispalyed.");
					}
					else
					{
						Fail("The Product List is not dispalyed.");
					}
				}
				else
				{
					Fail( "The Search Icon is not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 17 Issue ." + e.getMessage());
			Exception(" BRM - 17 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 23 Verify whether the response fetched for a particular keyword is correct and related to the keyword, containing links to the particular web page*/
	public void resultnavigation()
	{
		ChildCreation(" BRM - 23 Verify whether the response fetched for a particular keyword is correct and related to the keyword, containing links to the particular web page.");
		try
		{
			if(BNBasicfeature.isElementPresent(productListPage))
			{
				String srchText = BNBasicfeature.getExcelVal("BRM17", sheet, 3);
				wait.until(ExpectedConditions.visibilityOf(productListPage));
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(searchPageSearchTerm));
				if(BNBasicfeature.isElementPresent(searchPageSearchTerm))
				{
					log.add("The Searched Text was : " + srchText);
					String currText = searchPageSearchTerm.getText();
					if(srchText.equalsIgnoreCase(currText))
					{
						Pass("The User is redirected to the expected search result page.",log);
					}
					else
					{
						Fail("The User is not redirected to the expected search result page.",log);
					}
				}
				else
				{
					Fail( " The Search Result Product term is not displayed.");
				}
			}
			else
			{
				Fail( "The User is not in the Search Result Page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 23 Issue ." + e.getMessage());
			Exception(" BRM - 23 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 18 Verify that if the HTML tags are entered, the input is treated as a string, and the search operation is performed*/
	public void htmlsearch() throws InterruptedException
	{
		ChildCreation(" BRM - 18 Verify that if the HTML tags are entered, the input is treated as a string, and the search operation is performed.");
		try
		{
			ArrayList<String> mobileSiteSrch= new ArrayList<>();
			String srchtxt = BNBasicfeature.getExcelVal("BRM18", sheet, 3); 
			
			getSearchResults(srchtxt);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			if(BNBasicfeature.isListElementPresent(searchResultsLists))
			 {
				 for(WebElement srchsugg : searchResultsLists)
				 {
					 mobileSiteSrch.add(srchsugg.getText());
				 }
				 
				 if((searchResultsLists.size()>0) && (searchResultsLists.size()<=8))
				 {
					 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Pass("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion of only 8 and not more than 8.",log);
				 }
				 else
				 {
					 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Fail("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion more than 8.",log);
				 }
			 }
			 else
			 {
				 Fail("No Search Results dispalyed.");
			 }
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 18 Issue ." + e.getMessage());
			Exception(" BRM - 18 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 19 Enter some characters in search field and click on anywhere in the screen. The inline pop up should be closed*/
	public void inlinepopupclose() throws InterruptedException
	{
		ChildCreation(" BRM - 19 Enter some characters in search field and click on anywhere in the screen. The inline pop up should be closed.");
		try
		{
			boolean closed = false;
			String srchtxt = BNBasicfeature.getExcelVal("BRM19", sheet, 3);
			//Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchResults))
			{
				if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
				{
					closed = true;
					jsclick(searchResults);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(searchContainer))
					{
						 Fail("The Search Results is displayed.",log);
					}
					else
					{
						 Pass("The Search Results is not displayed.",log);
					}
				}
			}
			if(closed==false)
			{
				if(BNBasicfeature.isElementPresent(searchResults))
				{
					if(BNBasicfeature.isElementPresent(searchClose))
					{
						jsclick(searchClose);
					}
					if(BNBasicfeature.isElementPresent(searchContainerCancel))
					{
						jsclick(searchContainerCancel);
					}
					if(!BNBasicfeature.isElementPresent(searchContainer))
					{
						jsclick(searchIcon);
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
					}
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
					if(BNBasicfeature.isElementPresent(searchContainer))
					{
						getSearchResults(srchtxt);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
						if(BNBasicfeature.isElementPresent(searchResults))
						{
						   if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
						   {
							  jsclick(searchResults);
							  wait.until(ExpectedConditions.visibilityOf(header));
							  if(BNBasicfeature.isElementPresent(searchContainer))
							  {
								 Fail("The Search Results is displayed.",log);
							  }
							  else
							  {
								 Pass("The Search Results is not displayed.",log);
							  }
							}
						 }
						 else
						 {
							 Fail(" The Search Results is not displayed.");
						 }
					}
					else
					{
						Fail( "The Search Container is not displayed.");
					}
				}
				else
				{
					Fail( "The Search results are not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 19 Issue ." + e.getMessage());
			Exception(" BRM - 19 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 20 On entering any values the cross mark should be displayed*/
	public void searchcloseicon() throws InterruptedException
	{
		ChildCreation(" BRM - 20 On entering any values the cross mark should be displayed.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				Pass("The Close icon is displayed in the Search Container box.");
			}
			else
			{
				Fail("The Close icon is not displayed in the Search Container box.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 20 Issue ." + e.getMessage());
			Exception(" BRM - 20 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 21 Verify that misspelled keyword should be displayed with error screen */
	/* BRM - 25 If there is no match found for the searched text verify whether we are getting proper validation messages*/
	public void nosearchresult()
	{
		ChildCreation(" BRM - 21 Verify that misspelled keyword should be displayed with error screen.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM21", sheet, 3);
			String nores = BNBasicfeature.getExcelVal("BRM21", sheet, 4);
			getSearchResults(srchtxt);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			wait.until(ExpectedConditions.visibilityOf(noSearchResults));
			if(BNBasicfeature.isElementPresent(noSearchResults))
			{
			 String val = noSearchResults.getText();
			
			 log.add( "The Expected Value was : " + val );
			 log.add( "The actual Value is : " + nores);
			 if(val.contains(nores))
			 {
				 Pass("The No Suggested Result information is displayed.");
			 }
			 else
			 {
				 Fail("The No Suggested Result information is not displayed.");
			 }
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 21 Issue ." + e.getMessage());
			Exception(" BRM - 21 Issue." + e.getMessage());
		}
		
		/* BRM - 25 If there is no match found for the searched text verify whether we are getting proper validation messages*/
		ChildCreation(" BRM - 25 If there is no match found for the searched text verify whether we are getting proper validation messages.");
		try
		{
			String nores = BNBasicfeature.getExcelVal("BRM25", sheet, 4);
			Thread.sleep(500);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			wait.until(ExpectedConditions.visibilityOf(noSearchResults));
			if(BNBasicfeature.isElementPresent(noSearchResults))
			{
			  String val = noSearchResults.getText();
			  log.add( "The Expected Value was : " + val );
			  log.add( "The actual Value is : " + nores);
			   if(val.contains(nores))
			   {
				  Pass("The No Suggested Result information is displayed.");
			   }
			   else
			   {
				 Fail("The No Suggested Result information is not displayed.");
			   }
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 25 Issue ." + e.getMessage());
			Exception(" BRM - 25 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 24 Verify if the search is case-insensitive or not*/
	public void searchCaseSensitive()
	{
		ChildCreation(" BRM - 24 Verify if the search is case-insensitive or not.");
		ArrayList<String> mobileSiteSrch= new ArrayList<>();
		try
		{
			String[] srchtxt = BNBasicfeature.getExcelVal("BRM24", sheet, 3).split("\n"); 
			for(int i = 0; i<srchtxt.length; i++)
			{
				getSearchResults(srchtxt[i]);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				if(BNBasicfeature.isListElementPresent(searchResultsLists))
				{
				   for(WebElement srchsugg : searchResultsLists)
				   {
					 mobileSiteSrch.add(srchsugg.getText());
				   }
				   if((searchResultsLists.size()>0) && (searchResultsLists.size()<=8))
				   {
					 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Pass("The Search Results provided for the Keyword " + srchtxt + " and they are not case - sensitive.",log);
					 mobileSiteSrch.clear();
					}
					else
					{
					 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Fail("The Search Results provided for the Keyword " + srchtxt + "and they are case - sensitive.",log);
					 mobileSiteSrch.clear();
					}
				 }
				 else
				 {
					 Fail( "The Search Results are not displayed.");
				 }
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 24 Issue ." + e.getMessage());
			Exception(" BRM - 24 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 25 If there is no match found for the searched text verify whether we are getting proper validation messages*/
	public void searchvalidationmsg()
	{
		ChildCreation(" BRM - 25 If there is no match found for the searched text verify whether we are getting proper validation messages.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM25", sheet, 3); 
			String nores = BNBasicfeature.getExcelVal("BRM25", sheet, 4);
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
			}
			else if(BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchContainerCancel);
			}
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				if(!BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchIcon);	
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
				}
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					 act.moveToElement(searchContainer).click().sendKeys(srchtxt).build().perform();
					 Thread.sleep(1000);
					 int count = 0;
					 do
					 {
						 wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(searchResultsContainer),
									ExpectedConditions.visibilityOfAllElements(searchResultsLists)
										)
									);
						 count++;
					 }while(count<2);
					 
					 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
					 wait.until(ExpectedConditions.visibilityOf(noSearchResults));
					 if(BNBasicfeature.isElementPresent(noSearchResults))
					 {
						 String val = noSearchResults.getText();
						 log.add( "The Expected Value was : " + val );
						 log.add( "The actual Value is : " + nores);
						 if(val.contains(nores))
						 {
							 Pass("The No Suggested Result information is displayed.");
						 }
						 else
						 {
							 Fail("The No Suggested Result information is not displayed.");
						 }
					}
					else
					{
						Fail("The Search Container is not displayed.");
					}
				}
				else
				{
					Fail( "The Search Container is not displayed.");
				}
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 25 Issue ." + e.getMessage());
			Exception(" BRM - 25 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 26 Verify whether we are able to enter values[Alphabets, numbers and special characters] in search field and ensure that we are getting the search results instantly in search pop up. */
	public void searchDataVal()
	{
		ChildCreation(" BRM - 26 Verify whether we are able to enter values[Alphabets, numbers and special characters] in search field and ensure that we are getting the search results instantly in search pop up.");
		try
		{
			ArrayList<String> mobileSiteSrch= new ArrayList<>();
			String[] srchtxt = BNBasicfeature.getExcelVal("BRM26", sheet, 3).split("\n");
			for(int i = 0; i<srchtxt.length; i++)
			{
				getSearchResults(srchtxt[i]);
				wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				if(BNBasicfeature.isListElementPresent(searchResultsLists))
				{
				  for(WebElement srchsugg : searchResultsLists)
				  {
					 mobileSiteSrch.add(srchsugg.getText());
				  }
				  if((searchResultsLists.size()>0) && (searchResultsLists.size()<=8))
				  {
					 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Pass("The Search Results provided for the Keyword " + srchtxt + " and they are not case - sensitive.",log);
					 mobileSiteSrch.clear();
				  }
				  else
				  {
				 	 log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					 Fail("The Search Results provided for the Keyword " + srchtxt + "and they are case - sensitive.",log);
					 mobileSiteSrch.clear();
				  }
				 }
				 else
				 {
					 Fail( "The Search Results are not displayed.");
				 }
			 }
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 26 Issue ." + e.getMessage());
			Exception(" BRM - 26 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 27 On selecting the cross mark, the entered values should be cleared from the search box*/
	public void searchClose()
	{
		ChildCreation(" BRM - 27 On selecting the cross mark, the entered values should be cleared from the search box.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM27", sheet, 3);
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				if(searchContainer.getAttribute("value").isEmpty())
				{
				  Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
				}
				else
				{
				 Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
				}
				
				if(BNBasicfeature.isElementPresent(searchContainerCancel))
				{
					 jsclick(searchContainerCancel);	 
				}
			}
			else if(BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchContainerCancel);
			}
			else
			{
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
					if(BNBasicfeature.isElementPresent(searchContainer))
					{
						 act.moveToElement(searchContainer).click().sendKeys(srchtxt).build().perform();
						 Thread.sleep(1000);
						 int count = 0;
						 do
						 {
							 wait.until(ExpectedConditions.and(
										ExpectedConditions.visibilityOf(searchResultsContainer),
										ExpectedConditions.visibilityOfAllElements(searchResultsLists)
											)
										);
							 count++;
						 }while(count<2);
						 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
						 if(BNBasicfeature.isElementPresent(searchClose))
						 {
							jsclick(searchClose);
							if(searchContainer.getAttribute("value").isEmpty())
							{
							  Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
							}
							else
							{
							 Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
							}
							
							if(BNBasicfeature.isElementPresent(searchContainerCancel))
							{
								 jsclick(searchContainerCancel);	 
							}
						 }
						 else
						 {
							Fail( "The Search Close button is not displayed."); 
						 }
					}
					else
					{
						Fail("The Search Results are not displayed.");
					}
				}
				else
				{
					Fail("The Search icon is Clicked and the Search Container is not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 27 Issue ." + e.getMessage());
			Exception(" BRM - 27 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 29 Verify that the default text is displayed in the search box.*/
	public void searchTxt()
	{
		ChildCreation(" BRM - 29 Verify that the default text is displayed in the search box.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM29", sheet, 2); 
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
			}
			else if(BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchContainerCancel);
			}
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainerCancel));
				log.add( "The expected value was : " + srchtxt);
				String val = searchContainer.getAttribute("placeholder");
				log.add( "The actual value is : " + srchtxt);
				if(val.equals(srchtxt))
				{
					Pass("The Search text box placeholder with the Search Caption is present.",log);
				}
				else
				{
					Fail("The Search text box placeholder with the Search Caption is not visible.",log);
				}
			}
			else
			{
				Fail( "The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 29 Issue ." + e.getMessage());
			Exception(" BRM - 29 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 88 Verify whether the search icon is static in empty search text box.*/
	public void searchiconStatic()
	{
		ChildCreation(" BRM - 88 Verify whether the search icon is static in empty search text box.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			else if(BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchContainerCancel);
			}
			wait.until(ExpectedConditions.visibilityOf(menu));
			//if(isElementPresent(searchIcon))
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				log.add("The Search Icon is displayed.");
				jsclick(searchIcon);
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					Pass("The Search Icon is present and it is static.",log);
				}
				else
				{
					Fail("The Search Icon is present and it is static.",log);
				}
			}
			else
			{
				 Fail("The Search icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 88 Issue ." + e.getMessage());
			Exception(" BRM - 88 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 96 Verify that while clearing the search keyword using the backspace button in the keypad,the blank drop-down should not get displayed*/
	public void searchBackspace()
	{
		ChildCreation(" BRM - 96 Verify that while clearing the search keyword using the backspace button in the keypad,the blank drop-down should not get displayed.");
		try
		{
			Actions act = new Actions(driver);
			String srchtxt = "";
			if(!BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				srchtxt = BNBasicfeature.getExcelVal("BRM96", sheet, 3);
				getSearchResults(srchtxt);
				wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			}
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			 if(BNBasicfeature.isListElementPresent(searchResultsLists))
			 {
				//int txtlen = srchtxt.length();
				 
				 int txtlen = searchContainer.getAttribute("value").length();
				log.add("The Valid search data result is displayed for the keyword." + srchtxt.toString());
				do{
					
					 act.sendKeys(Keys.BACK_SPACE).build().perform();
				 	 txtlen--;
				 	 Thread.sleep(100);
				  } while(txtlen>=0);
				 log.add("The Search text is successfully deleted from the Search Container.");
				 
				 if(searchContainer.getAttribute("value").isEmpty())
				 {
					 Pass("The Valid search data result is not displayed for the keyword since the keyword is successfully deleted." + searchContainer.getAttribute("value"),log);
				 }
				 else
				 {
					 Fail("The Valid search data result is displayed for the keyword." + searchContainer.getAttribute("value"),log);
				 }
			 }
			 else
			 {
				 Fail(" No data result is displayed. Please Check" + srchtxt.toString());
			 }
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 96 Issue ." + e.getMessage());
			Exception(" BRM - 96 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 100 Verify that while searching the keyword in the search field, the search suggestion call has not been made with the "limit=20"*/
	public void searchResCount()
	{
		ChildCreation(" BRM - 100 Verify that while searching the keyword in the search field, the search suggestion call has not been made with the limit=20.");
		try
		{
			 String srchtxt =  BNBasicfeature.getExcelVal("BRM100", sheet, 3);
			 getSearchResults(srchtxt);
			 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			 if(BNBasicfeature.isListElementPresent(searchResultsLists))
			 {
				 int size = searchResultsLists.size();
				 if(size>8)
				 {
					 Fail("The Search Result provided for the keyword " + srchtxt + " returned more than the set 8 limit. The size of the result is " + size);
				 }
				 else
				 {
					 Pass("The Search Result provided for the keyword " + srchtxt + " returned are not more than the set 8 limit. The size of the result is " + size);
				 }
			 }
			 else
			 {
				 Fail(" No data result is displayed. Please Check" + srchtxt.toString());
			 }
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 100 Issue ." + e.getMessage());
			Exception(" BRM - 100 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 95 Verify that while navigating back from the search page,the home page should not get bounce along with the pancake menu.*/
	public void pancakeNavigation()
	{
		ChildCreation(" BRM - 95 Verify that while navigating back from the search page,the home page should not get bounce along with the pancake menu.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("BRM95", sheet, 3); 
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			if(!BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			wait.until(ExpectedConditions.visibilityOf(searchContainer));
			act.moveToElement(searchContainer).click().sendKeys(srchtxt).sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(homepageHeader));
			if(BNBasicfeature.isElementPresent(plpNoProductsFound))
			{
				Fail("The Product List is not dispalyed.");
			}
			else if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
			{
				Skip("The Product List is not dispalyed and the user is navigated to the PDP Page.");
			}
			else if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				Pass("The Product List is dispalyed.");
				wait.until(ExpectedConditions.visibilityOf(menu));
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				Thread.sleep(100);
				if(BNBasicfeature.isElementPresent(additionalLinks))
				{
					 if(BNBasicfeature.isElementPresent(additionalLinks))
					 {
						 BNBasicfeature.scrolldown(additionalLinks, driver); 
					 }
					  
					 if(BNBasicfeature.isElementPresent(additionalLinks))
					 {
						 BNBasicfeature.scrollup(myAccount,driver); 
					 }
				 
					  wait.until(ExpectedConditions.visibilityOf(myAccount));
					  Thread.sleep(100);
					  if(BNBasicfeature.isElementPresent(searchIcon))
						 {
							Pass("The Navigation back to the Homepage is successfull.",log);
						 }
						 else
						 {
							Fail("The Navigation back to the Homepage is not successfull.",log);
					     }
						 
						if(BNBasicfeature.isElementPresent(panCakeMenuMask))
						{
							jsclick(panCakeMenuMask);
						}
					 }
					 else
					 {
						 Fail( "The Additional link is not displayed in the menu");
						 if(BNBasicfeature.isElementPresent(panCakeMenuMask))
						 {
							jsclick(panCakeMenuMask);
						 }
						 wait.until(ExpectedConditions.visibilityOf(menu));
						 wait.until(ExpectedConditions.visibilityOf(header));
					 }
				}
				else
				{
					Fail("The Product List is not dispalyed.");
				}
		}
		catch(Exception e)
		{
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
			}
			System.out.println(" BRM - 95 Issue ." + e.getMessage());
			Exception(" BRM - 95 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 11 Search : Search Validation*/
	public void srchcomparision() throws IOException, JSONException, InterruptedException
	{
		ChildCreation(" BRM - 11 Search Suggestion Comparison.");
		driver.get(BNConstants.mobileSiteURL);
		wait.until(ExpectedConditions.visibilityOf(header));
		wait.until(ExpectedConditions.elementToBeClickable(searchIcon));
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(searchIcon));
		if(BNBasicfeature.isElementPresent(searchClose))
		{
			jsclick(searchClose);
			wait.until(ExpectedConditions.visibilityOf(searchContainer));
		}
		if(BNBasicfeature.isElementPresent(searchContainer))
		{
			jsclick(searchContainerCancel);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));
		}
		if(!BNBasicfeature.isElementPresent(searchContainer))
		{
			jsclick(searchIcon);
		}
		wait.until(ExpectedConditions.visibilityOf(searchContainer));
		Thread.sleep(500);
		sheet = BNBasicfeature.excelsetUp("Search Suggestion");
		int totalrows = sheet.getLastRowNum();
		String srchtext = null, encodetext,classicURL;
		HttpURLConnection conn = null;
		ArrayList<String> classicSitesrch = new ArrayList<>();
		ArrayList<String> mobileSiteSrch= new ArrayList<>();
		try
		{
			for(int i = 0; i<=totalrows; i++)
			{
			  int totalcolumns =sheet.getRow(i).getPhysicalNumberOfCells();
			  for(int j = 0; j<totalcolumns; j++)
			  {
				  try
				  {
					  srchtext = (sheet.getRow(i).getCell(j).getStringCellValue());
				  }
				  catch(Exception e)
				  {
					  DataFormatter formatter = new DataFormatter();
					  HSSFCell cell = sheet.getRow(i).getCell(j);
					  cell.setCellType(Cell.CELL_TYPE_STRING);
					  srchtext = formatter.formatCellValue(cell).toString();
				  }
				  // Encode the Search Text with UTF-8 format
				  encodetext = URLEncoder.encode(srchtext,"UTF-8");
				  
				 classicURL = "http://complete.barnesandnoble.com/www/search?types[]=product&term="+encodetext+"&limit=8";
				  
				  URL curl = new URL(classicURL);
				  conn = (HttpURLConnection) curl.openConnection();
				  //Check the URL is valid and returning the value
				  if(conn.getResponseCode()==200)
				  {
					  log.add("The Search Comparison Results for : " + srchtext);
					  log.add("Successful Reponse for the " + srchtext + " keyword");
					  
					  String classicResponse = IOUtils.toString(new URL(classicURL));
					  
					  JSONObject jObj = new JSONObject(classicResponse);
					  JSONObject classicResultsObj = jObj.getJSONObject("results");
					  JSONArray classicResultArray = classicResultsObj.getJSONArray("product");
					  //System.out.println(classicResultArray.length());
					  
					  for(int k=0; k<classicResultArray.length();k++)
					  {
						  String prtname = classicResultArray.getJSONObject(k).getString("id");
						  classicSitesrch.add(prtname);
					  }
					  
					  log.add("The Suggestion Results from the classic site API call : " + classicSitesrch.toString());
					  log.add("JSON Objects are successfully Fetched.");
				 }
				 else
				 {
				  Fail("There is something wrong with the keyword sent to the URL. " + conn.getResponseMessage());
				  System.out.println("HTTP Response Failure." + conn.getResponseMessage());
				 }
			  }
			  
			  //driver = BNBasicfeature.setMobileView(driver);
			  wait.until(ExpectedConditions.elementToBeClickable(searchIcon));
			  getSearchResults(srchtext);
			  Thread.sleep(500);
			  wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			  if(classicSitesrch.size()>0)
			  {
				  List<WebElement> srchsugg = searchResultsLists;
				  for(WebElement sugg : srchsugg)
				  {
					  mobileSiteSrch.add(sugg.getText().toString());  
				  }
			  }
			  else
			  {
				  wait.until(ExpectedConditions.visibilityOf(noSearchResults));
				  mobileSiteSrch.add(noSearchResults.getText());
			  }
			  
			  if(classicSitesrch.size()>0)
			  {
			  	if(mobileSiteSrch.equals(classicSitesrch))
			  	{
			  		log.add("The Search suggestion from the mobilesite: " + mobileSiteSrch.toString());
			  		Pass("The Search Suggestion matches with the Classic Site.",log);
			  	}
			  	else
			  	{
			  		log.add("The Search Suggestion from the Stream Call (Classic Site) " + classicSitesrch.toString());
			  		log.add("The Search Suggestion from the Mobile Site Search " + mobileSiteSrch.toString());
			  		Fail("There is mismatch in the Search suggestion criteria.",log);
			  	}
			  }
			  else
			  {
			  	String nores = "No suggested search";
			  	String mobres = mobileSiteSrch.toString();
			  	if(mobres.contains(nores));
			  	{
			  		Pass("There is no search suggestion for the entered keyword : " + mobres);
			  	}
			  }
			  classicSitesrch.clear();
			  mobileSiteSrch.clear();
			  jsclick(searchClose);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 11 Issue ." + e.getMessage());
			Exception(" BRM - 11 Issue." + e.getMessage());
		}
	 }
	
   /********************************************************************** 59 Logo Display *************************************************************************/
	
	/* BRM - 39 Verify whether the logo is displayed as per the creative */
	public void bnlogo()
	{
		ChildCreation(" BRM - 39 Verify whether the logo is displayed as per the creative.");
		boolean BRM42 = false;
		try
		{
			Actions act = new Actions(driver);
			driver.get(BNConstants.mobileSiteURL);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(bnLogo))
			{
				Pass("The Logo is displayed in the Main Page.");
			}
			else
			{
				Fail("The Logo is not displayed in the Main Page.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				Thread.sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(signIn));
				Thread.sleep(100);
				act.moveToElement(signIn).click().build().perform();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				if(BNBasicfeature.isElementPresent(checkoutLogo))
				{
					Pass("The Logo is displayed in the Sign In Page.");
					BRM42 = true;
				}
				else
				{
					Fail("The Logo is not displayed in the Sign In Page.");
				}
			}
			else
			{
				Fail("The menu is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(createAccbtn))
			{
				boolean pgLoaded = ldPage(signInlnk, createAccbtn, createAccFrame);
				if(pgLoaded==true)
				{
					if(BNBasicfeature.isElementPresent(checkoutLogo))
					{
						log.add("The user is navigated to the Create Account Page.");
						Pass("The Logo is displayed in the Create Account Page.",log);
						if(BRM42==true)
						{
							BRM42 = true;
						}
					}
					else
					{
						Fail("The Logo is not displayed in the Create Account Page.");
						BRM42 = false;
					}
				}
				else
				{
					Fail( " The user failed to load the create account page.");
				}
			}
			else
			{
				Fail("The Create Account button is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(signInlnk))
			{
				jsclick(signInlnk);
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
			}
			else
			{
				Fail("The Sign In link is not visible or present in the Create Account page.");
			}
			
			wait.until(ExpectedConditions.visibilityOf(forgotPassword));
			if(BNBasicfeature.isElementPresent(forgotPassword))
			{
				jsclick(forgotPassword);
				frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
				if(frgtPassPage==true)
				{
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
					if(BNBasicfeature.isElementPresent(checkoutLogo))
					{
						log.add("The user is navigated to the Forgot Password Page.");
						Pass("The Logo is displayed in the Forgot Password Page.");
						jsclick(checkoutLogo);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(menu));
					}
					else
					{
						Fail("The Logo is not displayed in the Forgot Password Page.");
						driver.get(BNConstants.mobileSiteURL);
						wait.until(ExpectedConditions.visibilityOf(header));
					}
				}
				else
				{
					Fail( "The User failed to load the Forgot Password page.");
				}
			}
			else
			{
				Fail("The Forgot Password link is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				Thread.sleep(100);
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(orderStatus));
				act.moveToElement(orderStatus).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when My Account was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when My Account was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(manageAccount));
				act.moveToElement(manageAccount).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Manage Account was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Manage Account was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(addressBook));
				act.moveToElement(addressBook).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Address Book was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Address Book was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(payments));
				act.moveToElement(payments).click().build().perform();
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Payments was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Payments was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 39 Issue ." + e.getMessage());
			Exception(" BRM - 39 Issue." + e.getMessage());
		}
		
		/* BRM - 42 Verify whether the logo is displayed in all the pages.*/
		ChildCreation(" BRM - 42 Verify whether the logo is displayed in all the pages.");
		try
		{
			if(BRM42==true)
			{
				Pass( "The Logo is displayed in all the pages.");
			}
			else
			{
				Fail( "The Logo is not displayed in all the pages.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 39 Issue ." + e.getMessage());
			Exception(" BRM - 39 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 40 While selecting the logo at the header it should be navigated to the home page*/
	public void logonavigation()
	{
		ChildCreation(" BRM - 40 Verify whether the logo is displayed as per the creative.");
		try
		{
			navigatetoURL();
			String baseURl = BNBasicfeature.getExcelVal("BRM40", sheet, 4); 
			String baseUrl1 = "http://mpreprod.barnesandnoble.com/home";
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(bnLogo))	
			{
				log.add("The Logo is displayed in the Main Page.");
				jsclick(bnLogo);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(100);
				if(driver.getCurrentUrl().contains(baseURl)||driver.getCurrentUrl().contains(baseUrl1))
				{
					Pass("The user is navigated to the base page. The base launched URL was " + baseURl +  " and the current URL the user navigated to is " + driver.getCurrentUrl(),log);
				}
				else
				{
					Fail("The user is not navigated to the base page. The base launched URL was " + baseURl +  " and the current URL the user navigated to is " + driver.getCurrentUrl(),log);
				}
			}
			else
			{
				Fail("The Logo is not displayed in the Main Page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 40 Issue ." + e.getMessage());
			Exception(" BRM - 40 Issue." + e.getMessage());
		}
	}
	
	/****************************************************************** 291 Forgot Password ****************************************************************************/
	
	/* BRM - 350 Verify that while selecting the forgot your password option in the sign-in page, the "Forgot password" page should be displayed*/
	public void forgotPasswordnavigation() throws IOException
	{
		ChildCreation(" BRM - 350 Verify that while selecting the forgot your password option in the sign-in page, the Forgot password page should be displayed.");
		try
		{
			if(BNBasicfeature.isElementPresent(header))
			{
				signedIn = false;
				//driver.get(BNConstants.mobileSiteURL);
				signedIn = signInExistingCustomer();
			}
			
			String forgotpasscaption = BNBasicfeature.getExcelVal("BRM350", sheet, 2);
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
				if(frgtPassPage==true)
				{
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					if(BNBasicfeature.isElementPresent(pgetitle))
					{
						log.add("The Forgot Password link is found and it is clicked.");
						String tit = pgetitle.getText();
						log.add( "The Expected value was : " + forgotpasscaption);
						log.add( "The actual value is : " + tit);
						if(tit.equals(forgotpasscaption))
						{
							Pass("The user is navigated to the Forgot Password Assistant page.",log);
						}
						else
						{
							Fail("The user is not navigated to the Forgot Password Assistant page.",log);
						}
					}
					else
					{
						Fail("The Forgot Password link is not visible in the Sign In page. Please Check.");
					}
				}
				else
				{
					Fail( "Failed to load the Forgot Password Page.");
				}
			}
			else
			{
				Fail( "The User failed to load the Sign In Page.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 350 Issue ." + e.getMessage());
			Exception(" BRM - 350 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 351 Verify that "email address" text field should be displayed in the reset password page with the "Continue' button*/
	public void forgorpasswordEmailandContinuefield()
	{
		ChildCreation(" BRM - 351 Verify that email address text field should be displayed in the reset password page with the Continue button.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(emailId))
				{
					Pass("The Email Address field is present in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Email Address field is not present in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					Pass("The Submit Button is present in the Forgot Password Assistant page.");
				}
				else
				{
					Pass("The Submit Button is not present in the Forgot Password Assistant page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 351 Issue ." + e.getMessage());
				Exception(" BRM - 351 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 352 Verify that forgot password page should match with the creative.*/
	public void forgorpasswordelementpresence()
	{
		ChildCreation(" BRM - 352 Verify that forgot password page should match with the creative.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The Forgot Password Assistant page title is displayed.");
				}
				else
				{
					Fail("The Forgot Password Assistant page title is not displayed.");
				}
						
				if(BNBasicfeature.isElementPresent(forgotPasswordInfo))
				{
					Pass("The Forgot Password Assistant page Info Message is displayed.");
				}
				else
				{
					Fail("The Forgot Password Assistant page Info Message is not displayed.");
				}
						
				if(BNBasicfeature.isElementPresent(emailId))
				{
					Pass("The Forgot Password Assistant page Email Field is displayed.");
				}
				else
				{
					Fail("The Forgot Password Assistant page Email field is not displayed.");
				}
						
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					Pass("The Forgot Password Assistant Continue Button is displayed.");
				}
				else
				{
					Fail("The Forgot Password Assistant page Continue button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 352 Issue ." + e.getMessage());
				Exception(" BRM - 352 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 356 Verify that while selecting the "Continue" button without entering the email address field,the alert message should be displayed */
	public void forgotpasswordEmptyEmailAlrt()
	{
		ChildCreation(" BRM - 356 Verify that while selecting the Continue button without entering the email address field,the alert message should be displayed.");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotPassalrt = BNBasicfeature.getExcelVal("BRM356", sheet, 7);
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					jsclick(forgotContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					if(BNBasicfeature.isElementPresent(forgotPasswordError))
					{
						Pass("The alert is raised when user tries to proceed to Password Assistant page without entering the Email Id in the Email Address field.");
						log.add("The Expected alert was : " + forgotPassalrt);
						String actVal = forgotPasswordError.getText(); 
						log.add("The actual alert is : " + actVal);
						if(actVal.equals(forgotPassalrt))
						{
							Pass("The alert that was raised matches with the expected alert.",log);
						}
						else
						{
							Fail("The alert that was raised does not matches with the expected alert.",log);
						}
					}
					else
					{
						Fail("The Alert is not raised in the Password Assistant Page. Please Check.");
					}
				}
				else
				{
					Fail("The Continue button is not present in the Forgot Assistant Page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 356 Issue ." + e.getMessage());
				Exception(" BRM - 356 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 426 Verify that email address text field should accept the maximum of 40 chars with the combination of numbers,alphabets and special characters.*/
	public void forgotpasswordEmailLength()
	{
		ChildCreation(" BNM - 426 Verify that email address text field should accept the maximum of 40 chars with the combination of numbers,alphabets and special characters.");
		if(frgtPassPage==true)
		{
			try
			{
				String[] forgotEmailID =  BNBasicfeature.getExcelVal("BRM426", sheet, 6).split("\n");
				for(int i = 0; i<forgotEmailID.length;i++)
				{
					if(BNBasicfeature.isElementPresent(emailId))
					{
						emailId.clear();
						emailId.sendKeys(forgotEmailID[i]);
						String txtinemail = emailId.getAttribute("value").toString();
						if(txtinemail.length()<=40)
						{
							Pass("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and its current length is " + txtinemail.length() + " and the value from the excel file was " + forgotEmailID[i] + " and it is less than or equal to 40 Characters. The length of the EmailId from the excel file was " + forgotEmailID[i].length());
						}
						else
						{
							Fail("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and its current length is " + txtinemail.length() + " and the value from the excel file was " + forgotEmailID[i] + " and it is more than 40 Characters. The length of the EmailId from the excel file was " + forgotEmailID[i].length());
						}
					}
					else
					{
						Fail("The Forgot Password Email field is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 426 Issue ." + e.getMessage());
				Exception(" BRM - 426 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 427 Verify that while selecting the "continue" button entering the non registered email address,the alert message should be displayed.*/
	public void forgotpasswordNonRegisterEmailAlrt()
	{
		ChildCreation(" BRM - 427 Verify that while selecting the continue button entering the non registered email address,the alert message should be displayed..");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotEmailID = BNBasicfeature.getExcelVal("BRM427", sheet, 6);
				String forgotNonRgstrAlrt = BNBasicfeature.getExcelVal("BRM427", sheet, 7);
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(forgotContinueBtn)))
				{
					emailId.clear();
					emailId.sendKeys(forgotEmailID);
					jsclick(forgotContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					if(BNBasicfeature.isElementPresent(forgotPasswordError))
					{
						log.add("The expected value was : " + forgotNonRgstrAlrt);
						String actVal = forgotPasswordError.getText(); 
						log.add("The actual alert is : " + forgotNonRgstrAlrt);
						if(actVal.equals(forgotNonRgstrAlrt))
						{
							Pass("The password alert raised to indicate the user matches with the expected alert.",log);
						}
						else
						{
							Fail("The password alert raised to indicate the user doen not matches with the expected alert.",log);
						}
					}
					else
					{
						Fail("The Forgot Password Error is not displayed.");
					}
				}
				else
				{
					Fail("The Forgot Password Email field or Continue Button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 427 Issue ." + e.getMessage());
				Exception(" BRM - 427 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 428 Verify that while selecting the "Continue" button after entering the html tags in the email and password field,the text should treat as a string*/
	public void forgotpasswordHtmlEmail()
	{
		ChildCreation(" BRM - 428 Verify that while selecting the Continue button after entering the html tags in the email and password field,the text should treat as a string.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(forgotContinueBtn)))
				{
					emailId.clear();
					String forgotEmailID = BNBasicfeature.getExcelVal("BRM428", sheet, 6); 
					String forgotNonRgstrAlrt = BNBasicfeature.getExcelVal("BRM428", sheet, 7);
					emailId.sendKeys(forgotEmailID);
					jsclick(forgotContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					if(BNBasicfeature.isElementPresent(forgotPasswordError))
					{
						log.add("The expected value was : " + forgotNonRgstrAlrt);
						String actVal = forgotPasswordError.getText(); 
						log.add("The actual alert is : " + forgotNonRgstrAlrt);
						if(actVal.equals(forgotNonRgstrAlrt))
						{
							Pass("The password alert raised to indicate the user matches with the expected alert.",log);
						}
						else
						{
							Fail("The password alert raised to indicate the user doen not matches with the expected alert.",log);
						}
					}
					else
					{
						Fail("The Forgot Password Error is not displayed.");
					}
				}
				else
				{
					Fail("The Forgot Password Email field or Continue Button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 428 Issue ." + e.getMessage());
				Exception(" BRM - 428 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/*BRM - 355 Verify that while selecting the "Continue" button after entering the valid Email Id ,the password assistant page should be displayed with the "Continue" and "Cancel" button */
	public void forgotpasswordContinuebtn()
	{
		ChildCreation(" BRM - 355 Verify that while selecting the Continue button after entering the valid Email Id ,the password assistant page should be displayed with the Continue and Cancel button .");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotPasstitle = BNBasicfeature.getExcelVal("BRM355", sheet, 2); 
				String validemailId = BNBasicfeature.getExcelVal("BRM355", sheet, 6); 
				if(BNBasicfeature.isElementPresent(emailId))
				{
					emailId.clear();
					emailId.sendKeys(validemailId);
					log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
					if(BNBasicfeature.isElementPresent(forgotContinueBtn))
					{
						frgtPassPage = false;
						jsclick(forgotContinueBtn);
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						if(BNBasicfeature.isElementPresent(pgetitle))
						{
							frgtPassPage = true;
							log.add("The User is navigated to the Password Assistant Page.");
							String tit = pgetitle.getText();
							log.add( "The Expected value was : " + forgotPasstitle);
							log.add( "The actual value is : " + tit);
							if(tit.equals(forgotPasstitle))
							{
								Pass("The User is navigated to the Password Assistant Page to reset the password after entering the Valid Email Address and the header matches.");
							}
							else
							{
								Fail("The User is navigated to the Password Assistant Page to reset the password after entering the Valid Email Address but the title of the page does not match.");
							}
						}
						else
						{
							Fail("The User is not navigated to the Password Assistant Page to reset the password after entering the Valid Email Address.");
						}
					}
					else
					{
						Fail("The Continue button is not present in the Password Assistant Page.");
					}
				}
				else
				{
					Fail("The Forgot Password Email field or Continue Button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 355 Issue ." + e.getMessage());
				Exception(" BRM - 355 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 361 Verify that Password assistant pages should be as per the creative */
	public void forgotpasswordpageelements()
	{
		ChildCreation(" BRM - 361 Verify that Password assistant pages should be as per the creative.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The Forgot Password Assistant page title is displayed.");
				}
				else
				{
					Fail("The Forgot Password Assistant page title is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotEmailDisplay))
				{
					Pass("The entered Email field is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The entered Email field is not displayed in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotEmailEdit))
				{
					Pass("The Email Edit link is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Email Edit link is not displayed in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotEmailRecoveryOption))
				{
					Pass("The Password recovery option by Email Option radio button is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Password recovery option by Email Option radio button is not displayed in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotSecurityRecoveryOption))
				{
					Pass("The Password recovery option by Security Question Option radio button is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Password recovery option by Security Question Option is not displayed in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					Pass("The Continue button is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Continue button is not displayed in the Forgot Password Assistant page.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotCancel))
				{
					Pass("The Cancel button is displayed in the Forgot Password Assistant page.");
				}
				else
				{
					Fail("The Cancel button is not displayed in the Forgot Password Assistant page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 361 Issue ." + e.getMessage());
				Exception(" BRM - 361 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 380 Verify that user entered email address should be displayed correctly in the password assistant page*/
	public void forgotpasswordEmailVerify()
	{
		ChildCreation(" BRM - 380 Verify that user entered email address should be displayed correctly in the password assistant page.");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotemail = BNBasicfeature.getExcelVal("BRM380", sheet, 6);
				String forgotemailcaption = BNBasicfeature.getExcelVal("BRM380", sheet, 3);
				if(BNBasicfeature.isElementPresent(forgotEmailDisplay))
				{
					Pass("The entered Email field is displayed in the Forgot Password Assistant page.");
					String actVal = forgotEmailDisplay.getText();
					log.add( "The actual value is : " + actVal);
					String expVal = forgotemailcaption.concat(forgotemail);
					log.add( "The Expected text was : " + expVal);
					if(actVal.equals(expVal))
					{
						/*System.out.println(forgotEmailDisplay.getText());
						System.out.println(forgotemailcaption.concat(forgotemail));
						System.out.println(forgotEmailDisplay.getText().equals(forgotemailcaption.concat(forgotemail)));*/
						Pass("The user entered email id is displayed with the Caption.");
					}
					else
					{
						/*System.out.println(forgotEmailDisplay.getText());
						System.out.println(forgotemailcaption.concat(forgotemail));
						System.out.println(forgotEmailDisplay.getText().equals(forgotemailcaption.concat(forgotemail)));*/
						Fail("The user entered email id is displayed with the Caption but there is mismatch.");
					}
				}
				else
				{
					Fail("The entered Email field is not displayed in the Forgot Password Assistant page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 380 Issue ." + e.getMessage());
				Exception(" BRM - 380 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 402 Verify that "Send a link to my email" and "Answer a security question" options should be displayed as per the classic site*/
	public void forgotpasswordRecoveryOptionVerify()
	{
		ChildCreation(" BRM - 402 Verify that Send a link to my email and Answer a security question options should be displayed as per the classic site .");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotemailrecoverycaption = BNBasicfeature.getExcelVal("BRM402", sheet, 8);
				String forgotsecurityrecoverycaption = BNBasicfeature.getExcelVal("BRM402", sheet, 9); 
				if(BNBasicfeature.isElementPresent(forgotEmailRecoveryOption))
				{
					Pass("The Email Recovery Option is displayed.");
					if(forgotEmailRecoveryOptionTxt.getText().equals(forgotemailrecoverycaption))
					{
						Pass("The Email Recovery Option Caption matches with the expected one.");
					}
					else
					{
						Fail("The Email Recovery Option Caption does not matches with the expected one.");
					}
				}
				else
				{
					Fail("The Email Recovery Option is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(forgotSecurityRecoveryOption))
				{
					Pass("The Security Recovery Option is displayed.");
					if(forgotSecurityRecoveryOptionTxt.getText().equals(forgotsecurityrecoverycaption))
					{
						Pass("The Security Recovery Option Caption matches with the expected one.");
					}
					else
					{
						Fail("The Security Recovery Option does not matches with the expected one.");
					}
				}
				else
				{
					Fail("The Security Recovery Option is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 402 Issue ." + e.getMessage());
				Exception(" BRM - 402 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 403 Verify that by default the "send a link to my email" option should be selected in the forgot password page */
	public void forgotpasswordRecoveryDefaultSelect()
	{
		ChildCreation(" BRM - 403 Verify that by default the send a link to my email option should be selected in the forgot password page ");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotEmailRecoveryOption))
				{
					if(forgotEmailRecoveryOption.getAttribute("value").equals("true"))
					{
						Pass("The Send a link to my email is found and it is selected by Default.");
					}
					else
					{
						Fail("The Send a link to my email is found and it is not selected by Default.");
					}
				}
				else
				{
					Fail("The Email Recovery Option is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 403 Issue ." + e.getMessage());
				Exception(" BRM - 403 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 406 Verify while selecting the "Answer a Security Question" in the forgot passsword page,the Security question should be displayed with the "Security Answer" text field*/
	public void forgotpasswordRecoverySecurityQuestionCheck()
	{
		ChildCreation(" BRM - 406 Verify while selecting the Answer a Security Question in the forgot passsword page,the Security question should be displayed with the Security Answer text field.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotSecurityRecoveryOption))
				{
					Pass("The Security Question Recovery Option is displayed.");
					
					boolean secchk = forgotSecurityRecoveryOption.isSelected();
					if(secchk==false)
					{
						driver.findElement(By.xpath("(//*[@class='styled-radio'])[2]")).click();
					}
					wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerFields));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(forgotSecurityAnswerFields))
					{
						Pass("The Security Question recovery option is clicked and the Security Fields section is displayed.");
					}
					else
					{
						Fail("The Security Question recovery option is clicked and the Security Fields section are not displayed.");
					}
				}
				else
				{
					Fail("The Security Question Recovery Option is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 406 Issue ." + e.getMessage());
				Exception(" BRM - 406 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 999 Verify that security answer text field should accept the maximum of 25 chars with the combination of numbers,alphabets and special characters.*/
	public void forgotpasswordSecurityAnswerLimit()
	{
		ChildCreation(" BRM - 999 Verify that security answer text field should accept the maximum of 25 chars with the combination of numbers,alphabets and special characters.");
		if(frgtPassPage==true)
		{
			try
			{
				String[] SecQuesAnswer = BNBasicfeature.getExcelVal("BRM999", sheet, 10).split("\n");
				for(int i = 0; i<SecQuesAnswer.length;i++)
				{
					if(BNBasicfeature.isElementPresent(forgotSecurityAnswerTextBox))
					{
						forgotSecurityAnswerTextBox.clear();
						forgotSecurityAnswerTextBox.sendKeys(SecQuesAnswer[i]);
						String txtinSecAns = forgotSecurityAnswerTextBox.getAttribute("value").toString();
						if(txtinSecAns.length()<=25)
						{
							Pass("The user was successfull in entering the value in the Security Question Field. And the user entered data was " + txtinSecAns + " and its current length is " + txtinSecAns.length() + " and the value from the excel file was " + SecQuesAnswer + " and it is less than or equal to 25 Characters. The length of the Security Answer from the excel file was " + SecQuesAnswer[i]);
						}
						else
						{
							Fail("The user was successfull in entering the value in the Security Question Field. And the user entered data was " + txtinSecAns + " and its current length is " + txtinSecAns.length() + " and the value from the excel file was " + SecQuesAnswer + " and it is more than 25 Characters. The length of the Security Answer from the excel file was " + SecQuesAnswer[i]);
						}
					}
					else
					{
						Fail("The Security Answer field is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 999 Issue ." + e.getMessage());
				Exception(" BRM - 999 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 407 Verify that while selecting the "Continue" button after entering the invalid security answer,the alert message should be displayed */
	public void forgotpasswordInvalidSecurityAnswer()
	{
		ChildCreation(" BRM - 407 Verify that while selecting the Continue button after entering the invalid security answer,the alert message should be displayed.");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotsecurityanswer = BNBasicfeature.getExcelVal("BRM407", sheet, 10); 
				String forgotsecurityanswererroralrt = BNBasicfeature.getExcelVal("BRM407", sheet, 7);
				if(BNBasicfeature.isElementPresent(forgotSecurityAnswerTextBox)&&(BNBasicfeature.isElementPresent(forgotContinueBtn)))
				{
					forgotSecurityAnswerTextBox.clear();
					log.add("The Security Question Answer Field is present and it is visible.");
					forgotSecurityAnswerTextBox.sendKeys(forgotsecurityanswer);
					jsclick(forgotContinueBtn);
					
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					if(BNBasicfeature.isElementPresent(forgotPasswordError))
					{
						Pass("The Security Question Answer error is raised and it is visible.");
						log.add("The Expected alert was : " + forgotsecurityanswererroralrt);
						String actVal = forgotPasswordError.getText(); 
						log.add("The actual alert is : " + actVal);
						if(actVal.equals(forgotsecurityanswererroralrt))
						{
							Pass("The Security Question Answer error is raised and the alert matches with the expected alert.",log);
						}
						else
						{
							Fail("The Security Question Answer error is raised and the alert does not matches with the expected alert.",log);
						}
					}
					else
					{
						Fail("The Security Question Answer field or Continue Button is not displayed.");
					}
				}
				else
				{
					Fail("The Invalid Security Answer error is not raised or not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 407 Issue ." + e.getMessage());
				Exception(" BRM - 407 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 408 Verify that while entering the invalid security answer for more than 3 items,the "You have exceeded 3 reset password attempts, please try again after 30 minutes." alert message should be displayed.*/
	public void forgotpasswordInvalidSecurityLimitExceed()
	{
		ChildCreation(" BRM - 408 Verify that while entering the invalid security answer for more than 3 items,the You have exceeded 3 reset password attempts, please try again after 30 minutes. alert message should be displayed.");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotsecurityanswer = BNBasicfeature.getExcelVal("BRM408", sheet, 10); 
				String forgotsecurityanswererroralrt = BNBasicfeature.getExcelVal("BRM408", sheet, 7);
				
				int j = 1;
				if(BNBasicfeature.isElementPresent(forgotSecurityAnswerTextBox))
				{
				    do
					{
						forgotSecurityAnswerTextBox.clear();
						forgotSecurityAnswerTextBox.sendKeys(forgotsecurityanswer);
						jsclick(forgotContinueBtn);
						wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
						j++;
					} while(j<4);
					
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					if(BNBasicfeature.isElementPresent(forgotPasswordError))
					{
						log.add("The Expected alert was : " + forgotsecurityanswererroralrt);
						String actVal = forgotPasswordError.getText(); 
						log.add("The actual alert is : " + actVal);
						if(actVal.equals(forgotsecurityanswererroralrt))
						{
							Pass("The Security Question Answer error is raised for each attempt and the alert indicating the maximum alert attempt is raised and it matches with the expected alert.",log);
						}
						else
						{
							Fail("The Security Question Answer error is raised for each attempt and the alert indicating the maximum alert attempt is raised and it does not matches with the expected alert.",log);
						}
					}
					else
					{
						Fail("The Security Question Answer error is not displayed.");
					}
				}
				else
				{
					Fail("The Security Question Answer field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 408 Issue ." + e.getMessage());
				Exception(" BRM - 408 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 989 Verify that while clicking the continue button by selecting the "send a link to my email" option,it should move to the "Check your mail" acknowledgement page and the reset password link should be send to respective email Id. */
	public void forgotpasswordResetByEmailLink()
	{
		ChildCreation(" BRM - 989 Verify that while clicking the continue button by selecting the send a link to my email option,it should move to the Check your mail acknowledgement page and the reset password link should be send to respective email Id. ");
		if(frgtPassPage==true)
		{
			try
			{
				String SuccessMsg1 = BNBasicfeature.getExcelVal("BRM989", sheet, 11);
				String SuccessMsg2 = BNBasicfeature.getExcelVal("BRM989", sheet, 12);
				String SuccessMsg3 = BNBasicfeature.getExcelVal("BRM989", sheet, 13);
				String emailId = BNBasicfeature.getExcelVal("BRM989", sheet, 6); 
				boolean emailchk = forgotEmailRecoveryOption.isSelected();
				if(emailchk==false)
				{
					driver.findElement(By.xpath("(//*[@class='styled-radio'])[1]")).click();
				}
				
				jsclick(forgotContinueBtn);
				Thread.sleep(4000);
				if(BNBasicfeature.isElementPresent(forgotResetSuccessContainer))
				{
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess1))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess1.getText();
						log.add( "The Actual value was : " + actVal );
						log.add( "The expected value was : " + SuccessMsg1 );
						if(actVal.contains(SuccessMsg1))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText());
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText() );
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
					
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess2))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess2.getText();
						log.add( "The Actual value was : " + actVal );
						String expVal = SuccessMsg2.concat(emailId);
						log.add( "The expected value was : " + SuccessMsg1 );
						if(actVal.contains(expVal))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess2.getText() );
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content. The raised alert was " + forgotSecurityResetSuccess2.getText());
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
					
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess3))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess3.getText();
						log.add( "The Actual value was : " + actVal );
						log.add( "The expected value was : " + SuccessMsg3);
						if(actVal.equals(SuccessMsg3))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content. The raised alert was " + forgotSecurityResetSuccess3.getText());
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess3.getText() );
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
				}
				else
				{
					Fail("The Success Message Container is not displayed." );
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 989 Issue ." + e.getMessage());
				Exception(" BRM - 989 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 991 Verify that while clicking the continue shopping button in the "Check your mail" acknowledgement page,it should redirect to the respective page*/
	public void forgotpasswordContinueShoppingNav()
	{
		ChildCreation(" BRM - 991 Verify that while clicking the continue shopping button in the Check your mail acknowledgement page,it should redirect to the respective page");
		if(frgtPassPage==true)
		{
			try
			{
				String NavigatedURL = BNBasicfeature.getExcelVal("BRM991", sheet, 14); 
				if(BNBasicfeature.isElementPresent(continueShopBtn))
				{
					log.add("The user password has been reset and the mail is sent to their specified email id.");
					jsclick(continueShopBtn);
					//wait.until(ExpectedConditions.visibilityOf(promoCarousel));
					wait.until(ExpectedConditions.visibilityOf(header));
					String currentUrl = driver.getCurrentUrl();
					
					if(currentUrl.contains(NavigatedURL))
					{
						Pass("The user successfully reset the password and they are navigated to the respective home page as expected. The current URL is " + currentUrl.toString());
					}
					else
					{
						Fail("The user successfully reset the password and they are navigated to the some other page than the desired page . The current URL is " + currentUrl.toString());
					}
				}
				else
				{
					Fail("The Continue Shop button is not displayed." );
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 991 Issue ." + e.getMessage());
				Exception(" BRM - 991 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 353 Verify that Email Address,New Password,Re-enter New Password,Continue and Cancel buttons should be displayed as per the creative*/
	public void forgotpasswordSecurityQuestionVerification() throws IOException
	{
		ChildCreation(" BRM - 353 Verify that Email Address,New Password,Re-enter New Password,Continue and Cancel buttons should be displayed as per the creative");
		if(frgtPassPage==true)
		{
			try
			{
				String email = BNBasicfeature.getExcelVal("BRM353", sheet, 6);
				String securityAnswer = BNBasicfeature.getExcelVal("BRM353", sheet, 10);
				//driver.get(BNConstants.mobileSiteURL);
				signedIn = signInExistingCustomer();
				if(signedIn==true)
				{
					wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
					frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
					if(frgtPassPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						if(BNBasicfeature.isElementPresent(emailId))
						{
							emailId.sendKeys(email);
							log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
							if(BNBasicfeature.isElementPresent(forgotContinueBtn))
							{
								jsclick(forgotContinueBtn);
								log.add("The Continue button in the Password Assistant is clicked.");
								wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
								wait.until(ExpectedConditions.visibilityOf(forgotContinueBtn));
								
								boolean secchk = forgotSecurityRecoveryOption.isSelected();
								if(secchk==false)
								{
									driver.findElement(By.xpath("(//*[@class='styled-radio'])[2]")).click();
								}
								wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerFields));
								if(BNBasicfeature.isElementPresent(forgotSecurityAnswerFields))
								{
									forgotSecurityAnswerTextBox.sendKeys(securityAnswer);
									jsclick(forgotContinueBtn);
									wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
									Thread.sleep(1000);
									
									if(BNBasicfeature.isElementPresent(forgotPasswordInfo))
									{
										Pass("The user is navigated to the Password Reset page and the Caption is displayed.");
									}
									else
									{
										Fail("The user is not navigated to the Password Reset page.");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordNew))
									{
										Pass("The user is navigated to the Password Reset page and the New Pasword field is displayed.");
									}
									else
									{
										Fail("The New Pasword field is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordNewCnfm))
									{
										Pass("The user is navigated to the Password Reset page and the Confirm New Pasword field is displayed.");
									}
									else
									{
										Fail("The Confirm New Pasword field is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordrules))
									{
										Pass("The user is navigated to the Password Reset page and the Forgot Password rules is displayed.");
									}
									else
									{
										Fail("The Forgot Password rules is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotContinueBtn))
									{
										Pass("The user is navigated to the Password Reset page and the Continue button is displayed.");
									}
									else
									{
										Fail("The Continue button is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotCancel))
									{
										Pass("The user is navigated to the Password Reset page and the Cancel button is displayed.");
									}
									else
									{
										Fail("The Cancel button is not displayed");
									}
								}
								else
								{
									Fail("The Security answer field is not displayed." );
								}
							}
							else
							{
								Fail("The Continue button is not displayed." );
							}
						}
						else
						{
							Fail("The Email id is not displayed." );
						}
					}
					else
					{
						Fail("The Continue button is not displayed." );
					}
				}
				else
				{
					Fail( "The User failed to sign in.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 353 Issue ." + e.getMessage());
				Exception(" BRM - 353 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 430 Verify that while selecting the "continue" button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed*/
	public void forgotpasswordBlankPassAlrt() throws IOException
	{
		ChildCreation(" BRM - 430 Verify that while selecting the continue button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed");
		if(frgtPassPage==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM430", sheet, 7); 
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					jsclick(forgotContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordError));
					
					String Actalrt1 = forgotPasswordError1.getText();
					String Actalrt2 = forgotPasswordError2.getText();
					
					if(Actalrt1.trim().concat(Actalrt2.trim()).equals(alert.trim()))
					{
						Pass("The user is alerted with the appropriate error and the content matches with the expected one. The raised error is " + forgotPasswordError.getText());
					}
					else
					{
						Fail("The user is alerted with the appropriate error and the content does not matches with the expected one. The raised error is " + forgotPasswordError.getText());
					}
				}
				else
				{
					Fail("The Continue button is not displayed." );
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 430 Issue ." + e.getMessage());
				Exception(" BRM - 430 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	
	/* BRM - 431 Verify that while selecting the "i" icon in the password rules option,the password rules overlay should be enabled*/
	public void forgotpasswordResetRulesEnable() throws IOException
	{
		ChildCreation(" BRM - 431 Verify that while selecting the i icon in the password rules option,the password rules overlay should be enabled.");
		if(frgtPassPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotPasswordrulesiiconShow))
				{
					log.add("The Forgot Password Rules i icon is found and displayed.");
					jsclick(forgotPasswordrulesiiconShow);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordruleslist));
					if(BNBasicfeature.isElementPresent(forgotPasswordruleslist))
					{
						Pass("The Forgot Password Rules are displayed.");
					}
					else
					{
						Fail("The Forgot Password Rules are not displayed.");
					}
				}
				else
				{
					Fail("The Forgot Password rules i icon is not displayed." );
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 431 Issue ." + e.getMessage());
				Exception(" BRM - 431 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 432 Verify that while selecting again the "i" icon in the password rules option,the password rules overlay should be closed */
	public void forgotpasswordResetRulesDisable() throws IOException
	{
		ChildCreation(" BRM - 432 Verify that while selecting again the i icon in the password rules option,the password rules overlay should be closed.");
		if(frgtPassPage==true)
		{
			try
			{
				log.add("The Forgot Password Rules i icon is found and displayed.");
				if(BNBasicfeature.isElementPresent(forgotPasswordrulesiiconHide))
				{
					jsclick(forgotPasswordrulesiiconHide);
					if(forgotPasswordruleslist.getAttribute("style")=="display: block;")
					{
						Fail("The Forgot Password Rules are displayed and not closed.",log);
					}
					else
					{
						Pass("The Forgot Password Rules are not displayed.",log);
					}
				}
				else
				{
					if(BNBasicfeature.isElementPresent(forgotPasswordruleslist))
					{
						Pass("The Forgot Password Rules are displayed.");
						jsclick(forgotPasswordruleslist);
						if(forgotPasswordruleslist.getAttribute("style")=="display: block;")
						{
							Fail("The Forgot Password Rules are displayed and not closed.",log);
						}
						else
						{
							Pass("The Forgot Password Rules are not displayed.",log);
						}
					}
					else
					{
						Fail("The Password Rules list is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 432 Issue ." + e.getMessage());
				Exception(" BRM - 432 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 429 Verify that while selecting the "continue" after entering the "password" and "confirm password" details,the page should be closed and password should reset for the entered email address*/
	public void forgotpasswordResetSuccess() throws IOException
	{
		ChildCreation(" BRM - 429 Verify that while selecting the Continue after entering the Password and Confirm Password details,the page should be closed and password should reset for the entered email address");
		if(frgtPassPage==true)
		{
			try
			{
				String newpass = BNBasicfeature.getExcelVal("BRM429", sheet, 15); 
				String cnfpass = BNBasicfeature.getExcelVal("BRM429", sheet, 16);
				String cnfpassheader = BNBasicfeature.getExcelVal("BRM429", sheet, 17);
				String cnfpassmessage = BNBasicfeature.getExcelVal("BRM429", sheet, 18);
				String SignpgeTitle = BNBasicfeature.getExcelVal("BRM429", sheet, 2);
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					forgotPasswordNew.sendKeys(newpass);
					forgotPasswordNewCnfm.sendKeys(cnfpass);
					
					jsclick(forgotContinueBtn);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordResetSuccessAlertHeader));
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertHeader))
					{
						log.add("The alert is displayed for the user.");
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = forgotPasswordResetSuccessAlertHeader.getText();
						log.add( "The actual value is : " + actVal);
						if(actVal.equals(cnfpassheader))
						{
							Pass("The alert is displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
						}
						else
						{
							Fail("The alert is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
						}
					}
					else
					{
						Fail("The Success Message Header is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertConfirmation))
					{
						log.add("The confirmation message is displayed for the user.");
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = forgotPasswordResetSuccessAlertConfirmation.getText();
						log.add( "The actual value is : " + cnfpassmessage);
						if(actVal.equals(cnfpassmessage))
						{
							Pass("The confirmation displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
						}
						else
						{
							Fail("The confirmation is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
						}
					}
					else
					{
						Fail("The Success Message Header is not displayed.");
					}
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertCloseBtn))
					{
						Pass("The Close Button is visible for the User.");
						jsclick(forgotPasswordResetSuccessAlertCloseBtn);
						Pass("The Close Button is Clicked Successfully.");
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						Thread.sleep(1000);
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = pgetitle.getText();
						log.add( "The actual value is : " + SignpgeTitle);
						if(actVal.equals(SignpgeTitle))
						{
							Pass("The page is closed and the user is navigated to the Sign In Page Successfully.");
						}
						else
						{
							Fail("The page is closed and the user is not in the Sign In Page. Please Check.");
						}
					}
					else
					{
						Fail("The Close Button is not visible for the User.");
					}
				}
				else
				{
					Fail("The Continue button is not visible for the User. Please Check.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 429 Issue ." + e.getMessage());
				Exception(" BRM - 429 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* BRM - 404 - Verify that while selecting the "customer service" link in "check your mail" page,it should redirects to the respective page*/
	public void forgotpasswordRecoveryCustomerService() throws IOException
	{
		ChildCreation(" BRM - 404 Verify that while selecting the customer service link in check your mail page,it should redirects to the respective page.");
		try
		{
			String customerServiceURL = BNBasicfeature.getExcelVal("BRM404", sheet, 14);
			String emailID = BNBasicfeature.getExcelVal("BRM404", sheet, 6);
			driver.get(BNConstants.mobileSiteURL);
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
			 	wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
			 	frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
				if(frgtPassPage==true)
				{
					Thread.sleep(1000);
					if(BNBasicfeature.isElementPresent(emailId))
					{
						emailId.sendKeys(emailID);
						log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
						if(BNBasicfeature.isElementPresent(forgotContinueBtn))
						{
							jsclick(forgotContinueBtn);
							log.add("The Continue button in the Password Assistant is clicked.");
							wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
							wait.until(ExpectedConditions.visibilityOf(forgotContinueBtn));
							Thread.sleep(100);
							if(BNBasicfeature.isElementPresent(forgotContinueBtn))
							{
								jsclick(forgotContinueBtn);
								Thread.sleep(5000);
								if(BNBasicfeature.isElementPresent(forgotResetSuccessContainer))
								{
									if(BNBasicfeature.isElementPresent(customerServiceLink))
									{
										customerServiceLink.click();
										Thread.sleep(5000);
										driver.switchTo().defaultContent();
										String currentURL = driver.getCurrentUrl();
										if(currentURL.equals(customerServiceURL))
										{
											Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed. The user is successfully navigated to the Customer Service Page. The Current URL of the Customer Service Page is " + currentURL.toString());
										}
										else
										{
											Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed. However user is not successfully navigated to the Customer Service Page. The Current user navigated URL is " + currentURL.toString());
										}
									}
									else
									{
										Fail("The Customer Service Link is not displayed." );
									}
								}
								else
								{
									Fail("The Success Message Container is not displayed." );
								}
							}
							else
							{
								Fail("The Continue Button is not displayed." );
							}
						}
						else
						{
							Fail("The Continue Button is not displayed." );
						}
					}
					else
					{
						Fail("The Email field is not displayed." );
					}
				}
				else
				{
					Fail( " Failed to load the Forgot Password Page.");
				}
			}
			else
			{
				Fail( " User failed to signIn.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 404 Issue ." + e.getMessage());
			Exception(" BRM - 404 Issue." + e.getMessage());
		}
	}
	
   /************************************************* Sign In ************************************************************************/
	
	/*387 - Verify that on clicking the Sign In from the My account drop down in the pancake menu, the Sign In overlay should be enabled*/
	public void signInnav() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 387 Verify that on clicking the Sign In from the My account drop down in the pancake menu, the Sign In overlay should be enabled.");
		try
		{
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				String title = pgetitle.getText();
				String signIntitle = BNBasicfeature.getExcelVal("BRM387", sheet, 2);
				if(title.equals(signIntitle))
				{
					Pass("The User is navigated successfully to the SignIn page and it is Verified.",log);
				}
				else
				{
					Fail("There is something wrong the user is not in the Sign In Page.",log);
				}	
			}
			else
			{
				Fail( "The User failed to sign In.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 387 Issue ." + e.getMessage());
			Exception(" BRM - 387 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1255 verify that on scrolling "Sign In" overlay,the overlay is scrolls properly */
	public void pagescroll() 
	{
		ChildCreation(" BRM - 1255 Verify that on scrolling Sign In overlay,the overlay is scrolls properly.");
		if(signedIn==true)
		{
			try
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				wait.until(ExpectedConditions.visibilityOf(loginDisclaimer));
				if(BNBasicfeature.isElementPresent(loginDisclaimer))
				{
					BNBasicfeature.scrolldown(loginDisclaimer, driver);
					Pass("The Page is scrolled down successfully.");
				}
				else
				{
					Fail("The Page is not scrolled down.");
				}
				
				BNBasicfeature.scrollup(pgetitle, driver);
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The Page is scrolled Up successfully.");
				}
				else
				{
					Fail("The Page is not scrolled Up.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1225 Issue ." + e.getMessage());
				Exception(" BRM - 1225 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 388 - Verify that "Secure Sign in" and "Create Account" buttons are displayed in the Sign In Overlay */
	public void signInbtnchk() throws InterruptedException
	{
		ChildCreation(" BRM - 388 Verify that Secure Sign in and Create Account buttons are displayed in the Sign In Overlay.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(secureSignIn))
				{
					Pass("The Secure Sign In Button is displayed.");
				}
				else
				{
					Fail("The Secure Sign In Button is not displayed.");
				}
			
				if(BNBasicfeature.isElementPresent(createAccbtn))
				{
					Pass("The Create Account button is displayed.");
				}
				else
				{
					Fail("The Create Account button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 388 Issue ." + e.getMessage());
				Exception(" BRM - 388 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/*389 Verify that email address and password fields should be displayed in the Sign In overlay */
	public void emailpasswrdfldchk() throws InterruptedException
	{
		ChildCreation(" BRM - 389 Verify that email address and password fields should be displayed in the Sign In overlay.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(emailId))
				{
					Pass("The Email ID field is displayed.");
				}
				else
				{
					Fail("The Email ID field not displayed.");
				}
			
				if(BNBasicfeature.isElementPresent(password))
				{
					Pass("The Password field is displayed.",log);
				}
				else
				{
					Fail("The Password field is not displayed.",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 389 Issue ." + e.getMessage());
				Exception(" BRM - 389 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/*390 - Verify that "Forgot your password" link should be displayed below the password field*/
	public void frgtpasswordlinkvisiblity() throws InterruptedException
	{
		ChildCreation(" BRM - 390 Verify that Forgot your password link should be displayed below the password field.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotPassword))
				{
					Pass("The Forgot Password link is displayed.");
				}
				else
				{
					Fail("The Forgot Password link is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 390 Issue ." + e.getMessage());
				Exception(" BRM - 390 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/*391 - Verify that on selecting "Secure Sign In" without entering email address and password, the text fields should be highlighted along with the alert message */
	public void SecureSignInalert() throws InterruptedException
	{
		ChildCreation(" BRM - 391 Verify that on selecting Secure Sign In without entering email address and password, the text fields should be highlighted along with the alert message.");
		if(signedIn==true)
		{
			try
			{
				String actualalert = BNBasicfeature.getExcelVal("BRM391", sheet, 2);
				String originalColor = BNBasicfeature.getExcelVal("BRM391", sheet, 3);
				jsclick(secureSignIn);
				wait.until(ExpectedConditions.visibilityOf(signInErralrt));
				String cssval;
				if(BNBasicfeature.isElementPresent(signInErralrt))
				{
					Pass("The Validation alert is raised and it is displayed. ");
					String valalert = signInErralrt.getText();
					if(actualalert.equals(valalert))
					{
						Pass("The Validation alert matches. " + valalert);
						log.add(" Checking the highlighted Color of the Email and Password Field.");
						cssval = emailId.getCssValue("border");
						String emailhexCode = BNBasicfeature.colorfinder(cssval);
						cssval = password.getCssValue("border");
						String pwdhxcnvtcode = BNBasicfeature.colorfinder(cssval);
						if(emailhexCode.equals(originalColor)&&(pwdhxcnvtcode.equals(originalColor)))
						{
							Pass(" Both the Email and Password field is highlighted after validation. The Color code expected was " + originalColor +" .Email text field highlighted color is " + emailhexCode + " and Password highlighted color is " + pwdhxcnvtcode + " and it is as expected.",log);
						}
						else
						{
							Fail("Both the Email and Password field is highlighted after validation. But there is mismatch in the highlighted color. Expected Color was " + originalColor + " but highligted color for the Email text field is " + emailhexCode + " and Password text field is " + pwdhxcnvtcode,log);
						}
					}
					else
					{
						Fail("The Validation alert does not match. " + valalert + " does not matches with the expected alert " + actualalert);
					}
				}
				else
				{
					Fail(" No Validation alert is not raised.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 391 Issue ." + e.getMessage());
				Exception(" BRM - 391 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 392 - Verify that email address text field should accepts the digits, letters and special characters with the limit of 40 characters */
	public void emaillengthValidation() throws InterruptedException
	{
		ChildCreation(" BRM - 392 Verify that email address text field should accepts the digits, letters and special characters with the limit of 40 characters.");
		if(signedIn==true)
		{
			try
			{
				String[] emailval = BNBasicfeature.getExcelVal("BRM392", sheet, 2).split("\n");
				for(int i = 0; i<emailval.length; i++)
				{
					if(BNBasicfeature.isElementPresent(emailId))
					{
						clearAll();
						emailId.sendKeys(emailval[i]);
						String txtinemail = emailId.getAttribute("value").toString();
						if(txtinemail.length() <= 40)
						{
							Pass("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and the value from the excel file was " + emailval + " and it is less than or equal to 40 Characters.");
						}
						else
						{
							Fail("The user was able to enter more than the allowed character. The Current characters length in the Input field is " + txtinemail.length() + " .The entered data to the Email field is " + emailval );
						}
					}
					else
					{
						Fail( "The Email id field is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 392 Issue ." + e.getMessage());
				Exception(" BRM - 392 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 399 - Verify that while selecting "Forgot your password" link, it should navigate to the forgot password page */
	public void forgetPasswordLinkVerify() throws IOException
	{
		ChildCreation(" BRM - 399 Verify that while selecting Forgot your password link, it should navigate to the forgot password page.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotPassword))
				{
					String pageTitle = BNBasicfeature.getExcelVal("BRM399", sheet, 2);
					log.add(" The user is navigated and they are in the Sign In Page.");
					jsclick(forgotPassword);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					log.add("The Expected title was : " + pageTitle);
					String actVal = pgetitle.getText();
					log.add("The actual title is : " + actVal);
					if(actVal.equals(pageTitle))
					{
						Pass("The user are successfully navigated to the Password Assistant Page.",log);
					}
					else
					{
						Fail("The user are not navigated to the Password Assistant Page.",log);
						System.out.println("User is not in the Forgot Password page.");
					}
				}
				else
				{
					Fail( "The Forgot password field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 399 Issue ." + e.getMessage());
				Exception(" BRM - 399 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 393 - Verify that while selecting the "Secure Sign In" button after entering the non-registered email address & password, alert messages should be displayed.*/
	public void nonregisterdAlrt() throws Exception
	{
		ChildCreation(" BRM - 393 Verify that while selecting the Secure Sign In button after entering the non-registered email address & password, alert messages should be displayed.");
		try
		{
			String emailidval = BNBasicfeature.getExcelVal("BRM393", sheet, 2);
			String passval = BNBasicfeature.getExcelVal("BRM393", sheet, 3);
			String expectedalrtVal = BNBasicfeature.getExcelVal("BRM393", sheet, 4);
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				log.add(" The user is navigated and they are in the Sign In Page.");
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					wait.until(ExpectedConditions.and
							(
								ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
								ExpectedConditions.visibilityOf(signInErralrt)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(signInErralrt));
					if(BNBasicfeature.isElementPresent(signInErralrt))
					{
						String actualalrtVal = signInErralrt.getText();
						log.add("The alert for the invalid login is raised and displayed. The raised alert was " + actualalrtVal);
						if(actualalrtVal.equals(expectedalrtVal))
						{
							log.add("The expected alert was " + expectedalrtVal);
							Pass("The raised alert matches with the specified alert content.",log);
						}
						else
						{
							log.add("The expected alert was " + expectedalrtVal);
							Fail("The raised alert does not matches with the specified alert content.",log);
						}
					}
					else
					{
						Fail("The invalid login alert is not raised .");
					}
				}
				else
				{
					Fail("The email field or password or sign in is not displayed.");
				}
			}
			else
			{
				Fail("The user failed to load the Sign In page.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 393 Issue ." + e.getMessage());
			Exception(" BRM - 393 Issue." + e.getMessage());
		}
	}
	
	/* 1083- Verify that on clicking "Cancel" button in "Create Account" overlay ,it should be closed displaying the previous "Sign-in" overlay*/
	/* BRM - 430 Verify that while selecting the continue button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed. */
	public void cancelbtnnav() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 1083 Verify that on clicking Cancel button in Create Account overlay ,it should be closed displaying the previous Sign-in overlay.");
		boolean BRM450 = false;
		if(signedIn==true)
		{
			try
			{
				String createAcc = BNBasicfeature.getExcelVal("BRM450", sheet, 2);
				createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
				if(createAccPage==true)
				{
					String Signintitle = BNBasicfeature.getExcelVal("BRM1083", sheet, 2);
					if(pgetitle.getText().contains(createAcc))
					{
						log.add("The User is successfully navigated to the Create Account Page.");
						Pass("The User is successfully navigated to the Create Account Page and they are expected.",log);
						BRM450 = true;
					}
					else
					{
						Fail("There is some mismatch in the title it seems. Please Check where you are. ",log);
					}
					
					jsclick(createAccountCancelBtn);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					log.add( "The Expected title was : " +Signintitle);
					String acttitle = pgetitle.getText(); 
					if(acttitle.equals(Signintitle))
					{
						Pass(" The user is navigated back and they are in the Sign In Page.",log);
					}
					else
					{
						Fail(" The user is not navigated back to the Sign In Page.",log);
					}
				}
				else
				{
					Fail( "Fail to load the create Account Page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1083 Issue ." + e.getMessage());
				Exception(" BRM - 1083 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
		
		/* BRM - 430 Verify that while selecting the continue button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed. */
		ChildCreation(" BRM - 430 Verify that while selecting the continue button without entering the details in the new-password & re-enter password text fields,the alert message should be displayed.");
		if(signedIn==true)
		{
			if(BRM450==true)
			{
				Pass( "The User is navigated to the create account page.");
			}
			else
			{
				Fail( "The User was not navigated to the create account page.");
			}
		}
		else
		{
			Fail("The user failed to navigate to sign in page.");
		}
	}
	
	/* 394 - Verify that while selecting the "Secure Sign In" after entering the registered email address and invalid password, the alert message should be raised. */
	public void invalidPasswordAlrt() throws IOException
	{
		ChildCreation("BRM - 394 Verify that while selecting the Secure Sign In after entering the registered email address and invalid password, the alert message should be raised. ");
		if(signedIn==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRM394", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRM394", sheet, 3);
				String expectedalrtVal = BNBasicfeature.getExcelVal("BRM394", sheet, 4);
				wait.until(ExpectedConditions.visibilityOf(secureSignIn));
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					log.add(" The user is navigated and they are in the Sign In Page.");
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					wait.until(ExpectedConditions.and
							(
								ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
								ExpectedConditions.visibilityOf(signInErralrt)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(signInErralrt));
					String actualalrtVal = signInErralrt.getText();
					if(BNBasicfeature.isElementPresent(signInErralrt))
					{
						log.add("The alert for the invalid login attempt is raised and displayed. The raised alert was " + actualalrtVal);
						log.add("The expected alert was " + expectedalrtVal);
						if(actualalrtVal.equals(expectedalrtVal))
						{
							Pass("The raised alert matches with the specified alert content.",log);
						}
						else
						{
							Fail("The raised alert does not matches with the specified alert content.");
						}						
					}
					else
					{
						Fail("The invalid login alert is not raised .",log);
					}
				}
				else
				{
					Fail( "The Email Id field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 394 Issue ." + e.getMessage());
				Exception(" BRM - 394 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 395 - Verify that selecting the "Secure Sign In" button after entering the invalid email address and valid password, alert message should be displayed. */
	public void invalidEmailAlrt() throws IOException
	{
		ChildCreation(" BRM - 395 Verify that selecting the Secure Sign In button after entering the invalid email address and valid password, alert message should be displayed. ");
		if(signedIn==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRM395", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRM395", sheet, 3);
				String expectedalrtVal = BNBasicfeature.getExcelVal("BRM395", sheet, 4);
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					wait.until(ExpectedConditions.and
							(
								ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
								ExpectedConditions.visibilityOf(signInErralrt)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(signInErralrt));
					String actualalrtVal = signInErralrt.getText();
					if(BNBasicfeature.isElementPresent(signInErralrt))
					{
						log.add("The alert for the invalid login attempt with invalid Email address and Valid Password is raised and displayed. The raised alert was " + actualalrtVal);
						log.add("The expected alert was " + expectedalrtVal);
						if(actualalrtVal.equals(expectedalrtVal))
						{
							Pass("The raised alert matches with the specified alert content.",log);
						}
						else
						{
							Fail("The raised alert does not matches with the specified alert content.",log);
						}		
					}
					else
					{
						Fail("The Invalid Email Alert is not displayed.",log);
					}
				}
				else
				{
					Fail("The email field or password or sign in is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 395 Issue ." + e.getMessage());
				Exception(" BRM - 395 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 397 - Verify that while selecting the "Secure Sign In" button after entering the html tags in the email fields,the input should not be treated as string */
	public void htmltagValidation() throws IOException
	{
		ChildCreation(" BRM - 397 Verify that while selecting the Secure Sign In button after entering the html tags in the email fields,the input should not be treated as string. ");
		if(signedIn==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRM397", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRM397", sheet, 3);
				String expectedalrtVal = BNBasicfeature.getExcelVal("BRM397", sheet, 4);
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					log.add(" The user is navigated and they are in the Sign In Page.");
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					wait.until(ExpectedConditions.and
							(
								ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
								ExpectedConditions.visibilityOf(signInErralrt)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(signInErralrt));
					String actualalrtVal = signInErralrt.getText();
					log.add( "The Actual alert is  : " + actualalrtVal);
					if(BNBasicfeature.isElementPresent(signInErralrt))
					{
						log.add("The alert for the invalid Email address is raised and displayed. The raised alert was " + actualalrtVal);
						if(actualalrtVal.equals(expectedalrtVal))
						{
							Pass("The raised alert matches with the specified alert content.",log);
						}
						else
						{
							Fail("The raised alert does not matches with the specified alert content.",log);
						}		
					}
					else
					{
						Fail("No alert is displayed.",log);
					}
				}
				else
				{
					Fail("The email field or password or sign in is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 397 Issue ." + e.getMessage());
				Exception(" BRM - 397 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 400 - Verify that while selecting "Terms of Use" option, the terms of use overlay should be displayed */
	/* BRM - 1131 Verify that Promo messages and Header in Terms Of Use and Privacy Policy page are similar as in home page. */
	public void termsOfUseverify() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 400 Verify that while selecting Terms of Use option, the terms of use overlay should be displayed .");
		if(signedIn==true)
		{
			try
			{
				String expTitle = BNBasicfeature.getExcelVal("BRM400", sheet, 4);
				if(BNBasicfeature.isElementPresent(loginDisclaimer))
				{
					Pass("The Login Disclaimer Container is displayed.");
					if(BNBasicfeature.isElementPresent(loginDisclaimerTermsofUse))
					{
						Pass("The Terms of Use link is found.");
						jsclick(loginDisclaimerTermsofUse);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(TermsofUsePrivacyPolicyTitle));
						Thread.sleep(1000);
						if(BNBasicfeature.isElementPresent(TermsofUsePrivacyPolicyTitle))
						{
							Pass("The Terms of Use disclaimer title is found.");
							log.add("The Expected title was : " + expTitle);
							String actTitle = TermsofUsePrivacyPolicyTitle.getText();
							if(expTitle.contains(actTitle))
							{
								Pass("The User is navigated to the Terms of Use Page.");
							}
							else
							{
								Fail("The User is not navigated to the Terms of Use Page.");
							}
						}
						else
						{
							Fail("The Terms of Use disclaimer title is not found.");
						}
					}
					else
					{
						Fail("The Terms of Use link is not found.");
					}
				}
				else
				{
					Fail("The Login Disclaimer Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 400 Issue ." + e.getMessage());
				Exception(" BRM - 400 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
		
		/*BRM - 1131 Verify that Promo messages and Header in Terms Of Use and Privacy Policy page are similar as in home page. */
		ChildCreation(" BRM - 1131 Verify that Promo messages and Header in Terms Of Use and Privacy Policy page are similar as in home page.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(promoCarousel))
				{
					Pass("The PromoCarousel is displayed in the Terms of Use page.");
				}
				else
				{
					Fail("The PromoCarousel is not displayed in the Terms of Use page.");
				}
				
				if(BNBasicfeature.isElementPresent(homepageHeader))
				{
					Pass("The Tab Header is displayed in the Terms of Use page.");
				}
				else
				{
					Fail("The Tab Header is not displayed in the Terms of Use page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1131 Issue ." + e.getMessage());
				Exception(" BRM - 1131 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 401 - Verify that while selecting "Privacy policy" option,the "Privacy Policy" overlay should be displayed  loginDisclaimerpolicy*/
	/*BRM - 1131 Verify that Promo messages and Header in Terms Of Use and Privacy Policy page are similar as in home page. */
	public void policyPage() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 401 Verify that while selecting Terms of Use option, the terms of use overlay should be displayed .");
		try
		{
			String expTitle = BNBasicfeature.getExcelVal("BRM401", sheet, 4);
			signedIn=signInExistingCustomer();
			if(signedIn==true)
			{
				if(BNBasicfeature.isElementPresent(loginDisclaimer))
				{
					Pass("The Login Disclaimer Container is displayed.");
					if(BNBasicfeature.isElementPresent(loginDisclaimerpolicy))
					{
						Pass("The Privacy Policy link is found.");
						jsclick(loginDisclaimerpolicy);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						if(BNBasicfeature.isElementPresent(TermsofUsePrivacyPolicyTitle))
						{
							Pass("The Privacy Policy disclaimer title is found.");
							log.add("The Expected title was : " + expTitle);
							String actTitle = TermsofUsePrivacyPolicyTitle.getText();
							if(expTitle.contains(actTitle))
							{
								Pass("The User is navigated to the Terms of Use Page.");
							}
							else
							{
								Fail("The User is not navigated to the Terms of Use Page.");
							}
						}
						else
						{
							Fail("The Privacy Policy title is not found.");
						}
					}
					else
					{
						Fail("The Privacy Policy link is not found.");
					}
				}
				else
				{
					Fail("The Login Disclaimer Container is not displayed.");
				}
			}
			else
			{
				Fail( "The User failed to sign in.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 400 Issue ." + e.getMessage());
			Exception(" BRM - 400 Issue." + e.getMessage());
		}
		
		ChildCreation(" BRM - 1131 Verify that Promo messages and Header in Terms Of Use and Privacy Policy page are similar as in home page.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(promoCarousel))
				{
					Pass("The PromoCarousel is displayed in the Privacy Policy page.");
				}
				else
				{
					Fail("The PromoCarousel is not displayed in the Privacy Policy page.");
				}
				
				if(BNBasicfeature.isElementPresent(homepageHeader))
				{
					Pass("The Tab Header is displayed in the Privacy Policy page.");
				}
				else
				{
					Fail("The Tab Header is not displayed in the Privacy Policy page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1131 Issue ." + e.getMessage());
				Exception(" BRM - 1131 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 410 Verify that "Remember me"  should be enabled as default in the "Sign In' overlay */
	public void rememberMeCheckboxstate() throws IOException
	{
		ChildCreation(" BRM - 410 Verify that Remember me should be enabled as default in the Sign In overlay.");
		try
		{
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				log.add(" The user is navigated and they are in the Sign In Page.");
				if(rememberMe.isSelected())
				{
					Fail("The Remember Me Checkbox is checked by default.",log);
				}
				else
				{
					Pass("The Remember Me Checkbox is not checked by default.",log);
				}
			}
			else
			{
				Fail( "The User failed to sign In.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 410 Issue ." + e.getMessage());
			Exception(" BRM - 410 Issue." + e.getMessage());
		}
	}
	
	/* 447 - Verify that "Email address" and "Password" default text should be displayed in the fields */
	public void passwordFieldTextChk() throws IOException
	{
		ChildCreation(" BRM - 447 Verify that Email address and Password default text should be displayed in the fields.");
		if(signedIn==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRM447", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRM447", sheet, 3);
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(emailtxt))&&(BNBasicfeature.isElementPresent(passwordtxt)))
				{
					clearAll();
					log.add( "The Expected value was : " + emailidval);
					String actVal = emailtxt.getText(); 
					log.add( "The actual value is : " + actVal);
					if(actVal.contains(emailidval))
					{
						Pass("The Email Id text value matches with the Expected value.");
					}
					else
					{
						Fail("The Email Id text value does not matche with the Expected value.");
					}
				
					log.add( "The Expected value was : " + passval);
					actVal = passwordtxt.getText(); 
					log.add( "The actual value is : " + actVal);
					if(actVal.contains(passval))
					{
						Pass("The Password text value matches with the Expected value.");
					}
					else
					{
						Fail("The Password text value does not matche with the Expected value.");
					}
				}
				else
				{
					Fail( "The Email field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 447 Issue ." + e.getMessage());
				Exception(" BRM - 447 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 450 - Verify that while selecting the "Create Account" button,the Create Account overlay should be enabled */
	public void createAccNav() throws IOException
	{
		ChildCreation(" BRM - 450 Verify that while selecting the Create Account button,the Create Account overlay should be enabled.");
		if(signedIn==true)
		{
			try
			{
				String createAcc = BNBasicfeature.getExcelVal("BRM450", sheet, 2);
				wait.until(ExpectedConditions.elementToBeClickable(createAccbtn));
				wait.until(ExpectedConditions.visibilityOf(createAccbtn));
				createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
				if(createAccPage==true)
				{
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					try
					{
						log.add(" The user is navigated and they are in the Create Account Page.");
						if(pgetitle.getText().contains(createAcc))
						{
							log.add("The User is successfully navigated to the Create Account Page.");
							Pass("The User is successfully navigated to the Create Account Page and they are expected.",log);
						}
						else
						{
							Fail("There is some mismatch in the title it seems. Please Check where you are. ",log);
						}
					}
					catch(Exception e)
					{
						Exception(" There is something wrong with the page or element is not found. Please Check." + e.getMessage());
					}
					jsclick(signInlnk);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					BRM357 = true;
				}
				else
				{
					Fail( "Fail to load the Create Account page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 450 Issue ." + e.getMessage());
				Exception(" BRM - 450 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 454 - Verify that Email address should accept 40 characters & password should accept 6-15 characters */
	public void emaillengthchk() throws IOException
	{
		ChildCreation(" BRM - 454 Verify that Email address should accept 40 characters & password should accept 6-15 characters.");
		if(signedIn==true)
		{
			try
			{
				String[] emailval = BNBasicfeature.getExcelVal("BRM454", sheet, 2).split("\n");
				String[] passval = BNBasicfeature.getExcelVal("BRM454", sheet, 3).split("\n");
				if((BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))))
				{
					wait.until(ExpectedConditions.elementToBeClickable(emailId));
					for(int i = 0;i<emailval.length; i++)
					{
						clearAll();
						log.add(" The user is navigated and they are in the Sign In Page.");
						emailId.sendKeys(emailval[i]);
						String txtinemail = emailId.getAttribute("value").toString();
						if(txtinemail.length() <= 40)
						{
							Pass("The user was successfull in entering the value in the Email Id Field. And the user entered data was " + txtinemail + " and the value from the excel file was " + emailval + " and it is less than or equal to 40 Characters.");
						}
						else
						{
							Fail("The user was able to enter more than the allowed character. The Current characters length in the Input field is " + txtinemail.length() + " .The entered data to the Email field is " + emailval );		
						}
						
						
						password.sendKeys(passval[i]);
						String passvaltxt = password.getAttribute("value").toString();
						if((passvaltxt.length() >=8) && (passvaltxt.length() <=15))
						{
							Pass("The user was successfull in entering the value in the password Field. And the user entered data was " + passvaltxt + " and the value from the excel file was " + passval + " and it is less than or equal to 15 Characters.");
						}
						else
						{
							Fail(" The user was able to enter more than the allowed character. The Current characters length in the Input field is " + passvaltxt.length() + " .The entered data to the Password field is " + passvaltxt );
						}
					}
				}
				else
				{
					Fail(" The Email Id and Password field is not displayed. ");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 454 Issue ." + e.getMessage());
				Exception(" BRM - 454 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 575 - Verify that password text field should accepts the digits, letters and special characters with the limit of 6 to 15 characters and if a user enters password less than 6 characters then alert must be displayed */
	public void pwdlengthchk() throws IOException
	{
		ChildCreation(" BRM - 575 Verify that password text field should accepts the digits, letters and special characters with the limit of 6 to 15 characters and if a user enters password less than 6 characters then alert must be displayed.");
		if(signedIn==true)
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{	
				String tcid = "BRM575";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					String emailval = sheet.getRow(i).getCell(2).getStringCellValue();
					try
					{
						if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
						{
							clearAll();
							log.add(" The user is navigated and they are in the Sign In Page.");
							emailId.sendKeys(emailval);
							String passval = sheet.getRow(i).getCell(3).getStringCellValue();
							password.sendKeys(passval);
							String passvaltxt = password.getAttribute("value").toString();
							if((passvaltxt.length() >=8) && (passvaltxt.length() <=15))
							{
								Pass("The user was successfull in entering the value in the password Field. And the user entered data was " + passvaltxt + " and the value from the excel file was " + passval + " and it is less than or equal to 15 Characters.");
							}
							else if(passvaltxt.length()<8)
							{
								jsclick(secureSignIn);
								wait.until(ExpectedConditions.and
										(
											ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
											ExpectedConditions.visibilityOf(signInErralrt)
										)
								);
								wait.until(ExpectedConditions.visibilityOf(signInErralrt));
								//Thread.sleep(1000);
								String actualalrtVal = signInErralrt.getText();
								String expectedalrtVal = sheet.getRow(i).getCell(4).getStringCellValue();
								if(BNBasicfeature.isElementPresent(signInErralrt))
								{
									log.add("The alert is raised and it is displayed.");
									if(actualalrtVal.equals(expectedalrtVal))
									{
										Pass("The alert matches with the expected alert content.",log);
									}
									else
									{
										Fail("The alert does not matches with the expected alert content.",log);
									}
								}
								else 
								{
									Fail("The alert is not raised.");
								}
							}
							else
							{
								Fail(" The user was able to enter more than the allowed character. The Current characters length in the Input field is " + passvaltxt.length() + " .The entered data to the Password field is " + passvaltxt );
							}
						}
						else
						{
							Fail("The email field or password or sign in is not displayed.");
						}
					}
					catch(Exception e)
					{
						Exception(" BNM - 575 Issue ." + e.getMessage());
						System.out.println("BNM - 575 Issue ." + e.getMessage());
					}
				}
			}
		}
		else
		{
			Fail( "The User failed to load the sign in page.");
		}
	}
	
	public void cancelbtnnav2() throws InterruptedException, IOException
	{
		if(signedIn==true)
		{
			try
			{
				signedIn = false;
				boolean pgLoaded = ldPage(signInlnk, createAccbtn, createAccFrame);
				if(pgLoaded==true)
				{
					jsclick(createAccountCancelBtn);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					signedIn = true;
				}
				else
				{
					Fail( "Fail to load the Create Acount page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}
	
	/* 1287- Verify that while selecting the  "Secure SignIn" option after entering the invalid email address format,the alert message should be displayed*/
	public void invalidemailformat() throws Exception
	{
		ChildCreation(" BRM - 1287 Verify that while selecting the Secure SignIn option after entering the invalid email address format,the alert message should be displayed.");
		if(signedIn==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					String emailidval = BNBasicfeature.getExcelVal("BRM1287", sheet, 2);
					String passval = BNBasicfeature.getExcelVal("BRM1287", sheet, 3);
					String expectedalrtVal = BNBasicfeature.getExcelVal("BRM1287", sheet, 4);
					log.add(" The user is navigated and they are in the Sign In Page.");
					clearAll();
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					wait.until(ExpectedConditions.or
							(
								ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
								ExpectedConditions.visibilityOf(signInErralrt)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(signInErralrt));
					String actualalrtVal = signInErralrt.getText();
					if(BNBasicfeature.isElementPresent(signInErralrt))
					{
						log.add("The alert for the invalid Email address is raised and displayed.");
						log.add("The expected alert was " + expectedalrtVal);
						log.add("The actual alert is " + actualalrtVal);
						if(actualalrtVal.equals(expectedalrtVal))
						{
							Pass("The raised alert matches with the specified alert content.",log);
						}
						else
						{
							Fail("The raised alert does not matches with the specified alert content. The test data for the invalid email used was " + emailidval.toString());
						}		
					}
					else
					{
						Fail("No alert is displayed.");
					}
				}
				else
				{
					Fail("The email field or password or sign in is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1287 Issue ." + e.getMessage());
				Exception(" BRM - 1287 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 1289 - Verify that when the user tries to "Sign In" using valid email address and invalid password,the "You have exceeded 6 login attempts, please try again after 30 minutes." alert message should be displayed.*/
	public void loginlimit()
	{
		ChildCreation("  BRM - 1289 Verify that when the user tries to Sign In using valid email address and invalid password,the You have exceeded 6 login attempts, please try again after 30 minutes. alert message should be displayed.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					log.add(" The user is navigated and they are in the Sign In Page.");
					String emailidval = BNBasicfeature.getExcelVal("BRM1289", sheet, 2);
					String passval = BNBasicfeature.getExcelVal("BRM1289", sheet, 3);
					String actualalrtVal;
					String expectedalrtVal = BNBasicfeature.getExcelVal("BRM1289", sheet, 4);
					int j = 1;
					do
					{
						emailId.clear();
						password.clear();
						emailId.sendKeys(emailidval);
						password.sendKeys(passval);
						//secureSignIn.click();
						//BNBasicfeature.click(secureSignIn, driver);
						jsclick(secureSignIn);
						wait.until(ExpectedConditions.and
								(
									ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"),
									ExpectedConditions.visibilityOf(signInErralrt)
								)
						);
						wait.until(ExpectedConditions.visibilityOf(signInErralrt));
						actualalrtVal = signInErralrt.getText();
						j++;
					} while(j<=7);
						
					
					if(actualalrtVal.equals(expectedalrtVal))
					{
						log.add("The expected alert was " + expectedalrtVal);
						log.add("The actual alert was " + actualalrtVal);
						Pass("The raised alert matches with the specified alert content.",log);
					}
					else
					{
						log.add("The expected alert was " + expectedalrtVal);
						log.add("The actual alert was " + actualalrtVal);
						Fail("The raised alert does not matches with the specified alert content. The test data for the invalid email used was " + emailidval.toString(),log);
					}
				}
				else
				{
					Fail("The email field or password or sign in is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1289 Issue ." + e.getMessage());
				Exception(" BRM - 1289 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* 396 - Verify that while selecting the "Secure Sign In" button after entering the registered email address and valid password,the user should be successfully  logged-in and user name should be displayed */ 
	public void successfulLogin() throws IOException
	{
		ChildCreation(" BRM - 396 Verify that while selecting the Secure Sign In button after entering the registered email address and valid password,the user should be successfully logged-in and user name should be displayed. ");
		if(signedIn==true)
		{
			try
			{
				signedIn = false;
				String emailidval = BNBasicfeature.getExcelVal("BRM396", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRM396", sheet, 3);
				String welcomemsg = BNBasicfeature.getExcelVal("BRM396", sheet, 5);
				String username = BNBasicfeature.getExcelVal("BRM396", sheet, 6);
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					log.add(" The user is navigated and they are in the Sign In Page.");
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					String finalSt = welcomemsg.concat(username);
					try
					{
						wait.until(ExpectedConditions.visibilityOf(header));
						jsclick(menu);
						wait.until(ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"));
						wait.until(ExpectedConditions.elementToBeClickable(loggedInAccntName));
						String actName  =  loggedInAccntName.getText();
						log.add( "The Expected name was : " + finalSt);
						log.add( "The Actual name is : " + actName);
						if(actName.contains(finalSt))
						{
							Pass("The user is able to successfully login and they are in the Home Page. " + actName,log);
						}
						else
						{
							Fail("The user is able to successfully login and they are in the Home Page. But there is mimatch in the name." + actName,log);
						}
					}
					catch(Exception e)
					{
						System.out.println("There is something wrong user is not signed in.");
						Exception(" There is something wrong with the page or element is not found. Please Check." + e.getMessage());
					}
				}
				else
				{
					Fail( "The Email or password field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 396 Issue ." + e.getMessage());
				Exception(" BRM - 396 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
		
		ChildCreation(" BRM - 39 Verify whether the logo is displayed as per the creative.");
		try
		{
			if(BNBasicfeature.isElementPresent(bnLogo))
			{
				Pass("The Logo is displayed in the Main Page.");
			}
			else
			{
				Fail("The Logo is not displayed in the Main Page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 39 Issue ." + e.getMessage());
			Exception(" BRM - 39 Issue ." + e.getMessage());
		}
	}
	
	/************************************************** Create Account **************************************************************/
	
	/* 354 - Verify that while selecting the "Create an Account" button from the "Sign In" overlay,the "Create Account" overlay should be displayed with the input fields, create account and cancel button */
	public void createAcntfieldchk() throws Exception
	{
		ChildCreation(" BRM - 354 Verify that while selecting the Create an Account button from the Sign In overlay,the Create Account overlay should be displayed with the input fields, create account and cancel button.");
		try
		{
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				
			}
			createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
			if(createAccPage==true)
			{
				if(BNBasicfeature.isElementPresent(ccFName))
				{
					Pass("The First Name field is present.");
				}
				else
				{
					Fail("The First Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccLName))
				{
					Pass("The Last Name field is present.");
				}
				else
				{
					Fail("The Last Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccEmailId))
				{
					Pass("The Email Address field is present.");
				}
				else
				{
					Fail("The Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfEmailId))
				{
					Pass("The Confirm Email Address field is present.");
				}
				else
				{
					Fail("The Confirm Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccPassword))
				{
					Pass("The Password field is present.");
				}
				else
				{
					Fail("The Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfPass))
				{
					Pass("The Confirm Password field is present.");
				}
				else
				{
					Fail("The Confirm Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					Pass("The Security Question field is present.");
				}
				else
				{
					Fail("The Security Question field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityAnswer))
				{
					Pass("The Security Answer field is present.");
				}
				else
				{
					Fail("The Security Answer field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCreateBtn))
				{
					Pass("The Create Account button is present.");
				}
				else
				{
					Fail("The Create Account button is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCancelBtn))
				{
					Pass("The Cancel button is present.",log);
				}
				else
				{
					Fail("The Cancel button is not present.",log);
				}
			}
			else
			{
				Fail( "The User is not navigated to the Create Account page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 354 Issue ." + e.getMessage());
			Exception(" BRM - 354 Issue ." + e.getMessage());
		}
	}
	
	/* 357 - Verify that while selecting the "Sign In" button in the (If you already have an account, please sign in) text,it should navigate to the "Sign In" overlay */
	public void signInNavigation() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 357 Verify that while selecting the Sign In button in the (If you already have an account, please sign in) text,it should navigate to the Sign In overlay.");
		if(createAccPage==true)
		{
			try
			{
				if(BRM357==true)
				{
					Pass("The user is navigated from the Created Account page to the Sign In Page.",log);
				}
				else
				{
					String pagetitle = BNBasicfeature.getExcelVal("BRM357", sheet, 2);
					jsclick(signInlnk);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					if(BNBasicfeature.isElementPresent(pgetitle))
					{
						createAccPage = false;
						log.add("The user is redirected to the Sign In Page.");
						if(pgetitle.getText().equals(pagetitle))
						{
							Pass("The user is navigated from the Created Account page to the Sign In Page.",log);
						}
						else
						{
							Fail("Something went wrong. Please Check...",log);
						}
						Thread.sleep(1000);
						createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
						if(createAccPage==true)
						{
							wait.until(ExpectedConditions.visibilityOf(signInlnk));
						}
					}
					else
					{
						Fail("The Page Title is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 357 Issue ." + e.getMessage());
				Exception(" BRM - 357 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 360 - Verify that default text should be displayed in the First Name,Last Name,Email Address,Re-Enter Email Address,Password,Confirm Password,Security Question and Answer and Security Answer fields*/
	public void fieldtxtchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 360 Verify that default text should be displayed in the First Name,Last Name,Email Address,Re-Enter Email Address,Password,Confirm Password,Security Question and Answer and Security Answer fields.");
		if(createAccPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(ccFNametxt))
				{
					Pass("The default text for the First Name field is displayed. ");
				}
				else
				{
					Fail("The default text for the First Name field is not displayed. ");
				}
				
				if(BNBasicfeature.isElementPresent(ccLNametxt))
				{
					Pass("The default text for the Last Name field is displayed. ");
				}
				else
				{
					Fail("The default text for the Email field is not displayed. ");
				}
				
				if(BNBasicfeature.isElementPresent(ccEmailIdtxt))
				{
					Pass("The default text for the Email Address field is displayed. ");
				}
				else
				{
					Fail("The default text for the Email Address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfEmailIdtxt))
				{
					Pass("The default text for the Confirm Email Address field is displayed. ");
				}
				else
				{
					Fail("The default text for the Confirm Email Address field is not displayed. ");
				}
				
				if(BNBasicfeature.isElementPresent(ccPasswordtxt))
				{
					Pass("The default text for the Password field is displayed.");
				}
				else
				{
					Fail("The default text for the Password field is not displayed.");
				}
				
				BNBasicfeature.scrolldown(emailId, driver);
				
				if(BNBasicfeature.isElementPresent(ccCnfPasstxt))
				{
					Pass("The default text for the Confirm Password field is displayed. ");
				}
				else
				{
					Fail("The default text for the Confirm Password field is not displayed. ");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityQuestiontxt))
				{
					Pass("The default text for the Security Question field is displayed. ");
				}
				else
				{
					Fail("The default text for the Security Question field is not displayed. ");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityAnswertxt))
				{
					Pass("The default text for the Security Answer field is displayed. ");
				}
				else
				{
					Fail("The default text for the Security Answer field is not displayed. ");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 360 Issue ." + e.getMessage());
				Exception(" BRM - 360 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 363 - Verify that while selecting the "Create Account" button without entering any values in all the input fields,all the fields should be highlighted along with the alert message */
	public void createAccVal() throws IOException, InterruptedException
	{
		ChildCreation(" BRM- 363 Verify that while selecting the Create an Account button from the Sign In overlay,the Create Account overlay should be displayed with the input fields, create account and cancel button.");
		if(createAccPage==true)
		{
			try
			{
				webEle = new ArrayList<>();
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					String error = BNBasicfeature.getExcelVal("BRM363", sheet, 3); 
					String Colorcomp = BNBasicfeature.getExcelVal("BRM363", sheet, 5); 
					jsclick(createAccountCreateBtn);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						BNBasicfeature.scrollup(erromsg, driver);
						wait.until(ExpectedConditions.visibilityOf(erromsg));
						log.add("The Expected alert message was : " + error);
						BNBasicfeature.scrollup(pgetitle, driver);
						String errormessage = erromsg.getText();
						log.add("The Actual alert message is : " + errormessage);
						if(error.equals(errormessage))
						{
							Pass("The raised error message matches the expected value when there is no data entered in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
						
						String fnamehex = ccFName.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(fnamehex).equals(Colorcomp))
						{
							Pass("The First Name field is highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(fnamehex));
						}
						else
						{
							Fail("The First Name field is not highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(fnamehex));
						}
								
						String lnamehex = ccLName.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(lnamehex).equals(Colorcomp))
						{
							Pass("The Last Name field is highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(lnamehex));
						}
						else
						{
							Fail("The Last Name field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(lnamehex));
						}
								
						String emailhex = ccEmailId.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(emailhex).equals(Colorcomp))
						{
							Pass("The Email field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(emailhex));
						}
						else
						{
							Fail("The Email field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(emailhex));
						}
								
						String confirmemailhex = ccCnfEmailId.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(confirmemailhex).equals(Colorcomp))
						{
							Pass("The Confirm Email field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmemailhex));
						}
						else
						{
							Fail("The Confirm Email field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmemailhex));
						}
								
						String passhex = ccPassword.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(passhex).equals(Colorcomp))
						{
							Pass("The Password field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(passhex));
						}
						else
						{
							Fail("The Password field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(passhex));
						}
								
						String confirmpasshex = ccCnfPass.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(confirmpasshex).equals(Colorcomp))
						{
							Pass("The Confirm Password field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmpasshex));
						}
						else
						{
							Fail("The Confirm Password field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmpasshex));
						}
								
						BNBasicfeature.scrolldown(ccCnfPass, driver);
						
						String secanshex = ccSecurityAnswer.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(secanshex).equals(Colorcomp))
						{
							Pass("The Security Answer field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(secanshex));
						}
						else
						{
							Fail("The Security Answer field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(secanshex));
						}
					}
					else
					{
						Fail("The Error alert message is not displayed.");
					}
				}
				else
				{
					Fail("Issue in Clearing the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 363 Issue ." + e.getMessage());
				Exception(" BRM - 363 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 362 - Verify that user should be able to enter the values in all the input text fields */
	public void checkisvalenterable() throws IOException, InterruptedException
	{
		ChildCreation("  BRM - 362 Verify that user should be able to enter the values in all the input text fields.");
		if(createAccPage==true)
		{
			try
			{
				String fstName = BNBasicfeature.getExcelVal("BRM362", sheet, 6);
				String lstName =  BNBasicfeature.getExcelVal("BRM362", sheet, 7);
				String email = BNBasicfeature.getExcelVal("BRM362", sheet, 8); 
				String cnfemail = BNBasicfeature.getExcelVal("BRM362", sheet, 9); 
				String pass = BNBasicfeature.getExcelVal("BRM362", sheet, 10); 
				String cnfpass = BNBasicfeature.getExcelVal("BRM362", sheet, 11);
				String secAns = BNBasicfeature.getExcelVal("BRM362", sheet, 13); 
				//clearAll();
				if(BNBasicfeature.isElementPresent(ccFName))
				{
					BNBasicfeature.scrollup(ccFName, driver);
					/*Thread.sleep(500);
					fName.click();
					Thread.sleep(500);*/
					ccFName.sendKeys(fstName);
					log.add("The First Name field is present.");
					String val = ccFName.getAttribute("value").toString();
					log.add("The Entered Value for the first name is  : " + fstName);
					log.add("The Value in the First Name field is  : " + val);
					if(val.equals(fstName))
					{
						Pass("The First Name is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The First Name is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The First Name is not displayed.");	
				}
						
				if(BNBasicfeature.isElementPresent(ccLName))
				{
					/*lName.click();
					//Thread.sleep(500);
	*/				ccLName.sendKeys(lstName);
					log.add("The Last Name field is present.");
					String val = ccLName.getAttribute("value").toString();
					log.add("The Entered Value for the last name is  : " + lstName);
					log.add("The Value in the First Name field is  : " + val);
					if(val.equals(lstName))
					{
						Pass("The Last Name is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Last Name is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Last Name is not displayed.");	
				}
						
				if(BNBasicfeature.isElementPresent(ccEmailId))
				{
					/*emailId.click();
					Thread.sleep(500);*/
					ccEmailId.sendKeys(email);
					log.add("The Email Id field is present.");
					String val = ccEmailId.getAttribute("value").toString();
					log.add("The Entered Value for the email field is  : " + email);
					log.add("The Value in the email field is  : " + val);
					if(val.equals(email))
					{
						Pass("The Email Id is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Email Id is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Email Id is not displayed.");	
				}
						
				if(BNBasicfeature.isElementPresent(ccCnfEmailId))
				{
					/*confirmEmail.click();
					Thread.sleep(500);*/
					ccCnfEmailId.sendKeys(cnfemail);
					log.add("The Confirm Email Id field is present.");
					String val = ccCnfEmailId.getAttribute("value").toString();
					log.add("The Entered Value for the confirm email field is  : " + cnfemail);
					log.add("The Value in the confirm email field is  : " + val);
					if(val.equals(cnfemail))
					{
						Pass("The Confirm Email Id is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Confirm Email Id is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Confirm Email Id is not displayed.");	
				}
				if(BNBasicfeature.isElementPresent(ccPassword))
				{
					/*password.click();
					Thread.sleep(500);*/
					ccPassword.sendKeys(pass);
					log.add("The Password Id field is present.");
					String val = ccPassword.getAttribute("value").toString();
					log.add("The Entered Value for the password field is  : " + pass);
					log.add("The Value in the password field is  : " + val);
					if(val.equals(pass))
					{
						Pass("The Password is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Password is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Password field is not displayed.");	
				}
						
				if(BNBasicfeature.isElementPresent(ccCnfPass))
				{
					/*confirmPass.click();
					//Thread.sleep(500);
	*/				ccCnfPass.sendKeys(cnfpass);
					log.add("The Confirm Password Id field is present.");
					String val = ccCnfPass.getAttribute("value").toString();
					log.add("The Entered Value for the confirm password field is  : " + cnfpass);
					log.add("The Value in the confirm password field is  : " + val);
					if(val.equals(cnfpass))
					{
						Pass("The Confirm Password is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Confirm Password is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Confirm Password field is not displayed.");	
				}
						
				Select drp1 = new Select(ccSecurityQuestion);
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					drp1.selectByIndex(1);
					Pass("The Security Question is successfully selected.");
				}
				else
				{
					Exception("The Security Question field is not displayed.");
				}
						
				if(BNBasicfeature.isElementPresent(ccSecurityAnswer))
				{
					/*securityanswer.click();
					//Thread.sleep(500);
	*/				ccSecurityAnswer.sendKeys(secAns);
					log.add("The Security Answer field is present.");
					String val = ccSecurityAnswer.getAttribute("value").toString();
					log.add("The Entered Value for the Security Answer field is  : " + secAns);
					log.add("The Value in the Security Answer field is  : " + val);
					if(val.equals(secAns))
					{
						Pass("The Security Answer is Successfully entered into the field.",log);
					}
					else
					{
						Fail("The Security Answer is entered into the field but the value mismatches please check.",log);
					}
				}
				else
				{
					Exception("The Security Answer field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 362 Issue ." + e.getMessage());
				Exception(" BRM - 362 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 364 - Verify that user should be able to enter alphabets only in the First and Last Name field*/
	public void namevalidation() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 364 Verify that user should be able to enter alphabets only in the First and Last Name field.");
		if(createAccPage==true)
		{
			String cssval;
			try
			{
				Actions act = new Actions(driver);
				String Colorcomp = BNBasicfeature.getExcelVal("BRM364", sheet, 5);
				String fstName = BNBasicfeature.getExcelVal("BRM364", sheet, 6);
				String lstName = BNBasicfeature.getExcelVal("BRM364", sheet, 7);
				BNBasicfeature.scrollup(ccFName,driver);
				Thread.sleep(500);
				webEle.add(ccFName);
				webEle.add(ccLName);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(signInlnk, driver);
					Thread.sleep(100);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					act.sendKeys(Keys.TAB).build().perform();
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					cssval = ccFName.getCssValue("border-color");
					log.add("The Expected Color was  : " + Colorcomp);
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						log.add("The Color of the First Name text field is : " + BNBasicfeature.colorfinder(cssval));
						Pass("The First Name field is highlighted with the expected Color.",log);
					}
					else
					{
						log.add("The Color of the First Name text field is : " + BNBasicfeature.colorfinder(cssval));
						Fail("The First Name field is not highlighted with the expected Color.",log);
					}
					
					cssval = ccLName.getCssValue("border-color");
					log.add("The Expected Color was  : " + Colorcomp);
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						log.add("The Color of the Last Name text field is : " + BNBasicfeature.colorfinder(cssval));
						Pass("The Last Name field is highlighted with the expected Color.",log);
					}
					else
					{
						log.add("The Color of the Last Name text field is : " + BNBasicfeature.colorfinder(cssval));
						Fail("The Last Name field is not highlighted with the expected Color.",log);
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 364 Issue ." + e.getMessage());
				Exception(" BRM - 364 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 365 - Verify that while entering numbers and special characters in the First Name & Last Name field, then the alert must be displayed */
	public void namevalidationalert() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 365 Verify that while entering numbers and special characters in the First Name & Last Name field, then the alert must be displayed.");
		if(createAccPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM365", sheet, 3);
				String fstName = BNBasicfeature.getExcelVal("BRM365", sheet, 6);
				String lstName = BNBasicfeature.getExcelVal("BRM365", sheet, 7);
				String email = BNBasicfeature.getExcelVal("BRM365", sheet, 8);
				String cnfemail = BNBasicfeature.getExcelVal("BRM365", sheet, 9);
				String pass = BNBasicfeature.getExcelVal("BRM365", sheet, 10);
				String cnfpass = BNBasicfeature.getExcelVal("BRM365", sheet, 11);
				String secAns = BNBasicfeature.getExcelVal("BRM365", sheet, 13);
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(pgetitle, driver);
					Thread.sleep(100);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					BNBasicfeature.scrolldown(emailId,driver);
					ccCnfEmailId.sendKeys(cnfemail);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					Select drp1 = new Select(ccSecurityQuestion);
					if(ccSecurityQuestion.isDisplayed())
					{
						drp1.selectByIndex(1);
					}
					else
					{
						Exception("The Security Question field is not displayed.");
					}
							
					ccSecurityAnswer.sendKeys(secAns);
					jsclick(createAccountCreateBtn);
					Thread.sleep(500);
					BNBasicfeature.scrollup(signInlnk, driver);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						wait.until(ExpectedConditions.visibilityOf(erromsg));
						String errormessage = erromsg.getText();
						log.add("The Expected alert is : " + error);
						log.add("The raised alert message : " + errormessage);
						if(error.equals(errormessage))
						{
							Pass("The raised error message matches the expected value when the there is invalid Username and Password entered in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
					}
					else
					{
						Exception("The Error alert message is not displayed.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 365 Issue ." + e.getMessage());
				Exception(" BRM - 365 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 366 - Verify that user should be allowed to enter the "Email Address" in Email Address field*/
	public void validemailchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 366 Verify that user should be allowed to enter the Email Address in Email Address field.");
		if(createAccPage==true)
		{
			try
			{
				String email = BNBasicfeature.getExcelVal("BRM366", sheet, 8);
				webEle.add(ccEmailId);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(emailId, driver);
					Thread.sleep(100);
					log.add("The Value from the excel file is  : " + email);
					if(BNBasicfeature.isElementPresent(ccEmailId))
					{
						ccEmailId.click();
						Thread.sleep(100);
						emailId.sendKeys(email);
						String currVal  = emailId.getAttribute("value");
						log.add("The Current Value in the email field is : " + currVal);
						if(currVal.equals(email))
						{
							Pass("The Email id field is present and the email id is Successfully entered.",log);
						}
						else
						{
							Fail("There is some mismatch in the entered data.",log);
						}
					}
					else
					{
						Fail("The Email id field is not present.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 366 Issue ." + e.getMessage());
				Exception(" BRM - 366 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 367 - Verify that while entering the invalid "Email Address" in the Email Address field,then the alert must be displayed */
	public void invalidemailchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 367 Verify that while entering the invalid Email Address in the Email Address field,then the alert must be displayed.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					String error = BNBasicfeature.getExcelVal("BRM367", sheet, 3);
					String fstName = BNBasicfeature.getExcelVal("BRM367", sheet, 6);
					String lstName = BNBasicfeature.getExcelVal("BRM367", sheet, 7); 
					String email = BNBasicfeature.getExcelVal("BRM367", sheet, 8);
					String cnfemail = BNBasicfeature.getExcelVal("BRM367", sheet, 9);
					String pass = BNBasicfeature.getExcelVal("BRM367", sheet, 10);
					String cnfpass = BNBasicfeature.getExcelVal("BRM367", sheet, 11);
					String secAns = BNBasicfeature.getExcelVal("BRM367", sheet, 13);
					//BNBasicfeature.scrollup(fName,driver);	
					Thread.sleep(500);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					BNBasicfeature.scrolldown(ccEmailId, driver);
					Thread.sleep(500);
					ccCnfEmailId.sendKeys(cnfemail);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					Select drp1 = new Select(ccSecurityQuestion);
					if(ccSecurityQuestion.isDisplayed())
					{
						drp1.selectByIndex(1);
					}
					else
					{
						Exception("The Security Question field is not displayed.");
					}
					ccSecurityAnswer.sendKeys(secAns);
					jsclick(createAccountCreateBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
					BNBasicfeature.scrollup(signInlnk, driver);
					BNBasicfeature.scrollup(pgetitle, driver);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						wait.until(ExpectedConditions.visibilityOf(erromsg));
						BNBasicfeature.scrollup(erromsg, driver);
						String errormessage = erromsg.getText();
						log.add("The raised alert was : " + erromsg.getText());
						log.add("The expected alert was : " + error);
						if(error.equals(errormessage))
						{
							Pass("The raised error message matches the expected value when the there is invalid Email Id entered in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
					}
					else
					{
						Exception("The Error alert message is not displayed.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 367 Issue ." + e.getMessage());
				Exception(" BRM - 367 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 368 - Verify that user should be able to enter the "Email Address" in the Re-Enter Email Address field*/
	public void validcnfmailchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 368 Verify that user should be able to enter the Email Address in the Re-Enter Email Address field.");
		if(createAccPage==true)
		{
			try
			{
				Thread.sleep(100);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					String email = BNBasicfeature.getExcelVal("BRM368", sheet, 8);
					String cnfemail = BNBasicfeature.getExcelVal("BRM368", sheet, 9);
					if(BNBasicfeature.isElementPresent(ccEmailId))
					{
						ccEmailId.sendKeys(email);
						Thread.sleep(100);
						String currVal = emailId.getAttribute("value");
						log.add("The entered value in the email field is : " + email);
						log.add("The Current value in the email field is : " + currVal);
						if(currVal.equals(email))
						{
							Pass("The Email id field is present and the email id is Successfully entered.",log);
						}
						else
						{
							Fail("There is some mismatch please check.",log);
						}
					}
					else
					{
						Fail("The Email id field is not present.");
					}
				
					if(BNBasicfeature.isElementPresent(ccCnfEmailId))
					{
						ccCnfEmailId.sendKeys(cnfemail);
						Thread.sleep(100);
						String currVal = ccCnfEmailId.getAttribute("value");
						log.add("The entered value in the confirm email field is : " + cnfemail);
						log.add("The Current value in the confirm field is : " + currVal);
						if(currVal.equals(cnfemail))
						{
							Pass("The Confirm Email id field is present and the email id is Successfully entered.",log);
						}
						else
						{
							Fail("There is some mismatch please check.",log);
						}
					}
					else
					{
						Fail("The Confirm Email id field is not present.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 368 Issue ." + e.getMessage());
				Exception(" BRM - 368 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 466 - Verify that while entering the invalid characters in the text field, the field should be highlighted in red*/
	public void invaliddatacoloralert() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 466 Verify that while entering the invalid characters in the text field, the field should be highlighted in red.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				String cssval;
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					String Colorcomp = BNBasicfeature.getExcelVal("BRM466", sheet, 5);
					String fstName = BNBasicfeature.getExcelVal("BRM466", sheet, 6);
					String lstName = BNBasicfeature.getExcelVal("BRM466", sheet, 7);
					String email = BNBasicfeature.getExcelVal("BRM466", sheet, 8);
					String cnfemail = BNBasicfeature.getExcelVal("BRM466", sheet, 9);
					String pass = BNBasicfeature.getExcelVal("BRM466", sheet, 10);
					String cnfpass = BNBasicfeature.getExcelVal("BRM466", sheet, 11);
					String secAns = BNBasicfeature.getExcelVal("BRM466", sheet, 13);
					//BNBasicfeature.scrollup(fName, driver);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					ccCnfEmailId.sendKeys(cnfemail);
					BNBasicfeature.scrollup(ccPassword, driver);
					Thread.sleep(100);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					ccSecurityAnswer.sendKeys(secAns);
					Thread.sleep(400);
					BNBasicfeature.scrollup(pgetitle, driver);
					Thread.sleep(100);
					cssval = ccFName.getCssValue("border-color");
					log.add("The expected color was : " + Colorcomp);
					log.add("The actual color in first name field is : " + BNBasicfeature.colorfinder(cssval));
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						Pass("The First Name field is highlighted with the expected Color.",log);
					}
					else
					{
						Fail("The First Name field is not highlighted with the expected Color.",log);
					}
					
					cssval = ccLName.getCssValue("border-color");
					log.add("The expected color was : " + Colorcomp);
					log.add("The actual color of last name field is : " + BNBasicfeature.colorfinder(cssval));
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						Pass("The Last Name field is highlighted with the expected Color.",log);
					}
					else
					{
						Fail("The Last Name field is not highlighted with the expected Color.",log);
					}
					
					cssval = ccEmailId.getCssValue("border-color");
					log.add("The expected color was : " + Colorcomp);
					log.add("The actual color of email field is : " + BNBasicfeature.colorfinder(cssval));
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						Pass("The Email field is highlighted with the expected Color.",log);
					}
					else
					{
						Fail("The Email field is not highlighted with the expected Color.",log);
					}
					
					cssval = ccCnfEmailId.getCssValue("border-color");
					log.add("The expected color was : " + Colorcomp);
					log.add("The actual color of confirm email field is : " + BNBasicfeature.colorfinder(cssval));
					if(BNBasicfeature.colorfinder(cssval).equals(Colorcomp))
					{
						Pass("The Confirm Email field is highlighted with the expected Color.",log);
					}
					else
					{
						Fail("The Confirm Email field is not highlighted with the expected Color.",log);
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 466 Issue ." + e.getMessage());
				Exception(" BRM - 466 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 369 - Verify that while entering the different "Email Address" in the Email Address & Re-Enter Email Address field, then the alert should be displayed*/
	public void diffemailchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 369 Verify that while entering the different Email Address in the Email Address & Re-Enter Email Address field, then the alert should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					String error = BNBasicfeature.getExcelVal("BRM369", sheet, 3);
					String fstName = BNBasicfeature.getExcelVal("BRM369", sheet, 6);
					String lstName = BNBasicfeature.getExcelVal("BRM369", sheet, 7);
					String email = BNBasicfeature.getExcelVal("BRM369", sheet, 8);
					String cnfemail = BNBasicfeature.getExcelVal("BRM369", sheet, 9);
					String pass = BNBasicfeature.getExcelVal("BRM369", sheet, 10);
					String cnfpass = BNBasicfeature.getExcelVal("BRM369", sheet, 10);
					String secAns = BNBasicfeature.getExcelVal("BRM369", sheet, 13);
					Thread.sleep(100);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					ccCnfEmailId.sendKeys(cnfemail);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					Select drp1 = new Select(ccSecurityQuestion);
					if(ccSecurityQuestion.isDisplayed())
					{
						drp1.selectByIndex(1);
					}
					else
					{
						Exception("The Security Question field is not displayed.");
					}
							
					ccSecurityAnswer.sendKeys(secAns);
					jsclick(createAccountCreateBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
					BNBasicfeature.scrollup(pgetitle, driver);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						wait.until(ExpectedConditions.visibilityOf(erromsg));
						Thread.sleep(100);
						String errormessage = erromsg.getText();
						log.add("The expected alert was : " + error);
						log.add("The actual alert is : " + errormessage);
						if(errormessage.equals(error))
						{
							Pass("The raised error message matches the expected value when the there is different Email Id entered in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
					}
					else
					{
						Exception("The Error alert message is not displayed.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 369 Issue ." + e.getMessage());
				Exception(" BRM - 369 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 371 - Verify that user should be able to enter alphabets, numbers & special characters in the Password and Confirm Password fields*/
	public void passwordfieldschk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 371 Verify that user should be able to enter alphabets, numbers & special characters in the Password and Confirm Password fields.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					String pass = BNBasicfeature.getExcelVal("BRM371", sheet, 10);
					String cnfpass = BNBasicfeature.getExcelVal("BRM371", sheet, 11); 
							
					if(BNBasicfeature.isElementPresent(ccPassword))
					{
						log.add("The Password field is present.");
						ccPassword.sendKeys(pass);
						log.add("The Entered value in the Password field was : " + pass);
						String currVal = password.getAttribute("value");
						log.add("The current value in the Password field was : " + currVal);
						if(currVal.equals(pass))
						{
							Pass("The Password field is present and the Password is Successfully entered.",log);
						}
						else
						{
							Fail("The Password field is present and the Password is not Successfully entered.");
						}
					}
					else
					{
						Fail("The Password field is not present. Please Check.");
					}
					
					if(BNBasicfeature.isElementPresent(ccCnfPass))
					{
						log.add("The Confirm Password field is present.");
						BNBasicfeature.scrollup(ccPassword, driver);
						ccCnfPass.sendKeys(cnfpass);
						log.add("The Entered value in the Confirm Password field was : " + pass);
						String currVal = password.getAttribute("value");
						log.add("The current value in the Confirm Password field was : " + currVal);
						if(currVal.equals(pass))
						{
							Pass("The Password field is present and the Confirm Password is Successfully entered.",log);
						}
						else
						{
							Fail("The Password field is present and the Confirm Password is not Successfully entered.");
						}
					}
					else
					{
						Fail("The Confirm Password field is not present. Please Check.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 371 Issue ." + e.getMessage());
				Exception(" BRM - 371 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 372 - Verify that while entering less than 4 characters in the Password and Confirm Password fields, then the alert should be displayed.*/
	public void passlengthchk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM  - 372 Verify that while entering less than 4 characters in the Password and Confirm Password fields, then the alert should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					String error = BNBasicfeature.getExcelVal("BRM372", sheet, 3); 
					String fstName = BNBasicfeature.getExcelVal("BRM372", sheet, 6);
					String lstName = BNBasicfeature.getExcelVal("BRM372", sheet, 7);
					String email = BNBasicfeature.getExcelVal("BRM372", sheet, 8);
					String cnfemail = BNBasicfeature.getExcelVal("BRM372", sheet, 9);
					String pass = BNBasicfeature.getExcelVal("BRM372", sheet, 10);
					String cnfpass = BNBasicfeature.getExcelVal("BRM372", sheet, 11);
					String secAns = BNBasicfeature.getExcelVal("BRM372", sheet, 13);
					BNBasicfeature.scrollup(signInlnk, driver);
					Thread.sleep(100);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					ccCnfEmailId.sendKeys(cnfemail);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					Select drp1 = new Select(ccSecurityQuestion);
					if(ccSecurityQuestion.isDisplayed())
					{
						drp1.selectByIndex(1);
					}
					else
					{
						Exception("The Security Question field is not displayed.");
					}
					
					ccSecurityAnswer.sendKeys(secAns);
					jsclick(createAccountCreateBtn);
					Thread.sleep(500);
					BNBasicfeature.scrolldown(signInlnk, driver);
					wait.until(ExpectedConditions.visibilityOf(erromsg));
					String errormessage = erromsg.getText();
					log.add("The Expected alert was : " + error);
					log.add("The actual alert was : " + errormessage);
					if(error.contains(errormessage))
					{
						Pass("The raised error message matches the expected value when the password character is entered less than the required in the Create Account fields.",log);
					}
					else
					{
						Fail("The raised error message does not matches with the expected alert message.",log);
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 372 Issue ." + e.getMessage());
				Exception(" BRM - 372 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 373 - Verify that while selecting the "Create Account" button without entering the Password and Confirm Password,then the alert message should be displayed */
	public void blankpasschk() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 373 Verify that while selecting the Create Account button without entering the Password and Confirm Password,then the alert message should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(100);
					String error =  BNBasicfeature.getExcelVal("BRM373", sheet, 3);
					ccPassword.click();
					jsclick(createAccountCreateBtn);
					Thread.sleep(400);
					wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
					BNBasicfeature.scrollup(pgetitle, driver);
					wait.until(ExpectedConditions.visibilityOf(erromsg));
					Thread.sleep(100);
					String errormessage = erromsg.getText();
					log.add("The Expected alert was : " + error);
					log.add("The Actual alert is  : " + errormessage);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						if(error.equals(errormessage))
						{
							Pass("The raised error message matches the expected value when the password character is entered less than the required in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
					}
					else
					{
						Fail("The error message is not displayed.");
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 373 Issue ." + e.getMessage());
				Exception(" BRM - 373 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 374 - Verify that while selecting the "Create Account" button on entering the Password and without entering Confirm Password,the alert message should be displayed */
	public void noconfirmpasschk() 
	{
		ChildCreation(" BRM - 374 Verify that while selecting the Create Account button on entering the Password and without entering Confirm Password,the alert message should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM374", sheet, 3);
				String pass = BNBasicfeature.getExcelVal("BRM374", sheet, 10); 
				BNBasicfeature.scrollup(signInlnk, driver);
				Thread.sleep(100);
				ccPassword.sendKeys(pass);
				jsclick(createAccountCreateBtn);
				Thread.sleep(500);
				wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
				BNBasicfeature.scrollup(signInlnk, driver);
				wait.until(ExpectedConditions.visibilityOf(erromsg));
				String errormessage = erromsg.getText();
				
				if(BNBasicfeature.isElementPresent(erromsg))
				{
					if(error.equals(errormessage))
					{
						Pass("The raised error message matches the expected value when the confirm password is not entered and the Create Account button is pressed in the Create Account page.");
					}
					else
					{
						Fail("The raised error message does not matches with the expected alert message.");
					}
				}
				else
				{
					Fail("The alert is not raised.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 374 Issue ." + e.getMessage());
				Exception(" BRM - 374 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 375 -  Verify that while selecting the "Create Account" after entering different Password in Password and Confirm Password field,the alert message should be displayed */
	public void passmismatch()
	{
		ChildCreation(" BRM - 375 Verify that while selecting the Create Account after entering different Password in Password and Confirm Password field,the alert message should be displayed .");
		if(createAccPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM375", sheet, 3);
				String pass = BNBasicfeature.getExcelVal("BRM375", sheet, 10); 
				String cnfpass = BNBasicfeature.getExcelVal("BRM375", sheet, 11); 
				BNBasicfeature.scrolldown(ccCnfPass, driver);
				Thread.sleep(100);
				ccPassword.clear();
				ccCnfPass.clear();
				ccPassword.sendKeys(pass);
				ccCnfPass.sendKeys(cnfpass);
				//BNBasicfeature.scrolldown(createAccPageCreateBtn, driver);
				//Thread.sleep(1000);
				jsclick(createAccountCreateBtn);
				Thread.sleep(100);
				wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
				BNBasicfeature.scrollup(signInlnk, driver);
				wait.until(ExpectedConditions.visibilityOf(erromsg));
				Thread.sleep(100);
				String errormessage = erromsg.getText();
				log.add("The Expected alert was : " + error);
				log.add("The Actual alert is : " + errormessage);
				if(BNBasicfeature.isElementPresent(erromsg))
				{
					if(error.equals(errormessage))
					{
						Pass("The raised error message matches the expected value when the Password mismatches with the Confirm Password in the Create Account fields.",log);
					}
					else
					{
						Fail("The raised error message does not matches with the expected alert message.",log);
					}
				}
				else
				{
					Fail( "The alert is not raised.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 375 Issue ." + e.getMessage());
				Exception(" BRM - 375 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 376 - Verify that while entering same Password in Password and Confirm Password Field,then green colour tick mark should be displayed in right side of the text box as per the creative */
	public void samepasswordOkval() throws IOException, InterruptedException
	{
		ChildCreation(" BRM - 376 Verify that while entering same Password in Password and Confirm Password Field,then green colour tick mark should be displayed in right side of the text box as per the creative.");
		if(createAccPage==true)
		{
			try
			{
				String pass = BNBasicfeature.getExcelVal("BRM376", sheet, 10);
				String cnfpass = BNBasicfeature.getExcelVal("BRM376", sheet, 11);
				ccPassword.clear();
				ccCnfPass.clear();
				ccPassword.sendKeys(pass);
				ccCnfPass.sendKeys(cnfpass);
				Actions act = new Actions(driver);
				act.sendKeys(Keys.TAB).build().perform();
				Thread.sleep(1000);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView(true);",ccCnfPass);
			}
			catch (Exception e)
			{
				Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
			}
			try
			{
				boolean passtick = ccPassword.getCssValue("background-image").contains("tick");
				boolean cnfpasstick = ccCnfPass.getCssValue("background-image").contains("tick");
				if((passtick==true) && (cnfpasstick==true))
				{
					Pass("The tick mark image is displayed.");
				}
				else
				{
					Pass("The tick mark image is displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 376 Issue ." + e.getMessage());
				Exception(" BRM - 376 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 377 - Verify that user should be able to select any values from the "Security Question and Answer" drop down field*/
	public void dropdownselection()
	{
		ChildCreation(" BRM - 377 Verify that user should be able to select any values from the Security Question and Answer drop down field.");
		if(createAccPage==true)
		{
			try
			{
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(500);
					String[] secQues = BNBasicfeature.getExcelVal("BRM377", sheet, 12).split("\n");
					for(int i = 0; i<secQues.length; i++)
					{
						BNBasicfeature.scrolldown(ccCnfPass, driver);
						Thread.sleep(100);
						Select drp1 = new Select(ccSecurityQuestion);
						if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
						{
							drp1.selectByValue(secQues[i]);
							Pass("The Security Question is Successfully Selected.");
							Thread.sleep(100);
						}
						else
						{
							Exception("The Security Question field is not displayed.");
						}
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 377 Issue ." + e.getMessage());
				Exception(" BRM - 377 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	 /* 378 - Verify that while selecting a value from "Security Question and Answer" drop down field and the user does not enter any values in the "Security Answer" field,the alert message should be displayed*/
	public void securityAnsVal()
	{
		ChildCreation(" BRM - 378 Verify that while selecting a value from Security Question and Answer drop down field and the user does not enter any values in the Security Answer field,the alert message should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM378", sheet, 3);
				String fstName = BNBasicfeature.getExcelVal("BRM378", sheet, 6);
				String lstName = BNBasicfeature.getExcelVal("BRM378", sheet, 7);
				String emal = BNBasicfeature.getExcelVal("BRM378", sheet, 8);
				String cnfemail = BNBasicfeature.getExcelVal("BRM378", sheet, 9);
				String pass = BNBasicfeature.getExcelVal("BRM378", sheet, 10);
				String cnfpass = BNBasicfeature.getExcelVal("BRM378", sheet, 11);
				String secQues = BNBasicfeature.getExcelVal("BRM378", sheet, 12);
						
				BNBasicfeature.scrollup(signInlnk, driver);
				ccFName.sendKeys(fstName);
				ccLName.sendKeys(lstName);
				ccEmailId.sendKeys(emal);
				ccCnfEmailId.sendKeys(cnfemail);
				ccPassword.sendKeys(pass);
				ccCnfPass.sendKeys(cnfpass);
				Select drp1 = new Select(ccSecurityQuestion);
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					drp1.selectByValue(secQues);
					Pass("The Security Question is Successfully Selected.");
					Thread.sleep(1500);
				}
				else
				{
					Exception("The Security Question field is not displayed.");
				}
				jsclick(createAccountCreateBtn);
				Thread.sleep(100);
				wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
				BNBasicfeature.scrollup(signInlnk, driver);
				Thread.sleep(400);
				wait.until(ExpectedConditions.visibilityOf(erromsg));
				String errormessage = erromsg.getText();
				log.add( "The expected alert was : " + error);
				log.add( " The Actual alert was : " + errormessage);
				if(BNBasicfeature.isElementPresent(erromsg))
				{
					if(error.equals(errormessage))
					{
						Pass("The raised error message matches the expected value when the Security Answer is not entered in the Create Account fields.",log);
					}
					else
					{
						Fail("The raised error message does not matches with the expected alert message.",log);
					}
				}
				else
				{
					Fail("The alert is not raised.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 378 Issue ." + e.getMessage());
				Exception(" BRM - 378 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 379 - Verify that while selecting the "Cancel" button in the Create Account overlay,the previous page should be displayed */
	public void cancelbtnnavigation()
	{
		ChildCreation(" BRM - 379 Verify that while selecting the Cancel button in the Create Account overlay,the previous page should be displayed.");
		if(createAccPage==true)
		{
			try
			{
				String pagetitle = BNBasicfeature.getExcelVal("BRM379", sheet, 2);
				jsclick(createAccountCancelBtn);
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				Thread.sleep(1000);
				if(pgetitle.getText().equals(pagetitle))
				{
					Pass("The user is navigated and they are in the Sign In Page.");
					createAccPage = false;
				}
				else
				{
					Fail("The user is not navigated to the Sign In Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 379 Issue ." + e.getMessage());
				Exception(" BRM - 379 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 382 - Verify that while selecting the "Create Account" button after entering the email address which is already created, then the alert must be displayed*/
	public void existingEmail() throws Exception
	{
		ChildCreation(" BRM - 382 Verify that while selecting the Create Account button after entering the email address which is already created, then the alert must be displayed.");
		try
		{
			String error = BNBasicfeature.getExcelVal("BRM382", sheet, 3);
			String fstName = BNBasicfeature.getExcelVal("BRM382", sheet, 6);
			String lstName = BNBasicfeature.getExcelVal("BRM382", sheet, 7);
			String email = BNBasicfeature.getExcelVal("BRM382", sheet, 8);
			String cnfemail = BNBasicfeature.getExcelVal("BRM382", sheet, 9);
			String pass = BNBasicfeature.getExcelVal("BRM382", sheet, 10);
			String cnfpass = BNBasicfeature.getExcelVal("BRM382", sheet, 11);
			String secAns = BNBasicfeature.getExcelVal("BRM382", sheet, 13);
			
			createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
			if(createAccPage==true)
			{
				Thread.sleep(1000);
				ccFName.sendKeys(fstName);
				ccLName.sendKeys(lstName);
				ccEmailId.sendKeys(email);
				ccCnfEmailId.sendKeys(cnfemail);
				ccPassword.sendKeys(pass);
				ccCnfPass.sendKeys(cnfpass);
				Select drp1 = new Select(ccSecurityQuestion);
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					drp1.selectByIndex(1);
				}
				else
				{
					Exception("The Security Question field is not displayed.");
				}
				
				ccSecurityAnswer.sendKeys(secAns);
				jsclick(createAccountCreateBtn);
				wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(erromsg, By.xpath("//*[@class]")));
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(erromsg));
				Thread.sleep(500);
				String errormessage = erromsg.getText();
				log.add( "The expected alert was : " + error);
				log.add( " The Actual alert was : " + errormessage);
				if(error.concat(email).equals(errormessage))
				{
					Pass("The raised error message matches the expected value when the exising account Email Id is used in the Create Account fields.",log);
				}
				else
				{
					Fail("The raised error message does not matches with the expected alert message.",log);
				}
			}
			else
			{
				Fail( "The User failed to load the create account page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 382 Issue ." + e.getMessage());
			Exception(" BRM - 382 Issue ." + e.getMessage());
		}
	}
	
	/* 462 - Verify whether the input fields are accepting the maximum number of characters*/
	public void maxcharval() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 462 Verify whether the input fields are accepting the maximum number of characters.");
		if(createAccPage==true)
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{	
				String tcid = "BRM462";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					String fstName = sheet.getRow(i).getCell(6).getStringCellValue().toString(); 
					String lstName = sheet.getRow(i).getCell(7).getStringCellValue().toString(); 
					String email = sheet.getRow(i).getCell(8).getStringCellValue().toString(); 
					String cnfemail = sheet.getRow(i).getCell(9).getStringCellValue().toString();
					String pass = sheet.getRow(i).getCell(10).getStringCellValue().toString(); 
					String cnfpass = sheet.getRow(i).getCell(11).getStringCellValue().toString(); 
					String secAns = sheet.getRow(i).getCell(13).getStringCellValue().toString(); 
					try
					{
						BNBasicfeature.scrollup(pgetitle, driver);
						Thread.sleep(1000);
						webEle.add(ccFName);
						webEle.add(ccLName);
						webEle.add(ccEmailId);
						webEle.add(ccCnfEmailId);
						webEle.add(ccPassword);
						webEle.add(ccCnfPass);
						webEle.add(ccSecurityAnswer);
						ccclearAll(webEle);
						if(fieldClear==true)
						{
							BNBasicfeature.scrollup(signInlnk, driver);
							Thread.sleep(100);
							ccFName.sendKeys(fstName);
							String txtinfirstname = ccFName.getAttribute("value").toString();
							if(txtinfirstname.length() <= 40)
							{
								Pass("The First Name field accepts character less than 40. The value from the Excel was " + fstName  + " and the value currently in the First Name field is " + txtinfirstname + " and it is less than 40 Characters.");
							}
							else
							{
								Fail("The First Name field accepts character more than 40. The value from the Excel was " + fstName  + " and the value currently in the First Name field is " + txtinfirstname + " and it is more than 40 Characters.");
							}
									
							ccLName.sendKeys(lstName);
							String txtinlastname = ccLName.getAttribute("value").toString();
							if(txtinlastname.length() <= 40)
							{
								Pass("The Last Name field accepts character less than 40. The value from the Excel was " + lstName  + " and the value currently in the Last Name field is " + txtinlastname + " and it is less than 40 Characters.");
							}
							else
							{
								Fail("The Last Name field accepts character more than 40. The value from the Excel was " + lstName  + " and the value currently in the Last Name field is " + txtinlastname + " and it is more than 40 Characters.");
							}
									
							ccEmailId.sendKeys(email);
							String txtinemail = ccEmailId.getAttribute("value").toString();
							if(txtinemail.length() <= 40)
							{
								Pass("The Email field accepts character less than 40. The value from the Excel was " + email  + " and the value currently in the Email field is " + txtinemail+ " and it is less than 40 Characters.");
							}
							else
							{
								Fail("The Email field accepts character more than 40. The value from the Excel was " + email  + " and the value currently in the Email field is " + txtinemail + " and it is more than 40 Characters.");
							}
									
							ccCnfEmailId.sendKeys(cnfemail);
							String txtincnfemail = ccCnfEmailId.getAttribute("value").toString();
							if(txtincnfemail.length() <= 40)
							{
								Pass("The Confirm Email field accepts character less than 40. The value from the Excel was " + cnfemail  + " and the value currently in the Confirm Email field is " + txtincnfemail + " and it is less than 40 Characters.");
							}
							else
							{
								Fail("The Confirm Email field accepts character more than 40. The value from the Excel was " + cnfemail  + " and the value currently in the Confirm Email field is " + txtincnfemail + " and it is more than 40 Characters.");
							}
									
							ccPassword.sendKeys(pass);
							String txtinpass = ccPassword.getAttribute("value").toString();
							if(txtinpass.length() <= 15)
							{
								Pass("The Password field accepts character less than 15. The value from the Excel was " + pass  + " and the value currently in the Password field is " + txtinpass + " and it is less than 15 Characters.");
							}
							else
							{
								Fail("The Password field accepts character more than 15. The value from the Excel was " + pass  + " and the value currently in the Password field is " + txtinpass + " and it is more than 15 Characters.");
							}
									
							ccCnfPass.sendKeys(cnfpass);
							String txtincnfpass = ccCnfPass.getAttribute("value").toString();
							if(txtincnfpass.length() <= 15)
							{
								Pass("The Confirm Password field accepts character less than 15. The value from the Excel was " + cnfpass  + " and the value currently in the Confirm Password field is " + txtincnfpass + " and it is less than 15 Characters.");
							}
							else
							{
								Fail("The Confirm Password field accepts character more than 15. The value from the Excel was " + cnfpass  + " and the value currently in the Confirm Password field is " + txtincnfpass + " and it is more than 15 Characters.");
							}
									
							ccSecurityAnswer.sendKeys(secAns);
							String txtinsecAns = ccSecurityAnswer.getAttribute("value").toString();
							if(txtincnfpass.length() <= 15)
							{
								Pass("The Security Answer field accepts character less than 15. The value from the Excel was " + secAns  + " and the value currently in the Security Answer field is " + txtinsecAns + " and it is less than 15 Characters.");
							}
							else
							{
								Fail("The Security Answer field accepts character more than 15. The value from the Excel was " + secAns  + " and the value currently in the Security Answer field is " + txtinsecAns + " and it is more than 15 Characters.");
							}
						}
						else
						{
							Skip("User failed to clear the fields.");
						}
					}
					catch(Exception e)
					{
						System.out.println(" BRM - 462 Issue ." + e.getMessage());
						Exception(" BRM - 462 Issue ." + e.getMessage());
					}
				}
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 616 - Verify that while selecting the down arrow in the security question option, the dropdown should be enabled */
	public void secquesdropdown() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 616 Verify that while selecting the down arrow in the security question option, the dropdown should be enabled.");
		if(createAccPage==true)
		{
			try
			{
				//clearAll();
				//Thread.sleep(1000);
				BNBasicfeature.scrolldown(ccSecurityQuestion, driver);
				Thread.sleep(100);
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					Pass("The Drop down box is visible in the Create Account Page.");
					try
					{
						act.moveToElement(ccSecurityQuestion).click().build().perform();
						//Thread.sleep(500);
						Pass("The drop down box is clicked and the options are listed.");
					}
					catch (Exception e) 
					{
						Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
					}
					act.moveToElement(ccSecurityQuestion).click().build().perform();
				}
				else
				{
					Fail("The Drop down box is not visible in the Create Account Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 462 Issue ." + e.getMessage());
				Exception(" BRM - 462 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 381 - Verify that while selecting the "Create Account" after entering all the details,the new account should be created*/
	public void createNewAccount() throws Exception
	{
		ChildCreation(" BRM - 381 Verify that while selecting the Create Account after entering all the details,the new account should be created.");
		if(createAccPage==true)
		{
			try
			{
				String fstName = BNBasicfeature.getExcelVal("BRM381", sheet, 6); 
				String lstName = BNBasicfeature.getExcelVal("BRM381", sheet, 7); 
				String email = BNBasicfeature.getExcelVal("BRM381", sheet, 8);
				String cnfemail = BNBasicfeature.getExcelVal("BRM381", sheet, 9);
				String pass = BNBasicfeature.getExcelVal("BRM381", sheet, 10);
				String cnfpass = BNBasicfeature.getExcelVal("BRM381", sheet, 11);
				String secAns = BNBasicfeature.getExcelVal("BRM381", sheet, 13);
				String accname = BNBasicfeature.getExcelVal("BRM381", sheet, 6);
				
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					BNBasicfeature.scrollup(signInlnk, driver);
					Thread.sleep(100);
				
					BNBasicfeature.scrollup(ccFName, driver);
					Thread.sleep(100);
					ccFName.sendKeys(fstName);
					ccLName.sendKeys(lstName);
					ccEmailId.sendKeys(email);
					ccCnfEmailId.sendKeys(cnfemail);
					ccPassword.sendKeys(pass);
					ccCnfPass.sendKeys(cnfpass);
					Select drp1 = new Select(ccSecurityQuestion);
					if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
					{
						drp1.selectByIndex(1);
					}
					else
					{
						Exception("The Security Question field is not displayed.");
					}
							
					ccSecurityAnswer.sendKeys(secAns);
					jsclick(createAccountCreateBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.or
							(
								ExpectedConditions.visibilityOf(erromsg),
								ExpectedConditions.visibilityOf(header)
							)
					);
					
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						Fail("The user is not registered successfully. Please Check.");
						accntCreated = false;
					}
					else
					{
						accntCreated = true;
						wait.until(ExpectedConditions.visibilityOf(menu));
						jsclick(menu);
						if(loggedInAccntName.getText().contains(accname))
						{
							/*System.out.println(accname.concat(fstName).equals(accntName.getText()));
							System.out.println(accntName.getText());
							System.out.println("Pass");*/
							Pass("The User is successfully registered in the website and they are already logged in.");
						}
						else
						{
							Fail("The User is successfully registered in the website and they are already logged in.");
						}
					}
				}
				else
				{
					Skip("User failed to clear the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 381 Issue ." + e.getMessage());
				Exception(" BRM - 381 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* 383 - Verify that welcome message must be displayed with the first name once the new account gets created */
	public void createAccwelcomeMsg() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 383 Verify that welcome message must be displayed with the first name once the new account gets created.");
		if(accntCreated ==true)
		{
			try
			{
				String fstName = BNBasicfeature.getExcelVal("BRM381", sheet, 6); 
				if(BNBasicfeature.isElementPresent(loggedInAccntName))
				{
					/*System.out.println(accntName.getText().equals(welcomemsg.concat(fstName)));
					System.out.println(welcomemsg.length());
					System.out.println(accntName.getText().length());
					System.out.println(fstName.length());*/
					if(loggedInAccntName.getText().contains(fstName))
					{
						Pass("The user is successfully registered and the User First Name is displayed. The displayed welcome message and name is " + loggedInAccntName.getText().toString());
					}
					else
					{
						Fail("The user is successfully registered and the User First Name is displayed. The displayed welcome message and name is " + loggedInAccntName.getText().toString());
					}
				}
				else
				{
					Fail("The Account Name canot be found."); 
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 383 Issue ." + e.getMessage());
				Exception(" BRM - 383 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The User failed to create the new Account.");
		}
	}
	
	/* 384 - Verify that after logging in using the registered user account,the "Sign Out" button should be displayed in the "My Account" dropdown*/
	public void signOut() throws InterruptedException, IOException
	{
		ChildCreation(" BRM - 384 Verify that after logging in using the registered user account,the Sign Out button should be displayed in the My Account dropdown.");
		if(accntCreated == true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(loggedInAccntName));
				loggedInAccntName.click();
				if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
				{
					//log.add("The Signout button is displayed.");
					try
					{
						signOut.click();
					}
					catch (Exception e)
					{
						signOutTemp.click();
					}
					wait.until(ExpectedConditions.or(
							ExpectedConditions.visibilityOf(signOutIndicator),
							ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign In")
							)
						);
					wait.until(ExpectedConditions.elementToBeClickable(menu));
					
					if(BNBasicfeature.isElementPresent(header))
					{
						Pass("The SignOut button is not present after the user logged in.");
					}
					else
					{
						Fail("The SignOut button is present after the user logged in.");
					}
				}
				else
				{
					Fail( "The Sign Out Button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 384 Issue ." + e.getMessage());
				Exception(" BRM - 384 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The User failed to create the new Account.");
		}
	}
	
}
