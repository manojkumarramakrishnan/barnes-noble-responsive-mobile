package bnPageObjects;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNChecklist_RegCheckout;
import bnConfig.BNConstants;
import bnConfig.BNXpaths;

public class BNRegChkListObj extends BNChecklist_RegCheckout implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static int gftsel;
	static boolean signedIn = false, ccAdded = false, chkPageLoaded = false;
	static String loginpass = "", loginusername = "", ccSuccess = "", actColor = "",  suggeAddreDet = "", setcardHolName = "", setAddress = "", savedShipAddress = "", savedCCHolName = "", savedCCNum = "";
	static WebElement useThiBtn = null, suggeAddressElement = null, savedAddressUseThisBtn = null, savedAddressEditBtn = null, savedAddressDetailsElement = null, savedAddressElement = null, savedCCEditBtn = null, savedCCUseThisBtn = null;
	static List<WebElement> shipOrbillCurrAddress = null;
	static WebElement giftChkBox = null, giftIndicator = null, giftMessageEditbutton = null; 
	static boolean chkBoxOk = false, gftOk = false, gftEdit = false;
	
	public BNRegChkListObj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		//this.parent = parent;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+myAccountt+"")
	WebElement myAccount;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;
	
	@FindBy(xpath = ""+signInn+"")
	WebElement signIn;
	
	@FindBy(xpath = ""+signOutt+"")
	WebElement signOut;
	
	@FindBy(xpath = ""+signOutTempp+"")
	WebElement signOutTemp;
	
	@FindBy(xpath = ""+signOutIndicatorr+"")
	WebElement signOutIndicator;
	
	@FindBy(xpath = ""+promoCarousell+"")
	WebElement promoCarousel;
	
	@FindBy(xpath = ""+signInpageframee+"") 
	WebElement signInpageframe;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+emailIdd+"")
	WebElement emailId;
	
	@FindBy(xpath = ""+passwordd+"")
	WebElement password;
	
	@FindBy(xpath = ""+secureSignInn+"")
	WebElement secureSignIn;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+panCakeMenuMaskk+"")
	WebElement panCakeMenuMask;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+miniCartLoadingGaugee+"")
	WebElement miniCartLoadingGauge;
	
	@FindBy(xpath = ""+miniCartShoppingBagMaskIdd+"")
	WebElement miniCartShoppingBagMaskId;
	
	@FindBy(xpath = ""+checkoutTopSubmitOrderr+"")
	WebElement checkoutTopSubmitOrder;
	
	@FindBy(xpath=""+mcContinueShoppingBtnn+"")
	WebElement mcContinueShoppingBtn;
	
	@FindBy(xpath = ""+footerSignInn+"")
	WebElement footerSignIn;
	
	@FindBy(xpath = ""+checkoutOrderTopTotall+"")
	WebElement checkoutOrderTopTotal;
	
	@FindBy(xpath = ""+checkoutMembershipContainerTitlee+"")
	WebElement checkoutMembershipContainerTitle;
	
	@FindBy(xpath = ""+checkoutShipEditBtnn+"")
	WebElement checkoutShipEditBtn;
	
	@FindBy(xpath = ""+checkoutBillEditBtnn+"")
	WebElement checkoutBillEditBtn;
	
	@FindBy(xpath = ""+checkoutShipSavedAddressListPageTitlee+"")
	WebElement checkoutShipSavedAddressListPageTitle;
	
	@FindBy(xpath = ""+checkoutShipAddAddresslinkk+"")
	WebElement checkoutShipAddAddresslink;
	
	@FindBy(xpath = ""+checkoutShipAddShippingAddressTitlee+"")
	WebElement checkoutShipAddShippingAddressTitle;
	
	@FindBy(xpath = ""+checkoutShipUseSavedShippingAddresslinkk+"")
	WebElement checkoutShipUseSavedShippingAddresslink;
	
	@FindBy(xpath = ""+countrySelectionn+"")
	WebElement countrySelection;
	
	@FindBy(xpath = ""+fNamee+"")
	WebElement fName;
	
	@FindBy(xpath = ""+fNametxtt+"")
	WebElement fNametxt;
	
	@FindBy(xpath = ""+lNamee+"")
	WebElement lName;
	
	@FindBy(xpath = ""+lNametxtt+"")
	WebElement lNametxt;
	
	@FindBy(xpath = ""+stAddresss+"")
	WebElement stAddress;
	
	@FindBy(xpath = ""+stAddresstxtt+"")
	WebElement stAddresstxt;
	
	@FindBy(xpath = ""+aptSuiteAddresss+"")
	WebElement aptSuiteAddress;
	
	@FindBy(xpath = ""+aptSuiteAddresstxtt+"")
	WebElement aptSuiteAddresstxt;
	
	@FindBy(xpath = ""+cityy+"")
	WebElement city;
	
	@FindBy(xpath = ""+citytxtt+"")
	WebElement citytxt;
					 
	@FindBy(xpath = ""+statee+"")
	WebElement state;
	
	@FindBy(xpath = ""+statetxtt+"")
	WebElement statetxt;
	
	@FindBy(xpath = ""+zipCodee+"")
	WebElement zipCode;
	
	@FindBy(xpath = ""+zipcodetxtt+"")
	WebElement zipcodetxt;
	
	@FindBy(xpath = ""+contactNoo+"")
	WebElement contactNo;
	
	@FindBy(xpath = ""+contactNotxtt+"")
	WebElement contactNotxt;
	
	@FindBy(xpath = ""+companyNamee+"")
	WebElement companyName;
	
	@FindBy(xpath = ""+companyNametxtt+"")
	WebElement companyNametxt;
	
	@FindBy(xpath = ""+poBoxtxtt+"")
	WebElement poBoxtxt;
	
	@FindBy(xpath = ""+addressSubmitBtnn+"")
	WebElement addressSubmitBtn;
	
	@FindBy(xpath = ""+addressCancell+"")
	WebElement addressCancel;
	
	@FindBy(xpath = ""+addressVerificationTitlee+"")
	WebElement addressVerificationTitle;
	
	@FindBy(xpath = ""+ccaddressVerificationTitlee+"")
	WebElement ccaddressVerificationTitle;
	
	@FindBy(xpath = ""+addressVerificationMatchesTitlee+"")
	WebElement addressVerificationMatchesTitle;
	
	@FindBy(xpath = ""+addressVerificationPartialMatchesTitlee+"")
	WebElement addressVerificationPartialMatchesTitle;
	
	@FindBy(xpath = ""+addressVerificationNoMatchesTitlee+"")
	WebElement addressVerificationNoMatchesTitle;
	
	@FindBy(xpath = ""+addressSuggestionFirstAddresss+"")
	WebElement addressSuggestionFirstAddress;
	
	@FindBy(xpath = ""+checkoutUseAsEnteredBtnn+"")
	WebElement checkoutUseAsEnteredBtn;
	
	@FindBy(xpath = ""+checkoutShipEditShippingAddressTitlee+"")
	WebElement checkoutShipEditShippingAddressTitle;
	
	@FindBy(xpath = ""+addressEditSubmitBtnn+"")
	WebElement addressEditSubmitBtn;
	
	@FindBy(xpath = ""+addressVerificationAddressEnteredTitlee1+"") // Common for All the pages
	WebElement addressVerificationAddressEnteredTitle1;
	
	@FindBy(xpath = ""+addressVerificationAddressEnteredTitlee2+"") // For Matches Page
	WebElement addressVerificationAddressEnteredTitle2;
	
	//@FindBy(xpath = "(//*[@id='addrEntered']//button)[1]") // Common for Both the Pages
	//@FindBy(xpath = "(//*[@id='addrEntered']//button)[2]") // Common for Both the Pages
	@FindBy(xpath = ""+addressPartialMatchesEditBtnn+"") // Common for Both the Pages
	WebElement addressPartialMatchesEditBtn;
	
	@FindBy(xpath = ""+addressNoMatchesEditBtnn+"") 
	WebElement addressNoMatchesEditBtn;
	
	@FindBy(xpath = ""+addressMatchesEditBtnn+"") 
	WebElement addressMatchesEditBtn;
	
	@FindBy(xpath = ""+checkoutSavedCreditCardTitlee+"")
	WebElement checkoutSavedCreditCardTitle;
	
	@FindBy(xpath = ""+checkoutSavedPayMethodss+"")
	WebElement checkoutSavedPayMethods;
	
	@FindBy(xpath = ""+checkoutCCAddCClinkk+"")
	WebElement checkoutCCAddCClink;
	
	@FindBy(xpath = ""+checkoutCCAddCreditCardPageTitlee+"")
	WebElement checkoutCCAddCreditCardPageTitle;
	
	@FindBy(xpath = ""+creditcardCancelBtnn+"")
	WebElement creditcardCancelBtn;
	
	@FindBy(xpath = ""+checkoutTopSubmitOrderr1+"")
	WebElement checkoutTopSubmitOrder1;
	
	@FindBy(xpath = ""+checkoutcreditcardEditPageTitlee+"")
	WebElement checkoutcreditcardEditPageTitle;
	
	@FindBy(xpath = ""+ccExistingSelectBillingInfoo+"")
	WebElement ccExistingSelectBillingInfo;
	
	@FindBy(xpath = ""+ccSelectBillingDisablee+"")
	WebElement ccSelectBillingDisable;
	
	@FindBy(xpath = ""+ccSelectBillingEnablee+"")
	WebElement ccSelectBillingEnable;
	
	@FindBy(xpath = ""+crediccardSubmitBtnn+"")
	WebElement crediccardSubmitBtn;
	
	@FindBy(xpath = ""+billUpdateSuccessPagee+"")
	WebElement billUpdateSuccessPage;
	
	@FindBy(xpath = ""+billUpdateSuccessPageCloseButtonn+"")
	WebElement billUpdateSuccessPageCloseButton;
	
	@FindBy(xpath = ""+ccNumberr+"")
	WebElement ccNumber;
	
	@FindBy(xpath = ""+ccNamee+"")
	WebElement ccName;
	
	@FindBy(xpath = ""+ccMonthh+"")
	WebElement ccMonth;
	
	@FindBy(xpath = ""+ccYearr+"")
	WebElement ccYear;
	
	@FindBy(xpath = ""+ccCsvv+"")
	WebElement ccCsv;
	
	@FindBy(xpath = ""+saveCreditCardErrorr+"")
	WebElement saveCreditCardError;
	
	@FindBy(xpath = ""+deliveryOptionContainerr+"")
	WebElement deliveryOptionContainer;
	
	@FindBy(xpath = ""+deliveryOptionTitlee+"")
	WebElement deliveryOptionTitle;
	
	@FindBy(xpath = ""+chekcoutRegisteredUserDeliveryPreferenceEditBtnn+"")
	WebElement chekcoutRegisteredUserDeliveryPreferenceEditBtn;
	
	@FindBy(xpath = ""+registeredUserDeliveryPreferenceEditBtnVisiblee+"")
	WebElement registeredUserDeliveryPreferenceEditBtnVisible;
	
	@FindBy(xpath = ""+deliveryOptionCartTotalPricee+"")
	WebElement checkoutOrderBottomPrice;
	
	@FindBy(xpath = ""+checkoutOrderTopPricee+"")
	WebElement checkoutOrderTopPrice;
	
	@FindBy(xpath = ""+checkoutOrderTopTotalTxtt+"")
	WebElement checkoutOrderTopTotalTxt;
	
	@FindBy(xpath = ""+checkoutLogoo+"")
	WebElement checkoutLogo;

	@FindBy(xpath = ""+deliveryOptionShipMethodss+"")
	WebElement deliveryOptionShipMethods;
	
	@FindBy(xpath = ""+regUserShipMethodPreferenceEnabledd+"")
	WebElement regUserShipMethodPreferenceEnabled;
	
	@FindBy(xpath = ""+regUserMembershipContainerOpenTriggerr+"")
	WebElement regUserMembershipContainerOpenTrigger;
	
	@FindBy(xpath = ""+regUserMembershipContainerFieldss+"")
	WebElement regUserMembershipContainerFields;
	
	@FindBy(xpath = ""+regUserMembershipContainerDropDownFieldd+"")
	WebElement regUserMembershipContainerDropDownField;
	
	@FindBy(xpath = ""+regUserMembershipContainerCloseTriggerr+"")
	WebElement regUserMembershipContainerCloseTrigger;
	
	@FindBy(xpath = ""+regUserLoyaltyContainerr+"")
	WebElement regUserLoyaltyContainer;

	@FindBy(xpath = ""+regUserMembershipContainerr+"")
	WebElement regUserMembershipContainer;
	
	@FindBy(xpath = ""+regUserMembershipContainerLogoo+"")
	WebElement regUserMembershipContainerLogo;
	
	@FindBy(xpath = ""+regUserMembershipContainerTitlee+"")
	WebElement regUserMembershipContainerTitle;
	
	@FindBy(xpath = ""+regUserMembershipContainerApplyButtonn+"")
	WebElement regUserMembershipContainerApplyButton;
	
	@FindBy(xpath = ""+regUserMembershipContainerCardNumberFieldd+"")
	WebElement regUserMembershipContainerCardNumberField;
	
	@FindBy(xpath = ""+regUserMembershipContainerCardNumberLabell+"")
	WebElement regUserMembershipContainerCardNumberLabel;
	
	@FindBy(xpath = ""+regUserGiftCardContainerr+"")
	WebElement regUserGiftCardContainer;
	
	@FindBy(xpath = ""+regUserGiftCardContainerLogoo+"")
	WebElement regUserGiftCardContainerLogo;
	
	@FindBy(xpath = ""+regUserGiftCardContainerOpenTriggerr+"")
	WebElement regUserGiftCardContainerOpenTrigger;
	
	@FindBy(xpath = ""+regUserGiftCardContainerTitlee+"")
	WebElement regUserGiftCardContainerTitle;
	
	@FindBy(xpath = ""+regUserGiftCardFieldss+"")
	WebElement regUserGiftCardFields;
	
	@FindBy(xpath = ""+regUserGiftCardApplyButtonn+"")
	WebElement regUserGiftCardApplyButton;
	
	@FindBy(xpath = ""+regUserGiftCardErrorr+"")
	WebElement regUserGiftCardError;
	
	@FindBy(xpath = ""+regUserGiftCardCreditCardFieldd+"")
	WebElement regUserGiftCardCreditCardField;
	
	@FindBy(xpath = ""+regUserGiftCardCreditCardFieldDefaultTextt+"")
	WebElement regUserGiftCardCreditCardFieldDefaultText;
	
	@FindBy(xpath = ""+regUserGiftCardCreditCardPinFieldd+"")
	WebElement regUserGiftCardCreditCardPinField;
	
	@FindBy(xpath = ""+regUserGiftCardCreditCardPinFieldDefaultTextt+"")
	WebElement regUserGiftCardCreditCardPinFieldDefaultText;
	
	@FindBy(xpath = ""+regUserCouponCodeContainerr+"")
	WebElement regUserCouponCodeContainer;
	
	@FindBy(xpath = ""+regUserCouponCodeContainerLogoo+"")
	WebElement regUserCouponCodeContainerLogo;
	
	@FindBy(xpath = ""+regUserCouponCodeContainerOpenTriggerr+"")
	WebElement regUserCouponCodeContainerOpenTrigger;
	
	@FindBy(xpath = ""+regUserCouponCodeContainerTitlee+"")
	WebElement regUserCouponCodeContainerTitle;
	
	@FindBy(xpath = ""+regUserCouponCodeFieldss+"")
	WebElement regUserCouponCodeFields;
	
	@FindBy(xpath = ""+regUserCouponCodeTextFieldd+"")
	WebElement regUserCouponCodeTextField;
	
	@FindBy(xpath = ""+regUserCouponCodeDefaultTextt+"")
	WebElement regUserCouponCodeDefaultText;
	
	@FindBy(xpath = ""+regUserCouponCodeApplyButtonn+"")
	WebElement regUserCouponCodeApplyButton;
	
	@FindBy(xpath = ""+regUserCouponCodeErrorr+"")
	WebElement regUserCouponCodeError;
	
	@FindBy(xpath = ""+regUserBookFairContainerr+"")
	WebElement regUserBookFairContainer;
	
	@FindBy(xpath = ""+regUserBookFairContainerOpenTriggerr+"")
	WebElement regUserBookFairContainerOpenTrigger;
	
	@FindBy(xpath = ""+regUserBookFairContainerTitlee+"")
	WebElement regUserBookFairContainerTitle;
	
	@FindBy(xpath = ""+regUserBookFairFieldss+"")
	WebElement regUserBookFairFields;
	
	@FindBy(xpath = ""+regUserBookFairTextFieldd+"")
	WebElement regUserBookFairTextField;
	
	@FindBy(xpath = ""+regUserBookFairFieldDefaultTextt+"")
	WebElement regUserBookFairFieldDefaultText;
	
	@FindBy(xpath = ""+regUserBookFairApplyButtonn+"")
	WebElement regUserBookFairApplyButton;
	
	@FindBy(xpath = ""+regUserBookFairErrorr+"")
	WebElement regUserBookFairError;
	
	@FindBy(xpath = ""+regUserBookFairContainerCloseTriggerr+"")
	WebElement regUserBookFairContainerCloseTrigger;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerr+"")
	WebElement regUserTaxExemptContainer;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerOpenTriggerr+"")
	WebElement regUserTaxExemptContainerOpenTrigger;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerTitlee+"")
	WebElement regUserTaxExemptContainerTitle;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerFieldss+"")
	WebElement regUserTaxExemptContainerFields;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerIiconn+"")
	WebElement regUserTaxExemptContainerIicon;
	
	@FindBy(xpath = ""+regUserTaxExemptCheckBoxx+"")
	WebElement regUserTaxExemptCheckBox;
	
	@FindBy(xpath = ""+regUserTaxExemptErrorr+"")
	WebElement regUserTaxExemptError;
	
	@FindBy(xpath = ""+regUserTaxExemptContainerCloseTriggerr+"")
	WebElement regUserTaxExemptContainerCloseTrigger;
	
	@FindBy(xpath = ""+regUserCouponCodeContainerCloseTriggerr+"")
	WebElement regUserCouponCodeContainerCloseTrigger;
	
	@FindBy(xpath = ""+checkoutBottomSubmitOrderr+"")
	WebElement checkoutBottomSubmitOrder;
	
	@FindBy(xpath = ""+checkoutSubmitOrderErrorr+"")
	WebElement checkoutSubmitOrderError;
	
	@FindBy(xpath = ""+billcciiconn+"")
	WebElement billcciicon;
	
	@FindBy(xpath = ""+billcctooltipEnablee+"")
	WebElement billcctooltipEnable;
	
	@FindBy(xpath = ""+cciicontooltipMaskk+"")
	WebElement cciicontooltipMask;
	
	@FindBy(xpath = ""+regUserShipDeliveryPreferencee+"")
	WebElement regUserShipDeliveryPreference;
	
	// LIST ELEMENTS
	
	@FindBy(how = How.XPATH, using = ""+mcContinueToCheckOutBtnn+"")
	List<WebElement> mcContinueToCheckOutBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutBillandShipContainerr+"")
	List<WebElement> checkoutBillandShipContainer;
	
	@FindBy(how = How.XPATH, using = ""+checkoutLoyaltyContainerr+"")
	List<WebElement> checkoutLoyaltyContainer;
	
	@FindBy(how = How.XPATH, using = ""+checkoutLoyaltyContainerTitless+"")
	List<WebElement> checkoutLoyaltyContainerTitles;
	
	@FindBy(how = How.XPATH, using = ""+checkoutShipSavedAddressListt+"")
	List<WebElement> checkoutShipSavedAddressList;
	
	@FindBy(how = How.XPATH, using = ""+checkoutShipSavedAddressDetailss+"")
	List<WebElement> checkoutShipSavedAddressDetails;
	
	@FindBy(how = How.XPATH, using = ""+checkoutShipSavedAddressEditBtnn+"")
	List<WebElement> checkoutShipSavedAddressEditBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutShipSavedAddressUseThisAddressBtnn+"")
	List<WebElement> checkoutShipSavedAddressUseThisAddressBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutAddressVerficationUseThisAddressBtnn+"")
	List<WebElement> checkoutAddressVerficationUseThisAddressBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutAddressVerificationPossibleSuggestionn+"")
	List<WebElement> checkoutAddressVerificationPossibleSuggestion;
	
	@FindBy(how = How.XPATH, using = ""+checkoutSavedCardListt+"")
	List<WebElement> checkoutSavedCardList;
	
	@FindBy(how = How.XPATH, using = ""+checkoutSavedCreditCardUseThisPayBtnn+"")
	List<WebElement> checkoutSavedCreditCardUseThisPayBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutSavedCreditCardEditBtnn+"")
	List<WebElement> checkoutSavedCreditCardEditBtn;
	
	@FindBy(how = How.XPATH, using = ""+checkoutExistingAddressListt+"")
	List<WebElement> checkoutExistingAddressList;
	
	@FindBy(how = How.XPATH, using = ""+checkoutccSavedAddressFromListt+"")
	List<WebElement> checkoutccSavedAddressFromList;
	
	@FindBy(how = How.XPATH, using = ""+cartItemTitlee+"")
	List<WebElement> cartItemTitle;
	
	@FindBy(how = How.XPATH, using = ""+deliveryOptionContainerr+"")
	List<WebElement> deliveryOptionContainerSize;
	
	@FindBy(how = How.XPATH, using = ""+deliveryOptionDeliverySelectt+"")
	List<WebElement> deliveryOptionDeliverySelect;
	
	@FindBy(how = How.XPATH, using = ""+deliveryOptionDeliveryPreferencee+"")
	List<WebElement> deliveryOptionDeliveryPreference;
	
	@FindBy(how = How.XPATH, using = ""+chekcoutRegisteredUserDeliveryPreferenceEnabledd+"")
	List<WebElement> chekcoutRegisteredUserDeliveryPreferenceEnabled;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionDeliveryMethodd+"")
	List<WebElement> deliveryOptionDeliveryMethod;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionShipMethodsLabell+"")
	List<WebElement> deliveryOptionShipMethodsLabel;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionShippingSelectt+"")
	List<WebElement> deliveryOptionShippingSelect;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionProductEditt+"")
	List<WebElement> deliveryOptionProductEdit;
	
	@FindBy(how = How.XPATH,using = ""+regUserCartItemsListt+"")
	List<WebElement> regUserCartItemsList;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemContainerr+"")
	List<WebElement> deliveryOptionCartItemContainer;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionMakeitasGiftt+"")
	List<WebElement> deliveryOptionMakeitasGift;
	
	@FindBy(how = How.XPATH,using = ""+regUserCartMakeitasGiftProductTitlee+"")
	List<WebElement> regUserCartMakeitasGiftProductTitle;
	
	@FindBy(how = How.XPATH,using = ""+giftMessageContainerr+"")
	List<WebElement> giftMessageContainer;
	
	@FindBy(how = How.XPATH,using = ""+giftMessageContainerEnablee+"")
	List<WebElement> giftMessageContainerEnable;
	
	@FindBy(how = How.XPATH,using = ""+giftMessageTextboxContainerr+"")
	List<WebElement> giftMessageTextboxContainer;
	
	@FindBy(how = How.XPATH,using = ""+giftwrapCheckboxx+"")
	List<WebElement> giftwrapCheckbox;
	
	@FindBy(how = How.XPATH,using = ""+giftMessageDefaultTextt+"")
	List<WebElement> giftMessageDefaultText;
	
	@FindBy(how = How.XPATH,using = ""+giftMessageSubmitBtnn+"")
	List<WebElement> giftMessageSubmitBtn;
	
	@FindBy(how = How.XPATH,using = ""+regUserGiftCardIndividualErrorr+"")
	List<WebElement> regUserGiftCardIndividualError;
	
	@FindBy(how = How.XPATH,using = ""+regUserBookFairIndividualErrorr+"")
	List<WebElement> regUserBookFairIndividualError;
	
	@FindBy(how = How.XPATH, using = ""+checkoutNoBillorShipAddMsgg+"")
	List<WebElement> checkoutNoBillorShipAddMsg;
	
	//Clear all the fields
	public void clearAll()
	{
		try
		{
			BNBasicfeature.scrollup(fName, driver);
			fName.clear();
			lName.clear();
			BNBasicfeature.scrolldown(stAddress,driver);
			stAddress.clear();
			aptSuiteAddress.clear();
			city.clear();
			zipCode.clear();
			contactNo.clear();
			companyName.clear();
			BNBasicfeature.scrollup(fName,driver);
		}
		catch(Exception e)
		{
			System.out.println("EXCEPTION IN CLEARING FIELDS.");
		}
	}
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	// Load the Mini Cart Feature
	public boolean loadMiniCart() throws Exception
	{
		boolean pgok = false;
		if(signedIn==true)
		{
			int count = 1;
			do
			{
				Thread.sleep(500);
				/*Actions act = new Actions(driver);
				act.moveToElement(bagIcon).click().build().perform();*/
				jsclick(bagIcon);
				Thread.sleep(100);
				try
				{
					wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block;"));
					wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
					//wait.until(ExpectedConditions.visibilityOf(mcContinueToCheckOutBtn));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(0)))
					{
						pgok = true;
						//driver.get(BNConstants.checkoutURL);
						mcContinueToCheckOutBtn.get(0).click();
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						chkPageLoaded = true;
						break;
					}
					else if((BNBasicfeature.isElementPresent(mcContinueShoppingBtn)))
					{
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
						driver.navigate().refresh();
						Thread.sleep(1000);
						//jsclick(bagIcon);
						pgok = false;
						count++;
						continue;
					}
					else
					{
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
						driver.navigate().refresh();
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						//jsclick(bagIcon);
						pgok = false;
						count++;
						continue;
					}
				}
				catch(Exception e)
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(1000);
					count++;
					continue;
				}
			}
			while(pgok==true||count<10);
		}
		return pgok;
	}
		
	// Sign In to the User Account
	public boolean signInExistingCustomerCheckout() throws IOException
	{
		boolean loadmin = false;
		try
		{
			if(!BNBasicfeature.isElementPresent(header))
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			wait.until(ExpectedConditions.elementToBeClickable(menu));
			Thread.sleep(100);
			jsclick(menu);
			wait.until(ExpectedConditions.visibilityOf(myAccount)).click();
			Thread.sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(signIn));
			signIn.click();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
			try
			{
				String Username = BNBasicfeature.getExcelVal("BRM Sign In", sheet, 1);
				String Password = BNBasicfeature.getExcelVal("BRM Sign In", sheet, 2); 
				if(BNBasicfeature.isElementPresent(emailId))
				{
					emailId.sendKeys(Username);
					Thread.sleep(100);
					log.add("The User Name is successfully entered.");
					//ArrayList<WebElement> elements = new ArrayList<>();
					if(BNBasicfeature.isElementPresent(password))
					{
						password.sendKeys(Password);
						log.add("The Password is successfully entered.");
						jsclick(secureSignIn);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(menu));
						wait.until(ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"));
						signedIn = true;
						Thread.sleep(1000);
						loadmin = loadMiniCart();
						if(loadmin==true)
						{
							loadmin = true;
							chkPageLoaded = true;
						}
						else
						{
							loadmin = false;
						}
					}
					else
					{
						Fail("Password field not found.");
					}
				}
				else
				{
					Fail("Email Id field not found.");
				}
			}	
			catch (Exception e)
			{
				Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
			}
			return loadmin;
			
		}
		catch (Exception e)
		{
			System.out.println(" Issue in Signing In to the User Account and navigating to the checkout page." );
			Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
		return loadmin;
	}
	
	// Sign In to Existing Customer who has no Shipping Address
	public boolean signInExistingCustomerNoShippingAddress() throws IOException, Exception
	{
		boolean loadmin = false;
		try
		{
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
			}
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(menu));
			menu.click();
			Thread.sleep(1000);
			wait.until(ExpectedConditions.visibilityOf(myAccount)).click();
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(signIn));
			Thread.sleep(500);
			signIn.click();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{	
				String tcid = "BRM Sign In No Shipping Address";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					try
					{
						String Username = sheet.getRow(i).getCell(1).getStringCellValue().toString();
						String Password = sheet.getRow(i).getCell(2).getStringCellValue().toString();
						//ArrayList<WebElement> elements = new ArrayList<>();
						if(BNBasicfeature.isElementPresent(emailId))
						{
							emailId.sendKeys(Username);
							log.add("The User Name is successfully entered.");
							if(BNBasicfeature.isElementPresent(password))
							{
								password.sendKeys(Password);
								log.add("The Password is successfully entered.");
								secureSignIn.click();
								wait.until(ExpectedConditions.visibilityOf(header));
								wait.until(ExpectedConditions.visibilityOf(menu));
								wait.until(ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"));
								signedIn = true;
								Thread.sleep(1000);
								/*elements.add(shoppingbag);
								elements.add(secureCheckOutBtn);
								elements.add(shopBagMaskId);
								BNBasicfeature.checkoutNavigation(elements,driver);*/
								loadmin = loadMiniCart();
								if(loadmin==true)
								{
									loadmin = true;
									break;
								}
								else
								{
									loadmin = false;
									break;
								}
							}
							else
							{
								Fail("Password field not found.");
							}
						}
						else
						{
							Fail("Email Id field not found.");
						}
					}
					catch (Exception e)
					{
						System.out.println(e.getMessage());
						Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
					}
				}
			}	
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
		return loadmin;
	}
		
	//Sign Out
	public boolean signOut() throws Exception 
	{
		boolean signedOut = false;
		try
		{
			if(BNBasicfeature.isElementPresent(checkoutLogo))
			{
				BNBasicfeature.scrollup(checkoutLogo, driver);
				driver.get(BNConstants.mobileSiteURL);
				//bnlogo.click();
			}
			else
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			wait.until(ExpectedConditions.visibilityOf(menu));
			jsclick(menu);
			wait.until(ExpectedConditions.visibilityOf(menuLists));
			wait.until(ExpectedConditions.elementToBeClickable(myAccount));
			myAccount.click();
			Thread.sleep(1000);
			if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
			{
				//log.add("The Signout button is displayed.");
				try
				{
					signOut.click();
				}
				catch (Exception e)
				{
					signOutTemp.click();
				}
				wait.until(ExpectedConditions.or(
						ExpectedConditions.visibilityOf(signOutIndicator),
						ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign In")
						)
					);
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				menu.click();
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				WebElement myaccounttext = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItemCont sk_My_Account']//*[@class='sk_mobCategoryItemTxt']"));
				if(myaccounttext.getText().contains("Sign In"))
				{
					//log.add("The User is signed out successfully.");
					signedOut = true;
					signedIn = false;
				}
				else
				{
					//Fail("The User is not signed out yet.The actual text in the container is : " + myaccounttext.getText());
					signedOut = false;
				}
			}
			else
			{
				//Fail("The Signout button is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			//Exception("There is something wrong." + e.getMessage());
		}
		jsclick(miniCartShoppingBagMaskId);
		Thread.sleep(1000);
		return signedOut;
	}
	
	// Load Billing Or Shipping Page
	public boolean ldPage(WebElement clickelement, WebElement expelemnt)
	{
		WebDriverWait ldWait = new WebDriverWait(driver, 5);
		boolean pgLoaded = false;
		try
		{
			int count = 1;
			//String currUrl = driver.getCurrentUrl();
			do
			{
				ldWait.until(ExpectedConditions.visibilityOf(clickelement));
				ldWait.until(ExpectedConditions.elementToBeClickable(clickelement));
				try
				{
					jsclick(clickelement);
					Thread.sleep(100);
					ldWait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
					Thread.sleep(100);
					ldWait.until(ExpectedConditions.visibilityOf(expelemnt));
					if(BNBasicfeature.isElementPresent(expelemnt))
					{
						pgLoaded = true;
					}
					else
					{
						count++;
						ldWait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						if(BNBasicfeature.isElementPresent(expelemnt))
						{
							pgLoaded = true;
						}
						else
						{
							driver.navigate().refresh();
							continue;
						}
					}
				}
				catch(Exception e)
				{
					try
					{
						ldWait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
					}
					catch(Exception e1)
					{
						driver.navigate().refresh();
						if(BNBasicfeature.isElementPresent(expelemnt))
						{
							pgLoaded = true;
						}
						else
						{
							driver.navigate().refresh();
							continue;
						}
						count++;
						continue;
					}
				}
			}while((pgLoaded == false)&&(count<5));
		}
		catch(Exception e)
		{
			System.out.println(" Billing or Shipping Page Load Error.");
			Exception(" Exception in Loading the Shipping or Billing Page.");
		}
		return pgLoaded;
	}
	
	public String colorfinder(WebElement element)
	{
		String ColorName = element.getCssValue("background-color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	public String colorfindeer(WebElement element)
	{
		String ColorName = element.getCssValue("border").replace("1px solid", "");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		//System.out.println(hexCode);
		return hexCode;
	}
	
	// Check if Attrubute is present or not
	private boolean isAttribtuePresent(WebElement element, String attribute) 
	{
	    Boolean result = false;
	    try 
	    {
	        boolean value = element.getAttribute(attribute).isEmpty();
	        if (value==true)
	        {
	            //result = false;
	        	result = false;
	        }
	        else if(element.getAttribute("style").contains("block"))
	        {
	        	result = false;
	        }
	        else if(element.getAttribute("style").contains("none"))
	        {
	        	result = true;
	        }
	    } 
	    catch (Exception e) 
	    {
	    	result = false;
	    }
	    return result;
	}
	
	// GET CURRENT SHIPPING ADDRESS DETAILS
	public void getCurrShipAddressDetails(int j)
	{
		try
		{
			setcardHolName = "";
			setAddress = "";
			shipOrbillCurrAddress = null;
			setcardHolName = driver.findElement(By.xpath("((//*[@id='checkoutContainer']//*[contains(@class,'col')])["+(j)+"]//p)[1]")).getText();
			setAddress = driver.findElement(By.xpath("((//*[@id='checkoutContainer']//*[contains(@class,'col')])["+(j)+"]//p)[2]")).getText();
			shipOrbillCurrAddress = driver.findElements(By.xpath("(//*[@id='reviewInfo']//*[contains(@class,'col')])["+(j)+"]//p[position()<=3]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting Current Shipping Address Details.");
			Exception("Error in getting Current Shipping Address Details.");
		}
	}
	
	// GET SAVED SHIP ADDRESS DETAILS
	public void getSavedShipAddressDetails(int j)
	{
		try
		{
			savedAddressEditBtn = null;
			savedShipAddress = "";
			savedAddressUseThisBtn = null;
			savedAddressElement = null;
			/*BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//address)["+(sel+1)+"]")),driver);
			Thread.sleep(500);
			WebElement usethibtn = driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//*[contains(@class,'use-address')])["+(sel+1)+"]"));
			WebElement editbtn = driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//*[contains(@class,'addEdit')])["+(sel+1)+"]"));*/
			BNBasicfeature.scrolldown(checkoutShipSavedAddressDetails.get(j-1), driver);
			savedAddressElement = driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//address[position()<3])["+(j)+"]"));
			savedShipAddress = savedAddressElement.getText();
			savedAddressUseThisBtn = checkoutShipSavedAddressUseThisAddressBtn.get(j-1);
			savedAddressEditBtn = checkoutShipSavedAddressEditBtn.get(j-1);
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting Saved Shipping Address Details.");
			Exception("Error in getting Saved Shipping Address Details.");
		}		
	}
	
	// GET MATCHES ADDRESS DETAILS
	public void getMatchesAddressDetails(int j)
	{
		try
		{
			useThiBtn = null;
			suggeAddressElement = null;
			suggeAddreDet = "";
			actColor = "";
			/*BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+j+"]")),driver);
			WebElement usethibtn = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+j+"]"));*/
			BNBasicfeature.scrolldown(checkoutAddressVerificationPossibleSuggestion.get(j-1), driver);
			suggeAddressElement = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//li//li)["+j+"]//data"));
			suggeAddreDet = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//li//li)["+j+"]//data")).getText();
			BNBasicfeature.scrolldown(checkoutAddressVerficationUseThisAddressBtn.get(j-1), driver);
			useThiBtn = checkoutAddressVerficationUseThisAddressBtn.get(j-1);
			actColor = colorfinder(useThiBtn);
			log.add("The actual Color is : " + actColor);
		}
		catch(Exception e)
		{
			System.out.println("Error in getting Matches Address Details.");
			Exception("Error in getting Matches Address Details.");
		}
	}
	
	// GET PARTIAL MATCHES ADDRESS DETAILS
	public void getPartialMatchesAddressDetails(int j)
	{
		try
		{
			useThiBtn = null;
			suggeAddressElement = null;
			suggeAddreDet = "";
			actColor = "";
			/*BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+j+"]")),driver);
			WebElement usethibtn = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+j+"]"));*/
			BNBasicfeature.scrolldown(checkoutAddressVerificationPossibleSuggestion.get(j-1), driver);
			suggeAddressElement = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//li//li)["+j+"]//data"));
			suggeAddreDet = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//li//li)["+j+"]//data")).getText();
			//BNBasicfeature.scrolldown(checkoutAddressVerficationUseThisAddressBtn.get(j-1), driver);
			useThiBtn = checkoutAddressVerficationUseThisAddressBtn.get(j-1);
			actColor = colorfinder(useThiBtn);
		}
		catch(Exception e)
		{
			System.out.println("Error in getting Partial Matches Address Details.");
			Exception("Error in getting Partial Matches Address Details.");
		}
	}
		
	// GET SAVED CARD ADDRESS DETAILS
	public void getSavedCreditCardAddressDetails(int j)
	{
		try
		{
			savedCCEditBtn = null;
			savedCCHolName = "";
			savedCCNum = "";
			savedCCUseThisBtn = null;
			savedAddressElement = null;
			/*BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//address)["+(sel+1)+"]")),driver);
			Thread.sleep(500);
			WebElement usethibtn = driver.findElement(By.xpath("(//*[contains(@data-modal-name,'saved')][contains(@class,'focus')]//*[@data-itemscope='PaymentMethod'])["+(j+1)+"]//*[@type='submit']"));
			WebElement editbtn = driver.findElement(By.xpath("(//*[contains(@data-modal-name,'saved')][contains(@class,'focus')]//*[@data-itemlist]//*[@data-itemscope='PaymentMethod'])["+(j+1)+"]//a"));*/
			BNBasicfeature.scrolldown(checkoutSavedCardList.get(j-1), driver);
			//savedAddressElement = driver.findElement(By.xpath("(//*[@data-itemlist='CheckoutPostalAddress']//address[position()<3])["+(j)+"]"));
			//savedShipAddress = savedAddressElement.getText();
			savedCCUseThisBtn = checkoutSavedCreditCardUseThisPayBtn.get(j-1);
			savedCCEditBtn = checkoutSavedCreditCardEditBtn.get(j-1);
			savedCCHolName = driver.findElement(By.xpath("((//*[contains(@class,'modal')][contains(@style,'block')]//*[@data-itemscope='PaymentMethod'])["+(j)+"]//*[contains(@class,'mb')]//span)[1]")).getText();
			savedCCNum = driver.findElement(By.xpath("(//*[contains(@class,'modal')][contains(@style,'block')]//*[@data-itemscope='PaymentMethod'])["+j+"]//*[contains(@class,'ending')]")).getText();
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting Saved Shipping Address Details.");
			Exception("Error in getting Saved Shipping Address Details.");
		}		
	}
	
	// Select Gift Container
	public void getGiftContainerDetails()
	{
		giftChkBox = null;
		giftIndicator = null;
		giftMessageEditbutton = null;
		chkBoxOk = false;
		gftOk = false;
		gftEdit = false;
		try
		{
			try
			{
				giftChkBox = driver.findElement(By.xpath("((//*[contains(@class,'make-it-gift__text-row hidden')])["+gftsel+"]//*[contains(@class,'gift-wrap-checkbox checkbox')])"));
				chkBoxOk = true;
			}
			catch(Exception chkbx)
			{
				System.out.println(" No Checkbox.");
				chkBoxOk = false;
			}
			
			try
			{
				giftIndicator = driver.findElement(By.xpath("(//*[@class='gift-wrap-message-indicator'])"));
				gftOk = true;
			}
			catch(Exception gftIndicator)
			{
				System.out.println(" No Gift Indicator.");
				gftOk = false;
			}
			
			try
			{
				giftMessageEditbutton = giftIndicator.findElement(By.xpath("//*[contains(@class,'edit-gift-wrap-message')]"));
			}
			catch(Exception giftEditButton)
			{
				System.out.println(" No Gift Edit Button.");
				gftEdit = false;
			}
			
		}
		catch(Exception e)
		{
			System.out.println(" Issue in selecting gift Container.");
			Exception(" Issue in selecting gift Container.");
		}
	}
		
	/* Chk - 84 Verify that saved shipping address should be displayed by default in the shipping information section of checkout page for logged-in user.*/
	public void checkoutExistingshippingDetails()
	{
		ChildCreation(" Chk - 84 Verify that saved shipping address should be displayed by default in the shipping information section of checkout page for logged-in user.");
		try
		{
			chkPageLoaded = signInExistingCustomerCheckout();
			if(chkPageLoaded==true)
			{
				String shippingCaption = BNBasicfeature.getExcelVal("Chk84", sheet, 2); 
				if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
				{
					log.add("The user is navigated to the Checkout Page.");
					List<WebElement> shippingContainer = checkoutBillandShipContainer;
					for(int j = 1; j<=shippingContainer.size();j++)
					{
						WebElement addrInfo = checkoutBillandShipContainer.get(j-1);
						log.add("The Expected title was : " + shippingCaption);
						String actTitle = addrInfo.getText();
						log.add("The Actual title is : " + actTitle);
						if(actTitle.contains(shippingCaption))
						{
							log.add("The Shipping Address Container is found for the User.");
							getCurrShipAddressDetails(1);
							for(WebElement addDetails : shipOrbillCurrAddress)
							{
								String addr = addDetails.getText();
								log.add("The displyed address is : " + addr);
								if(!addr.isEmpty())
								{
									Pass("The Existing address is displayed and they are visible.",log);
								}
								else
								{
									Fail("The Existing address is not displayed.",log);
								}
							}
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Fail("The user is not in the Checkout Page. Please Check.");
				}
			}
			else
			{
				Fail( "The User is not navigated to the the checkout page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 84 Issue ." + e.getMessage());
			Exception(" BRM - 84 Issue ." + e.getMessage());
		}
	}
	
	/* Chk - 97 Verify that Shipping and billing information, Delivery speed and their information, Member ship and gift cards should be displayed as per the creative in the checkout page */
	public void checkoutOrderShipAddressLoyaltyContainer()
	{
		ChildCreation(" BRM - 97 Verify that Shipping and billing information, Delivery speed and their information, Member ship and gift cards should be displayed as per the creative in the checkout page.");
		if(chkPageLoaded==true)
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{	
				String tcid = "Chk77";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					String billingcaption = sheet.getRow(i).getCell(4).getStringCellValue().toString();
					String shippingcaption = sheet.getRow(i).getCell(5).getStringCellValue().toString();
					if(BNBasicfeature.isElementPresent(checkoutOrderTopTotal))
					{
						BNBasicfeature.scrollup(checkoutOrderTopTotal,driver);
					}
					try
					{
						if(BNBasicfeature.isListElementPresent(checkoutBillandShipContainer))
						{
							log.add("The Billing and Shipping area container is Present and it is Visible.");
							int addresssize = checkoutBillandShipContainer.size();
							for(int j = 1; j <= addresssize; j++)
							{
								WebElement billshipname = checkoutBillandShipContainer.get(j-1);
								if(billshipname.getText().equals(billingcaption))
								{
									Pass("The Billing area is Present and it is Visible.");
								}
								else if(billshipname.getText().equals(shippingcaption))
								{
									Pass("The Shipping area is Present and it is Visible.");
								}
								else
								{
									Fail("The Billing or Shipping area container is not Present / Visible." + billshipname.getText());
								}
							}
						}
						else
						{
							Fail("The Billing and Shipping area container is not Present / Visible.");
						}
						
						if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
						{
							BNBasicfeature.scrolldown(checkoutMembershipContainerTitle,driver);	
						}
						String[] value = new String[10];
						int shval = 33;
						if(BNBasicfeature.isListElementPresent(checkoutLoyaltyContainer))
						{
							if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
							{
								log.add("The expected title was : " + sheet.getRow(i).getCell(32).getStringCellValue().toString());
								if(checkoutMembershipContainerTitle.getText().contains(sheet.getRow(i).getCell(32).getStringCellValue().toString()))
								{
									log.add("The actual title is : " + checkoutMembershipContainerTitle.getText());
									Pass("The title matches the expected content.",log);
								}
								else
								{
									log.add("The actual title is : " + checkoutMembershipContainerTitle.getText());
									Fail("The title does not matches the expected content.",log);
								}
							}
							else
							{
								Fail("The Loyalty container is not found.");
							}
							for(int j = 1; j<=checkoutLoyaltyContainerTitles.size(); j++)
							{
								value[j] = sheet.getRow(i).getCell(shval).getStringCellValue().toString();
								shval++;
								String actTitle = checkoutLoyaltyContainerTitles.get(j-1).getText();
								log.add("The actual title is : " + actTitle);
								if(value[j].contains(actTitle)||(actTitle.contains(value[j])))
								{
									log.add("The Container title is " + actTitle + " and it matches the expected one.");
									Pass("The Loyalty Container title is present",log);
								}
								else
								{
									log.add("The Container title is " + actTitle + " and it matches the expected one.");
									Fail("The Loyalty Container title is not present.",log);
								}
							}
						}
						else
						{
							Fail("The Loyalty Container is not displayed.");
						}
					}
					catch(Exception e)
					{
						System.out.println(" BRM - 97 Issue ." + e.getMessage());
						Exception(" BRM - 97 Issue." + e.getMessage());
					}
				}
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	
	/* Chk - 89 Verify that "Edit" option should be displayed in the shipping/billing information section to allow the user to edit/update the address*/
	public void checkoutExistingshippingEditButton() throws IOException
	{
		ChildCreation(" CHk - 89 Verify that Edit option should be displayed in the shipping/billing information section to allow the user to edit/update the address.");
		if(signedIn==true)
		{
			try
			{
				String editCaption = BNBasicfeature.getExcelVal("Chk89", sheet, 2); 
				if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
				{
					BNBasicfeature.scrollup(checkoutShipEditBtn, driver);
					log.add("The Add or Edit button is visible for the Billing and Shipping Address.");
					String btnname = checkoutShipEditBtn.getText();
					log.add( "The Actual title is : " + btnname);
					log.add( "The Expected title was : " + editCaption);
					if(btnname.equals(editCaption))
					{
						Pass("The Edit buttons is Present and it is Visible.");
					}
					else
					{
						Fail("The Billing or Shipping area container is not Present / Visible." + btnname);
					}
				}
				else
				{
					Fail("The Billing and Shipping area container is not Present / Visible.");
				}
				
				if(BNBasicfeature.isElementPresent(checkoutBillEditBtn))
				{
					log.add("The Add or Edit button is visible for the Billing and Shipping Address.");
					String btnname = checkoutBillEditBtn.getText();
					log.add( "The Actual title is : " + btnname);
					log.add( "The Expected title was : " + editCaption);
					if(btnname.equals(editCaption))
					{
						Pass("The Edit buttons is Present and it is Visible.",log);
					}
					else
					{
						Fail("The Billing or Shipping area container is not Present / Visible." + btnname);
					}
				}
				else
				{
					Fail("The Billing and Shipping area container is not Present / Visible.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 89 Issue ." + e.getMessage());
				Exception(" BRM - 89 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* Chk - 85 Verify that on selecting EDIT in shipping information, "select a saved shipping information" page must be opened.*/
	public void checkoutOrderShippingSavedAddressList()
	{
		ChildCreation(" Chk - 85 Verify that on selecting EDIT in shipping information, Select a saved shipping information page must be opened.") ;
		if(chkPageLoaded==true)
		{
			try
			{
				wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				Thread.sleep(200);
				if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
				{
					log.add("The user is navigated to the Checkout Page.");
					if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
					{
						Pass("The Edit button in Shipping Address is present and it is visible. ",log);
						boolean loaPage = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle);
						if(loaPage==true)
						{
							wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
							Thread.sleep(100);
							if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressListPageTitle))
							{
								log.add("The user is navigated to the Select a Saved Shipping Address Page.");
								if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
								{
									for(int i = 1; i<=checkoutShipSavedAddressList.size(); i++)
									{
										getSavedShipAddressDetails(i);
										if(savedShipAddress.isEmpty())
										{
											Fail("The address list is not displayed / the text is empty.");
										}
										else
										{
											Pass("The " + i +" Address Details displayed are " + savedShipAddress);
										}
									}
								}
								else
								{
									Fail("The Saved address list is not found.",log);
								}
							}
							else
							{
								Fail("The use is not navigated to the Select a Saved Address Page.",log);
							}
						}
						else
						{
							Fail("The User failed to load the Saved Shipping Address Page.");
						}
					}
					else
					{
						Fail("The Ship Edit button is not displayed.");
					}
				}
				else
				{
					Fail("The Checkout Top Submit Order button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 85 Issue ." + e.getMessage());
				Exception(" BRM - 85 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the checkout page.");
		}
	}
	
	/* Chk - 86 Verify that onclicking "create a new shipping addres" should be displayed in the "Add a Shipping Address" page */
	public void checkoutAddaShippingAddressPage()
	{
		ChildCreation(" Chk - 86 Verify that onclicking Create a new shipping addres should be displayed in the Add a Shipping Address page.");
		if(chkPageLoaded==true)
		{
			try
			{
				String pagetit = BNBasicfeature.getExcelVal("Chk86", sheet, 2);
				if(BNBasicfeature.isElementPresent(checkoutShipAddAddresslink))
				{
					Thread.sleep(100);
					Pass("The Add a Shipping Address link is present / visible.");
					jsclick(checkoutShipAddAddresslink);
					wait.until(ExpectedConditions.visibilityOf(checkoutShipAddShippingAddressTitle));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(checkoutShipAddShippingAddressTitle))
					{
						Pass("The user is navigated to the Add a Shipping Address page.");
						log.add("The Expected title was : " + pagetit);
						String actTitle = checkoutShipAddShippingAddressTitle.getText();
						log.add("The actual title is : " + actTitle);
						log.add("The user is navigated to the Add a Shipping Address Page.");
						if(actTitle.equals(pagetit))
						{
							Pass("The Add a Shipping Address Page caption matches.",log);
						}
						else
						{
							Fail("The Add a Shipping Address Page caption does not matches.",log);
						}
					}
					else
					{
						Fail("The user is not navigated to the Add a Shipping Address page.");
					}
				}
				else
				{
					Fail("The User failed to load the Saved Address List Page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 86 Issue ." + e.getMessage());
				Exception(" BRM - 86 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 87 Verify that "Use an existing shipping address" option should be displayed in the "Add a Shipping Address" section.*/
	public void EditShipInfoUseExistingShippingAdd()
	{
		ChildCreation(" Chk - 87 Verify that Use an existing shipping address option should be displayed in the Add a Shipping Address section.");
		if(chkPageLoaded==true)
		{
			try
			{
				String caption = BNBasicfeature.getExcelVal("Chk87", sheet, 2);
				if(BNBasicfeature.isElementPresent(checkoutShipUseSavedShippingAddresslink))
				{
					Pass(" The Use an Exising Shipping Address Link is found in the Add a Shipping Address Page.",log);
					log.add("The Expected title was : " + caption);
					String actTitle = checkoutShipUseSavedShippingAddresslink.getText();
					log.add("The Actual title is : " + actTitle);
					
					if(actTitle.contains(caption))
					{
						Pass("The user is navigated to the Add a Shipping Address Page and the Caption matches the expected one.",log);
					}
					else
					{
						Fail("The user is navigated to the Add a Shipping Address Page but the Caption does not matches the expected one.",log);
					}
				}
				else
				{
					Fail(" The Use an Exising Shipping Address Link is not found in the Add a Shipping Address Page.",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 87 Issue ." + e.getMessage());
				Exception(" BRM - 87 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");	
		}
	}
	
	/* Chk - 88 Verify that while selecting the "Use an existing shipping address" option in the "Add a shipping Address",it should move to the "Select a shipping address" page.*/
	/* Chk - 113 Verify that onselecting Use an existing shipping address in Add a shipping address page, then it should redirect to multiple shipping address page should be shown*/
	public void EditShipToMAddAddressUseExistingAddressLinkNavigation()
	{
		ChildCreation(" Chk - 88 Verify that while selecting the Use an existing shipping address option in the Add a shipping Address,it should move to the Select a shipping address page.");
		boolean chk113 = false;
		if(chkPageLoaded==true)
		{
			try
			{
				String caption = BNBasicfeature.getExcelVal("Chk87", sheet, 3);
				if(BNBasicfeature.isElementPresent(checkoutShipAddShippingAddressTitle))
				{
					BNBasicfeature.scrollup(checkoutShipAddShippingAddressTitle,driver);
				}
				if(BNBasicfeature.isElementPresent(checkoutShipUseSavedShippingAddresslink))
				{
					Pass("The Use a Existing Shipping Address link is found and it is visible.");
					
					jsclick(checkoutShipUseSavedShippingAddresslink);
					wait.until(ExpectedConditions.visibilityOf(checkoutShipAddAddresslink));
					wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
					wait.until(ExpectedConditions.visibilityOfAllElements(checkoutShipSavedAddressList));
					if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
					{
						Pass("The user is navigated back to the Select a Saved Shipping Address Page and the Saved Address List is displayed.");
						chk113 = true;
					}
					else
					{
						Fail("The user is not navigated back to the Select a Saved Shipping Address Page and the Saved Address List is not displayed.");
						chk113 = false;
					}
				
					String title = checkoutShipSavedAddressListPageTitle.getText();
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + caption);
					if(title.contains(caption))
					{
						Pass("The Select a Saved Shipping Address page caption matches the expected One.",log);
						if(chk113==true)
						{
							chk113 = true;
						}
					}
					else
					{
						Fail("The Select a Saved Shipping Address page caption does not matches the expected One.",log);
						chk113 = false;
					}
				}
				else
				{
					Fail("The Use a Saved Shipping Address link is not found / visible.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 88 Issue ." + e.getMessage());
				Exception(" BRM - 88 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 113 Verify that onselecting Use an existing shipping address in Add a shipping address page, then it should redirect to multiple shipping address page should be shown.*/
		ChildCreation(" Chk - 113 Verify that onselecting Use an existing shipping address in Add a shipping address page, then it should redirect to multiple shipping address page should be shown.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(chk113==true)
				{
					Pass("The user is navigated back to the Select a Saved Shipping Address Page and the Saved Address List is displayed.");
				}
				else
				{
					Fail("The Select a Saved Shipping Address page caption does not matches the expected One.");
				}
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage() + " Chk 113");
				Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* BRM - 114 Verify that the user can able to select any of the saved shipping address*/
	public void ShipToMAddAddressChooseFromAddressList()
	{
		ChildCreation(" BRM - 114 Verify that the user can able to select any of the saved shipping address.");
		if(chkPageLoaded==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
				if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
				{
					Random r = new Random();
					int sel = r.nextInt(checkoutShipSavedAddressList.size());
					if(sel<1)
					{
						sel = 1;
					}
					
					getSavedShipAddressDetails(sel);
					String addr = savedShipAddress;
					if(addr.isEmpty())
					{
						Fail("The Saved Address list is empty.");
					}
					else
					{
						Pass("The Saved Address list is displayed. The displayed address is : " + addr);
					}
				}
				else
				{
					Fail("The user is not in the Select a Saved Shipping Address Page and the Saved Address List is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 114 Issue ." + e.getMessage());
				Exception(" BRM - 114 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* BRM - 117 Verify that "Use this address" and "edit" buttons must be enabled near the selected address, when user selects any of the address in multiple address list*/
	public void ShipToMAddAddressButtonsEnableFromAddressList()
	{
		ChildCreation(" BRM - 117 Verify that Use this address and edit buttons must be enabled near the selected address, when user selects any of the address in multiple address list.");
		if(chkPageLoaded==true)
		{
			try
			{
				String caption = BNBasicfeature.getExcelVal("Chk117", sheet, 2);
				String[] btnname = caption.split("\n");
				
				wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
				Thread.sleep(200);
				if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
				{
					log.add("The Saved Address List page is displayed.");
					Random r = new Random();
					int sel = r.nextInt(checkoutShipSavedAddressList.size());
					if(sel<1)
					{
						sel = 1;
					}
					getSavedShipAddressDetails(sel);
					
					WebElement usebtn = savedAddressUseThisBtn;
					if(usebtn.isEnabled())
					{
						Pass("The Use This Button is Enabled for the selected address.",log);
						String btnName = savedAddressUseThisBtn.getAttribute("value").toString();
						log.add("The Expected button name was : " + btnname[0].toString());
						log.add("The Expected button name was : " + btnName);
						if(btnName.contains(btnname[0]))
						{
							Pass("The Use this Address button caption matches the expected one.",log);
						}
						else
						{
							Fail("The Use this Address button caption does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Use This Button is not Enabled for the selected address.",log);
					}
					
					WebElement editbtn = savedAddressEditBtn;
					if(editbtn.isEnabled())
					{
						Pass("The Edit button is Enabled for the selected address.");
						String edittbtn = savedAddressEditBtn.getText();
						log.add("The Expected button name was : " + btnname[1].toString());
						log.add("The Actual button name is : " + editbtn);
						if(edittbtn.contains(btnname[1]))
						{
							Pass("The Edit button caption matches the expected one.",log);
						}
						else
						{
							Fail("The Edit button caption does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Edit button is not Enabled for the selected address.");
					}
				}
				else
				{
					Fail("The user is not in the Select a Saved Shipping Address Page and the Saved Address List is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 117 Issue ." + e.getMessage());
				Exception(" BRM - 117 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 115 Verify that the User can be able to change the current address from the list by selecting "use this address" option.*/
	public void EditShipAddressChangeToAddressFromList()
	{
		ChildCreation(" Chk - 115 Verify that the User can be able to change the current address from the list by selecting use this address option.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressListPageTitle))
				{
					log.add("The user is navigated to the Select a Saved Shipping Address Page.");
					Random r = new Random();
					int sel = r.nextInt(checkoutShipSavedAddressList.size());
					if(sel<1)
					{
						sel = 1;
					}
					if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressDetails))
					{
						getSavedShipAddressDetails(sel);
						if(BNBasicfeature.isElementPresent(savedAddressUseThisBtn))
						{
							jsclick(savedAddressUseThisBtn);
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							Thread.sleep(250);
							if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
							{
								Pass("The user is navigated to the Checkout Page when Use This Address button is clicked.");
								getCurrShipAddressDetails(1);
								for(int i = 0;i<shipOrbillCurrAddress.size();i++)
								{
									//System.out.println(shipOrbillCurrAddress.get(i).getText());
									if(savedShipAddress.contains(shipOrbillCurrAddress.get(i).getText()))
									{
										log.add("The Current selected address is : " + shipOrbillCurrAddress.get(i).getText());
										log.add("The selected address details was : " + savedShipAddress);
										Pass("The User Selected address is displayed.",log);
									}
									else
									{
										log.add("The Current selected address is : " + shipOrbillCurrAddress.get(i).getText());
										log.add("The selected address details was : " + savedShipAddress);
										Fail("The User Selected address is displayed.But there is some mismatch.",log);
									}
								}
							}
							else
							{
								Fail("The user is not navigated to the Checkout Page when Use This Address button is clicked.");
							}
						}
						else
						{
							Fail( "The Use this Address button is not displayed.");
						}
					}
					else
					{
						Fail( "The Saved Address List is not displayed.");
					}
				}
				else
				{
					Fail("The user is not navigated to the Saved Address List page to select the Address from the List.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 115 Issue ." + e.getMessage());
				Exception(" BRM - 115 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 118 Verify that the user can able to add multiple shipping addresses and added addresses should be displayed as per the classic site order.*/
	public void AddressDoctorAddShippingAddress()
	{
		ChildCreation("  Chk - 118 Verify that the user can able to add multiple shipping addresses and added addresses should be displayed as per the classic site order.");
		if(chkPageLoaded==true)
		{
			try
			{
				String firstname = BNBasicfeature.getExcelVal("Chk118", sheet, 5);
				String lastname = BNBasicfeature.getExcelVal("Chk118", sheet, 6);
				String streetAdd = BNBasicfeature.getExcelVal("Chk118", sheet, 7);
				String cty = BNBasicfeature.getExcelVal("Chk118", sheet, 8);
				String zpcode = BNBasicfeature.getExcelNumericVal("Chk118", sheet, 9); 
				String cntcno = BNBasicfeature.getExcelNumericVal("Chk118", sheet, 11); 
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
				{
					log.add("The user is navigated to the Checkout Page.");
					if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
					{
						Pass("The Edit button in Shipping Address is present and it is visible. ");
						boolean loadPg = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle);
						if(loadPg==true)
						{
							wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
							if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressListPageTitle))
							{
								log.add("The user is navigated to the Select a Saved Shipping Address Page.");
								if(BNBasicfeature.isElementPresent(checkoutShipAddAddresslink))
								{
									log.add("The Add a Shipping Address link is found.");
									jsclick(checkoutShipAddAddresslink);
									wait.until(ExpectedConditions.visibilityOf(fName));
									Thread.sleep(500);
									log.add("The user is navigated to the Add a Shipping Address page.");
									clearAll();
									fName.sendKeys(firstname);
									lName.sendKeys(lastname);
									stAddress.sendKeys(streetAdd);
									BNBasicfeature.scrolldown(city,driver);
									city.sendKeys(cty);
									Select statesel = new Select(state);
									statesel.selectByIndex(35);
									zipCode.sendKeys(zpcode);
									contactNo.sendKeys(cntcno);
									jsclick(addressSubmitBtn);
									//Thread.sleep(3000);
									wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
									Thread.sleep(250);
									if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
									{
										BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
										Random r = new Random();
										int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
										if(sel<1)
										{
											sel = 1;
										}
										
										getMatchesAddressDetails(sel);
										log.add("The randomly selected address is : " + suggeAddreDet);
										WebElement usthisbtn = useThiBtn;
										jsclick(usthisbtn);
										//Thread.sleep(4000);
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
										{
											Pass("The user selected address is saved and the user is navigated to the Checkout page.");
											getCurrShipAddressDetails(1);
											String setcdholname = setcardHolName;
											log.add("The Created billing account name is : " + setcdholname);
											if(setcardHolName.isEmpty())
											{
												Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
											}
											else
											{
												Pass("The created address is set in the Checkout page.",log);
											}
											
											log.add("The randomly selected address is : " + suggeAddreDet);
											log.add("The Created billing address is : " + setAddress);
											if(suggeAddreDet.contains(setAddress))
											{
												Pass("The created address is set in the Checkout page.",log);
											}
											else
											{
												Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
											}
										}
										else
										{
												Fail("There is something wrong. The user is not navigated to the Checkout page.");
										}
									}
									else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
									{
										BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
										Random r = new Random();
										int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
										if(sel<1)
										{
											sel = 1;
										}
										getPartialMatchesAddressDetails(sel);
										log.add("The randomly selected address is : " + suggeAddreDet);
										WebElement usthisbtn = useThiBtn;
										jsclick(usthisbtn);
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
										{
											Pass("The user selected address is saved and the user is navigated to the Checkout page.");
											getCurrShipAddressDetails(1);
											String setcdholname = setcardHolName;
											log.add("The Created billing account name is : " + setcdholname);
											if(suggeAddreDet.isEmpty())
											{
												Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
											}
											else
											{
												Pass("The created address is set in the Checkout page.",log);
											}
											
											log.add("The randomly selected address is : " + suggeAddreDet);
											log.add("The Created billing address is : " + setAddress);
											if(suggeAddreDet.contains(setAddress))
											{
												Pass("The created address is set in the Checkout page.",log);
											}
											else
											{
												Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
											}
										}
										else
										{
											Fail("There is something wrong. The user is not navigated to the Checkout page.");
										} 
									}
									else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
									{
										if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
										{
											jsclick(checkoutUseAsEnteredBtn);
											Thread.sleep(4000);
											wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(250);
											if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
											{
												Pass("The user selected address is saved and the user is navigated to the Checkout page.");
											}
											else
											{
												Fail("There is something wrong. The user is not navigated to the Checkout page.");
											}
										}
										else
										{
											Fail( "The Use as Entered Button is not displayed.");
										}
									}
									else
									{
										Fail("There is something wrong.Please Check.");
									}
								}
								else
								{
									Fail("The Add a Shipping Address link is not found.");
								}
							}
							else
							{
								Fail("The user is not navigated to the Select a Saved Shipping Address Page.");
							}
						}
						else
						{
							Fail( "The User Failed to load the Select a Saved Shipping Address Page.");
						}
					}
					else
					{
						Fail("The Edit button is not available for the Shipping Address.");
					}
				}
				else
				{
					Fail("The user is not navigated to the Checkout Page. Please Check.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 118 Issue ." + e.getMessage());
				Exception(" BRM - 118 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* Chk - 90 Ship Verify that all the details given are pre_filled correctly while adding address in edit shipping address and billing information*/
	/* Chk - 116 Verify that the user can able to edit any address given in the multiple list.*/
	public void EditShipShippingEditDetails()
	{
		ChildCreation(" Chk - 90 Ship Verify that all the details given are pre_filled correctly while adding address in edit shipping address and billing information.");
		boolean chk116 = false;
		if(chkPageLoaded==true)
		{
			try
			{
				String pagetit = BNBasicfeature.getExcelVal("Chk90S", sheet, 2); 
				wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				Thread.sleep(200);
				if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
				{
					log.add("The user is navigated to the Checkout Page.");
					if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
					{
						Pass("The Edit button in Shipping Address is present and it is visible. ");
						boolean loadPage = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle);
						if(loadPage==true)
						{
							wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
							Thread.sleep(200);
							if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressListPageTitle))
							{
								log.add("The user is navigated to the Select a Saved Shipping Address Page.");
								Random r = new Random();
								int sel = r.nextInt(checkoutShipSavedAddressList.size());
								
								if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressList.get(sel)))
								{
									Pass("The Saved Address is displayed.");
									if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressEditBtn.get(sel)))
									{
										jsclick(checkoutShipSavedAddressEditBtn.get(sel));
										wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
										if(BNBasicfeature.isElementPresent(checkoutShipEditShippingAddressTitle))
										{
											log.add("The user is navigated to the Edit Shipping Page.");
											log.add("The Expected title was : " + pagetit);
											String actTitle = checkoutShipEditShippingAddressTitle.getText();
											log.add("The Expected title was : " + actTitle);
											if(actTitle.contains(pagetit))
											{
												Pass("The user is navigated to the Edit Shipping Address page and the page title value matches the expected content.",log);
												chk116 = true;
											}
											else
											{
												Fail("The user is navigated to the Edit Shipping Address page and the page title value does not match the expected content.",log);
												chk116 = false;
											}
											
											BNBasicfeature.scrollup(fName,driver);
											if(BNBasicfeature.isElementPresent(fName))
											{
												if(fName.getAttribute("value").isEmpty())
												{
													log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
													Fail("The First name field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
													Pass("The First Name field is displayed.",log);
												}
											}
											else
											{
												Fail("There is something wrong. First Name field is not displayed." );
											}
											
											if(BNBasicfeature.isElementPresent(lName))
											{
												if(lName.getAttribute("value").isEmpty())
												{
													log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
													Fail("The Last name field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
													Pass("The Last Name field is displayed and there is value present.",log);
												}
											}
											else
											{
												Fail("There is something wrong. Last Name field is not displayed." );
											}
											
											BNBasicfeature.scrolldown(stAddress,driver);
											
											if(BNBasicfeature.isElementPresent(stAddress))
											{
												if(stAddress.getAttribute("value").isEmpty())
												{
													log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
													Fail("The Street Address field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
													Pass("The Street Address field is displayed and there is value present in it.",log);
												}
											}
											else
											{
												Fail("There is something wrong. Street Address field is not displayed." );
											}
											
											if(BNBasicfeature.isElementPresent(city))
											{
												if(city.getAttribute("value").isEmpty())
												{
													log.add("The Value in the City field is : " + city.getAttribute("value").toString());
													Fail("The City field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the City field is : " + city.getAttribute("value").toString());
													Pass("The City field is displayed and there is value in it.",log);
												}
											}
											else
											{
												Fail("There is something wrong. City field is not displayed." );
											}
											
											if(BNBasicfeature.isElementPresent(zipCode))
											{
												if(zipCode.getAttribute("value").isEmpty())
												{
													log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
													Fail("The Zipcode field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
													Pass("The Zipcode field is displayed and there is value in it.",log);
												}
											}
											else
											{
												Fail("There is something wrong. Zipcode field is not displayed." );
											}
											
											if(BNBasicfeature.isElementPresent(state))
											{
												Select sele = new Select(state);
												if(sele.getFirstSelectedOption().getText()!=null)
												{
													log.add("The Selected Value in the State field is : " + sele.getFirstSelectedOption().getText());
													Pass("The State field is not empty and the State is selected.",log);
												}
												else
												{
													log.add("The Selected Value in the State field is : " + sele.getFirstSelectedOption().getText());
													Fail("The State is not selected and the value is empty.",log);
												}
											}
											else
											{
												Fail("There is something wrong. State Dropdown field is not displayed." );
											}
											
											if(BNBasicfeature.isElementPresent(contactNo))
											{
												if(contactNo.getAttribute("value").isEmpty())
												{
													log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
													Fail("The Contact Number field is displayed and the value is empty.",log);
												}
												else
												{
													log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
													Pass("The Contact Number field is displayed and there is value present in it.",log);
												}
											}
											else
											{
												Fail("There is something wrong. Contact Number field is not displayed." );
											}
										}
										else
										{
											Fail("The user is not navigated to the Edit Shipping Address Page.");
										}
									}
									else
									{
										Fail("The Edit button is not displayed.");
									}
								}
								else
								{
									Fail("The Selected Saved address is not displayed.");
								}
							}
							else
							{
								Skip("The Saved address list is not displayed.");
							}
						}
						else
						{
							Fail( "The User failed to load the Saved Address List Page.");
						}
					}
					else
					{
						Fail( "The Edit button in the Checkout page is not displayed.");
					}
				}
				else
				{
					Fail( "The User is not navigated to the checkout page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 90 Shipping Issue ." + e.getMessage());
				Exception(" BRM - 90 Shipping Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 116 Verify that the user can able to edit any address given in the multiple list.*/
		ChildCreation( " Chk - 116 Verify that the user can able to edit any address given in the multiple list." );
		if(chkPageLoaded==true)
		{
			try
			{
				if(chk116==true)
				{
					Pass( " The User was able to select the address to edit and they are redirected to the Edit Ship Address Page.");
				}
				else
				{
					Fail( " The User was not able to select the address to edit and they are not redirected to the Edit Ship Address Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage() + " Chk 90");
				Exception(" There is something wrong. Please Check. " + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 92 Verify that while selecting the "Save & Continue" button after entering the required fields,the Address verification page should be displayed*/
	public void EditShipShippingEditDetailsSaveAddressVerfication()
	{
		ChildCreation(" Chk - 92 Verify that while selecting the Save & Continue button after entering the required fields,the Address verification page should be displayed.");
		if(chkPageLoaded==true)
		{
			try
			{
				String pgetit = BNBasicfeature.getExcelVal("Chk92", sheet, 4);
				String stadd = BNBasicfeature.getExcelVal("Chk92", sheet, 9);
				String cty = BNBasicfeature.getExcelVal("Chk92", sheet, 10);
				String zpcode = BNBasicfeature.getExcelNumericVal("Chk92", sheet, 12);
				String cntcno = BNBasicfeature.getExcelNumericVal("Chk92", sheet, 13);
						
				BNBasicfeature.scrolldown(stAddress,driver);
				stAddress.clear();
				stAddress.sendKeys(stadd);
				city.clear();
				city.sendKeys(cty);
				zipCode.clear();
				zipCode.sendKeys(zpcode);
				contactNo.clear();
				contactNo.sendKeys(cntcno);
				jsclick(addressEditSubmitBtn);
				wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
				//Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					Pass("The user is in the Address Verification page.");
					log.add( "The Expected title was : " + pgetit);
					String actTitle = addressVerificationPartialMatchesTitle.getText();
					log.add( "The Actual title is : " + actTitle);
					if(actTitle.contains(pgetit))
					{
						Pass("The Caption of the Address Verification matches with the expected one.",log);
					}
					else
					{
						Fail("There is mismatch in the Page Caption. Please Check.",log);
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					Pass("The user is in the Address Verification page.");
					log.add( "The Expected title was : " + pgetit);
					String actTitle = addressVerificationMatchesTitle.getText();
					log.add( "The Actual title is : " + actTitle);
					if(actTitle.contains(pgetit))
					{
						Pass("The Caption of the Address Verification matches with the expected one.",log);
					}
					else
					{
						Fail("There is mismatch in the Page Caption. Please Check.",log);
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					Pass("The user is in the Address Verification page.");
					log.add( "The Expected title was : " + pgetit);
					String actTitle = addressVerificationNoMatchesTitle.getText();
					log.add( "The Actual title is : " + actTitle);
					if(actTitle.contains(pgetit))
					{
						Pass("The Caption of the Address Verification matches with the expected one.",log);
					}
					else
					{
						Fail("There is mismatch in the Page Caption. Please Check.",log);
					}
				}
				else
				{
					Fail("The user is not in the Address Verification Page. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 92 Shipping Issue ." + e.getMessage());
				Exception(" BRM - 92 Shipping Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}

	/* Chk - 93 Veriy that Address Verification page should be shown after adding new shipping address or updating the existing address with the list of suggested address.*/
	public void EditShipShippingEditAddressSuggestionList()
	{
		ChildCreation(" Chk - 93 Veriy that Address Verification page should be shown after adding new shipping address or updating the existing address with the list of suggested address.");
		if(chkPageLoaded==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
				if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					BNBasicfeature.scrolldown(addressSuggestionFirstAddress,driver);
					if(BNBasicfeature.isListElementPresent(checkoutAddressVerificationPossibleSuggestion))
					{
						log.add("The Address Suggestion List is displayed for the user entered address.");
						for(int i = 1; i<=checkoutAddressVerificationPossibleSuggestion.size();i++)
						{
							getMatchesAddressDetails(i);
							log.add("The " + i + " Suggested Address details : " + suggeAddreDet) ;
							if(suggeAddreDet.isEmpty())
							{
								Fail("The Suggested address details is empty.");
							}
							else
							{
								Pass(" The " + i + " Address Suggestion is displayed for the user entered address.");
							}
						}
					}
					else
					{
						Fail("No Address Suggestion List is displayed for the user entered ");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					BNBasicfeature.scrolldown(addressSuggestionFirstAddress,driver);
					if(BNBasicfeature.isListElementPresent(checkoutAddressVerificationPossibleSuggestion))
					{
						log.add("The Address Suggestion List is displayed for the user entered address.");
						for(int i = 1; i<=checkoutAddressVerificationPossibleSuggestion.size();i++)
						{
							getPartialMatchesAddressDetails(i);
							log.add("The " + i + " Suggested Address details : " + suggeAddreDet) ;
							if(suggeAddreDet.isEmpty())
							{
								Fail("The Suggested address details is empty.");
							}
							else
							{
								Pass(" The " + i + " Address Suggestion is displayed for the user entered address.",log);
							}
						}
					}
					else
					{
						Fail("No Address Suggestion List is displayed for the user entered ");
					} 
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					Skip("No Address Suggestion List is displayed for the user entered ");
				}
				else
				{
					Fail("The user is not in the address verification page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 93 EDIT Shipping Issue ." + e.getMessage());
				Exception(" BRM - 93 EDIT Shipping Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 94 Verify that user entered address should be displayed with edit option with the caption "Address you entered" in the address verification page as per the creative*/
	public void EditShipShippingEditAddressYouEntered()
	{
		ChildCreation(" Chk - 94 Verify that user entered address should be displayed with edit option with the caption Address you entered in the address verification page as per the creative.");
		if(chkPageLoaded==true)
		{
			try
			{
				String pageTitle = BNBasicfeature.getExcelVal("BRM573E", sheet, 4); 
				wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
				if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					if(BNBasicfeature.isElementPresent(addressVerificationAddressEnteredTitle2))
					{
						BNBasicfeature.scrolldown(addressVerificationAddressEnteredTitle2, driver);
						log.add("The User Entered Address Title is displayed.");
						String addrTitle = addressVerificationAddressEnteredTitle2.getText();
						log.add( "The Expected title was : " + pageTitle);
						log.add( "The actual title is : " + addrTitle);
						if(addrTitle.contains(pageTitle))
						{
							Pass("The User Entered Address Title matches the expected content.",log);
						}
						else
						{
							Fail("The User Entered Address Title does not matches the expected content.",log);
						}
						
						if(BNBasicfeature.isElementPresent(addressMatchesEditBtn))
						{
							Pass("The Edit button is visible for the User entered address.");
						}
						else
						{
							Fail("The Edit button is not visible for the User entered address.");
						}
					}
					else
					{
						Fail("The User Entered Address Title is not displayed.");
					}
				} 
				else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					if(BNBasicfeature.isElementPresent(addressVerificationAddressEnteredTitle1))
					{
						log.add("The User Entered Address Title is displayed.");
						BNBasicfeature.scrolldown(addressVerificationAddressEnteredTitle1, driver);
						log.add("The User Entered Address Title is displayed.");
						String addrTitle = addressVerificationAddressEnteredTitle1.getText();
						log.add( "The Expected title was : " + pageTitle);
						log.add( "The actual title is : " + addrTitle);
						if(addrTitle.contains(pageTitle))
						{
							Pass("The User Entered Address Title matches the expected content.",log);
						}
						else
						{
							Fail("The User Entered Address Title does not matches the expected content.",log);
						}
						
						if(BNBasicfeature.isElementPresent(addressPartialMatchesEditBtn))
						{
							Pass("The Edit button is visible for the User entered address.");
						}
						else
						{
							Fail("The Edit button is not visible for the User entered address.");
						}
					}
					else
					{
						Fail("The User Entered Address Title is not displayed.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					if(BNBasicfeature.isElementPresent(addressVerificationAddressEnteredTitle1))
					{
						BNBasicfeature.scrolldown(addressVerificationAddressEnteredTitle1, driver);
						log.add("The User Entered Address Title is displayed.");
						String addrTitle = addressVerificationAddressEnteredTitle1.getText();
						log.add( "The Expected title was : " + pageTitle);
						log.add( "The actual title is : " + addrTitle);
						if(addrTitle.contains(pageTitle))
						{
							Pass("The User Entered Address Title matches the expected content.",log);
						}
						else
						{
							Fail("The User Entered Address Title does not matches the expected content.",log);
						}
						
						if(BNBasicfeature.isElementPresent(addressNoMatchesEditBtn))
						{
							Pass("The Edit button is visible for the User entered address.");
						}
						else
						{
							Fail("The Edit button is not visible for the User entered address.");
						}
					}
					else
					{
						Fail("The User Entered Address Title is not displayed.");
					}
				}
				else
				{
					Fail("The Adrress Verirfication page is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 94 EDIT Shipping Issue ." + e.getMessage());
				Exception(" BRM - 94 EDIT Shipping Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}

	/* Chk - 95  Verify that on clicking the "edit" option in address verification page it should open the edit address page with prefilled address*/
	public void EditShippingAddressListEditButtonSelection()
	{
		ChildCreation(" Chk - 95  Verify that on clicking the Edit option in address verification page it should open the edit address page with prefilled address.");
		if(chkPageLoaded==true)
		{
			try
			{
				String pageTitle = BNBasicfeature.getExcelVal("Chk95", sheet, 2);
				wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
				if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					if(BNBasicfeature.isElementPresent(addressMatchesEditBtn))
					{
						BNBasicfeature.scrolldown(addressMatchesEditBtn, driver);
						Pass("The Edit is present for the user entered address.");
						//Thread.sleep(500);
						jsclick(addressMatchesEditBtn);
						//Thread.sleep(3000);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
						BNBasicfeature.scrollup(checkoutShipEditShippingAddressTitle,driver);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
						Thread.sleep(100);
						log.add( "The Expected title was : " + pageTitle);
						String actTitle = checkoutShipEditShippingAddressTitle.getText();
						log.add( "The actual title is : " + actTitle);
						if(actTitle.contains(pageTitle))
						{
							Pass("The Edit button was clicked and the user was redirected to the Edit Shipping Address Page to Edit the Address.",log);
						}
						else
						{
							Fail("The Edit button was clicked and the user was not redirected to the Edit Shipping Address Page to Edit the Address.",log);
						}
					}
					else
					{
						Fail("The Edit button is not present for the address to edit.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					BNBasicfeature.scrollup(addressVerificationPartialMatchesTitle,driver);
					wait.until(ExpectedConditions.visibilityOf(addressVerificationPartialMatchesTitle));
					if(BNBasicfeature.isElementPresent(addressPartialMatchesEditBtn))
					{
						Pass("The Edit is present for the user entered address.");
						BNBasicfeature.scrolldown(addressPartialMatchesEditBtn, driver);
						jsclick(addressPartialMatchesEditBtn);
						//Thread.sleep(3000);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
						BNBasicfeature.scrollup(checkoutShipEditShippingAddressTitle,driver);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
						Thread.sleep(100);
						log.add( "The Expected title was : " + pageTitle);
						String actTitle = checkoutShipEditShippingAddressTitle.getText();
						log.add( "The actual title is : " + actTitle);
						if(actTitle.contains(pageTitle))
						{
							Pass("The Edit button was clicked and the user was redirected to the Edit Shipping Address Page to Edit the Address.",log);
						}
						else
						{
							Fail("The Edit button was clicked and the user was not redirected to the Edit Shipping Address Page to Edit the Address.",log);
						}
					}
					else
					{
						Fail("The Edit button is not Visible  / Present for the entered Address.");
					} 
				}
				else if(BNBasicfeature.isElementPresent(addressNoMatchesEditBtn))
				{
					Pass("The Edit is present for the user entered address.");
					Thread.sleep(500);
					jsclick(addressNoMatchesEditBtn);
					//Thread.sleep(3000);
					wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
					BNBasicfeature.scrollup(checkoutShipEditShippingAddressTitle,driver);
					wait.until(ExpectedConditions.visibilityOf(checkoutShipEditShippingAddressTitle));
					Thread.sleep(100);
					log.add( "The Expected title was : " + pageTitle);
					String actTitle = checkoutShipEditShippingAddressTitle.getText();
					log.add( "The actual title is : " + actTitle);
					if(actTitle.contains(pageTitle))
					{
						Pass("The Edit button was clicked and the user was redirected to the Edit Shipping Address Page to Edit the Address.",log);
					}
					else
					{
						Fail("The Edit button was clicked and the user was not redirected to the Edit Shipping Address Page to Edit the Address.",log);
					}
				}
				else
				{
					Fail("The Edit button is not found.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 95 Issue ." + e.getMessage());
				Exception(" BRM - 95 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 91 Verify that while editing the shipping details and selecting save ,the updated details must be used for future for the registered user */
	/* Chk - 96 Verify that onselecting "use this address" in address verification page then it should redirect to the "checkout" page*/
	public void EditShippingAddressListModifyAddressReverfication()
	{
		ChildCreation(" Chk - 96 Verify that onselecting Use This Address in address verification page then it should redirect to the Checkout page.");
		boolean chk91 = false;
		boolean loaddPage  = false;
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(checkoutShipEditShippingAddressTitle))
				{
					BNBasicfeature.scrolldown(stAddress,driver);
					jsclick(addressEditSubmitBtn);
					wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
					if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
					{
						BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
						Random r = new Random();
						int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
						if(sel<1)
						{
							sel = 1;
						}
						
						getMatchesAddressDetails(sel);
						String addr = suggeAddreDet;
						log.add("The randomly selected address is : " + addr);
						WebElement usetbtn = useThiBtn;
						jsclick(usetbtn);
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						Thread.sleep(250);
						if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
						{
							Pass("The user selected address is saved and the user is navigated to the Checkout page.");
							loaddPage = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle); 
							if(loaddPage==true)
							{
								wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
								if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
								{
									for(int i = 1; i<=checkoutShipSavedAddressList.size(); i++)
									{
										getSavedShipAddressDetails(i);
										if(addr.contains(savedShipAddress))
										{
											chk91 = true;
											break;
										}
										else
										{
											continue;
										}
									}
								}
								else
								{
									Fail("The Saved address list is not displayed.");
								}
							}
							else
							{
								Fail("The User failed to load the Saved Addres List Page.");
							}
						}
						else
						{
							Fail("There is something wrong. The user is not navigated to the Checkout page.");
						}
					}
					else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
					{
						BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
						Random r = new Random();
						int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
						if(sel<1)
						{
							sel = 1;
						}
						getPartialMatchesAddressDetails(sel);
						String addr = suggeAddreDet;
						log.add("The randomly selected address is : " + addr);
						WebElement usthisbtn = useThiBtn;
						jsclick(usthisbtn);
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						Thread.sleep(250);
						if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
						{
							Pass("The user selected address is saved and the user is navigated to the Checkout page.");
							loaddPage = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle); 
							if(loaddPage==true)
							{
								wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
								if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
								{
									for(int i = 1; i<=checkoutShipSavedAddressList.size(); i++)
									{
										getSavedShipAddressDetails(i);
										if(savedShipAddress.contains(addr))
										{
											chk91 = true;
											break;
										}
										else
										{
											continue;
										}
									}
								}
								else
								{
									Fail("The Saved address list is not displayed.");
								}
							}
							else
							{
								Fail("The User failed to load the Saved Addres List Page.");
							}
						}
						else
						{
							Fail("There is something wrong. The user is not navigated to the Checkout page.");
						}
					}
					else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
					{
						if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
						{
							String addr = suggeAddreDet;
							jsclick(checkoutUseAsEnteredBtn);
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							Thread.sleep(250);
							if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
							{
								Pass("The user selected address is saved and the user is navigated to the Checkout page.");
								loaddPage = ldPage(checkoutShipEditBtn, checkoutShipSavedAddressListPageTitle); 
								if(loaddPage==true)
								{
									wait.until(ExpectedConditions.visibilityOf(checkoutShipSavedAddressListPageTitle));
									if(BNBasicfeature.isListElementPresent(checkoutShipSavedAddressList))
									{
										for(int i = 1; i<=checkoutShipSavedAddressList.size(); i++)
										{
											getSavedShipAddressDetails(i);
											if(addr.contains(savedShipAddress))
											{
												chk91 = true;
												break;
											}
											else
											{
												continue;
											}
										}
									}
									else
									{
										Fail("The Saved address list is not displayed.");
									}
								}
								else
								{
									Fail("The User failed to load the Saved Addres List Page.");
								}
							}
							else
							{
								Fail("There is something wrong. The user is not navigated to the Checkout page.");
							}
						}
						else
						{
							Fail( "The Use as Entered Button is not displayed.");
						}
					}
					else
					{
						Fail("There is something wrong.Please Check.");
					}
				}
				else
				{
					Fail("The user is not in the Edit Shipping Address Page.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 96 Issue ." + e.getMessage());
				Exception(" BRM - 96 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 91 Verify that while editing the shipping details and selecting save ,the updated details must be used for future for the registered user */
		ChildCreation(" Chk - 91 Verify that while editing the shipping details and selecting save ,the updated details must be used for future for the registered user");
		if(chkPageLoaded==true)
		{
			try
			{
				if(chk91==true)
				{
					Pass( " The Address is saved for future reference");
				}
				else
				{
					Fail( " The Added Address is not saved for future reference");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 91 Issue ." + e.getMessage());
				Exception(" BRM - 91 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 82 Verify that the address verfication page is shown while selecting save & continue after entering valid date in all fields*/
	public void checkoutAddShippingAddAddress()
	{
		ChildCreation(" Chk - 82 Verify that the address verfication page is shown while selecting save & continue after entering valid date in all fields.");
		if(chkPageLoaded==true)
		{
			try
			{
				String firstName = BNBasicfeature.getExcelVal("Chk82", sheet, 13); 
				String lastName = BNBasicfeature.getExcelVal("Chk82", sheet, 14);
				String staddress = BNBasicfeature.getExcelVal("Chk82", sheet, 16);
				String cty = BNBasicfeature.getExcelVal("Chk82", sheet, 18);
				String zipcde = BNBasicfeature.getExcelNumericVal("Chk82", sheet, 20);
				String cntcno = BNBasicfeature.getExcelNumericVal("Chk82", sheet, 21);
				String pageTitle = BNBasicfeature.getExcelVal("Chk82", sheet, 12);
				
				if(BNBasicfeature.isElementPresent(checkoutShipSavedAddressListPageTitle))
				{
					log.add("The user is navigated to the Select a Saved Shipping Address Page.");
					if(BNBasicfeature.isElementPresent(checkoutShipAddAddresslink))
					{
						log.add("The Add a Shipping Address link is found.");
						jsclick(checkoutShipAddAddresslink);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipAddShippingAddressTitle));
						Thread.sleep(100);
						log.add("The user is navigated to the Add a Shipping Address page.");
						clearAll();
						fName.sendKeys(firstName);
						lName.sendKeys(lastName);
						stAddress.sendKeys(staddress);
						BNBasicfeature.scrolldown(city,driver);
						city.sendKeys(cty);
						Select statesel = new Select(state);
						statesel.selectByIndex(35);
						zipCode.sendKeys(zipcde);
						contactNo.sendKeys(cntcno);
						jsclick(addressSubmitBtn);
						//Thread.sleep(3000);
						wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
						Thread.sleep(250);
						log.add("The expected title was : " + pageTitle);
						if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
						{
							log.add("The user is navigated to the Address Verification Page.");
							String actTitle = addressVerificationPartialMatchesTitle.getText(); 
							log.add("The actual title was : " + actTitle);
							if(actTitle.contains(pageTitle))
							{
								Pass("The user is navigated to the Address Verification Page.",log);
							}
							else
							{
								Fail("The user is not navigated to the Address Verification Page.",log);
							}
						}
						else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
						{
							log.add("The user is navigated to the Address Verification Page.");
							String actTitle = addressVerificationMatchesTitle.getText(); 
							log.add("The actual title was : " + actTitle);
							if(actTitle.contains(pageTitle))
							{
								Pass("The user is navigated to the Address Verification Page.",log);
							}
							else
							{
								Fail("The user is not navigated to the Address Verification Page.",log);
							}
						}
						else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
						{
							log.add("The user is navigated to the Address Verification Page.");
							String actTitle = addressVerificationNoMatchesTitle.getText(); 
							log.add("The actual title was : " + actTitle);
							if(actTitle.contains(pageTitle))
							{
								Pass("The user is navigated to the Address Verification Page.",log);
							}
							else
							{
								Fail("The user is not navigated to the Address Verification Page.",log);
							}
						}
						else
						{
							Fail("The user is not navigated to the Address Verification Page.",log);
						}
					}
					else
					{
						Fail("The Add a Shipping Address link is not found.");
					}
				}
				else
				{
					Fail("The user is not navigated to the Select a Saved Shipping Address Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 82 Issue ." + e.getMessage());
				Exception(" BRM - 82 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 83 Verify that the checkout page must be opened on selecting use as entered in address verification page .*/
	public void ShipToMAddAddressEditAddressUsethisAddressNavigation()
	{
		ChildCreation(" Chk - 83 Verify that the checkout page must be opened on selecting use as entered in address verification page .");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
					Random r = new Random();
					int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
					if(sel<1)
					{
						sel = 1;
					}
					
					getMatchesAddressDetails(sel);
					log.add("The randomly selected address is : " + suggeAddreDet);
					WebElement usthisbtn = useThiBtn;
					jsclick(usthisbtn);
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
					{
						Pass("The user selected address is saved and the user is navigated to the Checkout page.");
						getCurrShipAddressDetails(1);
						String setcdholname = setcardHolName;
						log.add("The Created billing account name is : " + setcdholname);
						if(setcardHolName.isEmpty())
						{
							Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
						}
						else
						{
							Pass("The created address is set in the Checkout page.",log);
						}
						
						log.add("The randomly selected address is : " + suggeAddreDet);
						log.add("The Created billing address is : " + setAddress);
						if(suggeAddreDet.contains(setAddress))
						{
							Pass("The created address is set in the Checkout page.",log);
						}
						else
						{
							Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
						}
					}
					else
					{
							Fail("There is something wrong. The user is not navigated to the Checkout page.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					BNBasicfeature.scrollup(addressSuggestionFirstAddress,driver);
					Random r = new Random();
					int sel = r.nextInt(checkoutAddressVerficationUseThisAddressBtn.size());
					if(sel<1)
					{
						sel = 1;
					}
					getPartialMatchesAddressDetails(sel);
					log.add("The randomly selected address is : " + suggeAddreDet);
					WebElement usthisbtn = useThiBtn;
					jsclick(usthisbtn);
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
					{
						Pass("The user selected address is saved and the user is navigated to the Checkout page.");
						getCurrShipAddressDetails(1);
						String setcdholname = setcardHolName;
						log.add("The Created billing account name is : " + setcdholname);
						if(setcardHolName.isEmpty())
						{
							Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
						}
						else
						{
							Pass("The created address is set in the Checkout page.",log);
						}
						
						log.add("The randomly selected address is : " + suggeAddreDet);
						log.add("The Created billing address is : " + setAddress);
						if(suggeAddreDet.contains(setAddress))
						{
							Pass("The created address is set in the Checkout page.",log);
						}
						else
						{
							Fail("There is some mismatch in the created address and displayed address in the checkout page. Please check.",log);
						}
					}
					else
					{
						Fail("There is something wrong. The user is not navigated to the Checkout page.");
					} 
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
					{
						jsclick(checkoutUseAsEnteredBtn);
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						Thread.sleep(250);
						if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
						{
							Pass("The user selected address is saved and the user is navigated to the Checkout page.");
						}
						else
						{
							Fail("There is something wrong. The user is not navigated to the Checkout page.");
						}
					}
					else
					{
						Fail( "The Use as Entered Button is not displayed.");
					}
				}
				else
				{
					Fail("There is something wrong.Please Check.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 83 Edit Issue ." + e.getMessage());
				Exception(" BRM - 83 Edit Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 98 Verify that while selecting the "add a credit card" option in the edit payment section,the add a credit card option should be displayed to update the credit card details*/
	public void checkoutAddBillingSavedAddressListContents()
	{
		ChildCreation(" Chk - 98 Verify that while selecting the Add a credit card option in the edit payment section,the add a credit card option should be displayed to update the credit card details.");
		if(chkPageLoaded==true)
		{
			try
			{
				String pageName = BNBasicfeature.getExcelVal("Chk98", sheet, 2); 
				wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				if(BNBasicfeature.isElementPresent(checkoutBillEditBtn))
				{
					log.add("The Edit button in the Billing Address Container is Present.");
					//checkoutBillEditBtn.click();
					boolean lodPage = ldPage(checkoutBillEditBtn, checkoutSavedCreditCardTitle);
					if(lodPage==true)
					{
						wait.until(ExpectedConditions.visibilityOfAllElements(checkoutSavedCardList));
						wait.until(ExpectedConditions.visibilityOf(checkoutSavedPayMethods));
						wait.until(ExpectedConditions.visibilityOf(checkoutCCAddCClink));
						if(BNBasicfeature.isElementPresent(checkoutCCAddCClink))
						{
							log.add("The Add a Credit Card Link is Present and Visible.");
							jsclick(checkoutCCAddCClink);
							wait.until(ExpectedConditions.visibilityOf(checkoutCCAddCreditCardPageTitle));
							Thread.sleep(200);
							String actTit = checkoutCCAddCreditCardPageTitle.getText();
							log.add( "The Expected title was : " + pageName);
							log.add( "The Actual title is : " + actTit);
							if(actTit.contains(pageName))
							{
								Pass("The user is navigated to the Add a New Billing Address page.",log);
							}
							else
							{
								Fail("The user is not navigated to the Create New Billing Address page.",log);
							}
							
							
							jsclick(creditcardCancelBtn);
							Thread.sleep(1000);
							wait.until(
									ExpectedConditions.or(
										ExpectedConditions.visibilityOf(checkoutTopSubmitOrder1),
										ExpectedConditions.visibilityOf(checkoutTopSubmitOrder),
										ExpectedConditions.visibilityOf(checkoutShipEditBtn)
											)
										);
							wait.until(ExpectedConditions.visibilityOf(checkoutShipEditBtn));
						}
						else
						{
							Fail("The Add a Credit Card link is not displayed.");
						}
					}
					else
					{
						Fail(" The user failed to load the Saved Billing Address Page.");
					}
				}
				else
				{
					Fail(" The Edit button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 98 Issue ." + e.getMessage());
				Exception(" BRM - 98 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 102 Verify that already saved address should be displayed correctly below the "Select a billing address" section if the user already had an saved address.*/
	public void checkoutOrderAddBillingExistingShippingRecords()
	{
		ChildCreation(" Chk - 102 Verify that already saved address should be displayed correctly below the Select a Billing Address section if the user already had an saved address.");
		if(chkPageLoaded==true)
		{
			try
			{
				wait.until(
						ExpectedConditions.or(
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder1),
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder),
							ExpectedConditions.visibilityOf(checkoutShipEditBtn)
								)
							);
				wait.until(ExpectedConditions.visibilityOf(checkoutShipEditBtn));
				boolean lodPage = ldPage(checkoutBillEditBtn, checkoutSavedCreditCardTitle);
				if(lodPage==true)
				{
					wait.until(ExpectedConditions.visibilityOfAllElements(checkoutSavedCardList));
					wait.until(ExpectedConditions.visibilityOf(checkoutSavedPayMethods));
					Random r = new Random();
					int sel = r.nextInt(checkoutSavedCardList.size());
					if(sel<1)
					{
						sel = 1;
					}
					getSavedCreditCardAddressDetails(sel);
					WebElement editbtn = savedCCEditBtn;
					jsclick(editbtn);
					wait.until(ExpectedConditions.visibilityOf(checkoutcreditcardEditPageTitle));
					wait.until(ExpectedConditions.visibilityOf(ccExistingSelectBillingInfo));
					if(BNBasicfeature.isElementPresent(ccExistingSelectBillingInfo))
					{
						Pass("The Select a Saved Billing Address link is present in the Add a New Billing Address Section.");
						jsclick(ccExistingSelectBillingInfo);
						wait.until(ExpectedConditions.attributeContains(ccSelectBillingDisable, "style", "none"));
						wait.until(ExpectedConditions.attributeContains(ccSelectBillingEnable, "style", "block"));
						if(BNBasicfeature.isElementPresent(ccSelectBillingEnable))
						{
							Pass("The Select a Billing Address is displayed.");
							if(BNBasicfeature.isListElementPresent(checkoutExistingAddressList))
							{
								for(int j = 1; j<=checkoutExistingAddressList.size();j++)
								{
									String title = checkoutExistingAddressList.get(j-1).getText();
									if(title.isEmpty())
									{
										Fail("The Address List is empty.");
									}
									else
									{
										log.add("The displyed " + (j-1) + " address " + title);
										Pass("The existing address list is found and it is displayed.",log);
									}
								}
							}
							else
							{
								Fail("The existing address list is not displayed.");
							}
						}
						else
						{
							Fail("The Select a Billing Address is not displayed.");
						}
					}
					else
					{
						Fail("The Select a Saved Billing Address link is not present in the Add a New Billing Address Section.");
					}
				}
				else
				{
					Fail(" The user failed to load the Saved Billing Address Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 102 Issue ." + e.getMessage());
				Exception(" BRM - 102 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 103 Verify that user should be able to select any of the billing address in the list if the user saved more than one shipping address.*/
	public void checkoutOrderAddBillingExistingShippingRecordsSelection()
	{
		ChildCreation(" Chk - 103 Verify that user should be able to select any of the billing address in the list if the user saved more than one shipping address.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(ccExistingSelectBillingInfo))
				{
					BNBasicfeature.scrollup(ccExistingSelectBillingInfo,driver);	
				}
				
				for(int i = 1; i<=checkoutccSavedAddressFromList.size();i++)
				{
					WebElement radio = checkoutccSavedAddressFromList.get(i-1);
					BNBasicfeature.scrolldown(radio,driver);
					radio.click();
					if(radio.isEnabled())
					{
						Pass("The Saved address is selected.");
					}
					else
					{
						Fail("The Saved address is not selected.");
					}
					Thread.sleep(250);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 103 Issue ." + e.getMessage());
				Exception(" BRM - 103 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 104 Verify that while selecting the "Save and continue" button, the entered address should be updated correctly.*/
	public void checkoutOrderAddBillingExistingShippingRecordsSelectionSave()
	{
		ChildCreation(" Chk - 104 Verify that while selecting the Save and continue button, the entered address should be updated correctly.");
		if(chkPageLoaded==true)
		{
			try
			{
				Random r = new Random();
				int sel = r.nextInt(checkoutccSavedAddressFromList.size());
				if(sel < 1)
				{
					sel = 1;
				}
				BNBasicfeature.scrolldown(checkoutccSavedAddressFromList.get(sel-1),driver);
				WebElement radio = checkoutccSavedAddressFromList.get(sel-1);
				BNBasicfeature.scrolldown(radio,driver);
				radio.click();
				Thread.sleep(1000);
				if(radio.isEnabled())
				{
					log.add("The Saved address ship is selected.");
					/*WebElement addressCnt = driver.findElement(By.xpath("(//*[@class='paymentAddress'])["+sel+"]"));
					String address = addressCnt.getText();
					String[] sptaddress = StringUtils.split(address, "\n");*/
					jsclick(crediccardSubmitBtn);
					wait.until(ExpectedConditions.visibilityOf(billUpdateSuccessPage));
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(billUpdateSuccessPageCloseButton));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(billUpdateSuccessPageCloseButton))
					{
						billUpdateSuccessPageCloseButton.click();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(checkoutSavedPayMethods));
						wait.until(ExpectedConditions.visibilityOf(checkoutSavedCreditCardTitle));
						Thread.sleep(500);
						if(BNBasicfeature.isListElementPresent(checkoutSavedCardList))
						{
							Pass("The Saved Credit Card List is displayed.");
							if(BNBasicfeature.isListElementPresent(checkoutSavedCardList))
							{
								Pass("The Saved Credit Card List is displayed.");
								sel = checkoutSavedCardList.size();
								if(sel<1)
								{
									sel = 1;
								}
								getSavedCreditCardAddressDetails(sel);
								WebElement useThisPay = savedCCUseThisBtn;
								jsclick(useThisPay);
								wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
								Thread.sleep(250);
							}
						}
						else
						{
							Fail("The Saved Credit Card List is not displayed.");
						}
					}
					else
					{
						Fail("The Close button in the Bill Updated Success Page is not displayed.");
					}
				}
				else
				{
					Fail("The Address Option is not selected from the list");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 104 Issue ." + e.getMessage());
				Exception(" BRM - 104 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 109 Verify that while selecting the "Save & Continue" button after entering the required fields in the Add a Credit Card page,the page should be closed and should redirect to the checkout  page*/
	public void EditShipEditBillingCCardCreateNew()
	{
		ChildCreation(" Chk - 109 Verify that while selecting the Save & Continue button after entering the required fields in the Add a Credit Card page,the page should be closed and should redirect to the checkout  page.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(ccAdded==false)
				{
					String cdtcname =  BNBasicfeature.getExcelVal("Chk109", sheet, 18);
					String ccnum = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 17);
					String csvnum = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 19);
					String firstName = BNBasicfeature.getExcelVal("Chk109", sheet, 7);
					String lastName = BNBasicfeature.getExcelVal("Chk109", sheet, 8);
					String staddress = BNBasicfeature.getExcelVal("Chk109", sheet, 9);
					String cty = BNBasicfeature.getExcelVal("Chk109", sheet, 10);
					String zipcde = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 12);
					String cntcno = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 13);
					
					//signInExistingCustomer(sheet, parent, Child);
					
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					Thread.sleep(100);
					boolean loaPage = ldPage(checkoutBillEditBtn, checkoutSavedCreditCardTitle);
					if(loaPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(checkoutSavedCreditCardTitle));
						wait.until(ExpectedConditions.visibilityOf(checkoutSavedPayMethods));
						if(BNBasicfeature.isElementPresent(checkoutCCAddCClink))
						{
							log.add("The Add a Credit Card Link is Present and Visible.");
							jsclick(checkoutCCAddCClink);
							wait.until(ExpectedConditions.visibilityOf(checkoutCCAddCreditCardPageTitle));
							wait.until(ExpectedConditions.visibilityOf(ccNumber));
							ccNumber.sendKeys(ccnum);
							ccName.sendKeys(cdtcname);
							Select mnth = new Select(ccMonth);
							mnth.selectByIndex(1);
							Select yr = new Select(ccYear);
							yr.selectByIndex(5);
							ccCsv.sendKeys(csvnum);
							Thread.sleep(500);
							BNBasicfeature.scrolldown(fName,driver);
							fName.sendKeys(firstName);
							lName.sendKeys(lastName);
							stAddress.sendKeys(staddress);
							city.sendKeys(cty);
							Select st = new Select(state);
							st.selectByIndex(35);
							zipCode.sendKeys(zipcde);
							contactNo.sendKeys(cntcno);
							BNBasicfeature.scrolldown(crediccardSubmitBtn,driver);
							jsclick(crediccardSubmitBtn);
							wait.until(ExpectedConditions.visibilityOf(addressVerificationTitle));
							Thread.sleep(500);
							
							if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
							{
								Pass("The user is in the Address Verification page.");
								if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
								{
									log.add("The Use as Entered button is displayed.");
									jsclick(checkoutUseAsEnteredBtn);
									wait.until(ExpectedConditions.or(
											ExpectedConditions.visibilityOf(saveCreditCardError),
											ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder),
											ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
											)
									);
									if(BNBasicfeature.isElementPresent(saveCreditCardError))
									{
										BNBasicfeature.scrollup(saveCreditCardError,driver);
										Skip("The Credit Card details was not added Successfully.Please check the raised error Message." + saveCreditCardError.getText());
										System.out.println(saveCreditCardError.getText());
										if(BNBasicfeature.isElementPresent(creditcardCancelBtn))
										{
											BNBasicfeature.scrolldown(creditcardCancelBtn, driver);
											Thread.sleep(1500);
											jsclick(creditcardCancelBtn);
											wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(500);
											ccAdded = false;
											ccSuccess = "Not Added";
										}
									}
									else
									{
										wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										ccAdded=true;
										ccSuccess = "Added";
										Pass("The newly entered address is saved successfully.");
									}
								}
								else
								{
									log.add("The Use as Entered button is not displayed.");
									Fail("The newly entered address is not saved successfully.");
								}
							}
							else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
							{
								Pass("The user is in the Address Verification page.");
								if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
								{
									log.add("The Use as Entered button is displayed.");
									jsclick(checkoutUseAsEnteredBtn);
									wait.until(ExpectedConditions.or(
											ExpectedConditions.visibilityOf(saveCreditCardError),
											ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder),
											ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
											)
									);
									if(BNBasicfeature.isElementPresent(saveCreditCardError))
									{
										BNBasicfeature.scrollup(saveCreditCardError,driver);
										Skip("The Credit Card details was not added Successfully.Please check the raised error Message." + saveCreditCardError.getText());
										System.out.println(saveCreditCardError.getText());
										if(BNBasicfeature.isElementPresent(creditcardCancelBtn))
										{
											BNBasicfeature.scrolldown(creditcardCancelBtn, driver);
											Thread.sleep(1500);
											jsclick(creditcardCancelBtn);
											wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(500);
											ccAdded = false;
											ccSuccess = "Not Added";
										}
									}
									else
									{
										wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										ccAdded=true;
										ccSuccess = "Added";
										Pass("The newly entered address is saved successfully.");
									}
								}
								else
								{
									log.add("The Use as Entered button is not displayed.");
									Fail("The newly entered address is not saved successfully.");
								}
								
							}
							else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
							{
								Pass("The user is in the Address Verification page.");
								if(BNBasicfeature.isElementPresent(checkoutUseAsEnteredBtn))
								{
									log.add("The Use as Entered button is displayed.");
									jsclick(checkoutUseAsEnteredBtn);
									wait.until(ExpectedConditions.or(
											ExpectedConditions.visibilityOf(saveCreditCardError),
											ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder),
											ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
											)
									);
									if(BNBasicfeature.isElementPresent(saveCreditCardError))
									{
										BNBasicfeature.scrollup(saveCreditCardError,driver);
										Skip("The Credit Card details was not added Successfully.Please check the raised error Message." + saveCreditCardError.getText());
										System.out.println(saveCreditCardError.getText());
										if(BNBasicfeature.isElementPresent(creditcardCancelBtn))
										{
											BNBasicfeature.scrolldown(creditcardCancelBtn, driver);
											Thread.sleep(1500);
											jsclick(creditcardCancelBtn);
											wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(500);
											ccAdded = false;
											ccSuccess = "Not Added";
										}
									}
									else
									{
										wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										ccAdded=true;
										ccSuccess = "Added";
										Pass("The newly entered address is saved successfully.");
									}
								}
								else
								{
									log.add("The Use as Entered button is not displayed.");
									Fail("The newly entered address is not saved successfully.");
								}
							}
							else
							{
								Fail("The user is not in the Address Verification Page. Please Check.");
							}
						}
						else
						{
							Fail("The Add a Credit Card Link is not displayed.");
						}
					}
					else
					{
						Fail("The User failed to load the Saved Credit Card Page.");
					}
				}
				else
				{
					ccAdded = false;
					ccSuccess = "Not Added";
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 109 Issue ." + e.getMessage());
				Exception(" BRM - 109 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 77 Verify that Shipping and billing information, Delivery speed and their information, Member ship and gift cards should be displayed as per the creative in the checkout page */
	public void checkoutOrderLoyaltyContainer()
	{
		ChildCreation(" Chk - 77 Verify that Shipping and billing information, Delivery speed and their information, Member ship and gift cards should be displayed as per the creative in the checkout page.");
		if(chkPageLoaded==true)
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{	
				String tcid = "Chk77";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					String billingcaption = sheet.getRow(i).getCell(4).getStringCellValue().toString();
					String shippingcaption = sheet.getRow(i).getCell(5).getStringCellValue().toString();
					BNBasicfeature.scrollup(checkoutOrderTopTotal,driver);
					try
					{
						if(BNBasicfeature.isListElementPresent(checkoutBillandShipContainer))
						{
							log.add("The Billing and Shipping area container is Present and it is Visible.");
							int addresssize = checkoutBillandShipContainer.size();
							for(int j = 1; j <= addresssize; j++)
							{
								WebElement billshipname = checkoutBillandShipContainer.get(j-1);
								if(billshipname.getText().equals(billingcaption))
								{
									Pass("The Billing area is Present and it is Visible.");
								}
								else if(billshipname.getText().equals(shippingcaption))
								{
									Pass("The Shipping area is Present and it is Visible.");
								}
								else
								{
									Fail("The Billing or Shipping area container is not Present / Visible." + billshipname.getText());
								}
							}
						}
						else
						{
							Fail("The Billing and Shipping area container is not Present / Visible.");
						}
						
						if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
						{
							BNBasicfeature.scrolldown(checkoutMembershipContainerTitle,driver);
						}
						
						String[] value = new String[10];
						int shval = 33;
						if(BNBasicfeature.isListElementPresent(checkoutLoyaltyContainer))
						{
							if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
							{
								log.add("The expected title was : " + sheet.getRow(i).getCell(32).getStringCellValue().toString());
								if(checkoutMembershipContainerTitle.getText().contains(sheet.getRow(i).getCell(32).getStringCellValue().toString()))
								{
									log.add("The actual title is : " + checkoutMembershipContainerTitle.getText());
									Pass("The title matches the expected content.",log);
								}
								else
								{
									log.add("The actual title is : " + checkoutMembershipContainerTitle.getText());
									Fail("The title does not matches the expected content.",log);
								}
							}
							else
							{
								Fail("The Loyalty container is not found.");
							}
							for(int j = 1; j<=checkoutLoyaltyContainerTitles.size(); j++)
							{
								value[j] = sheet.getRow(i).getCell(shval).getStringCellValue().toString();
								shval++;
								String actTitle = checkoutLoyaltyContainerTitles.get(j-1).getText();
								log.add("The actual title is : " + actTitle);
								if(value[j].contains(actTitle)||(actTitle.contains(value[j])))
								{
									log.add("The Container title is " + actTitle + " and it matches the expected one.");
									Pass("The Loyalty Container title is present",log);
								}
								else
								{
									log.add("The Container title is " + actTitle + " and it matches the expected one.");
									Fail("The Loyalty Container title is not present.",log);
								}
							}
						}
						else
						{
							Fail("The Loyalty Container is not displayed.");
						}
					}
					catch(Exception e)
					{
						System.out.println(" BRM - 77 Issue ." + e.getMessage());
						Exception(" BRM - 77 Issue." + e.getMessage());
					}
				}
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 119 Verify that shipping options with delivery preferences should be displayed below the "Ship From Barnes & Noble" as per the creative in the checkout page */
	/* CHk - 136 Verify that Delivery preferences should be displayed above the products "Ship from Barnes & Nobles" and "Ship from marketplace seller".*/
	public void regUserCheckoutDeliveryPreference()
	{
		ChildCreation(" Chk - 119 Verify that shipping options with delivery preferences should be displayed below the Ship From Barnes & Noble as per the creative in the checkout page.");
		boolean chk136 = false;
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk119", sheet, 2);
				String[] Val = cellVal.split("\n");
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				if(BNBasicfeature.isElementPresent(deliveryOptionContainer))
				{
					BNBasicfeature.scrolldown(deliveryOptionContainer, driver);
					Pass("The Delivery Option Container is displayed.");
					if(BNBasicfeature.isElementPresent(deliveryOptionTitle))
					{
						String deliveryContainerTitle = deliveryOptionTitle.getText();
						log.add("The Expected Title was : " + Val[0]);
						log.add("The Actual Title is : " + deliveryContainerTitle);
						if(Val[0].contains(deliveryContainerTitle)||Val[1].contains(deliveryContainerTitle))
						{
							Pass("The actual title matches the expected title.",log);
							chk136 = true;
						}
						else
						{
							Fail("There is some mismatch in the expected title.",log);
						}
					}
					
					if(BNBasicfeature.isElementPresent(chekcoutRegisteredUserDeliveryPreferenceEditBtn))
					{
						Pass("The Edit button is present.");
						log.add("The Edit button title was : " + Val[2]);
						String actTitle = chekcoutRegisteredUserDeliveryPreferenceEditBtn.getText();
						log.add("The Actual Title is : " + actTitle);
						if(Val[2].contains(actTitle))
						{
							Pass("The actual title matches the expected title.",log);
							if(chk136==true)
							{
								chk136 = true;
							}
						}
						else
						{
							Fail("There is some mismatch in the expected title.",log);
						}
					}
					else
					{
						Fail("The Edit button is not displayed.");
					}
				}
				else
				{
					Skip( "The User failed to load the checkout page.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 596 Issue ." + e.getMessage());
				Exception(" BRM - 596 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
		
		/* CHk - 136 Verify that Delivery preferences should be displayed above the products "Ship from Barnes & Nobles" and "Ship from marketplace seller".*/
		ChildCreation(" Chk - 136 Verify that Delivery preferences should be displayed above the products Ship from Barnes & Nobles and Ship from marketplace seller. ");
		if(chkPageLoaded==true)
		{
			try
			{
				log.add( "Refer  : Chk 129 for log file details.");
				if(chk136==true)
				{
					Pass( " The Delivery Preference Container is displayed.",log);
				}
				else
				{
					Fail( " The Delivery Preference Container is not displayed.",log);
				}
			}
			catch (Exception e) 
			{
				System.out.println(e.getMessage() + " Chk 136");
				Exception(" There is something wrong. Please Check. " + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the checkout page.");
		}
	}
	
	/* Chk - 125 Verify that shipping options with delivery preferences should be displayed if the user completed the shipping and billing information details in the checkout page for the Guest Checkout.*/
	public void regUserCheckoutDeliveryPresent()
	{
		ChildCreation(" Chk - 125 Verify that shipping options with delivery preferences should be displayed if the user completed the shipping and billing information details in the checkout page for the Guest Checkout.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM597", sheet, 2);
				if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
				{
					BNBasicfeature.scrollup(checkoutOrderTopTotal, driver);
					String caption = checkoutShipEditBtn.getText();
					log.add("The Expected Value in the Buttons was : " + cellVal);
					if(caption.contains(cellVal))
					{
						log.add("The Actual Value in the Shipping Buttons is : " + caption);
						Pass("The Shipping Information section is present and the Edit button caption is displayed.");
						if(BNBasicfeature.isElementPresent(checkoutBillEditBtn))
						{
							caption = checkoutBillEditBtn.getText();
							log.add("The Actual Value in the Billing Buttons is : " + caption);
							if(caption.contains(cellVal))
							{
								Pass("The Shipping Information section is present and the Edit button caption is displayed.",log);
								BNBasicfeature.scrolldown(deliveryOptionContainer, driver);
								if(BNBasicfeature.isElementPresent(deliveryOptionContainer))
								{
									Pass("The Delivery Option container is displayed.");
								}
								else
								{
									Fail("The Delivery Option container is not displayed.");
								}
							}
							else
							{
								Fail("The Shipping Information section is not present.");
							}
						}
						else
						{
							Fail("The Shipping Information Section is not displayed.");
						}
					}
					else
					{
						Fail("The Billing or Shipping information is not dispalyed.");
					}
				}
				else
				{
					Fail("The registered user shipping edit button is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 125 Issue ." + e.getMessage());
				Exception(" BRM - 125 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 130 Verify that added products should  be displayed below the "Shipping from Barnes and noble" if the products are B&N products. */
	public void shipFrmBarnesAndNoble()
	{
		ChildCreation( " Chk - 130 Verify that added products should  be displayed below the Shipping from Barnes and noble if the products are B&N products." );
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk130", sheet, 2);
				String[] Val = cellVal.split("\n");
				int i = 0;
				boolean found = false;
				if(BNBasicfeature.isListElementPresent(cartItemTitle))
				{
					Pass( " The Cart Item Title list is displayed.");
					for(i = 0; i<cartItemTitle.size();i++)
					{
						String val = cartItemTitle.get(i).getText();
						if(val.contains(Val[0]))
						{
							found=true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(found==true)
					{
						Pass( "The Ship From Barnes and Noble Section is displayed.");
						List<WebElement> mailId = driver.findElements(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//*[@class='product__details']//*[contains(@class,'link')]"));
						
						for(int j = 0; j<mailId.size();j++)
						{
							String prdtTitle = mailId.get(j).getText();
							if(prdtTitle.isEmpty())
							{
								Fail("The Product title is empty / not displayed.");
							}
							else
							{
								Pass("The displayed product title is : " + prdtTitle);
							}
						}
					}
					else
					{
						Skip ( " The Ships from Barnes and Noble Section is not displayed.");
					}
				}
				else
				{
					Fail( " The Cart Item Title list is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 130 Issue ." + e.getMessage());
				Exception(" BRM - 130 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 127 Verify that the Saved Email must be shown near the Electronic delivery in shipping information for the digital content products . */
	public void electronicDeliveryPref()
	{
		ChildCreation( " Chk - 127 Verify that the Saved Email must be shown near the Electronic delivery in shipping information for the digital content products . " );
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk127", sheet, 2);
				String[] Val = cellVal.split("\n");
				int i = 0;
				boolean found = false;
				if(BNBasicfeature.isListElementPresent(cartItemTitle))
				{
					Pass( " The Cart Item Title list is displayed.");
					for(i = 0; i<cartItemTitle.size();i++)
					{
						BNBasicfeature.scrolldown(cartItemTitle.get(i), driver);
						String val = cartItemTitle.get(i).getText();
						if(val.contains(Val[0]))
						{
							found=true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(found==true)
					{
						Pass( "The Delivered Electronic Section is displayed.");
						//List<WebElement> mailId = driver.findElements(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//*[contains(@class,'make-it-gift__text-row')])[2]//p"));
						List<WebElement> mailId = driver.findElements(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//p"));
						for(int j = 0; j<mailId.size();j++)
						{
							String idFound = mailId.get(j).getText();
							log.add( "The Displayed value is  : " + idFound);
							if(idFound.contains(Val[1]))
							{
								found = false;
								break;
							}
							else
							{
								continue;
							}
						}
						
						if(found==false)
						{
							Pass( "The mail id is displayed in the Electronic Delivered Section.",log);
						}
						else
						{
							Fail( "The Mail id is not displayed in the Electronic Delivered Section.",log);
						}
					}
					else
					{
						Skip ( " The Delivered Electronic Section is not displayed.");
					}
				}
				else
				{
					Fail( " The Cart Item Title list is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 137 Issue ." + e.getMessage());
				Exception(" BRM - 127 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 135 Verify that the added products must be displayed below the "Delivered electronically" if the products (product type) are digital content. */
	public void deliveredElectronicProducsts()
	{
		ChildCreation( " Chk - 135 Verify that the added products must be displayed below the Delivered electronically if the products (product type) are digital content." );
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk127", sheet, 2);
				String[] Val = cellVal.split("\n");
				int i = 0;
				boolean found = false;
				if(BNBasicfeature.isListElementPresent(cartItemTitle))
				{
					Pass( " The Cart Item Title list is displayed.");
					for(i = 0; i<cartItemTitle.size();i++)
					{
						BNBasicfeature.scrolldown(cartItemTitle.get(i), driver);
						String val = cartItemTitle.get(i).getText();
						if(val.contains(Val[0]))
						{
							found=true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(found==true)
					{
						Pass( "The Ship From Barnes and Noble Section is displayed.");
						List<WebElement> mailId = driver.findElements(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//*[@class='product__details']//*[contains(@class,'link')]"));
						
						for(int j = 0; j<mailId.size();j++)
						{
							String prdtTitle = mailId.get(j).getText();
							if(prdtTitle.isEmpty())
							{
								Fail("The Product title is empty / not displayed.");
							}
							else
							{
								Pass("The displayed product title is : " + prdtTitle);
							}
						}
					}
					else
					{
						Skip ( " The Ships from Barnes and Noble Section is not displayed.");
					}
				}
				else
				{
					Fail( " The Cart Item Title list is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 135 Issue ." + e.getMessage());
				Exception(" BRM - 135 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 131 Verify that added products must be displayed below the "Ships from market seller" if the products are coming marketplace. */
	public void shipFrmMarketSellerPlace()
	{
		ChildCreation( " Chk - 131 Verify that added products must be displayed below the Ships from market seller if the products are coming marketplace." );
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk131", sheet, 2);
				String[] Val = cellVal.split("\n");
				int i = 0;
				boolean found = false;
				if(BNBasicfeature.isListElementPresent(cartItemTitle))
				{
					Pass( " The Cart Item Title list is displayed.");
					for(i = 0; i<cartItemTitle.size();i++)
					{
						BNBasicfeature.scrolldown(cartItemTitle.get(i), driver);
						String val = cartItemTitle.get(i).getText();
						if(val.contains(Val[0]))
						{
							found=true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(found==true)
					{
						Pass( "The Ship From Barnes and Noble Section is displayed.");
						List<WebElement> mailId = driver.findElements(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//*[@class='product__details']//*[contains(@class,'link')]"));
						
						for(int j = 0; j<mailId.size();j++)
						{
							String prdtTitle = mailId.get(j).getText();
							if(prdtTitle.isEmpty())
							{
								Fail("The Product title is empty / not displayed.");
							}
							else
							{
								Pass("The displayed product title is : " + prdtTitle);
							}
						}
					}
					else
					{
						Skip ( " The Ships from Market Seller Section is not displayed.");
					}
				}
				else
				{
					Fail( " The Cart Item Title list is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 131 Issue ." + e.getMessage());
				Exception(" BRM - 131 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 126 Verify that "Choose Delivery Speed" option alone should be displayed for the "Ships from marketplace seller"*/
	public void deliveryContainerSize()
	{
		ChildCreation(" Chk - 126 Verify that Choose Delivery Speed option alone should be displayed for the Ships from marketplace seller . " );
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(deliveryOptionContainerSize))
				{
					Pass( "The Delivery Option is displayed.");
					if(deliveryOptionContainerSize.size()>1)
					{
						Fail(" The delivery option is displayed for the market seller place.");
					}
					else
					{
						Pass(" The delivery option is not displayed for the market seller place.");
					}
				}
				else
				{
					Fail( "The Delivery Option is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 126 Issue ." + e.getMessage());
				Exception(" BRM - 126 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 120 Verify that while selecting the edit option, the Edit delivery preferences should be displayed as per the creative.*/
	public void regUserDeliveryPreferenceView()
	{
		ChildCreation(" Chk - 120 Verify that while selecting the edit option, the Edit delivery preferences should be displayed as per the creative.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(chekcoutRegisteredUserDeliveryPreferenceEditBtn))
				{
					BNBasicfeature.scrollup(chekcoutRegisteredUserDeliveryPreferenceEditBtn, driver);
					Thread.sleep(500);
					jsclick(chekcoutRegisteredUserDeliveryPreferenceEditBtn);
					if((BNBasicfeature.isListElementPresent(deliveryOptionDeliverySelect))&&(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryPreference)))
					{
						Pass("The Container is expanded and the details is displayed.");
					}
					else
					{
						Fail("The Container is not expanded.");
					}
				}
				else
				{
					Fail("The Edit button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 120 Issue ." + e.getMessage());
				Exception(" BRM - 120 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 121 Verify that while updating the delivery options in the checkout page,the product details and price details should be updated.*/
	public void regUserDeliveryOptionSelect()
	{
		ChildCreation(" Chk - 121 Verify that while updating the delivery options in the checkout page,the product details and price details should be updated.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(deliveryOptionDeliverySelect))
				{
					Pass("The Delivery preference list is displayed.");
					int delSize = deliveryOptionDeliverySelect.size();
					for(int i = 1; i<=delSize;i++)
					{
						String chkd = "";
						List<WebElement> ele = chekcoutRegisteredUserDeliveryPreferenceEnabled;
						String originalPrice = checkoutOrderTopPrice.getText();
						String modifiedPrice = "";
						boolean edtBtnVisible = isAttribtuePresent(registeredUserDeliveryPreferenceEditBtnVisible, "style");
						if(edtBtnVisible==true)
						{
							if(BNBasicfeature.isElementPresent(registeredUserDeliveryPreferenceEditBtnVisible))
							{
								BNBasicfeature.scrolldown(registeredUserDeliveryPreferenceEditBtnVisible, driver);
								jsclick(registeredUserDeliveryPreferenceEditBtnVisible);
								try
								{
									chkd = ele.get(i-1).getAttribute("checked");
									if(chkd!=null)
									{
										Thread.sleep(200);
										jsclick(deliveryOptionDeliverySelect.get(1));
										//deliveryOptionDeliverySelect.get(1).click();
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopPrice.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
								}
								catch(Exception e)
								{
									Thread.sleep(1000);
									jsclick(deliveryOptionDeliverySelect.get(0));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									Thread.sleep(250);
									if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
									{
										if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
										{
											Pass("The Order Total is displayed.");
											modifiedPrice = checkoutOrderTopPrice.getText();
											log.add("The Original Price was : " + originalPrice);
											log.add("The Modified Price is : " + modifiedPrice);
											if(originalPrice.equals(modifiedPrice))
											{
												Fail("The price is not updated.",log);
											}
											else
											{
												Pass("The price is updated.",log);
											}
										}
										else
										{
											Fail("The Order Total is not displayed.");
										}
									}
									else
									{
										Fail("The Top Submit Order button is not displayed.");
									}
								}
							}
							else
							{
								try
								{
									chkd = ele.get(i-1).getAttribute("checked");
									if(chkd!=null)
									{
										Thread.sleep(1000);
										jsclick(deliveryOptionDeliverySelect.get(1));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopTotalTxt.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
									else
									{
										Thread.sleep(1000);
										jsclick(deliveryOptionDeliverySelect.get(0));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopTotalTxt.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
								}
								catch(Exception e)
								{
									//deliveryOptionDeliverySelect.get(0).click();
									jsclick(deliveryOptionDeliverySelect.get(0));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									Thread.sleep(250);
									if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
									{
										if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
										{
											Pass("The Order Total is displayed.");
											modifiedPrice = checkoutOrderTopTotalTxt.getText();
											log.add("The Original Price was : " + originalPrice);
											log.add("The Modified Price is : " + modifiedPrice);
											if(originalPrice.equals(modifiedPrice))
											{
												Fail("The price is not updated.",log);
											}
											else
											{
												Pass("The price is updated.",log);
											}
										}
										else
										{
											Fail("The Order Total is not displayed.");
										}
									}
									else
									{
										Fail("The Top Submit Order button is not displayed.");
									}
								}
							}
						}
						else
						{
							if(BNBasicfeature.isElementPresent(registeredUserDeliveryPreferenceEditBtnVisible))
							{
								BNBasicfeature.scrolldown(registeredUserDeliveryPreferenceEditBtnVisible, driver);
								jsclick(registeredUserDeliveryPreferenceEditBtnVisible);
								try
								{
									chkd = ele.get(i-1).getAttribute("checked");
									if(chkd!=null)
									{
										Thread.sleep(200);
										jsclick(deliveryOptionDeliverySelect.get(0));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopPrice.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
								}
								catch(Exception e)
								{
									Thread.sleep(1000);
									jsclick(deliveryOptionDeliverySelect.get(1));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									Thread.sleep(250);
									if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
									{
										if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
										{
											Pass("The Order Total is displayed.");
											modifiedPrice = checkoutOrderTopPrice.getText();
											log.add("The Original Price was : " + originalPrice);
											log.add("The Modified Price is : " + modifiedPrice);
											if(originalPrice.equals(modifiedPrice))
											{
												Fail("The price is not updated.",log);
											}
											else
											{
												Pass("The price is updated.",log);
											}
										}
										else
										{
											Fail("The Order Total is not displayed.");
										}
									}
									else
									{
										Fail("The Top Submit Order button is not displayed.");
									}
								}
							}
							else
							{
								try
								{
									chkd = ele.get(i-1).getAttribute("checked");
									if(chkd!=null)
									{
										Thread.sleep(1000);
										jsclick(deliveryOptionDeliverySelect.get(1));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopTotalTxt.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
									else
									{
										Thread.sleep(1000);
										jsclick(deliveryOptionDeliverySelect.get(0));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
										wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
										Thread.sleep(250);
										if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
										{
											if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
											{
												Pass("The Order Total is displayed.");
												modifiedPrice = checkoutOrderTopTotalTxt.getText();
												log.add("The Original Price was : " + originalPrice);
												log.add("The Modified Price is : " + modifiedPrice);
												if(originalPrice.equals(modifiedPrice))
												{
													Fail("The price is not updated.",log);
												}
												else
												{
													Pass("The price is updated.",log);
												}
											}
											else
											{
												Fail("The Order Total is not displayed.");
											}
										}
										else
										{
											Fail("The Top Submit Order button is not displayed.");
										}
									}
								}
								catch(Exception e)
								{
									//deliveryOptionDeliverySelect.get(0).click();
									jsclick(deliveryOptionDeliverySelect.get(0));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
									wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
									Thread.sleep(250);
									if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
									{
										if(BNBasicfeature.isElementPresent(checkoutOrderTopTotalTxt))
										{
											Pass("The Order Total is displayed.");
											modifiedPrice = checkoutOrderTopTotalTxt.getText();
											log.add("The Original Price was : " + originalPrice);
											log.add("The Modified Price is : " + modifiedPrice);
											if(originalPrice.equals(modifiedPrice))
											{
												Fail("The price is not updated.",log);
											}
											else
											{
												Pass("The price is updated.",log);
											}
										}
										else
										{
											Fail("The Order Total is not displayed.");
										}
									}
									else
									{
										Fail("The Top Submit Order button is not displayed.");
									}
								}
							}
						}
					}
				}
				else
				{
					Fail("The Delivery preference list is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 121 Issue ." + e.getMessage());
				Exception(" BRM - 121 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}

	/* Chk - 123 Verify that delivery speed and other sending preferences details should be displayed as per the classic site.*/
	public void regUserDeliveryPreferenceDetails()
	{
		ChildCreation(" Chk - 123 Verify that delivery speed and other sending preferences details should be displayed as per the classic site.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk123", sheet, 2);
				String[] shipMeth = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryMethod))
				{
					Pass("The Shipping Method Preference is displayed.");
					boolean shipPref = isAttribtuePresent(regUserShipMethodPreferenceEnabled, "style");
					if(shipPref==true)
					{
						Fail("The Shipping Method is not displayed.");
					}
					else
					{
						Pass("The Shipping Preference Method is displayed.");
						if(BNBasicfeature.isElementPresent(regUserShipDeliveryPreference))
						{
							BNBasicfeature.scrolldown(regUserShipDeliveryPreference, driver);
							Thread.sleep(500);
							Pass("The Edit button in the Shipping Methods preference section is displayed.");
							jsclick(regUserShipDeliveryPreference);
							if(BNBasicfeature.isListElementPresent(deliveryOptionShipMethodsLabel))
							{
								Pass("The Shipping Methods label is displayed.");
								for(int i = 0; i<deliveryOptionShipMethodsLabel.size();i++)
								{
									String actlabel = deliveryOptionShipMethodsLabel.get(i).getText();
									String explabel = shipMeth[i];
									log.add("The Expected label was : " + shipMeth[i]);
									if(actlabel.contains(explabel))
									{
										Pass("The Expected label is found.",log);
									}
									else
									{
										Fail("The Expected label is not found.",log);
									}
								}
							}
							else
							{
								Fail("The Shipping Methods label is not displayed.");
							}
						}
						else
						{
							Fail( " The Delivery preference is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Shipping Method Preference is not dispalyed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 123 Issue ." + e.getMessage());
				Exception(" BRM - 123 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 122 Verify that user can allow to select any Delivery options under the "Choose Delivery Speed" section in the checkout page.*/
	public void regUserDeliveryPreferenceSelection()
	{
		ChildCreation(" Chk - 122 Verify that user can allow to select any Delivery options under the Choose Delivery Speed section in the checkout page.");
		if(chkPageLoaded==true)
		{
			String originalVal = "" ,CurrVal = "";
			try
			{
				if(BNBasicfeature.isElementPresent(checkoutLogo))
				{
					BNBasicfeature.scrollup(checkoutLogo, driver);
					Thread.sleep(250);
					originalVal = checkoutOrderTopPrice.getText();
					log.add("The Original Value was : " + originalVal);
				}
				BNBasicfeature.scrolldown(deliveryOptionShipMethods, driver);
				Pass("The Shipping Preference Method is displayed.");
				boolean shipPref = isAttribtuePresent(regUserShipMethodPreferenceEnabled, "style");
				if(shipPref==true)
				{
					Pass("The Shipping Method Preference is displayed.");
					Random r = new Random();
					int sel = 0;
					String labelChk = "";
					do
					{
						labelChk = null;
						sel = r.nextInt(deliveryOptionDeliveryMethod.size());
						labelChk = deliveryOptionDeliveryMethod.get(sel).getAttribute("checked");
						if(labelChk!=null)
						{
							labelChk="Not Found";
							continue;
						}
						else
						{
							WebElement radio = deliveryOptionShippingSelect.get(sel);
							jsclick(radio);
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							Thread.sleep(250);
							labelChk = "Found";
							break;
						}
					}while(labelChk.contains("Found"));
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
					{
						CurrVal = checkoutOrderTopPrice.getText();
						log.add("The Current Value is : " + CurrVal);
						if(originalVal!=CurrVal)
						{
							Pass("The user was able to successfully select the Delivery preference from the list.",log);
						}
						else
						{
							Fail("The user was able to successfully select the Delivery preference from the list.",log);
						}
					}
					else
					{
						Fail("The Top Order Submit total is not dispalyed.");
					}
				}
				else
				{
					Fail("The Shipping Preference Method is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 122 Issue ." + e.getMessage());
				Exception(" BRM - 122 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 129 Verify that edit option should be  shown near the products in checkout page to update qty,price etc.. */
	public void regUserEditOption()
	{
		ChildCreation(" Chk - 129 Verify that edit option should be  shown near the products in checkout page to update qty,price etc.");
		if(chkPageLoaded==true)
		{
			try
			{
				String title = BNBasicfeature.getExcelVal("Chk129", sheet, 2);
				if(BNBasicfeature.isListElementPresent(deliveryOptionProductEdit))
				{
					Pass("The Edit button is displayed.");
					List<WebElement> ele = driver.findElements(By.xpath("(//*[@id='cartItems'])//*[@class='product__details']//*[contains(@class,'link')]"));
					for(int i = 0; i<deliveryOptionProductEdit.size();i++)
					{
						String actTitle = deliveryOptionProductEdit.get(i).getText();
						String prdtTitle = ele.get(i).getText();
						log.add("The Expected title was : " + title);
						log.add("The Actual title is : " + actTitle);
						if(title.equals(actTitle))
						{
							Pass("The Edit button is displayed for the product " + prdtTitle ,log);
						}
						else
						{
							Fail("The Edit button is not displayed for the product " + prdtTitle,log);
						}
					}
				}
				else
				{
					Fail("The Edit button is not displayed."); 
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 129 Issue ." + e.getMessage());
				Exception(" BRM - 129 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 147 Verify that  "Available Immediately" message should be displayed for the nook product,nook devices and digital content as per the classic site .*/
	public void regUserNookPrdtAvailableText()
	{
		ChildCreation(" Chk - 147 Verify that  Available Immediately message should be displayed for the nook product,nook devices and digital content as per the classic site .");
		boolean titleFound = false;
		int i = 0;
		String actTitle = "";
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk147", sheet, 4);
				String expItem = BNBasicfeature.getExcelVal("Chk147", sheet, 5);
				if(BNBasicfeature.isListElementPresent(cartItemTitle))
				{
					Pass("The Cart Item Title list is displayed.");
					for(i = 1; i<=cartItemTitle.size();i++)
					{
						actTitle = cartItemTitle.get(i-1).getText();
						BNBasicfeature.scrolldown(cartItemTitle.get((i-1)), driver);
						if(actTitle.contains(cellVal))
						{
							titleFound = true;
							break;
						}
						else
						{
							titleFound = false;
							continue;
						}
					}
					
					if(titleFound==true)
					{
						boolean itemFound = false;
						String prdtTitle = "";
						log.add("The expected title was : " + cellVal);
						log.add("The actual title is : " + actTitle);
						Pass("The Delivered Electronically Container was found.",log);
						if(BNBasicfeature.isListElementPresent(regUserCartItemsList))
						{
							for(int j = 1; j<=regUserCartItemsList.size();j++)
							{
								itemFound=false;
								prdtTitle = driver.findElement(By.xpath("(//*[@id='cartItemsContainer']//*[@id='cartItems'])["+i+"]//strong")).getText();
								
								if(prdtTitle.contains(expItem))
								{
									itemFound = true;
									break;
								}
								else
								{
									itemFound = false;
									continue;
								}
								/*prdtTitle = driver.findElement(By.xpath("(//*[@id='cartItemsContainer']//*[@id='cartItems'])["+i+"]//*[@class='product-title']")).getText();
								if(prdtTitle.contains(expItem))
								{
									itemFound = true;
									break;
								}
								else
								{
									itemFound = false;
									continue;
								}*/
							}
							if(itemFound==true)
							{
								WebElement prdtTitleLists = driver.findElement(By.xpath("(//*[@id='cartItemsContainer']//*[@id='cartItems'])["+i+"]//*[@class='product__details']//*[contains(@class,'link')]"));
								String title = prdtTitleLists.getText();
								if(title.isEmpty())
								{
									Fail("The Available Immeditely is not displayed or it was empty.",log);
								}
								else
								{
									log.add("The displayed title was : " + title);
									Pass("The Available Immeditely is displayed.",log);
								}
							}
							else
							{
								Fail("The searched product in the container is not found.");
							}
						}
						else
						{
							Fail("The Cart Items List is not displayed.");
						}
					}
					else
					{
						Fail("The Delivered Electronically Title was not found.");
					}
				}
				else
				{
					Fail("The Cart Item Title list is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 147 Issue ." + e.getMessage());
				Exception(" BRM - 147 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 148 Verify that "Not eligible for gift-wrap or message" message must be shown for the products which has no "Make it a gift" option.*/
	public void regUserGiftMessage()
	{
		ChildCreation(" Chk - 148 Verify that Not eligible for gift-wrap or message message must be shown for the products which has no Make it a gift option.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk148", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(deliveryOptionCartItemContainer))
				{
					Pass("The Product Container list is displayed.");
					for(int i = 0; i<deliveryOptionCartItemContainer.size();i++)
					{
						BNBasicfeature.scrolldown(deliveryOptionCartItemContainer.get(i), driver);
						if(BNBasicfeature.isElementPresent(deliveryOptionCartItemContainer.get(i)))
						{
							try
							{
								String title = driver.findElement(By.xpath("(//*[@id='cartItems'])["+(i+1)+"]//*[contains(@class,'make-it-a-gift-text')]")).getText();
								log.add("The Expected title was : " + Val[0]);
								log.add("The Actual title is : " + title);
								if(title.equals(Val[0]))
								{
									Pass("The Make it as Gift matches the expected value.",log);
								}
								else
								{
									Fail("There is some mismatch in the expected value.",log);
								}
							}
							catch(Exception e)
							{
								List<WebElement> ele = driver.findElements(By.xpath("((//*[@id='cartItems'])["+(i+1)+"]//p)"));
								boolean txtFound = false;
								String tit = "";
								for(int j = 0; j<ele.size(); j++)
								{
									txtFound = false;
									tit = ele.get(j).getText();
									if(tit.contains(Val[1]))
									{
										txtFound = true;
										break;
									}
									else
									{
										txtFound = false;
									}
								}
								if(txtFound==true)
								{
									log.add("The Expected title was : " + Val[1]);
									log.add("The Actual title is : " + tit);
									if(tit.equals(Val[1]))
									{
										Pass("The Make it as Gift matches the expected value.",log);
									}
									else
									{
										Fail("There is some mismatch in the expected value.",log);
									}
								}
								else
								{
									Fail("There Not Eligible For Gift Wrap Option is displayed for the product.",log);
								}
							}
						}
						else
						{
							Fail("The Product List Container is not displayed.");
						}
					}
				}
				else
				{
					Fail("The product Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 148 Issue ." + e.getMessage());
				Exception(" BRM - 148 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 150 Verify that "No" option should be selected as default in the "Make it as a Gift" section.*/
	public void regUserMakeitasGiftContainerDefaultSelection()
	{
		ChildCreation(" Chk - 150 Verify that No option should be selected as default in the Make it as a Gift section.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(regUserCartItemsList))
				{
					Pass("The Cart Item list is displayed.");
					if(BNBasicfeature.isListElementPresent(deliveryOptionMakeitasGift))
					{
						Pass("The Cart Item Make it as Gift is displayed.");
						for(int i = 0; i<deliveryOptionMakeitasGift.size();i++)
						{
							BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(i), driver);
							WebElement ele = deliveryOptionMakeitasGift.get(i);
							try
							{
								int j=0;
								List<WebElement> radioLabelType = driver.findElements(By.xpath("(//*[contains(@class,'make-it-a-gift-select')])["+(i+1)+"]//*[@type='radio']"));
								boolean bValue = false;
								for(j = 0;j<radioLabelType.size();j++)
								{
									bValue = radioLabelType.get(j).isSelected();
									if(bValue==true)
									{
										break;
									}
									else
									{
										bValue = false;
										continue;
									}
								}
								
								if(bValue==true)
								{
									WebElement radioLabel = driver.findElement(By.xpath("((//*[@id='cartItemsContainer']//*[@id='cartItems']//*[contains(@class,'make-it-a-gift-select')])["+(i+1)+"]//*[@class='radio__text'])["+(j+1)+"]"));
									log.add("The Radio button label is : " + radioLabel.getText());
									if(radioLabel.getText().contains("No"))
									{
										Pass("The radio button is checked by default.",log);
									}
									else
									{
										Fail("The radio button is not selected.",log);
									}
								}
								else
								{
									Fail("The radio button is not selected.",log);
								}
							}
							catch(Exception e)
							{
								System.out.println(e.getMessage());
								boolean chkdattr = ele.getAttribute("id").contains("Yes");
								Exception(" There is something wrong. Please Check. The No option is not selected by default." + chkdattr);
							}
						}
					}
					else
					{
						Fail("The Cart Item Make it as Gift is not displayed.");
					}
				}
				else
				{
					Fail("The Cart Item list is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 150 Issue ." + e.getMessage());
				Exception(" Chk - 150 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 151 Verify that clicking on Yes expands and displays a check box(Gift Wrap this item for $3.99.) and Add a Free Gift Message textbox field.*/
	/* Chk - 149 Verify that while selecting the Make it as a Gift option,the Yes or No options should be displayed.  */
	public void regUserMakeitasGiftContainerExpandDetails()
	{
		ChildCreation(" Chk - 151 Verify that clicking on Yes expands and displays a check box(Gift Wrap this item for $3.99.) and Add a Free Gift Message textbox field.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM977", sheet, 2);
				if(BNBasicfeature.isListElementPresent(regUserCartItemsList))
				{
					Pass("The Cart Item list is displayed.");
					if(BNBasicfeature.isListElementPresent(deliveryOptionMakeitasGift))
					{
						Pass("The Cart Item Make it as Gift is displayed.");
						WebElement ele = deliveryOptionMakeitasGift.get(0);
						BNBasicfeature.scrolldown(ele, driver);
						Random r = new Random();
						int low = 1, high = deliveryOptionMakeitasGift.size();
						gftsel = r.nextInt(high-low)+low;
						if(gftsel<1)
						//if(gftsel>0)
						{
							gftsel = 1;
						}
						
						BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get((gftsel-1)), driver);
						List<WebElement> ele1 = ele.findElements(By.xpath("(//*[contains(@class,'make-it-a-gift-select')])["+gftsel+"]//*[contains(@class,'circle')]"));
						boolean bValue = false;
						for(int j = 0;j<ele1.size();j++)
						{
							bValue = ele1.get(j).isSelected();
							if(bValue==false)
							{
								jsclick(ele1.get(j));
								//ele1.get(j).click();
								wait.until(ExpectedConditions.attributeContains(giftMessageContainer.get(gftsel-1), "style", "table"));
								Thread.sleep(200);
								break;
							}
							else
							{
								bValue = false;
								continue;
							}
						}
						
						String gifrContainer = giftMessageContainer.get(gftsel-1).getAttribute("style");
						//String giftContainer = ele.findElement(By.xpath("(//*[@class='gift-message-form-container'])["+sel+"]")).getAttribute("style");
						if(gifrContainer.contains("table"))
						{
							Pass("The Gift Container is displayed.");
							try
							{
								getGiftContainerDetails();
								if(chkBoxOk==true)
								{
									WebElement gftChkBox = giftChkBox;
									if(BNBasicfeature.isElementPresent(gftChkBox))
									{
										Pass("The Gift wrap Checkbox field is displayed.");
										gftChkBox.click();
										gftChkBox.click();
										if(BNBasicfeature.isElementPresent(giftMessageContainerEnable.get(0)))
										{
											String giftContainer = giftMessageContainerEnable.get(0).getAttribute("style");
											if(giftContainer.contains("table"))
											{
												Pass("The Gift Container is displayed.");
											}
											else
											{
												Fail("The Gift Container is not displayed.");
											}
										
											String giftContainerMessage = giftChkBox.getText();
											log.add("The Expected Value was  : " + cellVal);
											log.add("The Actual Value is : " + giftContainerMessage);
											
											if(giftContainerMessage.contains(cellVal))
											{
												Pass("The Expected and actual value matches.",log);
											}
											else
											{
												Fail("There is somemimatch in the expected value.",log);
											}
											
											WebElement gftTextarea = giftMessageTextboxContainer.get(gftsel-1);
											try
											{
												giftMessageTextboxContainer.get(gftsel-1).click();
												if(BNBasicfeature.isElementPresent(gftTextarea))
												{
													Pass("The Gift Message Container is displayed.");
												}
												else
												{
													Fail("The Gift Message Container is not displayed.");
												}
											}
											catch(Exception e)
											{
												Fail("The Gift Message Container is not displayed.");
											}
										}
										else
										{
											Fail( "The Gift Wrap text area is not displayed.");
										}
									}
									else
									{
										log.add("The Gift wrap appears not to have gift wrap option.");
										try
										{
											gftChkBox.click();
											Fail("The Gift wrap Checkbox container is displayed.");
										}
										catch(Exception e)
										{
											Pass("The Gift wrap Checkbox field is not displayed.");
											WebElement gftTextarea = giftMessageTextboxContainer.get(gftsel-1);
											if(BNBasicfeature.isElementPresent(gftTextarea))
											{
												Pass("The Gift Message Container is displayed.");
											}
											else
											{
												Fail("The Gift Message Container is not displayed.");
											}
											
											try
											{
												jsclick(gftTextarea);
												if(BNBasicfeature.isElementPresent(gftTextarea))
												{
													Pass("The Gift Message Container is displayed.");
												}
												else
												{
													Fail("The Gift Message Container is not displayed.");
												}
											}
											catch(Exception e1)
											{
												Fail("The Gift Message Container is not displayed.");
											}
										}
									}
								}
								else
								{
									Skip("The Gift wrap Checkbox is not displayed.");
									log.add("The Gift wrap appears not to have gift wrap option.");
									try
									{
										giftChkBox.click();
										Fail("The Gift wrap Checkbox container is displayed.");
									}
									catch(Exception e)
									{
										Pass("The Gift wrap Checkbox field is not displayed.");
										WebElement gftTextarea = giftMessageTextboxContainer.get(gftsel-1);
										if(BNBasicfeature.isElementPresent(gftTextarea))
										{
											Pass("The Gift Message Container is displayed.");
										}
										else
										{
											Fail("The Gift Message Container is not displayed.");
										}
										
										try
										{
											gftTextarea.click();
											if(BNBasicfeature.isElementPresent(gftTextarea))
											{
												Pass("The Gift Message Container is displayed.");
											}
											else
											{
												Fail("The Gift Message Container is not displayed.");
											}
										}
										catch(Exception e1)
										{
											Fail("The Gift Message Container is not displayed.");
										}
									}
								}
							}
							catch(Exception e)
							{
								Skip("The Gift wrap Checkbox is not displayed.");
							}
						}
						else
						{
							Fail("The Gift Container is not displayed.");
						}
					}
					else
					{
						Fail( "The Make it as a Gift Option is not displayed.");
					}
				}
				else
				{
					Fail("The Cart Item Make it as Gift is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 151 Issue ." + e.getMessage());
				Exception(" Chk - 151  Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 149 Verify that while selecting the Make it as a Gift option,the Yes or No options should be displayed.  */
		ChildCreation( " Chk - 149 Verify that while selecting the Make it as a Gift option,the Yes or No options should be displayed. ");
		if(chkPageLoaded==true)
		{
			try
			{
				List<WebElement> radio = driver.findElements(By.xpath("(//*[contains(@class,'make-it-a-gift-select')])["+gftsel+"]//*[@class='radio']"));
				for(int k = 0; k<radio.size(); k++)
				{
					Thread.sleep(100);
					boolean displayed = radio.get(k).isDisplayed();
					Thread.sleep(100);
					String val = driver.findElement(By.xpath("((//*[contains(@class,'make-it-a-gift-select')])["+gftsel+"]//*[@class='radio__text'])["+(k+1)+"]")).getText();
					if(displayed==true)
					{
						Pass( "The Radio button " + val + " is displayed.");
					}
					else
					{
						Fail( "The Radio button " + val + " is not displayed.");
					}
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 149 Issue ." + e.getMessage());
				Exception(" Chk - 149 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 154 Verify that default text "Add a Free Gift Message..." is displayed in Add a Free Gift Message field.*/
	public void regUserMakeitasGiftContainerDefaultText()
	{
		ChildCreation(" Chk - 154 Verify that default text Add a Free Gift Message... is displayed in Add a Free Gift Message field.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk154", sheet, 3);
				if(BNBasicfeature.isElementPresent(giftMessageDefaultText.get(gftsel-1)))
				{
					String placeHolderActualValue = giftMessageDefaultText.get(gftsel-1).getAttribute("placeholder");
					log.add("The Expected Value was : " + cellVal);
					log.add("The Actual Value is : " + placeHolderActualValue);
					if(cellVal.contains(placeHolderActualValue))
					{
						Pass("The Placeholder value matches with the expected one.",log);
					}
					else
					{
						Fail("The Placeholder value mismatches with the expected one.",log);
					}
				}
				else
				{
					Fail("The Place holder is not found.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 154 Issue ." + e.getMessage());
				Exception(" Chk - 154 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 155 Verify that user can able to enter maximum of 250 characters and minimum of 0 characters in Gift Message(250/250) field.*/
	public void regUserMakeitasGiftContainerDefaultTextLengthValidation()
	{
		ChildCreation(" Chk - 155 Verify that user can able to enter maximum of 250 characters and minimum of 0 characters in Gift Message(250/250) field.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk155", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(giftMessageContainerEnable.get(0)))
				{
					Pass("The Gift Container is displayed.");
					WebElement giftChkBoxMessageContainer = giftMessageDefaultText.get(gftsel-1);
					if(BNBasicfeature.isElementPresent(giftChkBoxMessageContainer))
					{
						Pass("The Gift Message Container is displayed.");
						for(int i=0;i<Val.length;i++)
						{
							giftChkBoxMessageContainer.click();
							giftChkBoxMessageContainer.clear();
							giftChkBoxMessageContainer.sendKeys(Val[i]);
							Thread.sleep(100);
							log.add("The Value from the Excel file was : " + Val[i] + ", and its length was " + Val[i].length());
							if((giftChkBoxMessageContainer.getAttribute("value").length()>=0)&&(giftChkBoxMessageContainer.getAttribute("value").length()<=250))
							{
								log.add("The Value currently in the Gift message Container is : " + giftChkBoxMessageContainer.getAttribute("value")  + ", and its length was " + giftChkBoxMessageContainer.getAttribute("value").length());
								Pass("The message container field accepts value less than or equal to 250 characters.",log);
							}
							else
							{
								log.add("The Value currently in the Gift message Container is : " + giftChkBoxMessageContainer.getAttribute("value")  + ", and its length was " + giftChkBoxMessageContainer.getAttribute("value").length());
								Fail("The message container field accepts value greater than 250 characters.",log);
							}
							giftChkBoxMessageContainer.clear();
						}
					}
					else
					{
						Fail("The Gift Message Container is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Container is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 155 Issue ." + e.getMessage());
				Exception(" Chk - 155 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 156 Verify that Apply button should be displayed below the Add a Free Gift Message field.*/
	public void regUserMakeitasGiftContainerApplyButton()
	{
		ChildCreation(" Chk - 156 Verify that Apply button should be displayed below the Add a Free Gift Message field.");
		if(chkPageLoaded==true)
		{
			try
			{
				String expVal  = BNBasicfeature.getExcelVal("Chk156", sheet, 2);
				String expColor = BNBasicfeature.getExcelVal("Chk156", sheet, 3);
				if(BNBasicfeature.isElementPresent(giftMessageContainerEnable.get(0)))
				{
					Pass("The Gift Container is displayed.");
					WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
					if(BNBasicfeature.isElementPresent(applyButton))
					{
						Pass("The Apply Button is displayed.");
						String cssVal = applyButton.getCssValue("background-color");
						Color colorhxcnvt = Color.fromString(cssVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The Expected color was : " + expVal);
						log.add("The Actual color is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							Pass("The Apply button color matches with the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches with the expected one.",log);
						}
						
						log.add("The Expected button caption was : " + expVal);
						log.add("The Actual button caption is : " + applyButton.getAttribute("value"));
						if(applyButton.getAttribute("value").equals(expVal))
						{
							Pass("The button caption matches with the expected one.",log);
						}
						else
						{
							Fail("The button caption does not matches with the expected one.",log);
						}
					}
					else
					{
						Fail("The Apply Button is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Container is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 156 Issue ." + e.getMessage());
				Exception(" Chk - 156 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 152 Verify that user can enable or disable the Gift Wrap this item for $3.99 checkbox.*/
	/* Chk - 153 Verify that on enabling the Gift Wrap this item for $3.99 checkbox, it is highlighted. */
	public void regUserMakeitasGiftContainerCheckboxEnableDisable()
	{
		ChildCreation(" Chk - 152 Verify that user can enable or disable the Gift Wrap this item for $3.99 checkbox and it is highlighted.");
		boolean child = false;
		if(chkPageLoaded==true)
		{
			try
			{
				//String cellVal = BNBasicfeature.getExcelVal("BRM978", sheet, 3);
				if(BNBasicfeature.isElementPresent(giftMessageContainerEnable.get(0)))
				{
					try
					{
						getGiftContainerDetails();
						WebElement gftChkBox = giftChkBox;
						if(chkBoxOk==true)
						{
							WebElement giftContainerChkBox = driver.findElement(By.xpath("(//*[contains(@class,'gift')][contains(@style,'table')]//*[@id='giftWrapId'])"));
							boolean chkd = giftContainerChkBox.getAttribute("aria-checked").contains("true");
							if(chkd==true)
							{
								gftChkBox.click();
								chkd = giftContainerChkBox.getAttribute("aria-checked").contains("true");
							}
							else
							{
								int count=0;
								do
								{
									gftChkBox.click();
									chkd = giftContainerChkBox.getAttribute("aria-checked").contains("true");
									/*Thread.sleep(1000);
									WebElement giftContainerText = driver.findElement(By.xpath("((//*[contains(@class,'make-it-gift__text-row')][contains(@style,'table')])//*[@class='checkbox__box'])"));
									String cssVal = giftContainerText.getCssValue("background");
									System.out.println(cssVal);
									Color colorhxcnvt = Color.fromString(cssVal);
									String hexCode = colorhxcnvt.asHex();*/
									if(chkd==true)
									{
										Pass("The Value is checked");
										/*if(hexCode.contains(cellVal))
										{
											Pass("The Text is highlighted.");
											child = true;
										}
										else
										{
											Fail("The Text is not highlighted.");
										}*/
									}
									else if(chkd==false)
									{
										Pass("The Value is unchecked");
										child = true;
									}
									else
									{
										Fail("There is some thing wrong.The checkbox is not in any state.");
									}
									count++;
								}while(count<=2);
							}
						}
						else
						{
							Pass("The Gift Checkobox is not displayed.");
							child = true;
						}
					}
					catch(Exception e)
					{
						System.out.println(e.getMessage());
						Pass("The Gift Checkobox is not displayed.");
						child = true;
					}
				}
				else
				{
					Fail( "The Gift Mesage Container is not enabled.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 152 Issue ." + e.getMessage());
				Exception(" Chk - 152 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 153 Verify that on enabling the Gift Wrap this item for $3.99 checkbox, it is highlighted. */
		ChildCreation(" Chk - 153 Verify that on enabling the Gift Wrap this item for $3.99 checkbox, it is highlighted.");
		if(chkPageLoaded==true)
		{
			if(child==true)
			{
				Pass("The Checkbox is highlighted.");
			}
			else
			{
				Fail("The Checkbox is not highlighted.");
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 157 Verify that entering some text in Add a Free Gift Message field and clicking on apply should display the gift message with edit option, 
	/* BRM - 987 Verify that on enabling "Gift Wrap this item for $3.99" and clicking on Apply should update the order summary with Gift wrap and its amount value and Order total must be updated accordingly*/
	public void regUserMakeitasGiftContainerApplyGiftCard()
	{
		ChildCreation(" Chk - 157 Verify that entering some text in Add a Free Gift Message field and clicking on apply should display the gift message with edit option,the order summary with Gift wrap and its amount value and Order total must be updated accordingly.");
		boolean child = false;
		
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(checkoutLogo))
				{
					BNBasicfeature.scrollup(checkoutLogo, driver);
				}
				
				String Total = checkoutOrderTopPrice.getText().replace("$", "");
				Float prevTotal = Float.parseFloat(Total);
				String expEditButton = BNBasicfeature.getExcelVal("Chk157", sheet, 2);
				String giftMessage = BNBasicfeature.getExcelVal("Chk157", sheet, 3);
				WebElement giftChkBoxMessageContainer = giftMessageDefaultText.get(gftsel-1);
				if(BNBasicfeature.isElementPresent(giftChkBoxMessageContainer))
				{
					BNBasicfeature.scrolldown(giftChkBoxMessageContainer, driver);
					Pass("The Gift Message Container is displayed.");
					giftChkBoxMessageContainer.click();
					giftChkBoxMessageContainer.clear();
					giftChkBoxMessageContainer.sendKeys(giftMessage);
					
					try
					{
						getGiftContainerDetails();
						WebElement gftChkBox = giftChkBox;
						if(chkBoxOk==true)
						{
							WebElement giftContainerChkBox = driver.findElement(By.xpath("(//*[contains(@class,'gift')][contains(@style,'table')]//*[@id='giftWrapId'])"));
							boolean chkd =  giftContainerChkBox.isSelected();
									//giftContainerChkBox.getAttribute("aria-checked").contains("true");
							if(chkd==false)
							{
								gftChkBox.click();
								chkd = giftContainerChkBox.getAttribute("aria-checked").contains("true");
							}
							WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
							if(BNBasicfeature.isElementPresent(applyButton))
							{
								jsclick(applyButton);
								wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
								Thread.sleep(250);
								if(BNBasicfeature.isElementPresent(checkoutLogo))
								{
									BNBasicfeature.scrollup(checkoutLogo, driver);
								}
								
								Total = checkoutOrderTopPrice.getText().replace("$", "");
								Float currTotal = Float.parseFloat(Total);
								
								if(prevTotal.equals(currTotal))
								{
									Fail("The order total is not updated or modified after applying the gift wrap when there is a gift wrap.");
									child = true;
								}
								else
								{
									Pass("The order total is updated or modified after since there was gift wrap.");
									child = false;
								}
								
								getGiftContainerDetails();
								BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(gftsel-1), driver);
								WebElement gftIndicator = giftIndicator;
								if(gftOk==true)
								{
									if(BNBasicfeature.isElementPresent(gftIndicator))
									{
										Pass("The Gift Indicator is dispalyed.");
										String giftCotainerText = giftIndicator.findElement(By.xpath("//*[@class='gift-message-val']")).getText();
										log.add("The Gift Message displayed is : " + giftCotainerText);
										if(giftCotainerText.isEmpty())
										{
											Fail("The Gift message is not displayed.",log);
											child = false;
										}
										else
										{
											if(giftCotainerText.contains(giftMessage))
											{
												Pass("The Gift message is displayed.",log);
												child = true;
											}
											else
											{
												Fail("There is some mismatch in the displayed gift message");
												child = false;
											}
										}
										
										WebElement gftMessageEditbutton = giftMessageEditbutton;
										if(BNBasicfeature.isElementPresent(gftMessageEditbutton))
										{
											Pass("The Edit button is displayed in the Gift Container field.");
											if(expEditButton.equals(gftMessageEditbutton.getText()))
											{
												Pass("The Edit button caption matches.");
											}
											else
											{
												Fail("The Edit button caption does not matches.");
											}
										}
										else
										{
											Fail("The Edit button is not displayed in the Gift Container field.");
										}
									}
									else
									{
										Fail("The Gift Indicator is not dispalyed.");
									}
								}
								else
								{
									Fail( "The Gift Message was not applied.");
								}
							}
							else
							{
								Fail("The Apply Button is not displayed.");
							}
						}
						else
						{
							Pass("The Gift Checkbox is not displayed.");
							WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
							if(BNBasicfeature.isElementPresent(applyButton))
							{
								jsclick(applyButton);
								wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
								Thread.sleep(250);
								
								if(BNBasicfeature.isElementPresent(checkoutLogo))
								{
									BNBasicfeature.scrollup(checkoutLogo, driver);
								}
								
								Total = checkoutOrderTopPrice.getText().replace("$", "");
								Float currTotal = Float.parseFloat(Total);
								
								if(prevTotal.equals(currTotal))
								{
									Pass(" The order total is not updated or modified after applying no gift wrap..");
									child = true;
								}
								else
								{
									Fail(" The order total is updated or modified after applying the no gift wrap.");
									child = false;
								}
								
								getGiftContainerDetails();
								BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(gftsel-1), driver);
								WebElement gftIndicator = giftIndicator;
								if(gftOk==true)
								{
									if(BNBasicfeature.isElementPresent(gftIndicator))
									{
										Pass("The Gift Indicator is dispalyed.");
										String giftCotainerText = gftIndicator.findElement(By.xpath("//*[@class='gift-message-val']")).getText();
										log.add("The Gift Message displayed is : " + giftCotainerText);
										if(giftCotainerText.isEmpty())
										{
											Fail("The Gift message is not displayed.",log);
											child = false;
										}
										else
										{
											if(giftCotainerText.contains(giftMessage))
											{
												Pass("The Gift message is displayed.",log);
												child = true;
											}
											else
											{
												Fail("There is some mismatch in the displayed gift message");
												child = false;
											}
										}
										
										WebElement gftMessageEditbutton = giftMessageEditbutton;
										if(BNBasicfeature.isElementPresent(gftMessageEditbutton))
										{
											Pass("The Edit button is displayed in the Gift Container field.");
											if(expEditButton.equals(gftMessageEditbutton.getText()))
											{
												Pass("The Edit button caption matches.");
											}
											else
											{
												Fail("The Edit button caption does not matches.");
											}
										}
										else
										{
											Fail("The Edit button is not displayed in the Gift Container field.");
										}
									}
									else
									{
										Fail("The Gift Indicator is not dispalyed.");
									}
								}
								else
								{
									Fail( "The Gift Message was not applied.");
								}
							}
							else
							{
								Fail("The Apply Button is not displayed.");
							}
						}
					}
					catch(Exception e)
					{
						System.out.println(e.getMessage());
						Pass("The Gift Checkbox is not displayed.");
						WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
						if(BNBasicfeature.isElementPresent(applyButton))
						{
							jsclick(applyButton);
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							Thread.sleep(250);
							
							if(BNBasicfeature.isElementPresent(checkoutLogo))
							{
								BNBasicfeature.scrollup(checkoutLogo, driver);
							}
							
							Total = checkoutOrderTopPrice.getText().replace("$", "");
							Float currTotal = Float.parseFloat(Total);
							
							if(prevTotal.equals(currTotal))
							{
								Fail(" The order total is updated or modified after applying the no gift wrap.");
								child = false;
							}
							else
							{
								Pass(" The order total is not updated or modified after applying no gift wrap..");
								child = true;
							}
							
							getGiftContainerDetails();
							BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(gftsel-1), driver);
							WebElement gftIndicator = giftIndicator;
							if(gftOk==true)
							{
								if(BNBasicfeature.isElementPresent(gftIndicator))
								{
									Pass("The Gift Indicator is dispalyed.");
									String giftCotainerText = gftIndicator.findElement(By.xpath("//*[@class='gift-message-val']")).getText();
									log.add("The Gift Message displayed is : " + giftCotainerText);
									if(giftCotainerText.isEmpty())
									{
										Fail("The Gift message is not displayed.",log);
										child = false;
									}
									else
									{
										if(giftCotainerText.contains(giftMessage))
										{
											Pass("The Gift message is displayed.",log);
											child = true;
										}
										else
										{
											Fail("There is some mismatch in the displayed gift message");
											child = false;
										}
									}
									
									WebElement gftMessageEditbutton = giftMessageEditbutton;
									if(BNBasicfeature.isElementPresent(gftMessageEditbutton))
									{
										Pass("The Edit button is displayed in the Gift Container field.");
										if(expEditButton.equals(gftMessageEditbutton.getText()))
										{
											Pass("The Edit button caption matches.");
										}
										else
										{
											Fail("The Edit button caption does not matches.");
										}
									}
									else
									{
										Fail("The Edit button is not displayed in the Gift Container field.");
									}
								}
								else
								{
									Fail("The Gift Indicator is not dispalyed.");
								}
							}
							else
							{
								Fail( "The Gift Message was not applied.");
							}
						}
						else
						{
							Fail("The Apply Button is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Gift Message Container is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 157 Issue ." + e.getMessage());
				Exception(" Chk - 157 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
		
		/* Chk - 158 Verify that on enabling "Gift Wrap this item for $3.99" and clicking on Apply should update the order summary with Gift wrap and its amount value and Order total must be updated accordingly.*/
		ChildCreation(" Chk - 158 Verify that on enabling Gift Wrap this item for $3.99 and clicking on Apply should update the order summary with Gift wrap and its amount value and Order total must be updated accordingly.");
		if(chkPageLoaded==true)
		{
			if(child==true)
			{
				Pass("The Amount is Updated.");
			}
			else
			{
				Fail("The Amount is not Updated.");
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 158 Verify that clicking on edit should allow us to edit the gift message.*/
	public void regUserMakeitasGiftContainerGiftCardEdit()
	{
		ChildCreation("Chk - 158 Verify that clicking on edit should allow us to edit the gift message.");
		if(chkPageLoaded==true)
		{
			try
			{
				getGiftContainerDetails();
				BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(gftsel-1), driver);
				WebElement gftIndicator = giftIndicator;
				if(gftOk==true)
				{
					if(BNBasicfeature.isElementPresent(gftIndicator))
					{
						WebElement gftMessageEditbutton = giftMessageEditbutton;
						if(BNBasicfeature.isElementPresent(gftMessageEditbutton))
						{
							Pass("The Edit button is displayed in the Gift Container field.");
							jsclick(gftMessageEditbutton);
							Thread.sleep(1000);
							WebElement ele = giftMessageContainerEnable.get(0);
							if(ele.getAttribute("style").contains("table"))
							{
								Pass("The Gift Message container is displayed.");
								WebElement giftChkBoxMessageContainer = giftMessageDefaultText.get(gftsel-1);
								try
								{
									WebElement noChkFieldAlert = giftChkBox;
									noChkFieldAlert.click();
									Pass("The Checkbox to gift wrap is not displayed.");
									if(BNBasicfeature.isElementPresent(giftChkBoxMessageContainer))
									{
										Pass("The Gift Message Container is displayed.");
										giftChkBoxMessageContainer.click();
										giftChkBoxMessageContainer.clear();
										WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
										if(BNBasicfeature.isElementPresent(applyButton))
										{
											applyButton.click();
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(250);
											BNBasicfeature.scrolldown(regUserCartMakeitasGiftProductTitle.get(gftsel-1), driver);
											if(BNBasicfeature.isElementPresent(giftIndicator))
											{
												Fail("The User was able to edit and delete the Gift Message.The Container is still dispalyed.");
											}
											else
											{
												Pass("The User was able to edit the Gift Message.");
											}
										}
										else
										{
											Fail("The Apply button is not displayed.");
										}
									}
									else
									{
										Fail("The Gift Message container is not displayed.");
									}
								}
								catch(Exception e)
								{
									if(BNBasicfeature.isElementPresent(giftChkBoxMessageContainer))
									{
										Pass("The Gift Message Container is displayed.");
										giftChkBoxMessageContainer.click();
										giftChkBoxMessageContainer.clear();
										WebElement applyButton = giftMessageSubmitBtn.get(gftsel-1);
										if(BNBasicfeature.isElementPresent(applyButton))
										{
											jsclick(applyButton);
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
											wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
											Thread.sleep(250);
											if(BNBasicfeature.isElementPresent(giftIndicator))
											{
												Fail("The User was able to edit and delete the Gift Message.The Container is still dispalyed.");
											}
											else
											{
												Pass("The User was able to edit the Gift Message.");
											}
										}
										else
										{
											Fail("The Apply button is not displayed.");
										}
									}
									else
									{
										Fail("The Gift Message container is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Gift Message container is not opened / displayed.");
							}
						}
						else
						{
							Fail("The Edit button is not displayed in the Gift Container field.");
						}
					}
					else
					{
						Fail("The Gift Card Message entered container is not displayed.");
					}
				}
				else
				{
					Fail( "The Gift Container is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 158 Issue ." + e.getMessage());
				Exception(" Chk - 158 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}

	/* Chk - 159 Verify that "+" button is displayed before expanding and "X" appears after expanding near the options of "Membership,gift card and coupon".*/
	public void regUserCheckoutBNMembershipPgmDrpDown()
	{
		ChildCreation(" Chk - 159 Verify that + button is displayed before expanding and X appears after expanding near the options of Membership,gift card and coupon.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(regUserMembershipContainerOpenTrigger))
				{
					BNBasicfeature.scrolldown(regUserMembershipContainerOpenTrigger, driver);
					Thread.sleep(100);
					jsclick(regUserMembershipContainerOpenTrigger);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(regUserMembershipContainerFields));
					if(BNBasicfeature.isElementPresent(regUserMembershipContainerFields))
					{
						Pass("The Membership Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerDropDownField))
						{
							Pass("The Program drop down is displayed.");
						}
						else
						{
							Fail("The Program drop down is not displayed.");
						}
					}
					else
					{
						Fail("The Membership Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserMembershipContainerCloseTrigger))
					{
						Pass("The Membership Container Close ( - ) trigger is displayed.");
					}
					else
					{
						Fail("The Membership Container Close ( - ) trigger is not displayed.");
					}
				}
				else
				{
					Fail("The Membership Container open ( + ) trigger is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 159 Issue ." + e.getMessage());
				Exception(" Chk - 159 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 174 Verify that Membership,Gift Cards & Coupons options should be displayed as per the creative in the registered checkout page.*/
	public void regUserCheckoutCouponContainerContainerDetails()
	{
		ChildCreation(" Chk - 174 Verify that Membership,Gift Cards & Coupons options should be displayed as per the creative in the registered checkout page.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk176", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(regUserLoyaltyContainer))
				{
					Pass("The Loyalty Container is displayed for the logged in User.");
					if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
					{
						Pass("The Loyalty Container title is displayed.");
						log.add("The Expected alert was : " + Val[0]);
						String title = checkoutMembershipContainerTitle.getText();
						log.add("The actual alert is : " + title);
						if(Val[0].contains(title))
						{
							Pass("The loyalty container title matches the expected content.",log);
						}
						else
						{
							Fail("The loyalty container title does not matches the expected one .",log);
						}
					}
					else
					{
						Fail("The Loyalty Container title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserMembershipContainer))
					{
						Pass("The Membership Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerLogo))
						{
							Pass("The Membership Logo is displayed.");
						}
						else
						{
							Fail("The Membership Logo is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerOpenTrigger))
						{
							Pass("The Membership Container Open Trigger + is displayed.");
						}
						else
						{
							Fail("The Membership Container Open Trigger + is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerTitle))
						{
							Pass("The Membership Container title is displayed.");
							log.add("The Expected title was : " + Val[1]);
							String title = regUserMembershipContainerTitle.getText();
							if(title.contains(Val[1]))
							{
								Pass("The MemberShip title matches the expected content.",log);
							}
							else
							{
								Fail("The MemberShip title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Membership Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Membership Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserGiftCardContainer))
					{
						Pass("The Gift Card Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserGiftCardContainerLogo))
						{
							Pass("The Gift Card Logo is displayed.");
						}
						else
						{
							Fail("The Gift Card Logo is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserGiftCardContainerOpenTrigger))
						{
							Pass("The Gift Card Container Open Trigger + is displayed.");
						}
						else
						{
							Fail("The Gift Card Container Open Trigger + is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserGiftCardContainerTitle))
						{
							Pass("The Gift Card Container title is displayed.");
							log.add("The Expected title was : " + Val[2]);
							String title = regUserGiftCardContainerTitle.getText();
							if(title.contains(Val[2]))
							{
								Pass("The Gift Card title matches the expected content.",log);
							}
							else
							{
								Fail("The Gift Card title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Gift Card Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserCouponCodeContainer))
					{
						Pass("The Coupon Code Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerLogo))
						{
							Pass("The Coupon Code Logo is displayed.");
						}
						else
						{
							Fail("The Coupon Code Logo is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerOpenTrigger))
						{
							Pass("The Coupon Code Container Open Trigger + is displayed.");
						}
						else
						{
							Fail("The Coupon Code Container Open Trigger + is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerTitle))
						{
							Pass("The Coupon Code Container title is displayed.");
							log.add("The Expected title was : " + Val[3]);
							String title = regUserCouponCodeContainerTitle.getText();
							if(title.contains(Val[3]))
							{
								Pass("The Coupon Code title matches the expected content.",log);
							}
							else
							{
								Fail("The Coupon Code title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Coupon Code Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserBookFairContainer))
					{
						Pass("The Book Fair Container is displayed.");
						
						if(BNBasicfeature.isElementPresent(regUserBookFairContainerOpenTrigger))
						{
							Pass("The Book Fair Container Open Trigger + is displayed.");
						}
						else
						{
							Fail("The Book Fair Container Open Trigger + is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserBookFairContainerTitle))
						{
							Pass("The Book Fair Container title is displayed.");
							log.add("The Expected title was : " + Val[4]);
							String title = regUserBookFairContainerTitle.getText();
							if(Val[4].contains(title))
							{
								Pass("The Book Fair title matches the expected content.",log);
							}
							else
							{
								Fail("The Book Fair title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Book Fair Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Code Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserTaxExemptContainer))
					{
						Pass("The Tax Exempt Container is displayed.");
						
						if(BNBasicfeature.isElementPresent(regUserTaxExemptContainerOpenTrigger))
						{
							Pass("The Tax Exempt Container Open Trigger + is displayed.");
						}
						else
						{
							Fail("The Tax Exempt Container Open Trigger + is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserTaxExemptContainerTitle))
						{
							Pass("The Tax Exempty Container title is displayed.");
							log.add("The Expected title was : " + Val[5]);
							String title = regUserTaxExemptContainerTitle.getText();
							if(title.contains(Val[5]))
							{
								Pass("The Tax Exempt title matches the expected content.",log);
							}
							else
							{
								Fail("The Tax Exempt title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Tax Exempt Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Tax Exempt Container is not displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container is not displayed for the logged in User.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 174 Issue ." + e.getMessage());
				Exception(" Chk - 174  Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 176 Verify that Membership, Gift card and Coupons options: "B&N Membership or B&N Educator","B&N Gift Cards" ,"Coupon Code","Bookfair ID" and "Tax Exemption" is displayed in the registered checkout page.*/
	public void regUserCheckoutCouponDetails2()
	{
		ChildCreation(" Chk - 176 Verify that Membership, Gift card and Coupons options: B&N Membership or B&N Educator,B&N Gift Cards ,Coupon Code,Bookfair ID and Tax Exemption is displayed in the registered checkout page.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk176", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(regUserLoyaltyContainer))
				{
					Pass("The Loyalty Container is displayed for the logged in User.");
					if(BNBasicfeature.isElementPresent(checkoutMembershipContainerTitle))
					{
						Pass("The Loyalty Container title is displayed.");
						log.add("The Expected alert was : " + Val[0]);
						String title = checkoutMembershipContainerTitle.getText();
						log.add("The actual alert is : " + title);
						if(Val[0].contains(title))
						{
							Pass("The loyalty container title matches the expected content.",log);
						}
						else
						{
							Fail("The loyalty container title does not matches the expected one .",log);
						}
					}
					else
					{
						Fail("The Loyalty Container title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserMembershipContainer))
					{
						Pass("The Membership Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerTitle))
						{
							Pass("The Membership Container title is displayed.");
							log.add("The Expected title was : " + Val[1]);
							String title = regUserMembershipContainerTitle.getText();
							if(Val[1].contains(title))
							{
								Pass("The MemberShip title matches the expected content.",log);
							}
							else
							{
								Fail("The MemberShip title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Membership Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Membership Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserGiftCardContainer))
					{
						Pass("The Gift Card Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserGiftCardContainerTitle))
						{
							Pass("The Gift Card Container title is displayed.");
							log.add("The Expected title was : " + Val[2]);
							String title = regUserGiftCardContainerTitle.getText();
							if(Val[2].contains(title))
							{
								Pass("The Gift Card title matches the expected content.",log);
							}
							else
							{
								Fail("The Gift Card title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Gift Card Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserCouponCodeContainer))
					{
						Pass("The Coupon Code Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerTitle))
						{
							Pass("The Coupon Code Container title is displayed.");
							log.add("The Expected title was : " + Val[3]);
							String title = regUserCouponCodeContainerTitle.getText();
							if(Val[3].contains(title))
							{
								Pass("The Coupon Code title matches the expected content.",log);
							}
							else
							{
								Fail("The Coupon Code title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Coupon Code Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserBookFairContainer))
					{
						Pass("The Book Fair Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserBookFairContainerTitle))
						{
							Pass("The Book Fair Container title is displayed.");
							log.add("The Expected title was : " + Val[4]);
							String title = regUserBookFairContainerTitle.getText();
							if(Val[4].contains(title))
							{
								Pass("The Book Fair title matches the expected content.",log);
							}
							else
							{
								Fail("The Book Fair title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Book Fair Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Code Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(regUserTaxExemptContainer))
					{
						Pass("The Tax Exempt Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserTaxExemptContainerTitle))
						{
							Pass("The Tax Exempty Container title is displayed.");
							log.add("The Expected title was : " + Val[5]);
							String title = regUserTaxExemptContainerTitle.getText();
							if(Val[5].contains(title))
							{
								Pass("The Tax Exempt title matches the expected content.",log);
							}
							else
							{
								Fail("The Tax Exempt title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Tax Exempt Container title is not displayed.");
						}
					}
					else
					{
						Fail("The Tax Exempt Container is not displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container is not displayed for the logged in User.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 176 Issue ." + e.getMessage());
				Exception(" Chk - 176  Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 175 Verify that while selecting the "+" button in the "B&N Gift Cards,B&N Kids' Club" section,the text box for Card Number , PIn should be displayed with the default text and the apply button.*/
	public void regUserCheckoutBNMembershipPgmDrpDownDefault()
	{
		ChildCreation(" Chk - 175 Verify that while selecting the + button in the B&N Gift Cards,B&N Kids' Club section,the text box for Card Number , PIn should be displayed with the default text and the apply button.");
		if(chkPageLoaded==true)
		{
			try
			{
				String defaultText = BNBasicfeature.getExcelVal("Chk160", sheet, 2);
				String cellVal = BNBasicfeature.getExcelVal("Chk160", sheet, 4);
				String[] Val = cellVal.split("\n");
				String exColor = BNBasicfeature.getExcelVal("Chk160", sheet, 3);
				if(BNBasicfeature.isElementPresent(regUserMembershipContainerFields))
				{
					Pass("The Membership Container is displayed.");
					if(BNBasicfeature.isElementPresent(regUserMembershipContainerDropDownField))
					{
						Pass("The Program drop down is displayed.");
						Select sel = new Select(regUserMembershipContainerDropDownField);
						String defaulSel = sel.getFirstSelectedOption().getText();
						log.add("The Expected Default Type : " + defaultText);
						log.add("The Actual Default Selected Option is : " + defaulSel);
						if(defaulSel.contains(defaultText))
						{
							Pass("The Membership Dropdown Default text is expected one.",log);
						}
						else
						{
							Fail("The Membership Dropdown Default text is not expected one.",log);
						}
						
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerApplyButton))
						{
							Pass("The Apply button is displayed.");
						}
						else
						{
							Fail("The Apply button is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserMembershipContainerCardNumberField))
						{
							Pass("The Card Number field is displayed.");
						}
						else
						{
							Fail("The Card Number field is not displayed.");
						}
						
						
						List<WebElement> option = driver.findElements(By.xpath("(//*[contains(@class,'loyaltyActive')]//*[@id='selectMembership']//*[@value])[position()>1]"));
						for(int i = 0; i<option.size();i++)
						{
							option.get(i).click();
							Thread.sleep(100);
							String pgmName = sel.getFirstSelectedOption().getText();
							
							if(BNBasicfeature.isElementPresent(regUserMembershipContainerApplyButton))
							{
								Pass("The Apply button is displayed for the selected " + pgmName);
								log.add("The Expected value is : " + Val[1]);
								String title = regUserMembershipContainerApplyButton.getAttribute("value");
								log.add("The Actual value is : " + title);
								if(title.contains(Val[1]))
								{
									Pass("The Apply Button title matches.",log);
								}
								else
								{
									Fail("The Apply Button title does not matches.",log);
								}
								
								String csVal = regUserMembershipContainerApplyButton.getCssValue("background-color");
								Color colorhxcnvt = Color.fromString(csVal);
								String hexCode = colorhxcnvt.asHex();
								log.add("The expected color of the button is : " + exColor);
								log.add("The actual color of the button is : " + hexCode);
								if(hexCode.equals(exColor))
								{
									Pass("The Apply button color matches the expected one.",log);
								}
								else
								{
									Fail("The Apply button color does not matches the expected one.",log);
								}
							}
							else
							{
								Fail("The Apply button is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(regUserMembershipContainerCardNumberField))
							{
								Pass("The Card Number field is displayed.");
							}
							else
							{
								Fail("The Card Number field is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(regUserMembershipContainerCardNumberLabel))
							{
								Pass("The Card Number Label is displayed for the selected " + pgmName);
								log.add("The Expected Value is : " + Val[0]);
								String title = regUserMembershipContainerCardNumberLabel.getText();
								if(title.contains(Val[0]))
								{
									Pass("The Card Number Default text matches.",log);
								}
								else
								{
									Fail("The Card Number Default text does not matches.",log);
								}
							}
							else
							{
								Fail("The Card Number Label is not displayed for the selected " + pgmName);
							}
						}
					}
					else
					{
						Fail("The Program drop down is not displayed.");
					}
				}
				else
				{
					Fail("The Membership Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 175 Issue ." + e.getMessage());
				Exception(" Chk - 175  Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 163 Verify that while selecting the "Apply" button without entering the card number and Pin details for the "B&N Gift Card,Rewards Certificates" section,the alert message should be displayed with text box highlighted.*/
	public void regUserCheckoutGiftCouponEmptyValidation()
	{
		ChildCreation(" Chk - 163 Verify that while selecting the Apply button without entering the card number and Pin details for the B&N Gift Card,Rewards Certificates section,the alert message should be displayed with text box highlighted.");
		if(chkPageLoaded==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("Chk163", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("Chk163", sheet, 3);
				String[] Val = alert.split("\n");
				
				if(BNBasicfeature.isElementPresent(regUserGiftCardContainerOpenTrigger))
				{
					Pass("The Container Open Trigger + is displayed.");
					jsclick(regUserGiftCardContainerOpenTrigger);
					wait.until(ExpectedConditions.visibilityOf(regUserGiftCardFields));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(regUserGiftCardFields))
					{
						Pass("The Coupon Code field is displayed.");
						if(BNBasicfeature.isElementPresent(regUserGiftCardCreditCardField))
						{
							Pass("The Credit Card Field in the Coupon Code is displayed.");
							if(BNBasicfeature.isElementPresent(regUserGiftCardCreditCardPinField))
							{
								Pass("The Credit Card PIN Field in the Coupon Code is displayed.");
								if(BNBasicfeature.isElementPresent(regUserGiftCardApplyButton))
								{
									Pass("The Apply button in the Gift Card is displayed.");
									jsclick(regUserGiftCardApplyButton);
									wait.until(ExpectedConditions.visibilityOf(regUserGiftCardError));
									Thread.sleep(100);
									log.add("The expected alert was : " + Val[0]);
									log.add("The expected alert was : " + Val[1]);
									String err1 = regUserGiftCardIndividualError.get(0).getText();
									String err2 = regUserGiftCardIndividualError.get(1).getText();
									log.add("The actual alert was  : " + err1);
									log.add("The actual alert was  : " + err2);
									if((err1.equals(Val[0]))&&(err2.contains(Val[1])))
									{
										Pass("The expected error matches the actual alert.",log);
									}
									else
									{
										Fail("The expected error does not match the actual alert.",log);
									}
									
									String cssVal = regUserGiftCardCreditCardField.getCssValue("border-color");
									String hexCode = BNBasicfeature.colorfinder(cssVal);
									log.add("The expected color was : " + expColor);
									log.add("The actual color is : " + hexCode);
									if(hexCode.contains(expColor))
									{
										Pass("The Credit Card number field is highlighted.",log);
									}
									else
									{
										Fail("The Credit Card number field is not highlighted.",log);
									}
									
									cssVal = regUserGiftCardCreditCardPinField.getCssValue("border-color");
									hexCode = BNBasicfeature.colorfinder(cssVal);
									log.add("The expected color was : " + expColor);
									log.add("The actual color is : " + hexCode);
									if(hexCode.contains(expColor))
									{
										Pass("The PIN number field is highlighted.",log);
									}
									else
									{
										Fail("The PIN number field is not highlighted.",log);
									}
								}
								else
								{
									Fail("The Apply button in the Gift Card is not displayed.");
								}
							}
							else
							{
								Fail("The Credit Card PIN Field in the Coupon Code is not displayed.");
							}
						}
						else
						{
							Fail("The Credit Card Field in the Coupon Code is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card field is not displayed.");
					}
					//regUserGiftCardContainerCloseTrigger.click();
					Thread.sleep(100);
				}
				else
				{
					Fail("The Container Open Trigger + is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 163 Issue ." + e.getMessage());
				Exception(" Chk - 163 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 165 Verify that while selecting the "+" button near the "Coupon Code" section,it should display a textbox for card number with default text and Apply button.*/
	public void regUserCheckoutCouponGftCpnBkfairDetails()
	{
		ChildCreation(" Chk - 165 Verify that while selecting the + button near the Coupon Code section,it should display a textbox for card number with default text and Apply button.");
		if(chkPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk165", sheet, 2);
				String applyBtn = BNBasicfeature.getExcelVal("Chk165", sheet, 4);
				String exColor = BNBasicfeature.getExcelVal("Chk165", sheet, 3);
				String[] Val = cellVal.split("\n");
				
				if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerOpenTrigger))
				{
					Pass("The Coupon Code Container Open Trigger is displayed.");
					jsclick(regUserCouponCodeContainerOpenTrigger);
					wait.until(ExpectedConditions.visibilityOf(regUserCouponCodeFields));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(regUserCouponCodeFields))
					{
						Pass("The Coupon Code Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserCouponCodeTextField))
						{
							Pass("The Coupon Code text field is displayed.");
							if(BNBasicfeature.isElementPresent(regUserCouponCodeDefaultText))
							{
								Pass("The Default text is displayed.");
								log.add("The Expected default text is : " + Val[2]);
								String title = regUserCouponCodeDefaultText.getText();
								if(title.contains(Val[2]))
								{
									Pass("The Coupon Code title matches.",log);
								}
								else
								{
									Fail("The Coupon Code title does not matches.",log);
								}
							}
							else
							{
								Fail("The Coupon Code Default text is not displayed.");
							}
						}
						else
						{
							Fail("The Coupon Code text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserCouponCodeApplyButton))
						{
							Pass("The Apply button in the Coupon Code is displayed.");
							log.add("The Expected value is : " + applyBtn);
							String title = regUserCouponCodeApplyButton.getAttribute("value");
							log.add("The Actual value is : " + title);
							if(title.contains(applyBtn))
							{
								Pass("The Apply button title matches.",log);
							}
							else
							{
								Fail("The Apply button title does not matches.",log);
							}
							
							String csVal = regUserCouponCodeApplyButton.getCssValue("background-color");
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + exColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(exColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Apply button in the Coupon Code is not displayed.");
						}
					}
					else
					{
						Fail("The Coupon Code Container is not displayed.");
					}
					//jsclick(regUserCouponCodeContainerCloseTrigger);
					Thread.sleep(250);
				}
				else
				{
					Fail("The Container Open Trigger + is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" Chk - 165 Issue ." + e.getMessage());
				Exception(" Chk - 165 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 167 Verify that while selecting the "Apply" button by entering the invalid code details in the "Coupon Code" text box,the alert mesage should be displayed along with the text box highlighted.*/
	public void regUserCheckoutCouponInvalidValidation()
	{
		ChildCreation(" Chk - 167 Verify that while selecting the Apply button by entering the invalid code details in the Coupon Code text box,the alert mesage should be displayed along with the text box highlighted.");
		if(chkPageLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("Chk167", sheet, 3);
				String expAlert = BNBasicfeature.getExcelVal("Chk167", sheet, 4);
				String cpnCode = BNBasicfeature.getExcelNumericVal("Chk167", sheet, 7);
				
				if(BNBasicfeature.isElementPresent(regUserCouponCodeContainerCloseTrigger))
				{
					Pass("The Coupon Code Container Open Trigger is displayed.");
					//regUserCouponCodeContainerOpenTrigger.click();
					wait.until(ExpectedConditions.visibilityOf(regUserCouponCodeFields));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(regUserCouponCodeFields))
					{
						Pass("The Coupon Code Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserCouponCodeTextField))
						{
							Pass("The Coupon Code text field is displayed.");
							regUserCouponCodeTextField.click();
							regUserCouponCodeTextField.clear();
							regUserCouponCodeTextField.sendKeys(cpnCode);
							if(BNBasicfeature.isElementPresent(regUserCouponCodeApplyButton))
							{
								Pass("The Apply button in the Coupon Code is displayed.");
								jsclick(regUserCouponCodeApplyButton);
								wait.until(ExpectedConditions.visibilityOf(regUserCouponCodeError));
								Thread.sleep(250);
								log.add("The expected alert was : " + expAlert);
								String err1 = regUserCouponCodeError.getText();
								log.add("The actual alert was  : " + err1);
								if((expAlert.contains(err1)))
								{
									Pass("The expected error matches the actual alert.",log);
								}
								else
								{
									Fail("The expected error does not match the actual alert.",log);
								}
								
								String cssVal = regUserCouponCodeTextField.getCssValue("border");
								String hexCode = BNBasicfeature.colorfinder(cssVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Coupon Code field is highlighted.",log);
								}
								else
								{
									Fail("The Coupon Code field is not highlighted.",log);
								}
							}
							else
							{
								Fail("The Apply button in the Coupon Code is not displayed.");
							}
						}
						else
						{
							Fail("The Coupon Code text field is not displayed.");
						}
					}
					else
					{
						Fail("The Coupon Code Container is not displayed.");
					}
					jsclick(regUserCouponCodeContainerCloseTrigger);
					Thread.sleep(250);
				}
				else
				{
					Fail("The Container Open Trigger + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 167 Issue ." + e.getMessage());
				Exception(" Chk - 167 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 168 Verify that while selecting the "+" button near the "Bookfair ID" section,the textbox should be displayed with default text and "Apply" button.*/
	public void regUserCheckoutBookFair()
	{
		ChildCreation(" Chk - 168 Verify that while selecting the + button near the Bookfair ID section,the textbox should be displayed with default text and Apply button..");
		if(chkPageLoaded==true)
		{	
			try
			{
				if(BNBasicfeature.isElementPresent(regUserBookFairContainerOpenTrigger))
				{
					Pass("The Book Fair Container Open Trigger is displayed.");
					jsclick(regUserBookFairContainerOpenTrigger);
					wait.until(ExpectedConditions.visibilityOf(regUserBookFairFields)).getAttribute("style").contains("display: block;");
					Thread.sleep(1000);
					if(BNBasicfeature.isElementPresent(regUserBookFairFields))
					{
						Pass("The Book Fair Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserBookFairTextField))
						{
							Pass("The Book Fair text field is displayed.");
						}
						else
						{
							Fail("The Book Fair text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserBookFairApplyButton))
						{
							Pass( "The Apply Button in the Book Fair Id is displayed.");
						}
						else
						{
							Fail( "The Apply Button in the Book Fair Id is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(regUserBookFairFieldDefaultText))
						{
							Pass( "The Default Text in the Book Fair Id is displayed.");
							if(regUserBookFairFieldDefaultText.getText().isEmpty())
							{
								Fail(" The Default Text is empty.");
							}
							else
							{
								Pass(" The Default Text is not empty.");
							}
						}
						else
						{
							Fail( "The Default Text in the Book Fair Id is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Container is not displayed.");
					}
				}
				else
				{
					Fail("The Book Fair Open Trigger + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 168 Issue ." + e.getMessage());
				Exception(" Chk - 168 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 169 Verify that on clicking inside the book-id text box in the "Bookfair ID" section,the cursor should blink inside the text box with highlightion.*/
	public void regUserCheckoutBookFairHighlight()
	{
		ChildCreation(" Chk - 169 Verify that on clicking inside the book-id text box in the Bookfair ID section,the cursor should blink inside the text box with highlightion.");
		if(chkPageLoaded==true)
		{
			try
			{
				String expCol = BNBasicfeature.getExcelVal("Chk169", sheet, 2);
				if(BNBasicfeature.isElementPresent(regUserBookFairContainerCloseTrigger))
				{
					Pass("The Book Fair Container Close Trigger is displayed.");
					if(BNBasicfeature.isElementPresent(regUserBookFairFields))
					{
						Pass("The Book Fair Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserBookFairTextField))
						{
							Pass("The Book Fair text field is displayed.");
							regUserBookFairTextField.click();
							Thread.sleep(250);
							String hexColor = regUserBookFairTextField.getCssValue("border").substring(10, 25);
							String colhex = Color.fromString(hexColor).asHex();
							//System.out.println(colhex);
							log.add( "The Expected Color was : " + expCol);
							log.add( "The Actual Color is : " + colhex);
							if(expCol.contains(colhex))
							{
								Pass ( " The Text field is highligted with the expected color.",log);
							}
							else
							{
								Fail( "The text field is not highlighted with the expected color.",log);
							}
						}
						else
						{
							Fail("The Book Fair text field is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Container is not displayed.");
					}
					//regUserBookFairContainerCloseTrigger.click();
					//Thread.sleep(1000);
				}
				else
				{
					Fail("The Book Fair Open Trigger + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 169 Issue ." + e.getMessage());
				Exception(" Chk - 169 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 170 Verify that while selecting the "Apply" button without entering the book-id details in the "Bookfair ID" section,the alert message should be displayed along with the text box highlighted.*/
	public void regUserCheckoutBookFairEmptyValidation()
	{
		ChildCreation(" Chk - 170 Verify that while selecting the Apply button without entering the book-id details in the Bookfair ID section,the alert message should be displayed along with the text box highlighted.");
		if(chkPageLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("Chk170", sheet, 3);
				String expAlert = BNBasicfeature.getExcelVal("Chk170", sheet, 4);
				if(BNBasicfeature.isElementPresent(regUserBookFairContainerCloseTrigger))
				{
					Pass("The Book Fair Container Close Trigger is displayed.");
					wait.until(ExpectedConditions.visibilityOf(regUserBookFairFields));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(regUserBookFairFields))
					{
						Pass("The Book Fair Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserBookFairTextField))
						{
							Pass("The Book Fair text field is displayed.");
							regUserBookFairTextField.click();
							if(BNBasicfeature.isElementPresent(regUserBookFairApplyButton))
							{
								Pass("The Apply button in the Coupon Code is displayed.");
								jsclick(regUserBookFairApplyButton);
								wait.until(ExpectedConditions.visibilityOf(regUserBookFairError));
								Thread.sleep(250);
								log.add("The expected alert was : " + expAlert);
								String err1 = regUserBookFairIndividualError.get(0).getText();
								log.add("The actual alert was  : " + err1);
								if((expAlert.equals(err1)))
								{
									Pass("The expected error matches the actual alert.",log);
								}
								else
								{
									Fail("The expected error does not match the actual alert.",log);
								}
								
								String cssVal = regUserBookFairTextField.getCssValue("border");
								String hexCode = BNBasicfeature.colorfinder(cssVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Coupon Code field is highlighted.",log);
								}
								else
								{
									Fail("The Coupon Code field is not highlighted.",log);
								}
							}
							else
							{
								Fail("The Apply button in the Coupon Code is not displayed.");
							}
						}
						else
						{
							Fail("The Book Fair text field is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Container is not displayed.");
					}
					Thread.sleep(100);
				}
				else
				{
					Fail("The Book Fair Open Trigger + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 170 Issue ." + e.getMessage());
				Exception(" Chk - 170 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 172 Verify that while selecting the "Apply" button after entering the 8 digit invalid bookfair id in the "Bookfair ID" section,the alert message should be displayed along with the text box highlighted.*/
	public void regUserCheckoutBookFairInvalidValidation()
	{
		ChildCreation(" Chk - 172 Verify that while selecting the Apply button after entering the 8 digit invalid bookfair id in the Bookfair ID section,the alert message should be displayed along with the text box highlighted.");
		if(chkPageLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("Chk172", sheet, 3);
				String expAlert = BNBasicfeature.getExcelVal("Chk172", sheet, 4);
				String val = BNBasicfeature.getExcelNumericVal("Chk172", sheet, 8);
				if(BNBasicfeature.isElementPresent(regUserBookFairContainerCloseTrigger))
				{
					Pass("The Book Fair Container Close Trigger is displayed.");
					//regUserBookFairContainerOpenTrigger.click();
					wait.until(ExpectedConditions.visibilityOf(regUserBookFairFields));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(regUserBookFairFields))
					{
						Pass("The Book Fair Container is displayed.");
						if(BNBasicfeature.isElementPresent(regUserBookFairTextField))
						{
							Pass("The Book Fair text field is displayed.");
							regUserBookFairTextField.click();
							regUserBookFairTextField.clear();
							regUserBookFairTextField.sendKeys(val);
							if(BNBasicfeature.isElementPresent(regUserBookFairApplyButton))
							{
								Pass("The Apply button in the Coupon Code is displayed.");
								jsclick(regUserBookFairApplyButton);
								wait.until(ExpectedConditions.visibilityOf(regUserBookFairError));
								Thread.sleep(250);
								log.add("The expected alert was : " + expAlert);
								String err1 = regUserBookFairIndividualError.get(0).getText();
								log.add("The actual alert was  : " + err1);
								if((err1.equals(expAlert)))
								{
									Pass("The expected error matches the actual alert.",log);
								}
								else
								{
									Fail("The expected error does not match the actual alert.",log);
								}
								
								String cssVal = regUserBookFairTextField.getCssValue("border");
								String hexCode = BNBasicfeature.colorfinder(cssVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Coupon Code field is highlighted.",log);
								}
								else
								{
									Fail("The Coupon Code field is not highlighted.",log);
								}
							}
							else
							{
								Fail("The Apply button in the Coupon Code is not displayed.");
							}
						}
						else
						{
							Fail("The Book Fair text field is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair Container is not displayed.");
					}
					/*jsclick(regUserBookFairContainerCloseTrigger);
					Thread.sleep(250);*/
				}
				else
				{
					Fail("The Book Fair Open Trigger + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 172 Issue ." + e.getMessage());
				Exception(" Chk - 172 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 173 Verify that while selecting the "Apply" button by entering alphabets,special character or numeric digit less/greater than 8 for "Bookfair ID",the alert should be displayed along with the text box highlighted.*/
	public void regUserCheckoutBookFairInvalidValidation1()
	{
		ChildCreation(" Chk - 173 Verify that while selecting the Apply button by entering alphabets,special character or numeric digit less/greater than 8 for Bookfair ID,the alert should be displayed along with the text box highlighted.");
		try
		{
			String expColor = BNBasicfeature.getExcelVal("Chk173", sheet, 3);
			String expAlert = BNBasicfeature.getExcelVal("Chk173", sheet, 4);
			String val = BNBasicfeature.getExcelNumericVal("Chk173", sheet, 8);
			if(BNBasicfeature.isElementPresent(regUserBookFairContainerCloseTrigger))
			{
				Pass("The Book Fair Container Close Trigger is displayed.");
				Pass("The Book Fair Container Close Trigger is displayed.");
				//regUserBookFairContainerOpenTrigger.click();
				wait.until(ExpectedConditions.visibilityOf(regUserBookFairFields));
				Thread.sleep(100);
				if(BNBasicfeature.isElementPresent(regUserBookFairFields))
				{
					Pass("The Book Fair Container is displayed.");
					if(BNBasicfeature.isElementPresent(regUserBookFairTextField))
					{
						Pass("The Book Fair text field is displayed.");
						regUserBookFairTextField.click();
						regUserBookFairTextField.clear();
						regUserBookFairTextField.sendKeys(val);
						if(BNBasicfeature.isElementPresent(regUserBookFairApplyButton))
						{
							Pass("The Apply button in the Coupon Code is displayed.");
							jsclick(regUserBookFairApplyButton);
							wait.until(ExpectedConditions.visibilityOf(regUserBookFairError));
							Thread.sleep(250);
							log.add("The expected alert was : " + expAlert);
							String err1 = regUserBookFairIndividualError.get(0).getText();
							log.add("The actual alert was  : " + err1);
							if((err1.equals(expAlert)))
							{
								Pass("The expected error matches the actual alert.",log);
							}
							else
							{
								Fail("The expected error does not match the actual alert.",log);
							}
							
							String cssVal = regUserBookFairTextField.getCssValue("border");
							String hexCode = BNBasicfeature.colorfinder(cssVal);
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Coupon Code field is highlighted.",log);
							}
							else
							{
								Fail("The Coupon Code field is not highlighted.",log);
							}
						}
						else
						{
							Fail("The Apply button in the Coupon Code is not displayed.");
						}
					}
					else
					{
						Fail("The Book Fair text field is not displayed.");
					}
				}
				else
				{
					Fail("The Book Fair Container is not displayed.");
				}
				jsclick(regUserBookFairContainerCloseTrigger);
				Thread.sleep(250);
			}
			else
			{
				Fail("The Book Fair Open Trigger + is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 173 Issue ." + e.getMessage());
			Exception(" Chk - 173 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 124 Verify that Delivery options are not enabled for the login user when they haven't saved the shipping details.*/
	public void checkoutNoDeliveryPreference()
	{
		ChildCreation("Chk - 124 Verify that Delivery options are not enabled for the login user when they haven't saved the shipping details.");
		boolean chk138 = false;
		try
		{
			boolean signedIn = signInExistingCustomerNoShippingAddress();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(checkoutTopSubmitOrder));
				wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
				if(BNBasicfeature.isElementPresent(deliveryOptionContainer))
				{
					Fail("The Delivery Option container is found for the user when they have no billing and shipping address.");
					chk138 = false;
				}
				else
				{
					Pass("The Delivery Option container is not found when they have no billing and shipping address.");
					chk138 = true;
				}
			}
			else
			{
				Fail( "Failed to load Checkout Page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 124 Issue ." + e.getMessage());
			Exception(" Chk - 124 Issue." + e.getMessage());
		}
		
		ChildCreation( " Chk - 138 Verify that Delivery preferences/speed details should not be shown for the digital content . " );
		try
		{
			if(chk138==true)
			{
				Pass ( " No Delivery preference was displayed for the Digital Content Products.");
			}
			else
			{
				Fail( " Delivery preference was displayed for the Digital Content Products.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 138 Issue ." + e.getMessage());
			Exception(" Chk - 138 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 78 Verify that "Please update your Shipping Information" with "add" button should be shown for the user who have no shipping and billing details in registered account*/
	public void checkoutAddNewInfo()
	{
		ChildCreation(" Chk - 78 Verify that Please update your Shipping Information with Add button should be shown for the user who have no shipping and billing details in registered account.");
		if(chkPageLoaded==true)
		{
			try
			{
				String info = BNBasicfeature.getExcelVal("Chk78", sheet, 2);
				if(BNBasicfeature.isListElementPresent(checkoutNoBillorShipAddMsg))
				{
					for(int j = 1 ; j <= checkoutNoBillorShipAddMsg.size() ; j++)
					{
						String msg = checkoutNoBillorShipAddMsg.get(j-1).getText();
						log.add( "The Expected caption was : " + info);
						log.add( "The actual caption is : " + msg);
						if(msg.equals(info))
						{
							Pass("The message matches for the Shipping address. The found info message was " + msg.toString(),log);
						}
						else
						{
							log.add(" The found info message was : " + info);
						}
					}
				}
				else
				{
					Fail("The Billing and Shipping container is not found. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 78 Issue ." + e.getMessage());
				Exception(" Chk - 78 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}

	/* Chk - 79 Verify that "To place an order, please provide valid shipping/billing information" alert message should be shown if the user selects "submit order" button without filling the shipping and billing information.*/
	public void checkoutSubmitOrderAlert()
	{
		ChildCreation(" Chk - 79 Verify that To place an order, please provide valid shipping/billing information alert message should be shown if the user selects Submit Order button without filling the shipping and billing information.");
		if(chkPageLoaded==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("Chk79", sheet, 2);
				jsclick(checkoutBottomSubmitOrder);
				wait.until(ExpectedConditions.visibilityOf(checkoutSubmitOrderError));
				if(checkoutSubmitOrderError.getText().equals(alert))
				{
					Pass(" The alert matches with the expected one. The raised alert was " + checkoutSubmitOrderError.getText());
				}
				else
				{
					Fail(" There alert mismatches. Please Check. The displayed error was " + checkoutSubmitOrderError.getText());
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 79 Issue ." + e.getMessage());
				Exception(" Chk - 79 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 80 Verify that while selecting the "add" button on the checkout page, the "Add a Shipping Address" popup should be displayed.*/
	public void checkoutShippingAddressAddBtn()
	{
		ChildCreation(" Chk - 80 Verify that while selecting the Add button on the checkout page, the Add a Shipping Address popup should be displayed.");
		if(chkPageLoaded==true)
		{
			try
			{
				String addShipAddtitle = BNBasicfeature.getExcelVal("Chk80", sheet, 2);
				if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
				{
					boolean loaPage = ldPage(checkoutShipEditBtn, checkoutShipAddShippingAddressTitle);
					if(loaPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(checkoutShipAddShippingAddressTitle));
						log.add( "The Expected title was : " + addShipAddtitle );
						String actTitle = checkoutShipAddShippingAddressTitle.getText();
						log.add( "The Actual title is : " + actTitle);
						if(actTitle.equals(addShipAddtitle))
						{
							Pass(" The user is successfully navigated to the Add a Shipping Address page and the page title matches with the expected one.",log);
						}
						else
						{
							Fail(" There is something wrong Add Shipping address page title. Please Check.",log);
						}
					}
					else
					{
						Fail( "The user failed to load the Add Shipping Address Page.");
					}
				}
				else
				{
					Fail(" There is something wrong No Add Button is found for the Shipping address. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 80 Issue ." + e.getMessage());
				Exception(" Chk - 80 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 81 Verify that Add a shipping address page is matched with the creative*/
	public void checkoutAddShippingAddressElements()
	{
		ChildCreation(" Chk - 81 Verify that Add a shipping address page is matched with the creative.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Pass("The Country drop down list box is present and visible.");
				}
				else
				{
					Fail("The Country drop down list box is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(fName))
				{
					Pass("The First Name text field is present and visible.");
				}
				else
				{
					Fail("The First Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name text field is present and visible.");
				}
				else
				{
					Fail("The Last Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Stree Address text field is present and visible.");
				}
				else
				{
					Fail("The Stree Address text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					Pass("The Apt/Suite text field is present and visible.");
				}
				else
				{
					Fail("The Apt/Suite text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City text field is present and visible.");
				}
				else
				{
					Fail("The City text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State selection drop down is present and visible.");
				}
				else
				{
					Fail("The State selection drop down is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					Pass("The Zipcode text field is present and visible.");
				}
				else
				{
					Fail("The Zipcode text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Phone Number text field is present and visible.");
				}
				else
				{
					Fail("The Phone Number text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name text field is present and visible.");
				}
				else
				{
					Fail("The Company Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(poBoxtxt))
				{
					Pass("The PO Check Box field is present and visible.");
				}
				else
				{
					Fail("The PO Check Box is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(addressCancel))
				{
					jsclick(addressCancel);
					Thread.sleep(1000);
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 81 Issue ." + e.getMessage());
				Exception(" Chk - 81 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 100 Verify that create a new billing address option should be displayed to select the address if user doesn't have the saved address*/
	public void checkoutOrderAddBillingInfo()
	{
		ChildCreation(" Chk - 100 Verify that create a new billing address option should be displayed to select the address if user doesn't have the saved address.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(addressCancel))
				{
					jsclick(addressCancel);
					Thread.sleep(1000);
				}
				
				String pgtit = BNBasicfeature.getExcelVal("Chk100", sheet, 2); 
				Thread.sleep(200);
				wait.until(
						ExpectedConditions.or(
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder1),
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
								)
							);
				wait.until(ExpectedConditions.visibilityOf(checkoutShipEditBtn));
				if(BNBasicfeature.isElementPresent(checkoutBillEditBtn))
				{
					log.add("The user is navigated to the Checkout Page.");
					boolean loaPage = ldPage(checkoutBillEditBtn, checkoutCCAddCreditCardPageTitle);
					if(loaPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(checkoutCCAddCreditCardPageTitle));
						Thread.sleep(100);
						String actTit = checkoutCCAddCreditCardPageTitle.getText();
						log.add( "The Expected title was : " + pgtit);
						log.add( "The Actual title is : " + actTit);
						if(actTit.contains(pgtit))
						{
							Pass("The user is navigated to the Add Credit Card Page.");
						}
						else
						{
							Fail("The user is not navigated to the Add Credit Card Page.");
						}
					}
					else
					{
						Fail("The user is not navigated to the Add Credit Card Page.");	
					}
				}
				else
				{
					Fail("The Add Button is not displayed in the Checkout page for the User.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 100 Issue ." + e.getMessage());
				Exception(" Chk - 100 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 99 Verify that Card type, card no, expiration date, security code fields should be displayed in the add a card page as per the creative for signed in user*/
	public void checkoutAddBillingAddNewPageElements()
	{
		ChildCreation(" Chk - 99 Verify that Card type, card no, expiration date, security code fields should be displayed in the add a card page as per the creative for signed in user.");
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(ccNumber))
				{
					Pass("The Credit Card Number field is displayed.");
				}
				else
				{
					Fail("The Credit Card Number field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(ccName))
				{
					Pass("The Credit Card Name field is displayed.");
				}
				else
				{
					Fail("The Credit Card Name field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(ccMonth))
				{
					Pass("The Credit Card Month drop down is displayed.");
				}
				else
				{
					Fail("The Credit Card Month drop down is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(ccYear))
				{
					Pass("The Credit Card Year drop down field is displayed.");
				}
				else
				{
					Fail("The Credit Card Year drop down field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCsv))
				{
					Pass("The Credit Card Security Field CSV is displayed.");
				}
				else
				{
					Fail("The Credit Card Security Field CSV field is not displayed.");
				}
				
				BNBasicfeature.scrolldown(countrySelection,driver);
				Thread.sleep(100);
				
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Pass("The Country Selection drop down field is displayed.");
				}
				else
				{
					Fail("The Country Selection drop down field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(fName))
				{
					Pass("The First Name field is displayed.");
				}
				else
				{
					Fail("The First Name field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name field is displayed.");
				}
				else
				{
					Fail("The Last Name field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Street Address field is displayed.");
				}
				else
				{
					Fail("The Street Address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					Pass("The Apt Suite (Optional) Address field is displayed.");
				}
				else
				{
					Fail("The Apt Suite (Optional) field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State drop down field is displayed.");
				}
				else
				{
					Fail("The State drop down field is not displayed.");
				}
				
				BNBasicfeature.scrolldown(city,driver);
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City field is displayed.");
				}
				else
				{
					Fail("The City field is not displayed.");
				}
				
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					Pass("The Zipcode field is displayed.");
				}
				else
				{
					Fail("The Zipcode field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Contact Number field is displayed.");
				}
				else
				{
					Fail("The Contact Number field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name (Optional) field is displayed.");
				}
				else
				{
					Fail("The Company Name (Optional) field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(crediccardSubmitBtn))
				{
					Pass("The Save and Continue button is displayed.");
				}
				else
				{
					Fail("The Save and Cancel button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(creditcardCancelBtn))
				{
					Pass("The Cancel button is displayed.");
				}
				else
				{
					Fail("The Cancel button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 99 Issue ." + e.getMessage());
				Exception(" Chk - 99 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 106 Verify that while selecting the "Save & Continue" button without entering the details in the "Add a card page",the alert message should be displayed*/
	public void checkoutAddBillingAddNewPageValidation()
	{
		ChildCreation(" Chk - 106 Verify that while selecting the Save & Continue button without entering the details in the Add a card page,the alert message should be displayed.");
		if(chkPageLoaded==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("Chk106", sheet, 2);
				if(BNBasicfeature.isElementPresent(crediccardSubmitBtn))
				{
					jsclick(crediccardSubmitBtn);
					wait.until(ExpectedConditions.visibilityOf(saveCreditCardError));
					Thread.sleep(100);
					log.add("The Expected alert was : " + alert);
					String actError  = saveCreditCardError.getText();
					log.add("The actual alert is : " + actError);
					if(actError.contains(alert))
					{
						Pass("The alert is raised for the page when user tries to save the billing information with no details. The alert matches the expected content.",log);
					}
					else
					{
						Fail("The alert is raised for the page when user tries to save the billing information with no details. The alert does not matches the expected content.,log");
					}
				}
				else
				{
					Fail( "The Credit Card Submit button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 106 Issue ." + e.getMessage());
				Exception(" Chk - 106 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 107 Verify that while selecting the "i" icon near the security code field in the "Add a Credit Card" page, "Security code tooltip" should be displayed as per the creative*/
	public void checkoutOrderAddBillingCCBillingiiconExpand()
	{
		ChildCreation(" Chk - 107 Verify that while selecting the i icon near the security code field in the Add a Credit Card page, Security code tooltip should be displayed as per the creative.") ;
		if(chkPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(billcciicon))
				{
					Pass("The i icon is present in the Add Credit Card page and it is visible.");
					jsclick(billcciicon);
					wait.until(ExpectedConditions.attributeContains(billcctooltipEnable, "style", "block"));
					if(BNBasicfeature.isElementPresent(billcctooltipEnable))
					{
						if(billcctooltipEnable.getAttribute("style").contains("block"))
						{
							Pass("The tooltip i icon is clicked and it is visible for the user.");
						}
						else
						{
							Fail("The tooltip i icon is clicked and it is not visible for the user.");
							BNBasicfeature.scrolldown(creditcardCancelBtn, driver);
							jsclick(creditcardCancelBtn);
						}
					}
					else
					{
						Fail("The tooltip is not visible for the user.");
					}
				}
				else
				{
					Fail("The i icon is not present in the Add Credit Card page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 107 Issue ." + e.getMessage());
				Exception(" Chk - 107 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 108 Verify that the "security tooltip"should be closed on selecting the icon (i) second time near security code field in the "Add a credit card" page for signed in user*/
	public void checkoutOrderAddBillingCCBillingiiconCollapse()
	{
		ChildCreation(" Chk - 108 Verify that the security tooltip should be closed on selecting the icon (i) second time near security code field in the Add a credit card page for signed in user.") ;
		if(chkPageLoaded==true)
		{
			try
			{
				
				if(BNBasicfeature.isElementPresent(cciicontooltipMask))
				{
					jsclick(cciicontooltipMask);
					Thread.sleep(500);
					boolean closed = wait.until(ExpectedConditions.attributeContains(cciicontooltipMask, "style", "none"));
					if(closed==false)
					{
						Fail( "The Mask is displayed.");
						jsclick(creditcardCancelBtn);
						Thread.sleep(1000);
						wait.until(
								ExpectedConditions.or(
									ExpectedConditions.visibilityOf(checkoutTopSubmitOrder1),
									ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
										)
									);
						wait.until(ExpectedConditions.visibilityOf(checkoutShipEditBtn));
					}
					else
					{
						Pass( "The Mask is not displayed.");
					}
				}
				else
				{
					Fail( "The Mask is displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 108 Issue ." + e.getMessage());
				Exception(" Chk - 108 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
	/* Chk - 105 Verify that while selecting the cancel button,the entered address should not saved for entered billing address and load back the ORDER TOTAL page with previously saved address. */ 
	public void BillingCCardCancelBtnNavigation()
	{
		ChildCreation(" Chk - 105 Verify that while selecting the cancel button,the entered address should not saved for entered billing address and load back the ORDER TOTAL page with previously saved address..");
		if(chkPageLoaded==true)
		{
			try
			{
				String firstname = BNBasicfeature.getExcelVal("Chk109", sheet, 7);
				String lastname = BNBasicfeature.getExcelVal("Chk109", sheet, 8);
				String streetAddress = BNBasicfeature.getExcelVal("Chk109", sheet, 9);
				String cty = BNBasicfeature.getExcelVal("Chk109", sheet, 10);
				String zipcde = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 12);
				String cntno = BNBasicfeature.getExcelNumericVal("Chk109", sheet, 13);
				
				BNBasicfeature.scrolldown(fName,driver);
				clearAll();
				BNBasicfeature.scrollup(ccName,driver);
				Actions act = new Actions(driver);
				act.moveToElement(ccMonth).build().perform();
				Select mntsel = new Select(ccMonth);
				mntsel.selectByIndex(5);
				act.moveToElement(ccYear).build().perform();
				Select yrsel = new Select(ccYear);
				yrsel.selectByIndex(5);
				BNBasicfeature.scrolldown(fName,driver);
				fName.sendKeys(firstname);
				lName.sendKeys(lastname);
				stAddress.sendKeys(streetAddress);
				BNBasicfeature.scrolldown(city,driver);
				city.sendKeys(cty);
				Select stsel = new Select(state);
				stsel.selectByIndex(35);
				zipCode.sendKeys(zipcde);
				contactNo.sendKeys(cntno);
				BNBasicfeature.scrolldown(state,driver);
				jsclick(creditcardCancelBtn);
				Thread.sleep(1000);
				wait.until(
						ExpectedConditions.or(
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder1),
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder)
								)
							);
				wait.until(ExpectedConditions.visibilityOf(checkoutShipEditBtn));
				if(BNBasicfeature.isElementPresent(checkoutShipEditBtn))
				{
					Pass("The user is navigated to the Checkout Page and the entered records is not saved.");
				}
				else
				{
					Fail("The user is not navigated to the Checkout Page. Please Check");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 105 Issue ." + e.getMessage());
				Exception(" Chk - 105 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load checkout page in.");
		}
	}
	
}
