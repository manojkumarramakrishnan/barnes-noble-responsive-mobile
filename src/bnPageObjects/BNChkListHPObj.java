package bnPageObjects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNChecklist_HomePage;
import bnConfig.BNConstants;
import bnConfig.BNXpaths;
import bnStreamCalls.BNTopCategoryCampaignCall;

public class BNChkListHPObj extends BNChecklist_HomePage implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static boolean signedIn = false, createAccPage = false, fieldClear = false, frgtPassPage = false, loggedin = false;
	static int sel = 0, mrno = 0, cmrno = 0, scno = 0, bgCount = 0;
	ArrayList<WebElement> webEle = new ArrayList<>();
	
	public BNChkListHPObj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		//this.parent = parent;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+myAccountt+"")
	WebElement myAccount;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+searchContainerCancell+"")
	WebElement searchContainerCancel;
	
	@FindBy(xpath = ""+searchClosee+"")
	WebElement searchClose;
	
	@FindBy(xpath = ""+searchResultss+"")
	WebElement searchResults;
	
	@FindBy(xpath = ""+bnLogoo+"")
	WebElement bnLogo;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+bagCountt+"")
	WebElement bagCount;
	
	@FindBy(xpath =""+storeLocatorr+"")
	WebElement storeLocator;
	
	@FindBy(xpath =""+storesNavTabss+"")
	WebElement storesNavTabs;
	
	@FindBy(xpath =""+storesSearchFieldd+"")
	WebElement storesSearchField;
	
	@FindBy(xpath =""+storesNoSearchResultt+"")
	WebElement storesNoSearchResult;
	
	@FindBy(xpath =""+storesSearchResultt+"")
	WebElement storesSearchResult;
	
	@FindBy(xpath = ""+promoCarousell+"")
	WebElement promoCarousel;
	
	@FindBy(xpath = ""+activePromoo+"")
	WebElement activePromo;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;
	
	@FindBy(xpath = ""+panCakeMenuMaskk+"")
	WebElement panCakeMenuMask;
	
	@FindBy(xpath = ""+signInn+"")
	WebElement signIn;
	
	@FindBy(xpath = ""+signInpageframee+"") 
	WebElement signInpageframe;
	
	@FindBy(xpath = ""+forgotPasspageframee+"") 
	WebElement forgotPasspageframe;
	
	@FindBy(xpath = ""+checkoutLogoo+"")
	WebElement checkoutLogo;
	
	@FindBy(xpath = ""+createAccbtnn+"")
	WebElement createAccbtn;
	
	@FindBy(xpath = ""+signInlnkk+"")
	WebElement signInlnk;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+rememberMee+"")
	WebElement rememberMe;
	
	@FindBy(xpath = ""+forgotPasswordd+"")
	WebElement forgotPassword;
	
	@FindBy(xpath = ""+forgotContinueBtnn+"")
	WebElement forgotContinueBtn;
	
	@FindBy(xpath = ""+orderStatuss+"")
	WebElement orderStatus;
	
	@FindBy(xpath = ""+manageAccountt+"")
	WebElement manageAccount;
	
	@FindBy(xpath = ""+addressBookk+"")
	WebElement addressBook;
	
	@FindBy(xpath = ""+paymentss+"")
	WebElement payments;
	
	@FindBy(xpath = ""+createAccFramee+"")
	WebElement createAccFrame;
	
	@FindBy(xpath = ""+additionalLinkss+"")
	WebElement additionalLinks;
	
	@FindBy(xpath = ""+noSearchResultss+"")
	WebElement noSearchResults;
	
	@FindBy(xpath = ""+searchResultsContainerr+"")
	WebElement searchResultsContainer;
	
	@FindBy(xpath = ""+ccFNamee+"")
	WebElement ccFName;
	
	@FindBy(xpath = ""+ccFNametxtt+"")
	WebElement ccFNametxt;
	
	@FindBy(xpath = ""+ccLNamee+"")
	WebElement ccLName;
	
	@FindBy(xpath = ""+ccLNametxtt+"")
	WebElement ccLNametxt;
	
	@FindBy(xpath = ""+ccEmailIdd+"")
	WebElement ccEmailId;
	
	@FindBy(xpath = ""+ccEmailIdtxtt+"")
	WebElement ccEmailIdtxt;
		
	@FindBy(xpath = ""+ccCnfEmailIdd+"")
	WebElement ccCnfEmailId;
	
	@FindBy(xpath = ""+ccCnfEmailIdtxtt+"")
	WebElement ccCnfEmailIdtxt;
	
	@FindBy(xpath = ""+ccPasswordd+"")
	WebElement ccPassword;
	
	@FindBy(xpath = ""+ccPasswordtxtt+"")
	WebElement ccPasswordtxt;
	
	@FindBy(xpath = ""+ccCnfPasss+"")
	WebElement ccCnfPass;
	
	@FindBy(xpath = ""+ccCnfPasstxtt+"")
	WebElement ccCnfPasstxt;
	
	@FindBy(xpath = ""+ccSecurityQuestionn+"")
	WebElement ccSecurityQuestion;
	
	@FindBy(xpath = ""+ccSecurityQuestiontxtt+"")
	WebElement ccSecurityQuestiontxt;
	
	@FindBy(xpath = ""+ccSecurityAnswerr+"")
	WebElement ccSecurityAnswer;
	
	@FindBy(xpath = ""+ccSecurityAnswertxtt+"")
	WebElement ccSecurityAnswertxt;
	
	@FindBy(xpath = ""+erromsgg+"")
	WebElement erromsg;
	
	@FindBy(xpath = ""+createAccountCreateBtnn+"")
	WebElement createAccountCreateBtn;
	
	@FindBy(xpath = ""+createAccountCancelBtnn+"")
	WebElement createAccountCancelBtn;
	
	@FindBy(xpath = ""+secureSignInn+"")
	WebElement secureSignIn;
	
	@FindBy(xpath = ""+signInErralrtt+"")
	WebElement signInErralrt;
	
	@FindBy(xpath = ""+emailIdd+"")
	WebElement emailId;
	
	@FindBy(xpath = ""+passwordd+"")
	WebElement password;
	
	@FindBy(xpath = ""+forgotEmailDisplayy+"")
	WebElement forgotEmailDisplay;
	
	@FindBy(xpath = ""+forgotEmailEditt+"")
	WebElement forgotEmailEdit;
	
	@FindBy(xpath = ""+forgotEmailRecoveryOptionn+"")
	WebElement forgotEmailRecoveryOption;
	
	@FindBy(xpath = ""+forgotSecurityRecoveryOptionn+"")
	WebElement forgotSecurityRecoveryOption;
	
	@FindBy(xpath = ""+forgotSecurityAnswerFieldss+"")
	WebElement forgotSecurityAnswerFields;
	
	@FindBy(xpath = ""+forgotCancell+"")
	WebElement forgotCancel;
	
	@FindBy(xpath = ""+forgotResetSuccessContainerr+"")
	WebElement forgotResetSuccessContainer;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss1+"")
	WebElement forgotSecurityResetSuccess1;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss2+"")
	WebElement forgotSecurityResetSuccess2;
	
	@FindBy(xpath = ""+forgotSecurityResetSuccesss3+"")
	WebElement forgotSecurityResetSuccess3;
	
	@FindBy(xpath = ""+continueShopBtnn+"")
	WebElement continueShopBtn;
	
	@FindBy(xpath = ""+forgotSecurityAnswerTextBoxx+"")
	WebElement forgotSecurityAnswerTextBox;
	
	@FindBy(xpath = ""+forgotPasswordInfoo+"")
	WebElement forgotPasswordInfo;
	
	@FindBy(xpath = ""+forgotPasswordNeww+"")
	WebElement forgotPasswordNew;
	
	@FindBy(xpath = ""+forgotPasswordNewCnfmm+"")
	WebElement forgotPasswordNewCnfm;
	
	@FindBy(xpath = ""+forgotPasswordruless+"")
	WebElement forgotPasswordrules;
	
	@FindBy(xpath = ""+forgotPasswordrulesiiconShoww+"")
	WebElement forgotPasswordrulesiiconShow;
	
	@FindBy(xpath = ""+forgotPasswordruleslistt+"")
	WebElement forgotPasswordruleslist;
	
	@FindBy(xpath = ""+forgotPasswordrulesiiconHidee+"")
	WebElement forgotPasswordrulesiiconHide;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertHeaderr+"")
	WebElement forgotPasswordResetSuccessAlertHeader;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertConfirmationn+"")
	WebElement forgotPasswordResetSuccessAlertConfirmation;
	
	@FindBy(xpath = ""+forgotPasswordResetSuccessAlertCloseBtnn+"")
	WebElement forgotPasswordResetSuccessAlertCloseBtn;
	
	@FindBy(xpath = ""+customerServiceLinkk+"")
	WebElement customerServiceLink;
	
	@FindBy(xpath = ""+loggedInAccntNamee+"")
	WebElement loggedInAccntName;
	
	@FindBy(xpath = ""+footerSignInn+"")
	WebElement footerSignIn;
	
	@FindBy(xpath = ""+signOutt+"")
	WebElement signOut;
	
	@FindBy(xpath = ""+signOutTempp+"")
	WebElement signOutTemp;
	
	@FindBy(xpath = ""+signOutIndicatorr+"")
	WebElement signOutIndicator;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy(xpath = ""+plpSearchPageFilterContainerr+"")
	WebElement plpSearchPageFilterContainer;
	
	@FindBy (xpath = ""+pdpPageAddToWishListContainerr+"")
	WebElement pdpPageAddToWishListContainer;
	
	@FindBy (xpath = ""+searchPageProductCountContainerr+"")
	WebElement searchPageProductCountContainer;
	
	@FindBy(xpath = ""+pdpPageProductNameContainerr+"")
	WebElement pdpPageProductNameContainer;
	
	
	@FindBy(how = How.XPATH,using = ""+mainMenuListss+"")
	List<WebElement> mainMenuLists;
	
	@FindBy(how = How.XPATH,using = ""+displayedSearchResultss+"")
	List<WebElement> displayedSearchResults;
	
	@FindBy(how = How.XPATH, using = ""+searchResultsListss+"")
	List<WebElement> searchResultsLists;
	
	@FindBy(how = How.XPATH, using = ""+storesSearchPageResultsListt+"")
	List<WebElement> storesSearchPageResultsList;
	
	
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	// Load Billing Or Shipping Page
	public boolean ldPage(WebElement ele, WebElement ele1, WebElement ele2)
	{
		boolean createAccloaded = false;
		WebDriverWait w1 = new WebDriverWait(driver, 5);
		int count = 1;
		try
		{
			do
			{
				if(BNBasicfeature.isElementPresent(ele))
				{
					createAccloaded = true;
				}
				else
				{
					try
					{
						jsclick(ele1);
						w1.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(ele2));
						if(BNBasicfeature.isElementPresent(ele))
						{
							createAccloaded = true;
						}
						count ++;
					}
					catch(Exception e)
					{
						createAccloaded = false;
					}
				}
			}while((createAccloaded==false)&&(count<5));
		}
		catch (Exception e) 
		{
			createAccloaded = false;
		}
		return createAccloaded;
	}
	
	// Get Category
	public WebElement getMenuSubCategory()
	{
		WebElement cat = null;
		try
		{
			List<WebElement> subcateele = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@class='sk_mobSubCategory'])["+cmrno+"]//*[@level]"));
			Random r = new Random();
			scno = r.nextInt(subcateele.size());
			if(scno<=1)
			{
				scno = 1;
			}
			//System.out.println(scno);
			cat = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel__1'])["+cmrno+"]//*[@class='sk_mobCategoryItemTxt'])["+scno+"]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting the SubCategory Name.");
			Exception("Error in getting SubCategory Name");
		}
		return cat;
	}
		
	// Get Category
	public WebElement getMenuCategory(int mrno)
	{
		WebElement cat = null;
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category']")));
			List<WebElement> ele1 = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category']"));
			//List<WebElement> ele1 = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobSubCategoryCnt']//*[@level='level_']"));
			Random r = new Random();
			cmrno = r.nextInt(ele1.size());
			//cmrno = r.nextInt(ele1.size());
			if(cmrno<1||mrno==ele1.size())
			{
				cmrno = 1;
			}
			//scrolldown(driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]")));
			cat = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting the Menu Category Name.");
			Exception("Error in getting Menu Category Name");
		}
		return cat;
	}
	
	// Get Search Results
	public void getSearchResults(String srchtxt)
	{
		String srchTxt = srchtxt;
		try
		{
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				if(!BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
				}
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					 act.moveToElement(searchContainer).click().sendKeys(srchTxt).build().perform();
					 Thread.sleep(500);
					 int count = 0;
					 do
					 {
						 Thread.sleep(150);
						 wait.until(ExpectedConditions.and(
								 	ExpectedConditions.visibilityOf(searchResults),
								 	ExpectedConditions.invisibilityOfAllElements(displayedSearchResults),
								 	ExpectedConditions.visibilityOf(searchResultsContainer),
									ExpectedConditions.visibilityOfAllElements(searchResultsLists)
										)
									);
						 count++;
						 Thread.sleep(150);
					 }while(count<3);
					 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				}
				else
				{
					Fail("The Search Results are not displayed.");
				}
			}
			else
			{
				Fail("The Search icon is Clicked and the Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Error in displaying the search results.");
			Exception("Error in displaying the search results.");
		}
	}
	
	//Clear all the fields in the createAccountPage
	public void ccclearAll(ArrayList<WebElement> ele)
	{
		fieldClear = false;
		try
		{
			int cnt = ele.size();
			BNBasicfeature.scrollup(checkoutLogo, driver);
			Thread.sleep(500);
			BNBasicfeature.scrollup(signInlnk, driver);
			for(int i = 0; i<cnt;i++)
			{
				BNBasicfeature.scrolldown(ele.get(i), driver);
				//Thread.sleep(50);
				ele.get(i).clear();
				fieldClear=true;
			}
			Thread.sleep(500);
			BNBasicfeature.scrollup(pgetitle, driver);
			Thread.sleep(100);
			webEle.removeAll(webEle);
		}
		catch (Exception e)
		{
			System.out.println("CLEAR ALL EXCEPTION");
			System.out.println(e.getMessage());
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	//Clear all the fields in the createAccountPage
	public void clearAll()
	{
		try
		{
			if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password)))
			{
				emailId.clear();
				password.clear();
			}
			else
			{
				Fail( "The Email and Password field is not displayed to clear.");
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in clearing the Email and Password.");
			Exception("Error in clearing the Email and Password.");
		}
	}
	
	// Sign In to the User Account
	public boolean signInExistingCustomer() throws IOException
	{
		Actions act = new Actions(driver);
		signedIn = false;
		try
		{
			if(!BNBasicfeature.isElementPresent(header))
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			if(BNBasicfeature.isElementPresent(menu))
			{
				Thread.sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				Thread.sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(signIn));
				Thread.sleep(100);
				act.moveToElement(signIn).click().build().perform();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				signedIn = true;
			}
		}
		catch (Exception e)
		{
			System.out.println(" Issue In Navigating to the Sign In Page.");
			Exception("There is something wrong with the page or element is not found. Please Check." + e.getMessage());
		}
		return signedIn;
	}
		
	// Get Bag Count Value
	public boolean getBagCount()
	{
		boolean bagCountOk = false;
		int count = 1;
		int refCount = 1;
		do
		{
			for( int i = 0; i<100; i++)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(bagCount));
					wait.until(ExpectedConditions.elementToBeClickable(bagCount));
					//String bagCnt = bagCount.getText();
					String bagCnt = bagCount.getText().replace("+", "");
					if(bagCnt.isEmpty())
					{
						count++;
						continue;
					}
					else
					{
						bgCount = Integer.parseInt(bagCnt);
						bagCountOk = true;
						break;
					}
				}
				catch(Exception e)
				{
					count++;
				}
			}
			if(count>=100)
			{
				driver.navigate().refresh();
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(bagCount));
				count = 1;
				refCount++;
			}
		}while((bagCountOk==false)&&(refCount<=3));
		return bagCountOk;
	}
	
	/* Chk 1 : Verify that while launching the url in the browsers, the home page should be displayed */
	public void homepage()
	{
		ChildCreation(" Chk 1 : Verify that while launching the url in the browsers, the home page should be displayed.");
		try
		{
			String expUrl = BNBasicfeature.getExcelVal("Chk1", sheet, 2);
			log.add( "The Expected URL was : " + expUrl);
			String actUrl = driver.getCurrentUrl();
			log.add( "The Actual URL is : " + actUrl);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(actUrl.contains(expUrl))
			{
				if(BNBasicfeature.isElementPresent(header))
				{
					Pass("The Header is displayed.");
				}
				else
				{
					Fail("The Header is not displayed.");
				}
			}
			else
			{
				Fail("The Expected and the Actual Url does not mathces.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1 Issue ." + e.getMessage());
			Exception(" BRM - 1 Issue ." + e.getMessage());
		}
	}
	
	/* Chk - 3 Verify that "Hamburger Menu","Search","Logo","Store Locator","Shopping Bag" icons should be displayed at the header along with the promo banner */
	public void hpelementpresent()
	{
		ChildCreation(" Chk 3 : Verify that Hamburger Menu,Search,Logo,Store Locator,Shopping Bag icons should be displayed at the header along with the promo banner .");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				Pass("The Pancake Menu is displayed / present.");
			}
			else
			{
				Fail("The Pancake Menu is not displayed / present.");
			}
			
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				Pass("The Search Icon is displayed / present.");
			}
			else
			{
				Fail("The Search Icon is displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(bnLogo))
			{
				Pass("The Barnes and Noble logo is displayed / present.");
			}
			else
			{
				Fail("The Barnes and Noble logo is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(storeLocator))
			{
				Pass("The Store Locator icon is displayed / present.");
			}
			else
			{
				Fail("The Store Locator icon is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				Pass("The Shopping Bag icon is displayed / present.");	
			}
			else
			{
				Fail("The Shopping Bag icon is not displayed / present.");
			}
				
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo Carousel icon is displayed / present.");
			}
			else
			{
				Fail("The Promo Carousel is not displayed / present.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 3 Issue ." + e.getMessage());
			Exception(" BRM - 3 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 22 Verify that barnes and noble logo should be displayed in all the pages and while selecting it should navigate to the home page .*/
	public void bnlogo()
	{
		ChildCreation(" Chk - 22 Verify that barnes and noble logo should be displayed in all the pages and while selecting it should navigate to the home page .");
		boolean BRM42 = false;
		try
		{
			Actions act = new Actions(driver);
			driver.get(BNConstants.mobileSiteURL);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(bnLogo))
			{
				Pass("The Logo is displayed in the Main Page.");
			}
			else
			{
				Fail("The Logo is not displayed in the Main Page.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				Thread.sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(signIn));
				Thread.sleep(100);
				act.moveToElement(signIn).click().build().perform();
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				if(BNBasicfeature.isElementPresent(checkoutLogo))
				{
					Pass("The Logo is displayed in the Sign In Page.");
					BRM42 = true;
				}
				else
				{
					Fail("The Logo is not displayed in the Sign In Page.");
				}
			}
			else
			{
				Fail("The menu is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(createAccbtn))
			{
				boolean pgLoaded = ldPage(signInlnk, createAccbtn, createAccFrame);
				if(pgLoaded==true)
				{
					if(BNBasicfeature.isElementPresent(checkoutLogo))
					{
						log.add("The user is navigated to the Create Account Page.");
						Pass("The Logo is displayed in the Create Account Page.",log);
						if(BRM42==true)
						{
							BRM42 = true;
						}
					}
					else
					{
						Fail("The Logo is not displayed in the Create Account Page.");
						BRM42 = false;
					}
				}
				else
				{
					Fail( " The user failed to load the create account page.");
				}
			}
			else
			{
				Fail("The Create Account button is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(signInlnk))
			{
				jsclick(signInlnk);
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
			}
			else
			{
				Fail("The Sign In link is not visible or present in the Create Account page.");
			}
			
			wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
			Thread.sleep(200);
			wait.until(ExpectedConditions.visibilityOf(forgotPassword));
			if(BNBasicfeature.isElementPresent(forgotPassword))
			{
				frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
				if(frgtPassPage==true)
				{
					if(BNBasicfeature.isElementPresent(checkoutLogo))
					{
						log.add("The user is navigated to the Forgot Password Page.");
						Pass("The Logo is displayed in the Forgot Password Page.");
						jsclick(checkoutLogo);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(menu));
					}
					else
					{
						Fail("The Logo is not displayed in the Forgot Password Page.");
						driver.get(BNConstants.mobileSiteURL);
						wait.until(ExpectedConditions.visibilityOf(header));
					}	
				}
				else
				{
					Fail( "The User failed to load the Forgot Password page.");
				}
			}
			else
			{
				Fail("The Forgot Password link is not visible or present in the main page.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				Thread.sleep(100);
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(orderStatus));
				act.moveToElement(orderStatus).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when My Account was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when My Account was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(manageAccount));
				act.moveToElement(manageAccount).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Manage Account was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Manage Account was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(addressBook));
				act.moveToElement(addressBook).click().build().perform();
				Thread.sleep(100);
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Address Book was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Address Book was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(payments));
				act.moveToElement(payments).click().build().perform();
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Logo is displayed when Payments was clicked.");
				}
				else
				{
					Fail("The Logo is not displayed when Payments was clicked.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 22 Issue ." + e.getMessage());
			Exception(" Chk - 22 Issue." + e.getMessage());
		}
	}
	
	/* Chk 23 - Verify  that promo messages should be displayed at the top of the banner as per the creative.*/
	public void promobannerpresent() throws InterruptedException
	{
		ChildCreation(" Chk 23 - Verify  that promo messages should be displayed at the top of the banner as per the creative.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo message banner is present at the top of the page.");
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the pages.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage() + " Chk 23");
			Exception("There is something wrong. Please Check. " + e.getMessage());
		}
	}
	
	/* Chk 24 - Verify that while selecting the promo messages,it should  redirects to the respective page*/
	public void promoredirect()
	{
		ChildCreation(" Chk 24 - Verify that while selecting the promo messages,it should  redirects to the respective page.");
		try
		{
			log.add("The browser is launched and the URL is successfully loaded.");
			wait.until(ExpectedConditions.visibilityOf(header));
			String currUrl = driver.getCurrentUrl();
			if(BNBasicfeature.isElementPresent(promoCarousel))
			{
				Pass("The Promo message banner is present at the top of the page.",log);
				wait.until(ExpectedConditions.visibilityOf(activePromo));
				if(BNBasicfeature.isElementPresent(activePromo))
				{
					wait.until(ExpectedConditions.elementToBeClickable(activePromo));
					String promotxt = activePromo.getText();
					Actions act = new Actions(driver);
					act.moveToElement(activePromo).click().build().perform();
					//activePromo.click();
					//jsclick(activePromo);
					//Thread.sleep(5000);
					Thread.sleep(2000);
					wait.until(ExpectedConditions.or(
							ExpectedConditions.visibilityOf(menu),
							ExpectedConditions.visibilityOf(header)
								)
						);
					String navUrl = driver.getCurrentUrl().toString();
					log.add( "The URL before navigation was : " + currUrl);
					log.add( "The URL after navigation is : " + navUrl);
					if(currUrl.equals(navUrl))
					{
						Fail( "The User is not navigated to the expected url.",log);
					}
					else
					{
						log.add(" The user is navigated to the " +  navUrl + " page after clicking the " + promotxt.toString());
						Pass("The user is redirected to the Page successfully.",log);	
						driver.navigate().back();
						wait.until(ExpectedConditions.visibilityOf(header));
					}
				}
				else
				{
					Fail("The user is not redirected to the Page successfully.",log);
				}
			}
			else
			{
				Fail("The Promo message banner is not present at the top of the page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 24 Issue ." + e.getMessage());
			Exception(" Chk - 24 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 4 Verify whether clicking on hamburger menu displays the sub menus and again clicking on hamburger menu collapses the sub menus*/
	public void menuopencollapse()
	{
		ChildCreation(" BRM - 4 Verify whether clicking on hamburger menu displays the sub menus and again clicking on hamburger menu collapses the sub menus.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				log.add("The browser is launched and the user is navigated to the URL");
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(panCakeMenuMask));
				if(BNBasicfeature.isElementPresent(panCakeMenuMask))
				{
					Pass("The Menu is successfully clicked and the Submenus are displayed.",log);
					jsclick(panCakeMenuMask);
					log.add("The Menu Mask area is successfully clicked.");
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					wait.until(ExpectedConditions.visibilityOf(bagIcon));
					if(BNBasicfeature.isElementPresent(menu))
					{
						Pass("The Submenu is successfully collapsed and the Menu icon is once again displayed / visible.",log);
					}
					else
					{
						Fail("The Submenu is not collapsed and the Menu icon is not displayed / visible .",log);
					}
				}
				else
				{
					Fail("The Menu Mask area is not visible to click.");
				}
			}
			else
			{
				Fail("The pancake menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 4 Issue ." + e.getMessage());
			Exception(" Chk - 4 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 5 Verify whether clicking on hamburger menu displays the sub menus and clicking on �+� or Item name can expand the menu [Sub menus too] and clicking on �-� or item name collapses the menu [ Sub menus too].*/
	public void menusubmenuclick()
	{
		ChildCreation(" Chk - 5 Verify whether clicking on hamburger menu displays the sub menus and clicking on + or Item name can expand the menu [Sub menus too] and clicking on - or item name collapses the menu [ Sub menus too].");
		//System.out.println(3);
		try
		{
			mrno = 0; cmrno = 0;
			if(BNBasicfeature.isElementPresent(menuLists))
			{
				jsclick(panCakeMenuMask);
			}
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(menu))
			{
				log.add("The browser is launched and the user is navigated to the URL");
				jsclick(menu);
				Pass("The Menu is successfully clicked and the Submenus are displayed.");
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				if(BNBasicfeature.isElementPresent(menuLists))
				{
					List<WebElement> ele = mainMenuLists;
					Random r = new Random();
					mrno = r.nextInt(ele.size());
					BNBasicfeature.scrolldown(mainMenuLists.get(mrno), driver);
					WebElement subMenu = mainMenuLists.get(mrno);
					WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[contains(@class,'sk_mobCategoryItemLink ')])"));
					//WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobCategoryItemLink '])"));
					log.add("The Selected Category from the list is " + subMenu.getText().toString());
					act.moveToElement(subMenu).click().build().perform();
					Pass("The Category is successfully clicked and the clicked Category is " + subMenu.getText().toString(),log);
					BNBasicfeature.scrollup(subMenu, driver);
					WebElement category = getMenuCategory(mrno);
					log.add("The Selected Sub Category from the list is " + category.getText().toString());
					Thread.sleep(100);
					act.moveToElement(category).click().build().perform();
					//Thread.sleep(2000);
					Pass("The Sub Category is successfully clicked and the clicked Subcategory is " + category.getText().toString(),log);
					
					String cat = category.getAttribute("class");
					if(!cat.isEmpty())
					{
						WebElement icon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@id='sk_mobSublevel_"+(mrno)+"_0']//*[@type='category'])["+(cmrno)+"]//*[@class='sk_mobCategoryItemLink ']"));
						act.moveToElement(icon).click().build().perform();
						//jsclick(icon);
						act.moveToElement(sicon).click().build().perform();
						Pass("The Sub Category are collapsed successfully when clicked.");
					}
					
					BNBasicfeature.scrolldown(subMenu, driver);
					Thread.sleep(100);
					//jsclick(subMenu);
					Pass("The Category are collapsed successfully when clicked.");
					BNBasicfeature.scrollup(myAccount, driver);
					Thread.sleep(100);
					jsclick(panCakeMenuMask);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					Pass("The User is able to close the menu successfully.");
				}
				else
				{
					Fail("The Menu list is not displayed / visible.");
				}
			}
			else
			{
				Fail("The pancake menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 5 Issue ." + e.getMessage());
			Exception(" Chk - 5 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 6 Verify whether the scrolling of categories and sub categories in hamburger menu are displayed properly.*/
	public void menuscroll()
	{
		ChildCreation(" Chk - 6 Verify whether the scrolling of categories and sub categories in hamburger menu are displayed properly.");
		//System.out.println(4);
		try
		{
			mrno = 0;
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				jsclick(panCakeMenuMask);
			}
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			BNBasicfeature.scrollup(header, driver);
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				log.add("The Menu is successfully clicked and the Submenus are displayed.");
				if(BNBasicfeature.isElementPresent(additionalLinks)&&(BNBasicfeature.isListElementPresent(mainMenuLists)))
				{
					BNBasicfeature.scrolldown(additionalLinks, driver);
					Pass("The Menu list is scrolled Down successfully.",log);
					BNBasicfeature.scrollup(myAccount, driver);
					Pass("The Menu list is scrolled Up successfully.");
					wait.until(ExpectedConditions.visibilityOf(myAccount));
				
					List<WebElement> ele = mainMenuLists;
					Random r = new Random();
					mrno = r.nextInt(ele.size());
					WebElement subMenu = mainMenuLists.get(mrno);
					WebElement sicon = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+(mrno)+"_0']//*[@class='sk_mobCategoryItemLink '])"));
					log.add("The Selected Category from the list is " + subMenu.getText().toString());
					act.moveToElement(subMenu).click().build().perform();
					Thread.sleep(100);
					BNBasicfeature.scrolldown(subMenu, driver);
					Pass("The User is scrolled down to the Category successfully.",log);
					log.add("The Category is successfully clicked and the clicked Category is " + subMenu.getText().toString());
					WebElement category = getMenuCategory(mrno);
					log.add("The Selected Sub Category from the list is " + category.getText().toString());
					act.moveToElement(category).click().build().perform();
					BNBasicfeature.scrolldown(category, driver);
					Pass("The User is scrolled down to the SubCategories successfully.",log);
					BNBasicfeature.scrollup(myAccount, driver);
					act.moveToElement(sicon).click().build().perform();
					jsclick(panCakeMenuMask);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					Pass("The User is able to close the menu successfully.");
				}
				else
				{
					Fail("The Menu list is not displayed / visible.");
				}
			}
			else
			{
				Fail("The Menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 6 Issue ." + e.getMessage());
			Exception(" Chk - 6 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 8 Verify that while selecting the categories and subcategories it should navigate to the respective page .*/
	/* Chk - 9 Verify that onclicking browser back from the subcategory pages, the previous page must be displayed and the header and promo messages should be displayed correctly.  */
	public void catenavigation()
	{
		ChildCreation(" Chk - 7 Verify whether clicking on categories are forwarding to the appropriate links.");
		String oldUrl = "";
		try
		{
			String befUrl = driver.getCurrentUrl();
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(header));
			mrno = 0; cmrno = 0;
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				List<WebElement> menuele = mainMenuLists;
				Random r = new Random();
				mrno = r.nextInt(menuele.size());
				WebElement mName = mainMenuLists.get(mrno);
				BNBasicfeature.scrolldown(mName, driver);
				act.moveToElement(mName).click().build().perform();
				Thread.sleep(100);
				BNBasicfeature.scrollup(mName, driver);
				List<WebElement> catele = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@type='category']"));
				cmrno = r.nextInt(catele.size());
				if(cmrno<=1)
				{
					cmrno = 1;
				}
				//System.out.println(cmrno);
				WebElement cName = getMenuCategory(mrno);
				BNBasicfeature.scrolldown(cName, driver);
				act.moveToElement(cName).click().build().perform();
				Thread.sleep(100);
				WebElement subcName = getMenuSubCategory();
				BNBasicfeature.scrolldown(subcName, driver);
				String subCatName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel__1'])["+cmrno+"]//*[@class='sk_mobCategoryItemTxt'])["+scno+"]")).getText();
				System.out.println(subCatName);
				act.moveToElement(subcName).click().build().perform();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				String currUrl = driver.getCurrentUrl();
				//System.out.println(currUrl);
				String link = BNTopCategoryCampaignCall.link(log, mrno, cmrno, scno);
				String[] brkCurLink = null;
				@SuppressWarnings("unused")
				boolean helpPage = false;
				@SuppressWarnings("unused")
				boolean deptPlp = false;
				@SuppressWarnings("unused")
				boolean ean = false;
				if(currUrl.contains("/h/"))
				{
					brkCurLink = link.split("/h/");
					helpPage  = true;
				}
				else if(currUrl.contains("nookpress"))
				{
					
				}
				else if(currUrl.contains("ean"))
				{
					brkCurLink = link.split("ean=");
					ean = true;
				}
				else 
				{
					brkCurLink = link.split("_/");
					deptPlp = true;
				}
				
				//System.out.println(link);
				boolean linkTrue = false; 
				for(int i = 0; i<=brkCurLink.length;i++)
				{
					if(currUrl.contains(brkCurLink[i]))
					{
						linkTrue = true;
						break;
					}
					else
					{
						linkTrue = false;
						continue;
					}
				}
				
				/*if(linkTrue==true)
				{
					Pass("The User is redirected to the expected url link.");
					
					if(deptPlp==true)
					{
						if(BNBasicfeature.isElementPresent(productListTab))
						{
							System.out.println("P");
							if(BNBasicfeature.isElementPresent(searchTerm))
							{
								String title = searchTerm.getText();
								if(title.isEmpty())
								{
									System.out.println("F");
								}
								else
								{
									if(subCatName.contains(title))
									{
										System.out.println("P");
									}
									else
									{
										System.out.println("F");
									}
								}
							}
						}
						else if(BNBasicfeature.isElementPresent(storeTitle))
						{
							String title = storeTitle.getText();
							if(title.isEmpty())
							{
								System.out.println("F");
							}
							else
							{
								if(title.contains(subCatName))
								//if(subCatName.contains(title))
								{
									System.out.println("P");
								}
								else
								{
									System.out.println("F");
								}
							}
						}
					}
					else if(helpPage==true)
					{
						System.out.println("P");
					}
					else if(ean == true)
					{
						String title = storeTitle.getText();
						if(title.isEmpty())
						{
							System.out.println("F");
						}
						else
						{
							if(title.contains(subCatName))
							//if(subCatName.contains(title))
							{
								System.out.println("P");
							}
							else
							{
								System.out.println("F");
							}
						}
					}
					else
					{
						System.out.println("F");
					}
					
						
				}
				else
				{
					if(BNBasicfeature.isElementPresent(productListTab))
					{
						System.out.println("P");
					}
					else if(BNBasicfeature.isElementPresent(storeTitle))
					{
						System.out.println("P");
					}
					else
					{
						System.out.println("F");
					}
				}*/
				
				if(linkTrue==true)
				{
					Pass("The user is navigated to the expected Url.");
				}
				else
				{
					Fail("The user is not navigated to the expected Url.");
				}
				
				if(!befUrl.equals(currUrl))
				{
					driver.navigate().back();
				}
				
			}
			else
			{
				Fail("The Menu is not displayed / visible.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 8 Issue ." + e.getMessage());
			Exception(" Chk - 8 Issue." + e.getMessage());
		}
		
		ChildCreation(" Chk - 9 Verify that onclicking browser back from the subcategory pages, the previous page must be displayed and the header and promo messages should be displayed correctly. ");
		try
		{
			String currUrl = driver.getCurrentUrl();
			if(!BNBasicfeature.isElementPresent(menu))
			{
				driver.navigate().back();
			}
			wait.until(ExpectedConditions.visibilityOf(header));
			String newUrl = driver.getCurrentUrl();
			log.add( " The Current Url is : " + currUrl);
			log.add( " The Expected Url is : " + oldUrl);
			log.add( " The Navigated Url when back button was used is : " + newUrl);
			if(currUrl.equals(newUrl)&&(!newUrl.equals(oldUrl)))
			{
				Fail( "The User was navigated to the unexpected url when back button in the browser was used.",log);
			}
			else
			{
				Pass( "The Current Url and New Url does not match and the previous page was displayed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage() + " Chk 9");
			Exception("There is something wrong. Please Check." + e.getMessage());
		}
	}
	
	/* Chk - 7 Verify that the categories in the Hamburger menu should match with the classic site .*/
	public void menuComparision() throws IOException, JSONException, InterruptedException
	{
		ChildCreation(" Chk - 7 Verify that the categories in the Hamburger menu should match with the classic site .");
		ArrayList<String> Menu = new ArrayList<>();
		ArrayList<String> Category = new ArrayList<>();
		ArrayList<String> SubCategory = new ArrayList<>();
		
		ArrayList<String> MMenu = new ArrayList<>();
		ArrayList<String> MCategory = new ArrayList<>();
		ArrayList<String> MSubCategory = new ArrayList<>();
		
		List<List<String>> res = BNTopCategoryCampaignCall.menudetails(log);
		Menu = (ArrayList<String>) res.get(0);
		Category = (ArrayList<String>) res.get(1);
		SubCategory = (ArrayList<String>) res.get(2);
		
		try
		{
			//navigatetoURL();
			if(!BNBasicfeature.isElementPresent(menu))
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			jsclick(menu);
			wait.until(ExpectedConditions.visibilityOf(myAccount));
			wait.until(ExpectedConditions.visibilityOf(menuLists));
			List<WebElement> menuele = mainMenuLists;
			Actions act = new Actions(driver);
			for(int i = 0; i<menuele.size(); i++)
			{
				try
				{
					WebElement mName = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItem']//*[@id='sk_mobCategoryItemCont_id_"+i+"_0']"));
					String menuName = mName.getText();
					if(!menuName.isEmpty())
					{
						MMenu.add(menuName);
						jsclick(mName);
						wait.until(ExpectedConditions.attributeContains(By.xpath("((//*[@class='sk_mobCategoryItem'])["+(i+1)+"]//*[@class='sk_mobSubCategory'])[1]"), "style", "block"));
						Thread.sleep(100);
						BNBasicfeature.scrollup(mName, driver);
						wait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id='id_skCtgryItmShopCnt_"+i+"']//*[@class='sk_ctgryItmShopTxt']"))));
						/*WebElement shopall = driver.findElement(By.xpath("//*[@id='id_skCtgryItmShopCnt_"+i+"']//*[@class='sk_ctgryItmShopTxt']"));
						MCategory.add(shopall.getText());*/
						List<WebElement> catele = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category']"));
						for(int j = 1; j <= catele.size(); j++)
						{
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]")));
							WebElement cName = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]"));
							BNBasicfeature.scrollup(cName, driver);
							MCategory.add(cName.getText());
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@type='category'])["+j+"]"))));
							act.moveToElement(cName).click().build().perform();
							//Thread.sleep(2000);
							BNBasicfeature.scrollup(cName, driver);
							//Thread.sleep(100);
							List<WebElement> subcateele = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@class='sk_mobSubCategory'])["+j+"]//*[@level]"));
							for(int k = 1; k<=subcateele.size(); k++)
							{
								String nook = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel_"+i+"_0']//*[@class='sk_mobSubCategory'])["+j+"]//*[@level])["+k+"]")).getAttribute("aria-label");
								if(nook.equals("My NOOK Library"))
								{
									continue;
								}
								else
								{
									wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]")));
									WebElement subcName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+i+"_0']//*[@id='sk_mobSublevel__1'])["+j+"]//*[@class='sk_mobCategoryItemTxt'])["+k+"]"));
									BNBasicfeature.scrollup(subcName, driver);
									//Thread.sleep(100);
									MSubCategory.add(subcName.getText());
								}
							}
						}
					}
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					Exception("There is something missing." + e.getMessage());
					continue;
				}
			}
			
			if((Menu.size()>0) && (MMenu.size()>0))
			{
				log.add("Comparison results of the Classic site and Mobile Site Menu Names.");
				/*System.out.println(Menu.size());
				System.out.println(Menu);
				System.out.println(MMenu.size());
				System.out.println(MMenu);*/
				String MenuName = MMenu.toString();
				Menu.removeAll(MMenu);
				if(Menu.isEmpty())
				{
					log.add("The Menu Contents from the Site are : " + MenuName);
					Pass("The Menu Contents of both the Classic and Mobile Site Matches.",log);
				}
				else
				{
					log.add("The following menu is Missing/Mismatching : " + MMenu.toString());
					Fail("The Menu Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
			
			if((Category.size()>0) && (MCategory.size()>0))
			{
				log.add("Comparison results of the Classic site and Mobile Site Category Names.");
				/*System.out.println(Category.size());
				System.out.println(Category);
				System.out.println(MCategory.size());
				System.out.println(MCategory);*/
				String CategoryName = MCategory.toString();
				Category.removeAll(MCategory);
				if(Category.isEmpty())
				{
					log.add("The Category Contents from the Site are : " + CategoryName);
					Pass("The Category Contents of both the Classic and Mobile Site Matches.");
				}
				else
				{
					log.add("The following Category are Missing/Mismatching : " + MCategory.toString());
					Fail("The Category Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
			
			if((SubCategory.size()>0) && (MSubCategory.size()>0))
			{
				log.add("Comparison results of Classic site and Mobile Site Sub-Category Menu Names.");
				/*System.out.println(SubCategory.size());
				System.out.println(SubCategory);
				System.out.println(MSubCategory.size());
				System.out.println(MSubCategory);*/
				String SubCategoryName = MSubCategory.toString();
				SubCategory.removeAll(MSubCategory);
				if(SubCategory.isEmpty())
				{
					log.add("The Sub-Category Contents from the Site are : " + SubCategoryName);
					Pass("The Sub-Category Menu Contents of both the Classic and Mobile Site Matches.",log);
				}
				else
				{
					log.add("The following Sub-Category Menu are Missing/Mismatching : " + MSubCategory.toString());
					Fail("The Sub-Category Menu Contents of both the Classic and Mobile Site does not Match.",log);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 7 Issue ." + e.getMessage());
			Exception(" Chk - 7 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 10 Verify that while selecting the "Search" icon the search field, default search text,clear and cancel button should be displayed .*/
	public void searchField()
	{
		ChildCreation(" Chk - 10 Verify that while selecting the Search icon the search field, default search text,clear and cancel button should be displayed .");
		String canceltxt = "";
		try
		{
			canceltxt = BNBasicfeature.getExcelVal("Chk10", sheet, 2);
			int count = 1;
			do
			{
				try
				{
					driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
					driver.get(BNConstants.mobileSiteURL);
					break;
				}
				catch (Exception e)
				{
					driver.navigate().refresh();
					System.out.print("There is something wrong with the URL Please Check." + e.getMessage());
					count++;
				}
			}while(count<5);
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				log.add("The Search icon is displayed.");
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					log.add("The Search icon is Clicked and the Search Field is displayed.");
					if(BNBasicfeature.isElementPresent(searchContainerCancel))
					{
						Pass("The Cancel Button is displayed.");
						log.add( "The Expected Value was : " + canceltxt);
						String val = searchContainerCancel.getText();
						log.add( "The actual Value is : " + val);
						if(val.equals(canceltxt))
						{
							Pass("The Cancel Button text matches.",log);
						}
						else
						{
							Fail("The Cancel Button text does not matches.",log);
						}
					}
					else
					{
						Fail("The Cancel Button is not displayed.",log);
					}
				}
				else
				{
					Fail("The Search icon is Clicked and the Search Container is not displayed.",log);
				}
			}
			else
			{
				Fail("The Search icon is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" Chk - 10 Issue ." + e.getMessage());
			Exception(" Chk - 10 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 15 Verify that misspelled keyword should be displayed with error screen */
	public void nosearchresult()
	{
		ChildCreation(" Chk - 15 Verify that misspelled keyword should be displayed with error screen.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("Chk15", sheet, 2);
			String nores = BNBasicfeature.getExcelVal("Chk15", sheet, 3);
			getSearchResults(srchtxt);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			wait.until(ExpectedConditions.visibilityOf(noSearchResults));
			if(BNBasicfeature.isElementPresent(noSearchResults))
			{
				 String val = noSearchResults.getText();
				 log.add( "The Expected Value was : " + val );
				 log.add( "The actual Value is : " + nores);
				 if(val.contains(nores))
				 {
					 Pass("The No Suggested Result information is displayed.");
				 }
				 else
				 {
					 Fail("The No Suggested Result information is not displayed.");
				 }
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 15 Issue ." + e.getMessage());
			Exception(" Chk - 15 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 16 Verify that while selecting the cross mark, the entered values should be cleared from the search field box.*/
	public void searchClose()
	{
		ChildCreation(" Chk - 16 Verify that while selecting the cross mark, the entered values should be cleared from the search field box.");
		try
		{
			String srchtxt = BNBasicfeature.getExcelVal("Chk16", sheet, 2);
			if(BNBasicfeature.isElementPresent(searchClose))
			{
				jsclick(searchClose);
				if(searchContainer.getAttribute("value").isEmpty())
				{
				   Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
				}
				else
				{
				   Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
				}
			}
			else
			{
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					 getSearchResults(srchtxt);
					 wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
					 if(BNBasicfeature.isElementPresent(searchClose))
					 {
						jsclick(searchClose);
						if(searchContainer.getAttribute("value").isEmpty())
						{
						   Pass("The Search Keyword is removed from the Search Container and it is empty.",log);
						}
						else
						{
						   Fail("The Search Keyword is not removed from the Search Container and it is not empty.",log);
						}
					 }
					 else
					 {
						Fail( "The Search Close button is not displayed."); 
					 }
				}
				else
				{
					Fail( "The Search Icon is not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 16 Issue ." + e.getMessage());
			Exception(" Chk - 16 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 17 Verify whether clicking on cancel clears the text in the search field*/
	public void searchCancel()
	{
		ChildCreation(" Chk - 17 Verify whether clicking on cancel clears the text in the search field.");
		try
		{
			String srchtxt =  BNBasicfeature.getExcelVal("Chk17", sheet, 2);
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				getSearchResults(srchtxt);
				wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
				wait.until(ExpectedConditions.visibilityOf(searchResultsContainer));
				jsclick(searchContainerCancel);
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					Fail("The Search Container is not closed and it is still Open.",log);
				}
				else
				{
					Pass("The Search Container is closed and it is not Open.",log);
				}
			}
			else
			{
				Fail( "The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 17 Issue ." + e.getMessage());
			Exception(" Chk - 17 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 27 Verify that while selecting the Sign In option, the sign in page should be displayed */
	public void signInnav() throws InterruptedException, IOException
	{
		ChildCreation(" Chk - 27 Verify that while selecting the Sign In option, the sign in page should be displayed.");
		try
		{
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				String title = pgetitle.getText();
				String signIntitle = BNBasicfeature.getExcelVal("Chk27", sheet, 2);
				if(title.equals(signIntitle))
				{
					Pass("The User is navigated successfully to the SignIn page and it is Verified.",log);
				}
				else
				{
					Fail("There is something wrong the user is not in the Sign In Page.",log);
				}	
			}
			else
			{
				Fail( "The User failed to sign In.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" Chk - 27 Issue ." + e.getMessage());
			Exception(" Chk - 27 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 35 Verify that "Remember Me" option should be selected as default in the sign-in page.*/
	public void rememberMeCheckboxstate() throws IOException
	{
		ChildCreation(" Chk - 35 Verify that Remember Me option should be selected as default in the sign-in page.");
		try
		{
			//signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(rememberMe))
				{
					log.add(" The user is navigated and they are in the Sign In Page.");
					if(rememberMe.isSelected())
					{
						Fail("The Remember Me Checkbox is checked by default.",log);
					}
					else
					{
						Pass("The Remember Me Checkbox is not checked by default.",log);
					}
				}
				else
				{
					Fail( "The Remember Me Checkbox is not displayed.");
				}
			}
			else
			{
				Fail( "The User failed to sign In.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" Chk - 35 Issue ." + e.getMessage());
			Exception(" Chk - 35 Issue." + e.getMessage());
		}
	}
	
	/* Chk - 40 Verify that while selecting the "Create an Account" button from the "Sign In" overlay,the "Create Account" overlay should be displayed with the input fields, create account and cancel button.*/
	/* Chk - 41 Verify that First Name,Last Name,Email Address,Re-Enter Email Address,Password,Confirm Password,Security Question and Answer and Security Answer fields should be displayed in the "Create Account" overlay.*/
	/* Chk - 42 Verify that while selecting the Create Account button without entering any values in all the input fields,all the fields should be highlighted along with the alert message..*/
	/* Chk - 43 Verify that while selecting the Cancel button in the Create Account overlay,the previous page should be displayed. */
	public void createAccountPge() throws Exception
	{
		ChildCreation(" Chk - 40 Verify that while selecting the Create an Account button from the Sign In overlay,the Create Account overlay should be displayed with the input fields, create account and cancel button.");
		try
		{
			/*signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				
			}*/
			createAccPage = ldPage(signInlnk, createAccbtn, createAccFrame);
			if(createAccPage==true)
			{
				if(BNBasicfeature.isElementPresent(ccFName))
				{
					Pass("The First Name field is present.");
				}
				else
				{
					Fail("The First Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccLName))
				{
					Pass("The Last Name field is present.");
				}
				else
				{
					Fail("The Last Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccEmailId))
				{
					Pass("The Email Address field is present.");
				}
				else
				{
					Fail("The Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfEmailId))
				{
					Pass("The Confirm Email Address field is present.");
				}
				else
				{
					Fail("The Confirm Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccPassword))
				{
					Pass("The Password field is present.");
				}
				else
				{
					Fail("The Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfPass))
				{
					Pass("The Confirm Password field is present.");
				}
				else
				{
					Fail("The Confirm Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					Pass("The Security Question field is present.");
				}
				else
				{
					Fail("The Security Question field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityAnswer))
				{
					Pass("The Security Answer field is present.");
				}
				else
				{
					Fail("The Security Answer field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCreateBtn))
				{
					Pass("The Create Account button is present.");
				}
				else
				{
					Fail("The Create Account button is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCancelBtn))
				{
					Pass("The Cancel button is present.",log);
				}
				else
				{
					Fail("The Cancel button is not present.",log);
				}
			}
			else
			{
				Fail( "The User is not navigated to the Create Account page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 40 Issue ." + e.getMessage());
			Exception(" Chk - 40 Issue ." + e.getMessage());
		}
		
		/* Chk - 41 Verify that First Name,Last Name,Email Address,Re-Enter Email Address,Password,Confirm Password,Security Question and Answer and Security Answer fields should be displayed in the "Create Account" overlay.*/
		ChildCreation("  Chk - 41 Verify that First Name,Last Name,Email Address,Re-Enter Email Address,Password,Confirm Password,Security Question and Answer and Security Answer fields should be displayed in the Create Account overlay. ");
		if(createAccPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(ccFName))
				{
					Pass("The First Name field is present.");
				}
				else
				{
					Fail("The First Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccLName))
				{
					Pass("The Last Name field is present.");
				}
				else
				{
					Fail("The Last Name field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccEmailId))
				{
					Pass("The Email Address field is present.");
				}
				else
				{
					Fail("The Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfEmailId))
				{
					Pass("The Confirm Email Address field is present.");
				}
				else
				{
					Fail("The Confirm Email Address field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccPassword))
				{
					Pass("The Password field is present.");
				}
				else
				{
					Fail("The Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccCnfPass))
				{
					Pass("The Confirm Password field is present.");
				}
				else
				{
					Fail("The Confirm Password field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityQuestion))
				{
					Pass("The Security Question field is present.");
				}
				else
				{
					Fail("The Security Question field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(ccSecurityAnswer))
				{
					Pass("The Security Answer field is present.");
				}
				else
				{
					Fail("The Security Answer field is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCreateBtn))
				{
					Pass("The Create Account button is present.");
				}
				else
				{
					Fail("The Create Account button is not present.");
				}
				
				if(BNBasicfeature.isElementPresent(createAccountCancelBtn))
				{
					Pass("The Cancel button is present.",log);
				}
				else
				{
					Fail("The Cancel button is not present.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 40 Issue ." + e.getMessage());
				Exception(" Chk - 40 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The USer failed to load the create account page.");
		}
		
		/* Chk - 42 Verify that while selecting the Create Account button without entering any values in all the input fields,all the fields should be highlighted along with the alert message..*/
		ChildCreation(" Chk - 42 Verify that while selecting the Create Account button without entering any values in all the input fields,all the fields should be highlighted along with the alert message.");
		if(createAccPage==true)
		{
			try
			{
				webEle = new ArrayList<>();
				webEle.add(ccFName);
				webEle.add(ccLName);
				webEle.add(ccEmailId);
				webEle.add(ccCnfEmailId);
				webEle.add(ccPassword);
				webEle.add(ccCnfPass);
				webEle.add(ccSecurityAnswer);
				ccclearAll(webEle);
				if(fieldClear==true)
				{
					String error = BNBasicfeature.getExcelVal("Chk42", sheet, 2); 
					String Colorcomp = BNBasicfeature.getExcelVal("Chk42", sheet, 3); 
					jsclick(createAccountCreateBtn);
					if(BNBasicfeature.isElementPresent(erromsg))
					{
						BNBasicfeature.scrollup(erromsg, driver);
						wait.until(ExpectedConditions.visibilityOf(erromsg));
						log.add("The Expected alert message was : " + error);
						BNBasicfeature.scrollup(pgetitle, driver);
						String errormessage = erromsg.getText();
						log.add("The Actual alert message is : " + errormessage);
						if(error.equals(errormessage))
						{
							Pass("The raised error message matches the expected value when there is no data entered in the Create Account fields.",log);
						}
						else
						{
							Fail("The raised error message does not matches with the expected alert message.",log);
						}
						
						String fnamehex = ccFName.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(fnamehex).equals(Colorcomp))
						{
							Pass("The First Name field is highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(fnamehex));
						}
						else
						{
							Fail("The First Name field is not highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(fnamehex));
						}
								
						String lnamehex = ccLName.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(lnamehex).equals(Colorcomp))
						{
							Pass("The Last Name field is highlighted with the expected Color. The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(lnamehex));
						}
						else
						{
							Fail("The Last Name field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(lnamehex));
						}
								
						String emailhex = ccEmailId.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(emailhex).equals(Colorcomp))
						{
							Pass("The Email field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(emailhex));
						}
						else
						{
							Fail("The Email field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(emailhex));
						}
								
						String confirmemailhex = ccCnfEmailId.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(confirmemailhex).equals(Colorcomp))
						{
							Pass("The Confirm Email field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmemailhex));
						}
						else
						{
							Fail("The Confirm Email field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmemailhex));
						}
								
						String passhex = ccPassword.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(passhex).equals(Colorcomp))
						{
							Pass("The Password field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(passhex));
						}
						else
						{
							Fail("The Password field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(passhex));
						}
								
						String confirmpasshex = ccCnfPass.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(confirmpasshex).equals(Colorcomp))
						{
							Pass("The Confirm Password field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmpasshex));
						}
						else
						{
							Fail("The Confirm Password field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(confirmpasshex));
						}
								
						BNBasicfeature.scrolldown(ccCnfPass, driver);
						
						String secanshex = ccSecurityAnswer.getCssValue("border-color");
						if(BNBasicfeature.colorfinder(secanshex).equals(Colorcomp))
						{
							Pass("The Security Answer field is highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(secanshex));
						}
						else
						{
							Fail("The Security Answer field is not highlighted with the expected Color.The expected color is : " + Colorcomp + " and the actual Color of the text field is : " + BNBasicfeature.colorfinder(secanshex));
						}
					}
					else
					{
						Fail("The Error alert message is not displayed.");
					}
				}
				else
				{
					Fail("Issue in Clearing the fields.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 42 Issue ." + e.getMessage());
				Exception(" Chk - 42 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
		
		/* Chk - 43 Verify that while selecting the Cancel button in the Create Account overlay,the previous page should be displayed. */
		ChildCreation(" Chk - 43 Verify that while selecting the Cancel button in the Create Account overlay,the previous page should be displayed. ");
		if(createAccPage==true)
		{
			try
			{
				String Signintitle = BNBasicfeature.getExcelVal("Chk34", sheet, 2);
				jsclick(createAccountCancelBtn);
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
				wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
				Thread.sleep(1000);
				if(pgetitle.getText().equals(Signintitle))
				{
					Pass("The user is navigated and they are in the Sign In Page.");
					createAccPage = false;
				}
				else
				{
					Fail("The user is not navigated to the Sign In Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 43 Issue ." + e.getMessage());
				Exception(" Chk - 43 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Create Account Page.");
		}
	}
	
	/* Chk - 28 Verify that on selecting "Secure Sign In" without entering email address and password, the text fields should be highlighted along with the alert message */
	public void SecureSignInalert() throws InterruptedException
	{
		ChildCreation(" Chk - 28 Verify that on selecting Secure Sign In without entering email address and password, the text fields should be highlighted along with the alert message.");
		if(signedIn==true)
		{
			try
			{
				String actualalert = BNBasicfeature.getExcelVal("Chk28", sheet, 2);
				String originalColor = BNBasicfeature.getExcelVal("Chk28", sheet, 3);;
				jsclick(secureSignIn);
				wait.until(ExpectedConditions.visibilityOf(signInErralrt));
				String cssval;
				if(BNBasicfeature.isElementPresent(signInErralrt))
				{
					Pass("The Validation alert is raised and it is displayed. ");
					String valalert = signInErralrt.getText();
					if(actualalert.equals(valalert))
					{
						Pass("The Validation alert matches. " + valalert);
						log.add(" Checking the highlighted Color of the Email and Password Field.");
						cssval = emailId.getCssValue("border");
						String emailhexCode = BNBasicfeature.colorfinder(cssval);
						cssval = password.getCssValue("border");
						String pwdhxcnvtcode = BNBasicfeature.colorfinder(cssval);
						if(emailhexCode.equals(originalColor)&&(pwdhxcnvtcode.equals(originalColor)))
						{
							Pass(" Both the Email and Password field is highlighted after validation. The Color code expected was " + originalColor +" .Email text field highlighted color is " + emailhexCode + " and Password highlighted color is " + pwdhxcnvtcode + " and it is as expected.",log);
						}
						else
						{
							Fail("Both the Email and Password field is highlighted after validation. But there is mismatch in the highlighted color. Expected Color was " + originalColor + " but highligted color for the Email text field is " + emailhexCode + " and Password text field is " + pwdhxcnvtcode,log);
						}
					}
					else
					{
						Fail("The Validation alert does not match. " + valalert + " does not matches with the expected alert " + actualalert);
					}
				}
				else
				{
					Fail(" No Validation alert is not raised.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 28 Issue ." + e.getMessage());
				Exception(" Chk - 28 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* Chk - 30 Verify that "Forgot your password" link should be displayed below the password field*/
	public void frgtpasswordlinkNavigation() throws InterruptedException
	{
		ChildCreation(" Verify that while selecting the Forgot Your Password link in the sign-in page, the forgot password page should be displayed .");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(forgotPassword))
				{
					String pageTitle = BNBasicfeature.getExcelVal("Chk30", sheet, 2);
					log.add(" The user is navigated and they are in the Sign In Page.");
					wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
					frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
					if(frgtPassPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						log.add("The Expected title was : " + pageTitle);
						String actVal = pgetitle.getText();
						log.add("The actual title is : " + actVal);
						if(actVal.equals(pageTitle))
						{
							Pass("The user are successfully navigated to the Password Assistant Page.",log);
							frgtPassPage = true;
						}
						else
						{
							Fail("The user are not navigated to the Password Assistant Page.",log);
							System.out.println("User is not in the Forgot Password page.");
						}
					}
					else
					{
						Fail( "The User failed to load the Forgot Password page.");
					}
				}
				else
				{
					Fail( "The Forgot password field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 30 Issue ." + e.getMessage());
				Exception(" Chk - 30 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign In.");
		}
	}
	
	/* Chk - 31 Verify that "Send a link to my email" and "Answer a security question" options should be displayed in the forgot your password page */
	public void forgotpasswordpageelements()
	{
		ChildCreation(" Chk - 31 Verify that Send a link to my email and Answer a security question options should be displayed in the forgot your password page .");
		if(frgtPassPage==true)
		{
			try
			{
				String forgotPasstitle = BNBasicfeature.getExcelVal("Chk31", sheet, 2); 
				String validemailId = BNBasicfeature.getExcelVal("Chk31", sheet, 6); 
				if(BNBasicfeature.isElementPresent(emailId))
				{
					emailId.clear();
					emailId.sendKeys(validemailId);
					log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
					if(BNBasicfeature.isElementPresent(forgotContinueBtn))
					{
						frgtPassPage = false;
						jsclick(forgotContinueBtn);
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						if(pgetitle.getText().contains(forgotPasstitle))
						{
							frgtPassPage = true;
							Pass("The Forgot Password Assistant page title is displayed.");
							if(BNBasicfeature.isElementPresent(forgotEmailDisplay))
							{
								Pass("The entered Email field is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The entered Email field is not displayed in the Forgot Password Assistant page.");
							}
							
							if(BNBasicfeature.isElementPresent(forgotEmailEdit))
							{
								Pass("The Email Edit link is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The Email Edit link is not displayed in the Forgot Password Assistant page.");
							}
							
							if(BNBasicfeature.isElementPresent(forgotEmailRecoveryOption))
							{
								Pass("The Password recovery option by Email Option radio button is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The Password recovery option by Email Option radio button is not displayed in the Forgot Password Assistant page.");
							}
							
							if(BNBasicfeature.isElementPresent(forgotSecurityRecoveryOption))
							{
								Pass("The Password recovery option by Security Question Option radio button is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The Password recovery option by Security Question Option is not displayed in the Forgot Password Assistant page.");
							}
							
							if(BNBasicfeature.isElementPresent(forgotContinueBtn))
							{
								Pass("The Continue button is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The Continue button is not displayed in the Forgot Password Assistant page.");
							}
							
							if(BNBasicfeature.isElementPresent(forgotCancel))
							{
								Pass("The Cancel button is displayed in the Forgot Password Assistant page.");
							}
							else
							{
								Fail("The Cancel button is not displayed in the Forgot Password Assistant page.");
							}
						}
						else
						{
							Fail("The Forgot Password Assistant page title is not displayed.");
						}
					}
					else
					{
						Fail("The Continue button is not present in the Password Assistant Page.");
					}
				}
				else
				{
					Fail("The Forgot Password Email field or Continue Button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 31 Issue ." + e.getMessage());
				Exception(" Chk - 31 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* Chk - 32 Verify that by default the "Send a link to my email" options gets selected and while selecting it,the notification email should be  send successfully .*/
	public void forgotpasswordResetByEmailLink()
	{
		ChildCreation(" Chk - 32 Verify that by default the Send a link to my email options gets selected and while selecting it,the notification email should be  send successfully . ");
		if(frgtPassPage==true)
		{
			try
			{
				String Val = BNBasicfeature.getExcelVal("Chk32", sheet, 2);
				String[] SuccessMessage = Val.split("\n");
				String emailId = BNBasicfeature.getExcelVal("Chk31", sheet, 6); 
				
				boolean emailchk = forgotEmailRecoveryOption.isSelected();
				if(emailchk==false)
				{
					driver.findElement(By.xpath("(//*[@class='styled-radio'])[1]")).click();
				}
				
				jsclick(forgotContinueBtn);
				Thread.sleep(4000);
				if(BNBasicfeature.isElementPresent(forgotResetSuccessContainer))
				{
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess1))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess1.getText();
						log.add( "The Actual value was : " + actVal );
						log.add( "The expected value was : " + SuccessMessage[0] );
						if(actVal.contains(SuccessMessage[0]))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText());
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess1.getText() );
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
					
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess2))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess2.getText();
						log.add( "The Actual value was : " + actVal );
						String expVal = SuccessMessage[1].concat(emailId);
						log.add( "The expected value was : " + SuccessMessage[1] );
						if(actVal.contains(expVal))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content.The raised alert was " + forgotSecurityResetSuccess2.getText() );
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content. The raised alert was " + forgotSecurityResetSuccess2.getText());
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
					
					if(BNBasicfeature.isElementPresent(forgotSecurityResetSuccess3))
					{
						log.add("The user was able to reset the password using the Send a link to my email option.");
						String actVal = forgotSecurityResetSuccess3.getText();
						log.add( "The Actual value was : " + actVal );
						log.add( "The expected value was : " + SuccessMessage[2]);
						if(actVal.equals(SuccessMessage[2]))
						{
							Pass("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it matches the expected Content. The raised alert was " + forgotSecurityResetSuccess3.getText());
						}
						else
						{
							Fail("The user is navigated to the Password Reset Success message page and the Success instruction is displayed and it does not matche the expected Content.The raised alert was " + forgotSecurityResetSuccess3.getText() );
						}
					}
					else
					{
						Fail("The Success Message is not displayed." );
					}
				}
				else
				{
					Fail("The Success Message Container is not displayed." );
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 32 Issue ." + e.getMessage());
				Exception(" Chk - 32 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* Chk - 33 Verify that while selecting the  Answer a security question options , the user should enter the security answer and proceed further to reset the password.*/
	public void forgotpasswordSecurityQuestionVerification() throws IOException
	{
		ChildCreation(" Chk - 33 Verify that while selecting the  Answer a security question options , the user should enter the security answer and proceed further to reset the password.");
		if(frgtPassPage==true)
		{
			try
			{
				String emailID = BNBasicfeature.getExcelVal("Chk33", sheet, 2);
				String securityAnswer = BNBasicfeature.getExcelVal("Chk33", sheet, 3);
				//driver.get(BNConstants.mobileSiteURL);
				signedIn = signInExistingCustomer();
				if(signedIn==true)
				{
					wait.until(ExpectedConditions.elementToBeClickable(forgotPassword));
					frgtPassPage = ldPage(forgotContinueBtn, forgotPassword, forgotPasspageframe);
					if(frgtPassPage==true)
					{
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						if(BNBasicfeature.isElementPresent(emailId))
						{
							emailId.sendKeys(emailID);
							log.add("The Email Id is entered in the Password Assistant Page Email Address Field.");
							if(BNBasicfeature.isElementPresent(forgotContinueBtn))
							{
								jsclick(forgotContinueBtn);
								log.add("The Continue button in the Password Assistant is clicked.");
								wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
								wait.until(ExpectedConditions.visibilityOf(forgotContinueBtn));
								
								boolean secchk = forgotSecurityRecoveryOption.isSelected();
								if(secchk==false)
								{
									driver.findElement(By.xpath("(//*[@class='styled-radio'])[2]")).click();
								}
								wait.until(ExpectedConditions.visibilityOf(forgotSecurityAnswerFields));
								if(BNBasicfeature.isElementPresent(forgotSecurityAnswerFields))
								{
									forgotSecurityAnswerTextBox.sendKeys(securityAnswer);
									jsclick(forgotContinueBtn);
									wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(forgotPasspageframe));
									Thread.sleep(1000);
									
									if(BNBasicfeature.isElementPresent(forgotPasswordInfo))
									{
										Pass("The user is navigated to the Password Reset page and the Caption is displayed.");
									}
									else
									{
										Fail("The user is not navigated to the Password Reset page.");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordNew))
									{
										Pass("The user is navigated to the Password Reset page and the New Pasword field is displayed.");
									}
									else
									{
										Fail("The New Pasword field is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordNewCnfm))
									{
										Pass("The user is navigated to the Password Reset page and the Confirm New Pasword field is displayed.");
									}
									else
									{
										Fail("The Confirm New Pasword field is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotPasswordrules))
									{
										Pass("The user is navigated to the Password Reset page and the Forgot Password rules is displayed.");
									}
									else
									{
										Fail("The Forgot Password rules is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotContinueBtn))
									{
										Pass("The user is navigated to the Password Reset page and the Continue button is displayed.");
									}
									else
									{
										Fail("The Continue button is not displayed");
									}
									
									if(BNBasicfeature.isElementPresent(forgotCancel))
									{
										Pass("The user is navigated to the Password Reset page and the Cancel button is displayed.");
									}
									else
									{
										Fail("The Cancel button is not displayed");
									}
								}
								else
								{
									Fail("The Security answer field is not displayed." );
								}
							}
							else
							{
								Fail("The Continue button is not displayed." );
							}
						}
						else
						{
							Fail("The Email id is not displayed." );
						}
					}
					else
					{
						Fail("The Continue button is not displayed." );
					}
				}
				else
				{
					Fail( "The User failed to sign in.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 33 Issue ." + e.getMessage());
				Exception(" Chk - 33 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* Chk - 34 Verify that while selecting the continue button after entering the new password details, the  password should be reset for the particular user .*/
	public void forgotpasswordResetSuccess() throws IOException
	{
		ChildCreation(" Chk - 34 Verify that while selecting the continue button after entering the new password details, the  password should be reset for the particular user .");
		if(frgtPassPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("Chk34", sheet, 3);
				String[] Val = cellVal.split("\n");
				String SignpgeTitle = BNBasicfeature.getExcelVal("Chk34", sheet, 2);
				String cnfpassheader = Val[1];
				String cnfpassmessage = Val[2];
				
				if(BNBasicfeature.isElementPresent(forgotContinueBtn))
				{
					forgotPasswordNew.sendKeys(Val[0]);
					forgotPasswordNewCnfm.sendKeys(Val[0]);
					
					jsclick(forgotContinueBtn);
					Thread.sleep(3000);
					wait.until(ExpectedConditions.visibilityOf(forgotPasswordResetSuccessAlertHeader));
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertHeader))
					{
						log.add("The alert is displayed for the user.");
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = forgotPasswordResetSuccessAlertHeader.getText();
						log.add( "The actual value is : " + actVal);
						if(actVal.equals(cnfpassheader))
						{
							Pass("The alert is displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
						}
						else
						{
							Fail("The alert is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertHeader.getText());
						}
					}
					else
					{
						Fail("The Success Message Header is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertConfirmation))
					{
						log.add("The confirmation message is displayed for the user.");
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = forgotPasswordResetSuccessAlertConfirmation.getText();
						log.add( "The actual value is : " + cnfpassmessage);
						if(actVal.equals(cnfpassmessage))
						{
							Pass("The confirmation displayed for the user and the content matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
						}
						else
						{
							Fail("The confirmation is displayed for the user and the content does not matches the expected value. The raised success message was " + forgotPasswordResetSuccessAlertConfirmation.getText(),log);
						}
					}
					else
					{
						Fail("The Success Message Header is not displayed.");
					}
					if(BNBasicfeature.isElementPresent(forgotPasswordResetSuccessAlertCloseBtn))
					{
						Pass("The Close Button is visible for the User.");
						jsclick(forgotPasswordResetSuccessAlertCloseBtn);
						Pass("The Close Button is Clicked Successfully.");
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
						wait.until(ExpectedConditions.visibilityOf(pgetitle));
						Thread.sleep(1000);
						log.add( "The Expected value was : " + cnfpassheader);
						String actVal = pgetitle.getText();
						log.add( "The actual value is : " + SignpgeTitle);
						if(actVal.equals(SignpgeTitle))
						{
							Pass("The page is closed and the user is navigated to the Sign In Page Successfully.");
						}
						else
						{
							Fail("The page is closed and the user is not in the Sign In Page. Please Check.");
						}
					}
					else
					{
						Fail("The Close Button is not visible for the User.");
					}
				}
				else
				{
					Fail("The Continue button is not visible for the User. Please Check.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" Chk - 34 Issue ." + e.getMessage());
				Exception(" Chk - 34 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The Forgot Password Page is not loaded.");
		}
	}
	
	/* Chk - 36 Verify that user name should be displayed in the hamburger menu once the user gets logged-in successfully .*/
	/* Chk - 29 Verify that while selecting the "Secure Sign In" by entering the valid email address and password, the user should be logged-in successfully .*/
	public void successfulLogin() throws IOException
	{
		ChildCreation(" Chk - 36 Verify that user name should be displayed in the hamburger menu once the user gets logged-in successfully. ");
		loggedin = false;
		try
		{
			String emailidval = BNBasicfeature.getExcelVal("Chk36", sheet, 2);
			String passval = BNBasicfeature.getExcelVal("Chk36", sheet, 3);
			String welcomemsg = BNBasicfeature.getExcelVal("Chk36", sheet, 4);
			String username = BNBasicfeature.getExcelVal("Chk36", sheet, 5);
			signedIn = signInExistingCustomer();
			if(signedIn==true)
			{
				if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password))&&(BNBasicfeature.isElementPresent(secureSignIn)))
				{
					clearAll();
					log.add(" The user is navigated and they are in the Sign In Page.");
					emailId.sendKeys(emailidval);
					password.sendKeys(passval);
					jsclick(secureSignIn);
					String finalSt = welcomemsg.concat(username);
					try
					{
						wait.until(ExpectedConditions.visibilityOf(header));
						jsclick(menu);
						wait.until(ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"));
						wait.until(ExpectedConditions.elementToBeClickable(loggedInAccntName));
						String actName  =  loggedInAccntName.getText();
						log.add( "The Expected name was : " + finalSt);
						log.add( "The Actual name is : " + actName);
						if(actName.contains(finalSt))
						{
							Pass("The user is able to successfully login and they are in the Home Page. " + actName,log);
							loggedin = true;
						}
						else
						{
							Fail("The user is able to successfully login and they are in the Home Page. But there is mimatch in the name." + actName,log);
						}
					}
					catch(Exception e)
					{
						System.out.println("There is something wrong user is not signed in.");
						Exception(" There is something wrong with the page or element is not found. Please Check." + e.getMessage());
					}
				}
				else
				{
					Fail( "The Email or password field is not displayed.");
				}
			}
			else
			{
				Fail( "The User failed to load the sign in page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 36 Issue ." + e.getMessage());
			Exception(" Chk - 36 Issue ." + e.getMessage());
		}
		
		/* Chk - 29 Verify that while selecting the "Secure Sign In" by entering the valid email address and password, the user should be logged-in successfully .*/
		ChildCreation("Chk - 29 Verify that while selecting the Secure Sign In by entering the valid email address and password, the user should be logged-in successfully .");
		try
		{
			if(loggedin==true)
			{
				Pass("The user is able to successfully login and they are in the Home Page. " + loggedInAccntName.getText(),log);
			}
			else
			{
				Fail("The user is able to successfully login and they are in the Home Page. But there is mimatch in the name." + loggedInAccntName.getText(),log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 29 Issue ." + e.getMessage());
			Exception(" Chk - 29 Issue ." + e.getMessage());
		}
	}
	
	/* Chk - 37 Verify that when the user gets logged-in successfully, the already added item in the shopping bag page should be displayed .*/
	public void minicartShoppingBagSignInViewCount()
	{
		ChildCreation(" Chk - 37 Verify that when the user gets logged-in successfully, the already added item in the shopping bag page should be displayed .");
		if(loggedin==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(panCakeMenuMask))
				{
					Pass("The Menu is successfully clicked and the Submenus are displayed.",log);
					jsclick(panCakeMenuMask);
					log.add("The Menu Mask area is successfully clicked.");
					wait.until(ExpectedConditions.and(
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(menu)
							)
							);
					wait.until(ExpectedConditions.visibilityOf(bagIcon));
				}
				
				if(BNBasicfeature.isElementPresent(bagCount))
				{
					Pass("The User was Signed In and the promo carousel was displayed.",log);
					if(BNBasicfeature.isElementPresent(bagIcon))
					{
						log.add("The Shopping Bag is displayed.");
						if(BNBasicfeature.isElementPresent(bagCount))
						{
							log.add("The Shopping Bag Count is displayed.");
							boolean bgCnt = getBagCount();
							if(bgCnt==true)
							{
								int currVal = Integer.parseInt(bagCount.getText());
								log.add("The Actual Value is : " + currVal);
								if(currVal>=0)
								{
									Fail("The shopping Bag count for the signed in user is displayed and it is not 0.",log);
								}
								else
								{
									Pass("The shopping Bag count for the signed in user is displayed and it is not 0.",log);
								}
							}
							else
							{
								Fail( "The user failed to get the bagCount.");
							}
						}
						else
						{
							Fail("The Shopping Bag Count is not displayed.");
						}
					}
					else
					{
						Fail("The Shopping Bag is not displayed.");
					}
				}
				else
				{
					Fail("The User is not signed in.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 37 Issue ." + e.getMessage());
				Exception(" Chk - 37 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In.");
		}
	}
	
	/* Chk - 38 Verify that Sign out option should be displayed in the hamburger menu once the user gets logged-in successfully .*/
	/* Chk - 39 Verify that while sign-out option in the hamburger menu, the user should be logged-out successfully */
	public void signOut() 
	{
		ChildCreation(" Chk - 38 Verify that Sign out option should be displayed in the hamburger menu once the user gets logged-in successfully ." );
		boolean logOut = false;
		if(signedIn==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(menu));
				menu.click();
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				myAccount.click();
				Thread.sleep(1000);
				if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
				{
					Pass( "The Sign Out Button is displayed.");
					logOut = true;
				}
				else
				{
					Fail("The Signout button is not displayed.");
					logOut = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" Chk - 38 Issue ." + e.getMessage());
				Exception(" Chk - 38 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to Sign In.");
		}
		
		/* Chk - 39 Verify that while sign-out option in the hamburger menu, the user should be logged-out successfully */
		ChildCreation(" Chk - 39 Verify that while sign-out option in the hamburger menu, the user should be logged-out successfully .");
		try
		{
			boolean signedOut = false;
			if(logOut==true)
			{
				if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
				{
					try
					{
						signOut.click();
					}
					catch (Exception e)
					{
						signOutTemp.click();
					}
					wait.until(ExpectedConditions.or(
								ExpectedConditions.visibilityOf(signOutIndicator),
								ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign In")
								)
							);
					wait.until(ExpectedConditions.elementToBeClickable(menu));
					jsclick(menu);
					wait.until(ExpectedConditions.elementToBeClickable(myAccount));
					WebElement myaccounttext = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItemCont sk_My_Account']//*[@class='sk_mobCategoryItemTxt']"));
					if(myaccounttext.getText().contains("Sign In"))
					{
						signedOut = true;
						signedIn = false;
					}
					else
					{
						Fail("The User is not signed out yet.The actual text in the container is : " + myaccounttext.getText());
						signedOut = false;
					}
				}
				else
				{
					Fail("The Signout button is not displayed.");
				}
			}
			else
			{
				Fail( "The Logout Option is not displayed.");
			}
			
			if(signedOut==true)
			{
				Pass( "The User is Signed Out successfully.");
			}
			else
			{
				Fail( "The User is not Signed Out successfully.");
			}
			
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				Pass("The Menu is successfully clicked and the Submenus are displayed.",log);
				jsclick(panCakeMenuMask);
				log.add("The Menu Mask area is successfully clicked.");
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(bagIcon));
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 39 Issue ." + e.getMessage());
			Exception(" Chk - 39 Issue ." + e.getMessage());
		}
	}
	
	/* Chk - 18 Verify that while selecting the "Store Locator" option at the header, the store locator page should be displayed .*/
	/* Chk - 20 Verify that while selecting the Store locator icon after entering the store name or location or zip code,the respective store details should be displayed with the store name,location and zipcode. */
	/* Chk - 21 Verify that valid alert message should be displayed if there is no store details for the zipcode . */
	public void storeLocator()
	{
		ChildCreation(" Chk - 18 Verify that while selecting the Store Locator option at the header, the store locator page should be displayed . ");
		String origUrl = "";
		boolean storePgeloaded = false;
		try
		{
			if(BNBasicfeature.isElementPresent(panCakeMenuMask))
			{
				Pass("The Menu is successfully clicked and the Submenus are displayed.",log);
				jsclick(panCakeMenuMask);
				log.add("The Menu Mask area is successfully clicked.");
				wait.until(ExpectedConditions.and(
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@class,'showPanCakeMask')]")),
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(menu)
						)
						);
				wait.until(ExpectedConditions.visibilityOf(bagIcon));
			}
			if(!BNBasicfeature.isElementPresent(header))
			{
				driver.get(BNConstants.mobileSiteURL);
			}
			if(BNBasicfeature.isElementPresent(storeLocator))
			{
				Pass("The Store Locator is displayed.");
				origUrl = driver.getCurrentUrl();
				jsclick(storeLocator);
				Thread.sleep(1000);
				String currUrl = driver.getCurrentUrl();
				if(currUrl.contains("store")&&(!origUrl.equals(currUrl)))
				{
					Pass("The Old url and the new url does not matches and the user is redirected to the Stores page.");
				}
				else
				{
					Fail(" The user is not navigated to the Store Page.");
				}
				
				wait.until(ExpectedConditions.visibilityOf(storesNavTabs));
				if(BNBasicfeature.isElementPresent(storesNavTabs))
				{
					Pass("The User is navigated to the Stores pages.");
					storePgeloaded = true;
				}
				else
				{
					Fail(" The user is not navigated to the Store Page.");
				}
			}
			else
			{
				Fail("The Store Locator is not displayed.");
			}
			//driver.get(BNConstants.mobileSiteURL);
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 18 Issue ." + e.getMessage());
			Exception(" Chk - 18 Issue ." + e.getMessage());
		}
		
		ChildCreation(" Chk - 21 Verify that valid alert message should be displayed if there is no store details for the zipcode .");
		try
		{
			String zip = BNBasicfeature.getExcelVal("Chk21", sheet, 2);
			if(storePgeloaded==true)
			{
				Pass("The Store page was loaded successfully.");
				if(BNBasicfeature.isElementPresent(storesSearchField))
				{
					storesSearchField.click();
					storesSearchField.clear();
					storesSearchField.sendKeys(zip);
					Actions act = new Actions(driver);
					act.sendKeys(Keys.ENTER).build().perform();
					wait.until(ExpectedConditions.visibilityOf(storesNoSearchResult));
					if(BNBasicfeature.isElementPresent(storesNoSearchResult))
					{
						Pass("The Store Search No result is displayed.");
						if(BNBasicfeature.isListElementPresent(storesSearchPageResultsList))
						{
							Fail( "The Stores Search Result page is displayed.");	
						}
						else
						{
							Pass( "The Stores Search Result page is not displayed.");
						}
					}
					else
					{
						Fail("The Store Search result is not displayed.");
					}
				}
				else
				{
					Fail( "The Search box for the Stores field is not displayed.");
				}
			}
			else
			{
				Skip("The Store page was not loaded successfully.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 21 Issue ." + e.getMessage());
			Exception(" Chk - 21 Issue ." + e.getMessage());
		}
		
		ChildCreation(" Chk - 20 Verify that while selecting the Store locator icon after entering the store name or location or zip code,the respective store details should be displayed with the store name,location and zipcode.");
		try
		{
			String zip = BNBasicfeature.getExcelVal("Chk20", sheet, 2);
			if(storePgeloaded==true)
			{
				Pass("The Store page was loaded successfully.");
				if(BNBasicfeature.isElementPresent(storesSearchField))
				{
					storesSearchField.click();
					storesSearchField.clear();
					storesSearchField.sendKeys(zip);
					Actions act = new Actions(driver);
					act.sendKeys(Keys.ENTER).build().perform();
					wait.until(ExpectedConditions.visibilityOf(storesSearchResult));
					if(BNBasicfeature.isElementPresent(storesSearchResult))
					{
						Pass("The Store Search result is displayed.");
						if(BNBasicfeature.isListElementPresent(storesSearchPageResultsList))
						{
							if(storesSearchPageResultsList.size()>0)
							{
								Pass("The Stores list is displayed.");
							}
							else
							{
								Fail( "The Stores list is not displayed.");
							}
						}
						else
						{
							Fail( "The Stores Search Result page is not displayed.");
						}
					}
					else
					{
						Fail("The Store Search result is not displayed.");
					}
				}
				else
				{
					Fail( "The Search box for the Stores field is not displayed.");
				}
			}
			else
			{
				Skip("The Store page was not loaded successfully.");
			}
			driver.get(BNConstants.mobileSiteURL);
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 20 Issue ." + e.getMessage());
			Exception(" Chk - 20 Issue ." + e.getMessage());
		}
	}
	
	/* Chk - 11 Verify that while selecting the search icon  after entering search keywords,the search results page should be displayed.*/
	/* Chk - 13 Verify that while entering the search keyword in the search field,the search suggestion should be displayed ie(limit of 8 search suggestions).*/
	/* Chk - 14 Verify that while selecting the search suggestions in the dropdown,the respective search results page should be displayed .*/
	public void searchresultnavigation() throws InterruptedException
	{
		ChildCreation(" Chk - 13 Verify that while entering the search keyword in the search field,the search suggestion should be displayed ie(limit of 8 search suggestions).");
		String srchtxt = "";
		boolean pgOk = false;
		String originaltext = "";
		try
		{
			srchtxt = BNBasicfeature.getExcelVal("Chk11", sheet, 2);
			ArrayList<String> mobileSiteSrch= new ArrayList<>();
			wait.until(ExpectedConditions.visibilityOf(header));
			getSearchResults(srchtxt);
			wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
			if(BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				 for(WebElement srchsugg : searchResultsLists)
				 {
					 mobileSiteSrch.add(srchsugg.getText());
				 }
				 if((mobileSiteSrch.size()>0) && (mobileSiteSrch.size()<=8))
				 {	
					log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
					Pass("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion of only 8 and not more than 8. ");
			     }
				 else
			 	 {
			 		log.add("The fetched search suggestion are " + mobileSiteSrch.toString());
			 		Fail("The Search Results provided for the Keyword " + srchtxt + " provided search suggestion more than 8 or the list is empty. ");
			 	 }
			}
			else
			{
				Fail("The Search Results is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 13 Issue ." + e.getMessage());
			Exception(" Chk - 13 Issue ." + e.getMessage());
		}
		
		ChildCreation(" Chk - 14 Verify that while selecting the search suggestions in the dropdown,the respective search results page should be displayed .");
		boolean srcPage = false;
		try
		{
			if(BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				Random r = new Random();
				int sel = r.nextInt(searchResultsLists.size());
				if(sel>=0)
				{
					sel = 1;
				}
				Actions act = new Actions(driver);
				String selText = searchResultsLists.get(sel-1).getText();
				act.moveToElement(searchResultsLists.get(sel-1)).click().build().perform();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(homepageHeader));
				if(BNBasicfeature.isElementPresent(plpNoProductsFound))
				{
					pgOk=false;
				}
				else if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
				{
					wait.until(ExpectedConditions.visibilityOf(pdpPageProductNameContainer));
					originaltext = pdpPageProductNameContainer.getText();
					pgOk = true;
				}
				else if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
				{
					originaltext = searchPageProductCountContainer.getText();
					pgOk = true;
				}
				else
				{
					pgOk = false;	
				}
				
				if(pgOk==true)
				{
					log.add("The expected page was : " + selText);
					log.add("The actual page is : " + originaltext);
					if(originaltext.contains(selText))
					{
						Pass("The User is redirected to the expected page.",log);
						srcPage = true;
					}
					else
					{
						Fail("The User is not redirected to the expected page.",log);
						srcPage = false;
					}
				}
				else
				{
					Fail("The Search Results page is not loaded.");
				}
			}
			else if(BNBasicfeature.isElementPresent(noSearchResults))
			{
				Skip("No Value was returned for the searched products. The displayed value was : " + noSearchResults.getText());
			}
			else
			{
				Fail("The Search Results are not displayed and No Search Suggestion as well not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 14 Issue ." + e.getMessage());
			Exception(" Chk - 14 Issue ." + e.getMessage());
		}
		
		ChildCreation(" Chk - 11 Verify that while selecting the search icon  after entering search keywords,the search results page should be displayed.");
		try
		{
			if(srcPage==true)
			{
				Pass( "The User expected page was displayed.");	
			}
			else
			{
				Fail( "The User expected page was not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" Chk - 11 Issue ." + e.getMessage());
			Exception(" Chk - 11 Issue ." + e.getMessage());
		}
	}
	
}
