package bnPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import barnesnoble.BNMiniCart;
import bnConfig.BNConstants;
import bnConfig.BNXpaths;
import commonFiles.BNBasicfeature;

public class BNMiniCartObj extends BNMiniCart implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static boolean signedIn = false, stockAlert = false, signedOut = false, nookAdded = false;
	static int currVal = 0;
	static String setcardHolName = "", setAddress = "",type = "";
	static List<WebElement> shipOrbillCurrAddress = null;
	static int bgCount = 0;
	
	public BNMiniCartObj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, timeout);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+panCakeMenuMaskk+"")
	WebElement panCakeMenuMask;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuList;
	
	@FindBy(xpath = ""+bnLogoo+"")
	WebElement bnLogo;
	
	@FindBy(xpath = ""+myAccountt+"")
	WebElement myAccount;
	
	@FindBy(xpath = ""+signInn+"")
	WebElement signIn;
	
	@FindBy(xpath = ""+signOutt+"")
	WebElement signOut;
	
	@FindBy(xpath = ""+signOutTempp+"")
	WebElement signOutTemp;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	@FindBy(xpath = ""+productListPagee+"")
	WebElement productListPage;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy(xpath = ""+productIdd+"")
	WebElement productId;
	
	@FindBy(xpath = ""+pdpPageProductNamee+"")
	WebElement pdpPageProductName;
	
	@FindBy(xpath = ""+pdpPageOverlayContentt+"")
	WebElement pdpPageOverlayContent;
	
	@FindBy(xpath = ""+pdpPageProductNameContainerr+"")
	WebElement pdpPageProductNameContainer;
	
	@FindBy(xpath = ""+pdpPageATBBtnn+"")
	WebElement pdpPageATBBtn;
	
	@FindBy(xpath = ""+bagCountt+"")
	WebElement bagCount;
	
	@FindBy(xpath = ""+footerSignInn+"")
	WebElement footerSignIn;
	
	@FindBy(xpath = ""+miniCartLoadingGaugee+"")
	WebElement miniCartLoadingGauge;
	
	@FindBy(xpath = ""+miniCartShoppingBagStateOpenn+"")
	WebElement miniCartShoppingBagStateOpen;
	
	@FindBy(xpath = ""+miniCartShoppingBagStateClosee+"")
	WebElement miniCartShoppingBagStateClose;
	
	@FindBy(xpath = ""+miniCartShoppingBagTitlee+"")
	WebElement miniCartShoppingBagTitle;
	
	@FindBy(xpath = ""+miniCartShoppingItemCountInfoo+"")
	WebElement miniCartShoppingItemCountInfo;
	
	@FindBy(xpath = ""+miniCartShoppingCouponn+"")
	WebElement miniCartShoppingCoupon;
	
	@FindBy(xpath=""+mcContinueShoppingBtnn+"")
	WebElement mcContinueShoppingBtn;
	
	@FindBy(xpath = ""+miniCartShoppingBagMaskIdd+"")
	WebElement miniCartShoppingBagMaskId;
	
	@FindBy(xpath=""+miniCartContinueShoppingBtnTextt+"")
	WebElement miniCartContinueShoppingBtnText;
	
	@FindBy(xpath=""+miniCartSignInBtnn+"")
	WebElement miniCartSignInBtn;
	
	@FindBy(xpath = ""+miniCartShoppingCloseIconn+"")
	WebElement miniCartShoppingCloseIcon;
	
	@FindBy(xpath = ""+signInpageframee+"") 
	WebElement signInpageframe;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+emailIdd+"")
	WebElement emailId;
	
	@FindBy(xpath = ""+passwordd+"")
	WebElement password;
	
	@FindBy(xpath = ""+secureSignInn+"")
	WebElement secureSignIn;
	
	@FindBy(xpath = ""+miniCartCouponFieldd+"")
	WebElement miniCartCouponField;
	
	@FindBy(xpath = ""+miniCartPriceContainerr+"")
	WebElement miniCartPriceContainer;
	
	@FindBy(xpath = ""+miniCartPriceSubTotalTextt+"")
	WebElement miniCartPriceSubTotalText;
	
	@FindBy(xpath = ""+miniCartSubTotalPriceDetailss+"")
	WebElement miniCartSubTotalPriceDetails;
	
	@FindBy(xpath = ""+miniCartSubTotalEstimatedShippingLabell+"")
	WebElement miniCartSubTotalEstimatedShippingLabel;
	
	@FindBy(xpath = ""+miniCartSubTotalEstimatedShippingPricee+"")
	WebElement miniCartSubTotalEstimatedShippingPrice;
	
	@FindBy(xpath = ""+miniCartSubTotalEstimatedTaxLabell+"")
	WebElement miniCartSubTotalEstimatedTaxLabel;
	
	@FindBy(xpath = ""+miniCartSubTotalEstimatedTaxPricee+"")
	WebElement miniCartSubTotalEstimatedTaxPrice;
	
	@FindBy(xpath = ""+miniCartSubTotalDiscountLabell+"")
	WebElement miniCartSubTotalDiscountLabel;
	
	@FindBy(xpath = ""+miniCartSubTotalDiscountPricee+"")
	WebElement miniCartSubTotalDiscountPrice;
	
	@FindBy(xpath = ""+miniCartSubTotalLabell+"")
	WebElement miniCartSubTotalLabel;
	
	@FindBy(xpath = ""+miniCartSubTotalPricee+"")
	WebElement miniCartSubTotalPrice;
	
	@FindBy(xpath = ""+miniCartCouponDefaultTextt+"")
	WebElement miniCartCouponDefaultText;
	
	@FindBy(xpath = ""+miniCartCouponApplyButtonn+"")
	WebElement miniCartCouponApplyButton;
	
	@FindBy(xpath = ""+miniCartMySavedItemss+"")
	WebElement miniCartMySavedItems;
	
	@FindBy(xpath = ""+miniCartMySavedItemsTitlee+"")
	WebElement miniCartMySavedItemsTitle;
	
	@FindBy(xpath = ""+checkoutTopSubmitOrderr+"")
	WebElement checkoutTopSubmitOrder;
	
	@FindBy(xpath = ""+checkoutLogoo+"")
	WebElement checkoutLogo;
	
	@FindBy(xpath = ""+mcShoppingBagAlertt+"")
	WebElement mcShoppingBagAlert;
	
	@FindBy(xpath = ""+signOutIndicatorr+"")
	WebElement signOutIndicator;
	
	@FindBy(xpath = ""+pdpPageMiniCartflyy+"")
	WebElement pdpPageMiniCartfly;
	
	@FindBy(xpath = ""+pdpPageProductDropDownn+"")
	WebElement pdpPageProductDropDown;
	
	@FindBy(xpath = ""+pdpPageATBErrorr+"")
	WebElement pdpPageATBError;
	
	@FindBy(xpath = ""+productAddedSuccessIdd+"")
	WebElement productAddedSuccessId;
	
	@FindBy(xpath = ""+checkoutPageFramee+"") 
	WebElement checkoutPageFrame;
	
	@FindBy(xpath = ""+checkoutAsGuestbtnn+"")
	WebElement checkoutAsGuestbtn;
	
	@FindBy (xpath = ""+guestCheckoutAddPageTitlee+"")
	WebElement guestCheckoutAddPageTitle;
	
	@FindBy (xpath = ""+checkoutStepss+"")
	WebElement checkoutSteps;
	
	@FindBy (xpath = ""+miniCartCouponErrorr+"")
	WebElement miniCartCouponError;
	
	@FindBy (xpath = ""+pdpPageAddToWishListContainerr+"")
	WebElement pdpPageAddToWishListContainer;
	
	@FindBy (xpath = ""+mcUpdateAlertt+"")
	WebElement mcUpdateAlert;
	
	// List Items
	
	@FindBy(how = How.XPATH, using = ""+mcContinueToCheckOutBtnn+"")
	List<WebElement> mcContinueToCheckOutBtn;
	
	@FindBy(how = How.XPATH, using = ""+miniCartAddedItemListss+"")
	List<WebElement> miniCartAddedItemLists;
	
	@FindBy(how = How.XPATH, using = ""+miniCartAddedItemSaveForLaterr+"")
	List<WebElement> miniCartAddedItemSaveForLater;
	
	@FindBy(how = How.XPATH, using = ""+miniCartQtyFieldd+"")
	List<WebElement> miniCartQtyField;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemListss+"")
	List<WebElement> miniCartSavedItemLists;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtTitlee+"")
	List<WebElement> miniCartSavedItemPrdtTitle;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtAuthorr+"")
	List<WebElement> miniCartSavedItemPrdtAuthor;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtRatingg+"")
	List<WebElement> miniCartSavedItemPrdtRating;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtAddtoBagButtonn+"")
	List<WebElement> miniCartSavedItemPrdtAddtoBagButton;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtRemoveButtonn+"")
	List<WebElement> miniCartSavedItemPrdtRemoveButton;
	
	@FindBy(how = How.XPATH, using = ""+miniCartSavedItemPrdtImagee+"")
	List<WebElement> miniCartSavedItemPrdtImage;
	
	@FindBy(how = How.XPATH, using = ""+checkoutBillandShipContainerr+"")
	List<WebElement> checkoutBillandShipContainer;
	
	@FindBy(how = How.XPATH, using = ""+searchPageProductListss+"")
	List<WebElement> searchPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageProductTypess+"")
	List<WebElement> pdpPageProductTypes;
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	// Load the Mini Cart Feature
	public boolean loadMiniCart() throws Exception
	{
		boolean pgok = false;
		int count = 1;
		do
		{
			Thread.sleep(100);
			wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
			Thread.sleep(900);
			Actions act = new Actions(driver);
			act.moveToElement(bagIcon).click().build().perform();
			//jsclick(bagIcon);
			//Thread.sleep(1000);
			try
			{
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block;"));
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
				//wait.until(ExpectedConditions.visibilityOf(mcContinueToCheckOutBtn));
				Thread.sleep(1000);
				if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(0)))
				{
					pgok = true;
					break;
				}
				else if((BNBasicfeature.isElementPresent(mcContinueShoppingBtn)))
				{
					jsclick(miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					pgok = false;
					count++;
					continue;
				}
				else
				{
					jsclick(miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					pgok = false;
					count++;
					continue;
				}
			}
			catch(Exception e)
			{
				jsclick(miniCartShoppingBagMaskId);
				driver.navigate().refresh();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				count++;
				continue;
			}
		}
		while(pgok==false||count<10);
		return pgok;
	}
	
	// Mini Cart Get Product Name Details
	public String getprdtName(int sel)
	{
		WebElement prdtName = driver.findElement(By.xpath("//*[@class='sk_mobShoppingBagContainer']//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtTitle']"));
		BNBasicfeature.scrolldown(prdtName, driver);
		String prdtTitle = prdtName.getText();
		log.add("The Name of the product is : " + prdtTitle);
		return prdtTitle;
	}
	
	//Mini Cart Get Identifier
	public String getprdtIdentifier(int sel)
	{
		WebElement prdtIdentifier = driver.findElement(By.xpath("(//*[@class='sk_mobShoppingBagContainer']//*[contains(@id,'PdtWrapper')]//*[contains(@class,'PdtItemContainer')])["+(sel+1)+"]"));
		String prdtId = prdtIdentifier.getAttribute("identifier");
		//System.out.println(prdtId);
		return prdtId;
	}
	
	//Mini Cart Get Identifier
	public String getprdtType(int sel)
	{
		WebElement prdtType = driver.findElement(By.xpath("//*[@class='sk_mobShoppingBagContainer']//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[contains(@class,'pdtType')]"));
		//WebElement prdtType = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtTypeName']"));
		String productType = prdtType.getText();
		return productType;
	}
	
	//Mini Cart Get Update Button Status
	public boolean getRemoveBtnState(int sel)
	{
		boolean updatedBtnEnabled = false;
		try
		{
			WebElement updDateBtn = driver.findElement(By.xpath("//*[@class='sk_mobShoppingBagContainer']//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[contains(@class,'removeItem')]"));
			if(BNBasicfeature.isElementPresent(updDateBtn))
			{
				updatedBtnEnabled = true;
			}
			else
			{
				updatedBtnEnabled = false;
			}
		}
		catch(Exception e)
		{
			updatedBtnEnabled = false;
		}
		return updatedBtnEnabled;
	}
	
	//Mini Cart Get Update Button Status
	public boolean getSaveForLaterBtnState(int sel)
	{
		boolean updatedBtnEnabled = false;
		try
		{
			WebElement updDateBtn = driver.findElement(By.xpath("//*[@class='sk_mobShoppingBagContainer']//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[contains(@class,'saveLater')]"));
			if(BNBasicfeature.isElementPresent(updDateBtn))
			{
				updatedBtnEnabled = true;
			}
			else
			{
				updatedBtnEnabled = false;
			}
		}
		catch(Exception e)
		{
			updatedBtnEnabled = false;
		}
		
		return updatedBtnEnabled;
	}
	
	// GET CURRENT SHIPPING ADDRESS DETAILS
	public void getCurrShipAddressDetails(int j)
	{
		try
		{
			setcardHolName = "";
			setAddress = "";
			shipOrbillCurrAddress = null;
			setcardHolName = driver.findElement(By.xpath("((//*[@id='checkoutContainer']//*[contains(@class,'col')])["+(j)+"]//p)[1]")).getText();
			setAddress = driver.findElement(By.xpath("((//*[@id='checkoutContainer']//*[contains(@class,'col')])["+(j)+"]//p)[2]")).getText();
			shipOrbillCurrAddress = driver.findElements(By.xpath("(//*[@id='reviewInfo']//*[contains(@class,'col')])["+(j)+"]//p[position()<=3]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting Current Shipping Address Details.");
			Exception("Error in getting Current Shipping Address Details.");
		}
	}
	
	// Get Bag Count Value
	public boolean getBagCount()
	{
		boolean bagCountOk = false;
		int count = 1;
		int refCount = 1;
		do
		{
			for( int i = 0; i<100; i++)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(bagCount));
					wait.until(ExpectedConditions.elementToBeClickable(bagCount));
					//String bagCnt = bagCount.getText();
					String bagCnt = bagCount.getText().replace("+", "");
					if(bagCnt.isEmpty())
					{
						count++;
						continue;
					}
					else
					{
						bgCount = Integer.parseInt(bagCnt);
						bagCountOk = true;
						break;
					}
				}
				catch(Exception e)
				{
					count++;
				}
			}
			if(count>=100)
			{
				driver.navigate().refresh();
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(bagCount));
				count = 1;
				refCount++;
			}
		}while((bagCountOk==false)&&(refCount<=3));
		return bagCountOk;
	}
	
	//Sign Out
	public boolean signOut() throws Exception 
	{
		signedOut = false;
		try
		{
			if(BNBasicfeature.isElementPresent(checkoutLogo))
			{
				BNBasicfeature.scrollup(checkoutLogo, driver);
				driver.get(BNConstants.mobileSiteURL);
			}
			else if(BNBasicfeature.isElementPresent(header))
			{
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					jsclick(miniCartShoppingBagMaskId);
					wait.until(ExpectedConditions.visibilityOf(menu));
					Thread.sleep(100);
				}
				if(BNBasicfeature.isElementPresent(panCakeMenuMask))
				{
					jsclick(panCakeMenuMask);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(menu));
				}
				if(BNBasicfeature.isElementPresent(menu))
				{
					wait.until(ExpectedConditions.visibilityOf(menu));
					jsclick(menu);
					wait.until(ExpectedConditions.visibilityOf(menuList));
					wait.until(ExpectedConditions.elementToBeClickable(myAccount));
					Thread.sleep(500);
					myAccount.click();
					Thread.sleep(1000);
					if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
					{
						try
						{
							signOut.click();
						}
						catch (Exception e)
						{
							signOutTemp.click();
						}
						wait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(signOutIndicator),
									ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign In")
									)
								);
						wait.until(ExpectedConditions.elementToBeClickable(menu));
						jsclick(menu);
						wait.until(ExpectedConditions.elementToBeClickable(myAccount));
						WebElement myaccounttext = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItemCont sk_My_Account']//*[@class='sk_mobCategoryItemTxt']"));
						if(myaccounttext.getText().contains("Sign In"))
						{
							signedOut = true;
							signedIn = false;
						}
						else
						{
							Fail("The User is not signed out yet.The actual text in the container is : " + myaccounttext.getText());
							signedOut = false;
						}
					}
					else
					{
						Fail("The Signout button is not displayed.");
					}
				}
			}
			else
			{
				driver.get(BNConstants.mobileSiteURL);
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(menu));
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuList));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				myAccount.click();
				Thread.sleep(1000);
				if(BNBasicfeature.isElementPresent(signOut)||BNBasicfeature.isElementPresent(signOutTemp))
				{
					//log.add("The Signout button is displayed.");
					try
					{
						signOut.click();
					}
					catch (Exception e)
					{
						signOutTemp.click();
					}
					wait.until(ExpectedConditions.or(
								ExpectedConditions.visibilityOf(signOutIndicator),
								ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign In")
								)
							);
					wait.until(ExpectedConditions.elementToBeClickable(menu));
					menu.click();
					wait.until(ExpectedConditions.elementToBeClickable(myAccount));
					WebElement myaccounttext = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItemCont sk_My_Account']//*[@class='sk_mobCategoryItemTxt']"));
					if(myaccounttext.getText().contains("Sign In"))
					{
						//log.add("The User is signed out successfully.");
						signedOut = true;
						signedIn = false;
					}
					else
					{
						//Fail("The User is not signed out yet.The actual text in the container is : " + myaccounttext.getText());
						signedOut = false;
					}
				}
				else
				{
					//Fail("The Signout button is not displayed.");
				}
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			signedOut = false;
			signedIn = false;
			//Exception("There is something wrong." + e.getMessage());
		}
		jsclick(miniCartShoppingBagMaskId);
		Thread.sleep(500);
		return signedOut;
	}
	
	/* Search EAN Numbers */
	public boolean searchEanNumbers(String ean) throws Exception
	{
		boolean pdpPageloaded = false;
		try
		{
			do
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(header));
					//BNBasicfeature.scrollup(header, driver);
					if(BNBasicfeature.isElementPresent(searchIcon))
					{
						Actions act = new Actions(driver);
						jsclick(searchIcon);
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
						act.moveToElement(searchContainer).click().sendKeys(ean).sendKeys(Keys.ENTER).build().perform();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(homepageHeader));
						//driver.manage().deleteAllCookies();
						//driver.navigate().refresh();
						if(BNBasicfeature.isElementPresent(plpNoProductsFound))
						{
							pdpPageloaded = false;
							continue;
						}
						else
						{
							wait.until(ExpectedConditions.visibilityOf(productId));
							wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
							if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
							{
								pdpPageloaded = true;
								//Thread.sleep(500);
								break;
							}
							else if(BNBasicfeature.isElementPresent(plpNoProductsFound))
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
							else
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
						}
					}
					else
					{
						System.out.println("The Search Icon is not found.Please Check.");
					}
				}
				catch(Exception e)
				{
					System.out.println("In Catch Block 1");
					System.out.println(e.getMessage());
					//driver.navigate().refresh();
				}
			}while(pdpPageloaded==true);
		}
		catch (Exception e) 
		{
			System.out.println("In Outer Catch Block 2");
			System.out.println(e.getMessage());
		}
		return pdpPageloaded;
	}
	
	// Add to Bag
	public boolean clickaddtoBag(int prevVal, WebElement ATBBtn) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			prdadded = false;
			int count = 1;
			//Thread.sleep(1000);
			do
			{
				WebDriverWait newWait = new WebDriverWait(driver, 5);
				try
				{
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						newWait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(500);
						currVal = Integer.parseInt(bagCount.getText());
						if(currVal>prevVal)
						{
							prdadded = true;
							//break;
						}
						
						jsclick(ATBBtn);
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
						//BNBasicfeature.scrollup(promoCarousel, driver);
						String error = pdpPageATBError.getAttribute("style");
						if(error.contains("block"))
						{
							driver.navigate().refresh();
							wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOf(bagCount)
									)
								);
							wait.until(ExpectedConditions.visibilityOf(header));
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
						}
						
						if(BNBasicfeature.isElementPresent(productAddedSuccessId))
						{
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
							else
							{
								Thread.sleep(1000);
								jsclick(ATBBtn);
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
								count++;
								continue;
							}
						}
						else
						{
							count++;
							jsclick(ATBBtn);
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
							continue;
						}
					}
					else
					{
						continue;
					}
				}
				catch(Exception e)
				{
					driver.navigate().refresh();
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(bagCount)
							)
						);
					wait.until(ExpectedConditions.visibilityOf(header));
					
					boolean bgCnt1 = getBagCount();
					if(bgCnt1==true)
					{
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(100);
						currVal = Integer.parseInt(bagCount.getText());
					}
					else
					{
						continue;
					}
					
					if(currVal>prevVal)
					{
						prdadded = true;
						//break;
					}
				}
			}while((prdadded==false)&&(count <= 5));
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}
	
	// Add a Product to the Bag.
	public void searchProduct(boolean val,String id) throws Exception
	{
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = id;
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String searchvalue = sheet.getRow(i).getCell(5).getStringCellValue();
				String[] searchcont = searchvalue.split("\n"); 
				try
				{
					//wait.until(ExpectedConditions.visibilityOf(promoCarousel));
					wait.until(ExpectedConditions.visibilityOf(header));
					if(BNBasicfeature.isElementPresent(searchIcon))
					{
						//Child.log(LogStatus.INFO, "The Search Icon is present and it is visible.");
						Actions act = new Actions(driver);
						jsclick(searchIcon);
						//act.build().perform();
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
						act.moveToElement(searchContainer).click().sendKeys(searchcont[0]).sendKeys(Keys.ENTER).build().perform();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(homepageHeader));
						//driver.manage().deleteAllCookies();
						//driver.navigate().refresh();
						wait.until(ExpectedConditions.visibilityOf(productListPage));
						if(BNBasicfeature.isElementPresent(plpNoProductsFound))
						{
							continue;
						}
						else
						{
							break;
						}
					}
					else
					{
						Fail("The Search Icon is not displayed.");
						System.out.println("The Search Icon is not found.Please Check.");
					}
				}
				catch (Exception e)
				{
					Exception("There is something wrong." + e.getMessage());
					System.out.println(e.getMessage());
				}
			}
		}
	}
	
	/* Select Product From PLP Page*/
	public boolean selectProduct() throws Exception
	{
		boolean bookfound = false;
		try
		{
			do
			{
				wait.until(ExpectedConditions.visibilityOf(productListPage));
				if(BNBasicfeature.isElementPresent(productListPage))
				{
					if(BNBasicfeature.isListElementPresent(searchPageProductLists))
					{
						Random r = new Random();
						int sel = r.nextInt(searchPageProductLists.size());
						Thread.sleep(500);
						//if(sel>=0)
						if(sel<1)
						{
							sel=1;
						}
						Thread.sleep(100);
						BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
						WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
						//String prdtname = prdtlist.getText();
						//System.out.println(prdtname);
						Thread.sleep(100);
						Actions act = new Actions(driver);
						act.moveToElement(prdtlist).click().build().perform();
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
						if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
						{
							bookfound = true;	
						}
						else
						{
							bookfound = false;
							driver.navigate().back();
							wait.until(ExpectedConditions.visibilityOf(header));
							wait.until(ExpectedConditions.visibilityOf(productListPage));
						}
						//prdtlist.click();
					}
					else
					{
						Fail("The PDP Page Product list is not found.");
					}
				}
				else
				{
					Fail("The PLP Page is not displayed.");
				}
			}while(bookfound==false);
		}
		catch(Exception e)
		{
			Exception("There is something wrong with the Page. The Search PLP Page is not displayed." + e.getMessage());
		}
		return bookfound;
	}
	
	/*******************************************MIni Cart *****************************************************************************/
	/* BRM - 45 Cart : Verify that by default the empty shopping bag icon should be displayed at the header before signing in.*/
	public void miniCartShopBagPresent()
	{
		ChildCreation(" BRM - 45 Cart : Verify that by default the empty shopping bag icon should be displayed at the header before signing in.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			//Thread.sleep(500);
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				Pass("The Shopping Bag is displayed.");
			}
			else
			{
				Fail("The Shopping Bag is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 45 Issue ." + e.getMessage());
			Exception(" BRM - 45 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 341 Verify whether the shopping bag icon should be displayed at the header as per the creative.*/
	public void miniCartShoppingBagIcon()
	{
		ChildCreation(" BRM - 341 Verify whether the shopping bag icon should be displayed at the header as per the creative.");
		try
		{
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				Pass("The Shopping Bag is displayed.");
				if(BNBasicfeature.isElementPresent(bagCount))
				{
					Pass( "The bag Count is displayed.");
					boolean bgCount = getBagCount();
					if(bgCount==true)
					{
						wait.until(ExpectedConditions.textToBePresentInElement(bagCount, "0"));
						int currVal = Integer.parseInt(bagCount.getText());
						if(currVal==0)
						{
							Pass( "The Bag Count is as expected.The Current Bag count is  : " + currVal);
						}
						else
						{
							Fail( "The Bag Count is not as expected. The Current Bag count is  : " + currVal);
						}
					}
					else
					{
						Fail("The User failed to get the bag count.");
					}
				}
				else
				{
					Fail( "The bag Count is not displayed.");
				}
			}
			else
			{
				Fail("The Shopping Bag is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 341 Issue ." + e.getMessage());
			Exception(" BRM - 341 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 342 Verify whether the shopping bag icon count should displayed as empty as default.*/
	public void miniCartShoppingBagCount()
	{
		ChildCreation(" BRM - 342 Verify whether the shopping bag icon count should displayed as empty as default.");
		try
		{
			String bgCount = BNBasicfeature.getExcelNumericVal("BRM342", sheet, 2);
			if(BNBasicfeature.isElementPresent(bagCount))
			{
				Pass("The Shopping Bag Count is displayed.");
				log.add("The Expected Count was : " + bagCount);
				boolean bCnt = getBagCount();
				if(bCnt==true)
				{
					String actualCount = bagCount.getText();
					if(bgCount.equals(actualCount))
					{
						Pass("The Bag Count is as expected.",log);
					}
					else
					{
						Fail("The Bag Count is not what was expected.",log);
					}
				}
				else
				{
					Fail( "The User failed to get the bag count.");
				}
			}
			else
			{
				Fail("The Shopping Bag Count is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 342 Issue ." + e.getMessage());
			Exception(" BRM - 342 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 343 Verify whether clicking on mini cart the page slides from right to left displaying shopping bag page and clicking again slide back closing the shopping bag page.*/
	/* BRM - 471 Verify whether clicking on x on top closes the shopping bag page and displays the home page/previous page. */
	public void mincartShoppingBagOpenClose()
	{
		ChildCreation(" BRM - 343 Verify whether clicking on mini cart the page slides from right to left displaying shopping bag page and clicking again slide back closing the shopping bag page.");
		boolean BRM471 = false;
		try
		{
			//Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				//act.moveToElement(bagIcon).click().build().perform();
				wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
				Thread.sleep(100);
				jsclick(bagIcon);
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block"));
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
				wait.until(ExpectedConditions.visibilityOf(miniCartShoppingBagStateOpen));
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					Pass("The Shopping Bag icon was clicked and the Mini Cart fly is displayed.");
					if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
					{
						jsclick(miniCartShoppingCloseIcon);
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
						if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateClose))
						{
							Pass("The Shopping bag is closed.");
							BRM471 = true;
						}
						else
						{
							Fail("The Shopping Bag is not closed.");
						}
					}
					else
					{
						Fail("The Shopping Bag is not closed.");
					}
				}
				else
				{
					Fail("The Shopping Bag icon was clicked and the Mini Cart fly is not displayed.");
				}
			}
			else
			{
				Fail( " The Bag icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 343 Issue ." + e.getMessage());
			Exception(" BRM - 343 Issue." + e.getMessage());
		}
		
		/* BRM - 471 Verify whether clicking on x on top closes the shopping bag page and displays the home page/previous page. */
		ChildCreation(" BRM - 471 Verify whether clicking on x on top closes the shopping bag page and displays the home page/previous page.");
		if(BRM471==true)
		{
			Pass("The Tab Header Section is displayed.");
		}
		else
		{
			Fail("The Tab Header Section is not displayed and the user is not navigated to the home page.");
		}
	}
	
	/* BRM - 46 Cart : Verify that the shopping cart page should be displayed on selecting the "Bag" icon as per the creative.*/
	public void miniCartShoppingBagSection()
	{
		ChildCreation(" BRM - 46 Cart : Verify that the shopping cart page should be displayed on selecting the Bag icon as per the creative.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM46", sheet, 2);
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				log.add("The Shopping Bag is displayed / present.");
				/*Actions act = new Actions(driver);
				act.moveToElement(bagIcon).click().build().perform();*/
				wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
				Thread.sleep(100);
				jsclick(bagIcon);
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
				wait.until(ExpectedConditions.visibilityOf(miniCartShoppingBagStateOpen));
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					Pass("The Shopping bag was clicked and the container is displayed.");
					wait.until(ExpectedConditions.visibilityOf(miniCartShoppingBagTitle));
					if(BNBasicfeature.isElementPresent(miniCartShoppingBagTitle))
					{
						log.add("The Shopping Bag title is displayed.");
						log.add("The Expected title was : " + cellVal);
						String actualTitle = miniCartShoppingBagTitle.getText();
						log.add("The Actual title is : " + actualTitle);
						if(actualTitle.equals(cellVal))
						{
							Pass("The title matches the expected content.",log);
						}
						else
						{
							Fail("The title doesn't matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Shoppin Bag title is not displayed.");
					}
				}
				else
				{
					Fail("The Shopping bag was clicked and the container is not displayed.");
				}
			}
			else
			{
				Fail("The Shopping Bag is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 46 Issue ." + e.getMessage());
			Exception(" BRM - 46 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 344 Verify whether the shopping bag page has title ,number of items purchased(eg"0 item /3 item) ,close option and alerts (if any) at the top as per the creative.*/
	public void mincartShoppingBagEmptyDetails()
	{
		ChildCreation(" BRM - 344 Verify whether the shopping bag page has title ,number of items purchased(eg0 item /3 item) ,close option and alerts (if any) at the top as per the creative.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM344", sheet, 2);
			String[] Val = cellVal.split("\n");
			if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
			{
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagTitle))
				{
					Pass("The Mini Cart Title is displayed.");
					log.add("The Expected Title was : " + Val[0]);
					String actualTitle = miniCartShoppingBagTitle.getText();
					log.add("The Actual Title was : " + actualTitle);
					if(Val[0].contains(actualTitle))
					{
						Pass("The Title matches the expected content.",log);
					}
					else
					{
						Fail("The Title does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Mini Cart Title is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(miniCartShoppingItemCountInfo))
				{
					Pass("The Mini Cart Item Count is displayed.");
					log.add("The Expected Value was : " + Val[1]);
					String itemCount = miniCartShoppingItemCountInfo.getText();
					if(itemCount.contains(Val[1]))
					{
						Pass("The Count matches the expected content.",log);
					}
					else
					{
						Fail("The Count does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Mini Cart Item Count is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
				{
					Pass("The Mini Cart Close Icon is displayed.");
				}
				else
				{
					Fail("The Mini Cart Close Icon is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(miniCartShoppingCoupon))
				{
					Pass("The Mini Cart Coupon Section is displayed.");
					log.add("The Expected Content was : " + Val[2]);
					String actualCoupon = miniCartShoppingCoupon.getText();
					/*System.out.println(actualCoupon);
					System.out.println(Val[2]);*/
					if(actualCoupon.contains(Val[2]))
					{
						Pass("The Coupon matches the expected content.",log);
					}
					else
					{
						Fail("The Coupon does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Mini Cart Coupon Section is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(mcContinueShoppingBtn))
				{
					Pass("The Continue button is displayed in the Mini Cart fly out.");
					log.add("The Expected Title was : " + Val[4]);
					if(BNBasicfeature.isElementPresent(miniCartContinueShoppingBtnText))
					{
						String btnTitle = miniCartContinueShoppingBtnText.getText();
						log.add("The Actual Shopping Button text is : " + btnTitle);
						if(Val[4].contains(btnTitle))
						{
							Pass("The Title matches the expected content.",log);
						}
						else
						{
							Fail("The Title does not matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Continue Shopping Button text is not displayed.",log);
					}
				}
				else
				{
					Fail("The Continue button is not displayed in the Mini Cart fly out.");
				}
				
				if(BNBasicfeature.isElementPresent(miniCartSignInBtn))
				{
					Pass("The Sign In button is displayed in the Mini Cart fly out.");
					log.add("The Expected Title was : " + Val[5]);
					String actualTitle = miniCartSignInBtn.getText();
					if(Val[5].contains(actualTitle))
					{
						Pass("The Title matches the expected content.",log);
					}
					else
					{
						Fail("The Title matches does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Sign In button is not displayed in the Mini Cart fly out.");
				}
			}
			else
			{
				Fail( "The Mini Cart is not in open state.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 344 Issue ." + e.getMessage());
			Exception(" BRM - 344 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 348 Verify whether the number of items displayed at the top should be "0 item" before shopping in the shopping bag page.*/
	public void mincartShoppingBagEmptyItemCount()
	{
		ChildCreation(" BRM - 348 Verify whether the number of items displayed at the top should be 0 item before shopping in the shopping bag page.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM348", sheet, 2);
			if(BNBasicfeature.isElementPresent(miniCartShoppingItemCountInfo))
			{
				Pass("The Mini Cart Item Count is displayed.");
				log.add("The Expected Value was : " + cellVal);
				String itemCount = miniCartShoppingItemCountInfo.getText();
				if(itemCount.contains(cellVal))
				{
					Pass("The Count matches the expected content.",log);
				}
				else
				{
					Fail("The Count does not matches the expected content.",log);
				}
			}
			else
			{
				Fail("The Mini Cart Item Count is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 348 Issue ." + e.getMessage());
			Exception(" BRM - 348 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 359 Verify whether "Continue Shopping" button is present when no items are purchased and clicking on it should load the respective page.*/
	public void minicartShoppingBagContinueBtnNavigation()
	{
		ChildCreation(" BRM - 359 Verify whether Continue Shopping button is present when no items are purchased and clicking on it should load the respective page.");
		try
		{
			Actions act = new Actions(driver);
			String cellVal = BNBasicfeature.getExcelVal("BRM359", sheet, 2);
			if(BNBasicfeature.isElementPresent(mcContinueShoppingBtn))
			{
				Thread.sleep(250);
				Pass("The Continue button is displayed in the Mini Cart fly out.");
				log.add("The Expected Title was : " + cellVal);
				if(BNBasicfeature.isElementPresent(miniCartContinueShoppingBtnText))
				{
					String btnTitle = miniCartContinueShoppingBtnText.getText();
					log.add("The Actual Shopping Button text is : " + btnTitle);
					if(cellVal.contains(btnTitle))
					{
						Pass("The Title matches the expected content.",log);
						act.moveToElement(mcContinueShoppingBtn).doubleClick().build().perform();
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
						Thread.sleep(200);
						if((BNBasicfeature.isElementPresent(menu))&&(BNBasicfeature.isElementPresent(searchIcon))&&(BNBasicfeature.isElementPresent(miniCartShoppingBagStateClose)))
						{
							Pass("The User is navigated to the respective page and the minicart is closed.");
						}
						else
						{
							Fail("The user is not navigated to the respective page. ");
						}
					}
					else
					{
						Fail("The Title does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Continue Shopping Button text is not displayed.",log);
				}
			}
			else
			{
				Fail("The Continue button is not displayed in the Mini Cart fly out.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 359 Issue ." + e.getMessage());
			Exception(" BRM - 359 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 405 Verify whether Sign In to see your shopping Bag button is present when no items are shopped and before signing in, also clicking on it should display the sign in overlay.*/
	public void minicartShoppingBagSignInNavigation()
	{
		ChildCreation(" BRM - 405 Verify whether Sign In to see your shopping Bag button is present when no items are shopped and before signing in, also clicking on it should display the sign in overlay.");
		try
		{
			String titleCellVal = BNBasicfeature.getExcelVal("BRM405", sheet, 2);
			if(BNBasicfeature.isElementPresent(bagIcon))
			{
				log.add("The shopping bag icon is displayed.");
				//System.out.println(shoppingbagCount.getText());
				boolean bgCnt = getBagCount();
				if(bgCnt==true)
				{
					//wait.until(ExpectedConditions.textToBePresentInElement(bagCount, "0"));
					int currVal = Integer.parseInt(bagCount.getText());
					if(currVal==0)
					{
						log.add("The Shopping Bag count is 0 and no items is purchased.");
						Pass( "The Bag Count is as expected.The Current Bag count is  : " + currVal);
						boolean pgok = false;
						int count = 1;
						if(!BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
						{
							do
							{
								Actions act = new Actions(driver);
								act.moveToElement(bagIcon).click().build().perform();
								//Thread.sleep(1000);
								try
								{
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block;"));
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
									Thread.sleep(1000);
									if(BNBasicfeature.isElementPresent(mcContinueShoppingBtn))
									{
										pgok = true;
									}
									else if((BNBasicfeature.isElementPresent(miniCartSignInBtn)))
									{
										pgok = true;
									}
									else
									{
										jsclick(miniCartShoppingBagMaskId);
										driver.navigate().refresh();
										Thread.sleep(1000);
										wait.until(ExpectedConditions.visibilityOf(header));
										pgok = false;
										count++;
										continue;
									}
								}
								catch(Exception e)
								{
									jsclick(miniCartShoppingBagMaskId);
									driver.navigate().refresh();
									Thread.sleep(1000);
									wait.until(ExpectedConditions.visibilityOf(header));
									count++;
									continue;
								}
							}while((pgok==false)&&(count<10));
						}
						
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "display: none;"));
						log.add("The Mini Cart Shopping Bag is displayed.");
						if(BNBasicfeature.isElementPresent(miniCartSignInBtn))
						{
							log.add("The Sign In button is displayed.");
							wait.until(ExpectedConditions.elementToBeClickable(miniCartSignInBtn));
							jsclick(miniCartSignInBtn);
							wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
							wait.until(ExpectedConditions.visibilityOf(pgetitle));
							if(BNBasicfeature.isElementPresent(pgetitle))
							{
								log.add("The user is navigated from the Mini Cart Fly Out.");
								log.add("The Expeced Title was : " + titleCellVal);
								String actTitle = pgetitle.getText(); 
								log.add("The Actual title is : " + actTitle);
								if(actTitle.contains(titleCellVal))
								{
									Pass("There is user is navigated to the Sign In Page and the title matches the expected content.",log);
								}
								else
								{
									Fail("There is mismatch in the Expected Title / the user is not navigated to the Sign In Page.",log);
								}
							}
							else
							{
								Fail("The user is not navigated to the sign in page.",log);
							}
						}
						else
						{
							Fail("The Shopping Bag Sign In button is not displayed.",log);
						}
					}
					else
					{
						log.add("The Expected Count was 0");
						log.add("The Actual Count is : " + currVal);
						Fail("The Shopping Bag Count is not 0.",log);
					}
				}
				else
				{
					Fail( "The User failed to get the bagCount.");
				}
			}
			else
			{
				Fail("The shopping bag icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 405 Issue ." + e.getMessage());
			Exception(" BRM - 405 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 409 Verify whether after signing in using "sign in to see your Account" the already shopped items corresponding to signed in account is displayed in shopping bag page along with product count update.*/
	public void minicartShoppingBagSignInViewCount()
	{
		ChildCreation(" BRM - 409 Verify whether after signing in using Sign in to see your Account the already shopped items corresponding to signed in account is displayed in shopping bag page along with product count update.");
		try
		{
			int bagItemCount = Integer.parseInt(BNBasicfeature.getExcelNumericVal("BRM409", sheet, 2));
			String signInName = BNBasicfeature.getExcelVal("BRM409", sheet, 3);
			String pass = BNBasicfeature.getExcelVal("BRM409", sheet, 4);
			Thread.sleep(100);
			if(BNBasicfeature.isElementPresent(emailId))
			{
				log.add("The Email Id field is displayed.The entered email id is : " + signInName);
				emailId.sendKeys(signInName);
				Thread.sleep(100);
				if(BNBasicfeature.isElementPresent(password))
				{
					log.add("The password field is displayed.The entered password is : " + pass);
					password.sendKeys(pass);
					if(BNBasicfeature.isElementPresent(secureSignIn))
					{
						jsclick(secureSignIn);
						log.add("The Secure Sign In button is displayed and it is clicked.");
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"));
						String expVal = "Sign Out";
						log.add("The expected value was : Sign Out");
						String actVal = footerSignIn.getText();
						log.add("The expected value was : " + actVal);
						if(expVal.equalsIgnoreCase(actVal))
						{
							signedIn = true;
							if(BNBasicfeature.isElementPresent(bagCount))
							{
								Pass("The User was Signed In and the promo carousel was displayed.",log);
								if(BNBasicfeature.isElementPresent(bagIcon))
								{
									log.add("The Shopping Bag is displayed.");
									if(BNBasicfeature.isElementPresent(bagCount))
									{
										log.add("The Shopping Bag Count is displayed.");
										log.add("The user not expected value was : " + bagItemCount);
										boolean bgCnt = getBagCount();
										if(bgCnt==true)
										{
											int currVal = Integer.parseInt(bagCount.getText());
											log.add("The Actual Value is : " + currVal);
											if(currVal==bagItemCount)
											{
												Fail("The shopping Bag count for the signed in user is displayed and it is not 0.",log);
											}
											else
											{
												Pass("The shopping Bag count for the signed in user is displayed and it is not 0.",log);
											}
										}
										else
										{
											Fail( "The user failed to get the bagCount.");
										}
									}
									else
									{
										Fail("The Shopping Bag Count is not displayed.");
									}
								}
								else
								{
									Fail("The Shopping Bag is not displayed.");
								}
							}
							else
							{
								Fail("The User is not signed in.",log);
							}
						}
						else
						{
							Fail("The User is not signed in.",log);
						}
					}
					else
					{
						Fail("The Secure Sign In button is not displayed.");
					}
				}
				else
				{
					Fail("The Password field is not displayed.",log);
				}
			}
			else
			{
				Fail("The user is not in the Sign In Page and the Email Id field is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 409 Issue ." + e.getMessage());
			Exception(" BRM - 409 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1309 verify that on signing in the count in mini cart logo should be updated accordingly and on clicking the mini cart the item count should not be modified.*/
	public void miniCartBagCount()
	{
		ChildCreation("BRM - 1309 verify that on signing in the count in mini cart logo should be updated accordingly and on clicking the mini cart the item count should not be modified.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(bagIcon))
				{
					log.add("The Shopping Bag icon is displayed.");
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						String currPrdtCount = bagCount.getText();
						log.add("The Shopping Bag Count is  : " + currPrdtCount);
						boolean loadMiniCartPage = loadMiniCart();
						if(loadMiniCartPage==true)
						{
							if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
							{
								String prdrtCount = miniCartShoppingItemCountInfo.getText();
								log.add("The Shopping Bag Count Info is  : " +prdrtCount);
								//System.out.println(prdrtCount);
								if(prdrtCount.contains(currPrdtCount))
								{
									Pass("The Value matches the Shopping Bag Content.",log);
								}
								else
								{
									Fail("The Value does not matches the Shopping Bag Content Value.",log);
								}
							}
							else
							{
								Fail("The Saved Item List is not dispalyed.");
							}
						}
						else
						{
							Fail("The Mini Cart failed to load.");
						}
					}
					else
					{
						Fail("The Shopping Bag is not displayed.");
					}
				}
				else
				{
					Fail( "The User failed to get the bag Count.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1309 Issue ." + e.getMessage());
				Exception(" BRM - 1309 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 918 Verify that the qty box is displayed as per the creative and classic site.*/
	public void miniCartQtyField()
	{
		ChildCreation("BRM - 918 Verify that the qty box is displayed as per the creative and classic site.");//miniCartQtyField
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(miniCartQtyField))
				{
					for(WebElement qty : miniCartQtyField)
					{
						BNBasicfeature.scrolldown(qty, driver);
						if(BNBasicfeature.isElementPresent(qty))
						{
							Pass("The Qty Field is displayed.");
						}
						else
						{
							Fail("The Qty Field is not dispalyed.");
						}
					}
				}
				else
				{
					Fail("The Qty Field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 918 Issue ." + e.getMessage());
				Exception(" BRM - 918 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1325 Verify whether for different product types have their details,update,remove and save for later options displayed as per the classic site.*/
	public void miniCartProductDetails()
	{
		ChildCreation("BRM - 1325 Verify whether for different product types have their details,update,remove and save for later options displayed as per the classic site.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					log.add("The miniCart list is displayed.");
					for(int i = 0;i<miniCartAddedItemLists.size();i++)
					{
						String prdtName = getprdtName(i);
						if(prdtName.isEmpty())
						{
							Fail("The Product Name is not displayed.");
						}
						else
						{
							Pass("The Product Name is displayed. The Product Name is : " + prdtName);
						}
						
						String prdType = getprdtType(i);
						if(prdType.isEmpty())
						{
							Fail("The Product Type is not displayed.");
						}
						else
						{
							Pass("The Product Type is displayed. The Product Type is : " + prdType);
							
						}
						
						boolean removeBtnEnabled = getRemoveBtnState(i);
						if(removeBtnEnabled==true)
						{
							Pass("The Remove button is displayed.");
						}
						else
						{
							Fail("The Remove button is not displayed.");
						}
						
						boolean saveForBtnEnabled = getSaveForLaterBtnState(i);
						if(saveForBtnEnabled==true)
						{
							Pass("The Save for Later button is displayed.");
						}
						else
						{
							Fail("The Save for Later button is not displayed.");
						}
					}
				}
				else
				{
					Fail("The miniCart list is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1325 Issue ." + e.getMessage());
				Exception(" BRM - 1325 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 455 Verify whether "NOOK Book" category items displayed in shopping bag page has a tag "Available Immediately".*/
	public void miniCartAvailableImmediately()
	{
		ChildCreation("BRM - 455 Verify whether NOOK Book category items displayed in shopping bag page has a tag Available Immediately.");
		if(signedIn==true)
		{
			try
			{
				String cellVall = BNBasicfeature.getExcelVal("BRM455", sheet, 2);
				String[] Val = cellVall.split("\n");
				String availText = BNBasicfeature.getExcelVal("BRM455", sheet, 6);
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					log.add("The added item list is displayed.");
					BNBasicfeature.scrollup(miniCartShoppingBagTitle, driver);
					//Thread.sleep(1000);
					//response = BNMiniCartCall.miniCartgetCall(driver);
					try
					{
						List<WebElement> miniCartItemSize = driver.findElements(By.xpath("//*[contains(@class,'qtyUpdate')]"));
						String url = BNConstants.miniCartViewBagURL;
						ArrayList<String> skuId = new ArrayList<>();
						for(int i = 0; i<miniCartItemSize.size(); i++)
						{
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'qtyUpdate')]"));
							String skid = prdtUpdatebtn.getAttribute("skuid");
							skuId.add(skid);
						}
						String listString = String.join(",", skuId);
						url = url.concat(listString);
						driver.get(url);
						//driver.get(BNConstants.miniCartViewBagURL);
						Thread.sleep(1000);
						String response = driver.findElement(By.xpath("/html/body/pre")).getText();
						//System.out.println(response);
						driver.get(BNConstants.mobileSiteURL);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
						wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
						Thread.sleep(100);
						boolean loadcart = loadMiniCart();
						if(loadcart==true)
						{
							JSONObject obj = new JSONObject(response);
							JSONObject children = obj.getJSONObject("children");
							JSONArray products = children.getJSONArray("products");
							int productsSize = products.length();
							for(int i = 0; i<productsSize; i++)
							{
								String prdtName = products.getJSONObject(i).getString("name");
								//System.out.println(prdtName);
								JSONObject prdt = products.getJSONObject(i).getJSONObject("properties").getJSONObject("iteminfo");
								String itemType = prdt.getString("itemtype");
								//System.out.println(itemType);
								if(itemType.equalsIgnoreCase(Val[0])||itemType.contains(Val[1]))
								{
									log.add("The Product name from the Stream Call is : " + prdtName);
									log.add("The Product Type from the Stream Call is : " + itemType);
									//JSONArray shipmsg = prdt.getJSONArray("shippingmessages");
									//System.out.println(shipmsg.length());
									//int size = shipmsg.length();
									//String availablityresp = shipmsg.getJSONObject(size-1).getString("description");
									//System.out.println(availablityresp);
									String minicartprdtName = getprdtName(i);
									//System.out.println(minicartprdtName);
									log.add("The Product Name is : " + minicartprdtName);
									String productType = getprdtType(i);
									//System.out.println(productType);
									log.add("The Selected Product Type : " + productType);
									try
									{
										WebElement availability = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_pdtAvailInfotxt']"));
										if(BNBasicfeature.isElementPresent(availability))
										{
											Pass("The Available Immediately text is displayed.");
											log.add("The Expected value was : " + availText);
											log.add("The current value for the product is : " + availability.getText());
											//System.out.println(availability.getText());
											if(availability.getText().contains(availText))
											{
												Pass("The Value matches the expected content.",log);
											}
											else
											{
												Fail("The Value does not matches the expected content.",log);
											}
										}
										else
										{
											Fail("The Text is not present for the product.",log);
										}
									}
									catch(Exception e)
									{
										Fail("The Availability text is not displayed for the Product.",log);
									}
								}
								else
								{
									continue;
								}
							}
						}
						else
						{
							Fail("Failed to load the MiniCart.");
						}
					}
					catch(Exception e)
					{
						System.out.println(" BRM - 455 Issue ." + e.getMessage());
						Exception(" BRM - 455 Issue." + e.getMessage());
					}
				}
				else
				{
					Fail("The added item list is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 455 Issue ." + e.getMessage());
				Exception(" BRM - 455 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 463 Verify whether subtotal with total item shopped,Estimated shipping,Estimated Tax ,discount and Gift wrap(if available)along with its value appear below coupon text box/coupon details in shopping bag page.*/
	public void miniCartTotalPriceContainer()
	{
		ChildCreation("BRM - 463 Verify whether subtotal with total item shopped,Estimated shipping,Estimated Tax ,discount and Gift wrap(if available)along with its value appear below coupon text box/coupon details in shopping bag page.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(miniCartCouponField))
				{
					BNBasicfeature.scrolldown(miniCartCouponField, driver);
				}
				
				if(BNBasicfeature.isElementPresent(miniCartPriceContainer))
				{
					Pass("The Price Container is displayed.");
					if(BNBasicfeature.isElementPresent(miniCartPriceSubTotalText))
					{
						log.add("The Subtotal text is displayed.");
						String val = miniCartPriceSubTotalText.getText(); 
						if(val.isEmpty())
						{
							Fail("The Subtotal text is not displayed.");
						}
						else
						{
							Pass("The Subtotal text is displayed.The Subtotal text displayed is : " + val);
						}
					}
					else
					{
						Fail("The Subtotal text is not displayed.");
					}
						
					if(BNBasicfeature.isElementPresent(miniCartSubTotalPriceDetails))
					{
						log.add("The Subtotal Price is displayed.");
						String val = miniCartSubTotalPriceDetails.getText(); 
						if(val.isEmpty())
						{
							Fail("The Subtotal is not displayed.");
						}
						else
						{
							Pass("The Subtotal is displayed.The Subtotal displayed is : " + val);
						}
					}
					else
					{
						Fail("The Subtotal Price is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalEstimatedShippingLabel))
					{
						log.add("The Estimated Shipping Label is displayed.");
						String val = miniCartSubTotalEstimatedShippingLabel.getText(); 
						if(val.isEmpty())
						{
							Fail("The Estimated Shipping Label is not displayed.");
						}
						else
						{
							Pass("The Estimated Shipping Label id displayed.The Label is : " + val);
						}
					}
					else
					{
						Fail("The Estimated Shipping Label is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalEstimatedShippingPrice))
					{
						log.add("The Estimated Shipping Price is displayed.");
						String val = miniCartSubTotalEstimatedShippingPrice.getText(); 
						if(val.isEmpty())
						{
							Fail("The Estimated Shipping Price is not displayed.");
						}
						else
						{
							Pass("The Estimated Shipping Price is displayed.The Estimated Shipping Price displayed is : " + val);
						}
					}
					else
					{
						Fail("The Estimated Shipping Price is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalEstimatedTaxLabel))
					{
						log.add("The Estimated Tax Price is displayed.");
						String val = miniCartSubTotalEstimatedTaxLabel.getText(); 
						if(val.isEmpty())
						{
							Fail("The Estimated Tax is not displayed.");
						}
						else
						{
							Pass("The Estimated Tax is displayed.The Estimated Tax Label is : " + val);
						}
					}
					else
					{
						Fail("The Estimated Tax is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalEstimatedTaxPrice))
					{
						log.add("The Estimated Tax Price is displayed.");
						String val = miniCartSubTotalEstimatedTaxPrice.getText(); 
						if(val.isEmpty())
						{
							Fail("The Estimated Tax Price is not displayed.");
						}
						else
						{
							Pass("The Estimated Tax Price is displayed.The Estimated Tax displayed is : " + val);
						}
					}
					else
					{
						Fail("The Estimated Tax Price is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalDiscountLabel))
					{
						log.add("The Discount label is displayed.");
						String val = miniCartSubTotalDiscountLabel.getText(); 
						if(val.isEmpty())
						{
							Fail("The Discount label is not displayed.");
						}
						else
						{
							Pass("The Discount label is displayed.The Discount label displayed is : " + val);
						}
					}
					else
					{
						Skip("The Discount label is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalDiscountPrice))
					{
						log.add("The Discount Price is displayed.");
						String val = miniCartSubTotalDiscountPrice.getText(); 
						if(val.isEmpty())
						{
							Fail("The Discount Price is not displayed.");
						}
						else
						{
							Pass("The Discount Price is displayed.The Discount Price displayed is : " + val);
						}
					}
					else
					{
						Fail("The Discount Price is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalLabel))
					{
						log.add("The Subtotal label is displayed.");
						String val = miniCartSubTotalLabel.getText(); 
						if(val.isEmpty())
						{
							Fail("The Subtotal label is not displayed.");
						}
						else
						{
							Pass("The Subtotal label is displayed.The Subtotal label displayed is : " + val);
						}
					}
					else
					{
						Fail("The Subtotal label is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartSubTotalPrice))
					{
						log.add("The Discount Price is displayed.");
						String val = miniCartSubTotalPrice.getText(); 
						if(val.isEmpty())
						{
							Fail("The Subtotal Price is not displayed.");
						}
						else
						{
							Pass("The Subtotal Price is displayed.The Subtotal Price displayed is : " + val);
						}
					}
					else
					{
						Fail("The Subtotal Price is not displayed.");
					}
				}
				else
				{
					Fail("The Price Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 463 Issue ." + e.getMessage());
				Exception(" BRM - 463 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 464 Verify whether "ORDER TOTAL" is present along with its correct value in red when items are shopped in shopping bag page.*/
	public void miniCartSubTotal()
	{
		ChildCreation("BRM - 464 Verify whether ORDER TOTAL is present along with its correct value in red when items are shopped in shopping bag page.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM464", sheet, 2);
				if(BNBasicfeature.isElementPresent(miniCartSubTotalLabel))
				{
					log.add("The Subtotal label is displayed.");
					String val = miniCartSubTotalLabel.getText(); 
					if(val.isEmpty())
					{
						Fail("The Subtotal label is not displayed.");
					}
					else
					{
						Pass("The Subtotal label is displayed.The Subtotal label displayed is : " + val);
					}
				}
				else
				{
					Fail("The Subtotal label is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(miniCartSubTotalPrice))
				{
					log.add("The Discount Price is displayed.");
					String val = miniCartSubTotalPrice.getText(); 
					if(val.isEmpty())
					{
						Fail("The Subtotal Price is not displayed.");
					}
					else
					{
						Pass("The Subtotal Price is displayed.The Subtotal Price displayed is : " + val);
						String csvalue = miniCartSubTotalPrice.getCssValue("color");
						//System.out.println(csvalue);
						Color colorhxcnvt = Color.fromString(csvalue);
						String hexCode = colorhxcnvt.asHex();
						//System.out.println(hexCode);
						log.add("The expected color was : "+ cellVal);
						log.add("The actual color is : "+ hexCode);
						if(hexCode.contains(cellVal))
						{
							Pass("The Color matches the expected content.",log);
						}
						else
						{
							Fail("The Color does not matches the expected content.",log);
						}
					}
				}
				else
				{
					Fail("The Subtotal Price is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 464 Issue ." + e.getMessage());
				Exception(" BRM - 464 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 453 Verify whether only items of category "Nook Book","Newsstand and "Textbook" displayed in shopping bag has already chosen quantity while shopping and has no quantity update option.*/
	public void miniCartNoUpdateOption()
	{
		ChildCreation("BRM - 453 Verify whether only items of category Nook Book,Newsstand and Textbook displayed in shopping bag has already chosen quantity while shopping and has no quantity update option.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM453", sheet, 2);
				String[] Val = cellVal.split("\n");
				BNBasicfeature.scrollup(miniCartShoppingBagTitle, driver);
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					log.add("The added item list is displayed.");
					for(int i=0;i<miniCartAddedItemLists.size();i++)
					{
						String prdtName = getprdtName(i);
						String prdtType = getprdtType(i);
						if(prdtType.contains(Val[0])||prdtType.contains(Val[1])||prdtType.contains(Val[2]))
						{
							log.add("The Selected product name is : " + prdtName);
							log.add("The Selected product type is : " + prdtType);
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'qtyUpdate')]"));
							try
							{
								prdtUpdatebtn.click();
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								Fail("The Product Update button is visible for the Product.",log);
							}
							catch(Exception e)
							{
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								Pass("The Update button is not displayed for the category.",log);
							}
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Fail("The added Item List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 453 Issue ." + e.getMessage());
				Exception(" BRM - 453 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1365 Verify that quantity box for Nook products in shopping bag page does not allow to edit the quantity value that appeared by default.*/
	public void miniCartNookNoUpdateOption()
	{
		ChildCreation("BRM - 1365 Verify that quantity box for Nook products in shopping bag page does not allow to edit the quantity value that appeared by default");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM453", sheet, 2);
				String[] Val = cellVal.split("\n");
				BNBasicfeature.scrollup(miniCartShoppingBagTitle, driver);
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					log.add("The added item list is displayed.");
					for(int i=0;i<miniCartAddedItemLists.size();i++)
					{
						String prdtName = getprdtName(i);
						String prdtType = getprdtType(i);
						if(prdtType.contains(Val[0])||prdtType.contains(Val[1])||prdtType.contains(Val[2]))
						{
							log.add("The Selected product name is : " + prdtName);
							log.add("The Selected product type is : " + prdtType);
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_pdtUpdateQty']"));
							
							try
							{
								prdtUpdatebtn.click();
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								Fail("The Product Update button is visible for the Product.",log);
							}
							catch(Exception e)
							{
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								Pass("The Update button is not displayed for the category.",log);
							}
							
							/*boolean checkDisabled = prdtUpdatebtn.getAttribute("disabled").contains("disabled");
							if(checkDisabled==false)
							{
								Pass("The Product Update field is disabled for the Product.",log);
							}
							else
							{
								Fail("The Product Update field is not disabled for the Product.",log);
							}*/
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Fail("The added Item List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1365 Issue ." + e.getMessage());
				Exception(" BRM - 1365 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 456 Verify whether coupon text box with default text with "Apply" button near is present in shopping bag page after shopping items.*/
	public void miniCartCouponSection()
	{
		ChildCreation("BRM - 456 Verify whether coupon text box with default text with Apply button near is present in shopping bag page after shopping items.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM456", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(miniCartCouponField))
				{
					BNBasicfeature.scrolldown(miniCartCouponField, driver);
					Pass("The Coupon Code Field is displayed.");
					if(BNBasicfeature.isElementPresent(miniCartCouponDefaultText))
					{
						log.add("The Default text is displayed in the Coupon Code.");
						log.add("The Expected Value is : " + Val[0]);
						String val = miniCartCouponDefaultText.getText();
						log.add("The Current Value is : " + val);
						if(val.contains(Val[0]))
						{
							Pass("The Value matches the expected content.",log);
						}
						else
						{
							Fail("The Value does not matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Default text is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(miniCartCouponApplyButton))
					{
						Pass("The Apply button is displayed.");
						String csvalue = miniCartCouponApplyButton.getCssValue("background-color");
						//System.out.println(csvalue);
						Color colorhxcnvt = Color.fromString(csvalue);
						String hexCode = colorhxcnvt.asHex();
						//System.out.println(hexCode);
						log.add("The expected color was : "+ Val[1]);
						log.add("The actual color is : "+ hexCode);
						if(hexCode.contains(Val[1]))
						{
							Pass("The Color matches the expected content.",log);
						}
						else
						{
							Fail("The Color does not matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Apply button is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 456 Issue ." + e.getMessage());
				Exception(" BRM - 456 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 412 Verify whether after signing into the account "My Saved Items" are listed out at the bottom part of shopping bag page if items are saved already*/
	public void minicartShoppingBagSavedItemsList()
	{
		ChildCreation(" BRM - 412 Verify whether after signing into the account My Saved Items are listed out at the bottom part of shopping bag page if items are saved already.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM412", sheet, 2);
				if(BNBasicfeature.isElementPresent(bagIcon))
				{
					log.add("The Shopping bag is displayed.");
					if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(1)))
					{
						BNBasicfeature.scrolldown(mcContinueToCheckOutBtn.get(1), driver);
					}
					else if(BNBasicfeature.isElementPresent(miniCartPriceContainer))
					{
						BNBasicfeature.scrolldown(miniCartPriceContainer, driver);
					}
					
					if(BNBasicfeature.isElementPresent(miniCartMySavedItems))
					{
						Pass("The My Saved item container is displayed.");
						if(BNBasicfeature.isElementPresent(miniCartMySavedItemsTitle))
						{
							log.add("The My Saved items title is displayed.");
							log.add("The Expected Title was : " + cellVal);
							String actTitle = miniCartMySavedItemsTitle.getText();
							log.add("The Actual Title is : " + actTitle);
							if(cellVal.contains(actTitle))
							{
								Pass("The Title matches the expected content.",log);
							}
							else
							{
								Fail("The Title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The My Saved Items Title is not displayed.",log);
						}
					}
					else
					{
						Fail("The Mini Cart My Saved Items container is displayed.",log);
					}
				}
				else
				{
					Fail("The Shopping icon is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 412 Issue ." + e.getMessage());
				Exception(" BRM - 412 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 413 Verify whether the saved items should have title as "My Saved Items", book image and its detail displayed as per the creative.*/
	public void minicartShoppingBagSavedItemsDetails()
	{
		ChildCreation(" BRM - 413 Verify whether the saved items should have title as My Saved Items, book image and its detail displayed as per the creative.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM413", sheet, 2);
				String[] Val = cellVal.split("\n"); 
				Thread.sleep(250);
				if(BNBasicfeature.isElementPresent(miniCartMySavedItemsTitle))
				{
					if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(1)))
					{
						BNBasicfeature.scrolldown(mcContinueToCheckOutBtn.get(1), driver);
					}
					else if(BNBasicfeature.isElementPresent(miniCartPriceContainer))
					{
						BNBasicfeature.scrolldown(miniCartPriceContainer, driver);
					}
					
					log.add("The Shopping Bag title is displayed.");
					log.add("The My Saved items title is displayed.");
					log.add("The Expected Title was : " + Val[0]);
					String actTitle = miniCartMySavedItemsTitle.getText();
					log.add("The Actual Title is : " + actTitle);
					if(Val[0].contains(actTitle))
					{
						Pass("The Title matches the expected content.",log);
					}
					else
					{
						Fail("The Title does not matches the expected content.",log);
					}
					
					if(BNBasicfeature.isListElementPresent(miniCartSavedItemLists))
					{
						Pass("The Saved Item list Container is displayed.");
						for(int i = 1; i<=miniCartSavedItemLists.size();i++)
						{
							WebElement prdtTitle = miniCartSavedItemPrdtTitle.get(i-1);
							BNBasicfeature.scrolldown(prdtTitle, driver);
							if(BNBasicfeature.isElementPresent(prdtTitle))
							{
								String val = prdtTitle.getText(); 
								if(val.isEmpty())
								{
									Fail("The Product Title is not displayed and it is empty.");
								}
								else
								{
									Pass("The Product Title is displayed.The Product Title is : " + val);
								}
							}
							else
							{
								Fail("The Product Title is not displayed.");
							}
							
							try
							{
								WebElement prdtAuthor = miniCartSavedItemPrdtAuthor.get(i-1);
								if(BNBasicfeature.isElementPresent(prdtTitle))
								{
									String val = prdtAuthor.getText(); 
									if(val.isEmpty())
									{
										Fail("The Author Name is not displayed.");
									}
									else
									{
										Pass("The Author Name is displayed.The Author Name is : " + val);
									}
								}
								else
								{
									Fail("The Product Title is not displayed.");
								}
							}
							catch(Exception e)
							{
								Skip(" The Author Name is not displayed for the Saved Items List.");
							}
							
							WebElement prdtRating = miniCartSavedItemPrdtRating.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtRating))
							{
								Pass("The Rating Container is displayed.");
							}
							else
							{
								Fail("The Rating Continer is not displayed.");
							}
							
							WebElement prdtAddtoBagButton = miniCartSavedItemPrdtAddtoBagButton.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtAddtoBagButton))
							{
								Pass("The Add to Bag button is displayed.");
								log.add("The Expected content was  : " + Val[1]);
								String addtoBagTitle = prdtAddtoBagButton.getText();
								log.add("The Actual text is : " + addtoBagTitle);
								if(addtoBagTitle.contains(Val[1]))
								{
									Pass("Add to Bag title matches the expected content",log);
								}
								else
								{
									Fail("The Add to Bag title does not matches the expected content.",log);
								}
							}
							else
							{
								Fail("The Add to Bag button is not displayed.");
							}
							
							WebElement prdtRemoveButton = miniCartSavedItemPrdtRemoveButton.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtRemoveButton))
							{
								Pass("The Remove button is displayed.");
								log.add("The Expected content was  : " + Val[2]);
								String removeTitle = prdtAddtoBagButton.getText();
								log.add("The Actual text is : " + removeTitle);
								if(removeTitle.contains(Val[1]))
								{
									Pass("The Remove title matches the expected content",log);
								}
								else
								{
									Fail("The Remove title does not matches the expected content.",log);
								}
							}
							else
							{
								Fail("The Remove button is not displayed.");
							}
							
							WebElement prdtImage = miniCartSavedItemPrdtImage.get(i-1);
							int imgresponse = BNBasicfeature.imageBroken(prdtImage, log);
							if(imgresponse==200)
							{
								Pass("The Image is displayed and the image is not broken.",log);
							}
							else
							{
								Fail("The Image is not displayed or the image is broken.",log);
							}
						}
					}
					else
					{
						Fail("The Saved Item list Container is not displayed.");
					}
				}
				else 
				{
					Fail("The Shopping Bag Title is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 413 Issue ." + e.getMessage());
				Exception(" BRM - 413 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 415 Verify whether the saved items has following details: Product name,author name,review stars also "Add to Bag" and "Remove" option.*/
	public void minicartShoppingBagSavedItemsDetailsLists()
	{
		ChildCreation(" BRM - 415 Verify whether the saved items has following details: Product name,author name,review stars also Add to Bag and Remove option.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM415", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(miniCartSavedItemLists))
				{
					Pass("The Saved Item list Container is displayed.");
					for(int i = 1; i<=miniCartSavedItemLists.size();i++)
					{
						WebElement prdtTitle = miniCartSavedItemPrdtTitle.get(i-1);
						BNBasicfeature.scrolldown(prdtTitle, driver);
						if(BNBasicfeature.isElementPresent(prdtTitle))
						{
							String val = prdtTitle.getText(); 
							if(val.isEmpty())
							{
								Fail("The Product Title is not displayed and it is empty.");
							}
							else
							{
								Pass("The Product Title is displayed.The Product Title is : " + val);
							}
						}
						else
						{
							Fail("The Product Title is not displayed.");
						}
						
						try
						{
							WebElement prdtAuthor = miniCartSavedItemPrdtAuthor.get(i-1);
							if(BNBasicfeature.isElementPresent(prdtTitle))
							{
								String val = prdtAuthor.getText(); 
								if(val.isEmpty())
								{
									Fail("The Author Name is not displayed.");
								}
								else
								{
									Pass("The Author Name is displayed.The Author Name is : " + val);
								}
							}
							else
							{
								Fail("The Product Title is not displayed.");
							}
						}
						catch(Exception e)
						{
							Skip(" The Author Name is not displayed for the Saved Items List.");
						}
						
						WebElement prdtRating = miniCartSavedItemPrdtRating.get(i-1);
						if(BNBasicfeature.isElementPresent(prdtRating))
						{
							Pass("The Rating Container is displayed.");
						}
						else
						{
							Fail("The Rating Continer is not displayed.");
						}
						
						WebElement prdtAddtoBagButton = miniCartSavedItemPrdtAddtoBagButton.get(i-1);
						if(BNBasicfeature.isElementPresent(prdtAddtoBagButton))
						{
							Pass("The Add to Bag button is displayed.");
							log.add("The Expected content was  : " + Val[1]);
							String addtoBagTitle = prdtAddtoBagButton.getText();
							log.add("The Actual text is : " + addtoBagTitle);
							if(addtoBagTitle.contains(Val[1]))
							{
								Pass("Add to Bag title matches the expected content",log);
							}
							else
							{
								Fail("The Add to Bag title does not matches the expected content.",log);
							}
							
							String cssValue = prdtAddtoBagButton.getCssValue("color");
							//System.out.println(cssValue);
							Color colorhxcnvt = Color.fromString(cssValue);
							String hexCode = colorhxcnvt.asHex();
							//System.out.println(hexCode);
							log.add("The Expected Link Color was : " + Val[0]);
							log.add("The Actual Link Color is : " + hexCode);
							if(hexCode.equals(Val[0]))
							{
								Pass("The Link Color matches the expected color.",log);
							}
							else
							{
								Fail("The Link Color does not matches the expected color.",log);
							}
						}
						else
						{
							Fail("The Add to Bag button is not displayed.");
						}
						
						WebElement prdtRemoveButton = miniCartSavedItemPrdtRemoveButton.get(i-1);
						if(BNBasicfeature.isElementPresent(prdtRemoveButton))
						{
							Pass("The Remove button is displayed.");
							log.add("The Expected content was  : " + Val[2]);
							String removeTitle = prdtAddtoBagButton.getText();
							log.add("The Actual text is : " + removeTitle);
							if(removeTitle.contains(Val[1]))
							{
								Pass("The Remove title matches the expected content",log);
							}
							else
							{
								Fail("The Remove title does not matches the expected content.",log);
							}
							
							String cssValue = prdtRemoveButton.getCssValue("color");
							//System.out.println(cssValue);
							Color colorhxcnvt = Color.fromString(cssValue);
							String hexCode = colorhxcnvt.asHex();
							//System.out.println(hexCode);
							log.add("The Expected Link Color was : " + Val[0]);
							log.add("The Actual Link Color is : " + hexCode);
							if(hexCode.equals(Val[0]))
							{
								Pass("The Link Color matches the expected color.",log);
							}
							else
							{
								Fail("The Link Color does not matches the expected color.",log);
							}
						}
						else
						{
							Fail("The Remove button is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Saved Item list Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 415 Issue ." + e.getMessage());
				Exception(" BRM - 415 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1344 Verify whether clicking on Checkout button displays Registered checkout page if already signed-in.*/
	public void miniCartSignedInUserCheckoutNavigation()
	{
		ChildCreation("BRM - 1344 Verify whether clicking on Checkout button displays Registered checkout page if already signed-in.");
		if(signedIn==true)
		{
			try
			{
				String alert = "",item = "";
				
				if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(1)))
				{
					BNBasicfeature.scrolldown(mcContinueToCheckOutBtn.get(1), driver);
					jsclick(mcContinueToCheckOutBtn.get(1));
					wait.until(ExpectedConditions.or(
							ExpectedConditions.visibilityOf(checkoutTopSubmitOrder),
							ExpectedConditions.visibilityOf(mcShoppingBagAlert)
								)
							);
					if(BNBasicfeature.isElementPresent(mcShoppingBagAlert))
					{
						stockAlert = true;
						alert = mcShoppingBagAlert.getText();
						item = driver.findElement(By.xpath("//*[@class='sk_cartTempOutSTK']//li")).getText();
					}
					if(stockAlert==false)
					{
						wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
						if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
						{
							Pass("The User is navigated to the Registered User Checkout Page.");
						}
						else
						{
							Fail("The User is not navigated to the Registered User Checkout Page.");
						}
					}
					else
					{
						Skip("The Product of Stock Alert was raised in the Mini Cart Fly. The raised alert was " + alert + " , and the raised for the item " + item);
						jsclick(miniCartShoppingBagMaskId);
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
					}
				}
				else
				{
					Fail("The Secure Checkout button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1344 Issue ." + e.getMessage());
				Exception(" BRM - 1344 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 425 Verify that while selecting the "Checkout" button in the shopping bag page, the checkout page should be displayed with the pre-filled shipping address once the registered users gets already logged-in.*/
	public void miniCartCheckoutPageShipDet()
	{
		ChildCreation(" BRM - 425 Verify that while selecting the Checkout button in the shopping bag page, the checkout page should be displayed with the pre-filled shipping address once the registered users gets already logged-in.");
		if(signedIn==true)
		{
			try
			{
				String caption = BNBasicfeature.getExcelVal("BRM425", sheet, 2);
				if(stockAlert==false)
				{
					wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
					if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
					{
						Pass("The User is navigated to the Registered User Checkout Page.");
					}
					else
					{
						Fail("The User is not navigated to the Registered User Checkout Page.");
					}
					
					List<WebElement> shippingContainer = checkoutBillandShipContainer;
					for(int j = 1; j<=shippingContainer.size();j++)
					{
						WebElement addrInfo = checkoutBillandShipContainer.get(j-1);
						log.add("The Expected title was : " + caption);
						String actTitle = addrInfo.getText();
						log.add("The Actual title is : " + actTitle);
						if(actTitle.contains(caption))
						{
							log.add("The Shipping Address Container is found for the User.");
							getCurrShipAddressDetails(1);
							for(WebElement addDetails : shipOrbillCurrAddress)
							{
								String addr = addDetails.getText();
								log.add("The displyed address is : " + addr);
								if(!addr.isEmpty())
								{
									Pass("The Existing address is displayed and they are visible.",log);
								}
								else
								{
									Fail("The Existing address is not displayed.",log);
								}
							}
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Skip("The Product of Stock Alert was raised in the Mini Cart Fly");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 425 Issue ." + e.getMessage());
				Exception(" BRM - 425 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 468 Verify whether clicking on back from registered checkout page, loaded on choosing "Checkout" button takes back to corresponding page page.*/
	public void miniCartHomeNavigationFromRegisteredCheckout()
	{
		ChildCreation("BRM - 468 Verify whether clicking on back from registered checkout page, loaded on choosing Checkout button takes back to corresponding page.");
		if(signedIn==true)
		{
			try
			{
				if(stockAlert==false)
				{
					if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
					{
						log.add("The User is in Registered User Checkout Page.");
						driver.navigate().back();
						wait.until(ExpectedConditions.visibilityOf(header));
						if(BNBasicfeature.isElementPresent(bagIcon))
						{
							Pass("The user is navigated back to the home page.");
						}
						else
						{
							Fail("The user is not navigated back to the home page.");
						}
					}
					else
					{
						Fail("The User is not in the Registered User Checkout Page.");
					}
				}
				else
				{
					Skip("The User was not navigated to the Checkout page since some product was out of stock.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 468 Issue ." + e.getMessage());
				Exception(" BRM - 468 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1354 verify that on logging out, the product count in mini cart should become zero and shopping bag page should become empty.*/
	public void miniCartSigoutBagCount()
	{
		ChildCreation("BRM - 1354 verify that on logging out, the product count in mini cart should become zero and shopping bag page should become empty.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM1354", sheet, 6);
				boolean signOut = signOut();
				if(signOut==true)
				{
					if(BNBasicfeature.isElementPresent(bagIcon))
					{
						log.add("The shopping bag in the homepage is displayed.");
						Thread.sleep(100);
						wait.until(ExpectedConditions.elementToBeClickable(bagCount));
						Thread.sleep(100);
						boolean bgCnt = getBagCount();
						if(bgCnt==true)
						{
							String Bagcount = bagCount.getText();
							log.add("The Expected bag count is : " + cellVal);
							log.add("The Current Bag Count is  : " + Bagcount);
							if(cellVal.equals(Bagcount))
							{
								Pass("The Expected Bag value matches.",log);
							}
							else
							{
								Fail("The Expected Bag value does not matches.",log);
							}
						}
						else
						{
							Fail( "The User failed to get the bag count.");
						}
					}
					else
					{
						Fail("The shopping bag in the home page is not displayed.");
					}
				}
				else
				{
					Fail("The user failed to signout properly.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1354 Issue ." + e.getMessage());
				Exception(" BRM - 1354 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 465 Verify whether "CHECKOUT" button is present after items are shopped in shopping bag page and clicking on it displays a overlay with "Sign in" and "Checkout as Guest" if not signed in already. */
	public void miniCartGuestCheckoutNavigation()
	{
		ChildCreation("BRM - 465 Verify whether CHECKOUT button is present after items are shopped in shopping bag page and clicking on it displays a overlay with Sign in and Checkout as Guest if not signed in already. ");
		signedOut = true;
		if(signedOut==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRM465", sheet, 2);
				String guestButtonCaption = BNBasicfeature.getExcelVal("BRM465", sheet, 6);
				//addProduct(sheet,val);
				
				String ean = BNBasicfeature.getExcelVal("BRM465", sheet, 7);
				String cellVal = ean;
				String[] eanNum = cellVal.split("\n");
				boolean bkfound = false;
				boolean isbn = false;
				for(int i = 0; i<eanNum.length;i++)
				{
					bkfound = searchEanNumbers(eanNum[i]);
					 if(bkfound == true)
					 {
						 if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
						 {
							 isbn = true;
							 break;
						 }
						 else
						 {
							 isbn = false;
							 continue;
						 }
					 }
				}
				
				if(isbn==true)
				{
					int prevVal = 0;
					wait.until(ExpectedConditions.visibilityOf(bagCount));
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						prevVal = Integer.parseInt(bagCount.getText());
						//Thread.sleep(250);
						boolean prdtAdded = clickaddtoBag(prevVal, pdpPageATBBtn);
						if(prdtAdded==true)
						{
							Pass("The user was able to add the product successfully.");
							boolean loadMin = loadMiniCart();
							if(loadMin==true)
							{
								Pass("The Mini Cart was loaded successfully.");
								wait.until(ExpectedConditions.visibilityOf(mcContinueToCheckOutBtn.get(0)));
								if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(0)))
								{
									log.add("The Checkout button is displayed.");
									jsclick(mcContinueToCheckOutBtn.get(0));
									wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageFrame));
									if(BNBasicfeature.isElementPresent(pgetitle))
									{
										Pass("The Page title is displayed.");
										log.add("The Expected page title was : " + pgeTitle);
										log.add("The Actual title is : " + pgetitle.getText());
										if(pgeTitle.contains(pgetitle.getText()))
										{
											Pass("The Page title matches the expected value and the user is navigated to the Sign In Or Checkout as Guest page.",log);
										}
										else
										{
											Fail("There is some mismatch in the expected value.",log);
										}
										
										if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
										{
											log.add("The Checkout as Guest button is displayed.");
											log.add("The Expected button caption was : " + guestButtonCaption);
											log.add("The Actual button caption is : " + checkoutAsGuestbtn.getText());
											if(guestButtonCaption.contains(checkoutAsGuestbtn.getText()))
											{
												Pass("There button caption matches the expected content.",log);
											}
											else
											{
												Fail("There is mismatch in the expected button caption.",log);
											}
										}
										else
										{
											Fail("The Checkout as Guest button is not displayed.");
										}
									}
									else
									{
										Fail("The user is not navigated to the Sign In Page.",log);
									}
								}
								else
								{
									Fail("The Checkout button is not displayed.");
								}
							}
							else
							{
								Fail("The user failed to load the mini cart.");
							}
						}
						else
						{
							Fail("The user failed to add the product.");
						}
					}
					else
					{
						Fail( "The User failed to get the product count");
					}
				}
				else
				{
					Fail("The User was not able to search the desired product.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 465 Issue ." + e.getMessage());
				Exception(" BRM - 465 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign out.");
		}
	}
	
	/* BRM - 469 Verify whether choosing "Checkout as Guest" option from checkout overlay loads the guest checkout page.*/
	public void miniCartCheckoutAsGuestNavigation()
	{
		ChildCreation("BRM - 469 Verify whether choosing Checkout as Guest option from checkout overlay loads the guest checkout page.");
		if(signedOut==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM469", sheet, 2);
				if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
				{
					log.add("The Checkout as Guest button is displayed.");
					jsclick(checkoutAsGuestbtn);
					//driver.get(BNConstants.guestcheckoutURL);
					wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
					//Thread.sleep(1000);
					
					if(BNBasicfeature.isElementPresent(guestCheckoutAddPageTitle))
					{
						log.add("The User is naviagted to the Guest Checkout Page.");
						log.add("The Expected Checkout Page Title is : " + cellVal);
						log.add("The Expected Checkout Page Title is : " + guestCheckoutAddPageTitle.getText());
						if(guestCheckoutAddPageTitle.getText().contains(cellVal))
						{
							Pass("The Guest Checkout Page title matches the expected content.",log);
						}
						else
						{
							Fail("The Guest Checkout Page title does not matches the expected content.",log);
						}
						
						if(BNBasicfeature.isElementPresent(checkoutSteps))
						{
							Pass("The Checkout Steps is displayed.");
						}
						else
						{
							Fail("The Checkout Steps is not displayed.");
						}
					}
					else
					{
						Fail("The user is not navigated to the Guest Checkout Page.");
					}
				}
				else
				{
					Fail("The Checkout as Guest button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 469 Issue ." + e.getMessage());
				Exception(" BRM - 469 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign out.");
		}
	}
	
	/* BRM - 470 Verify whether clicking on back from shipping address page loaded on selecting checkout as guest from checkout overlay loads the correspodning page back.*/
	public void miniCartCheckoutAsGuestBackNavigation()
	{
		ChildCreation("BRM - 470 Verify whether clicking on back from shipping address page loaded on selecting checkout as guest from checkout overlay loads the corresponding page back.");
		if(signedOut==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(guestCheckoutAddPageTitle))
				{
					log.add("The User is naviagted to the Guest Checkout Page.");
					driver.navigate().back();
					//wait.until(ExpectedConditions.visibilityOf(promoCarousel));
					wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
					if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
					{
						Pass("The user is navigated back to the user intended page.");
					}
					else
					{
						Fail("The user is not navigated back to the user intended page.");
					}
				}
				else
				{
					Fail("The user is not navigated to the Guest Checkout Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 470 Issue ." + e.getMessage());
				Exception(" BRM - 470 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign out.");
		}
	}
	
	/* BRM - 471 Verify whether clicking on "x" on top closes the shopping bag page and displays the home page/previous page.*/
	public void miniCartLoadAndClose()
	{
		ChildCreation("BRM - 471 Verify whether clicking on x on top closes the shopping bag page and displays the home page/previous page.");
		if(signedOut==true)
		{
			try
			{
				Thread.sleep(1000);
				wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
				if(BNBasicfeature.isElementPresent(bagIcon))
				{
					log.add("The Shopping Bag is displayed.");
					boolean loadMin = loadMiniCart();
					if(loadMin == true)
					{
						if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
						{
							log.add("The MiniCart Close Icon is displayed.");
							jsclick(miniCartShoppingCloseIcon);
							wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
							wait.until(ExpectedConditions.visibilityOf(menu));
							if(BNBasicfeature.isElementPresent(header))
							{
								Pass("The Tab Header Section is displayed.");
							}
							else
							{
								Fail("The Tab Header Section is not displayed and the user is not navigated to the home page.");
							}
						}
						else
						{
							Fail("The Close icon is not displayed in the Mini Cart page.");
						}
					}
					else
					{
						Fail("The Mini Cart Failed to load.");
					}
				}
				else
				{
					Fail("The Shopping Bag is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 471 Issue ." + e.getMessage());
				Exception(" BRM - 471 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign out.");
		}
	}
	
	/* BRM - 467 Verify whether on Signing In using "sign in" overlay displayed on selecting checkout button loads the registered checkout page.*/
	public void miniCartCheckoutPageNavigationFromSignIn()
	{
		ChildCreation("BRM - 467 Verify whether on Signing In using Sign In overlay displayed on selecting checkout button loads the registered checkout page.");
		if(signedOut==true)
		{
			try
			{
				String Username = BNBasicfeature.getExcelVal("BRM467", sheet, 3);
				String Password = BNBasicfeature.getExcelVal("BRM467", sheet, 4);
				Thread.sleep(100);
				wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
				Thread.sleep(900);
				boolean loadMin = loadMiniCart();
				if(loadMin==true)
				{
					jsclick(mcContinueToCheckOutBtn.get(0));
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(signInpageframe));
					if(BNBasicfeature.isElementPresent(emailId))
					{
						emailId.sendKeys(Username);
						log.add("The User Name is successfully entered.");
						if(BNBasicfeature.isElementPresent(password))
						{
							Thread.sleep(100);
							password.sendKeys(Password);
							log.add("The Password is successfully entered.");
							jsclick(secureSignIn);
							wait.until(ExpectedConditions.visibilityOf(checkoutTopSubmitOrder));
							signedIn = true;
							if(BNBasicfeature.isElementPresent(checkoutTopSubmitOrder))
							{
								Pass("The User is navigated to the Registered User Checkout Page.");
							}
							else
							{
								Fail("The User is not navigated to the Registered User Checkout Page.");
							}
						}
						else
						{
							Fail("Password field not found.",log);
						}
					}
					else
					{
						Fail("Email Id field not found.");
					}
				}
				else
				{
					Fail("The User failed to load the mini-cart.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 467 Issue ." + e.getMessage());
				Exception(" BRM - 467 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign out.");
		}
	}
	
	/* BRM - 461 Verify whether clicking on "Apply" on entering invalid coupon code displays the alert message with textbox highlighted in red.*/
	public void miniCartInvalidCouponCode()
	{
		ChildCreation("BRM - 461 Verify whether clicking on Apply on entering invalid coupon code displays the alert message with textbox highlighted in red.");
		if(signedIn==true)
		{
			try
			{
				Thread.sleep(100);
				wait.until(ExpectedConditions.elementToBeClickable(bagIcon));
				Thread.sleep(900);
				boolean loadMin = loadMiniCart();
				if(loadMin==true)
				{
					String expAlert = BNBasicfeature.getExcelVal("BRM461", sheet, 2);
					String copCde = BNBasicfeature.getExcelNumericVal("BRM461", sheet, 7);
					String expColoCde = BNBasicfeature.getExcelNumericVal("BRM461", sheet, 8);
					if(BNBasicfeature.isElementPresent(miniCartCouponField))
					{
						BNBasicfeature.scrolldown(miniCartCouponField, driver);
						log.add("The Coupon field is displayed.");
						miniCartCouponField.click();
						miniCartCouponField.sendKeys(copCde);
						jsclick(miniCartCouponApplyButton);
						//Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(miniCartCouponError));
						log.add("The expected alert was : " + expAlert);
						String actalert = miniCartCouponError.getText();
						log.add("The Actual alert is  : " + actalert);
						if(expAlert.contains(actalert))
						{
							Pass("The alert matches with the expected content.",log);
						}
						else
						{
							Fail("The alert does not matches with the expected content.",log);
						}
						
						String actcolor = miniCartCouponField.getCssValue("border").replace("1px solid", "");
						Color colcnvt = Color.fromString(actcolor);
						String hexCode = colcnvt.asHex();
						log.add("The expected color was : " + expColoCde);
						log.add("The actual color code is  : " + hexCode);
						
						if(expColoCde.equals(hexCode))
						{
							Pass("The text field is highlighted with the expected color.",log);
						}
						else
						{
							Fail("The text field is not highlighted with the expected color.",log);
						}
						
						/*if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
						{
							jsclick(miniCartShoppingCloseIcon);
						}
						
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
						Thread.sleep(500);*/
					}
					else
					{
						Fail("The Coupon Field is not displayed.");
					}
				}
				else
				{
					Fail("The mini cart failed to load.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 461 Issue ." + e.getMessage());
				Exception(" BRM - 461 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 438 Verify whether free shipping message details change according to the subtotal (matching the creative)and clicking on "details" enable the overlay*/
	public void miniCartShippingMessageDetails()
	{
		ChildCreation("BRM - 438 Verify whether free shipping message details change according to the subtotal (matching the creative)and clicking on Details enable the overlay.");
		if(signedIn==true)
		{
			try
			{
				String prevFreeMessageDetails,currFreeMessageDetails;
				boolean loadMin  = false;
				int prevBagCount = 0, currBagCount = 0;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					loadMin = true;
				}
				else
				{
					wait.until(ExpectedConditions.visibilityOf(menu));
					loadMin = loadMiniCart();
				}
				if(loadMin==true)
				{
					if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
					{
						//Thread.sleep(1000);
						prevFreeMessageDetails = miniCartShoppingCoupon.getText();
						log.add("The Free Message before adding the product : " + prevFreeMessageDetails);
						jsclick(miniCartShoppingCloseIcon);
						//System.out.println(prevFreeMessageDetails);
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
						log.add("The Close icon is clicked to close the Mini Cart overlay.");
						if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateClose))
						{
							log.add("The Mini Cart is closed.");
							wait.until(ExpectedConditions.visibilityOf(header));
							Thread.sleep(100);
							wait.until(ExpectedConditions.visibilityOf(bagCount));
							Thread.sleep(100);
							prevBagCount = 0;
							boolean bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								prevBagCount = Integer.parseInt(bagCount.getText());
								log.add("The Previous Bag Count before adding any products : " + prevBagCount);
								//System.out.println(prevBagCount);
								searchProduct(false, "BRM430");
								boolean prdtSelect = selectProduct();
								if(prdtSelect==true)
								{
									boolean atb = false;
									do
									{
										wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
										if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
										{
											boolean prdtadded = clickaddtoBag(prevBagCount,pdpPageATBBtn);
											if(prdtadded == true)
											{
												//System.out.println("Current Count : " + shoppingbagCount.getText());
												currBagCount = Integer.parseInt(bagCount.getText());
												log.add("The Current Bag Count after adding the Product  : " + currBagCount);
												/*bnLogo.click();
												wait.until(ExpectedConditions.visibilityOf(header));*/
												//Thread.sleep(1000);
												boolean loadMiniCartpage = loadMiniCart();
												if(loadMiniCartpage==true)
												{
													atb=true;
													break;
												}
												else
												{
													Fail("The Mini Cart failed to load.");
												}
											}
											else
											{
												Fail("The Product is not added.");
												break;
											}
										}
										else
										{
											driver.navigate().back();
											selectProduct();
											atb=false;
										}
									}while(atb=true);
									
									if(atb==true)
									{
										Pass("The Product was added to the Mini Cart.");
										//Thread.sleep(1000);
										currFreeMessageDetails = miniCartShoppingCoupon.getText();
										log.add("The Current Free Message after adding the product : " + currFreeMessageDetails);
										if(prevFreeMessageDetails.equals(currFreeMessageDetails))
										{
											Fail("The Free Shipping message failed to change.",log);
										}
										else
										{
											Pass("The Free Shipping message changes when products are added.",log);
										}
									}
									else
									{
										Fail("The Product was not added.",log);
									}
								}
								else
								{
									Fail("The Product is not selected from the PLP page.");
								}
							}
							else
							{
								Fail("The user failed to get the bag count.");
							}
						}
						else
						{
							Fail("The Shopping Bag Close Icon is not displayed.");
						}
					}
					else
					{
						Fail("The Mini Cart is not closed.");
					}
				}
				else
				{
					Fail("The min Cart Failed to load.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 438 Issue ." + e.getMessage());
				Exception(" BRM - 438 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 442 - Verify whether shopped items listed in shopping bag has following: product image ,product title,category,author name,actual price,registered price(if available) and the quantity.*/
	public void miniCartAddedPrdtDetails()
	{
		ChildCreation("BRM - 442 Verify whether shopped items listed in shopping bag has following: product image ,product title,category,author name,actual price,registered price(if available) and the quantity.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					Pass("The Mini Cart Purchased item list is displayed.");
					for(int i=0;i<miniCartAddedItemLists.size();i++)
					{
						WebElement prdtImage = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'pdtImage')]//img"));
						int imgResponse = BNBasicfeature.imageBroken(prdtImage, log);
						if(imgResponse==200)
						{
							Pass("The Image is not broken.",log);
						}
						else
						{
							Fail("The Image is broken.Please check.",log);
						}
							
						String prdtTitle = getprdtName(i);
						if(prdtTitle.isEmpty())
						{
							Fail("The Product Title is not displayed.");
						}
						else
						{
							Pass("The Product Title is displayed.The Name of the product is : " + prdtTitle);
						}
						try
						{
							WebElement prdtAuthor = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'pdtAuthor')]"));
							String prdtAuthorName = prdtAuthor.getText();
							try
							{
								if(prdtAuthorName.isEmpty())
								{
									Fail("The Product Author is not displayed.");
								}
								else
								{
									Pass("The Product Author Name is displayed.The Name of the author is : " + prdtAuthorName);
								}
							}
							catch(Exception e)
							{
								Fail("The Author for the Product is not displayed.");
							}
						}
						catch(Exception e)
						{
							Fail("The Author name is not dispalyed for the product.");
						}
								
						String productType = getprdtType(i);
						if(productType.isEmpty())
						{
							Fail("The Product Type is not displayed.");
						}
						else
						{
							Pass("The Product Type is displayed.The Name of the product type is : " + productType);
						}
								
						WebElement salePrice = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'SalePrice')]"));
						String currentsalePrice = salePrice.getText();
						//System.out.println(currentsalePrice);
						if(BNBasicfeature.isElementPresent(salePrice))
						{
							if(currentsalePrice.isEmpty())
							{
								Fail("The Sale Price is not displayed and it seems to be empty.");
							}
							else
							{
								Pass("The Sale Price is displayed and the displayed Sale price is : " + currentsalePrice);
							}
							
						}
						else
						{
							Fail("The Sale Price is not displayed.");
						}
								
						try
						{
							WebElement regisgteredPrice = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[contains(@class,'pdtPrice_reg')]//*[contains(@class,'pdtPriceVal')]"));
							if(BNBasicfeature.isElementPresent(regisgteredPrice))
							{
								String currentregPrice = regisgteredPrice.getText();
								if(currentregPrice.isEmpty())
								{
									Fail("The Registered Price is not displayed and it seems to be empty.");
								}
								else
								{
									Pass("The Registered Price is displayed and the displayed Registered Price is : " + currentregPrice);
								}
							}
							else
							{
								Fail("The Registered Price is not displayed");
							}
						}
						catch(Exception e)
						{
							Pass("The Registered Price is not available.");
						}
					}
				}
				else
				{
					Fail("The Mini Cart Purchased item list is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 442 Issue ." + e.getMessage());
				Exception(" BRM - 442 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 441 Verify whether mini cart in header has its count updated instantly as the items are added to bag.*/
	public void miniCartProductAddedCount()
	{
		ChildCreation("BRM - 441 Verify whether mini cart in header has its count updated instantly as the items are added to bag.");
		if(signedIn==true)
		{
			int successIdsize;
			int prevShopBagCount,currentShopbagCount;
			try
			{
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					BNBasicfeature.scrollup(miniCartShoppingBagTitle, driver);
					jsclick(miniCartShoppingCloseIcon);
				}
				wait.until(ExpectedConditions.visibilityOf(header));
				String ean = BNBasicfeature.getExcelVal("BRM441", sheet, 7);
				String cellVal = ean;
				String[] eanNum = cellVal.split("\n");
				boolean bkfound = false;
				boolean bookFound = false;
				for(int i = 0; i<eanNum.length;i++)
				{
					bkfound = searchEanNumbers(eanNum[i]);
					 if(bkfound == true)
					 {
						 if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
						 {
							 bookFound = true;
							 break;
						 }
						 else
						 {
							 bookFound = false;
							 continue;
						 }
					 }
				}
				if(bookFound==true)
				{
					wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
					log.add("The Searched content is found.");
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						prevShopBagCount = Integer.parseInt(bagCount.getText());
						//System.out.println(shoppingbagCount.getText());
						if(BNBasicfeature.isElementPresent(productAddedSuccessId))
						{
							successIdsize = driver.findElements(By.xpath("//*[@id='sk_addtobagSuccess_id']")).size();
							boolean prdtAdded = clickaddtoBag(prevShopBagCount, pdpPageATBBtn);
							if(prdtAdded==true)
							{
								currentShopbagCount = Integer.parseInt(bagCount.getText());
								if((currentShopbagCount==prevShopBagCount))
								{
									Fail("The added product count is not added or displayed.",log);
								}
								else
								{
									Pass("The product count is displayed and updated in the Shopping Bag.",log);
								}
							}
							else
							{
								Fail("The Mini Cart fly is not displayed.",log);
							}
						}
						else
						{
							successIdsize = 0;
							if(BNBasicfeature.isElementPresent(pdpPageProductDropDown))
							{
								BNBasicfeature.scrolldown(pdpPageAddToWishListContainer, driver);
								List<WebElement> ele = pdpPageProductTypes;
								nookAdded = false;
								for(int i = 1; i<=ele.size();i++)
								{
									String actVal = ele.get(i).getAttribute("value");
									if(!actVal.contains("NOOK"))
									{
										jsclick(ele.get(i));
										//ele.get(i).click();
										Thread.sleep(500);
										type = ele.get(i).getAttribute("value");
										nookAdded = true;
										break;
									}
									else
									{
										nookAdded = false;
										continue;
									}
								}
							}
							if(nookAdded==true)
							{
								wait.until(ExpectedConditions.visibilityOf(pdpPageATBBtn));
								Thread.sleep(500);
								boolean addBag = clickaddtoBag(prevShopBagCount,pdpPageATBBtn);
								if(addBag==true)
								{
									List<WebElement> totalsuccessIdSize = driver.findElements(By.xpath("//*[@id='sk_addtobagSuccess_id']"));
									if(BNBasicfeature.isElementPresent(pdpPageMiniCartfly))
									{
										Pass("The MiniCart Fly is displayed.");
										//BNBasicfeature.scrollup(header, driver);
										currentShopbagCount = Integer.parseInt(bagCount.getText());
										log.add("The Current Shopping Bag Count after adding the product : " + bagCount.getText());
										//Thread.sleep(500);
										//System.out.println(currentShopbagCount);
										if((currentShopbagCount==prevShopBagCount)&&(successIdsize<totalsuccessIdSize.size()))
										{
											Fail("The added product count is not added or displayed.",log);
										}
										else
										{
											Pass("The product count is displayed and updated in the Shopping Bag.",log);
										}
									}
									else
									{
										Fail("The Mini Cart fly is not displayed.",log);
									}
								}
								else
								{
									Fail("The product was failes to add.",log);
								}
							}
							else
							{
								Skip("The Nook Products is not dispalyed in the drop down.");
							}
						}
					}
					else
					{
						Fail( "The User failed to get the bag count.");
					}
				}
				else
				{
					Fail("The Searched product content is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 441 Issue ." + e.getMessage());
				Exception(" BRM - 441 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 473 Verify on adding same item of different category to bag which was already shopped/in bag ,their details should be displayed individually in shopping bag page.*/
	public void miniCartDifferentCategorySameProduct() throws Exception
	{
		ChildCreation("BRM - 473 Verify on adding same item of different category to bag which was already shopped/in bag ,their details should be displayed individually in shopping bag page.");
		if(signedIn==true)
		{
			int i=0;
			if(nookAdded==true)
			{
				try
				{
					String bookName = BNBasicfeature.getExcelVal("BRM441", sheet, 5);
					if(BNBasicfeature.isElementPresent(pdpPageProductDropDown))
					{
						BNBasicfeature.scrolldown(pdpPageAddToWishListContainer, driver);
						log.add("The Format drop down is displayed.");
						String prdtType = "";
						List<WebElement> ele = pdpPageProductTypes;
						for(i = 1; i<=ele.size();i++)
						{
							String actVal = ele.get(i).getAttribute("value");
							//if((actVal.contains("NOOK")||actVal.contains("PAPER"))&&(!actVal.contains(type)))
							if(!actVal.contains(type))
							{
								prdtType = ele.get(i).getText();
								break;
							}
						}
						log.add("The Product format before changing was : " + prdtType);
						String prdtIdtoAdd = productId.getAttribute("identifier");
						//System.out.println(prdtIdtoAdd);
						log.add("The Product Identifer before changing the format is : " + prdtIdtoAdd);
						boolean bgCnt = getBagCount();
						if(bgCnt==true)
						{
							int prevVal = Integer.parseInt(bagCount.getText());
							//Thread.sleep(1000);
							//pdpPageProductDropDown.click();
							//Thread.sleep(1000);
							ele = pdpPageProductTypes;
							for(i = 1; i<=ele.size();i++)
							{
								String actVal = ele.get(i).getAttribute("value");
								//if(((actVal.contains("NOOK")||actVal.contains("PAPER"))&&(!actVal.contains(type))))
								if(!actVal.contains(type))
								{
									ele.get(i).click();
									break;
								}
								else
								{
									continue;
								}
							}
							//Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							wait.until(ExpectedConditions.visibilityOf(pdpPageProductDropDown));
							BNBasicfeature.scrolldown(pdpPageAddToWishListContainer, driver);
							//Thread.sleep(1000);
							ele = pdpPageProductTypes;
							prdtType = ele.get(i).getText();
							log.add("The Current Product format is : " + prdtType);
							//System.out.println(prdtType);
							prdtIdtoAdd = productId.getAttribute("identifier");
							log.add("The Product Identifier found in the Product page is  :" + prdtIdtoAdd);
							//System.out.println(prdtIdtoAdd);
							boolean addprdt = clickaddtoBag(prevVal, pdpPageATBBtn); 
							if(addprdt==true)
							{
								/*bnLogo.click();
								wait.until(ExpectedConditions.visibilityOf(menu));*/
								//Thread.sleep(1000);
								boolean loadMiniCartpage = loadMiniCart();
								if(loadMiniCartpage==true)
								{
									boolean prdtfound = false; 
									WebElement miniCartPrdtId;
									String miniCartId; 
									if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
									{
										int count = 0;
										prdtfound = false;
										for(i=0;i<miniCartAddedItemLists.size();i++)
										{
											String prdtTitle = getprdtName(i);
											if(!prdtTitle.isEmpty())
											{
												if(prdtTitle.contains(bookName))
												{
													miniCartPrdtId = miniCartAddedItemLists.get(i);
													miniCartId = miniCartPrdtId.getAttribute("identifier");
													
													log.add("The Product Identifier found in the Mini Cart Bag is  :" + miniCartId);
													String productType = getprdtType(i);
													if(productType.isEmpty())
													{
														Fail("The Product Type is not displayed.");
													}
													else
													{
														log.add("The Product Type is displayed.The Name of the product type is : " + productType);
													}
													
													if(prdtTitle.isEmpty())
													{
														Fail("The Product Title is not displayed.");
													}
													else
													{
														log.add("The Product Title is displayed.The Name of the product is : " + prdtTitle);
													}
													prdtfound=true;
													count++;
												}
												else
												{
													continue;
												}
											}
											else
											{
												Fail("The product name is not dispalyed.");
											}
										}
										
										if(prdtfound==true)
										{
											Pass("The added product identifier is found in the minicart.",log);
											if(count>=2)
											{
												Pass("The Products are displayed seperately as expected.");
											}
											else
											{
												Fail("The Products are not displayed seperately.");
											}
										}
										else
										{
											Fail("The Added product identifer is not found.",log);
										}
									}
									else
									{
										Fail("The Mini Cart list is not displayed.");
									}
								}
								else
								{
									Fail("Failed to load Mini Cart page.");
								}
							}
							else
							{
								Fail("Failed to add the Product to the bag.");
							}
						}
						else
						{
							Fail(" The user failed to get the bag count.");
						}
					}
					else
					{
						Fail("The PDP Page Product Drop down is not displayed.");
					}
				}
				catch(Exception e)
				{
					System.out.println(" BRM - 473 Issue ." + e.getMessage());
					Exception(" BRM - 473 Issue." + e.getMessage());
				}
				/*if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
				{
					jsclick(miniCartShoppingCloseIcon);
					wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
					wait.until(ExpectedConditions.visibilityOf(menu));
				}*/
			}
			else
			{
				Skip("The Product was not added to check.");
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 451 Verify whether shopped items in shopping bag page has "Save for later" option and clicking on it adds that particular item to saved items and the shopping bag should be updated. */
	public void miniCartSaveforLater()
	{
		ChildCreation("BRM - 451 Verify whether shopped items in shopping bag page has Save for later option and clicking on it adds that particular item to saved items and the shopping bag should be updated.");
		if(signedIn==true)
		{
			try
			{
				boolean miniCartloaded  = false;
				Integer prevbagCount = Integer.parseInt(bagCount.getText());
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					miniCartloaded = true;
				}
				else
				{
					miniCartloaded = loadMiniCart();
				}
				/*if(BNBasicfeature.isElementPresent(bnLogo))
				{
					jsclick(bnLogo);
					wait.until(ExpectedConditions.visibilityOf(header));
					//Thread.sleep(1000);
				}*/
				
				if(miniCartloaded==true)
				{
					log.add("The mini cart is loaded.");
					String prevItemInfo = miniCartShoppingItemCountInfo.getText();
					
					int prevSavedItemSize,currentSavedItemSize;
					if(BNBasicfeature.isListElementPresent(miniCartSavedItemLists))
					{
						prevSavedItemSize = miniCartSavedItemLists.size();
						log.add("The Previous Saved item list before adding is : " + prevSavedItemSize);
					}
					else
					{
						 prevSavedItemSize = 0;
						 log.add("The Previous Saved item list before adding is : " + prevSavedItemSize);
					}
					
					if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
					{
						log.add("The added item list is displayed.");
						Random r = new Random();
						int sel = r.nextInt(miniCartAddedItemLists.size());
						String prdtName = getprdtName(sel);
						String prdtId = getprdtIdentifier(sel);
						WebElement saveForLater = miniCartAddedItemSaveForLater.get(sel);
						jsclick(saveForLater);
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "display: none;"));
						String currItemInfo = miniCartShoppingItemCountInfo.getText();
						log.add("The previous shopping infomation count is : " + prevItemInfo);
						log.add("The current shopping infomation count is : " + currItemInfo);
						if(prevItemInfo.equals(currItemInfo))
						{
							Fail("The Shopping Information Count is not updated.",log);
						}
						else
						{
							Pass("The Shopping Information Count is updated.",log);
						}
						
						if(BNBasicfeature.isListElementPresent(miniCartSavedItemLists))
						{
							log.add("The Mini Cart Saved item list is displayed.");
							currentSavedItemSize = miniCartSavedItemLists.size();
							log.add("The Previous Saved Item list size : " + prevSavedItemSize);
							log.add("The Previous Saved Item list size : " + currentSavedItemSize);
							if(currentSavedItemSize>prevSavedItemSize)
							{
								Pass("The Size of the My Saved Item List is as expected.",log);
							}
							else
							{
								Fail("The Size of the My Saved Item List differ.",log);
							}
							
							BNBasicfeature.scrolldown(miniCartMySavedItems, driver);
							boolean prdtAdded = false;
							int i;
							log.add("The Expected Product id in the My Saved items list is : " + prdtId);
							for(i=1; i <= miniCartSavedItemLists.size(); i++)
							{
								String savedprdtId = miniCartSavedItemPrdtAddtoBagButton.get(i-1).getAttribute("identifier");
								//System.out.println(savedprdtId);
								if(savedprdtId.contains(prdtId))
								{
									log.add("The Found Product id in the My Saved items list is : " + prdtId);
									prdtAdded = true;
									break;
								}
								else
								{
									prdtAdded = false;
									continue;
								}
							}
							
							if(prdtAdded==true)
							{
								Pass("There product id matches the expected content.",log);
							}
							else
							{
								Fail("There is some mismatch in the expected product id.",log);
							}
							
							String savedprdtName = miniCartSavedItemPrdtTitle.get(i-1).getText();
							log.add("The Expected Product Name : " + prdtName);
							log.add("The Current Product Name : " + savedprdtName);
							if(prdtName.contains(savedprdtName))
							{
								Pass("There expected product name matches with the in the My Saved item list.",log);
							}
							else
							{
								Pass("There is some mismatch in the expected product name in the My Saved item list.",log);
							}
						
							/*jsclick(miniCartShoppingCloseIcon);
							wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
							wait.until(ExpectedConditions.visibilityOf(menu));*/
							//Thread.sleep(1000);
							boolean bgCnt = getBagCount();
							if(bgCnt==true)
							{
								Integer currbagCount = Integer.parseInt(bagCount.getText());
								log.add("The Previous Bag Count before adding is : " + prevbagCount);
								log.add("The Current Bag Count after adding is : " + currbagCount);
								if(currbagCount<prevbagCount)
								{
									Pass("The Bag Count is updated.",log);
								}
								else
								{
									Fail("The Bag Count is not updated.",log);
								}
							}
							else
							{
								Fail( "The User failed to get the bag count.");
							}
						}
						else
						{
							Fail("The mini Cart saved item list is not displayed.");
						}
					}
					else
					{
						Fail("The added item list is not displayed.",log);
					}
				}
				else
				{
					Fail("The Mini cart is not loaded.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 451 Issue ." + e.getMessage());
				Exception(" BRM - 451 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 420 Verify whether clicking on "Add to Bag" for product in My Saved Item adds that particular item to bag by updating the count in mini cart and displaying it along with items purchased.*/
	public void minicartShoppingBagSavedItemAddtoBag()
	{
		ChildCreation("BRM - 420 Verify whether clicking on Add to Bag for product in My Saved Item adds that particular item to bag by updating the count in mini cart and displaying it along with items purchased.");
		if(signedIn==true)
		{
			try
			{
				boolean loadMin = false;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					loadMin = true;
				}
				else
				{
					loadMin = loadMiniCart();
				}
				if(loadMin==true)
				{
					BNBasicfeature.scrollup(miniCartMySavedItemsTitle, driver);
					wait.until(ExpectedConditions.visibilityOf(miniCartShoppingItemCountInfo));
					String prevBagCount = miniCartShoppingItemCountInfo.getText();
					log.add("The Previous bag count was : " + prevBagCount);
					if(BNBasicfeature.isListElementPresent(miniCartSavedItemLists))
					{
						BNBasicfeature.scrolldown(miniCartMySavedItemsTitle, driver);
						Random r = new Random();
						int sel = r.nextInt(miniCartSavedItemLists.size());
						WebElement svdPrdt = miniCartSavedItemPrdtAddtoBagButton.get(sel);
						String prdtId = svdPrdt.getAttribute("identifier");
						jsclick(svdPrdt);
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						JavascriptExecutor js = (JavascriptExecutor)driver;
						js.executeScript("return document.readyState").toString().equals("complete");
						//Thread.sleep(1500);
						String currBagCount = miniCartShoppingItemCountInfo.getText();
						log.add("The Current bag count is : " + currBagCount);
						if(prevBagCount.equals(currBagCount))
						{
							Fail("The total items in the mini cart header is not updated.",log);
						}
						else
						{
							Pass("The total items in the mini cart header is updated.",log);
						}
						boolean prdtAdded = false;
						if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
						{
							Pass("The Items in the Mini Cart bag is displayed.");
							String itemsPrdtItentifier = null; 
							WebElement prdt;
							for(int i = 0;i<miniCartAddedItemLists.size();i++)
							{
								//prdt = driver.findElement(By.xpath("(//*[@id='sk_shoppingBagItemsContainer']//*[@id='sk_shoppingBag_PdtWrapper']//*[@class='sk_shoppingBag_PdtItemContainer'])["+i+"]"));
								prdt = miniCartAddedItemLists.get(i);
								BNBasicfeature.scrolldown(prdt, driver);
								itemsPrdtItentifier = prdt.getAttribute("identifier");
								//System.out.println(itemsPrdtItentifier);
								if(itemsPrdtItentifier.equals(prdtId))
								{
									prdtAdded = true;
									break;
								}
								else
								{
									prdtAdded = false;
									continue;
								}
							}
							
							if(prdtAdded==true)
							{
								log.add("The Selected product id from the list is : " + prdtId);
								log.add("The Added product itentifier is : " + itemsPrdtItentifier);
								Pass("The Product is successfully added from the My Saved Items lists and it is displayed.",log);
							}
							else
							{
								Fail("The Product identifier is not found in the list of items in the minicart.");
							}
						}
						else
						{
							Fail("The Items in the Mini Cart bag is not displayed.");
						}
					}
					else
					{
						Fail("The My Saved Items list container is empty or not found.");
					}
				}
				else
				{
					Fail("The Mini Cart failed to load.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 420 Issue ." + e.getMessage());
				Exception(" BRM - 420 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 443 Verify whether the shopped item(except Nook Book category) details has "update" option and clicking on it after entering the required count updates the quantity , correspondingly in number of items in shopping bag page and in mini cart.*/
	public void miniCartUpdateButton()
	{
		ChildCreation("BRM - 443 Verify whether the shopped item(except Nook Book category) details has update option and clicking on it after entering the required count updates the quantity , correspondingly in number of items in shopping bag page and in mini cart.");
		if(signedIn==true)
		{
			String prevminiCartCountInfo = "",currminiCartCountInfo = "";
			boolean itemUpdated = false;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM443", sheet, 2);
				String[] Val = cellVal.split("\n");
				prevminiCartCountInfo = miniCartShoppingItemCountInfo.getText();
				log.add("The Previous Mini Cart Count Information  was : " + prevminiCartCountInfo);
				if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
				{
					for(int i=0; i<miniCartAddedItemLists.size();i++)
					{
						WebElement prdtName = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_pdtTitle']"));
						BNBasicfeature.scrolldown(prdtName, driver);
						String prdtTitle = getprdtName(i);
						log.add("The Product Title is : " + prdtTitle);
						if(prdtTitle.isEmpty())
						{
							Fail("The Product Title is not displayed.");
						}
						else
						{
							Pass("The Product Title is displayed.The Name of the product is : " + prdtTitle);
						}
						
						String productType = getprdtType(i);
						log.add("The Product Type is : " + productType);
						if(productType.isEmpty())
						{
							Fail("The Product Type is not displayed.");
						}
						else
						{
							Pass("The Product Type is displayed.The Name of the product type is : " + productType);
							//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_qtyUpdate']")));
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_qtyUpdate']"));
							if(BNBasicfeature.isElementPresent(prdtUpdatebtn))
							{
								if((productType.contains(Val[0]))||(productType.contains(Val[1])))
								{
									try
									{
										prdtUpdatebtn.click();
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
										Fail("The Update button is displayed for the Product type Nook Book.",log);
									}
									catch(Exception e)
									{
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
										Pass("The Update button is not displayed for the Product type Nook Book.",log);
									}
									
									try
									{
										WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_qtyUpdate']"));
										prdtQtyTextfield.click();
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
										Fail("The Qty field is displayed for the Nook Book device.It should not be.");
									}
									catch(Exception e)
									{
										wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
										Pass("The Update Qty text box is not visible.");
									}
								}
								else
								{
									//WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_qtyUpdate']"));
									WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_QtyForm']"));
									Actions act = new Actions(driver);
									act.moveToElement(prdtQtyTextfield).click().build().perform();
									act.moveToElement(prdtQtyTextfield).sendKeys(Keys.DELETE).build().perform();
									act.sendKeys("5").build().perform();
									jsclick(prdtUpdatebtn);
									//wait.until(ExpectedConditions.visibilityOf(miniCartLoadingGauge)).getAttribute("style:none;");
									wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
									currminiCartCountInfo = miniCartShoppingItemCountInfo.getText();
									log.add("The Current Mini Cart Count Information  is : " + currminiCartCountInfo);
									if(prevminiCartCountInfo.equals(currminiCartCountInfo))
									{
										Fail("The Product Count is not updated.",log);
										itemUpdated = false;
										break;
									}
									else
									{
										Pass("The Update text field and the Update button is visible for the product type.",log);
										itemUpdated = true;
										break;
									}
								}
							}
							else
							{
								Fail("The product update button is not displayed.");
							}
						}
					}
					
					if(itemUpdated==true)
					{
						Pass( " The User was able to Update the Quantity for the Products.");
					}
					else
					{
						Fail( " The User was not able to Update the Quantity for the Products.");
					}
				}
				else
				{
					Fail("The Mini Cart seems to be empty.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 443 Issue ." + e.getMessage());
				Exception(" BRM - 443 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 446 Verify on updating the quantity as zero for shopped item,it should be removed from shopping bag page also whether corresponding reduction is made in number of items at top shopping bag page and in mini cart.*/
	public void miniCartZeroQtyProductRemove()
	{
		ChildCreation("BRM - 446 Verify on updating the quantity as zero for shopped item,it should be removed from shopping bag page also whether corresponding reduction is made in number of items at top shopping bag page and in mini cart.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM446", sheet, 2);
				String[] Val = cellVal.split("\n");
				String qtyVal = BNBasicfeature.getExcelNumericVal("BRM446", sheet, 6);
				BNBasicfeature.scrollup(miniCartShoppingBagTitle, driver);
				int prevBagVal = 0,currentBagVal = 0;
				WebElement prdtType; 
				boolean loadMin = false;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					loadMin = true;
				}
				else
				{
					loadMin = loadMiniCart();	
				}
				if(loadMin==true)
				{
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						prevBagVal = Integer.parseInt(bagCount.getText());
						if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
						{
							int sel;
							do
							{
								Random r = new Random();
								sel = r.nextInt(miniCartAddedItemLists.size());
								prdtType = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtType']"));
								//System.out.println(prdtType.getText());
							}while(!prdtType.getText().contains(Val[0])==false||!prdtType.getText().contains(Val[1])==false||!prdtType.getText().contains(Val[2])==false);
							
							WebElement prdtIdentifier = miniCartAddedItemLists.get(sel);
							String prdtId = prdtIdentifier.getAttribute("identifier");
							log.add("The Product id randomly choosen to update id : " + prdtId);
							//System.out.println(prdtId);
							
							BNBasicfeature.scrolldown(prdtIdentifier, driver);
							WebElement prdtName = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtTitle']"));
							String prdtTitle = prdtName.getText();
							log.add("The Name of the product is : " + prdtTitle);
							if(prdtTitle.isEmpty())
							{
								Fail("The Product Title is not displayed.");
							}
							else
							{
								Pass("The Product Title is displayed.The Name of the product is : " + prdtTitle);
							}
							
							Integer previousAddedItemsListSize = miniCartAddedItemLists.size();
							
							BNBasicfeature.scrolldown(prdtType, driver);
							WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_QtyForm']"));
							Actions act = new Actions(driver);
							act.moveToElement(prdtQtyTextfield).click().build().perform();
							act.moveToElement(prdtQtyTextfield).sendKeys(Keys.DELETE).build().perform();
							act.moveToElement(prdtQtyTextfield).sendKeys(qtyVal).build().perform();
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_qtyUpdate']"));
							jsclick(prdtUpdatebtn);
							//wait.until(ExpectedConditions.visibilityOf(miniCartLoadingGauge)).getAttribute("style").contains("display:none;");
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							Thread.sleep(500);
							if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
							{
								boolean prdtRemoved = false;
								Integer currentAddedItemsListSize = miniCartAddedItemLists.size();
								for(int i =0;i<miniCartAddedItemLists.size();i++)
								{
									prdtIdentifier = miniCartAddedItemLists.get(i);
									//System.out.println();
									BNBasicfeature.scrolldown(prdtIdentifier, driver);
									String currentPrdtId = prdtIdentifier.getAttribute("identifier");
									if(currentPrdtId.equals(prdtId))
									{
										prdtRemoved = true;
										break;
									}
									else
									{
										prdtRemoved = false;
										continue;
									}
								}
								
								if(prdtRemoved == true)
								{
									Fail("The Product is not removed from the list and the minicart still displays the product.",log);
								}
								else
								{
									Pass("The Product is removed from the list and the minicart does not display the product.",log);
									BNBasicfeature.scrollup(miniCartShoppingCloseIcon, driver);
									/*jsclick(miniCartShoppingCloseIcon);
									wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
									wait.until(ExpectedConditions.visibilityOf(menu));
									Thread.sleep(500);*/
									bgCnt = getBagCount();
									if(bgCnt==true)
									{
										currentBagVal = Integer.parseInt(bagCount.getText());
										log.add("The Previous Bag Count is : " + prevBagVal);
										log.add("The Current Bag Count is : " + currentBagVal);
										if(prevBagVal==currentBagVal)
										{
											Fail("The Value is not updated in the Shopping Bag aswell.");
										}
										else
										{
											Pass("The Value is updated in the Shopping Bag.");
										}
										
										log.add("The Previous Added Item list size before updating the product was : " + previousAddedItemsListSize);
										currentAddedItemsListSize = miniCartAddedItemLists.size();
										log.add("The Current Added Item list size after updating the product was : " + currentAddedItemsListSize);
										if(previousAddedItemsListSize.equals(currentAddedItemsListSize))
										{
											Fail("The mini cart saved item list size is still same.",log);
										}
										else
										{
											Pass("The Mini Cart Added item list size different.",log);
										}
									}
									else
									{
										Fail( "The User failed to get the product count after update.");
									}
								}
							}
							else
							{
								Fail("The Mini Cart list is not displayed.");
							}
						}
						else
						{
							Fail("The item list in the mini cart seems to be empty.");
						}
					}
					else
					{
						Fail(" The User failed to get the bagcount.");
					}
				}
				else
				{
					Fail("The Mini Cart Failed to load.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 446 Issue ." + e.getMessage());
				Exception(" BRM - 446 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 445 Verify whether clicking on quantity box numeric keypad is enabled and must be able to enter only numeric values till 999.*/
	public void miniCartUpdateQtyLimit()
	{
		ChildCreation("BRM - 445 Verify whether clicking on quantity box numeric keypad is enabled and must be able to enter only numeric values till 999.");
		if(signedIn==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM445", sheet, 2);
				String lengthCellVal = BNBasicfeature.getExcelNumericVal("BRM445", sheet, 6);
				String[] Val = lengthCellVal.split("\n");
				/*if(BNBasicfeature.isElementPresent(bnLogo))
				{
					bnLogo.click();
					wait.until(ExpectedConditions.visibilityOf(menu));
					Thread.sleep(1000);
				}*/
				boolean loadmin = false;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					loadmin = true;
				}
				else
				{
					loadmin = loadMiniCart();
				}
				if(loadmin==true)
				{
					if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
					{
						WebElement prdtType; 
						int sel;
						do
						{
							Random r = new Random();
							sel = r.nextInt(miniCartAddedItemLists.size());
							prdtType = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtType']"));
							//System.out.println(prdtType.getText());
							//System.out.println(!prdtType.getText().contains(cellVal));
						}while(!prdtType.getText().contains(cellVal)==false);
						
						//System.out.println(prdtType.getText());
						BNBasicfeature.scrolldown(prdtType, driver);
						//WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_qtyUpdate']"));
						WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtUpdateQty']"));
						
						for(int i = 0;i<Val.length;i++)
						{
							Actions act = new Actions(driver);
							act.moveToElement(prdtQtyTextfield).click().build().perform();
							act.sendKeys(Keys.DELETE).build().perform();
							act.sendKeys(Val[i]).build().perform();
							//prdtQtyTextfield.sendKeys(Val[i]);
							log.add("The Entered Qty from the excel was : " + Val[i]);
							WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_qtyUpdate']"));
							jsclick(prdtUpdatebtn);
							wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
							
							//Thread.sleep(1000);
							/*jsclick(miniCartShoppingCloseIcon);
							wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
							wait.until(ExpectedConditions.visibilityOf(menu));*/
							loadmin = false;
							if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
							{
								loadmin = true;
							}
							else
							{
								loadmin = loadMiniCart();
							}
							Thread.sleep(500);
							//Thread.sleep(1000);
							//boolean loadMin = loadMiniCart();
							if(loadmin==true)
							{
								prdtType = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtType']"));
								BNBasicfeature.scrolldown(prdtType, driver);
								prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtUpdateQty']"));
								int currentQty = Integer.parseInt(prdtQtyTextfield.getAttribute("value"));
								log.add("The Current Qty in the field is : " + currentQty);
								//System.out.println(currentQty);
								if(currentQty<1000)
								{
									Pass("The Value is less than 1000.",log);
								}
								else 
								{
									Fail("The Value is greater than 1000.",log);
								}
							}
							else
							{
								Fail("The Mini Cart Failed to load.");
							}
						}
					}
					else
					{
						Fail("The Mini Cart list seems to be empty.");
					}
				}
				else
				{
					Fail("The Mini Cart failed to load.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 445 Issue ." + e.getMessage());
				Exception(" BRM - 445 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1368 When the total product added to bag is more than 100, the count inside shopping bag icon gets truncated.*/
	public void miniCartShoppinBagCountOver100()
	{
		ChildCreation("BRM - 1368 When the total product added to bag is more than 100, the count inside shopping bag icon gets truncated.");
		if(signedIn=true)
		{
			try
			{
				boolean ldMin = false;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					ldMin = true;
				}
				else
				{
					if(BNBasicfeature.isElementPresent(miniCartShoppingCloseIcon))
					{
						jsclick(miniCartShoppingCloseIcon);
						wait.until(ExpectedConditions.attributeContains(miniCartShoppingBagStateClose, "class", "close"));
						wait.until(ExpectedConditions.visibilityOf(menu));
					}
					ldMin = loadMiniCart();
				}
				if(ldMin==true)
				{
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(bagCount))
					{
						boolean bgCnt = getBagCount();
						if(bgCnt==true)
						{
							String remv = bagCount.getText().replace("+", "");
							int prdtval = Integer.parseInt(remv);
							log.add("The Current Shopping Bag Count is : " + prdtval);
							if(prdtval>99)
							{
								Fail("The Value is not truncated when the shopping bag count is more than 100.",log);
							}
							else
							{
								Pass("The Shopping Bag value is truncated and it is not over 100",log);
							}
						}
						else
						{
							Fail("The User failed to get the product count.");
						}
					}
					else
					{
						Fail("The Shopping Bag Count is not dispalyed.");
					}
				}
				else
				{
					Fail("The Shopping Bag Close icon was not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1368 Issue ." + e.getMessage());
				Exception(" BRM - 1368 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 2056 verify whether alert is generated when a product's quantity is updated with value more than its availability.*/
	public void miniCartAvailableLimitCheck()
	{
		ChildCreation(" BRM - 2056 Verify whether alert is generated when a product's quantity is updated with value more than its availability.");
		if(signedIn==true)
		{
			try
			{
				/*if(BNBasicfeature.isElementPresent(bnLogo))
				{
					bnLogo.click();
					wait.until(ExpectedConditions.visibilityOf(menu));
					Thread.sleep(1000);
				}*/
				boolean loadMin = false;
				if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
				{
					loadMin = true;
				}
				else
				{
					loadMin = loadMiniCart(); 
				}
				if(loadMin==true)
				{
					String cellVal = BNBasicfeature.getExcelVal("BRM445", sheet, 2);
					String lengthCellVal = BNBasicfeature.getExcelNumericVal("BRM445", sheet, 6);
					String alert = BNBasicfeature.getExcelVal("BRM2056", sheet, 2);
					String[] Val = lengthCellVal.split("\n");
					if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
					{
						WebElement prdtType; 
						int sel;
						do
						{
							Random r = new Random();
							sel = r.nextInt(miniCartAddedItemLists.size());
							prdtType = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtType']"));
							//System.out.println(prdtType.getText());
							//System.out.println(!prdtType.getText().contains(cellVal));
						}while(!prdtType.getText().contains(cellVal)==false);
						
						//System.out.println(prdtType.getText());
						BNBasicfeature.scrolldown(prdtType, driver);
						//WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_qtyUpdate']"));
						WebElement prdtQtyTextfield = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_pdtUpdateQty']"));
						Actions act = new Actions(driver);
						act.moveToElement(prdtQtyTextfield).click().build().perform();
						act.sendKeys(Keys.DELETE).build().perform();
						act.sendKeys(Val[0]).build().perform();
						//prdtQtyTextfield.sendKeys(Val[i]);
						log.add("The Entered Qty from the excel was : " + Val[1]);
						WebElement prdtUpdatebtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+sel+"']//*[@class='sk_qtyUpdate']"));
						jsclick(prdtUpdatebtn);
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						WebDriverWait newWait = new WebDriverWait(driver, 10);
						try
						{
							//newWait.until(ExpectedConditions.visibilityOf(mcUpdateAlert));
							newWait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(mcUpdateAlert),
									ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none")
									)
							);
							if(BNBasicfeature.isElementPresent(mcUpdateAlert))
							{
								String actAlert = mcUpdateAlert.getText();
								log.add("The Actual alert raised was : " + actAlert);
								if(actAlert.contains(alert))
								{
									Pass("The alert was raised when the QTY was entered more than the available limit.",log);
								}
								else
								{
									Fail("There seems to be mismatch in the alert raised.",log);
								}
							}
							else
							{
								Skip("No Alert was raised for the product when qty was enetered more.");
							}
						}
						catch(Exception e)
						{
							Skip("No Alert was raised for the product when qty was enetered more.");
						}
					}
					else
					{
						Fail("The Mini Cart seems to be empty.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2056 Issue ." + e.getMessage());
				Exception(" BRM - 2056 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 1353 Verify that "Sign In to See Your Bag" should not be available when account is been signed In already.*/
	public void miniCartEmptyBagSignedInUserBtns()
	{
		ChildCreation("BRM - 1353 Verify that Sign In to See Your Bag should not be available when account is been signed In already.");
		if(signedIn==true)
		{
			try
			{
				/*if(BNBasicfeature.isElementPresent(bnLogo))
				{
					bnLogo.click();
					wait.until(ExpectedConditions.visibilityOf(menu));
					Thread.sleep(1000);
				}*/
				boolean pgok  = false;
				int count = 1;
				do
				{
					if(BNBasicfeature.isElementPresent(miniCartShoppingBagStateOpen))
					{
						pgok = true;
					}
					else
					{
						pgok = loadMiniCart();
					}
					try
					{
						//wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "display: none;"));
						wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
						if(BNBasicfeature.isListElementPresent(miniCartAddedItemLists))
						{
							//System.out.println(miniCartAddedItemLists.size());
							for(int i=0;i<miniCartAddedItemLists.size();i++)
							{
								WebElement removeBtn = driver.findElement(By.xpath("//*[@id='id_skShppngBgPrdItm_"+i+"']//*[@class='sk_removeItem']"));
								jsclick(removeBtn);
								//wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "display: none;"));
								wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none"));
								Thread.sleep(500);
								i--;
								//System.out.println(miniCartAddedItemLists.size());
							}
						}
						wait.until(ExpectedConditions.visibilityOf(mcContinueShoppingBtn));
						Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(mcContinueShoppingBtn))
						{
							pgok = true;
							break;
						}
						else
						{
							((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
							Thread.sleep(1000);
							pgok = false;
							count++;
							continue;
						}
					}
					catch(Exception e)
					{
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
						Thread.sleep(1000);
						count++;
						continue;
					}
				}while(pgok==true||count<10);
						
				if(pgok==true)
				{
					if(BNBasicfeature.isElementPresent(miniCartSignInBtn))
					{
						Fail("The Sign In to see your Shopping bag button is still displayed.");
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
					}
					else
					{
						Pass("The Continue Shopping button is displayed.");
						((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
					}
				}
				else
				{
					Fail("The Continue Shopping button is not displayed.");
				}
				/*}
				else
				{
					Fail("The user failed to signIn Successfully.");
				}*/
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1353 Issue ." + e.getMessage());
				Exception(" BRM - 1353 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
}
	
