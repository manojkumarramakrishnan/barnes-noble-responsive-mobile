package bnPageObjects;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bnConfig.BNConstants;
import bnConfig.BNXpaths;

import com.relevantcodes.extentreports.ExtentTest;

import commonFiles.BNBasicfeature;

public class BNCompleteGuestCheckoutStories extends barnesnoble.BNGuestUserCheckoutStories implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	//ExtentTest parent;
	HSSFSheet sheet;
	static boolean signedIn = false, gcLoaded = false, delOpt = false;
	static boolean shipPage = false, prdtAdded = false ;
	static int sel, currVal = 0, bgCount = 0;
	
	public BNCompleteGuestCheckoutStories(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		wait = new WebDriverWait(driver, timeout);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = ""+modalDialogg+"")
	WebElement modalDialog;
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+productListPagee+"")
	WebElement productListPage;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy(xpath = ""+pdpPageOverlayContentt+"")
	WebElement pdpPageOverlayContent;
	
	@FindBy(xpath = ""+pdpPageProductNameContainerr+"")
	WebElement pdpPageProductNameContainer;
	
	@FindBy(xpath = ""+pdpPageATBBtnn+"")
	WebElement pdpPageATBBtn;
	
	@FindBy(xpath = ""+pdpPageEGiftCardATBBtnn+"")
	WebElement pdpPageEGiftCardATBBtn;
	
	@FindBy(xpath = ""+pdpPageGiftCardATBBtnn+"")
	WebElement pdpPageGiftCardATBBtn;
	
	@FindBy(xpath = ""+pdpPageATBErrorr+"")
	WebElement pdpPageATBError;
	
	@FindBy(xpath = ""+pdpPageMiniCartflyy+"")
	WebElement pdpPageMiniCartfly;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath = ""+bagCountt+"")
	WebElement bagCount;
	
	@FindBy(xpath = ""+miniCartLoadingGaugee+"")
	WebElement miniCartLoadingGauge;
	
	@FindBy(xpath=""+mcContinueShoppingBtnn+"")
	WebElement mcContinueShoppingBtn;
	
	@FindBy(xpath = ""+checkoutPageFramee+"") 
	WebElement checkoutPageFrame;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+checkoutAsGuestbtnn+"")
	WebElement checkoutAsGuestbtn;
	
	@FindBy (xpath = ""+guestCheckoutAddPageTitlee+"")
	WebElement guestCheckoutAddPageTitle;
	
	@FindBy (xpath = ""+checkoutStepss+"")
	WebElement checkoutSteps;
	
	@FindBy(xpath = ""+countrySelectionn+"")
	WebElement countrySelection;
	
	@FindBy(xpath = ""+fNamee+"")
	WebElement fName;
	
	@FindBy(xpath = ""+fNametxtt+"")
	WebElement fNametxt;
	
	@FindBy(xpath = ""+lNamee+"")
	WebElement lName;
	
	@FindBy(xpath = ""+lNametxtt+"")
	WebElement lNametxt;
	
	@FindBy(xpath = ""+stAddresss+"")
	WebElement stAddress;
	
	@FindBy(xpath = ""+stAddresstxtt+"")
	WebElement stAddresstxt;
	
	@FindBy(xpath = ""+aptSuiteAddresss+"")
	WebElement aptSuiteAddress;
	
	@FindBy(xpath = ""+aptSuiteAddresstxtt+"")
	WebElement aptSuiteAddresstxt;
	
	@FindBy(xpath = ""+cityy+"")
	WebElement city;
	
	@FindBy(xpath = ""+citytxtt+"")
	WebElement citytxt;
	
	@FindBy(xpath = ""+statee+"")
	WebElement state;
	
	@FindBy(xpath = ""+statetxtt+"")
	WebElement statetxt;
	
	@FindBy(xpath = ""+zipCodee+"")
	WebElement zipCode;
	
	@FindBy(xpath = ""+zipcodetxtt+"")
	WebElement zipcodetxt;
	
	@FindBy(xpath = ""+contactNoo+"")
	WebElement contactNo;
	
	@FindBy(xpath = ""+contactNotxtt+"")
	WebElement contactNotxt;
	
	@FindBy(xpath = ""+companyNamee+"")
	WebElement companyName;
	
	@FindBy(xpath = ""+companyNametxtt+"")
	WebElement companyNametxt;
	
	@FindBy(xpath = ""+orderSummaryContainerr+"")
	WebElement orderSummaryContainer;
	
	@FindBy(xpath = ""+orderContinueBtnn+"")
	WebElement orderContinueBtn;
	
	@FindBy(xpath = ""+shippingCheckoutStepp+"")
	WebElement shippingCheckoutStep;
	
	@FindBy(xpath = ""+deliveryCheckoutStepp+"")
	WebElement deliveryCheckoutStep;
	
	@FindBy(xpath = ""+customerServicee+"")
	WebElement customerService;
	
	@FindBy(xpath = ""+siteCopyRightt+"")
	WebElement siteCopyRight;
	
	@FindBy(xpath = ""+errorMsgg+"")
	WebElement errorMsg;
	
	@FindBy(xpath = ""+addressVerificationMatchesTitlee+"")
	WebElement addressVerificationMatchesTitle;
	
	@FindBy(xpath = ""+addressVerificationPartialMatchesTitlee+"")
	WebElement addressVerificationPartialMatchesTitle;
	
	@FindBy(xpath = ""+addressVerificationNoMatchesTitlee+"")
	WebElement addressVerificationNoMatchesTitle;
	
	@FindBy(xpath = ""+addressNoMatchesEditBtnn+"") 
	WebElement addressNoMatchesEditBtn;
	
	@FindBy(xpath = ""+addressPartialMatchesEditBtnn+"") 
	WebElement addressPartialMatchesEditBtn;
	
	@FindBy(xpath = ""+addressMatchesEditBtnn+"") 
	WebElement addressMatchesEditBtn;
	
	@FindBy(xpath = ""+addressSuggestionFirstAddresss+"")
	WebElement addressSuggestionFirstAddress;
	
	@FindBy(xpath = ""+deliveryOptionTitlee+"")
	WebElement deliveryOptionTitle;
	
	@FindBy(xpath = ""+deliveryCartsItemContainerr+"")
	WebElement deliveryCartsItemContainer;
	
	@FindBy(xpath = ""+guestCheckoutPaymentPageTitlee+"")
	WebElement guestCheckoutPaymentPageTitle;
	
	@FindBy(xpath = ""+guestPaymentsOptionn+"")
	WebElement guestPaymentsOption;
	
	@FindBy(xpath = ""+ccNumberr+"")
	WebElement ccNumber;
	
	@FindBy(xpath = ""+ccNamee+"")
	WebElement ccName;
	
	@FindBy(xpath = ""+ccMonthh+"")
	WebElement ccMonth;
	
	@FindBy(xpath = ""+ccYearr+"")
	WebElement ccYear;
	
	@FindBy(xpath = ""+ccCsvv+"")
	WebElement ccCsv;
	
	@FindBy(xpath = ""+ccEmaill+"")
	WebElement ccEmail;
	
	@FindBy(xpath = ""+reviewPagetitlee+"")
	WebElement reviewPagetitle;
	
	@FindBy(xpath = ""+paymentCheckoutStepp+"")
	WebElement paymentCheckoutStep;
	
	//@FindBy(xpath = "//*[@id='payWPPLink']")
	@FindBy(xpath = ""+otherPayOptionNotActivee+"")
	WebElement otherPayOptionNotActive;
	
	@FindBy(xpath = ""+guestPaypalActiveTabb+"")
	WebElement guestPaypalActiveTab;
	
	@FindBy(xpath = ""+guestPaypalButtonn+"")
	WebElement guestPaypalButton;
	
	@FindBy(xpath = ""+deliveryCartItemsContainerr+"")
	WebElement deliveryCartItemsContainer;
	
	@FindBy(xpath = ""+deliveryOptionShipMethodss+"")
	WebElement deliveryOptionShipMethods;
	
	//@FindBy(xpath = "//*[@class='sk_prdtType']")
	@FindBy(xpath = ""+pdpPageProductDropDownn+"")
	WebElement pdpPageProductDropDown;
	
	@FindBy(xpath = ""+pdpPageGiftCardValueFieldd+"")
	WebElement pdpPageGiftCardValueField;
	
	@FindBy(xpath = ""+pdpPageGiftCardNoOfCardsDDD+"")
	WebElement pdpPageGiftCardNoOfCardsDD;
	
	@FindBy(xpath = ""+pdpPageEGiftCardReceipentFieldd+"")
	WebElement pdpPageEGiftCardReceipentField;
	
	@FindBy(xpath = ""+pdpPageEGiftCardReceipentFielddd+"")
	WebElement pdpPageEGiftCardConfirmEmailField;
	
	@FindBy(xpath = ""+pdpPageGiftCardToo+"")
	WebElement pdpPageGiftCardTo;
	
	@FindBy(xpath = ""+pdpPageGiftCardFromm+"")
	WebElement pdpPageGiftCardFrom;
	
	@FindBy(xpath = ""+deliveryPreferenceTitlee+"")
	WebElement deliveryPreferenceTitle;
	
	@FindBy(xpath = ""+deliveryOptionDeliverySloww+"")
	WebElement deliveryOptionDeliverySlow;
	
	@FindBy(xpath = ""+deliveryOptionCartItemContainerr+"")
	WebElement deliveryOptionCartItemContainer;
	
	@FindBy(xpath = ""+deliveryOptionCartTotalPricee+"")
	WebElement deliveryOptionCartTotalPrice;
	
	@FindBy(xpath = ""+couponContainerr+"")
	WebElement couponContainer;
	
	@FindBy(xpath = ""+couponContainerTitlee+"")
	WebElement couponContainerTitle;
	
	@FindBy(xpath = ""+gftCardImgg+"")
	WebElement gftCardImg;
	
	@FindBy(xpath = ""+gftCardTitt+"")
	WebElement gftCardTit;
	
	//@FindBy(how=How.XPATH,using="//*[@id='frmApplyGC']//*[@class='rs-tb giftCardNumber']")
	@FindBy(xpath = ""+couponCreditCardd+"")
	WebElement couponCreditCard;
			
	@FindBy(xpath = ""+couponCreditCardPinn+"")
	WebElement couponCreditCardPin;
		
	@FindBy(xpath = ""+couponCreditCardSubmitt+"")
	WebElement couponCreditCardSubmit;
	
	@FindBy(xpath = ""+couponCodeApplyContainerr+"")
	WebElement couponCodeApplyContainer;
	
	@FindBy(xpath = ""+couponCodeApplyButtonn+"")
	WebElement couponCodeApplyButton;
	
	@FindBy(xpath = ""+bookfairApplyContainerr+"")
	WebElement bookfairApplyContainer;
	
	@FindBy(xpath = ""+bookfairApplyButtonn+"")
	WebElement bookfairApplyButton;
	
	@FindBy(xpath = ""+giftCardsOpenTriggerr+"")
	WebElement giftCardsOpenTrigger;
	
	@FindBy(xpath = ""+couponGiftCardOpenn+"")
	WebElement couponGiftCardOpen;
	
	@FindBy(xpath = ""+couponCreditCardErrorr+"")
	WebElement couponCreditCardError;
	
	//@FindBy(how=How.XPATH,using="//*[@id='frmApplyGC']//*[contains(@class,'user-error')]")
	@FindBy(xpath = ""+couponCreditCardTextBoxx+"")
	WebElement couponCreditCardTextBox;
	
	@FindBy(xpath = ""+couponCreditCardPinTextBoxx+"")
	WebElement couponCreditCardPinTextBox;
	
	@FindBy(xpath = ""+couponCouponOpenTriggerr+"")
	WebElement couponCouponOpenTrigger;
	
	@FindBy(xpath = ""+couponCodeDefaultTextt+"")
	WebElement couponCodeDefaultText;
	
	@FindBy(xpath = ""+couponCodeTextFieldd+"")
	WebElement couponCodeTextField;
	
	@FindBy(xpath = ""+couponCodeTextFieldActivee+"")
	WebElement couponCodeTextFieldActive;
	
	@FindBy(how=How.XPATH,using=""+couponCouponErrorr+"")
	WebElement couponCouponError;
	
	@FindBy(how=How.XPATH,using=""+bookFairOpenTriggerr+"")
	WebElement bookFairOpenTrigger;
	
	@FindBy(xpath = ""+bookFairDefaultTextt+"")
	WebElement bookFairDefaultText;
	
	@FindBy(xpath = ""+bookFairTextFieldd+"")
	WebElement bookFairTextField;
	
	@FindBy(how=How.XPATH,using=""+bookfairApplyFieldd+"")
	WebElement bookfairApplyField;
	
	@FindBy(xpath = ""+couponBookFairErrorr+"")
	WebElement couponBookFairError;
	
	@FindBy(xpath = ""+guestPaymentTabb+"")
	WebElement guestPaymentTab;
	
	@FindBy(xpath = ""+guestCCPaymentTabb+"")
	WebElement guestCCPaymentTab;
	
	@FindBy(xpath = ""+guestPaypalNotActiveTabb+"")
	WebElement guestPaypalNotActiveTab;
	
	@FindBy(xpath = ""+guestCCPaymentNotActiveTabb+"")
	WebElement guestCCPaymentNotActiveTab;
	
	@FindBy(xpath = ""+guestCCContainerActiveTabb+"") // CC Container Display
	WebElement guestCCContainerActiveTab;
	
	@FindBy(xpath = ""+guestPaypalContainerActiveTabb+"") // CC Container Display
	WebElement guestPaypalContainerActiveTab;
	
	@FindBy(xpath = ""+ccLogoContainerr+"")
	WebElement ccLogoContainer;
	
	@FindBy(xpath = ""+cciiconn+"")
	WebElement cciicon;
	
	@FindBy(xpath = ""+guestSameAsShippingChkboxx+"")
	//@FindBy(xpath = "//*[@class='styled-checkbox']")
	WebElement guestSameAsShippingChkbox;
	
	@FindBy(xpath = ""+guestEmailAddresss+"")
	WebElement guestEmailAddress;
	
	@FindBy(xpath = ""+guestErrorr+"")
	WebElement guestError;
	
	//@FindBy(xpath = "//*[@class='expand-cvv-container modal-cvv']")
	@FindBy(xpath = ""+cciicontooltipp+"")
	WebElement cciicontooltip;
	
	@FindBy(xpath = ""+cciicontooltipMaskk+"")
	WebElement ccicontooltipMask;
	
	@FindBy(xpath = ""+guestSameAsShippingChkboxContainerr+"")
	WebElement guestSameAsShippingChkboxContainer;
	
	@FindBy(xpath = ""+guestSameAsShippingChkboxLabell+"")
	WebElement guestSameAsShippingChkboxLabel;
	
	@FindBy(xpath = ""+guestSameAsShippingContainerr+"")
	WebElement guestSameAsShippingContainer;
	
	@FindBy(xpath = ""+couponContainerCouponCodeTitlee+"")
	WebElement couponContainerCouponCodeTitle;
	
	@FindBy(xpath = ""+couponContainerBookFairTitlee+"")
	WebElement couponContainerBookFairTitle;
	
	@FindBy(xpath = ""+couponGiftCardsActivee+"")
	WebElement couponGiftCardsActive;
	
	@FindBy(xpath =""+couponCreditCardLabell+"")
	WebElement couponCreditCardLabel;
	
	@FindBy(xpath =""+couponCreditCardPINLabell+"")
	WebElement couponCreditCardPINLabel;
	
	@FindBy(xpath = ""+productIdd+"")
	WebElement productId;
	
	@FindBy(xpath = ""+guestreviewHeaderr+"")
	WebElement guestreviewHeader;
	
	@FindBy(xpath = ""+ccAddDefaultMonthTxtt+"")
	WebElement ccAddDefaultMonthTxt;
	
	@FindBy(xpath = ""+ccAddDefaultMonthTxtt1+"")
	WebElement ccAddDefaultMonthTxt1;
	
	@FindBy(xpath = ""+ccAddDefaultYearTxtt+"")
	WebElement ccAddDefaultYearTxt;
	
	@FindBy(xpath = ""+deliveryOptionContainerr+"")
	WebElement deliveryOptionContainer;
	
	@FindBy(xpath = ""+deliverySpeedTitlee+"")
	WebElement deliverySpeedTitle;
	
	@FindBy(xpath = ""+productAddedSuccessIdd+"")
	WebElement productAddedSuccessId;
	
	@FindBy(xpath = ""+miniCartShoppingBagMaskIdd+"")
	WebElement miniCartShoppingBagMaskId;
	
	@FindBy(xpath = ""+prdtshortDescc+"")
	WebElement prdtshortDesc;
	
	@FindBy(xpath = ""+pdpPageProductNamee+"")
	WebElement pdpPageProductName;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	// LIST WEBELEMENTS
	
	@FindBy(how = How.XPATH, using = ""+mcContinueToCheckOutBtnn+"")
	List<WebElement> mcContinueToCheckOutBtn;
	
	@FindBy(how = How.XPATH, using = ""+searchPageProductListss+"")
	List<WebElement> searchPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+checkoutStepsListss+"")
	List<WebElement> checkoutStepsLists;
	
	@FindBy(how = How.XPATH, using = ""+copyrightContainerr+"")
	List<WebElement> copyrightContainer;
	
	@FindBy(how=How.XPATH, using = ""+addressVerficationPossibleSuggestionn+"")
	List<WebElement> addressVerficationPossibleSuggestion;
	
	@FindBy(how = How.XPATH,using = ""+orderSummaryListss+"")
	List<WebElement> orderSummaryLists;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionMakeitasGiftt+"")
	List<WebElement> deliveryOptionMakeitasGift;
	
	@FindBy(how = How.XPATH,using = ""+makeItAGiftTitlee+"")
	List<WebElement> makeItAGiftTitle;
	
	@FindBy(how  = How.XPATH,using = ""+deliveryOptionProductEditt+"")
	List<WebElement> deliveryOptionProductEdit;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionDeliveryPreferencee+"")
	List<WebElement> deliveryOptionDeliveryPreference;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionDeliverySelectt+"")
	List<WebElement> deliveryOptionDeliverySelect;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionShipMethodsLabell+"")
	List<WebElement> deliveryOptionShipMethodsLabel;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionDeliveryMethodd+"")
	List<WebElement> deliveryOptionDeliveryMethod;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionShippingSelectt+"")
	List<WebElement> deliveryOptionShippingSelect;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemsImagee+"")
	List<WebElement> deliveryOptionCartItemsImage;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemsProductTitlee+"")
	List<WebElement> deliveryOptionCartItemsProductTitle;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemsProductAuthorr+"")
	List<WebElement> deliveryOptionCartItemsProductAuthor;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemsProductPricee+"")
	List<WebElement> deliveryOptionCartItemsProductPrice;
	
	@FindBy(how = How.XPATH,using = ""+deliveryOptionCartItemsProductQtyy+"")
	List<WebElement> deliveryOptionCartItemsProductQty;
	
	@FindBy(how = How.XPATH,using = ""+loyaltyContainerListt+"")
	List<WebElement> loyaltyContainerList;
	
	//@FindBy(xpath = "//*[@class='cardlogos cardPayField']")
	@FindBy(how=How.XPATH,using = ""+ccLogosListt+"")
	List<WebElement> ccLogosList;
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	//Clear all the fields
	public void clearAll()
	{
		fName.clear();
		lName.clear();
		BNBasicfeature.scrolldown(stAddress, driver);
		stAddress.clear();
		aptSuiteAddress.clear();
		city.clear();
		zipCode.clear();
		contactNo.clear();
		companyName.clear();
		BNBasicfeature.scrollup(fName, driver);
	}
	
	// Check if Attrubute is present or not
	private boolean isAttribtuePresent(WebElement element, String attribute) 
	{
	    Boolean result = false;
	    try 
	    {
	        boolean value = element.getAttribute(attribute).isEmpty();
	        if (value==true)
	        {
	            //result = false;
	        	result = true;
	        }
	        else if(element.getAttribute("style").contains("block"))
	        {
	        	result = false;
	        }
	        else if(element.getAttribute("style").contains("none"))
	        {
	        	result = false;
	        }
	    } 
	    catch (Exception e) 
	    {
	    	result = false;
	    }
	    return result;
	}
	
	// Navigate to URL
	public void navigatetoURL(ExtentTest Child) throws IOException, AWTException
	{
		try
		{
			driver.get(BNConstants.mobileSiteURL);
			log.add("The browser is launched and the user is navigated to the URL");
			//BNBasicfeature.signIn();
			wait = new WebDriverWait(driver, 30);
		}
		catch(Exception e)
		{
			Exception("There is something wrong . Please Check." + e.getMessage());
		
			System.out.println(e.getMessage());
		}
	}
	
	// Fields Empty Value
	public void chkfieldvalues()
	{
		if(BNBasicfeature.isElementPresent(fName))
		{
			if(fName.getAttribute("value").isEmpty())
			{
				log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
				Fail("The First name field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the First Name field is : " + fName.getAttribute("value").toString());
				Pass("The First Name field is displayed.",log);
			}
		}
		else
		{
			Fail("There is something wrong. First Name field is not displayed." );
		}
		
		if(BNBasicfeature.isElementPresent(lName))
		{
			if(lName.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
				Fail("The Last name field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Last Name field is : " + lName.getAttribute("value").toString());
				Pass("The Last Name field is displayed and there is value present.",log);
			}
		}
		else
		{
			Fail("There is something wrong. Last Name field is not displayed." );
		}
		BNBasicfeature.scrolldown(stAddress, driver);
		
		if(BNBasicfeature.isElementPresent(stAddress))
		{
			if(stAddress.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				Fail("The Street Address field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Street Address field is : " + stAddress.getAttribute("value").toString());
				Pass("The Street Address field is displayed and there is value present in it.",log);
			}
		}
		else
		{
			Fail("There is something wrong. Street Address field is not displayed." );
		}
		
		if(BNBasicfeature.isElementPresent(city))
		{
			if(city.getAttribute("value").isEmpty())
			{
				log.add("The Value in the City field is : " + city.getAttribute("value").toString());
				Fail("The City field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the City field is : " + city.getAttribute("value").toString());
				Pass("The City field is displayed and there is value in it.",log);
			}
		}
		else
		{
			Fail("There is something wrong. City Name field is not displayed." );
		}
		
		if(BNBasicfeature.isElementPresent(zipCode))
		{
			if(zipCode.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				Fail("The Zipcode field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
				Pass("The Zipcode field is displayed and there is value in it.",log);
			}
		}
		else
		{
			Fail("There is something wrong. Zipcode field is not displayed." );
		}
		
		if(BNBasicfeature.isElementPresent(state))
		{
			Select sel = new Select(state);
			if(sel.getFirstSelectedOption().getText()!=null)
			{
				log.add("The Selected Value in the State field is : " + sel.getFirstSelectedOption().getText());
				Pass("The State field is not empty and the State is selected.",log);
			}
			else
			{
				log.add("The Selected Value in the State field is : " + sel.getFirstSelectedOption().getText());
				Fail("The State is not selected and the value is empty.",log);
			}
		}
		else
		{
			Fail("There is something wrong. State drop down is not displayed." );
		}
		
		if(BNBasicfeature.isElementPresent(contactNo))
		{
			if(contactNo.getAttribute("value").isEmpty())
			{
				log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				Fail("The Contact Number field is displayed and the value is empty.",log);
			}
			else
			{
				log.add("The Value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
				Pass("The Contact Number field is displayed and there is value present in it.",log);
			}
		}
		else
		{
			Fail("There is something wrong. Contact Number field is not displayed." );
		}
	}
	
	// Color Finder	
	public String colorfinder(WebElement element)
	{
		String ColorName = element.getCssValue("background-color");
		Color colorhxcnvt = Color.fromString(ColorName);
		String hexCode = colorhxcnvt.asHex();
		return hexCode;
	}
	
	// Payments Page Field Check
	public void paymentPageFieldCheck(String title)
	{
		String couponTitle = title;
		try
		{
			Pass("The User is navigated to the Payments Page.");
			if(BNBasicfeature.isElementPresent(guestPaymentTab))
			{
				Pass("The Guest Payment Tab is displayed.");
				if(BNBasicfeature.isElementPresent(guestCCPaymentTab))
				{
					Pass("The Guest Credit Card Payment Tab is displayed.");
					if(BNBasicfeature.isElementPresent(guestCCContainerActiveTab))
					{
						String active = guestCCContainerActiveTab.getAttribute("style");
						if(active.contains("display: block;"))
						{
							Pass("The Credit Card Container is displayed by default.");
							
							if(BNBasicfeature.isElementPresent(ccLogoContainer))
							{
								Pass("The Credit Card Container logo is displayed.");
								if(BNBasicfeature.isListElementPresent(ccLogosList))
								{
									for(WebElement logo : ccLogosList)
									{
										if(BNBasicfeature.isElementPresent(logo))
										{
											Pass("The Credit Card Logo is displayed.",log);
										}
										else
										{
											Fail(" The Credit Card logo is not displayed.");
										}
									}
								}
								else
								{
									Fail("The Credit Card Logo List is not displayed.");
								}
							}
							else
							{
								Fail("The Credit Card Container logo is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(ccNumber))
							{
								Pass("The Credit Card Number is displayed.");
							}
							else
							{
								Fail("The Credit Card Number is not displayed.");
							}
								
							if(BNBasicfeature.isElementPresent(ccName))
							{
								Pass("The Credit Card Name is displayed.");
							}
							else
							{
								Fail("The Credit Card Name is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(ccMonth))
							{
								Pass("The Credit Card Month drop down is displayed.");
							}
							else
							{
								Fail("The Credit Card Month drop down is not displayed.");
							}
								
							if(BNBasicfeature.isElementPresent(ccYear))
							{
								Pass("The Credit Card Year drop down field is displayed.");
							}
							else
							{
								Fail("The Credit Card Year drop down field is not displayed.");
							}
								
							if(BNBasicfeature.isElementPresent(ccCsv))
							{
								Pass("The Credit Card Security Field CSV is displayed.");
							}
							else
							{
								Fail("The Credit Card Security Field CSV field is not displayed.");
							}
								
							if(BNBasicfeature.isElementPresent(cciicon))
							{
								Pass("The Tooltip icon is dispalyed.");
							}
							else
							{
								Fail("The Tooltip icon is not dispalyed.");
							}
							
							if(BNBasicfeature.isElementPresent(guestSameAsShippingChkbox))
							{
								Pass("The Same As Shipping Address checkbox is dispalyed.");
							}
							else
							{
								Fail("The Same As Shipping Address checkbox is not dispalyed.");
							}
								
							if(BNBasicfeature.isElementPresent(guestEmailAddress))
							{
								Pass("The Email Address Field is dispalyed.");
							}
							else
							{
								Fail("The Email Address Field is not dispalyed.");
							}
						}
						else 
						{
							Fail("The Credit Card Container is not displayed by default.");
						}
					}
					else
					{
						Fail("The Credit Card Container is displayed.");
					}
				}
				else
				{
					Fail("The Guest Credit Card payment tab is not displayed.");
				}
					
				if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
				{
					Pass("The Guest Paypal Payment Tab is displayed.");
					jsclick(guestPaypalNotActiveTab);
					wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(guestCCContainerActiveTab))
					{
						boolean active = isAttribtuePresent(guestPaypalContainerActiveTab, "style");
						if(active==true)
						{
							Pass("The Gusest other payment Tab is active");
						}
						else
						{
							Fail("The Gusest other payment Tab is not active");
						}
						
						if(BNBasicfeature.isElementPresent(guestPaypalButton))
						{
							Pass("The Paypal button is displayed.");
							if(BNBasicfeature.isElementPresent(guestCCPaymentNotActiveTab))
							{
								jsclick(guestCCPaymentNotActiveTab);
								Thread.sleep(250);
							}
						}
						else
						{
							Fail("The Paypal button is not displayed.");
							if(BNBasicfeature.isElementPresent(guestCCPaymentNotActiveTab))
							{
								guestCCPaymentNotActiveTab.click();
								Thread.sleep(250);
							}
						}
					}
					else
					{
						Fail("The Gusest other payment Tab is not active");
					}
				}
				else
				{
					Fail("The Guest Paypal payment tab is not displayed.");
				}
			}
			else
			{
				Fail("The Guest Payment Tab is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(couponContainer))
			{
				BNBasicfeature.scrolldown(couponContainer, driver);
				Pass("The Coupon Container is found.");
				if(couponContainerTitle.getText().equals(couponTitle))
				{
					Pass("The title matches the expected content .");
				}
				else
				{
					Fail("There is mismatch in the Title.");
				}
			}
			else
			{
				Fail("The Coupon Container is not found.");
			}
		}
		catch(Exception e)
		{
			Exception("There is something wrong . Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	// Load the Mini Cart Feature
	public boolean loadMiniCart() throws Exception
	{
		boolean pgok = false;
		int count = 1;
		do
		{
			Actions act = new Actions(driver);
			act.moveToElement(bagIcon).click().build().perform();
			//Thread.sleep(1000);
			try
			{
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "block;"));
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "none;"));
				//wait.until(ExpectedConditions.visibilityOf(mcContinueToCheckOutBtn));
				Thread.sleep(1000);
				if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn.get(0)))
				{
					pgok = true;
					break;
				}
				else if((BNBasicfeature.isElementPresent(mcContinueShoppingBtn)))
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					//jsclick(bagIcon);
					pgok = false;
					count++;
					continue;
				}
				else
				{
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
					driver.navigate().refresh();
					Thread.sleep(1000);
					//jsclick(bagIcon);
					pgok = false;
					count++;
					continue;
				}
			}
			catch(Exception e)
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", miniCartShoppingBagMaskId);
				driver.navigate().refresh();
				Thread.sleep(1000);
				count++;
				continue;
			}
		}
		while(pgok==true||count<10);
		return pgok;
	}
	
	/* Search EAN Numbers */
	public boolean searchEanNumbers(String ean) throws Exception
	{
		boolean pdpPageloaded = false;
		try
		{
			do
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(header));
					//BNBasicfeature.scrollup(header, driver);
					if(BNBasicfeature.isElementPresent(searchIcon))
					{
						Actions act = new Actions(driver);
						jsclick(searchIcon);
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
						act.moveToElement(searchContainer).click().sendKeys(ean).sendKeys(Keys.ENTER).build().perform();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(homepageHeader));
						//driver.manage().deleteAllCookies();
						//driver.navigate().refresh();
						if(BNBasicfeature.isElementPresent(plpNoProductsFound))
						{
							pdpPageloaded = false;
							continue;
						}
						else
						{
							wait.until(ExpectedConditions.visibilityOf(productId));
							wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
							if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
							{
								pdpPageloaded = true;
								//Thread.sleep(500);
								break;
							}
							else if(BNBasicfeature.isElementPresent(pdpPageEGiftCardATBBtn))
							{
								pdpPageloaded = true;
								//Thread.sleep(500);
								break;
							}
							else if(BNBasicfeature.isElementPresent(pdpPageGiftCardATBBtn))
							{
								pdpPageloaded = true;
								//Thread.sleep(500);
								break;
							}
							else if(BNBasicfeature.isElementPresent(plpNoProductsFound))
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
							else
							{
								pdpPageloaded = false;
								//Thread.sleep(500);
								continue;
							}
						}
					}
					else
					{
						System.out.println("The Search Icon is not found.Please Check.");
					}
				}
				catch(Exception e)
				{
					System.out.println("In Catch Block 1");
					System.out.println(e.getMessage());
					//driver.navigate().refresh();
				}
			}while(pdpPageloaded==true);
		}
		catch (Exception e) 
		{
			System.out.println("In Outer Catch Block 2");
			System.out.println(e.getMessage());
		}
		return pdpPageloaded;
	}
	
	
	// Add to Bag
	public void clickaddtoBag() throws InterruptedException
	{
		BNBasicfeature.scrolldown(prdtshortDesc, driver);
		jsclick(pdpPageATBBtn);
		boolean prdadded = false;
		int count = 1;
		Thread.sleep(3000);
		//BNBasicfeature.scrollup(promoCarousel, driver);
		BNBasicfeature.scrollup(bagIcon, driver);
		do
		{
			if(BNBasicfeature.isElementPresent(productAddedSuccessId))
			{
				prdadded = true;
				break;
			}
			else
			{
				count++;
				jsclick(pdpPageATBBtn);
				Thread.sleep(3000);
				prdadded = false;
				continue;
			}
		}while(count<=5 || prdadded == true);
	}
	
	// Get Bag Count Value
	public boolean getBagCount()
	{
		boolean bagCountOk = false;
		int count = 1;
		int refCount = 1;
		do
		{
			for( int i = 0; i<100; i++)
			{
				try
				{
					wait.until(ExpectedConditions.visibilityOf(bagCount));
					wait.until(ExpectedConditions.elementToBeClickable(bagCount));
					//String bagCnt = bagCount.getText();
					String bagCnt = bagCount.getText().replace("+", "");
					if(bagCnt.isEmpty())
					{
						count++;
						continue;
					}
					else
					{
						bgCount = Integer.parseInt(bagCnt);
						bagCountOk = true;
						break;
					}
				}
				catch(Exception e)
				{
					count++;
				}
			}
			if(count>=100)
			{
				driver.navigate().refresh();
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(bagCount));
				count = 1;
				refCount++;
			}
		}while((bagCountOk==false)&&(refCount<=3));
		return bagCountOk;
	}
		
	// Add to Bag
	public boolean clickaddtoBag(int prevVal, WebElement ATBBtn) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			prdadded = false;
			int count = 1;
			//Thread.sleep(1000);
			do
			{
				WebDriverWait newWait = new WebDriverWait(driver, 5);
				try
				{
					boolean bgCnt = getBagCount();
					if(bgCnt==true)
					{
						newWait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(500);
						currVal = Integer.parseInt(bagCount.getText());
						if(currVal>prevVal)
						{
							prdadded = true;
							//break;
						}
						
						jsclick(ATBBtn);
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
						//BNBasicfeature.scrollup(promoCarousel, driver);
						String error = pdpPageATBError.getAttribute("style");
						if(error.contains("block"))
						{
							driver.navigate().refresh();
							wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOf(bagCount)
									)
								);
							wait.until(ExpectedConditions.visibilityOf(header));
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
						}
						
						if(BNBasicfeature.isElementPresent(productAddedSuccessId))
						{
							bgCnt = getBagCount();
							if(bgCnt==true)
							{
								wait.until(ExpectedConditions.visibilityOf(bagCount));
								Thread.sleep(100);
								currVal = Integer.parseInt(bagCount.getText());
							}
							else
							{
								continue;
							}
							
							if(currVal>prevVal)
							{
								prdadded = true;
								//break;
							}
							else
							{
								Thread.sleep(1000);
								jsclick(ATBBtn);
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
								newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
								count++;
								continue;
							}
						}
						else
						{
							count++;
							jsclick(ATBBtn);
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
							continue;
						}
					}
					else
					{
						continue;
					}
				}
				catch(Exception e)
				{
					driver.navigate().refresh();
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(bagCount)
							)
						);
					wait.until(ExpectedConditions.visibilityOf(header));
					
					boolean bgCnt1 = getBagCount();
					if(bgCnt1==true)
					{
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(100);
						currVal = Integer.parseInt(bagCount.getText());
					}
					else
					{
						continue;
					}
					
					if(currVal>prevVal)
					{
						prdadded = true;
						//break;
					}
				}
			}while((prdadded==false)&&(count <= 5));
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}
	
	/*// Add to Bag
	public boolean clickaddtoBag(int prevVal, WebElement ATBBtn) throws InterruptedException
	{
		boolean prdadded = false;
		try
		{
			prdadded = false;
			int count = 1;
			//Thread.sleep(1000);
			do
			{
				WebDriverWait newWait = new WebDriverWait(driver, 5);
				try
				{
					try
					{
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(500);
						currVal = Integer.parseInt(bagCount.getText());
					}
					catch(Exception e)
					{
						currVal  = 0;
					}
					if(currVal>prevVal)
					{
						prdadded = true;
						break;
					}
					
					jsclick(ATBBtn);
					newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
					newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
					//BNBasicfeature.scrollup(promoCarousel, driver);
					String error = pdpPageATBError.getAttribute("style");
					if(error.contains("block"))
					{
						driver.navigate().refresh();
						try
						{
							wait.until(ExpectedConditions.visibilityOf(bagCount));
							currVal = Integer.parseInt(bagCount.getText());
						}
						catch(Exception e)
						{
							currVal  = 0;
						}
						if(currVal>prevVal)
						{
							prdadded = true;
							break;
						}
					}
					
					try
					{
						Thread.sleep(200);
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						currVal = Integer.parseInt(bagCount.getText());
					}
					catch(Exception e)
					{
						currVal = Integer.parseInt(bagCount.getText());
					}
					
					if(BNBasicfeature.isElementPresent(productAddedSuccessId))
					{
						if(currVal>prevVal)
						{
							prdadded = true;
							break;
						}
						else
						{
							Thread.sleep(1000);
							jsclick(ATBBtn);
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
							newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
							count++;
							continue;
						}
					}
					else
					{
						count++;
						jsclick(ATBBtn);
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "block"));
						newWait.until(ExpectedConditions.attributeContains(pdpPageMiniCartfly, "style", "none"));
						continue;
					}
				}
				catch(Exception e)
				{
					driver.navigate().refresh();
					try
					{
						wait.until(ExpectedConditions.visibilityOf(bagCount));
						Thread.sleep(500);
						currVal = Integer.parseInt(bagCount.getText());
					}
					catch(Exception e1)
					{
						currVal  = 0;
					}
					if(currVal>prevVal)
					{
						prdadded = true;
						break;
					}
				}
			}while(prdadded==false|| count <= 5);
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		return prdadded;
	}*/
			
	/*// Check for Bag Count
	public boolean bagCount(boolean val) throws Exception
	{
		boolean prdtadded = false;
		int prdtval = Integer.parseInt(bagCount.getText());
		//System.out.println(prdtval);
		int count = 1;
		boolean prdtAdded = clickaddtoBag(prdtval);
		do
		{
			int currVal= Integer.parseInt(bagCount.getText());
			//System.out.println(currVal);
			if(prdtAdded==true)
			{
				if(currVal>prdtval)
				{
					prdtadded=true;
					//bagIcon.click();
					break;
				}
				else
				{
					count++;
					clickaddtoBag(prdtval);
					continue;
				}
			}
			else
			{
				count++;
				clickaddtoBag(prdtval);
				continue;
			}
		}while(count<3||prdtadded==true);
		return prdtadded;
	}*/
	
	/*// Check for Bag Count
	public boolean bagCount(boolean val) throws Exception
	{
		boolean prdtadded = false;
		int count = 1;
		clickaddtoBag();
		do
		{
			int prdtval = Integer.parseInt(prdtBagCount.getText());
			if(prdtval>0)
			{
				prdtadded=true;
				bagIcon.click();
				//wait.until(ExpectedConditions.attributeToBe(miniCartLoadingGauge, "style", "display: none;"));
				wait.until(ExpectedConditions.attributeContains(miniCartLoadingGauge, "style", "display: none;"));
				wait.until(ExpectedConditions.visibilityOf(secureCheckOutBtn));
				break;
			}
			else
			{
				count++;
				prdtadded=false;
				clickaddtoBag();
				continue;
			}
		}while(prdtadded==true||count<10);
		return prdtadded;
	}*/
	
	// Compare the Bottom Container Fields
	public void valcompare(String[] det, String[] val, String subtotallabelname, String subtotalvalue, String estimatedShiplabel, String estimatedshipvalue, String estimatedTaxlabel, String estimatedTaxvalue, String OrderTotallabel, String OrderTotalvalue)
	{
		if(det[0].equalsIgnoreCase(subtotallabelname))
		{
			log.add("The Subtotal Value and it is dispalyed.");
			log.add("The Expected content was : " + subtotallabelname);
			log.add("The Actual content was : " + det[0]);
			/*System.out.println(subtotallabelname);
			System.out.println(det[0]);*/
		
		}
		else
		{
			log.add("The Subtotal Value and it is dispalyed.");
			log.add("The Expected content was : " + subtotallabelname);
			log.add("The Actual content was : " + det[0]);
			/*System.out.println(subtotallabelname);
			System.out.println(det[0]);*/
		
		}
		
		if(val[0].contains(subtotalvalue))
		{
			log.add("The Subtotal Amount is dispalyed.");
			log.add("The Expected content was : " + subtotalvalue);
			log.add("The Actual content was : " + val[0]);
			/*System.out.println(subtotalvalue);
			System.out.println(val[0]);*/
		
		}
		else
		{
			log.add("The Subtotal Value and Line is dispalyed.");
			log.add("The Expected content was : " + subtotalvalue);
			log.add("The Actual content was : " + val[0]);
			/*System.out.println(subtotalvalue);
			System.out.println(val[0]);*/
		
		}
		
		if(det[1].equalsIgnoreCase(estimatedShiplabel))
		{
			log.add("The Estimated Ship Label is dispalyed.");
			log.add("The Expected content was : " + estimatedShiplabel);
			log.add("The Actual content was : " + det[1]);
			/*System.out.println(estimatedShiplabel);
			System.out.println(det[1]);*/
		
		}
		else
		{
			log.add("The Estimated Ship Label is dispalyed.");
			log.add("The Expected content was : " + estimatedShiplabel);
			log.add("The Actual content was : " + det[1]);
			/*System.out.println(estimatedShiplabel);
			System.out.println(det[1]);*/
		
		}
		
		if(val[1].equalsIgnoreCase(estimatedshipvalue))
		{
			log.add("The Estimated Ship value is dispalyed.");
			log.add("The Expected content was : " + estimatedshipvalue);
			log.add("The Actual content was : " + val[1]);
		
		}
		else
		{
			log.add("The Estimated Ship value is dispalyed.");
			log.add("The Expected content was : " + estimatedshipvalue);
			log.add("The Actual content was : " + val[1]);
		
		}
		
		if(det[2].equalsIgnoreCase(estimatedTaxlabel))
		{
			log.add("The Estimated Tax is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxlabel);
			log.add("The Actual content was : " + det[2]);
		
		}
		else
		{
			log.add("The Estimated Tax is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxlabel);
			log.add("The Actual content was : " + det[2]);
		}
		
		if(val[2].contains(estimatedTaxvalue))
		{
			log.add("The Estimated Tax value is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxvalue);
			log.add("The Actual content was : " + val[2]);
		}
		else
		{
			log.add("The Estimated Tax value is dispalyed.");
			log.add("The Expected content was : " + estimatedTaxvalue);
			log.add("The Actual content was : " + val[2]);
		}
		
		if(det[3].equalsIgnoreCase(OrderTotallabel))
		{
			log.add("The Order Total is dispalyed.");
			log.add("The Expected content was : " + OrderTotallabel);
			log.add("The Actual content was : " + det[3]);
		}
		else
		{
			log.add("The Order Total is dispalyed.");
			log.add("The Expected content was : " + OrderTotallabel);
			log.add("The Actual content was : " + det[3]);
		}
		
		if(val[3].contains(OrderTotalvalue))
		{
			log.add("The Order Total value is dispalyed.");
			log.add("The Expected content was : " + OrderTotalvalue);
			log.add("The Actual content was : " + val[3]);
		
		}
		else
		{
			log.add("The Order Total value is dispalyed.");
			log.add("The Expected content was : " + OrderTotalvalue);
			log.add("The Actual content was : " + val[3]);
		
		}
	}
	
	/* Add a Product to the Bag.*/
	/*public void addProduct(HSSFSheet sheet, boolean val) throws Exception
	{
		int brmkey = sheet.getLastRowNum();
		for(int i = 0; i <= brmkey; i++)
		{
			String tcid = "BRMProductSearch";
			String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
			if(cellCont.equals(tcid))
			{
				String searchvalue = sheet.getRow(i).getCell(2).getStringCellValue();
				String[] searchcont = searchvalue.split("\n"); 
				try
				{
					wait.until(ExpectedConditions.visibilityOf(bagIcon));
					if(BNBasicfeature.isElementPresent(searchIcon))
					{
						Actions act = new Actions(driver);
						jsclick(searchIcon);
						wait.until(ExpectedConditions.visibilityOf(searchContainer));
						act.moveToElement(searchContainer).sendKeys(searchcont[0]).build().perform();
						Thread.sleep(2000);
						//wait.until(ExpectedConditions.visibilityOf(searchresults));
						act.sendKeys(Keys.ENTER).build().perform();
						wait.until(ExpectedConditions.visibilityOf(productListPage));
						boolean atbfound = false;
						if(BNBasicfeature.isElementPresent(productListPage))
						{
							if(BNBasicfeature.isListElementPresent(plpPageProductLists))
							{
								for(int j = 1; j<=plpPageProductLists.size();j++)
								{
									Thread.sleep(1000);
									int count=1;
									atbfound = false;
									do
									{
										Random r = new Random();
										int sel = r.nextInt(plpPageProductLists.size());
										if(sel>=0)
										{
											sel = 2;
										}
										
										BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
										WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
										//String prdtname = prdtlist.getText();
										atbfound = false;
										prdtlist.click();
										wait.until(ExpectedConditions.visibilityOf(pdpPageProductNameContainer));
										if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
										{
											atbfound = true;
											break;
										}
										else
										{
											driver.navigate().back();
											count++;
											continue;
										}
									} while(atbfound==true||count<10);
								}
								if(atbfound == true)
								{
									wait.until(ExpectedConditions.visibilityOf(pdpPageOverlayContent));
									Thread.sleep(1000);
									BNBasicfeature.scrolldown(prdtshortDesc, driver);
									if(BNBasicfeature.isElementPresent(pdpPageATBBtn))
									{
										boolean prdtAdded = false;
										do
										{
											prdtAdded = bagCount(val);
											if(prdtAdded==true)
											{
												prdtAdded=true;
											}
											else
											{
												continue;
											}
											
										}while(!prdtAdded==true);
										
										boolean pgok = false;
										do
										{
											Thread.sleep(2000);
											if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn))
											{
												pgok = true;
												System.out.println(pgok);
												break;
											}
											else
											{
												((JavascriptExecutor) driver).executeScript("arguments[0].click();", chkoutMaskId);
												Thread.sleep(1000);
												bagIcon.click();
												pgok = false;
												continue;
											}
										}while(pgok=true);
										
										if(BNBasicfeature.isElementPresent(mcContinueToCheckOutBtn))
										{
											wait.until(ExpectedConditions.visibilityOf(miniCartSubTotalContainer));
											BNBasicfeature.scrolldown(miniCartSubTotalContainer, driver);
											if(BNBasicfeature.isElementPresent(miniCartSubTotalContainer))
											{
												String subtotal = miniCartSubTotalDetails.getText();
												HSSFCell totalline = sheet.getRow(i).createCell(15);
												totalline.setCellType(Cell.CELL_TYPE_STRING);
												totalline.setCellValue(subtotal);
												//System.out.println(subtotal);
												String subtotalprice = miniCartSubTotalPriceDetails.getText();
												HSSFCell totalprice = sheet.getRow(i).createCell(16);
												totalprice.setCellType(Cell.CELL_TYPE_STRING);
												totalprice.setCellValue(subtotalprice);
												//System.out.println(subtotalprice);
												String shipdet = miniCartShippingDetails.getText();
												HSSFCell prdtshipdet = sheet.getRow(i).createCell(18);
												prdtshipdet.setCellType(Cell.CELL_TYPE_STRING);
												prdtshipdet.setCellValue(shipdet);
												//System.out.println(shipdet);
												String txdet  = miniCartShippingTaxDetails.getText();
												HSSFCell prdTaxdet = sheet.getRow(i).createCell(20);
												prdTaxdet.setCellType(Cell.CELL_TYPE_STRING);
												prdTaxdet.setCellValue(txdet);
												//System.out.println(txdet);
												String shipoveralltotdetails = miniCartShippingTotalDetails.getText();
												HSSFCell prdTotal= sheet.getRow(i).createCell(22);
												prdTotal.setCellType(Cell.CELL_TYPE_STRING);
												prdTotal.setCellValue(shipoveralltotdetails);
												//System.out.println(shipoveralltotdetails);
												/*FileOutputStream fos = new FileOutputStream(BNConstants.commonExcelFile);
												wb.write(fos);*/
												/*mcContinueToCheckOutBtn.click();
												//driver.get(BNConstants.guestcheckoutURL);
											}
											else
											{
												System.out.println("The Subtotal Container is not found.");
											}
										}
										else
										{
											System.out.println("The Guest Checkout Button is not found.");
										}
									}
									else
									{
										System.out.println("The Product Add to Bag is not found.Please Check.");
									}
								}
								else
								{
									System.out.println("The searched book content is not found.");
								}
								}
								else
								{
									System.out.println("The Product Page doesn't list any product.Please Check.");
								}
							}
							else
							{
								System.out.println("The Product Page is empty. Please Check the searched keyword.");
							}
						}
						else
						{
							System.out.println("The Search Icon is not found.Please Check.");
						}
					}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
				}
			}
		}
	}*/

	/* BRM - 623 Guest Checkout � Verify that on selecting the "Checkout" button the Shopping overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page*/
	/* BRM - 346 Verify that while selecting the "Checkout" option in the shopping bag page,the Login overlay should be displayed when the user is not signed In.*/
	public void signInorCheckoutoverlay(boolean val)
	{
		ChildCreation(" BRM - 623 Verify that on selecting the Checkout button the Shopping overlay should be enabled to Proceed for the Sign in/Check out as Guest checkout page.");
		boolean egftCard = false;
		boolean nookAdded = false;
		try
		{
			String pgcaption = BNBasicfeature.getExcelVal("BRM623", sheet, 2);
			String[] title = pgcaption.split("\n");
			String cellVal = BNBasicfeature.getExcelNumericVal("BRM623", sheet, 27);
			String[] eanNumbers = cellVal.split("\n");
			String[] gftCardeanNumbers = BNBasicfeature.getExcelNumericVal("BRM623", sheet, 29).split("\n");
			String[] egftCardeanNumbers = BNBasicfeature.getExcelNumericVal("BRM623", sheet, 28).split("\n");
			String[] nookeanNumbers = BNBasicfeature.getExcelNumericVal("BRM623", sheet, 30).split("\n");
			
			//addProduct(sheet, Child, false);
			//Thread.sleep(2000);
			//addProduct(sheet,val);
			boolean prdtFound = false;
			
			if(prdtAdded==false)
			{
				for(int i = 0; i<eanNumbers.length;i++)
				{
					prdtFound = searchEanNumbers(eanNumbers[i]);
					if(prdtFound == true)
					 {
						prdtAdded = clickaddtoBag(0, pdpPageATBBtn);
						if(prdtAdded==true)
						{
							prdtAdded = false;
							break;
						}
					 }
					else
					{
						continue;
					}
				}
			}
			
			/*if(prdtFound==true)
			{
				prdtAdded = clickaddtoBag(0, pdpPageATBBtn);
				if(prdtAdded==true)
				{
					prdtAdded = false;
				}
			}*/
			
			// NOOK PRODUCTS
			if(prdtAdded==true)
			{
				for(int i = 0; i<nookeanNumbers.length;i++)
				{
					prdtFound = searchEanNumbers(nookeanNumbers[i]);
					//String type = "";
					if(prdtFound==true)
					{
						if(BNBasicfeature.isElementPresent(pdpPageProductDropDown))
						{
							BNBasicfeature.scrolldown(pdpPageProductDropDown, driver);
							//Thread.sleep(1000);
							//pdpPageProductDropDown.click();
							//Select sel = new Select(pdpPageProductDropDown);
							//sel.selectByValue("NOOK Book");
							List<WebElement> ele = driver.findElements(By.xpath("//*[contains(@class,'sk_pdtType')]//*[@pdttype]"));
							nookAdded = false;
							for(int j = 1; j<=ele.size();j++)
							{
								String actVal = ele.get(j).getAttribute("value");
								if(actVal.contains("NOOK"))
								{
									ele.get(j).click();
									Thread.sleep(500);
									//type = ele.get(j).getAttribute("value");
									nookAdded = true;
									break;
								}
								else
								{
									nookAdded = false;
									continue;
								}
							}
							
							if(nookAdded == true)
							{
								break;
							}
						}
						else
						{
							continue;
						}
					}
					else
					{
						continue;
					}
				}
			}
			
			if(nookAdded==true)
			{
				prdtAdded = clickaddtoBag(currVal, pdpPageATBBtn);
				if(prdtAdded==true)
				{
					prdtAdded = true;
				}
			}
			
			if(nookAdded==false)
			{
				// EGIFT CARD ADD
				if(prdtAdded==false)
				{
					for(int i = 0; i<egftCardeanNumbers.length;i++)
					{
						prdtFound = searchEanNumbers(egftCardeanNumbers[i]);
						if(prdtFound == true)
						{
						  if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
						  {
							pdpPageGiftCardValueField.clear();
							pdpPageGiftCardValueField.sendKeys("15");
							if(BNBasicfeature.isElementPresent(pdpPageEGiftCardReceipentField))
							{
								pdpPageEGiftCardReceipentField.sendKeys("test@mail.com");
								if(BNBasicfeature.isElementPresent(pdpPageEGiftCardConfirmEmailField))
								{
									pdpPageEGiftCardConfirmEmailField.sendKeys("test@mail.com");
									if(BNBasicfeature.isElementPresent(pdpPageGiftCardTo))
									{
										pdpPageGiftCardTo.sendKeys("SKAVA");
										if(BNBasicfeature.isElementPresent(pdpPageGiftCardFrom))
										{
											pdpPageGiftCardFrom.sendKeys("INFOSYSY");
											Thread.sleep(100);
											if(BNBasicfeature.isElementPresent(pdpPageEGiftCardATBBtn))
											{
												log.add("The Add to Bag icon is displayed.");
												prdtAdded = clickaddtoBag(currVal, pdpPageEGiftCardATBBtn);
												if(prdtAdded == true)
												{
													egftCard = true;
													break;
												}
											}
											else
											{
												Fail("The Add to Bag Button is not displayed in the Gift Card page.");
											}
										}
									}
								}
							}
						 }
						}
						else
						{
							continue;
						}
					}
				}
			
				// GIFT CARD ADD
				if(egftCard==false)
				{
					for(int i = 0; i<gftCardeanNumbers.length;i++)
					{
						prdtFound = searchEanNumbers(gftCardeanNumbers[i]);
						if(prdtFound==true)
						{
							if(BNBasicfeature.isElementPresent(pdpPageGiftCardValueField))
							{
								pdpPageGiftCardValueField.clear();
								pdpPageGiftCardValueField.sendKeys("15");
								if(BNBasicfeature.isElementPresent(pdpPageGiftCardNoOfCardsDD))
								{
									Select sel = new Select(pdpPageGiftCardNoOfCardsDD);
									sel.selectByIndex(2);
									Thread.sleep(200);
									if(BNBasicfeature.isElementPresent(pdpPageGiftCardATBBtn))
									{
										log.add("The Add to Bag icon is displayed.");
										prdtAdded = clickaddtoBag(currVal, pdpPageGiftCardATBBtn);
										if(prdtAdded == true)
										{
											egftCard = true;
											prdtFound = false;
											break;
										}
									}
									else
									{
										Fail("The Add to Bag Button is not displayed in the Gift Card page.");
									}
								}
							}
						}
						else
						{
							continue;
						}
					}
				}
			}
			
			if(prdtAdded==true)
			{
				Pass("The Product added successfully.");
				boolean loadMin = loadMiniCart();
				if(loadMin==true)
				{
					Pass("The Mini Cart loaded successfully.");
					jsclick(mcContinueToCheckOutBtn.get(0));
					//BNBasicfeature.loginCred();
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageFrame));
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					if(pgetitle.getText().equals(title[0]))
					{
						log.add("The Page title is : " + pgetitle.getText());
						Pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
					}
					else
					{
						log.add("The Page title is : " + pgetitle.getText());
						log.add("The expected page title is " + title[0]);
						Fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
					}
					
					if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
					{
						Pass("The Checkout as Guset button is visible.");
						if(checkoutAsGuestbtn.getText().equals(title[1]))
						{
							Pass("The Checkout as Guest button caption matches with the expected one.");
						}
						else
						{
							Fail("The Checkout as Guest button caption does not matches with the expected one.");
						}
					}
					else
					{
						Fail("The Checkout as Guest Button is not visible / present.");
					}
				}
				else
				{
					Fail("The Mini Cart failed to load.");
				}
			}
			else
			{
				Fail("The User failed to add the product to the cart.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 623 Issue ." + e.getMessage());
			Exception(" BRM - 623 Issue ." + e.getMessage());
		}
		
		/* BRM - 346 Verify that while selecting the "Checkout" option in the shopping bag page,the Login overlay should be displayed when the user is not signed In.*/
		ChildCreation(" BRM - 346 Verify that while selecting the Checkout option in the shopping bag page,the Login overlay should be displayed when the user is not signed In.");
		try
		{
			String pgcaption = BNBasicfeature.getExcelVal("BRM623", sheet, 2);
			String[] title = pgcaption.split("\n");
			wait.until(ExpectedConditions.visibilityOf(pgetitle));
			if(pgetitle.getText().equals(title[0]))
			{
				log.add("The Page title is : " + pgetitle.getText());
				Pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
			}
			else
			{
				log.add("The Page title is : " + pgetitle.getText());
				log.add("The expected page title is " + title[0]);
				Fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 346 Issue ." + e.getMessage());
			Exception(" BRM - 346 Issue ." + e.getMessage());
		}
	}
	
	/* BRM - 346 Verify that while selecting the "Checkout" option in the shopping bag page,the Login overlay should be displayed when the user is not signed In.*/
	public void guestSignInOverlayNavigation()
	{
		ChildCreation(" BRM - 346 Verify that while selecting the Checkout option in the shopping bag page,the Login overlay should be displayed when the user is not signed In.");
		try
		{
			String pgcaption = BNBasicfeature.getExcelVal("BRM623", sheet, 2);
			String[] title = pgcaption.split("\n");
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(header));
			boolean loadMin = loadMiniCart();
			if(loadMin==true)
			{
				Pass("The Mini Cart loaded successfully.");
				jsclick(mcContinueToCheckOutBtn.get(0));
				wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(checkoutPageFrame));
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(pgetitle.getText().equals(title[0]))
				{
					log.add("The Page title is : " + pgetitle.getText());
					Pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
				}
				else
				{
					log.add("The Page title is : " + pgetitle.getText());
					log.add("The expected page title is " + title[0]);
					Fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
				}
			}
			else
			{
				Fail("The Mini Cart Page is not loaded.");
			}
		}
		catch (Exception e)
		{
			Exception("There is something wrong.Please Check." + e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	/* BRM - 422 Verify that login overlay should have the option to process checkout as a guest and registered user.*/
	public void guestsignInOverlayDetails()
	{
		ChildCreation("BRM - 422 Verify that login overlay should have the option to process checkout as a guest and registered user.");
		try
		{
			String pgcaption = BNBasicfeature.getExcelVal("BRM623", sheet, 2);
			String[] title = pgcaption.split("\n");
			if(pgetitle.getText().equals(title[0]))
			{
				log.add("The Page title is : " + pgetitle.getText());
				Pass("The user is navigated to the Checkout page and the caption matches with the expected one.",log);
			}
			else
			{
				log.add("The Page title is : " + pgetitle.getText());
				log.add("The expected page title is " + title[0]);
				Fail("The user is navigated to the Checkout page and the caption does not matches with the expected one.",log);
			}
			
			if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
			{
				Pass("The Checkout as Guset button is visible.");
				if(checkoutAsGuestbtn.getText().equals(title[1]))
				{
					Pass("The Checkout as Guest button caption matches with the expected one.");
				}
				else
				{
					Fail("The Checkout as Guest button caption does not matches with the expected one.");
				}
			}
			else
			{
				Fail("The Checkout as Guest Button is not visible / present.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 422 Issue ." + e.getMessage());
			Exception(" BRM - 422 Issue ." + e.getMessage());
		}
	}

	/* BRM - 627 Guest Checkout � Verify that the Shipping section should be displayed as per the creative in the guest checkout page*/
	public void guestCheckoutoverlay()
	{
		ChildCreation(" BRM - 627 Verify that the Shipping section should be displayed as per the creative in the guest checkout page.");
		try
		{
			String pgcaption = BNBasicfeature.getExcelVal("BRM627", sheet, 2);
			String expcolor = BNBasicfeature.getExcelVal("BRM627", sheet, 3);
			String[] title = pgcaption.split("\n");
			if(BNBasicfeature.isElementPresent(checkoutAsGuestbtn))
			{
				jsclick(checkoutAsGuestbtn);
				wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
				Thread.sleep(1000);
				
				if(BNBasicfeature.isElementPresent(guestCheckoutAddPageTitle))
				{
					gcLoaded = true;
					log.add("The User is naviagted to the Guest Checkout Page.");
					if(guestCheckoutAddPageTitle.getText().contains(title[0]))
					{
						log.add("The expected page title was : " + title[0]);
						log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
						Pass("The Guest Checkout Page title matches the expected content.",log);
					}
					else
					{
						log.add("The expected page title was : " + title[0]);
						log.add("The actual page title is : " + guestCheckoutAddPageTitle.getText());
						Fail("The Guest Checkout Page title does not matches the expected content.",log);
					}
					
					if(BNBasicfeature.isElementPresent(checkoutSteps))
					{
						Pass("The Checkout Steps container is displayed.");
						if(BNBasicfeature.isListElementPresent(checkoutStepsLists))
						{
							for(int j = 1; j<=checkoutStepsLists.size();j++)
							{
								WebElement chksteps = driver.findElement(By.xpath("(//*[@class='header-checkout']//a)["+j+"]"));
								if(chksteps.getText().contains(title[j]))
								{
									log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
									log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
									Pass("The Checkout Steps " + j + " matches with the expected one.",log);
								}
								else
								{
									log.add("The exected " + j + " step in the Checkout Steps is : " + title[j]);
									log.add("The actual " + j + " step in the Checkout Steps is : " + chksteps.getText());
									Fail("The Checkout Steps " + j + " does not matches with the expected one.",log);
								}
							}
						}
						else
						{
							Fail("The Checkout Steps List is not displayed.Please Check.");
						}
					}
					else
					{
						Fail("The Checkout Steps container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(countrySelection))
					{
						Pass("The Country drop down list box is present and visible.");
					}
					else
					{
						Fail("The Country drop down list box is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(fName))
					{
						Pass("The First Name text field is present and visible.");
					}
					else
					{
						Fail("The First Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(lName))
					{
						Pass("The Last Name text field is present and visible.");
					}
					else
					{
						Fail("The Last Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(stAddress))
					{
						Pass("The Stree Address text field is present and visible.");
					}
					else
					{
						Fail("The Stree Address text field is not present / visible.");
					}
					
					BNBasicfeature.scrolldown(aptSuiteAddress, driver);
					if(BNBasicfeature.isElementPresent(aptSuiteAddress))
					{
						Pass("The Apt/Suite text field is present and visible.");
					}
					else
					{
						Fail("The Apt/Suite text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(city))
					{
						Pass("The City text field is present and visible.");
					}
					else
					{
						Fail("The City text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(state))
					{
						Pass("The State selection drop down is present and visible.");
					}
					else
					{
						Fail("The State selection drop down is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(zipCode))
					{
						Pass("The Zipcode text field is present and visible.");
					}
					else
					{
						Fail("The Zipcode text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(contactNo))
					{
						Pass("The Phone Number text field is present and visible.");
					}
					else
					{
						Fail("The Phone Number text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(companyName))
					{
						Pass("The Company Name text field is present and visible.");
					}
					else
					{
						Fail("The Company Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(orderSummaryContainer))
					{
						Pass("The Summary Container area is present and visible.");
					}
					else
					{
						Fail("The Summary Container area is not present / visible.");
					
					}
					
					if(BNBasicfeature.isElementPresent(orderContinueBtn))
					{
						Pass("The Continue button is present and visible.");
						
						String ColorName = orderContinueBtn.getCssValue("background").substring(0, 17);
						Color colorhxcnvt = Color.fromString(ColorName);
						String hexCode = colorhxcnvt.asHex();
						
						if(expcolor.equals(hexCode))
						{
							log.add("The expected color of the Contintue button is : " + expcolor);
							log.add("The actual color of the Continue button is : " + hexCode);
							Pass("The Continue button color matches with the expected One.",log);
						}
						else
						{
							log.add("The expected color of the Contintue button is : " + expcolor);
							log.add("The actual color of the Continue button is : " + hexCode);
							Fail("The Continue button color does not matches with the expected One.",log);
						}
					}
					else
					{
						Fail("The Summary Container area is not present / visible.");
					}
				}
				else
				{
					Fail("The User is not naviagted to the Guest Checkout Page.");
				}
			}
			else
			{
				Exception("The Checkout as Guest button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 627 Issue ." + e.getMessage());
			Exception(" BRM - 627 Issue ." + e.getMessage());
		}
	}

	/* BRM - 629 Guest Checkout � Guest Checkout � Verify that to add a new shipping address the required input fields should be displayed*/
	public void guestCheckoutfields()
	{
		ChildCreation(" BRM - 629 Verify that to add a new shipping address the required input fields should be displayed.");
		if(gcLoaded == true)
		{
			try
			{
				BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Pass("The Country drop down list box is present and visible.");
				}
				else
				{
					Fail("The Country drop down list box is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(fName))
				{
					Pass("The First Name text field is present and visible.");
				}
				else
				{
					Fail("The First Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name text field is present and visible.");
				}
				else
				{
					Fail("The Last Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Stree Address text field is present and visible.");
				}
				else
				{
					Fail("The Stree Address text field is not present / visible.");
				}
				
				BNBasicfeature.scrolldown(aptSuiteAddress, driver);
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					Pass("The Apt/Suite text field is present and visible.");
				}
				else
				{
					Fail("The Apt/Suite text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City text field is present and visible.");
				}
				else
				{
					Fail("The City text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State selection drop down is present and visible.");
				}
				else
				{
					Fail("The State selection drop down is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					Pass("The Zipcode text field is present and visible.");
				}
				else
				{
					Fail("The Zipcode text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Phone Number text field is present and visible.");
				}
				else
				{
					Fail("The Phone Number text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name text field is present and visible.");
				}
				else
				{
					Fail("The Company Name text field is not present / visible.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 629 Issue ." + e.getMessage());
				Exception(" BRM - 629 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 598 Verify that Shipping Delivery preferences should not be shown if the user doesn't completed the shipping and billing information in the checkout for the guest user.*/
	public void guestNoDeliveryNavigation()
	{
		ChildCreation(" BRM - 598 Verify that Shipping Delivery preferences should not be shown if the user doesn't completed the shipping and billing information in the checkout for the guest user.");
		if(gcLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(deliveryCheckoutStep))
				{
					Actions act = new Actions(driver);
					act.moveToElement(deliveryCheckoutStep).click().build().perform();
					Thread.sleep(2000);
					boolean steptwo = deliveryCheckoutStep.getAttribute("class").contains("active");
					if(steptwo == true)
					{
						Fail("The user is in the Delivery Page when no Shipping Information was entered.");
						jsclick(shippingCheckoutStep);
						wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
					}
					else
					{
						Pass("The user is not in the Delivery Page");
					}
				}
				else
				{
					Fail( "The Delivery Checkout Step TWO is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 598 Issue ." + e.getMessage());
				Exception(" BRM - 598 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 628 Guest Checkout � Verify that the guest user can able to add the new shipping address details in the shipping section*/
	public void guestCheckoutEnterdatas()
	{
		ChildCreation(" BRM - 628 Verify that the guest user can able to add the new shipping address details in the shipping section.");
		if(gcLoaded==true)
		{
			try
			{
				String firstname = BNBasicfeature.getExcelVal("BRM628", sheet, 6);
				String lastname = BNBasicfeature.getExcelVal("BRM628", sheet, 7);
				String streetAdd = BNBasicfeature.getExcelVal("BRM628", sheet, 8);
				String cty = BNBasicfeature.getExcelVal("BRM628", sheet, 10);
				String zpcode = BNBasicfeature.getExcelNumericVal("BRM628", sheet, 12);
				String cntnumber = BNBasicfeature.getExcelNumericVal("BRM628", sheet, 13);
				BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				Actions act = new Actions(driver);
				act.moveToElement(countrySelection).build().perform();
				Select sel = new Select(countrySelection);
				sel.selectByValue("US");
				clearAll();
				fName.sendKeys(firstname);
				lName.sendKeys(lastname);
				stAddress.sendKeys(streetAdd);
				BNBasicfeature.scrolldown(city, driver);
				city.sendKeys(cty);
				Select stsel = new Select(state);
				stsel.selectByIndex(35);
				zipCode.sendKeys(zpcode);
				contactNo.sendKeys(cntnumber);
				
				BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				
				if(fName.getAttribute("value").equals(firstname))
				{
					log.add("The entered value from the excel file was " + firstname);
					log.add("The current value in the First Name field is : " + fName.getAttribute("value").toString());
					Pass("The user was able to enter the value in the First Name field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + firstname);
					log.add("The current value in the First Name field is : " + fName.getAttribute("value").toString());
					Fail("The user was able to enter the value in the First Name field but there is some mismatch.",log);
				}
				
				if(lName.getAttribute("value").equals(lastname))
				{
					log.add("The entered value from the excel file was " + lastname);
					log.add("The current value in the Last Name field is : " + lName.getAttribute("value").toString());
					Pass("The user was able to enter the value in the Last Name field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + lastname);
					log.add("The current value in the Last Name field is : " + lName.getAttribute("value").toString());
					Fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
				}
				if(stAddress.getAttribute("value").equals(streetAdd))
				{
					log.add("The entered value from the excel file was " + streetAdd);
					log.add("The current value in the Street Address field is : " + stAddress.getAttribute("value").toString());
					Pass("The user was able to enter the value in the Stree Address field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + streetAdd);
					log.add("The current value in the Street Address field is : " + stAddress.getAttribute("value").toString());
					Fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
				}
				
				BNBasicfeature.scrolldown(aptSuiteAddress, driver);
				
				if(city.getAttribute("value").equals(cty))
				{
					log.add("The entered value from the excel file was " + cty);
					log.add("The current value in the City field is : " + city.getAttribute("value").toString());
					Pass("The user was able to enter the value in the City field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + cty);
					log.add("The current value in the City field is : " + city.getAttribute("value").toString());
					Fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
				}
				if(stsel.getFirstSelectedOption().getText()!=null)
				{
					log.add("The current value in the State Name field is : " + stsel.getFirstSelectedOption().getText());
					Pass("The user was able to Select the State.",log);
				}
				else
				{
					log.add("The current value in the State Name field is : " + stsel.getFirstSelectedOption().getText());
					Fail("The State is not selected.",log);
					
				}
				if(zipCode.getAttribute("value").toString().equals(zpcode))
				{
					log.add("The entered value from the excel file was " + zpcode);
					log.add("The current value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
					Pass("The user was able to enter the value in the Zipcode field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + zpcode);
					log.add("The current value in the Zipcode field is : " + zipCode.getAttribute("value").toString());
					Fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
				}
				
				
				if(contactNo.getAttribute("value").equals(cntnumber))
				{
					log.add("The entered value from the excel file was " + cntnumber);
					log.add("The current value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
					Pass("The user was able to enter the value in the Contact Number field.",log);
				}
				else
				{
					log.add("The entered value from the excel file was " + cntnumber);
					log.add("The current value in the Contact Number field is : " + contactNo.getAttribute("value").toString());
					Fail("The user was able to enter the value in the Zipcode field but there is some mismatch.",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 628 Issue ." + e.getMessage());
				Exception(" BRM - 628 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		
	/* BRM - 630 Guest Checkout � Verify that default text should be displayed in all the input fields in the shipping address section for the guest checkout user */
	public void guestCheckoutDefaultText()
	{
		ChildCreation(" BRM - 630 Verify that default text should be displayed in all the input fields in the shipping address section for the guest checkout user.");
		if(gcLoaded==true)
		{
			try
			{
				String fNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 6);
				String lNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 7);
				String stNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 8);
				String suiteNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 9);
				String cityNametxt = BNBasicfeature.getExcelVal("BRM630", sheet, 10);
				String stateNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 11);
				String zipcodeNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 12);
				String phoneNumbertext = BNBasicfeature.getExcelVal("BRM630", sheet, 13);
				String companyNametext = BNBasicfeature.getExcelVal("BRM630", sheet, 14);
				BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				if(BNBasicfeature.isElementPresent(fNametxt))
				{
					if(fNametxt.getText().equals(fNametext))
					{
						log.add("The current value in the First Name text field is : " + fNametxt.getText());
						Pass("The First Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The First Name text is missing or the value does not matches with the expected one. The current value in the text field is " + fNametxt.getText());
					}
				}
				else
				{
					Fail("The First Name inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(lNametxt))
				{
					if(lNametxt.getText().equals(lNametext))
					{
						log.add("The current value in the Last Name text field is : " + lNametxt.getText());
						Pass("The Last Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Last Name text is missing or the value does not matches with the expected one. The current value in the text field is " + lNametxt.getText());
					}
				}
				else
				{
					Fail("The Last Name inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddresstxt))
				{
					if(stAddresstxt.getText().equals(stNametext))
					{
						log.add("The current value in the Street Name text field is : " + stAddresstxt.getText());
						Pass("The Street Address text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Street Address text is missing or the value does not matches with the expected one. The current value in the text field is " + stAddresstxt.getText());
					}
				}
				else
				{
					Fail("The Street Address inline text is not found.");
				}
				
				BNBasicfeature.scrolldown(aptSuiteAddress, driver);
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddresstxt))
				{
					if(aptSuiteAddresstxt.getText().equals(suiteNametext))
					{
						log.add("The current value in the Apt Suite text field is : " + aptSuiteAddresstxt.getText());
						Pass("The Apt / Suite Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Apt / Suite Name text is missing or the value does not matches with the expected one. The current value in the text field is " + aptSuiteAddresstxt.getText());
					}
				}
				else
				{
					Fail("The Apt/Suite Name inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(citytxt))
				{
					if(citytxt.getText().equals(cityNametxt))
					{
						log.add("The current value in the City text field is : " + citytxt.getText());
						Pass("The City Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The City Name text is missing or the value does not matches with the expected one. The current value in the text field is " + citytxt.getText());
					}
				}
				else
				{
					Fail("The City Name inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(statetxt))
				{
					if(statetxt.getText().equals(stateNametext))
					{
						log.add("The current value in the State text field is : " + statetxt.getText());
						Pass("The State Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The State Name text is missing or the value does not matches with the expected one. The current value in the text field is " + statetxt.getText());
					}
				}
				else
				{
					Fail("The State Name inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(zipcodetxt))
				{
					if(zipcodetxt.getText().equals(zipcodeNametext))
					{
						log.add("The current value in the Zip Code text field is : " + zipcodetxt.getText());
						Pass("The Zip Code text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Zip Code text is missing or the value does not matches with the expected one. The current value in the text field is " + zipcodetxt.getText());
					}
				}
				else
				{
					Fail("The Zip Code inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNotxt))
				{
					if(contactNotxt.getText().equals(phoneNumbertext))
					{
						log.add("The current value in the Contact Number text field is : " + contactNotxt.getText());
						Pass("The Contact Number text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Contact Number text is missing or the value does not matches with the expected one. The current value in the text field is " + contactNotxt.getText());
					}
				}
				else
				{
					Fail("The Contact Number inline text is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(companyNametxt))
				{
					if(companyNametxt.getText().equalsIgnoreCase(companyNametext))
					{
						log.add("The current value in the Company Name field is : " + companyNametxt.getText());
						Pass("The Company Name text is present and the value matches with the expected one.",log);
					}
					else
					{
						Fail("The Company Name text is missing or the value does not matches with the expected one. The current value in the text field is " + companyNametxt.getText());
					}
				}
				else
				{
					Fail("The Company Name inline text is not found.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 630 Issue ." + e.getMessage());
				Exception(" BRM - 630 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 634 Guest Checkout � Verify that user can be able to select country from the drop down and by default United states should be selected*/
	public void guestCheckoutDefaultCountry()
	{
		ChildCreation(" BRM - 634 Verify that user can be able to select country from the drop down and by default United states should be selected.");
		if(gcLoaded==true)
		{
			try
			{
				String country = BNBasicfeature.getExcelVal("BRM634", sheet, 5);
				String[] cntyname = country.split("\n");
				//BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Select cntysel = new Select(countrySelection);
					if(cntysel.getFirstSelectedOption().getText().equals(cntyname[0]))
					{
						Pass("The default Selected Country is : " + cntysel.getFirstSelectedOption().getText() + "  and it matches the expected one.");
						Actions act = new Actions(driver);
						cntysel.selectByValue(cntyname[1]);
						act.sendKeys(Keys.TAB).build().perform();
						if(cntysel.getFirstSelectedOption().getText().equals(cntyname[2]))
						{
							Pass("The User was able to select the desired country : " + cntysel.getFirstSelectedOption().getText() + "  and it matches the expected one.");
						}
						else
						{
							Fail("The User was not able to select the desired country. The current selected Country is " + cntysel.getFirstSelectedOption().getText() + "  and the expected country was : " + cntyname[1]);
						}
					}
					else
					{
						Fail("The default Selected Country is : " + cntysel.getFirstSelectedOption().getText() +" and it does not matches the expected one. The expected country was " + cntyname[1]);
					}
				}
				else
				{
					Fail( "The Country Selection Drop Down field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 634 Issue ." + e.getMessage());
				Exception(" BRM - 634 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 636 Guest Checkout � Verify that user can be able to enter first name in the First name field and it should accept maximum of 40 characters*/
	public void guestCheckoutFirstNameLengthValidation()
	{
		ChildCreation(" BRM - 636 Verify that user can be able to enter first name in the First name field and it should accept maximum of 40 characters.");
		if(gcLoaded==true)
		{
			try
			{
				String[] firstName = BNBasicfeature.getExcelVal("BRM636", sheet, 6).split("\n");
				BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
				Actions act = new Actions(driver);
				for(int i = 0; i<firstName.length; i++)
				{
					fName.clear();
					fName.sendKeys(firstName[i]);
					act.sendKeys(Keys.TAB).build().perform();
					if(fName.getAttribute("value").toString().length()<=40)
					{
						log.add("The entered value in the First Name from the excel file is " + firstName[i] + " and its length is " + firstName[i].length() + " . The current value in the First Name field is " + fName.getText() + " and its length is " + fName.getAttribute("value").length());
						Pass("The First Name field accepts character less than 40 only.",log);
					}
					else
					{
						log.add("The entered value in the First Name from the excel file is " + firstName[i] + " and its length is " + firstName[i].length() + " . The current value in the First Name field is " + fName.getText() + " and its length is " + fName.getAttribute("value").length());
						Fail("The First Name field accepts character more than 40 .",log);
					}
					fName.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 636 Issue ." + e.getMessage());
				Exception(" BRM - 636 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 640 Guest Checkout � Verify that user can be able to enter last name in the Last name field and it should accept maximum of 40 characters*/
	public void guestCheckoutLastNameLengthValidation()
	{
		ChildCreation(" BRM - 640 Verify that user can be able to enter last name in the First name field and it should accept maximum of 40 characters.");
		if(gcLoaded==true)
		{
			try
			{
				String[] lastName = BNBasicfeature.getExcelVal("BRM640", sheet, 7).split("\n");
				Actions act = new Actions(driver);
				for(int i = 0; i<lastName.length; i++)
				{
					BNBasicfeature.scrollup(guestCheckoutAddPageTitle, driver);
					lName.clear();
					lName.sendKeys(lastName[i]);
					act.sendKeys(Keys.TAB).build().perform();
					if(lName.getAttribute("value").length()<=40)
					{
						log.add("The entered value in the Last Name from the excel file is " + lastName[i] + " and its length is " + lastName[i].length() + " . The current value in the Last Name field is " + lName.getText() + " and its length is " + lName.getAttribute("value").length());
						Pass("The Last Name field accepts character less than 40 only.",log);
					}
					else
					{
						log.add("The entered value in the Last Name from the excel file is " + lastName[i] + " and its length is " + lastName[i].length() + " . The current value in the Last Name field is " + lName.getText() + " and its length is " + lName.getAttribute("value").length());
						Fail("The Last Name field accepts character more than 40 .",log);
					}
					lName.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 640 Issue ." + e.getMessage());
				Exception(" BRM - 640 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 641 Guest Checkout � Verify that user can be able to enter street address in the Street address field and it should accept maximum of 35 characters*/
	public void guestCheckoutStreetAddressLengthValidation()
	{
		ChildCreation(" BRM - 641 Verify that the street address field accepts only maximum of 35 characters in Add a shipping address page.");
		if(gcLoaded==true)
		{
			try
			{
				String[] addr = BNBasicfeature.getExcelVal("BRM641", sheet, 8).split("\n");
				Actions act = new Actions(driver);
				for(int i = 0; i<addr.length; i++)
				{
					stAddress.clear();
					stAddress.sendKeys(addr[i]);
					act.sendKeys(Keys.TAB).build().perform();
					//Thread.sleep(1000);
					if(stAddress.getAttribute("value").length()<=35)
					{
						log.add("The entered characters for the Street Address is : " + addr[i] + " , and its length is : " + addr[i].length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + stAddress.getAttribute("value").length());
						Pass("The address field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Street Address is : " + addr[i] + " , and its length is : " + addr[i].length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + stAddress.getAttribute("value").length());
						Fail("The address field accepts character more than 35.",log);
					}
					//stAddress.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 641 Issue ." + e.getMessage());
				Exception(" BRM - 641 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 642 Guest Checkout � Verify that user can be able to enter Apt/Suite details in the Apt/Suite (Optional) field and it should accept maximum of 35 characters*/
	public void guestCheckoutAptSuiteAddressLengthValidation()
	{
		ChildCreation(" BRM - 642 Verify that user can be able to enter Apt/Suite details in the Apt/Suite (Optional) field and it should accept maximum of 35 characters.");
		if(gcLoaded==true)
		{
			try
			{
				String[] aptsuiteaddr = BNBasicfeature.getExcelVal("BRM642", sheet, 9).split("\n");
				Actions act = new Actions(driver);
				for(int i = 0; i< aptsuiteaddr.length; i++)
				{
					aptSuiteAddress.clear();
					aptSuiteAddress.sendKeys(aptsuiteaddr[i]);
					act.sendKeys(Keys.TAB).build().perform();
					//Thread.sleep(1000);
					if(aptSuiteAddress.getAttribute("value").length()<=35)
					{
						log.add("The entered characters for the Apt, Suite address field is : " + aptsuiteaddr[i] + " , and its length is : " + aptsuiteaddr[i].length());
						log.add("The Current Value in the Street Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + aptSuiteAddress.getAttribute("value").length());
						Pass("The Apt, Suite field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Apt, Suite Address field is : " + aptsuiteaddr[i] + " , and its length is : " + aptsuiteaddr[i].length());
						log.add("The Current Value in the Apt, Suite Address field is : " + stAddress.getAttribute("value").toString() + " , and its length is : " + aptSuiteAddress.getAttribute("value").length());
						Fail("The Apt, Suite address field accepts character more than 35.",log);
					}
					//aptSuiteAddress.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 642 Issue ." + e.getMessage());
				Exception(" BRM - 642 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 643 Guest Checkout � Verify that user can be able to enter city details in the City field and it should accept maximum of 60 characters*/
	public void guestCheckoutCityLengthValidation()
	{
		ChildCreation(" BRM - 643 Verify that the city field accepts only maximum of 60 characters in Add a shipping address page");
		if(gcLoaded==true)
		{
			try
			{
				String[] cityname = BNBasicfeature.getExcelVal("BRM643", sheet, 10).split("\n");
				Actions act = new Actions(driver);
				BNBasicfeature.scrolldown(stAddress, driver);
				for(int i = 0; i<cityname.length; i++)
				{
					city.clear();
					city.sendKeys(cityname[i]);
					act.sendKeys(Keys.TAB).build().perform();
					//Thread.sleep(1000);
					if(city.getAttribute("value").length()<=60)
					{
						log.add("The entered characters for the City Field is : " + cityname[i] + " , and its length is : " + cityname[i].length());
						log.add("The Current Value in the City Field is : " + city.getAttribute("value").toString() + " , and its length is : " + city.getAttribute("value").length());
						Pass("The City Field accepts character less than 60.",log);
					}
					else
					{
						log.add("The entered characters for the City Field is : " + cityname[i] + " , and its length is : " + cityname[i].length());
						log.add("The Current Value in the City Field is : " + city.getAttribute("value").toString() + " , and its length is : " + city.getAttribute("value").length());
						Fail("The City Field accepts character more than 60.",log);
					}
					//city.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 643 Issue ." + e.getMessage());
				Exception(" BRM - 643 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 644 Guest Checkout � Verify that user can be able to select state from the state drop down*/
	public void guestCheckoutStateSelectionValidation()
	{
		ChildCreation(" BRM - 644 Verify that user can be able to select state from the state drop down.");
		if(gcLoaded==true)
		{
			try
			{
				String stname = BNBasicfeature.getExcelVal("BRM644", sheet, 11);
				String[] name = stname.split("\n");
				if(BNBasicfeature.isElementPresent(state))
				{
					BNBasicfeature.scrollup(countrySelection, driver);
					Select ctsel = new Select(countrySelection);
					String selcountry = ctsel.getFirstSelectedOption().getText();
					BNBasicfeature.scrolldown(city, driver);
					//act.moveToElement(state).click().build().perform();
					Select stsel = new Select(state);
					stsel.selectByValue(name[0]);
					int stsize = stsel.getOptions().size();
					boolean stfound = false;
					for(int j = 1; j<=stsize; j++)
					{
						if(stsel.getOptions().get(j).getText().contains(name[1]))
						{
							stsel.selectByValue(name[0]);
							stfound = true;
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(stfound == true)
					{
						log.add("The user expected state was : ONTARIO, value " + stname);
						log.add("The state selected for the Country : " + selcountry + " is : " +  stsel.getFirstSelectedOption().getText());
						Pass("The Desired state for the Country is found and it is selected.",log);
					}
					else
					{
						for(int j = 1; j<=stsize; j++)
						{
							log.add("The State populated for the Selected Country : " + selcountry + " is : " +  stsel.getOptions().get(j).getText());
						}
						log.add("The user expected state was : ONTARIO, value " + stname);
						Fail("The desired State is not found for the selected country.",log);
					}
				}
				else
				{
					Fail( "The State Selection drop down field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 644 Issue ." + e.getMessage());
				Exception(" BRM - 644 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 645 Guest Checkout - Verify that user can be able to enter zip code details in the Zip Code field and it should accept maximum of 10 characters*/
	public void guestCheckoutZipCodeLengthValidation()
	{
		ChildCreation(" BRM - 645 Verify that user can be able to enter zip code details in the Zip Code field and it should accept maximum of 10 characters.");
		if(gcLoaded==true)
		{
			try
			{
				String[] zpcode = BNBasicfeature.getExcelVal("BRM645", sheet, 12).split("\n");
				Actions act = new Actions(driver);
				BNBasicfeature.scrolldown(stAddress, driver);
				for(int i = 0; i<zpcode.length; i++)
				{
					zipCode.clear();
					zipCode.sendKeys(zpcode[i]);
					act.sendKeys(Keys.TAB).build().perform();
					//Thread.sleep(1000);
					if(zipCode.getAttribute("value").length()<=10)
					{
						log.add("The entered characters for the Zipcode Field is : " + zpcode[i] + " , and its length is : " + zpcode[i].length());
						log.add("The Current Value in the Zipcode Field is : " + zipCode.getAttribute("value").toString() + " , and its length is : " + zipCode.getAttribute("value").length());
						Pass("The Zip Code Field accepts character less than 10.",log);
					}
					else
					{
						log.add("The entered characters for the Zipcode Field is : " + zpcode[i] + " , and its length is : " + zpcode[i].length());
						log.add("The Current Value in the Zipcode Field is : " + zipCode.getAttribute("value").toString() + " , and its length is : " + zipCode.getAttribute("value").length());
						Fail("The Zip Code field accepts character more than 10.",log);
					}
					//zipCode.clear();
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 645 Issue ." + e.getMessage());
				Exception(" BRM - 645 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 646 Guest Checkout � Verify that user can be able to enter phone number in the phone number field and it should accept maximum of 60 characters */
	public void guestCheckoutPhoneNumberLengthValidation()
	{
		 ChildCreation(" BRM - 646 Verify that user can be able to enter phone number in the phone number field and it should accept maximum of 60 characters.");
		 if(gcLoaded==true)
		 {
			 try
			 {
			    String[] phoneNolengthValidation = BNBasicfeature.getExcelNumericVal("BRM646", sheet, 13).split("\n");
				Actions act = new Actions(driver);
				for(int i = 0; i<phoneNolengthValidation.length; i++)
				{
					contactNo.clear();
					contactNo.sendKeys(phoneNolengthValidation[i]);
					act.sendKeys(Keys.TAB).build().perform();
					if(contactNo.getAttribute("value").length()<=60)
					{
						log.add("The entered Phone number was " + phoneNolengthValidation[i] + " . The Current value in the Phone Number field is "+ contactNo.getAttribute("value").toString() +  " and its length was " + phoneNolengthValidation[i].length());
						Pass("The length of the Phone No field is less than or equal to 20.",log);
					}
					else
					{
						log.add("The entered Phone number was " + phoneNolengthValidation[i] + " . The Current value in the Phone Number field is "+ contactNo.getAttribute("value").toString() +  " and its length was " + phoneNolengthValidation[i].length());
						Fail("The length of the Phone No field is greater than 20.",log);
					}
					//contactNo.clear();
				}
			 }
			 catch(Exception e)
			 {
				 System.out.println(" BRM - 646 Issue ." + e.getMessage());
				 Exception(" BRM - 646 Issue ." + e.getMessage());
			 } 
		 }
		 else
		 {
			 Skip(" The User failed to load the Guest Checkout Page.");
		 }
	}
	
	/* BRM - 647 Guest Checkout � Verify that user can be able to enter company name(Optional) in the Company name(Optional) field and it should accept maximum of 35 characters */
	public void guestCheckoutCompanyLengthValidation()
	{
		ChildCreation(" BRM - 647 Verify that user can be able to enter company name(Optional) in the Company name(Optional) field and it should accept maximum of 35 characters ");
		if(gcLoaded==true)
		{
			try
			{
				String[] companyname = BNBasicfeature.getExcelVal("BRM647", sheet, 14).split("\n");
				Actions act = new Actions(driver);
				for( int i = 0; i<companyname.length; i++)
				{
					BNBasicfeature.scrolldown(city, driver);
					companyName.clear();
					companyName.sendKeys(companyname[i]);
					act.sendKeys(Keys.TAB).build().perform();
					//Thread.sleep(1000);
					if(companyName.getAttribute("value").length()<=35)
					{
						log.add("The entered characters for the Company Name field is : " + companyname[i] + " , and its length is : " + companyname[i].length());
						log.add("The Current Value in the Company Name Field is : " + companyName.getAttribute("value").toString() + " , and its length is : " + companyName.getAttribute("value").length());
						Pass("The Company Name Field accepts character less than 35.",log);
					}
					else
					{
						log.add("The entered characters for the Company Name Field is : " + companyname[i] + " , and its length is : " + companyname[i].length());
						log.add("The Current Value in the Company Name Field is : " + companyName.getAttribute("value").toString() + " , and its length is : " + companyName.getAttribute("value").length());
						Fail("The City Field accepts character more than 35.",log);
					}
					//companyName.clear();
				}
			}
			catch(Exception e)
			{
				 System.out.println(" BRM - 647 Issue ." + e.getMessage());
				 Exception(" BRM - 647 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 648 Guest Checkout � Verify that without entering any values in the input field, clicking on Submit order should display a alert message */
	public void guestCheckoutBlankSaveAlertValidation()
	{
		ChildCreation(" BRM - 648 Verify that without entering any values in the input field, clicking on Submit order should display a alert message.");
		if(gcLoaded==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM648", sheet, 4);
				wait.until(ExpectedConditions.visibilityOf(checkoutSteps));
				//BNBasicfeature.scrolldown(city, driver);
				jsclick(orderContinueBtn);
				wait.until(ExpectedConditions.visibilityOf(errorMsg));
				BNBasicfeature.scrollup(errorMsg, driver);
				if(errorMsg.getText().equals(alert))
				{
					log.add("The raised error " + errorMsg.getText());
					Pass("The appropriate error is raised when user tries to save the Submit the Order without entering the values.",log);
				}
				else
				{
					log.add("The raised error " + errorMsg.getText());
					log.add("The Expected alert was " + alert);
					Fail("The appropriate error is raised when user tries to save the Shipping Address with only Spaces.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 648 Issue ." + e.getMessage());
				Exception(" BRM - 648 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		
	/* BRM - 649 Guest Checkout � Verify that special characters should not be allowed in First name, last name, phone number and zip code fields and alerts should be displayed*/
	public void guestCheckoutInvalidFieldDataValidation()
	{
		ChildCreation(" BRM 649 Verify that special characters should not be allowed in First name, last name, phone number and zip code fields and alerts should be displayed.");
		if(gcLoaded==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM649", sheet, 4); 
				String firstName = BNBasicfeature.getExcelVal("BRM649", sheet, 6);
				String lastName = BNBasicfeature.getExcelVal("BRM649", sheet, 7);
				String zpcode = BNBasicfeature.getExcelVal("BRM649", sheet, 12);
				fName.clear();
				fName.sendKeys(firstName);
				lName.clear();
				lName.sendKeys(lastName);
				BNBasicfeature.scrolldown(city, driver);
				zipCode.clear();
				zipCode.sendKeys(zpcode);
				jsclick(orderContinueBtn);
				wait.until(ExpectedConditions.visibilityOf(errorMsg));
				BNBasicfeature.scrollup(errorMsg, driver);
				if(errorMsg.getText().equals(alert))
				{
					log.add("The raised error " + errorMsg.getText());
					Pass("The appropriate error is raised when user tries to save the Submit the Order without entering the values.",log);
				}
				else
				{
					log.add("The raised error " + errorMsg.getText());
					log.add("The Expected alert was " + alert);
					Fail("The appropriate error is raised when user tries to save the Shipping Address with only Spaces.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 649 Issue ." + e.getMessage());
				Exception(" BRM - 649 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		
	/* BRM - 651 Guest Checkout � Verify that clicking on Customer service should redirect to the customer service page */
	/* BRM - 673 Guest Checkout - Verify that clicking on Customer service should redirect to the customer service page. */
	/* BRM - 774 Guest Checkout � Verify that clicking on Customer service should redirect to the customer service page.*/
	/* BRM - 965 Guest Checkout � Other Pay Options � Verify that clicking on Customer service should redirect to the customer service page*/
	public void guestCheckoutCustomerServiceNavigation(String tc)
	{
		String tcId = "";
		boolean lc = false;
		if(tc.equals("BRM651"))
		{
			tcId = " BRM - 651 Verify that clicking on Customer service should redirect to the customer service page.";
			lc = gcLoaded;
		}
		else if(tc.equals("BRM673"))
		{
			tcId = " BRM - 673 Guest Checkout - Verify that clicking on Customer service should redirect to the customer service page.";
			lc = delOpt;
		}
		else if(tc.equals("BRM774"))
		{
			tcId = " BRM - 774 Guest Checkout - Verify that clicking on Customer service should redirect to the customer service page.";
			lc = shipPage;
		}
		else if(tc.equals("BRM965"))
		{
			tcId = " BRM - 965 Guest Checkout - Other Pay Options - Verify that clicking on Customer service should redirect to the customer service page.";
			lc = shipPage;
		}
		ChildCreation(tcId);
		if(lc==true)
		{
			try
			{
				String pgeurl = BNBasicfeature.getExcelVal("BRM651", sheet, 3);
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					BNBasicfeature.scrolldown(orderSummaryContainer, driver);
					Thread.sleep(500);
				}
				else if(BNBasicfeature.isElementPresent(orderContinueBtn))
				{
					BNBasicfeature.scrolldown(orderContinueBtn, driver);
					Thread.sleep(500);
				}
				String currUrl = driver.getCurrentUrl();
				String navUrl = "";
				if(tc.equals("BRM965"))
				{
					wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						BNBasicfeature.scrollup(guestPaypalNotActiveTab, driver);
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						Thread.sleep(500);
					}
				}
				if(BNBasicfeature.isElementPresent(customerService))
				{
					log.add("The Customer Service Link is found.");
					Actions act = new Actions(driver);
					act.moveToElement(customerService).click().build().perform();
					//jsclick(customerService);
					Thread.sleep(5000);
					navUrl = driver.getCurrentUrl();
					if(navUrl.contains(pgeurl))
					{
						log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
						log.add("The Expected URL for the Customer Service Link was : " + pgeurl);
						Pass("The user is  navigated to the Customer Service Page.",log);
					}
					else
					{
						log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
						log.add("The Expected URL for the Customer Service Link was : " + pgeurl);
						Fail("The user is not navigated to the Customer Service Page. Please Check.",log);
					}
					navUrl = driver.getCurrentUrl();
				}
					
				if(!currUrl.equals(navUrl))
				{
					driver.navigate().back();
					if(tc.equals("BRM774")||(tc.equals("BRM965")))
					{
						if(!BNBasicfeature.isElementPresent(guestPaymentsOption))
						{
							driver.navigate().refresh();
						}
					}
				}
				else
				{
					Fail("The Customer Service link is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(tc + " Issue ." + e.getMessage());
				Exception(tc + "  Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 652 Guest Checkout � Verify that clicking on terms of use should redirect to the terms of use page*/
	/* BRM - 674 Guest Checkout - Verify that clicking on terms of use should redirect to the terms of use page.*/
	/* BRM - 775 Guest Checkout � Verify that clicking on terms of use should redirect to the terms of use page.*/
	/* BRM - 966 Guest Checkout � Other Pay Options � Verify that clicking on terms of use should redirect to the terms of use page.*/
	public void guestCheckoutTermsOfUseNavigation(String tc)
	{
		String tcId = "";
		boolean lc = false;
		if(tc.equals("BRM652"))
		{
			tcId = " BRM - 652 Verify that clicking on terms of use should redirect to the terms of use page. ";
			lc = gcLoaded;
		}
		else if(tc.equals("BRM674"))
		{
			tcId = " BRM - 674 Guest Checkout - Verify that clicking on terms of use should redirect to the terms of use page.";
			lc = delOpt;
		}
		else if(tc.equals("BRM775"))
		{
			tcId = " BRM - 775 Guest Checkout - Verify that clicking on terms of use should redirect to the terms of use page.";
			lc = shipPage;
		}
		else if(tc.equals("BRM966"))
		{
			tcId = " BRM - 966 Guest Checkout - Other Pay Options � Verify that clicking on terms of use should redirect to the terms of use page.";
			lc = shipPage;
		}
		ChildCreation(tcId);
		if(lc==true)
		{
			try
			{
				String excelvalue = BNBasicfeature.getExcelVal("BRM652", sheet, 3);
				String[] pgeurl = excelvalue.split("\n");
				wait.until(ExpectedConditions.visibilityOf(checkoutSteps));
				String currUrl = driver.getCurrentUrl();
				String navUrl = "";
				if(tc.equals("BRM966"))
				{
					wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
					Thread.sleep(1000);
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						BNBasicfeature.scrollup(guestPaypalNotActiveTab, driver);
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						Thread.sleep(500);
					}
				}
				//BNBasicfeature.scrolldown(zipCode, driver);
				boolean linkfound = false;
				if(BNBasicfeature.isListElementPresent(copyrightContainer))
				{
					log.add("The Copyright Container is found and it is visible.");
					for(int j = 1; j <= copyrightContainer.size(); j++)
					{
						WebElement pgenamelink = driver.findElement(By.xpath("(//*[@class='footer-legal']//a)["+j+"]"));
						if(pgenamelink.getText().equals(pgeurl[0]))
						{
							log.add("The  Terms of Use Link is found.");
							jsclick(pgenamelink);
							Thread.sleep(1000);
							navUrl = driver.getCurrentUrl();
							if(navUrl.contains(pgeurl[1]))
							{
								linkfound=true;
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Terms of Use Page was : " + pgeurl[1]);
								Pass("The user is navigated to the Terms of Use Page.",log);
								break;
							}
							else
							{
								linkfound=true;
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Terms of Use page was : " + pgeurl[1]);
								Fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
								break;
							}
						}
						else
						{
							continue;
						}
					}
					
					navUrl = driver.getCurrentUrl();
					
					if(linkfound == true)
					{
						Pass("The Terms of Use link was found and it was clicked and verified");
					}
					else
					{
						Fail("The Terms of Use link is not found. Please Check.");
					}
					
					if(!currUrl.equals(navUrl))
					{
						driver.navigate().back();
						if(tc.equals("BRM775")||(tc.equals("BRM966")))
						{
							if(!BNBasicfeature.isElementPresent(guestPaymentsOption))
							{
								driver.navigate().refresh();
							}
						}
					}
				}
				else
				{
					Fail("The Copyrights Container List is not found. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(tc + "Issue ." + e.getMessage());
				Exception(tc + "Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		
	/* BRM - 653 Guest Checkout � Verify that clicking on Copyright should redirect to the Copyright page*/
	/* BRM - 675 Guest Checkout - Verify that clicking on Copyright should redirect to the Copyright page.*/
	/* BRM - 776 Guest Checkout � Verify that clicking on Copyright should redirect to the Copyright page.*/
	/* BRM - 967 Guest Checkout � Other Pay Options � Verify that clicking on Copyright should redirect to the Copyright page.*/
	public void guestCheckoutCopyrightsPageNavigation(String tc)
	{
		String tcId = "";
		boolean lc = false;
		if(tc.equals("BRM653"))
		{
			tcId = " BRM - 653 Guest Checkout - Verify that clicking on Copyright should redirect to the Copyright page.";
			lc = gcLoaded;
		}
		else if(tc.equals("BRM675"))
		{
			tcId = " BRM - 675 Guest Checkout - Verify that clicking on Copyright should redirect to the Copyright page.";
			lc = delOpt;
		}
		else if(tc.equals("BRM776"))
		{
			tcId = " BRM - 776 Guest Checkout - Verify that clicking on Copyright should redirect to the Copyright page.";
			lc = shipPage;
		}
		else if(tc.equals("BRM967"))
		{
			tcId = "BRM - 967 Guest Checkout - Other Pay Options � Verify that clicking on Copyright should redirect to the Copyright page.";
			lc = shipPage;
		}
		ChildCreation(tcId);
		if(lc==true)
		{
			try
			{
				String excelvalue = BNBasicfeature.getExcelVal("BRM653", sheet, 3);
				String[] pgeurl = excelvalue.split("\n");
				wait.until(ExpectedConditions.visibilityOf(checkoutSteps));
				String currUrl = driver.getCurrentUrl();
				String navUrl = "";
				if(tc.equals("BRM967"))
				{
					wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						BNBasicfeature.scrollup(guestPaypalNotActiveTab, driver);
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						Thread.sleep(500);
					}
				}
				//BNBasicfeature.scrolldown(zipCode, driver);
				boolean linkfound = false;
				if(BNBasicfeature.isListElementPresent(copyrightContainer))
				{
					log.add("The Copyright Container is found and it is visible.");
					for(int j = 1; j <= copyrightContainer.size(); j++)
					{
						WebElement pgenamelink = driver.findElement(By.xpath("(//*[@class='footer-legal']//a)["+j+"]"));
						//System.out.println(pgenamelink.getText());
						if(pgenamelink.getText().contains(pgeurl[0]))
						{
							linkfound = true;
							log.add("The  Copyrights link is found.");
							jsclick(pgenamelink);
							Thread.sleep(1000);
							if(driver.getCurrentUrl().contains(pgeurl[1]))
							{
								log.add("The user is navigated to the Copyrights Page.");
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Copyrights page was : " + pgeurl[1]);
								Pass("The User is navigated to the Copyrights Page",log);
								break;
							}
							else
							{
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Copyrights page was : " + pgeurl[1]);
								Fail("The user is not navigated to the Copyrights Page. Please Check.",log);
								break;
							}
						}
						else
						{
							continue;
						}
					}
					navUrl = driver.getCurrentUrl();
					
					if(linkfound == true)
					{
						Pass("The Copyright link was found and it was clicked and navigated.");
					}
					else
					{
						Fail("The Copyright link is not found. Please Check.");
					}
					
					if(!currUrl.equals(navUrl))
					{
						driver.navigate().back();
						if(tc.equals("BRM776")||(tc.equals("BRM967")))
						{
							if(!BNBasicfeature.isElementPresent(guestPaymentsOption))
							{
								driver.navigate().refresh();
							}
						}
					}
				}
				else
				{
					Fail("The Copyrights Container List is not found. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(tc + "Issue ." + e.getMessage());
				Exception(tc + "Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 654 Guest Checkout � Verify that clicking on Privacy policy should redirect to the Privacy policy page*/
	/* BRM - 676 Guest Checkout - Verify that clicking on Privacy policy should redirect to the Privacy policy page.*/
	/* BRM - 777 Guest Checkout � Verify that clicking on Privacy policy should redirect to the Privacy policy page.*/
	/* BRM - 968 Guest Checkout � Other Pay Options � Verify that clicking on Privacy policy should redirect to the Privacy policy page.*/
	public void guestCheckoutPolicyPageNavigation(String tc)
	{
		String tcId = "";
		boolean lc = false;
		if(tc.equals("BRM654"))
		{
			tcId = "BRM - 654 Guest Checkout - Verify that clicking on Privacy policy should redirect to the Privacy policy page.";
			lc = gcLoaded;
		}
		else if(tc.equals("BRM676"))
		{
			tcId = "BRM - 676 Guest Checkout - Verify that clicking on Privacy policy should redirect to the Privacy policy page.";
			lc = delOpt;
		}
		else if(tc.equals("BRM777"))
		{
			tcId = "BRM - 777 Guest Checkout - Verify that clicking on Privacy policy should redirect to the Privacy policy page.";
			lc = shipPage;
		}
		else if(tc.equals("BRM968"))
		{
			tcId = " BRM - 968 Guest Checkout - Other Pay Options � Verify that clicking on Privacy policy should redirect to the Privacy policy page.";
			lc = shipPage;
		}
		ChildCreation(tcId);
		if(lc==true)
		{
			try
			{
				String excelvalue = BNBasicfeature.getExcelVal("BRM654", sheet, 3);
				String[] pgeurl = excelvalue.split("\n");
				wait.until(ExpectedConditions.visibilityOf(checkoutSteps));
				String currUrl = driver.getCurrentUrl();
				String navUrl = "";
				if(tc.equals("BRM968"))
				{
					wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
					Thread.sleep(1000);
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						BNBasicfeature.scrollup(guestPaypalNotActiveTab, driver);
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						Thread.sleep(500);
					}
				}
				
				BNBasicfeature.scrolldown(orderSummaryContainer, driver);
				
				boolean linkfound = false;
				if(BNBasicfeature.isListElementPresent(copyrightContainer))
				{
					log.add("The Copyright Container is found and it is visible.");
					for(int j = 1; j <= copyrightContainer.size(); j++)
					{
						WebElement pgenamelink = driver.findElement(By.xpath("(//*[@class='footer-legal']//a)["+j+"]"));
						if(pgenamelink.getText().equals(pgeurl[0]))
						{
							linkfound = true;
							log.add("The  Privacy Policy link is found.");
							jsclick(pgenamelink);
							Thread.sleep(1000);
							navUrl = driver.getCurrentUrl();
							if(navUrl.contains(pgeurl[1]))
							{
								log.add("The user is navigated to the Privacy Policy Page.");
								log.add("The Expected URL for the Privacy Policy page was : " + pgeurl[1]);
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								Pass("The User is navigated to the Privacy Policy Page.",log);
								break;
							}
							else
							{
								log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
								log.add("The Expected URL for the Privacy Policy page was : " + pgeurl[1]);
								Fail("The user is not navigated to the Privacy Policy Page. Please Check.",log);
								break;
							}
						}
						else
						{
							continue;
						}
					}
					navUrl = driver.getCurrentUrl();
					
					if(linkfound == true)
					{
						Pass("The Privacy Policy link was found and it was clicked. Please Check.");
					}
					else
					{
						Fail("The Privacy Policy link is not found. Please Check.");
					}
					
					if(!currUrl.equals(navUrl))
					{
						driver.navigate().back();
						if(tc.equals("BRM777")||(tc.equals("BRM968")))
						{
							if(!BNBasicfeature.isElementPresent(guestPaymentsOption))
							{
								driver.navigate().refresh();
							}
						}
					}
				}
				else
				{
					Fail("The Copyrights Container List is not found. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(tc + "Issue ." + e.getMessage());
				Exception(tc + "Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 655 Guest Checkout � Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page*/
	/* BRM - 677 Guest Checkout - Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page.*/
	/* BRM - 778 Guest Checkout � Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page.*/
	/* BRM - 969 Guest Checkout � Other Pay Options � Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the Payment � terms of use page.*/
	public void guestCheckoutTermsOfUseNavigation2(String tc)
	{
		String tcId = "";
		boolean lc = false;
		if(tc.equals("BRM655"))
		{
			tcId = " BRM - 655 Guest Checkout - Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page.";
			lc = gcLoaded;
		}
		else if(tc.equals("BRM677"))
		{
			tcId = " BRM - 677 Guest Checkout - Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page.";
			lc = delOpt;
		}
		else if(tc.equals("BRM778"))
		{
			tcId = " BRM - 778 Guest Checkout - Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the terms of use page.";
			lc = shipPage;
		}
		else if(tc.equals("BRM969"))
		{
			tcId = " BRM - 969 Guest Checkout - Other Pay Options - Verify that clicking on 1997-2016 Barnesandnoble.com should redirect to the Payment � terms of use page.";
			lc = shipPage;
		}
		ChildCreation(tcId);
		if(lc==true)
		{
			try
			{
				String excelvalue = BNBasicfeature.getExcelVal("BRM655", sheet, 3);
				boolean linkfound=false;
				String[] pgeurl = excelvalue.split("\n");
				wait.until(ExpectedConditions.visibilityOf(checkoutSteps));
				//BNBasicfeature.scrolldown(zipCode, driver);
				String currUrl = driver.getCurrentUrl();
				String navUrl = "";
				if(tc.equals("BRM969"))
				{
					wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						BNBasicfeature.scrollup(guestPaypalNotActiveTab, driver);
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						Thread.sleep(500);
					}
				}
				if(BNBasicfeature.isElementPresent(siteCopyRight))
				{
					jsclick(siteCopyRight);
					Thread.sleep(1000);
					navUrl = driver.getCurrentUrl();
					if(navUrl.contains(pgeurl[1]))
					{
						linkfound=true;
						log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
						log.add("The Expected URL for the Terms of Use Page was : " + pgeurl[1]);
						Pass("The user is navigated to the Terms of Use Page.",log);
					}
					else
					{
						linkfound=true;
						log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
						log.add("The Expected URL for the Terms of Use page was : " + pgeurl[1]);
						Pass("The user is not navigated to the Terms of Use Page. Please Check.",log);
					}
				}
				navUrl = driver.getCurrentUrl();
				
				if(linkfound == true)
				{
					Pass("The Terms of Use link was found and it was clicked and verified");
				}
				else
				{
					Pass("The Terms of Use link is not found. Please Check.");
				}
				
				if(!currUrl.equals(navUrl))
				{
					driver.navigate().back();
					if(tc.equals("BRM778")||(tc.equals("BRM969")))
					{
						if(!BNBasicfeature.isElementPresent(guestPaymentsOption))
						{
							driver.navigate().refresh();
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(tc + "Issue ." + e.getMessage());
				Exception(tc + "Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		

	/* BRM - 571 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address*/
	public void guestCheckoutAddressVerification()
	{
		ChildCreation(" BRM - 571 Verify that Address Verification page should be shown after adding new shipping address or updating the existing address.");
		if(gcLoaded==true)
		{
			try
			{
				String pagetitle = BNBasicfeature.getExcelVal("BRM571", sheet, 3);
				jsclick(orderContinueBtn);
				//Thread.sleep(4000);
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					Pass("The user is redirected to the Address Verification page.");
					if(addressVerificationPartialMatchesTitle.getText().equals(pagetitle))
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationPartialMatchesTitle.getText());
						Pass("The Page title matches with the expected One.",log);
					}
					else
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationPartialMatchesTitle.getText());
						Fail("The Page title does not matches with the expected One.",log);
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					Pass("The user is redirected to the Address Verification page.");
					if(addressVerificationMatchesTitle.getText().equals(pagetitle))
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationMatchesTitle.getText());
						Pass("The Page title matches with the expected One.",log);
					}
					else
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationMatchesTitle.getText());
						Fail("The Page title does not matches with the expected One.",log);
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					Pass("The user is redirected to the Address Verification page.");
					if(addressVerificationNoMatchesTitle.getText().equals(pagetitle))
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationNoMatchesTitle.getText());
						Pass("The Page title matches with the expected One.",log);
					}
					else
					{
						log.add("The expected page title was : " + pagetitle);
						log.add("The Current page title is : " + addressVerificationNoMatchesTitle.getText());
						Fail("The Page title does not matches with the expected One.",log);
					}
				}
				else
				{
					Skip("The user is not redirected to the Address Verification page. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 571 Issue ." + e.getMessage());
				Exception(" BRM - 571 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		
	/* BRM - 582 Verify that on clicking the "edit" option in address verification page it should open the edit address page with prefilled address*/
	public void guestCheckoutAddressVerificationEdit()
	{
		ChildCreation(" BRM - 582 Verify that on clicking the Edit option in address verification page it should open the edit address page with prefilled address.");
		if(gcLoaded==true)
		{
			try
			{
				String editbutton = BNBasicfeature.getExcelVal("BRM582", sheet, 3);
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					log.add("The user is in the Address Verification page.");
					if(BNBasicfeature.isElementPresent(addressPartialMatchesEditBtn))
					{
						Pass("The Edit button is present in the Address Verification page.");
						if(addressPartialMatchesEditBtn.getText().equals(editbutton))
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressPartialMatchesEditBtn.getText());
							Pass("The Edit button caption matches with the Expected One.",log);
							jsclick(addressPartialMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
						else
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressPartialMatchesEditBtn.getText());
							Fail("The Edit button caption does not matches with the expected One.",log);
							jsclick(addressPartialMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
					}
					else
					{
						Fail("The Edit button is not found on the Address Verification Page.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					log.add("The user is redirected to the Address Verification page.");
					if(BNBasicfeature.isElementPresent(addressMatchesEditBtn))
					{
						Pass("The Edit button is present in the Address Verification page.");
						if(addressMatchesEditBtn.getText().equals(editbutton))
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressMatchesEditBtn.getText());
							Pass("The Edit button caption matches with the Expected One.",log);
							jsclick(addressMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
						else
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressMatchesEditBtn.getText());
							Fail("The Edit button caption does not matches with the expected One.",log);
							jsclick(addressMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
					}
					else
					{
						Fail("The Edit button is not found on the Address Verification Page.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					log.add("The user is redirected to the Address Verification page.");
					if(BNBasicfeature.isElementPresent(addressNoMatchesEditBtn))
					{
						Pass("The Edit button is present in the Address Verification page.");
						if(addressNoMatchesEditBtn.getText().equals(editbutton))
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressNoMatchesEditBtn.getText());
							Pass("The Edit button caption matches with the Expected One.",log);
							jsclick(addressNoMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
						else
						{
							log.add("The expected button caption was : " + editbutton);
							log.add("The Current button caption is : " + addressNoMatchesEditBtn.getText());
							Fail("The Edit button caption does not matches with the expected One.",log);
							jsclick(addressNoMatchesEditBtn);
							wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
							chkfieldvalues();
						}
					}
					else
					{
						Fail("The Edit button is not found on the Address Verification Page.");
					}
				}
				else
				{
					Skip("The user is not redirected to the Address Verification page. Please Check.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 582 Issue ." + e.getMessage());
				Exception(" BRM - 582 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
				
	/* BRM - 657 Verify that on entering all the input fields and clicking on Submit order should proceed to the Delivery page */
	public void guestCheckoutDeliveryPageNavigation()
	{
		ChildCreation(" BRM - 657 Verify that on entering all the input fields and clicking on Submit order should proceed to the Delivery page.");
		if(gcLoaded==true)
		{
			try
			{
				String pagetitle = BNBasicfeature.getExcelVal("BRM657", sheet, 3);
				jsclick(orderContinueBtn);
				wait.until(ExpectedConditions.visibilityOf(pgetitle));
				if(BNBasicfeature.isElementPresent(addressVerificationPartialMatchesTitle))
				{
					BNBasicfeature.scrolldown(addressSuggestionFirstAddress, driver);
					Random r = new Random();
					int sel = r.nextInt(addressVerficationPossibleSuggestion.size());
					BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//li//li)["+(sel+1)+"]")), driver);
					WebElement usethibtn = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+(sel+1)+"]"));
					usethibtn.click();
					Thread.sleep(4000);
					wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = deliverycheckoutstep.getAttribute("aria-label").contains("Current");
					
					if(steptwo == true)
					{
						log.add("The User is navigated to the Delivery Page.");
						if(deliveryOptionTitle.getText().equals(pagetitle))
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						Fail("The user is not in the Delivery Page.Please Check.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationMatchesTitle))
				{
					BNBasicfeature.scrolldown(addressSuggestionFirstAddress, driver);
					Random r = new Random();
					int sel = r.nextInt(addressVerficationPossibleSuggestion.size());
					BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')])["+(sel+1)+"]")), driver);
					/*WebElement radio = driver.findElement(By.xpath("(//*[@id='addrMatches']//*[@class='addRadio'])["+(sel+1)+"]"));
					radio.click();*/
					WebElement usethibtn = driver.findElement(By.xpath("(//*[contains(@id,'addrMatches')]//*[contains(@class,'continue')])["+(sel+1)+"]"));
					jsclick(usethibtn);
					wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = deliverycheckoutstep.getAttribute("aria-label").contains("Current");
					
					if(steptwo == true)
					{
						log.add("The User is navigated to the Delivery Page.");
						if(deliveryOptionTitle.getText().equals(pagetitle))
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						Fail("The user is not in the Delivery Page.Please Check.");
					}
				}
				else if(BNBasicfeature.isElementPresent(addressVerificationNoMatchesTitle))
				{
					WebElement nomatchesbtn = driver.findElement(By.xpath("//*[contains(@id,'asEntered')]"));
					jsclick(nomatchesbtn);
					Thread.sleep(4000);
					WebElement deliverycheckoutstep = driver.findElement(By.xpath("//*[@id='checkoutStepTwo']"));
					boolean steptwo = deliverycheckoutstep.getAttribute("aria-label").contains("Current");
					
					if(steptwo == true)
					{
						log.add("The User is navigated to the Delivery Page.");
						if(deliveryOptionTitle.getText().equals(pagetitle))
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
						}
						else
						{
							log.add("The expected page title was : " + pagetitle);
							log.add("The Current Page title is : " + deliveryOptionTitle.getText());
							Fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
						}
					}
					else
					{
						Fail("The user is not in the Delivery Page.Please Check.");
					}
				}
				else
				{
					Fail("The user is not redirected to the Address Verification Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 657 Issue ." + e.getMessage());
				Exception(" BRM - 657 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 631 Guest Checkout � Verify that subtotal (number of items) and the total amount should be displayed at the bottom of the details in every section ie (Shipping,Delivery,payment and Review)*/
	/* BRM - 633 Guest Checkout � Verify that Estimated shipping and the amount should be displayed below the subtotal in every section ie (Shipping,Delivery,payment and Review) */
	/* BRM - 657 Guest Checkout � Verify that on entering all the input fields and clicking on Submit order should proceed to the Delivery page. */
	/* BRM - 789 Guest Checkout � Verify that estimated tax and the amount is displayed below the estimated shipping.*/
	/* BRM - 790 Guest Checkout � Verify that ORDER TOTAL and its amount is displayed above the Submit order.*/
	public void guestCheckoutBottomSummarySectionDetails()
	{
		ChildCreation(" BRM 631 Verify that subtotal (number of items) and the total amount should be displayed at the bottom of the details in every section ie (Shipping,Delivery,payment and Review).");
		boolean BRM633S = true,BRM633D = true, BRM633P = true, BRM633R = true, BRM633SA = true, BRM633DA = true, BRM633PA = true, BRM633RA = true;
		boolean BRM789S = true,BRM789D = true, BRM789P = true, BRM789R = true, BRM789SA = true, BRM789DA = true, BRM789PA = true, BRM789RA = true;
		boolean BRM790S = true,BRM790D = true, BRM790P = true, BRM790R = true, BRM790SA = true, BRM790DA = true, BRM790PA = true, BRM790RA = true;
		boolean BRM657 = false;
		if(gcLoaded==true)
		{
			try
			{
				String[] chksteps = BNBasicfeature.getExcelVal("BRM631", sheet, 2).split("\n");
				String subtotallabelname = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 15);
				String subtotalvalue = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 16);
				String estimatedShiplabel = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 17);
				String estimatedshipvalue = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 18);
				String estimatedTaxlabel = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 19);
				String estimatedTaxvalue = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 20);
				String OrderTotallabel = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 21);
				String OrderTotalvalue = BNBasicfeature.getExcelVal("BRMProductSearch", sheet, 22);
				String creditcardholname = BNBasicfeature.getExcelVal("BRM631", sheet, 24);
				//String ccnum = BNBasicfeature.getExcelNumericVal("BRM631", sheet, 23);
				String csvnum = BNBasicfeature.getExcelNumericVal("BRM631", sheet, 25);
				String creditcardholemail= BNBasicfeature.getExcelVal("BRM631", sheet, 26);
				if(BNBasicfeature.isListElementPresent(checkoutStepsLists))
				{
					for(int j=1; j<=checkoutStepsLists.size(); j++)
					{
						ArrayList<String> labeldet = new ArrayList<>();
						ArrayList<String> valdet = new ArrayList<>();
						BNBasicfeature.scrollup(checkoutSteps, driver);
						WebElement chkoutstp = driver.findElement(By.xpath("(//*[@class='header-checkout']//a)["+j+"]"));
						String step = chkoutstp.getText();
						if(step.equals(chksteps[0]))
						{
							try
							{
								jsclick(chkoutstp);
								//Thread.sleep(2000);
								wait.until(ExpectedConditions.visibilityOf(guestCheckoutAddPageTitle));
								BNBasicfeature.scrolldown(contactNo, driver);
								if(BNBasicfeature.isElementPresent(orderSummaryContainer))
								{
									log.add("The Summary Container is found and it is displayed.");
									if(BNBasicfeature.isListElementPresent(orderSummaryLists))
									{
										log.add("The Summary Container List is found and it is displayed.");
										for(int k = 1; k<=orderSummaryLists.size();k++)
										{
											WebElement label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
											WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
											String labelTxt = label.getText();
											String detailsTxt = details.getText();
											if(k==1)
											{
												if(labelTxt.isEmpty())
												{
													Fail( "The Label is not displayed and the text is empty.");
												}
												else
												{
													Pass("The "+ labelTxt + " line is found and it is displayed.");
												}
												
												if(detailsTxt.isEmpty())
												{
													Fail( "The Amount is not displayed and the text is empty.");
												}
												else
												{
													Pass("The "+ detailsTxt + " line is found and it is displayed.");
												}
											}
											
											if(k==2)
											{
												if(labelTxt.isEmpty())
												{
													Fail( "The Label is not displayed and the text is empty.");
													BRM633S = false;
												}
												else
												{
													Pass("The "+ labelTxt + " line is found and it is displayed.");
												}
												
												if(detailsTxt.isEmpty())
												{
													Fail( "The Amount is not displayed and the text is empty.");
													BRM633SA = false;
												}
												else
												{
													Pass("The "+ detailsTxt + " line is found and it is displayed.");
												}
											}
											
											if(k==3)
											{
												if(labelTxt.isEmpty())
												{
													Fail( "The Label is not displayed and the text is empty.");
													BRM789S = false;
												}
												else
												{
													Pass("The "+ labelTxt + " line is found and it is displayed.");
												}
												
												if(detailsTxt.isEmpty())
												{
													Fail( "The Amount is not displayed and the text is empty.");
													BRM789SA = false;
												}
												else
												{
													Pass("The "+ detailsTxt + " line is found and it is displayed.");
												}
											}
											
											if(k==4)
											{
												if(labelTxt.isEmpty())
												{
													Fail( "The Label is not displayed and the text is empty.");
													BRM790S = false;
												}
												else
												{
													Pass("The "+ labelTxt + " line is found and it is displayed.");
												}
												
												if(detailsTxt.isEmpty())
												{
													Fail( "The Amount is not displayed and the text is empty.");
													BRM790SA = false;
												}
												else
												{
													Pass("The "+ detailsTxt + " line is found and it is displayed.");
												}
											}
											/*System.out.println(label.getText());
											System.out.println(details.getText());*/
											labeldet.add(labelTxt);
											valdet.add(detailsTxt);
										}
										
										String[] det = labeldet.toArray(new String[labeldet.size()]);
										String[] val = valdet.toArray(new String[valdet.size()]);
										valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
										jsclick(orderContinueBtn);
										//Thread.sleep(3000);
									}
									else
									{
										Fail("The Summary Conatainer List is not found.Please Check.");
									
									}
								}
								else
								{
									Fail("The Summary Conatainer is not found.Please Check.");
								
								}
							}
							catch(Exception e)
							{
								System.out.println(" BRM - 631 Issue ." + e.getMessage());
								Exception(" BRM - 631 Issue ." + e.getMessage());
							}
						}
						else if(step.equals(chksteps[1]))
						{
							try
							{
								wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
								if(BNBasicfeature.isElementPresent(deliveryOptionTitle))
								{
									BRM657 = true;
									BNBasicfeature.scrolldown(deliveryCartsItemContainer, driver);
									if(BNBasicfeature.isElementPresent(orderSummaryContainer))
									{
										log.add("The Summary Container is found and it is displayed.");
										if(BNBasicfeature.isListElementPresent(orderSummaryLists))
										{
											log.add("The Summary Container List is found and it is displayed.");
											for(int k = 1; k<=orderSummaryLists.size();k++)
											{
												WebElement label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
												WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
												String labelTxt = label.getText();
												String detailsTxt = details.getText();
												
												if(k==1)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==2)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM633D = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM633DA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==3)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM789D = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM789DA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==4)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM790D = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM790DA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												/*System.out.println(label.getText());
												System.out.println(details.getText());*/
												labeldet.add(labelTxt);
												valdet.add(detailsTxt);
											}
											String[] det = labeldet.toArray(new String[labeldet.size()]);
											String[] val = valdet.toArray(new String[valdet.size()]);
											valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
											jsclick(orderContinueBtn);
											//Thread.sleep(3000);
										}
										else
										{
											Fail("The Summary Conatainer List is not found.Please Check.");
										}
									}
									else
									{
										Fail("The Summary Conatainer is not found.Please Check.");
									}
								}
								else
								{
									Fail("The User is not navigated to the Delivery option Page.");
								}
							}
							catch(Exception e)
							{
								System.out.println(" BRM - 631 Issue ." + e.getMessage());
								Exception(" BRM - 631 Issue ." + e.getMessage());
							}
							
						}
						else if(step.equals(chksteps[2]))
						{
							try
							{
								//jsclick(orderContinueBtn);
								wait.until(ExpectedConditions.visibilityOf(guestCheckoutPaymentPageTitle));
								if(BNBasicfeature.isElementPresent(guestPaymentsOption))
								{
									BNBasicfeature.scrolldown(ccNumber, driver);
									ccNumber.sendKeys("4024007159422533");
									ccName.sendKeys(creditcardholname);
									Select mntsel = new Select(ccMonth);
									mntsel.selectByIndex(6);
									Select yrsel = new Select(ccYear);
									yrsel.selectByIndex(6);
									ccCsv.sendKeys(csvnum);
									
									BNBasicfeature.scrolldown(ccEmail, driver);
									ccEmail.clear();
									ccEmail.sendKeys(creditcardholemail);
									if(BNBasicfeature.isElementPresent(orderSummaryContainer))
									{
										BNBasicfeature.scrolldown(orderSummaryContainer, driver);
										log.add("The Summary Container is found and it is displayed.");
										if(BNBasicfeature.isListElementPresent(orderSummaryLists))
										{
											log.add("The Summary Container List is found and it is displayed.");
											for(int k = 1; k<=orderSummaryLists.size();k++)
											{
												WebElement label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
												WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
												String labelTxt = label.getText();
												String detailsTxt = details.getText();
	
												if(k==1)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==2)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM633P = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM633PA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==3)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM789P = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM789PA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==4)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM790P = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM790PA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												/*System.out.println(label.getText());
												System.out.println(details.getText());*/
												labeldet.add(labelTxt);
												valdet.add(detailsTxt);
											}
											
											String[] det = labeldet.toArray(new String[labeldet.size()]);
											String[] val = valdet.toArray(new String[valdet.size()]);
											valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
											jsclick(orderContinueBtn);
											//Thread.sleep(3000);
										}
										else
										{
											Fail("The Summary Conatainer List is not found.Please Check.");
										
										}
									}
									else
									{
										Fail("The Summary Conatainer is not found.Please Check.");
									
									}
								}
								else
								{
									Fail("The User is not navigated to the Payments Page and the Payments Option Tab is not found.");
								
								}
							}
							catch(Exception e)
							{
								Fail("The User is not navigated to the Delivery option Page.");
								System.out.println(e.getMessage());
							
							}
						}
						else if(step.equals(chksteps[3]))
						{
							try
							{
								//jsclick(orderContinueBtn);
								wait.until(ExpectedConditions.visibilityOf(reviewPagetitle));
								if(BNBasicfeature.isElementPresent(reviewPagetitle))
								{
									Pass("The user is navigated to the Review Order Page.");
									if(BNBasicfeature.isElementPresent(orderSummaryContainer))
									{
										BNBasicfeature.scrolldown(orderSummaryContainer, driver);
										Thread.sleep(1000);
										log.add("The Summary Container is found and it is displayed.");
										if(BNBasicfeature.isListElementPresent(orderSummaryLists))
										{
											log.add("The Summary Container List is found and it is displayed.");
											for(int k = 1; k<=orderSummaryLists.size();k++)
											{
												WebElement label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
												WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
												String labelTxt = label.getText();
												String detailsTxt = details.getText();
		
												if(k==1)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==2)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM633R = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM633RA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==3)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM789R = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM789RA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												if(k==4)
												{
													if(labelTxt.isEmpty())
													{
														Fail( "The Label is not displayed and the text is empty.");
														BRM790R = false;
													}
													else
													{
														Pass("The "+ labelTxt + " line is found and it is displayed.");
													}
													
													if(detailsTxt.isEmpty())
													{
														Fail( "The Amount is not displayed and the text is empty.");
														BRM790RA = false;
													}
													else
													{
														Pass("The "+ detailsTxt + " line is found and it is displayed.");
													}
												}
												
												labeldet.add(labelTxt);
												valdet.add(detailsTxt);
												
											}
											
											String[] det = labeldet.toArray(new String[labeldet.size()]);
											String[] val = valdet.toArray(new String[valdet.size()]);
											valcompare(det, val, subtotallabelname, subtotalvalue, estimatedShiplabel, estimatedshipvalue, estimatedTaxlabel, estimatedTaxvalue, OrderTotallabel, OrderTotalvalue);
											Thread.sleep(1000);
										}
										else
										{
											Fail("The Summary Conatainer List is not found.Please Check.");
										}
									}
									else
									{
										Fail("The Summary Conatainer is not found.Please Check.");
									}
								}
								else
								{
									Fail("The user is not navigated to the review Order page.");
								}
							}
							catch(Exception e)
							{
								System.out.println(" BRM - 631 Issue ." + e.getMessage());
								Exception(" BRM - 631 Issue ." + e.getMessage());
							}
						}
						else
						{
							Fail( "The Checklist Steps are not displayed or end of the checklist steps.");
						}
					}
				}
				else
				{
					Fail("The Checkout Steps is not found.Please Check.");
				
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 631 Issue ." + e.getMessage());
				Exception(" BRM - 631 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
		
		/* BRM - 633 Guest Checkout � Verify that Estimated shipping and the amount should be displayed below the subtotal in every section ie (Shipping,Delivery,payment and Review) */
		ChildCreation(" BRM - 633 Guest Checkout � Verify that Estimated shipping and the amount should be displayed below the subtotal in every section ie (Shipping,Delivery,payment and Review).");
		if(gcLoaded==true)
		{
			try
			{
				if((BRM633S==true)&&(BRM633SA==true)&&(BRM633D==true)&&(BRM633DA==true)&&(BRM633P==true)&&(BRM633PA==true)&&(BRM633R==true)&&(BRM633RA==true))
				{
					Pass( " The Estimated Shipping and the amount is displayed in all of the pages.");
				}
				else
				{
					Fail( " The Estimated Shipping and the amount is not displayed in one of the pages.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 633 Issue ." + e.getMessage());
				Exception(" BRM - 633 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");	
		}
		
		/* BRM - 657 Guest Checkout � Verify that on entering all the input fields and clicking on Submit order should proceed to the Delivery page. */
		ChildCreation(" BRM - 657 Guest Checkout � Verify that on entering all the input fields and clicking on Submit order should proceed to the Delivery page.");
		if(gcLoaded==true)
		{
			try
			{
				if(BRM657==true)
				{
					Pass( " The user is navigated to the Delivery page.");
				}
				else
				{
					Pass( " The user is not navigated to the Delivery page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 657 Issue ." + e.getMessage());
				Exception(" BRM - 657 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
		
		/* BRM - 789 Guest Checkout � Verify that estimated tax and the amount is displayed below the estimated shipping.*/
		ChildCreation(" BRM - 789 Guest Checkout � Verify that estimated tax and the amount is displayed below the estimated shipping.");
		if(gcLoaded==true)
		{
			try
			{
				if((BRM789S==true)&&(BRM789SA==true)&&(BRM789D==true)&&(BRM789DA==true)&&(BRM789P==true)&&(BRM789PA==true)&&(BRM789R==true)&&(BRM789RA==true))
				{
					Pass( " The Estimated Tax and the amount is displayed in all of the pages.");
				}
				else
				{
					Fail( " The Estimated Tax and the amount is not displayed in one of the pages.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 789 Issue ." + e.getMessage());
				Exception(" BRM - 789 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
		
		/* BRM - 790 Guest Checkout � Verify that ORDER TOTAL and its amount is displayed above the Submit order.*/
		ChildCreation(" BRM - 790 BRM - 790 Guest Checkout � Verify that ORDER TOTAL and its amount is displayed above the Submit order.");
		if(gcLoaded==true)
		{
			try
			{
				if((BRM790S==true)&&(BRM790SA==true)&&(BRM790D==true)&&(BRM790DA==true)&&(BRM790P==true)&&(BRM790PA==true)&&(BRM790R==true)&&(BRM790RA==true))
				{
					Pass( " The Order Total and the amount is displayed in all of the pages.");
				}
				else
				{
					Fail( " The Order Total and the amount is not displayed in one of the pages.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 790 Issue ." + e.getMessage());
				Exception(" BRM - 790 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 656 Guest Checkout � Verify that Shipping,delivery,payment and review tabs(breadcrumb ) are highlighted till shipping*/
	public void guestCheckoutBreadcrumHighlight()
	{
		ChildCreation(" BRM - 656 Verify that Shipping,delivery,payment and review tabs(breadcrumb ) are highlighted till shipping.");
		if(gcLoaded==true)
		{
			try
			{
				String color = BNBasicfeature.getExcelVal("BRM656", sheet, 3);
				wait.until(ExpectedConditions.visibilityOf(reviewPagetitle));
				if(BNBasicfeature.isListElementPresent(checkoutStepsLists))
				{
					BNBasicfeature.scrollup(checkoutSteps, driver);
					String[] script = new String[4];
					String[] hexCode = new String[script.length];
					//String[] breadcrumtitle = new String[script.length]; UnComment
					log.add("The Checkout Steps are displayed.");
					script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('background')";
					script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('background')";
					script[2] = "return window.getComputedStyle(document.querySelector('#checkoutStepThree'),':before').getPropertyValue('background')";
					script[3] = "return window.getComputedStyle(document.querySelector('#checkoutStepFour'),':before').getPropertyValue('background')";
					
					
					for(int j=0; j<script.length-1; j++)
					{
						WebElement ele = driver.findElement(By.xpath("(//*[@class='header-checkout']//li)["+(j+1)+"]"));
						/*String elecolor = ele.getCssValue("color"); Un Comment
						Color colorhxcnvt = Color.fromString(elecolor);
						breadcrumtitle[j] = colorhxcnvt.asHex();*/
						/*JavascriptExecutor js = (JavascriptExecutor)driver;
						String content = (String) js.executeScript(script[j]);
						String split = content.substring(0,16);
						Color colorhxcnvt1 = Color.fromString(split);
						hexCode[j] = colorhxcnvt1.asHex();*/
						String elecolor = ele.getCssValue("border-bottom").substring(9, 25);
						Color colorhxcnvt = Color.fromString(elecolor);
						String actCol = colorhxcnvt.asHex();
						if(color.equals(actCol))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						
						/*if(color.equals(breadcrumtitle[j]))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
							Pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + breadcrumtitle[j]);
							Fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}*/
					}
				}
				else
				{
					Fail("The Checkout Steps are not dispalyed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 656 Issue ." + e.getMessage());
				Exception(" BRM - 656 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}

	/* BRM - 550 Verify that user should be able to click the "Other Ways to Pay" option in the checkout page */
	public void guestCheckoutOtherwaysToPay()
	{
		ChildCreation(" BRM - 550 Verify that user should be able to click the Other Ways to Pay option in the checkout page.");
		if(gcLoaded==true)
		{
			try
			{
				String name = BNBasicfeature.getExcelVal("BRM550", sheet, 2);
				String[] caption = name.split("\n");
				wait.until(ExpectedConditions.visibilityOf(paymentCheckoutStep));
				//WebElement paymentCheckoutStep = driver.findElement(By.xpath("//*[@id='checkoutStepThree']"));
				jsclick(paymentCheckoutStep);
				//Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(guestCheckoutPaymentPageTitle));
				if(guestCheckoutPaymentPageTitle.getText().equals(caption[0]))
				{
					log.add("The user is navigated to the Payment Page in the Guest Checkout Page.");
					wait.until(ExpectedConditions.visibilityOf(otherPayOptionNotActive));
					if(BNBasicfeature.isElementPresent(otherPayOptionNotActive))
					{
						Pass("The Other Pay Option tab is found.");
						if(otherPayOptionNotActive.getText().equals(caption[1]))
						{
							Pass("The Other Pay Option caption matches the expected content.");
						}
						else
						{
							Fail("The Other Pay Option caption does not matches the expected content.");
						}
					}
					else
					{
						Fail("The Other Pay Option tab is not found.");
					}
				}
				else
				{
					Fail("The user is not in the Payments Page of the Guest Checkout.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 550 Issue ." + e.getMessage());
				Exception(" BRM - 550 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 551 Verify that user should be able to click the "Other Ways to Pay" option in the checkout page */
	public void guestCheckoutOtherwaysToPayNavigation()
	{
		ChildCreation(" BRM - 551 Verify that user should be able to click the Other Ways to Pay option in the checkout page.");
		if(gcLoaded==true)
		{
			try
			{
				String name = BNBasicfeature.getExcelVal("BRM551", sheet, 2);
				String[] caption = name.split("\n");
				if(guestCheckoutPaymentPageTitle.getText().equals(caption[0]))
				{
					log.add("The user is navigated to the Payment Page in the Guest Checkout Page.");
					if(BNBasicfeature.isElementPresent(otherPayOptionNotActive))
					{
						log.add("The Other Pay Option tab is found.");
						jsclick(otherPayOptionNotActive);
						wait.until(ExpectedConditions.visibilityOf(guestPaypalActiveTab));
						if(BNBasicfeature.isElementPresent(guestPaypalActiveTab))
						{
							Pass("The Other Pay Option content is displayed",log);
						}
						else
						{
							Fail("The Other Pay Option content is not displayed.",log);
						}
					}
					else
					{
						Fail("The Other Pay Option tab is not found.",log);
					}
				}
				else
				{
					Fail("The user is not in the Payments Page of the Guest Checkout.",log);
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 551 Issue ." + e.getMessage());
				Exception(" BRM - 551 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
	
	/* BRM - 549 Verify that "other ways to pay - paypal" option should be displayed as per the creative.*/
	public void guestCheckoutOtherwaysToPayNavigationDet()
	{
		ChildCreation(" BRM - 549 Verify that other ways to pay - paypal option should be displayed as per the creative.");
		if(gcLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM549", sheet, 3);
				if(BNBasicfeature.isElementPresent(guestCCPaymentNotActiveTab))
				{
					if(BNBasicfeature.isElementPresent(guestPaypalActiveTab))
					{
						Pass("The Other Pay Option content is displayed",log);
					}
					else
					{
						Fail("The Other Pay Option content is not displayed.",log);
					}
					
					if(BNBasicfeature.isElementPresent(guestPaypalButton))
					{
						Pass("The Paypal button is displayed.");
					}
					else
					{
						Fail("The Paypal button is not displayed.");
					}
					
					String ColorName = guestPaypalActiveTab.getCssValue("color");
					Color colorhxcnvt = Color.fromString(ColorName);
					String hexCode = colorhxcnvt.asHex();
					log.add("The Expected Color was : " + expColor);
					log.add("The Actual Color is : " + hexCode);
					if(expColor.equals(hexCode))
					{
						Pass("The Tab is highlighted with the expected color.",log);
					}
					else
					{
						Fail("The Tab is not highlighted with the expected color.",log);
					}
				}
				else
				{
					Fail("The Gusest other payment Tab is not active");
				}
			}
			catch (Exception e)
			{
				Exception("There is something wrong.Please Check." + e.getMessage());
				System.out.println(e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}
		

	/* BRM - 1237 verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address */
	public void guestCheckoutTabKeyUse()
	{
		ChildCreation(" BRM - 1237 Verify that clicking on tab in virtual keypad , the cursor moves displaying the next field while filling the shipping address .");
		if(gcLoaded==true)
		{
			try
			{
				BNBasicfeature.scrollup(checkoutSteps, driver);
				Actions act = new Actions(driver);
				act.moveToElement(fName).click().sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the First Name field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Last Name field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Street Address field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Apt Suite field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the City field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the State field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Zipcode field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Contact Number field.");
				act.sendKeys(Keys.TAB).build().perform();
				Pass("The User is navigated to the Company field field.");
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 1237 Issue ." + e.getMessage());
				Exception(" BRM - 1237 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip(" The User failed to load the Guest Checkout Page.");
		}
	}	
	
	/**************************************** BRM - 296 Guest Delivery Options **********************************************/
	
	/* BRM - 658 Guest Checkout - Verify that delivery section should be displayed as per the creative in the guest checkout page.*/
	public void guestDeliveryOptions()
	{
		ChildCreation(" BRM - 658 Guest Checkout - Verify that delivery section should be displayed as per the creative in the guest checkout page.");
		try
		{
			String pagetitle = BNBasicfeature.getExcelNumericVal("BRM631", sheet, 2);
			wait.until(ExpectedConditions.visibilityOf(deliveryCheckoutStep));
			jsclick(deliveryCheckoutStep);
			wait.until(ExpectedConditions.attributeContains(deliveryCheckoutStep, "aria-label", "Current"));
			wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
			Thread.sleep(500);
			boolean steptwo = deliveryCheckoutStep.getAttribute("aria-label").contains("Current");
			if(steptwo == false)
			{
				Fail("The user is not in the Delivery Page.Please Check.");
			}
			else
			{
				delOpt = true;
				log.add("The User is navigated to the Delivery Page.");
				if(deliveryOptionTitle.getText().equals(pagetitle))
				{
					log.add("The expected page title was : " + pagetitle);
					log.add("The Current Page title is : " + deliveryOptionTitle.getText());
					Pass("The user is in the Delivery Page and the Page title matches the expected One.",log);
				}
				else
				{
					log.add("The expected page title was : " + pagetitle);
					log.add("The Current Page title is : " + deliveryOptionTitle.getText());
					Fail("The user is in the Delivery Page.But there is mismatch in the page title.",log);
				}
				
				if(BNBasicfeature.isElementPresent(deliveryCartItemsContainer))
				{
					Pass( "The Delivery Cart Container is displayed.");
				}
				else
				{
					Fail( "The Delivery Cart Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(deliveryCartItemsContainer))
				{
					Pass( "The Delivery Cart Container is displayed.");
				}
				else
				{
					Fail( "The Delivery Cart Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(deliveryPreferenceTitle))
				{
					Pass("The Delivery Preference title is found. The displayed title is  : " + deliveryPreferenceTitle.getText());
				}
				else
				{
					Fail("The Delivery Preference title is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(deliveryOptionShipMethods))
				{
					Pass("The Delivery Option Ship Methods is found.");
				}
				else
				{
					Fail("The Delivery Option Ship Methods is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(deliverySpeedTitle))
				{
					Pass("The Delivery Speed title is found.The displayed text is : " + deliverySpeedTitle.getText());
				}
				else
				{
					Fail("The Delivery Speed title is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(deliveryOptionMakeitasGift))
				{
					Pass("The Make it as Gift Option is displayed.");
				}
				else
				{
					Fail("The Make it as Gift Option is not displayed.");
				}
				
				if(BNBasicfeature.isListElementPresent(makeItAGiftTitle))
				{
					for(int i = 0; i<makeItAGiftTitle.size(); i++)
					{
						Pass("The Make It A Gift is found.The displayed text is : " + makeItAGiftTitle.get(i).getText());
					}
				}
				else
				{
					Fail("The Make It A Gift is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(deliveryOptionProductEdit))
				{
					for(int i = 0; i<deliveryOptionProductEdit.size(); i++)
					{
						Pass("The Edit title is found.The displayed text is : " + deliveryOptionProductEdit.get(i).getText());
					}
				}
				else
				{
					Fail("The Edit link is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderContinueBtn))
				{
					Pass("The Order Continue button is displayed.");
				}
				else
				{
					Fail("The Order Continue button is not displayed.");
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 658 Issue ." + e.getMessage());
			Exception(" BRM - 658 Issue ." + e.getMessage());
		}
	}
	
	/* BRM - 659 Guest Checkout - Verify that delivery section should allow the users to enter the required delivery details in the guest checkout page.*/
	public void guestCheckoutDeliveryPage()
	{
		ChildCreation("BRM - 659 Guest Checkout - Verify that delivery section should allow the users to enter the required delivery details in the guest checkout page.");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM659", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(deliveryOptionContainer))
				{
					Pass("The Delivery Option container is found.");
				}
				else
				{
					Fail("The Delivery Option container is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(deliveryPreferenceTitle))
				{
					if((deliveryPreferenceTitle.getText().isEmpty())&&(!deliveryPreferenceTitle.getText().contains(Val[0])))
					{
						Fail("The Delivery Preference title is not found / empty.");
					}
					else
					{
						Pass("The Delivery Preference title is found. The displayed title is  : " + deliveryPreferenceTitle.getText());
					}
				}
				else
				{
					Fail("The Delivery Preference title is not found.");
				}
				
				
				if(BNBasicfeature.isElementPresent(deliveryOptionShipMethods))
				{
					Pass("The Delivery Option Ship Methods is found.");
				}
				else
				{
					Fail("The Delivery Option Ship Methods is not found.");
				}
				
				if(BNBasicfeature.isElementPresent(deliverySpeedTitle))
				{
					if((deliverySpeedTitle.getText().isEmpty())&&(!deliverySpeedTitle.getText().contains(Val[1])))
					{
						Fail("The Delivery Speed title is not found / empty.");
					}
					else
					{
						Pass("The Delivery Speed title is found.The displayed text is : " + deliverySpeedTitle.getText());
					}
				}
				else
				{
					Fail("The Delivery Speed title is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(deliveryOptionMakeitasGift))
				{
					Pass("The Make it as Gift Option is displayed.");
				}
				else
				{
					Fail("The Make it as Gift Option is not displayed.");
				}
				
				if(BNBasicfeature.isListElementPresent(makeItAGiftTitle))
				{
					for(int i = 0; i<makeItAGiftTitle.size(); i++)
					{
						String txt = makeItAGiftTitle.get(i).getText(); 
						if((txt.isEmpty())&&(!txt.contains(Val[2])))
						{
							Fail("The Make It A Gift title is not found / empty.");
						}
						else
						{
							Pass("The Make It A Gift is found.The displayed text is : " + txt);
						}
					}
				}
				else
				{
					Fail("The Delivery Speed title is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(deliveryOptionProductEdit))
				{
					for(int i = 0; i<deliveryOptionProductEdit.size();i++)
					{
						String txt = deliveryOptionProductEdit.get(i).getText(); 
						if((txt.isEmpty())&&(!txt.contains(Val[3])))
						{
							Fail("The Edit title is not found / empty.");
						}
						else
						{
							Pass("The Edit title is found.The displayed text is : " + txt);
						}
					}
				}
				else
				{
					Fail("The Edit link is not displayed.");
				}
				
				BNBasicfeature.scrolldown(orderSummaryContainer, driver);
				
				if(BNBasicfeature.isElementPresent(orderContinueBtn))
				{
					Pass("The Order Continue button is displayed.");
				}
				else
				{
					Fail("The Order Continue button is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 659 Issue ." + e.getMessage());
				Exception(" BRM - 659 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 660 Guest Checkout - Verify that subtotal (number of items) and the total amount should be displayed below the "Delivery Details" section.*/
	public void guestCheckoutDeliveryPageSummaryContainer()
	{
		ChildCreation("BRM - 660 Guest Checkout - Verify that subtotal (number of items) and the total amount should be displayed below the Delivery Details section.");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM660", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					if(BNBasicfeature.isListElementPresent(orderSummaryLists))
					{
						Pass("The Summary Container list is displayed.");
						for(int i=1;i<=orderSummaryLists.size();i++)
						{
							WebElement label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+i+"]//*[@class])[1]"));
							WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+i+"]//*[@class])[2]"));
							//System.out.println(details.getText());
							if((label.getText().isEmpty())&&(!label.getText().contains(Val[i])))
							{
								Fail("The " + label.getText() + " line is found and it is empty.");
							}
							else
							{
								Pass("The " + label.getText() + " line is found and it is displayed.");
							}
							
							if(details.getText().isEmpty())
							{
								Fail("The " + details.getText() + " line is found and it is empty.");
							}
							else
							{
								Pass("The " + details.getText() + " line is found and it is displayed.");
							}
						}
					}
					else
					{
						Fail("The Summary Container list is not displayed.");
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 660 Issue ." + e.getMessage());
				Exception(" BRM - 660 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 662 Guest Checkout - Verify that Ships from Barnes & Noble section has two options: Send everything in as few packages as possible(required for free shipping), Send each item as it is available (at additional cost).*/
	public void guestCheckoutDeliveryPreferenceOptions()
	{
		ChildCreation("BRM - 662 Guest Checkout - Verify that Ships from Barnes & Noble section has two options: Send everything in as few packages as possible(required for free shipping), Send each item as it is available (at additional cost).");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM661", sheet, 23);
				String[] delOptions = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(deliveryOptionContainer))
				{
					Pass("The Delivery Option Container is displayed.");
					BNBasicfeature.scrollup(checkoutSteps, driver);
					if(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryPreference))
					{
						Pass("The Delivery Preference is displayed.");
						for(int i = 0;i<deliveryOptionDeliveryPreference.size();i++)
						{
							String delPref = deliveryOptionDeliveryPreference.get(i).getText();
							String expectedText = delOptions[i];
							log.add("The Expected text was : " + expectedText);
							if(expectedText.contains(delPref))
							{
								Pass("The expected delivery preference text is displayed.",log);
							}
							else
							{
								Fail("The expected delivery preference text is not displayed.",log);
							}
						}
					}
					else
					{
						Fail("The Delivery Preference is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 662 Issue ." + e.getMessage());
				Exception(" BRM - 662 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 663 Guest Checkout - Verify that �Send everything in as few packages as possible(required for free shipping)� option is selected by default in Ships from Barnes & Noble section.*/
	public void guestCheckoutDeliveryPreferenceDefaultSelection()
	{
		ChildCreation("BRM - 663 Guest Checkout - Verify that �Send everything in as few packages as possible(required for free shipping)� option is selected by default in Ships from Barnes & Noble section.");
		if(delOpt==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryPreference))
				{
					Pass("The Delivery Preference is displayed.");
					if(BNBasicfeature.isElementPresent(deliveryOptionDeliverySlow))
					{
						Pass("The Delivery Option slow is displayed.");
						
						boolean chkd = deliveryOptionDeliverySlow.getAttribute("aria-checked").contains("true");
						if(chkd == true)
						{
							Pass("The default slow preference is selected by default.The Selected default option is : " + deliveryOptionDeliveryPreference.get(0).getText());
						}
						else
						{
							Fail("The default slow preference is not selected.");
						}
					}
					else
					{
						Fail("The Delivery Option slow is not displayed.");
					}
				}
				else
				{
					Fail("The Delivery Preference is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 663 Issue ." + e.getMessage());
				Exception(" BRM - 663 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 940 Guest Checkout - Verify whether user can able to select any one of the options in Ships from Barnes & Noble section and the selected option should be highlighted.*/
	public void guestCheckoutDeliveryPreferenceHighlight()
	{
		ChildCreation("BRM - 940 Guest Checkout - Verify whether user can able to select any one of the options in Ships from Barnes & Noble section and the selected option should be highlighted.");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM940", sheet, 3);
				if(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryPreference))
				{
					Pass("The Delivery Preference is displayed.");
					if(BNBasicfeature.isListElementPresent(deliveryOptionDeliverySelect))
					{
						for(int i = 0; i<deliveryOptionDeliverySelect.size();i++)
						{
							WebElement radio = deliveryOptionDeliverySelect.get(i);
							jsclick(radio);
							wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
							//Thread.sleep(3000);
							wait.until(ExpectedConditions.visibilityOf(deliveryOptionContainer));
							WebElement label = deliveryOptionDeliveryPreference.get(i);
							//System.out.println(label.getText());
							String cssVal = label.getCssValue("color");
							Color colorhxcnvt = Color.fromString(cssVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color was : " + cellVal);
							log.add("The Actual Color is : " + hexCode);
							if(hexCode.contains(cellVal))
							{
								Pass("The delivery preference " + label.getText() + " is highlighted with the expected color.",log);
							}
							else
							{
								Fail("The delivery preference " + label.getText() + " is not highlighted with the expected color.",log);
							}
						}
						jsclick(deliveryOptionDeliverySelect.get(0));
						wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
					}
					else
					{
						Fail("The Delivery Option slow is not displayed.");
					}
				}
				else
				{
					Fail("The Delivery Preference is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 940 Issue ." + e.getMessage());
				Exception(" BRM - 940 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 664 Guest Checkout - Verify that Choose delivery speed section has three options: Standard delivery $3.99 2 to 6 business days, Express Delivery $8.94 Arrives by Weekday, month name date, Expedited Delivery $18.94 Arrives by Weekday, month name date.*/
	public void guestCheckoutDeliveryShippingMethods()
	{
		ChildCreation("BRM - 664 Guest Checkout - Verify that Choose delivery speed section has three options: Standard delivery $3.99 2 to 6 business days, Express Delivery $8.94 Arrives by Weekday, month name date, Expedited Delivery $18.94 Arrives by Weekday, month name date.");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM664", sheet, 24);
				String[] shipMeth = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(deliveryOptionShipMethodsLabel))
				{
					Pass("The Shipping Methods label is displayed.");
					for(int i = 0; i<deliveryOptionShipMethodsLabel.size();i++)
					{
						String actlabel = deliveryOptionShipMethodsLabel.get(i).getText();
						String explabel = shipMeth[i];
						log.add("The Expected label was : " + shipMeth[i]);
						log.add("The Actual label was : " + actlabel);
						if(actlabel.contains(explabel))
						{
							Pass("The Expected label is found.",log);
						}
						else
						{
							Fail("The Expected label is not found.",log);
						}
						
						String actArriveBy = deliveryOptionShipMethodsLabel.get(i).getText();
						String expArriveBy = shipMeth[3];
						log.add("The Expected label was : " + shipMeth[3]);
						log.add("The Actual label was : " + actArriveBy);
						if(actArriveBy.contains(expArriveBy))
						{
							Pass("The Expected Arrive By text is found.",log);
						}
						else
						{
							Fail("The Expected Arrive By is not found.",log);
						}
					}
				}
				else
				{
					Fail("The Shipping Methods label is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 664 Issue ." + e.getMessage());
				Exception(" BRM - 664 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 665 Guest Checkout - Verify that �Standard delivery $3.99 2 to 6 business days� is selected by default in choose delivery speed option in the guest checkout page.*/
	public void guestCheckoutDeliveryShippingMethodsDefaultSelection()
	{
		ChildCreation("BRM - 665 Guest Checkout - Verify that �Standard delivery $3.99 2 to 6 business days� is selected by default in choose delivery speed option in the guest checkout page.");
		if(delOpt==true)
		{
			try
			{
				boolean chkd = false;
				int i;
				String cellVal = BNBasicfeature.getExcelVal("BRM665", sheet, 24);
				if(BNBasicfeature.isListElementPresent(deliveryOptionDeliveryMethod))
				{
					Pass("The Delivery Methods is displayed.");
					Thread.sleep(250);
					for(i = 0;i<deliveryOptionDeliveryMethod.size();i++)
					{
						boolean selected = deliveryOptionDeliveryMethod.get(i).isSelected();
						if(selected==true)
						{
							chkd = true;
							break;
						}
						else
						{
							chkd = false;
							continue;
						}
					}
					
					if(chkd==true)
					{
						String deloptionText = deliveryOptionShipMethodsLabel.get(i).getText();
						log.add("The Default selection expected label content was : " + cellVal);
						log.add("The Default selection actual label content is : " + deloptionText);
						if(deloptionText.contains(cellVal))
						{
							Pass("The Default Selection of the delivery method is as expected.",log);
						}
						else
						{
							Fail("The Default Selection of the delivery method is not as expected.",log);
						}
					}
					else
					{
						Fail("No Delivery Method is selected.");
					}
				}
				else
				{
					Fail("The Delivery Methods is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 665 Issue ." + e.getMessage());
				Exception(" BRM - 665 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 941 Guest Checkout - Verify whether we can able to select any one of the options in Choose delivery speed section and the selected option should be highlighted.*/
	public void guestCheckoutShippingMethodsHighlight()
	{
		ChildCreation("BRM - 941 Guest Checkout - Verify whether we can able to select any one of the options in Choose delivery speed section and the selected option should be highlighted.");
		if(delOpt==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM941", sheet, 3);
				if(BNBasicfeature.isListElementPresent(deliveryOptionShipMethodsLabel))
				{
					Pass("The Shipping Method is displayed.");
					if(BNBasicfeature.isListElementPresent(deliveryOptionShippingSelect))
					{
						//int sel = BNBasicfeature.RandomNumGen(deliveryOptionDeliverySelect, log);
						Random r = new Random();
						int sel = r.nextInt(deliveryOptionDeliverySelect.size());
						WebElement radio = deliveryOptionShippingSelect.get(sel);
						jsclick(radio);
						//Thread.sleep(2000);
						wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
						wait.until(ExpectedConditions.visibilityOf(deliveryOptionContainer));
						//WebElement label = deliveryOptionContainerText.get(sel);
						WebElement label = deliveryOptionShipMethodsLabel.get(sel);
						String cssVal = label.getCssValue("color");
						Color colorhxcnvt = Color.fromString(cssVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color was : " + cellVal);
						log.add("The Actual Color is : " + hexCode);
						if(hexCode.contains(cellVal))
						{
							Pass("The Shipping Method is highlighted with the expected color.",log);
						}
						else
						{
							Fail("The Shipping Method is not highlighted with the expected color.",log);
						}
					}
					else
					{
						Fail("The Shipping Method selection option is not displayed.");
					}
				}
				else
				{
					Fail("The delivery option shipping methods is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 941 Issue ." + e.getMessage());
				Exception(" BRM - 941 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 666 Guest Checkout - Verify that product image, product name, author name, price, qty and make it as a gift are displayed in Ships from Barnes & Noble section in the delivery tab.*/
	public void guestCheckoutCartItemDetails()
	{
		ChildCreation("BRM - 666 Guest Checkout - Verify that product image, product name, author name, price, qty and make it as a gift are displayed in Ships from Barnes & Noble section in the delivery tab.");
		if(delOpt==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(deliveryOptionCartItemContainer))
				{
					Pass("The Cart Item Container is displayed.");
					BNBasicfeature.scrolldown(deliveryOptionCartItemContainer, driver);
					for(int i = 0;i<deliveryOptionCartItemsImage.size();i++)
					{
						WebElement img = deliveryOptionCartItemsImage.get(i);
						int imgResp = BNBasicfeature.imageBroken(img, log);
						if(imgResp==200)
						{
							Pass("The Image is not broken.",log);
						}
						else
						{
							Fail("The Image is broken.",log);
						}
						
						WebElement title = deliveryOptionCartItemsProductTitle.get(i);
						String prdtTitle = title.getText();
						if(prdtTitle.isEmpty())
						{
							Fail("The Product title is displayed. But the displayed title is empty.");
						}
						else
						{
							Pass("The Product title is displayed. The displayed title is : " + prdtTitle);
						}
						
						WebElement author = deliveryOptionCartItemsProductAuthor.get(i);
						String prdtAuthor = author.getText();
						if(prdtAuthor.isEmpty())
						{
							Fail("The Product author is displayed. But the displayed author is empty.");
						}
						else
						{
							Pass("The Product Author is displayed. The displayed author name is : " + prdtAuthor);
						}
						
						WebElement price = deliveryOptionCartItemsProductPrice.get(i);
						Float prdtPrice = Float.parseFloat(price.getText().replace("$", ""));
						if(prdtPrice>0.0)
						{
							Pass("The Product price is displayed. The displayed price is : " + prdtPrice);
						}
						else
						{
							Fail("The Product price is displayed. But the displayed price is empty.");
						}
						
						WebElement qty = deliveryOptionCartItemsProductQty.get(i);
						String prdtQuantity = qty.getText();
						if(prdtQuantity.isEmpty())
						{
							Fail("The Product quantity is displayed. But the displayed quantity is empty.");
						}
						else
						{
							Pass("The Product quantity is displayed. The displayed quantity is : " + prdtQuantity);
						}
					}
					

					for(int i = 0 ;i<deliveryOptionMakeitasGift.size();i++)
					{
						WebElement giftElement = deliveryOptionMakeitasGift.get(i);
						if(BNBasicfeature.isElementPresent(giftElement))
						{
							Pass("The gift section is displayed.");
						}
						else
						{
							Fail("The gift section is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Cart Item Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 666 Issue ." + e.getMessage());
				Exception(" BRM - 666 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 671 Guest Checkout - Verify that Order total value should be updated properly as per the delivery options selected.*/
	public void guestCheckoutSummaryContainerPriceDetails()
	{
		ChildCreation("BRM - 671 Guest Checkout - Verify that Order total value should be updated properly as per the delivery options selected.");
		if(delOpt==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Float originalVal = null,currVal = null;
					Pass("The Summary Container is displayed.");
					if(BNBasicfeature.isListElementPresent(deliveryOptionDeliverySelect))
					{
						Pass("The Delivery preference list is displayed.");
						for(int i = 0; i<deliveryOptionShippingSelect.size();i++)
						{
							WebElement delOption = deliveryOptionShippingSelect.get(i);
							jsclick(delOption);
							wait.until(ExpectedConditions.visibilityOf(deliveryOptionTitle));
							//Thread.sleep(2000);
							BNBasicfeature.scrolldown(orderSummaryContainer, driver);
							if(BNBasicfeature.isElementPresent(deliveryOptionCartTotalPrice))
							{
								Pass("The Total Price container is displayed.");
								String Price = deliveryOptionCartTotalPrice.getText().replace("$", "");
								currVal = Float.parseFloat(Price);
							}
							else
							{
								Fail("The Total Price container is not displayed.");
							}
							
							if(currVal.equals(originalVal))
							{
								Fail("The Value are same.");
							}
							else
							{
								Pass("The Value are changed based on the delivery preference.The current Value in the Total Price is : " + currVal);
								originalVal = currVal;
							}
						}
					}
					else
					{
						Fail("The Delivery preference list is not displayed.");
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 671 Issue ." + e.getMessage());
				Exception(" BRM - 671 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 791 Guest Checkout - Verify that estimated tax and the amount is displayed below the estimated shipping.*/
	public void guestCheckoutDeliveryEstimatedTax()
	{
		ChildCreation("BRM - 791 Guest Checkout - Verify that estimated tax and the amount is displayed below the estimated shipping.");
		if(delOpt==true)
		{
			try
			{
				String estimatedTaxlabel = sheet.getRow(0).getCell(19).getStringCellValue();
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					if(BNBasicfeature.isListElementPresent(orderSummaryLists))
					{
						log.add("The Summary Container List is found and it is displayed.");
						boolean fieldFound = false;
						WebElement label = null,details = null; 
						for(int k = 1; k<=orderSummaryLists.size();k++)
						{
							label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
							details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
							if(label.getText().contains(estimatedTaxlabel))
							{
								fieldFound=true;
								break;
							}
							else
							{
								fieldFound=false;
								continue;
							}
						}
						
						if(fieldFound==true)
						{
							Pass("The " + label.getText() + " line is found and it is displayed.");
							if(details.getText().isEmpty())
							{
								Fail("The Estimated tax is not displayed.It is empty.");
							}
							else
							{
								Pass("The " + details.getText() + " line is found and it is displayed.");
							}
						}
						else
						{
							Fail("The estimated tax is not displayed.");
						}
					}
					else
					{
						Fail("The Summary Conatainer List is not found.Please Check.");
					
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 791 Issue ." + e.getMessage());
				Exception(" BRM - 791 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 661 Guest Checkout - Verify that Estimated shipping and the amount is displayed below the subtotal in the delivery section.*/
	public void guestCheckoutDeliverySummaryContainerEstimatedShipping()
	{
		ChildCreation("BRM - 661 Guest Checkout - Verify that Estimated shipping and the amount is displayed below the subtotal in the delivery section.");
		if(delOpt==true)
		{
			try
			{
				String shiplabel = BNBasicfeature.getExcelVal("BRM661", sheet, 2);
				if(BNBasicfeature.isListElementPresent(orderSummaryLists))
				{
					Pass("The Summary List is displayed.");
					boolean labelFound = false;
					WebElement label; 
					for(int i = 1;i<=orderSummaryLists.size();i++)
					{
						label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+i+"]//*[@class])[1]"));
						String labelName = label.getText();
						if(labelName.contains(shiplabel))
						{
							labelFound=true;
							break;
						}
						else
						{
							labelFound=false;
							continue;
						}
					}
					
					if(labelFound==true)
					{
						Pass("The expected label is found.");
					}
					else
					{
						Fail("The expected label is not found.");
					}
				}
				else
				{
					Fail("The Summary List is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 661 Issue ." + e.getMessage());
				Exception(" BRM - 661 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 792 Guest Checkout - Verify that ORDER TOTAL and its amount is displayed above the continue.*/
	public void guestCheckoutDeliveryOrderTotal()
	{
		ChildCreation("BRM - 792 Guest Checkout - Verify that ORDER TOTAL and its amount is displayed above the continue.");
		if(delOpt==true)
		{
			try
			{
				String OrderTotallabel = sheet.getRow(0).getCell(21).getStringCellValue();
				if(BNBasicfeature.isListElementPresent(orderSummaryLists))
				{
					log.add("The Summary Container List is found and it is displayed.");
					boolean fieldFound = false;
					WebElement label = null,details = null; 
					for(int k = 1; k<=orderSummaryLists.size();k++)
					{
						label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[1]"));
						details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])["+k+"]//*[@class])[2]"));
						//System.out.println(label.getText());
						//System.out.println(details.getText());
						if(label.getText().equalsIgnoreCase(OrderTotallabel))
						{
							fieldFound=true;
							break;
						}
						else
						{
							fieldFound=false;
							continue;
						}
					}
					
					if(fieldFound==true)
					{
						Pass("The " + label.getText() + " line is found and it is displayed.");
						if(details.getText().isEmpty())
						{
							Fail("The Order Total is not displayed.It is empty.");
						}
						else
						{
							Pass("The " + details.getText() + " line is found and it is displayed.");
						}
					}
					else
					{
						Fail("The Order Total is not displayed.");
					}
				}
				else
				{
					Fail("The Summary Conatainer List is not found.Please Check.");
				
				}
			}
			catch (Exception e) 
			{
				System.out.println(" BRM - 792 Issue ." + e.getMessage());
				Exception(" BRM - 792 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 679 Guest Checkout - Verify that tabs are highlighted till delivery and Shipping should be highlighted.*/
	public void guestCheckoutDeliveryBreadcrumHighlight()
	{
		ChildCreation(" BRM - 679 Guest Checkout - Verify that tabs are highlighted till delivery and Shipping should be highlighted.");
		if(delOpt==true)
		{
			try
			{
				String color = BNBasicfeature.getExcelVal("BRM679", sheet, 3);
				if(BNBasicfeature.isListElementPresent(checkoutStepsLists))
				{
					BNBasicfeature.scrollup(checkoutSteps, driver);
					String[] script = new String[2];
					String[] hexCode = new String[script.length];
					//String[] breadcrumtitle = new String[script.length];
					log.add("The Checkout Steps are displayed.");
					/*script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('background')";
					script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('background')";*/
					
					for(int j=0; j<script.length; j++)
					{
						WebElement ele = driver.findElement(By.xpath("(//*[@class='header-checkout']//li)["+(j+1)+"]"));
						/*String elecolor = ele.getCssValue("color"); Un Comment
						Color colorhxcnvt = Color.fromString(elecolor);
						breadcrumtitle[j] = colorhxcnvt.asHex();*/
						/*JavascriptExecutor js = (JavascriptExecutor)driver;
						String content = (String) js.executeScript(script[j]);
						String split = content.substring(0,16);
						Color colorhxcnvt1 = Color.fromString(split);
						hexCode[j] = colorhxcnvt1.asHex();*/
						String elecolor = ele.getCssValue("border-bottom").substring(9, 25);
						Color colorhxcnvt = Color.fromString(elecolor);
						String actCol = colorhxcnvt.asHex();
						
						if(color.equals(actCol))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
					}
				}
				else
				{
					Fail(" THe Checkout List steps is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 679 Issue ." + e.getMessage());
				Exception(" BRM - 679 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/* BRM - 680 Guest Checkout - Verify that on selecting all the options and clicking on continue should proceed to the Payment page.*/
	public void guestCheckoutPaymentsPageNavigation()
	{
		ChildCreation("BRM - 680 Guest Checkout - Verify that on selecting all the options and clicking on continue should proceed to the Payment page.");
		if(delOpt==true)
		{
			try 
			{
				BNBasicfeature.scrolldown(orderSummaryContainer, driver);
				jsclick(orderContinueBtn);
				wait.until(ExpectedConditions.visibilityOf(guestPaymentsOption));
				if(BNBasicfeature.isElementPresent(guestPaymentsOption))
				{
					Pass("The user is navigated to the payments page.");
				}
				else
				{
					Fail("The user is not navigated to the payments page.");
				}
			} 
			catch (Exception e) 
			{
				System.out.println(" BRM - 680 Issue ." + e.getMessage());
				Exception(" BRM - 680 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Delivery Option Page.");
		}
	}
	
	/********************************************** BRM - 301 Guest Coupon Container ***********************************************/
	
	/* BRM - 548 Verify that Membership,Gift Cards & Coupons options should be displayed below payment information for the Guest checkout.*/
	public void guestCheckooutCouponVerification(boolean val)
	{
		ChildCreation(" BRM - 548 Verify that Membership,Gift Cards & Coupons options should be displayed below payment information for the Guest checkout.");
		try
		{
			String couponTitle = BNBasicfeature.getExcelNumericVal("BRM548", sheet, 3);
			wait.until(ExpectedConditions.attributeContains(paymentCheckoutStep, "aria-label", "Current"));
			boolean stepthree = paymentCheckoutStep.getAttribute("aria-label").contains("Current");
			if(stepthree == true)
			{
				shipPage = true;
				log.add("The User is navigated to the Payment Option Page.");
				if(BNBasicfeature.isElementPresent(couponContainer))
				{
					Pass("The Coupon Container is found.");
					if(BNBasicfeature.isElementPresent(couponContainerTitle))
					{
						Pass( "The Loyalty Container title is displayed.");
						String txt = couponContainerTitle.getText();
						log.add( "The Expected title was : " + couponTitle);
						log.add( "The actual title is : " + txt);
						if(txt.equalsIgnoreCase(couponTitle))
						{
							Pass("The title matches the expected content .",log);
						}
						else
						{
							Fail("There is mismatch in the Title.",log);
						}
					}
					else
					{
						Fail( " The Loyalty Container title is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon Container is not found.");
				}
			}
			else
			{
				Fail("The user is not navigated to the Payments Page.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 548 Issue ." + e.getMessage());
			Exception(" BRM - 548 Issue ." + e.getMessage());
		}
	}
	
	/* BRM - 565 Verify that while selecting the "+" button in the "B&N Gift Cards,B&N Kids' Club" section,the text box for Card Number , PIn should be displayed with the default text and the apply button as per creative.*/
	public void guestCheckooutGiftCardFields()
	{
		ChildCreation("BRM - 565 Verify that while selecting the + button in the B&N Gift Cards,B&N Kids' Club section,the text box for Card Number , PIn should be displayed with the default text and the apply button as per creative.");
		if(shipPage==true)
		{
			try
			{
				BNBasicfeature.scrolldown(couponContainer, driver);
				String cellVal= BNBasicfeature.getExcelNumericVal("BRM553", sheet, 2);
				String expColor = BNBasicfeature.getExcelNumericVal("BRM553", sheet, 3);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					Pass("The Loyalty Container list is displayed.");
					if(BNBasicfeature.isElementPresent(gftCardImg))
					{
						Pass("The Gift Card Image is displayed and not broken.",log);
					}
					else
					{
						Fail("The Gift Card Image is not displayed and it is broken.",log);
					}
					
					if(BNBasicfeature.isElementPresent(gftCardTit))
					{
						String txt = gftCardTit.getText();
						if(txt.isEmpty())
						{
							Fail("The gift catd title is not displayd/empty.");
						}
						else
						{
							Pass("The Gift Card Title is displayed.");
							log.add("The expected title was : " + Val[1]);
							log.add("The actual title is : " + gftCardTit.getText());
							/*System.out.println(title);
							System.out.println(Val[1]);*/
							if(txt.contains(Val[1]))
							{
								Pass("The expected title and the actual title matches.",log);
							}
							else
							{
								Fail("The expected title and the actual title does not matches.",log);
							}
						}
					}
					else
					{
						Fail( "The Gift Card title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCard))
					{
						Pass("The Credit Card Number field in the Coupon Container is displayed.");
					}
					else
					{
						Fail("The Credit Card Number field in the Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCardPin))
					{
						Pass("The Credit Card Number Pin field in the Coupon Container is displayed.");
					}
					else
					{
						Fail("The Credit Card Number Pin field in the Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
					{
						Pass("The Apply button in Credit Card Section of the Coupon Container is displayed.");
						log.add("The Expected content was : " + Val[2]);
						String caption = couponCreditCardSubmit.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[2]))
						{
							Pass("The Submit button caption matches.",log);
						}
						else
						{
							Fail("The Submit button caption does not matches.");
						}
						
						String csVal = couponCreditCardSubmit.getCssValue("background").substring(0,16);;
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
 							Pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Apply button in Credit Card Section of the Coupon Container is not displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container list is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 565 Issue ." + e.getMessage());
				Exception(" BRM - 565 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 553 Verify that Membership,Gift Cards & Coupons options should be displayed as per the creative for registered checkout.*/
	public void guestCheckooutCouponFieldVerification() 
	{
		ChildCreation("BRM - 553 Verify that Membership,Gift Cards & Coupons options should be displayed as per the creative for registered checkout.");
		if(shipPage==true)
		{
			try
			{
				String cellVal= BNBasicfeature.getExcelNumericVal("BRM553", sheet, 2);
				String expColor = BNBasicfeature.getExcelNumericVal("BRM553", sheet, 3);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(couponContainer))
				{
					Pass("The Coupon Container is found.");
					if(BNBasicfeature.isElementPresent(couponContainerTitle))
					{
						log.add("The Expected title was : " + Val[0]);
						String title = couponContainerTitle.getText();
						log.add( "The Expected title was : " + title);
						if(Val[0].contains(title))
						{
							Pass("The title matches the expected content .",log);
						}
						else
						{
							Fail("There is mismatch in the Title.",log);
						}
					}
					else
					{
						Fail( " The Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
					{
						Pass("The Loyalty Container list is displayed.");
						if(BNBasicfeature.isElementPresent(gftCardImg))
						{
							Pass("The Gift Card Image is displayed and not broken.",log);
						}
						else
						{
							Fail("The Gift Card Image is not displayed and it is broken.",log);
						}
						
						if(BNBasicfeature.isElementPresent(gftCardTit))
						{
							String txt = gftCardTit.getText();
							if(txt.isEmpty())
							{
								Fail("The gift catd title is not displayd/empty.");
							}
							else
							{
								Pass("The Gift Card Title is displayed.");
								log.add("The expected title was : " + Val[1]);
								log.add("The actual title is : " + gftCardTit.getText());
								/*System.out.println(title);
								System.out.println(Val[1]);*/
								if(txt.contains(Val[1]))
								{
									Pass("The expected title and the actual title matches.",log);
								}
								else
								{
									Fail("The expected title and the actual title does not matches.",log);
								}
							}
						}
						else
						{
							Fail( "The Gift Card title is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCreditCard))
						{
							Pass("The Credit Card Number field in the Coupon Container is displayed.");
						}
						else
						{
							Fail("The Credit Card Number field in the Coupon Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCreditCardPin))
						{
							Pass("The Credit Card Number Pin field in the Coupon Container is displayed.");
						}
						else
						{
							Fail("The Credit Card Number Pin field in the Coupon Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
						{
							Pass("The Apply button in Credit Card Section of the Coupon Container is displayed.");
							log.add("The Expected content was : " + Val[2]);
							String caption = couponCreditCardSubmit.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[2]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCreditCardSubmit.getCssValue("background").substring(0, 16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Apply button in Credit Card Section of the Coupon Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeApplyContainer))
						{
							Pass("The Coupon Code text field is displayed.");
						}
						else
						{
							Fail("The Coupon Code text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							Pass("The Coupon Code Apply button is displayed.");
							String caption = couponCodeApplyButton.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[2]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCodeApplyButton.getCssValue("background").substring(0, 16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Apply button is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(bookfairApplyContainer))
						{
							Pass("The Book Fair ID Code text field is displayed.");
						}
						else
						{
							Fail("The Book Fair ID text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(bookfairApplyButton))
						{
							Pass("The Book Fair Apply button is displayed.");
							String caption = bookfairApplyButton.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[2]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = bookfairApplyButton.getCssValue("background").substring(0, 16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Apply button is not displayed.");
						}
						
					}
					else
					{
						Fail("The Loyalty Container list is not displayed.");
					}			
				}
				else
				{
					Fail("The Coupon Container is not found.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 553 Issue ." + e.getMessage());
				Exception(" BRM - 553 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 607 Verify that corresponding "Coupon" images should be displayed near the Membership,Gift Cards and Coupon section.*/
	public void guestCheckooutGiftCardImage()
	{
		ChildCreation("BRM - 607 Verify that corresponding Coupon images should be displayed near the Membership,Gift Cards and Coupon section.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					Pass("The Loyalty Container list is displayed.");
					if(BNBasicfeature.isElementPresent(gftCardImg))
					{
						Pass("The Gift Card Image is displayed and not broken.",log);
					}
					else
					{
						Fail("The Gift Card Image is not displayed and it is broken.",log);
					}
				}
				else
				{
					Fail("The Loyalty Container list is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 607 Issue ." + e.getMessage());
				Exception(" BRM - 607 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 559 Verify that Membership,Gift Cards & Coupons part should have the following options: "B&N Gift Card,B&N Kids Club Rewards Certificates", "Coupon Code" , "Bookfair ID" for the Guest User Checkout.*/
	public void guestCheckooutCouponFieldPresent()
	{
		ChildCreation("BRM - 559 Verify that Membership,Gift Cards & Coupons part should have the following options: B&N Gift Card, Coupon Code , Bookfair ID for the Guest User Checkout.");
		if(shipPage==true)
		{
			try
			{
				String cellVal= BNBasicfeature.getExcelNumericVal("BRM553", sheet, 2);
				String expColor = BNBasicfeature.getExcelNumericVal("BRM553", sheet, 3);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					Pass("The Loyalty Container list is displayed.");
					if(BNBasicfeature.isElementPresent(gftCardImg))
					{
						Pass("The Gift Card Image is displayed and not broken.",log);
					}
					else
					{
						Fail("The Gift Card Image is not displayed and it is broken.",log);
					}
					
					if(BNBasicfeature.isElementPresent(gftCardTit))
					{
						String txt = gftCardTit.getText();
						if(txt.isEmpty())
						{
							Fail("The gift catd title is not displayd/empty.");
						}
						else
						{
							Pass("The Gift Card Title is displayed.");
							log.add("The expected title was : " + Val[1]);
							log.add("The actual title is : " + gftCardTit.getText());
							/*System.out.println(title);
							System.out.println(Val[1]);*/
							if(txt.contains(Val[1]))
							{
								Pass("The expected title and the actual title matches.",log);
							}
							else
							{
								Fail("The expected title and the actual title does not matches.",log);
							}
						}
					}
					else
					{
						Fail( "The Gift Card title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCard))
					{
						Pass("The Credit Card Number field in the Coupon Container is displayed.");
					}
					else
					{
						Fail("The Credit Card Number field in the Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCardPin))
					{
						Pass("The Credit Card Number Pin field in the Coupon Container is displayed.");
					}
					else
					{
						Fail("The Credit Card Number Pin field in the Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
					{
						Pass("The Apply button in Credit Card Section of the Coupon Container is displayed.");
						log.add("The Expected content was : " + Val[2]);
						String caption = couponCreditCardSubmit.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[2]))
						{
							Pass("The Submit button caption matches.",log);
						}
						else
						{
							Fail("The Submit button caption does not matches.",log);
						}
						
						String csVal = couponCreditCardSubmit.getCssValue("background").substring(0, 16);
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							Pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Apply button in Credit Card Section of the Coupon Container is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCodeApplyContainer))
					{
						Pass("The Coupon Code text field is displayed.");
					}
					else
					{
						Fail("The Coupon Code text field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
					{
						Pass("The Coupon Code Apply button is displayed.");
						String caption = couponCodeApplyButton.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[2]))
						{
							Pass("The Submit button caption matches.",log);
						}
						else
						{
							Fail("The Submit button caption does not matches.");
						}
						
						String csVal = couponCodeApplyButton.getCssValue("background").substring(0, 16);
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							Pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Coupon Code Apply button is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(bookfairApplyContainer))
					{
						Pass("The Book Fair ID Code text field is displayed.");
					}
					else
					{
						Fail("The Book Fair ID text field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(bookfairApplyButton))
					{
						Pass("The Book Fair Code Apply button is displayed.");
						String caption = bookfairApplyButton.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[2]))
						{
							Pass("The Submit button caption matches.",log);
						}
						else
						{
							Fail("The Submit button caption does not matches.");
						}
						
						String csVal = bookfairApplyButton.getCssValue("background").substring(0, 16);
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							Pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Coupon Code Apply button is not displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container list is not displayed.");
				}	
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 559 Issue ." + e.getMessage());
				Exception(" BRM - 559 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 570 Verify that while selecting the "Apply" button without entering the card number and Pin details for the "B&N Gift Card,Rewards Certificates" section,the alert message should be displayed with text box highlighted.*/
	public void guestCheckooutGiftCardFieldEmptyValidation()
	{
		ChildCreation("BRM - 570 Verify that while selecting the Apply button without entering the card number and Pin details for the B&N Gift Card,Rewards Certificates section,the alert message should be displayed with text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM570", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM570", sheet, 3);
				String[] Val = cellVal.split("\n");
				String csVal = "",hexCode = "";
				if(BNBasicfeature.isElementPresent(giftCardsOpenTrigger))
				{
					jsclick(giftCardsOpenTrigger);
					wait.until(ExpectedConditions.visibilityOf(couponGiftCardOpen));
					Thread.sleep(200);
					if(BNBasicfeature.isElementPresent(couponGiftCardOpen))
					{
						Pass("The Gift Container is opened.");
						if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
						{
							Pass("The Apply button is displayed.");
							jsclick(couponCreditCardSubmit);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
							String cpnAlert = couponCreditCardError.getText();
							Thread.sleep(500);
							log.add("The expected alert was : " + Val[0]);
							log.add("The expected alert was : " + Val[1]);
							String err1 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[1]")).getText();
							String err2 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[2]")).getText();
							log.add("The actual alert was  : " + err1);
							log.add("The actual alert was  : " + err2);
							if(cpnAlert.contains("unexpected"))
							{
								Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
							}
							else if((err1.equals(Val[0]))&&(err2.equals( Val[1])))
							{
								Pass("The expected error matches the actual alert.",log);
							}
							else
							{
								Fail("The expected error does not match the actual alert.",log);
							}
							
							if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
							{
								csVal = couponCreditCardTextBox.getCssValue("border");
								hexCode = BNBasicfeature.colorfinder(csVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Credit Card number field is highlighted.",log);
								}
								else
								{
									Fail("The Credit Card number field is not highlighted.",log);
								}
							}
							else
							{
								Fail( "The Gift Card Text box is not displayed.");
							}
							
							if(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox))
							{
								csVal = couponCreditCardPinTextBox.getCssValue("border");
								hexCode = BNBasicfeature.colorfinder(csVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The PIN number field is highlighted.",log);
								}
								else
								{
									Fail("The PIN number field is not highlighted.",log);
								}
							}
							else
							{
								Fail( "The Gift Card PIN Text box is not displayed.");
							}
						}
						else
						{
							Fail("The Apply button is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Container is not opened.");
					}
				}
				else
				{
					Fail( " The Gift Card Open Trigger is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 570 Issue ." + e.getMessage());
				Exception(" BRM - 570 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 579 Verify whether text boxes are focused and highlighted in red by clicking on Apply without entering card Number and Pin for"B&N Gift Cards,B&N Kids Club Reward Certificate".*/
	public void guestCheckooutGiftCardFieldEmptyHighlight()
	{
		ChildCreation("BRM - 579 Verify whether text boxes are focused and highlighted in red by clicking on Apply without entering card Number and Pin for B&N Gift Cards,B&N Kids Club Reward Certificate");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM570", sheet, 3);
				String csVal = "", hexCode = "";
				if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
				{
					csVal = couponCreditCardTextBox.getCssValue("border");
					hexCode = BNBasicfeature.colorfinder(csVal);
					log.add("The expected color was : " + expColor);
					log.add("The actual color is : " + hexCode);
					if(hexCode.contains(expColor))
					{
						Pass("The Credit Card number field is highlighted.",log);
					}
					else
					{
						Fail("The Credit Card number field is not highlighted.",log);
					}
				}
				else
				{
					Fail( "The Gift Card Text box is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox))
				{
					csVal = couponCreditCardPinTextBox.getCssValue("border");
					hexCode = BNBasicfeature.colorfinder(csVal);
					log.add("The expected color was : " + expColor);
					log.add("The actual color is : " + hexCode);
					if(hexCode.contains(expColor))
					{
						Pass("The PIN number field is highlighted.",log);
					}
					else
					{
						Fail("The PIN number field is not highlighted.",log);
					}
				}
				else
				{
					Fail( "The Gift Card PIN Text box is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 579 Issue ." + e.getMessage());
				Exception(" BRM - 579 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 584 Verify whether on clicking inside Pin textbox with empty card Number textbox/invalid card number ,the card Number box should be highlighted in red.*/
	public void guestCheckooutGiftCreditCardFieldHighlight()
	{
		ChildCreation("BRM - 584 Verify whether on clicking inside Pin textbox with empty card Number textbox/invalid card number ,the card Number box should be highlighted in red.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM584", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
				{
					Pass("The Coupon Credit Card Text Box field .");
					couponCreditCardTextBox.click();
					couponCreditCardTextBox.clear();
					if(BNBasicfeature.isElementPresent(couponCreditCardPin))
					{
						Pass( "The Pin Number field is displayed");
						couponCreditCardPin.click();
						if(BNBasicfeature.isElementPresent(couponCreditCard))
						{
							Pass("The Credit Card field is displayed.");
							String csVal = couponCreditCardTextBox.getCssValue("border");
							String hexCode = BNBasicfeature.colorfinder(csVal);
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Credit Card number field is highlighted.",log);
							}
							else
							{
								Fail("The Credit Card number field is not highlighted.",log);
							}
						}
						else
						{
							Fail("The Credit Card field is not displayed.");
						}
					}
					else
					{
						Fail("The Pin Number field is not displayed.");
					}
				}
				else
				{
					Fail( "The Gift Card Text box is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 584 Issue ." + e.getMessage());
				Exception(" BRM - 584 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 581 Verify that while clicking inside the card number and pin text box of "B&N Gift Cards,B&N Kids' Club,Rewards Certificates" section,the cursor should blink inside the text box and should be highlighted.*/
	public void guestCheckooutGiftCreditCardFieldClick()
	{
		ChildCreation("BRM - 581 Verify that while clicking inside the card number and pin text box of B&N Gift Cards,B&N Kids' Club,Rewards Certificates section,the cursor should blink inside the text box and should be highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM581", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCreditCard))
				{
					Pass("The Credit Card Field is displayed.");
					couponCreditCard.click();
					Thread.sleep(500);
					
					if(BNBasicfeature.isElementPresent(couponCreditCard))
					{
						Pass("The Credit Card field is active.");
						String csVal = couponCreditCardTextBox.getCssValue("border");
						String hexCode = BNBasicfeature.colorfinder(csVal);
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							Pass("The Credit Card number field is highlighted.",log);
						}
						else
						{
							Fail("The Credit Card number field is not highlighted.",log);
						}
					}
					else
					{
						Fail("The Credit Card field is not active.");
					}
				}
				else
				{
					Fail("The Credit Card Field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 581 Issue ." + e.getMessage());
				Exception(" BRM - 581 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 586 Verify that while selecting the Apply button after entering the card number less/more than 15 digit's for B&N Gift Cards,B&N Kids',Rewards CertificatesClub section,the alert message should be displayed along with the text box,B&N Kids' Club.*/
	public void guestCheckooutGiftCreditCardFieldValidation()
	{
		ChildCreation("BRM - 586 Verify that while selecting the Apply button after entering the card number less/more than 15 digit's for B&N Gift Cards,B&N Kids',Rewards CertificatesClub section,the alert message should be displayed along with the text box,B&N Kids' Club.");
		if(shipPage==true)
		{
			try
			{
				String expError = BNBasicfeature.getExcelVal("BRM586", sheet, 4);
				String creditCardNumber = BNBasicfeature.getExcelNumericVal("BRM586", sheet, 5);
				String creditCardPin = BNBasicfeature.getExcelNumericVal("BRM586", sheet, 6);
				String expColor = BNBasicfeature.getExcelNumericVal("BRM586", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCreditCard))
				{
					log.add("The Credit Card field is displayed.");
					//couponCreditCard.click();
					couponCreditCard.click();
					if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
					{
						couponCreditCardTextBox.clear();
						Actions act = new Actions(driver);
						act.sendKeys(creditCardNumber).build().perform();
						//couponCreditCardfieldActive.sendKeys(creditCardNumber);
						if(BNBasicfeature.isElementPresent(couponCreditCardPin))
						{
							couponCreditCardPin.click();
							act.sendKeys(creditCardPin).build().perform();
							if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
							{
								jsclick(couponCreditCardSubmit);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
								Thread.sleep(500);
								String cpnAlert = couponCreditCardError.getText();
								log.add("The Expected Error is : " + expError);
								String err1 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[1]")).getText();
								log.add("The actual error is :  " + err1);
								if(cpnAlert.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
								}
								else if(expError.contains(err1))
								{
									Pass("The actual alert match the expected alert.",log);
								}
								else
								{
									Fail("The actual alert does not match the expected alert.",log);
								}
								
								String csVal = couponCreditCardTextBox.getCssValue("border");
								String hexCode = BNBasicfeature.colorfinder(csVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Credit Card number field is highlighted.",log);
								}
								else
								{
									Fail("The Credit Card number field is not highlighted.",log);
								}
							}
							else
							{
								Fail( "The Gift Card Submit Button is not displayed.");
							}
						}
						else
						{
							Fail( "The Gift Card PIN Text is not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card text field is not displayed.");
					}
				}
				else
				{
					Fail("The Credit Card Text Field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 586 Issue ." + e.getMessage());
				Exception(" BRM - 586 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 589 Verify that while selecting the "Apply" button by entering the alphabets,special char for the card Number and Pin in the "B&N Gift Cards,B&N Kids' Club Reward Certificate" section, alert message should be displayed with the text box highlighted.*/
	public void guestCheckooutGiftCreditCardFieldSpecialCharValidation()
	{
		ChildCreation("BRM - 589 Verify that while selecting the Apply button by entering the alphabets,special char for the card Number and Pin in the B&N Gift Cards,B&N Kids' Club Reward Certificate section, alert message should be displayed with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expError = BNBasicfeature.getExcelVal("BRM589", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM589", sheet, 3);
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM589", sheet, 5);
				String[] creditCardNumber = cellVal.split("\n");
				for(int i=0;i<creditCardNumber.length;i++)
				{
					if(BNBasicfeature.isElementPresent(couponCreditCard))
					{
						log.add("The Credit Card field is displayed.");
						//couponCreditCard.click();
						couponCreditCard.click();
						if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
						{
							couponCreditCardTextBox.clear();
							couponCreditCardTextBox.sendKeys(creditCardNumber[i]);
							if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
							{
								jsclick(couponCreditCardSubmit);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
								Thread.sleep(500);
								String cpnAlert = couponCreditCardError.getText();
								log.add("The Expected Error is : " + expError);
								String err1 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[1]")).getText();
								log.add("The Entered Value from the excel was : " + creditCardNumber[i]);
								log.add("The Expected Error is : " + expError);
								log.add("The actual error is :  " + err1);
								if(cpnAlert.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
								}
								else if(expError.contains(err1))
								{
									Pass("The actual alert match the expected alert.",log);
								}
								else
								{
									Fail("The actual alert does not match the expected alert.",log);
								}
								String csVal = couponCreditCardTextBox.getCssValue("border");
								String hexCode = BNBasicfeature.colorfinder(csVal);
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Credit Card number field is highlighted.",log);
								}
								else
								{
									Fail("The Credit Card number field is not highlighted.",log);
								}
							}
							else
							{
								Fail( "The Gift Card Submit Button is not displayed.");
							}
						}
						else
						{
							Fail("The Credit Card text field is not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card field is not displayed.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 589 Issue ." + e.getMessage());
				Exception(" BRM - 589 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 594 Verify that while selecting the "Apply" button after entering the "Pin" details less/more than 4 digits for "B&N Gift Cards,B&N Kids CLub Reward Certificates", the alert message should be displayed with text box highlighted.*/
	public void guestCheckooutGiftCreditCardPinValidation()
	{
		ChildCreation("BRM - 594 Verify that while selecting the Apply button after entering the Pin details less/more than 4 digits for B&N Gift Cards,B&N Kids CLub Reward Certificates, the alert message should be displayed with text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expError = BNBasicfeature.getExcelVal("BRM594", sheet, 4);
				String creditCardNumber = BNBasicfeature.getExcelNumericVal("BRM594", sheet, 5);
				String creditCardPin = BNBasicfeature.getExcelNumericVal("BRM594", sheet, 6);
				if(BNBasicfeature.isElementPresent(couponCreditCard))
				{
					log.add("The Credit Card field is displayed.");
					couponCreditCard.click();
					if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
					{
						couponCreditCardTextBox.clear();
						couponCreditCardTextBox.sendKeys(creditCardNumber);
						if(BNBasicfeature.isElementPresent(couponCreditCardPin)&&(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox)))
						{
							couponCreditCardPin.click();
							couponCreditCardPinTextBox.clear();
							couponCreditCardPinTextBox.sendKeys(creditCardPin);
							if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
							{
								jsclick(couponCreditCardSubmit);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
								Thread.sleep(500);
								String cpnAlert = couponCreditCardError.getText();
								log.add("The Expected Error is : " + expError);
								String err1 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[1]")).getText();
								log.add("The actual error is :  " + err1);
								if(cpnAlert.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
								}
								else if(expError.contains(err1))
								{
									Pass("The actual alert match the expected alert.",log);
								}
								else
								{
									Fail("The actual alert does not match the expected alert.",log);
								}
							}
							else
							{
								Fail( "The Gift Card Submit Button is not displayed.");
							}
						}
						else
						{
							Fail("The Credit Card PIN field is not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card text field is not displayed.");
					}
				}
				else
				{
					Fail("The Credit Card field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 594 Issue ." + e.getMessage());
				Exception(" BRM - 594 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 595 Verify that while selecting the "Apply" button after entering the invalid card number of 15 digits in the "B&N Gift Cards,B&N Kids' Club" section,the alert message should be displayed along with the text box highlighted .*/
	public void guestCheckooutGiftInvalidCreditCardValidation()
	{
		ChildCreation("BRM - 595 Verify that while selecting the Apply button after entering the invalid card number of 15 digits in the B&N Gift Cards,B&N Kids' Club section,the alert message should be displayed along with the text box highlighted .");
		if(shipPage==true)
		{
			try
			{
				String expError = BNBasicfeature.getExcelVal("BRM595", sheet, 4);
				String creditCardNumber = BNBasicfeature.getExcelNumericVal("BRM595", sheet, 5);
				String creditCardPin = BNBasicfeature.getExcelNumericVal("BRM595", sheet, 6);
				if(BNBasicfeature.isElementPresent(couponCreditCard))
				{
					log.add("The Credit Card field is displayed.");
					couponCreditCard.click();
					if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
					{
						couponCreditCardTextBox.clear();
						couponCreditCardTextBox.sendKeys(creditCardNumber);
						if(BNBasicfeature.isElementPresent(couponCreditCardPin)&&(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox)))
						{
							couponCreditCardPin.click();
							couponCreditCardPinTextBox.clear();
							couponCreditCardPinTextBox.sendKeys(creditCardPin);
							if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
							{
								jsclick(couponCreditCardSubmit);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
								Thread.sleep(500);
								String cpnAlert = couponCreditCardError.getText();
								log.add("The Expected Error is : " + expError);
								String err1 = couponCreditCardError.findElement(By.xpath("(//*[contains(@class,'emphasis--alert')])[1]")).getText();
								log.add("The actual error is :  " + err1);
								if(cpnAlert.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
								}
								else if(expError.contains(err1))
								{
									Pass("The actual alert match the expected alert.",log);
								}
								else
								{
									Fail("The actual alert does not match the expected alert.",log);
								}
							}
							else
							{
								Fail( "The Gift Card Submit Button is not displayed.");
							}
						}
						else
						{
							Fail("The Credit Card PIN field is not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card text field is not displayed.");
					}
				}
				else
				{
					Fail("The Credit Card field is not displayed.");
				}
				jsclick(giftCardsOpenTrigger);
				//couponGiftCardsOpenTrigger.click();
				Thread.sleep(1000);
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 595 Issue ." + e.getMessage());
				Exception(" BRM - 595 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 608 Verify that while selecting the "+" button near the "Coupon Code" section,it should display a textbox for card number with default text and Apply button.*/
	public void guestCheckooutCouponField()
	{
		ChildCreation("BRM - 608 Verify that while selecting the "+" button near the Coupon Code section,it should display a textbox for card number with default text and Apply button.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(couponCouponOpenTrigger))
				{
					jsclick(couponCouponOpenTrigger);
					Thread.sleep(1000);
					String expColor = BNBasicfeature.getExcelNumericVal("BRM608", sheet, 3);
					String cellVal = BNBasicfeature.getExcelNumericVal("BRM608", sheet, 2);
					String[] Val = cellVal.split("\n");
					if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
					{
						if(BNBasicfeature.isElementPresent(couponCodeApplyContainer))
						{
							Pass("The Coupon Code text field is displayed.");
						}
						else
						{
							Fail("The Coupon Code text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeDefaultText))
						{
							Pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default Text was : " + Val[1]);
							String dfltText = couponCodeDefaultText.getText();
							log.add("The actual default text is : " + dfltText);
							if((dfltText.contains(Val[1])))
							{
								Pass("The Default text matches the expected content.",log);
							}
							else
							{
								Fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							Fail("The Default text is not displayed for the Coupon field");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							Pass("The Coupon Code Apply button is displayed.");
							String caption = couponCodeApplyButton.getAttribute("value");
							log.add("The expected value was : " + Val[0]);
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[0]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCodeApplyButton.getCssValue("background").substring(0,16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Apply button is not displayed.");
						}
					}
					else
					{
						Fail("The Loyalty Container list is not displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container Open Trigger Icon is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 608 Issue ." + e.getMessage());
				Exception(" BRM - 608 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 611 Verify that while selecting inside the card number text box in the "Coupon Code" section,the text box cursor should be blink inside with the highlightion.*/
	public void guestCheckooutCouponFieldClick()
	{
		ChildCreation("BRM - 611 Verify that while selecting inside the caard number text box in the Coupon Code section,the text box cursor should be blink inside with the highligted.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM611", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCodeTextField))
				{
					Pass("The Coupon Code field is displayed.");
					couponCodeTextField.click();
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(couponCodeTextFieldActive))
					{
						String csVal = couponCodeTextFieldActive.getCssValue("border-color");
						Color colorhxcnvt = Color.fromString(csVal);
						String hexCode = colorhxcnvt.asHex();
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							Pass("The Coupon Code field is highlighted.",log);
						}
						else
						{
							Fail("The Coupon Code field is not highlighted.",log);
						}
					}
					else
					{
						Fail("The Coupon Code field is not active.");
					}
				}
				else
				{
					Fail("The Coupon Code field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 611 Issue ." + e.getMessage());
				Exception(" BRM - 611 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 612 Verify that while selecting the "Apply" button without entering the card number in the "Coupon Code" text box,the alert message should be displayed along with the text box highlighted.*/
	public void guestCheckooutCouponEmptyValidation()
	{
		ChildCreation("BRM - 612 Verify that while selecting the Apply button without entering the card number in the Coupon Code text box,the alert message should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String cpnAlert = BNBasicfeature.getExcelVal("BRM612", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM612", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCodeApplyContainer))
				{
					Pass("The Coupon Code field is displayed.");
					if(BNBasicfeature.isElementPresent(couponCodeTextField))
					{
						couponCodeTextField.click();
						if(BNBasicfeature.isElementPresent(couponCodeTextFieldActive))
						{
							couponCodeTextFieldActive.clear();
							if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
							{
								jsclick(couponCodeApplyButton);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCouponError, "aria-atomic", "true"));
								Thread.sleep(500);
								log.add("The Expected alert was : " + cpnAlert);
								String actAlert = couponCouponError.getText();
								log.add("The actual alert is : " +actAlert);
								if(cpnAlert.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
								}
								else if(cpnAlert.contains(actAlert))
								{
									Pass("The expected alert and actual alert match.",log);
								}
								else
								{
									Fail("The expected alert and actual alert does not match.",log);
								}
								
								String csVal = couponCodeTextFieldActive.getCssValue("border-color");
								Color colorhxcnvt = Color.fromString(csVal);
								String hexCode = colorhxcnvt.asHex();
								log.add("The expected color was : " + expColor);
								log.add("The actual color is : " + hexCode);
								if(hexCode.contains(expColor))
								{
									Pass("The Coupon Code field is highlighted.",log);
								}
								else
								{
									Fail("The Coupon Code field is not highlighted.",log);
								}
							}
							else
							{
								Fail( "The Coupon Code Apply button is not displayed.");
							}
						}
						else
						{
							Fail( "The Coupon Code field is not active.");
						}
					}
					else
					{
						Fail( "The Coupon Code Tezt field is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon Code field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 612 Issue ." + e.getMessage());
				Exception(" BRM - 612 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 610 Verify that while selecting the "Apply" button by entering the invalid code details in the "Coupon Code" text box,the alert mesage should be displayed along with the text box highlighted.*/
	public void guestCheckooutCouponInvalidValidation()
	{
		ChildCreation("BRM - 610 Verify that while selecting the Apply button by entering the invalid code details in the Coupon Code text box,the alert mesage should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String cpnCode = BNBasicfeature.getExcelNumericVal("BRM610", sheet, 7);
				String cpnAlert = BNBasicfeature.getExcelVal("BRM610", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM610", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCodeTextField))
				{
					Pass("The Coupon Code Container is displayed.");
					if(BNBasicfeature.isElementPresent(couponCodeTextFieldActive))
					{
						couponCodeTextFieldActive.click();
						couponCodeTextFieldActive.clear();
						/*Actions act = new Actions(driver);
						act.sendKeys(cpnCode).build().perform();*/
						couponCodeTextFieldActive.sendKeys(cpnCode);
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							jsclick(couponCodeApplyButton);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(couponCouponError, "aria-atomic", "true"));
							Thread.sleep(500);
							log.add("The Expected alert was : " + cpnAlert);
							String actAlert = couponCouponError.getText();
							log.add("The actual alert is : " + actAlert);
							if(cpnAlert.contains("unexpected"))
							{
								Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
							}
							else if(cpnAlert.contains(actAlert))
							{
								Pass("The expected alert and actual alert match.",log);
							}
							else
							{
								Fail("The expected alert and actual alert does not match.",log);
							}
							
							String csVal = couponCodeTextFieldActive.getCssValue("border-color");
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Coupon Code field is highlighted.",log);
							}
							else
							{
								Fail("The Coupon Code field is not highlighted.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Apply button is not displayed.");
						}
					}
					else
					{
						Fail("The Coupon Code field is not Active.");
					}
				}
				else
				{
					Fail("The Coupon Code Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 610 Issue ." + e.getMessage());
				Exception(" BRM - 610 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 613 Verify that while selecting the "Apply" button by entering invalid coupon for the card Number in the "Coupon Code" section,the alert message should be displayed*/
	public void guestCheckooutCouponInvalidValidation2() throws Exception
	{
		ChildCreation("BRM - 613 Verify that while selecting the Apply button by entering the invalid code details in the Coupon Code text box,the alert mesage should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String cpnCode = BNBasicfeature.getExcelNumericVal("BRM613", sheet, 7);
				String cpnAlert = BNBasicfeature.getExcelVal("BRM613", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM613", sheet, 3);
				if(BNBasicfeature.isElementPresent(couponCodeTextField))
				{
					Pass("The Coupon Code Container is displayed.");
					couponCodeTextField.click();
					if(BNBasicfeature.isElementPresent(couponCodeTextFieldActive))
					{
						couponCodeTextFieldActive.clear();
						couponCodeTextFieldActive.sendKeys(cpnCode);
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							jsclick(couponCodeApplyButton);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(couponCouponError, "aria-atomic", "true"));
							Thread.sleep(500);
							log.add("The Expected alert was : " + cpnAlert);
							String actAlert = couponCouponError.getText();
							log.add("The actual alert is : " + actAlert);
							if(cpnAlert.contains("unexpected"))
							{
								Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + cpnAlert);
							}
							else if(cpnAlert.contains(actAlert))
							{
								Pass("The expected alert and actual alert match.",log);
							}
							else
							{
								Fail("The expected alert and actual alert does not match.",log);
							}
							
							String csVal = couponCodeTextFieldActive.getCssValue("border-color");
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Coupon Code field is highlighted.",log);
							}
							else
							{
								Fail("The Coupon Code field is not highlighted.",log);
							}
						}
						else
						{
							Fail( "The Coupon Code Apply button is not displayed.");
						}
					}
					else
					{
						Fail( "The Coupon Code Text field  is not active.");
					}
				}
				else
				{
					Fail("The Coupon Code Container is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 613 Issue ." + e.getMessage());
				Exception(" BRM - 613 Issue ." + e.getMessage());
			}
			jsclick(couponCouponOpenTrigger);
			Thread.sleep(1000);
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}

	/* BRM - 614 Verify that while selecting the "+" button near the "Bookfair ID" section,the textbox should be displayed with default text and "Apply" button.*/
	public void guestCheckooutBookFairField()
	{
		ChildCreation("BRM - 614 Verify that while selecting the "+" button near the Bookfair ID section,the textbox should be displayed with default text and Apply button.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(bookFairOpenTrigger))
				{
					jsclick(bookFairOpenTrigger);
					Thread.sleep(1000);
					String expColor = BNBasicfeature.getExcelNumericVal("BRM614", sheet, 3);
					String cellVal = BNBasicfeature.getExcelNumericVal("BRM614", sheet, 2);
					String[] Val = cellVal.split("\n");
					if(BNBasicfeature.isElementPresent(bookfairApplyContainer))
					{
						Pass("The Book Fair ID Code text field is displayed.");
					}
					else
					{
						Fail("The Book Fair ID text field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(bookFairDefaultText))
					{
						Pass("The Default text is displayed for the Book Fair ID  field");
						log.add("The Expected Default text was : " + Val[1]);
						String title = bookFairDefaultText.getText();
						log.add("The actual Default text is : " + bookFairDefaultText.getText());
						if(Val[1].contains(title))
						{
							Pass("The Default text matches the expected content.",log);
						}
						else
						{
							Fail("The Default text does not match the expected conten.",log);
						}
					}
					else
					{
						Fail("The Default text is not displayed for the Coupon field");
					}
					
					if(BNBasicfeature.isElementPresent(bookfairApplyButton))
					{
						Pass("The Book Fair ID Apply button is displayed.");
						String caption = bookfairApplyButton.getAttribute("value");
						log.add("The actual val is : " + caption);
						if(caption.equals(Val[0]))
						{
							Pass("The Submit button caption matches.",log);
						}
						else
						{
							Fail("The Submit button caption does not matches.");
						}
						
						String csVal = bookfairApplyButton.getCssValue("background").substring(0, 16);
						String hexCode = BNBasicfeature.colorfinder(csVal);
						log.add("The expected color of the button is : " + expColor);
						log.add("The actual color of the button is : " + hexCode);
						if(hexCode.equals(expColor))
						{
							Pass("The Apply button color matches the expected one.",log);
						}
						else
						{
							Fail("The Apply button color does not matches the expected one.",log);
						}
					}
					else
					{
						Fail("The Book Fair ID Apply button is not displayed.");
					}
				}
				else
				{
					Fail( "The Book Fair Open trigger is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 614 Issue ." + e.getMessage());
				Exception(" BRM - 614 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 615 Verify that on clicking inside the book-id text box in the "Bookfair ID" section,the cursor should blink inside the text box with highlightion.*/
	public void guestCheckooutBookFairFieldClick()
	{
		ChildCreation("BRM - 615 Verify that on clicking inside the book-id text box in the Bookfair ID section,the cursor should blink inside the text box with highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM615", sheet, 3);
				if(BNBasicfeature.isElementPresent(bookfairApplyField))
				{
					Pass("The Book Fair ID field is displayed.");
					bookfairApplyField.click();
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(bookFairTextField))
					{
						String csVal = bookFairTextField.getCssValue("color").substring(0, 16);
						String hexCode = BNBasicfeature.colorfinder(csVal);
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							Pass("The Book Fair Id field is highlighted.",log);
						}
						else
						{
							Fail("The Book Fair Id field is not highlighted.",log);
						}
					}
					else
					{
						Fail("The Book Fair ID is not active.");
					}
				}
				else
				{
					Fail("The Book Fair ID  field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 615 Issue ." + e.getMessage());
				Exception(" BRM - 615 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 618 Verify that while selecting the "Apply" button without entering the book-id details in the "Bookfair ID" section,the alert message should be displayed along with the text box highlighted.*/
	public void guestCheckooutBookFairEmptyValidation()
	{
		ChildCreation("BRM - 618 Verify that while selecting the Apply button without entering the book-id details in the Bookfair ID section,the alert message should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expAlert = BNBasicfeature.getExcelVal("BRM618", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM618", sheet, 3);
				if(BNBasicfeature.isElementPresent(bookfairApplyButton))
				{
					Pass("The Apply button is displayed in the Book Fair ID section.");
					jsclick(bookfairApplyButton);
					Thread.sleep(500);
					wait.until(ExpectedConditions.attributeContains(couponBookFairError, "aria-atomic", "true"));
					Thread.sleep(500);
					log.add("The Expected alert was : " + expAlert);
					String actAlert = couponBookFairError.getText();
					log.add("The actual alert was : " + actAlert);
					if(actAlert.contains("unexpected"))
					{
						Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + actAlert);
					}
					else if(expAlert.contains(actAlert))
					{
						Pass("The raised alert match the expected content.",log);
					}
					else
					{
						Fail("The raised alert does not match the expected content.",log);
					}
					
					//String csVal = bookFairTextField.getCssValue("border").substring(0,25);
					String csVal = bookFairTextField.getCssValue("border");
					//String csVal = bookFairTextField.getCssValue("border");
					String hexCode = BNBasicfeature.colorfinder(csVal);
					log.add("The expected color was : " + expColor);
					log.add("The actual color is : " + hexCode);
					if(hexCode.contains(expColor))
					{
						Pass("The Book Fair ID number field is highlighted.",log);
					}
					else
					{
						Fail("The Book Fair ID field is not highlighted.",log);
					}
				}
				else
				{
					Fail("The Apply button is not displayed in the Book Fair ID section.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 618 Issue ." + e.getMessage());
				Exception(" BRM - 618 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 620 Verify that while selecting the "Apply" button after entering the 8 digit invalid bookfair id in the "Bookfair ID" section,the alert message should be displayed along with the text box highlighted.*/
	public void guestCheckoutBookFairIDInvalidValidation()
	{
		ChildCreation("BRM - 620 Verify that while selecting the Apply button after entering the 8 digit invalid bookfair id in the Bookfair ID section,the alert message should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expAlert = BNBasicfeature.getExcelVal("BRM620", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM620", sheet, 3);
				String bookFairID = BNBasicfeature.getExcelNumericVal("BRM620", sheet, 8);
				if(BNBasicfeature.isElementPresent(bookFairTextField))
				{
					Pass("The Apply button is displayed in the Book Fair ID section.");
					bookFairTextField.click();
					bookFairTextField.clear();
					bookFairTextField.sendKeys(bookFairID);
					if(BNBasicfeature.isElementPresent(bookfairApplyButton))
					{
						jsclick(bookfairApplyButton);
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(couponBookFairError, "aria-atomic", "true"));
						Thread.sleep(500);
						log.add("The Expected alert was : " + expAlert);
						String actAlert = couponBookFairError.getText();
						log.add("The actual alert was : " + actAlert);
						log.add("The actual alert was : " + actAlert);
						/*System.out.println(expAlert);
						System.out.println(actAlert);*/
						if(actAlert.contains("unexpected"))
						{
							Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + actAlert);
						}
						else if(expAlert.contains(actAlert))
						{
							Pass("The raised alert match the expected content.",log);
						}
						else
						{
							Fail("The raised alert does not match the expected content.",log);
						}
						
						//String csVal = bookFairTextField.getCssValue("border").substring(0, 25);
						String csVal = bookFairTextField.getCssValue("border");
						String hexCode = BNBasicfeature.colorfinder(csVal);
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							Pass("The Book Fair ID number field is highlighted.",log);
						}
						else
						{
							Fail("The Book Fair ID field is not highlighted.",log);
						}
					}
					else
					{
						Fail( "The Book Fair Apply button is not displayed.");
					}
				}
				else
				{
					Fail("The Apply button is not displayed in the Book Fair ID section.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 620 Issue ." + e.getMessage());
				Exception(" BRM - 620 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}
	
	/* BRM - 621 Verify that while selecting the "Apply" button by entering alphabets,special character or numeric digit less/greater than 8 for "Bookfair ID",the alert should be displayed along with the text box highlighted*/
	public void guestCheckoutBookFairIDInvalidValidation2()
	{
		ChildCreation("BRM - 621 Verify that while selecting the Apply button after entering the 8 digit invalid bookfair id in the Bookfair ID section,the alert message should be displayed along with the text box highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expAlert = BNBasicfeature.getExcelVal("BRM621", sheet, 4);
				String expColor = BNBasicfeature.getExcelVal("BRM621", sheet, 3);
				String bookFairID = BNBasicfeature.getExcelNumericVal("BRM621", sheet, 8);
				if(BNBasicfeature.isElementPresent(bookFairTextField))
				{
					Pass("The Apply button is displayed in the Book Fair ID section.");
					bookFairTextField.click();
					bookFairTextField.clear();
					bookFairTextField.sendKeys(bookFairID);
					if(BNBasicfeature.isElementPresent(bookfairApplyButton))
					{
						jsclick(bookfairApplyButton);
						Thread.sleep(500);
						wait.until(ExpectedConditions.attributeContains(couponBookFairError, "aria-atomic", "true"));
						Thread.sleep(500);
						log.add("The Expected alert was : " + expAlert);
						String actAlert = couponBookFairError.getText();
						log.add("The actual alert was : " + actAlert);
						if(actAlert.contains("unexpected"))
						{
							Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + actAlert);
						}
						else if(expAlert.contains(actAlert))
						{
							Pass("The raised alert match the expected content.",log);
						}
						else
						{
							Fail("The raised alert does not match the expected content.",log);
						}
						
						//String csVal = bookFairTextField.getCssValue("border").substring(0, 25);
						String csVal = bookFairTextField.getCssValue("border");
						String hexCode = BNBasicfeature.colorfinder(csVal);
						log.add("The expected color was : " + expColor);
						log.add("The actual color is : " + hexCode);
						if(hexCode.contains(expColor))
						{
							Pass("The Book Fair ID number field is highlighted.",log);
						}
						else
						{
							Fail("The Book Fair ID field is not highlighted.",log);
						}
					}
					else
					{
						Fail( "The Book Fair Apply button is not displayed.");
					}
				}
				else
				{
					Fail("The Apply button is not displayed in the Book Fair ID section.");
				}
				jsclick(bookFairOpenTrigger);
				Thread.sleep(1000);
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 621 Issue ." + e.getMessage());
				Exception(" BRM - 621 Issue ." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User is not navigated to the Payments Page.");
		}
	}

	/********************************************* BRM - 298 Payment Info for Guest User ********************************************/
	
	/* BRM - 717 Guest Checkout - Verify that Credit Card tab should be selected by default in the payment page*/
	public void payInfoGuestPaymentDefaultSelection()
	{
		ChildCreation("  BRM - 717 - Guest Checkout - Verify that Credit Card tab should be selected by default in the payment page");
		try
		{
			String title = BNBasicfeature.getExcelVal("BRM717", sheet, 12);
			if(BNBasicfeature.isElementPresent(paymentCheckoutStep))
			{
				boolean stepthree = paymentCheckoutStep.getAttribute("aria-label").contains("Current");
				if(stepthree == true)
				{
					shipPage = true;
					log.add("The User is navigated to the Payment Option Page.");
					if(BNBasicfeature.isElementPresent(guestPaymentTab))
					{
						Pass("The Guest Payment Tab is displayed.");
						if(BNBasicfeature.isElementPresent(guestCCPaymentTab))
						{
							Pass("The Guest Credit Card Payment Tab is displayed.");
							log.add("The Expected title was : " + title);
							String acttitle = guestCCPaymentTab.getText();
							if(title.equals(acttitle))
							{
								Pass("The Caption matches the expected one.",log);	
							}
							else
							{
								Fail("The Caption does not match the expected one.",log);
							}
						}
						else
						{
							Fail("The Credit card Payment Tab is not displayed .");
						}
													
						if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
						{
							jsclick(guestPaypalNotActiveTab);
							wait.until(ExpectedConditions.visibilityOf(guestCCPaymentNotActiveTab));
						}
													
						if(BNBasicfeature.isElementPresent(guestCCPaymentNotActiveTab))
						{
							jsclick(guestCCPaymentNotActiveTab);
							wait.until(ExpectedConditions.visibilityOf(guestPaypalNotActiveTab));
						}
					}
					else
					{
						Fail("The Payment tab is not displayed by default.");
					}
				}
				else
				{
					shipPage = false;
					Fail("The User is not in the Payments Tab.");
				}
			}
			else
			{
				Fail( "The Payment Checkout Step is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 717 Issue ." + e.getMessage());
			Exception(" BRM - 717 Issue ." + e.getMessage());
		}
	}
	
	/* BRM - 943 Guest Checkout - Verify that on selecting Credit Card tab it should be highlighted.*/
	public void payInfoCCTabHighlight()
	{
		ChildCreation(" BRM - 943 - Guest Checkout - Verify that clicking on �Check out with PayPal� should redirect to the PayPal page.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM943", sheet, 11);
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					if(BNBasicfeature.isElementPresent(guestCCContainerActiveTab))
					{
						Pass("The Credit Card Tab is active.");
						if(BNBasicfeature.isElementPresent(guestCCPaymentTab))
						{
							Pass("The credit card payment tab is displayed.");
							String ColorName = guestCCPaymentTab.getCssValue("color");
							Color colorhxcnvt = Color.fromString(ColorName);
							String hexCode = colorhxcnvt.asHex();
							log.add("The Expected Color was : " + expColor);
							log.add("The Actual Color is : " + hexCode);
							if(expColor.equals(hexCode))
							{
								Pass("The Tab is highlighted with the expected color.",log);
							}
							else
							{
								Fail("The Tab is not highlighted with the expected color.",log);
							}
						}
						else
						{
							Fail("The credit card payment tab is not displayed.");
						}
					}
					else
					{
						Fail("The Guest Credit Card Tab is not active");
					}
				}
				else
				{
					Fail("The Guest Credit Card payment tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 943 Issue ." + e.getMessage());
				Exception(" BRM - 943 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 713 Guest Checkout - Verify that the payment page should be displayed as per the creative in the guest checkout page*/
	public void payInfoGuestPaymentPageElements()
	{
		ChildCreation("BRM - 713 - Guest Checkout - Verify that the payment page should be displayed as per the creative in the guest checkout page.");
		if(shipPage==true)
		{
			try
			{
				String couponTitle = BNBasicfeature.getExcelVal("BRM548", sheet, 3);
				paymentPageFieldCheck(couponTitle);
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 713 Issue ." + e.getMessage());
				Exception(" BRM - 713 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 830 Guest Checkout - Verify that tabs are highlighted till Payment and Shipping & Delivery should be highlighted.*/
	public void payInfoCCBreadCrumHighlight()
	{
		ChildCreation("BRM - 830 - Guest Checkout - Verify that tabs are highlighted till Payment and Shipping & Delivery should be highlighted.");
		if(shipPage==true)
		{
			try
			{
				String color = BNBasicfeature.getExcelVal("BRM830", sheet, 11);
				if(BNBasicfeature.isListElementPresent(checkoutStepsLists))
				{
					BNBasicfeature.scrollup(checkoutSteps, driver);
					String[] script = new String[3];
					String[] hexCode = new String[script.length];
					//String[] breadcrumtitle = new String[script.length];
					log.add("The Checkout Steps are displayed.");
					script[0] = "return window.getComputedStyle(document.querySelector('#checkoutStepOne'),':before').getPropertyValue('background')";
					script[1] = "return window.getComputedStyle(document.querySelector('#checkoutStepTwo'),':before').getPropertyValue('background')";
					script[2] = "return window.getComputedStyle(document.querySelector('#checkoutStepThree'),':before').getPropertyValue('background')";
					
					/*for(int j=0; j<script.length; j++)
					{
						WebElement ele = driver.findElement(By.xpath("(//*[@id='checkoutSteps']//li//a)["+(j+1)+"]"));
						String elecolor = ele.getCssValue("color");
						Color colorhxcnvt = Color.fromString(elecolor);
						breadcrumtitle[j] = colorhxcnvt.asHex();
						JavascriptExecutor js = (JavascriptExecutor)driver;
						String content = (String) js.executeScript(script[j]);
						String split = content.substring(0,16);
						Color colorhxcnvt1 = Color.fromString(split);
						hexCode[j] = colorhxcnvt1.asHex();
						if(color.equals(hexCode[j]))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
					}*/
					
					for(int j=0; j<script.length; j++)
					{
						WebElement ele = driver.findElement(By.xpath("(//*[@class='header-checkout']//li)["+(j+1)+"]"));
						/*String elecolor = ele.getCssValue("color"); Un Comment
						Color colorhxcnvt = Color.fromString(elecolor);
						breadcrumtitle[j] = colorhxcnvt.asHex();*/
						/*JavascriptExecutor js = (JavascriptExecutor)driver;
						String content = (String) js.executeScript(script[j]);
						String split = content.substring(0,16);
						Color colorhxcnvt1 = Color.fromString(split);
						hexCode[j] = colorhxcnvt1.asHex();*/
						String elecolor = ele.getCssValue("border-bottom").substring(9, 25);
						Color colorhxcnvt = Color.fromString(elecolor);
						String actCol = colorhxcnvt.asHex();
						
						if(color.equals(actCol))
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Pass("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
						else
						{
							log.add("The " + ele.getText() + " breadcrum expected color was " + color);
							log.add("The " + ele.getText() + " breadcrum actual color is " + hexCode[j]);
							Fail("The " + ele.getText() + " breadcrum bottom section is highlighted.",log);
						}
					}
				}
				else
				{
					Fail("The Checkout Steps are not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 830 Issue ." + e.getMessage());
				Exception(" BRM - 830 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 720 Guest Checkout - Verify that all the mandatory input fields related to credit card should be displayed.*/
	public void payInfoGuestCCElements()
	{
		ChildCreation(" BRM - 720 - Guest Checkout - Verify that all the mandatory input fields related to credit card should be displayed. ");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					if(BNBasicfeature.isElementPresent(ccNumber))
					{
						Pass("The Credit Card Number is displayed.");
					}
					else
					{
						Fail("The Credit Card Number is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(ccName))
					{
						Pass("The Credit Card Name is displayed.");
					}
					else
					{
						Fail("The Credit Card Name is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(ccMonth))
					{
						Pass("The Credit Card Month drop down is displayed.");
					}
					else
					{
						Fail("The Credit Card Month drop down is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(ccYear))
					{
						Pass("The Credit Card Year drop down field is displayed.");
					}
					else
					{
						Fail("The Credit Card Year drop down field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(ccCsv))
					{
						Pass("The Credit Card Security Field CSV is displayed.");
					}
					else
					{
						Fail("The Credit Card Security Field CSV field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(cciicon))
					{
						Pass("The Tooltip icon is dispalyed.");
					}
					else
					{
						Fail("The Tooltip icon is not dispalyed.");
					}
					
					if(BNBasicfeature.isElementPresent(guestSameAsShippingChkbox))
					{
						Pass("The Same As Shipping Address checkbox is dispalyed.");
					}
					else
					{
						Fail("The Same As Shipping Address checkbox is not dispalyed.");
					}
					
					if(BNBasicfeature.isElementPresent(guestEmailAddress))
					{
						guestEmailAddress.clear();
						Pass("The Email Address Field is dispalyed.");
					}
					else
					{
						Fail("The Email Address Field is not dispalyed.");
					}
				}
				else
				{
					Fail("The payments tab is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 720 Issue ." + e.getMessage());
				Exception(" BRM - 720 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	
	/* BRM - 721 Guest Checkout � Verify that while selecting the "Submit" button without entering any values in the credit card input fields,the alert message should be displayed.*/
	public void payInfoGuestCCEmptyValidation()
	{
		ChildCreation("BRM - 721 - Guest Checkout - Verify that while selecting the Submit button without entering any values in the credit card input fields,the alert message should be displayed.");
		if(shipPage==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelNumericVal("BRM721", sheet, 10);
				if(BNBasicfeature.isElementPresent(orderContinueBtn))
				{
					jsclick(orderContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(guestError));
					Thread.sleep(500);
					log.add("The Expected alert was : " + alert);
					if(BNBasicfeature.isElementPresent(guestError))
					{
						Pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual Alert was : " + actAlert);
						if(actAlert.contains(alert))
						{
							Pass("The alert match the expected content.",log);
						}
						else
						{
							Fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						Fail("No Validation is not raised to indicate user.");
					}
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 721 Issue ." + e.getMessage());
				Exception(" BRM - 721 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 902 Guest Checkout � Verify that alert messages are displayed for wrong card numbers entered.*/
	public void payInfoCCInvalidCardValidation()
	{
		ChildCreation(" BRM - 902 - Guest Checkout - Verify that alert messages are displayed for wrong card numbers entered.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM902", sheet, 38);
				String alert = BNBasicfeature.getExcelVal("BRM902", sheet, 10);
				if(BNBasicfeature.isElementPresent(ccNumber))
				{
					BNBasicfeature.scrolldown(ccNumber,driver);
					ccNumber.click();
					ccNumber.clear();
					ccNumber.sendKeys(cellVal);
					jsclick(orderContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(guestError));
					Thread.sleep(200);
					log.add("The Expected alert was : " + alert);
					if(BNBasicfeature.isElementPresent(guestError))
					{
						Pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual alert was : " + actAlert);
						if(actAlert.contains(alert))
						{
							Pass("The alert match the expected content.",log);
						}
						else
						{
							Fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						Fail("No Validation is not raised to indicate user.");
					}
				}
				else
				{
					Fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 902 Issue ." + e.getMessage());
				Exception(" BRM - 902 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 727 Guest Checkout � Verify that user is able to enter only number in the Card Number field and it should accept maximum of 16 digits.*/
	public void payInfoGuestCCNumberLengthValidation()
	{
		ChildCreation("BRM - 727 - Guest Checkout - Verify that user is able to enter only number in the Card Number field and it should accept maximum of 16 digits.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM727", sheet, 38);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(ccNumber))
				{
					Pass("The Credit Card Number field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						ccNumber.click();
						ccNumber.clear();
						ccNumber.sendKeys(Val[i]);
						//int size = ccNumber.getAttribute("value").length();
						if((ccNumber.getAttribute("value").length()>0) && (ccNumber.getAttribute("value").length()<=16))
						{
							log.add("The value from the excel sheet for the credit card number is " + Val[i] + " and its length is " + Val[i].length());
							log.add("The Current value in the credit card number feild is " + ccNumber.getAttribute("value") + " and its length is " + ccNumber.getAttribute("value").length());
							Pass("The Credit number field accepts characters less than 16.",log);
						}
						else
						{
							log.add("The value from the excel sheet for the credit card number is " + Val[i] + " and its length is " + Val[i].length());
							log.add("The Current value in the credit card number feild is " + ccNumber.getAttribute("value") + " and its length is " + ccNumber.getAttribute("value").length());
							Fail("The Credit number field accepts characters more than 16.",log);
						}
					}
				}
				else
				{
					Fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 727 Issue ." + e.getMessage());
				Exception(" BRM - 727 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 730 Guest Checkout - Verify that name on the card field should accept alphabets with a limit of 60 characters.*/
	public void payInfoGuestCCNameLengthValidation()
	{
		ChildCreation(" BRM - 730 - Guest Checkout - Verify that name on the card field should accept alphabets with a limit of 60 characters.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM730", sheet, 41);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(ccNumber))
				{
					Pass("The Credit Card Number field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						ccName.click();
						ccName.clear();
						ccName.sendKeys(Val[i]);
						//int size = ccName.getAttribute("value").length();
						if(ccName.getAttribute("value").length()<=60)
						{
							log.add("The entered value in the Credit Card Name from the excel file is " + Val[i] + " and its length is " + Val[i].length() + " . The current value in the First Name field is " + ccName.getAttribute("value").toString()+ " and its length is " + ccName.getAttribute("value").length());
							Pass("The Credit Card Name field accepts character less than 60 only.",log);
						}
						else
						{
							log.add("The entered value in the Credit Card Name from the excel file is " + Val[i] + " and its length is " + Val[i].length() + " . The current value in the First Name field is " + ccName.getAttribute("value").toString() + " and its length is " + ccName.getAttribute("value").length());
							Fail("The First Name field accepts character more than 60 .",log);
						}
					}
				}
				else
				{
					Fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 730 Issue ." + e.getMessage());
				Exception(" BRM - 730 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 907 Guest Checkout - Verify that name on the card field does not accept numbers and special characters.*/
	public void payInfoCCInvalidNameValidation()
	{
		ChildCreation("  BRM - 907 - Guest Checkout - Verify that name on the card field does not accept numbers and special characters.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM907", sheet, 38);
				String cellName = BNBasicfeature.getExcelNumericVal("BRM907", sheet, 41);
				String alert = BNBasicfeature.getExcelVal("BRM907", sheet, 10);
				if(BNBasicfeature.isElementPresent(ccNumber))
				{
					BNBasicfeature.scrolldown(ccNumber,driver);
					ccNumber.click();
					ccNumber.clear();
					ccNumber.sendKeys(cellVal);
					ccName.clear();
					ccName.sendKeys(cellName);
					jsclick(orderContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(guestError));
					Thread.sleep(200);
					log.add("The Expected alert was : " + alert);
					if(BNBasicfeature.isElementPresent(guestError))
					{
						Pass("The alert is raised to indicate user.");
						String actAlert = guestError.getText();
						log.add("The Actual alert was : " + actAlert);
						if(actAlert.contains(alert))
						{
							Pass("The alert match the expected content.",log);
						}
						else
						{
							Fail("The alert does not match the expected content.",log);
						}
					}
					else
					{
						Fail("No Validation is not raised to indicate user.");
					}
				}
				else
				{
					Fail("The Credit Card Number field is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 907 Issue ." + e.getMessage());
				Exception(" BRM - 907 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 740 Guest Checkout � Verify that security code field accepts numbers with a limit of 4 characters and alert message should be displayed.*/
	public void payInfoCCCsvLengthValidation()
	{
		ChildCreation(" BRM - 740 - Guest Checkout - Verify that security code field accepts numbers with a limit of 4 characters and alert message should be displayed.");
		if(shipPage==true)
		{
			try
			{
				String[] csvNumber = BNBasicfeature.getExcelNumericVal("BRM740", sheet, 42).split("\n");
				for(int i = 0; i<csvNumber.length;i++)
				{
					BNBasicfeature.scrolldown(ccNumber,driver);
					ccCsv.clear();
					ccCsv.sendKeys(csvNumber);
					if((ccCsv.getAttribute("value").length()>0) && (ccCsv.getAttribute("value").length()<=4))
					{
						log.add("The entered value in the CSV field from the excel file is " + csvNumber + " and its length is " + csvNumber[i].length() + " . The current value in the CSV field is " + ccCsv.getAttribute("value").toString()+ " and its length is " + ccCsv.getAttribute("value").length());
						Pass("The Credit Card CSV field accepts character less than 4 only.",log);
					}
					else
					{
						log.add("The entered value in the CSV field from the excel file is " + csvNumber + " and its length is " + csvNumber[i].length() + " . The current value in the CSV field is " + ccCsv.getAttribute("value").toString() + " and its length is " + ccCsv.getAttribute("value").length());
						Fail("The First Name field accepts character more than 4 .",log);
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 907 Issue ." + e.getMessage());
				Exception(" BRM - 907 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 733 Guest Checkout � Verify that Default text �month� and �year� should be displayed in expiration date and user should be able to select month and year from the drop down.*/
	public void payInfoGuestCCMonthYearDefaultText()
	{
		ChildCreation(" BRM - 733 - Guest Checkout - Verify that Default text month and year should be displayed in expiration date and user should be able to select month and year from the drop down.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM733", sheet, 12);
				String[] Val = cellVal.split("\n");
				BNBasicfeature.scrolldown(ccNumber,driver);
				if(BNBasicfeature.isElementPresent(ccAddDefaultMonthTxt))
				{
					String monthText = ccAddDefaultMonthTxt.getText();
					String monthText1 = ccAddDefaultMonthTxt1.getText();
					log.add("The Expected text was : " + Val[0]);
					log.add("The default text displayed in the Month drop down field is " + monthText);
					log.add("The default text displayed in the Month drop down field is " + monthText1);
					if(ccAddDefaultMonthTxt1.getText().equals(Val[0])||ccAddDefaultMonthTxt.getText().equals(Val[0]))
					{
						Pass("The default text is present for the Month Field.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and the actual value.",log);
					}
				}
				else
				{
					Fail("The default text value is not present in the Month drop down.");
				}
				
				if(BNBasicfeature.isElementPresent(ccAddDefaultYearTxt))
				{
					String yearText = ccAddDefaultYearTxt.getText();
					log.add("The Expected text was : " + Val[1]);
					log.add("The default text displayed in the Month drop down field is " + yearText);
					if(yearText.equals(Val[1]))
					{
						Pass("The default text is present for the Year Field.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and the actual value.",log);
					}
				}
				else
				{
					Fail("The default text value is not present in the Year drop down.");
				}
				
				Select mnt = new Select(ccMonth);
				Select yr = new Select(ccYear);
				Random r = new Random();
				//int msel = r.nextInt(mnt.getOptions().size());
				int mlow = 1;
				int mhigh = mnt.getOptions().size();
				int msel = 1;
				if(mhigh>1)
				{
					msel = r.nextInt(mhigh - mlow) + mlow;
				}
				
				mnt.selectByIndex(msel);
				if(ccMonth.getText().isEmpty())
				{
					Fail("The Month is not selected. Please Check");
				}
				else
				{
					log.add("The selected month is : " + mnt.getOptions().get(msel).getText());
					log.add("The selected month is : " + mnt.getFirstSelectedOption().getText());
					Pass("The Month is selected.",log);
				}
				
				int ylow = 1;
				int yhigh = yr.getOptions().size();
				int ysel = 1;
				if(yhigh>1)
				{
					ysel = r.nextInt(yhigh - ylow) + ylow;
				}
				//int ysel = r.nextInt(yr.getOptions().size());
				yr.selectByIndex(ysel);
				if(ccYear.getText().isEmpty())
				{
					Fail("The Year is not selected. Please Check");
				}
				else
				{
					log.add("The selected year is : " + yr.getOptions().get(ysel).getText());
					log.add("The selected year is : " + yr.getFirstSelectedOption().getText());
					Pass("The Year is selected.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 733 Issue ." + e.getMessage());
				Exception(" BRM - 733 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 735 Guest Checkout � Verify that on selecting the month, the list of months must be displayed in the drop down.*/
	public void payInfoGuestCCardMonthList()
	{
		ChildCreation(" BRM - 735 - Guest Checkout - Verify that on selecting the month, the list of months must be displayed in the drop down.");
		if(shipPage==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(ccMonth))
				{
					log.add("The Month field is displayed.");
					act = new Actions(driver);
					act.moveToElement(ccMonth).click().build().perform();
					//act.build().perform();
					Select mnthsel = new Select(ccMonth);
					int size = mnthsel.getOptions().size();
					if(size==13)
					{
						for(int i = 1; i <size; i++)
						{
							log.add("The Month list includes " + mnthsel.getOptions().get(i).getText());
						}
						Pass("The Month Size is : " + size,log);
					}
					else
					{
						Fail("The Month size is less than 12. Please Check");
					}
				}
				else
				{
					Fail("The Month field is not displayed.");
				}
				act.moveToElement(ccMonth).click().build().perform();
				//ccMonth.click();
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 735 Issue ." + e.getMessage());
				Exception(" BRM - 735 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 738 Guest Checkout � Verify that on selecting the year option then the list of years from the current year should be displayed.*/
	public void payInfoGuestCCardYearList()
	{
		ChildCreation(" BRM - 738 - Guest Checkout - Verify that on selecting the month, the list of months must be displayed in the drop down.");
		if(shipPage==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(ccYear))
				{
					log.add("The Year field is displayed.");
					Select yrsel = new Select(ccYear);
					act = new Actions(driver);
					act.moveToElement(ccYear).click().build().perform();
					if(yrsel.getOptions().size()>0)
					{
						Pass("The Year drop down is clicked and it is not empty");
						int year = Calendar.getInstance().get(Calendar.YEAR);
						String yr = Integer.toString(year);
						if(yr.equals(yrsel.getOptions().get(1).getText()))
						{
							log.add("The current year is " + yr);
							log.add("The first value in the year drop down is " + yrsel.getOptions().get(1).getText());
							//act.sendKeys(Keys.TAB).build().perform();
							act.click().build().perform();
							Pass("The Current year and the drop down first value matches",log);
						}
						else
						{
							log.add("The current year is " + yr);
							log.add("The first value in the year drop down is " + yrsel.getOptions().get(1).getText());
							act.click().build().perform();
							Fail("The Current year and the drop down first value down not matches",log);
						}
					}
					else
					{
						Fail("The Month size is less than 12. Please Check");
					}
				}
				else
				{
					Fail("The Month field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 738 Issue ." + e.getMessage());
				Exception(" BRM - 738 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 741  Guest Checkout � Verify that clicking on "I" icon should display a information overlay and again clicking on "I" icon should close the information overlay.*/
	public void payInfoCCiicon()
	{
		ChildCreation(" BRM - 741 - Guest Checkout - Verify that clicking on I icon should display a information overlay and again clicking on I icon should close the information overlay.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(cciicon))
				{
					Pass("The i icon is present in the Add Credit Card page and it is visible.");
					jsclick(cciicon);
					wait.until(ExpectedConditions.attributeContains(cciicontooltip, "style", "absolute"));
					wait.until(ExpectedConditions.attributeContains(ccicontooltipMask, "style", "block"));
					if(BNBasicfeature.isElementPresent(cciicontooltip))
					{
						if(cciicontooltip.getAttribute("style").contains("absolute"))
						{
							Pass("The tooltip i icon is clicked and it is visible for the user.");
							wait.until(ExpectedConditions.attributeContains(ccicontooltipMask, "style", "block"));
							if(cciicontooltip.getAttribute("style").contains("block"))
							{
								Fail("The Container is not Closed.");
							}
							else
							{
								Pass("The tooltip Container is Closed.");
								jsclick(ccicontooltipMask);
							}
						}
						else
						{
							Fail("The tooltip i icon is clicked and it is not visible for the user.");
						}
					}
					else
					{
						Fail("The tooltip is not visible for the user.");
					}
				}
				else
				{
					Fail("The i icon is not present in the Add Credit Card page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 741 Issue ." + e.getMessage());
				Exception(" BRM - 741 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 747 Guest Checkout � Verify that on entering the invalid email address, then the alert must be displayed.*/
	public void payInfoCCShipsAddressInvalidEmailIdValidation()
	{
		ChildCreation(" BRM - 747 - Guest Checkout - Verify that on entering the invalid email address, then the alert must be displayed.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM747", sheet, 45);
				String alert = BNBasicfeature.getExcelVal("BRM747", sheet, 10);
				if(BNBasicfeature.isElementPresent(guestEmailAddress))
				{
					Pass("The Email Address field is displayed.");
					guestEmailAddress.click();
					guestEmailAddress.clear();
					guestEmailAddress.sendKeys(cellVal);
					log.add("The Entered Value was : " + cellVal);
					String currEmail = guestEmailAddress.getAttribute("value");
					if(currEmail.equals(cellVal))
					{
						Pass("The entered email address and the current email address matches.",log);
					}
					else
					{
						Fail("The entered email address and the current email address does not match.",log);
					}
					
					jsclick(orderContinueBtn);
					wait.until(ExpectedConditions.visibilityOf(guestError));
					BNBasicfeature.scrollup(guestError, driver);
					Thread.sleep(200);
					log.add("The Expected alert was : " + alert);
					String actAlert = guestError.getText();
					log.add("The Actual Alert was : " + actAlert);
					if(actAlert.contains(alert))
					{
						Pass("The alert matches the expected content.",log);
					}
					else
					{
						Fail("The alert does not matches the expected content.",log);
					}
					
					BNBasicfeature.scrolldown(guestEmailAddress, driver);
					Thread.sleep(250);
					guestEmailAddress.clear();
				}
				else
				{
					Fail("The Email Address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 747 Issue ." + e.getMessage());
				Exception(" BRM - 747 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 745 Guest Checkout � Verify that user is able to enter a email address in email address field*/
	public void payInfoCCShipsAddressEmailId()
	{
		ChildCreation(" BRM - 745 - Guest Checkout - Verify that user is able to enter a email address in email address field");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM745", sheet, 45);
				if(BNBasicfeature.isElementPresent(guestEmailAddress))
				{
					Pass("The Email Address field is displayed.");
					guestEmailAddress.click();
					guestEmailAddress.clear();
					guestEmailAddress.sendKeys(cellVal);
					log.add("The Entered Value was : " + cellVal);
					String currEmail = guestEmailAddress.getAttribute("value");
					if(currEmail.equals(cellVal))
					{
						Pass("The entered email address and the current email address matches.",log);
					}
					else
					{
						Fail("The entered email address and the current email address does not match.",log);
					}
				}
				else
				{
					Fail("The Email Address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 745 Issue ." + e.getMessage());
				Exception(" BRM - 745 Issue ." + e.getMessage());
			}
			guestEmailAddress.clear();
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 742 Guest Checkout � Verify that �Same as Shipping address� should be selected by default.*/
	public void payInfoCCShipsameasdefault()
	{
		ChildCreation("  BRM - 742 - Guest Checkout - Verify that �Same as Shipping address� should be selected by default.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM742", sheet, 10);
				BNBasicfeature.scrollup(ccNumber, driver);
				if(BNBasicfeature.isElementPresent(guestSameAsShippingChkboxContainer))
				{
					Pass("The Same as Shipping Address Container is displayed.");
					if(BNBasicfeature.isElementPresent(guestSameAsShippingChkboxLabel))
					{
						Pass("The Shipping Address label is displayed.");
						String label = guestSameAsShippingChkboxLabel.getText();
						log.add("The Expected label was : " + cellVal);
						log.add("The Actual label is : " + label);
						if(cellVal.contains(label))
						{
							Pass("The Check box label match the expected content.",log);
						}
						else
						{
							Fail("The Check box label does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The Checkbox label is not displayed.");
					}
				}
				else
				{
					Fail("The Checkbox container is not dispalyed.");
				}
					
				boolean chkBoxchkd = guestSameAsShippingChkbox.isSelected();
				if(chkBoxchkd==true)
				{
					Pass("The Checkbox is selected by default.");
				}
				else
				{
					Fail("The Checkbox is not selected by default.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 742 Issue ." + e.getMessage());
				Exception(" BRM - 742 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 743 Guest Checkout � Verify that if a user disables �Same as shipping address� then shipping address overlay should be displayed with all the input fields.*/
	public void payInfoCCShipsAddressOverlay()
	{
		ChildCreation("  BRM - 743 - Guest Checkout - Verify that if a user disables Same as shipping address then shipping address overlay should be displayed with all the input fields.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(guestSameAsShippingChkboxContainer))
				{
					Pass("The Same as Shipping Address Container is displayed.");
					boolean chkBoxchkd = guestSameAsShippingChkbox.isSelected();
					if(chkBoxchkd==true)
					{
						Pass("The Checkbox is selected by default.");
						jsclick(guestSameAsShippingChkbox);
						Thread.sleep(250);
						boolean addLd = isAttribtuePresent(guestSameAsShippingContainer, "style");
						Thread.sleep(250);
						if(addLd==true)
						{
							if(BNBasicfeature.isElementPresent(guestSameAsShippingContainer))
							{
								Pass("The Shipping Container is displayed.");
							}
							else
							{
								Fail("The Shipping Container is not displayed.");
							}
						}
						else
						{
							Fail("The Shipping Container is not displayed.");
						}
					}
					else
					{
						Fail("The Checkbox is not selected by default.");
					}
				}
				else
				{
					Fail("The Checkbox container is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 743 Issue ." + e.getMessage());
				Exception(" BRM - 743 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 782 Guest Checkout � Verify and validate all the input fields in shipping address overlay.*/
	public void payInfoCCShipAddressDetails()
	{
		ChildCreation(" BRM - 782 - Guest Checkout - Verify and validate all the input fields in shipping address overlay.");
		if(shipPage==true)
		{
			try
			{
				BNBasicfeature.scrolldown(countrySelection,driver);
				Thread.sleep(500);
				
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Pass("The Country Selection drop down field is displayed.");
				}
				else
				{
					Fail("The Country Selection drop down field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(fName))
				{
					Pass("The First Name field is displayed.");
				}
				else
				{
					Fail("The First Name field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name field is displayed.");
				}
				else
				{
					Fail("The Last Name field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Street Address field is displayed.");
				}
				else
				{
					Fail("The Street Address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					Pass("The Apt Suite (Optional) Address field is displayed.");
				}
				else
				{
					Fail("The Apt Suite (Optional) field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State drop down field is displayed.");
				}
				else
				{
					Fail("The State drop down field is not displayed.");
				}
				
				BNBasicfeature.scrolldown(city,driver);
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City field is displayed.");
				}
				else
				{
					Fail("The City field is not displayed.");
				}
				
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					Pass("The Zipcode field is displayed.");
				}
				else
				{
					Fail("The Zipcode field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Contact Number field is displayed.");
				}
				else
				{
					Fail("The Contact Number field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name (Optional) field is displayed.");
				}
				else
				{
					Fail("The Company Name (Optional) field is not displayed.");
				}
				
				jsclick(guestSameAsShippingChkbox);
				Thread.sleep(500);
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 782 Issue ." + e.getMessage());
				Exception(" BRM - 782 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 754 Guest Checkout � Verify that Membership, Gift cards & coupons section should be displayed with three categories: B&N Gift cards, Coupon code and Book Fair ID */
	public void payInfoCCCouponConatainer()
	{
		ChildCreation(" BRM - 754 - Guest Checkout - Verify that Membership, Gift cards & coupons section should be displayed with three categories: B&N Gift cards, Coupon code and Book Fair ID .");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM754", sheet, 12);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(couponContainer))
				{
					BNBasicfeature.scrolldown(couponContainer, driver);
					Pass("The Coupon Container is found.");
					if(BNBasicfeature.isElementPresent(couponContainerTitle))
					{
						String title = couponContainerTitle.getText();
						log.add("The Expected title was : " + Val[0]);
						log.add("The Actual title is : " + title);
						if(title.equals(Val[0]))
						{
							Pass("The title matches the expected content .",log);
						}
						else
						{
							Fail("There is mismatch in the Title.",log);
						}
					}
					else
					{
						Fail( "The Coupon Title is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon Container is not found.");
				}
				
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					Pass("The Loyalty Container list is displayed.");
					
					if(BNBasicfeature.isElementPresent(gftCardTit))
					{
						String txt = gftCardTit.getText();
						if(txt.isEmpty())
						{
							Fail("The gift catd title is not displayd/empty.");
						}
						else
						{
							Pass("The Gift Card Title is displayed.");
							log.add("The expected title was : " + Val[1]);
							log.add("The actual title is : " + gftCardTit.getText());
							/*System.out.println(title);
							System.out.println(Val[1]);*/
							if(txt.contains(Val[1]))
							{
								Pass("The expected title and the actual title matches.",log);
							}
							else
							{
								Fail("The expected title and the actual title does not matches.",log);
							}
						}
					}
					else
					{
						Fail( "The Gift Card title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponContainerCouponCodeTitle))
					{
						log.add("The expected title was : " + Val[2]);
						String title = couponContainerCouponCodeTitle.getText();
						log.add("The actual title is : " + title);
						/*System.out.println(title);
						System.out.println(Val[1]);*/
						if(title.contains(Val[2]))
						{
							Pass("The expected title and the actual title matches.");
						}
						else
						{
							Fail("The expected title and the actual title does not matches.");
						}
					}
					else
					{
						Fail("The Coupon Code text is not dispalyed.");
					}
					
					if(BNBasicfeature.isElementPresent(couponContainerBookFairTitle))
					{
						log.add("The expected title was : " + Val[3]);
						String title = couponContainerBookFairTitle.getText();
						log.add("The actual title is : " + title);
						/*System.out.println(title);
						System.out.println(Val[1]);*/
						if(title.contains(Val[3]))
						{
							Pass("The expected title and the actual title matches.");
						}
						else
						{
							Fail("The expected title and the actual title does not matches.");
						}
					}
					else
					{
						Fail("The Coupon Code text is not dispalyed.");
					}
				}
				else
				{
					Fail("The Loyalty Container list is not displayed.");
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 754 Issue ." + e.getMessage());
				Exception(" BRM - 754 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 755 Guest Checkout � Verify that clicking on + in B&N Gift cards should display the Card number, pin input fields and apply button.*/
	public void payInfoCCGiftCardConatainer()
	{
		ChildCreation(" BRM - 755 - Guest Checkout - Verify that clicking on + in B&N Gift cards should display the Card number, pin input fields and apply button. ");
		if(shipPage==true)
		{
			try
			{
				String cellVal= BNBasicfeature.getExcelNumericVal("BRM755", sheet, 12);
				String expColor = BNBasicfeature.getExcelNumericVal("BRM755", sheet, 11);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					Pass("The Loyalty Container list is displayed.");
					
					if(BNBasicfeature.isElementPresent(gftCardImg))
					{
						Pass("The Gift Card Image is displayed and not broken.",log);
					}
					else
					{
						Fail("The Gift Card Image is not displayed and it is broken.",log);
					}
					
					if(BNBasicfeature.isElementPresent(gftCardTit))
					{
						String txt = gftCardTit.getText();
						if(txt.isEmpty())
						{
							Fail("The gift catd title is not displayd/empty.");
						}
						else
						{
							Pass("The Gift Card Title is displayed.");
							log.add("The expected title was : " + Val[1]);
							log.add("The actual title is : " + gftCardTit.getText());
							/*System.out.println(title);
							System.out.println(Val[1]);*/
							if(txt.contains(Val[1]))
							{
								Pass("The expected title and the actual title matches.",log);
							}
							else
							{
								Fail("The expected title and the actual title does not matches.",log);
							}
						}
					}
					else
					{
						Fail( "The Gift Card title is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(giftCardsOpenTrigger))
					{
						jsclick(giftCardsOpenTrigger);
						wait.until(ExpectedConditions.visibilityOf(couponGiftCardsActive));
						Thread.sleep(200);
						if(BNBasicfeature.isElementPresent(couponCreditCard))
						{
							Pass("The Credit Card Number field in the Coupon Container is displayed.");
						}
						else
						{
							Fail("The Credit Card Number field in the Coupon Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCreditCardPin))
						{
							Pass("The Credit Card Number Pin field in the Coupon Container is displayed.");
						}
						else
						{
							Fail("The Credit Card Number Pin field in the Coupon Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
						{
							Pass("The Apply button in Credit Card Section of the Coupon Container is displayed.");
							log.add("The Expected content was : " + Val[2]);
							String caption = couponCreditCardSubmit.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[2]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCreditCardSubmit.getCssValue("background").substring(0,16);;
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Apply button in Credit Card Section of the Coupon Container is not displayed.");
						}
					}
					else
					{
						Fail("The Gift Card Open Trigger is nor displayed.");
					}
				}
				else
				{
					Fail("The Loyalty Container list is not displayed.");
				}
			}
			catch (Exception e)
			{
				System.out.println(" BRM - 755 Issue ." + e.getMessage());
				Exception(" BRM - 755 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 758 Guest Checkout � Verify that Card number should accept maximum of 19 characters and pin number should accept 4 characters and it should be masked by round dots. Default text should be displayed for both the fields.*/
	public void payInfoCCGiftCardLengthValidationandDefaultText()
	{
		ChildCreation("BRM - 758 - Guest Checkout - Verify that Card number should accept maximum of 19 characters and pin number should accept 4 characters and it should be masked by round dots. Default text should be displayed for both the fields.");
		if(shipPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM758", sheet, 38);
				String[] Val = cellVal.split("\n");
				String cellVal1 = BNBasicfeature.getExcelNumericVal("BRM758", sheet, 42);
				String[] Val1 = cellVal1.split("\n");
				String label = BNBasicfeature.getExcelVal("BRM758", sheet, 12);
				String[] labelVal = label.split("\n");
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(couponCreditCard))
					{
						log.add("The Credit Card field is displayed.");
						couponCreditCard.click();
						if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
						{
							couponCreditCardTextBox.clear();
							couponCreditCardTextBox.sendKeys(Val[i]);
							if(BNBasicfeature.isElementPresent(couponCreditCardPin)&&(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox)))
							{
								couponCreditCardPin.click();
								couponCreditCardPinTextBox.clear();
								couponCreditCardPinTextBox.sendKeys(Val1[i]);
								Thread.sleep(100);
								log.add("The Expected length in the credit card field is  : " + Val[i]);
								int curcclen =  couponCreditCardTextBox.getAttribute("value").length();
								log.add("The actual length is :  " + curcclen);
								if(curcclen<=19)
								{
									Pass("The Card Number length is as expected.",log);
								}
								else
								{
									Fail("The Card Number length is not as expected.",log);
								}
								
								log.add("The Expected length in the pin  field is  : " + Val[i]);
								int curpinlen =  couponCreditCardPinTextBox.getAttribute("value").length();
								log.add("The actual length is :  " + curpinlen);
								if(curpinlen<=4)
								{
									Pass("The Card PIN length is as expected.",log);
								}
								else
								{
									Fail("The Card PIN length is not as expected.",log);
								}
							}
							else
							{
								Fail("The Credit Card PIN field is not displayed.");
							}
						}
						else
						{
							Fail("The Credit Card text field is not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card field is not displayed.");
					}
				}
			
				if(BNBasicfeature.isElementPresent(couponCreditCardLabel))
				{
					Pass("The Gift Card Default Text is displayed.");
					log.add("The expected title was : " + labelVal[0]);
					String title = couponCreditCardLabel.getText();
					log.add("The actual title is : " + title);
					/*System.out.println(title);
					System.out.println(Val[1]);*/
					if(title.contains(labelVal[0]))
					{
						Pass("The expected title and the actual title matches.");
					}
					else
					{
						Fail("The expected title and the actual title does not matches.");
					}
				}
				else
				{
					Fail("The Credit Card Default text is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(couponCreditCardPINLabel))
				{
					Pass("The Gift Card PIN Default Text is displayed.");
					log.add("The expected title was : " + labelVal[1]);
					String title = couponCreditCardPINLabel.getText();
					log.add("The actual title is : " + title);
					/*System.out.println(title);
					System.out.println(Val[1]);*/
					if(title.contains(labelVal[1]))
					{
						Pass("The expected title and the actual title matches.");
					}
					else
					{
						Fail("The expected title and the actual title does not matches.");
					}
				}
				else
				{
					Fail("The Credit Card PIN Default text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 758 Issue ." + e.getMessage());
				Exception(" BRM - 758 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 757 Guest Checkout � Verify that if a user enter a invalid card number and pin and clicking on apply, coupon should not applied and alert must me displayed.*/
	public void payInfoCCGiftCardInvalidValidation()
	{
		ChildCreation("BRM - 757 - Guest Checkout - Verify that if a user enter a invalid card number and pin and clicking on apply, coupon should not applied and alert must me displayed.");
		if(shipPage==true)
		{
			try
			{
				String expError = BNBasicfeature.getExcelVal("BRM757", sheet, 10);
				String creditCardNumber = BNBasicfeature.getExcelNumericVal("BRM757", sheet, 38);
				String creditCardPin = BNBasicfeature.getExcelNumericVal("BRM757", sheet, 42);
				if(BNBasicfeature.isElementPresent(couponCreditCard))
				{
					log.add("The Credit Card field is displayed.");
					couponCreditCard.click();
					if(BNBasicfeature.isElementPresent(couponCreditCardTextBox))
					{
						couponCreditCardTextBox.clear();
						couponCreditCardTextBox.sendKeys(creditCardNumber);
						if(BNBasicfeature.isElementPresent(couponCreditCardPin)&&(BNBasicfeature.isElementPresent(couponCreditCardPinTextBox)))
						{
							couponCreditCardPin.click();
							couponCreditCardPinTextBox.clear();
							couponCreditCardPinTextBox.sendKeys(creditCardPin);
							if(BNBasicfeature.isElementPresent(couponCreditCardSubmit))
							{
								jsclick(couponCreditCardSubmit);
								Thread.sleep(500);
								wait.until(ExpectedConditions.attributeContains(couponCreditCardError, "aria-atomic", "true"));
								Thread.sleep(500);
								log.add("The Expected Error is : " + expError);
								String err1 = couponCreditCardError.getText();
								log.add("The actual error is :  " + err1);
								if(err1.contains("unexpected"))
								{
									Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + err1);
								}
								else if(expError.contains(err1))
								{
									Pass("The actual alert match the expected alert.",log);
								}
								else
								{
									Fail("The actual alert does not match the expected alert.",log);
								}
							}
							else
							{
								Fail( "The Coupon Credit Card Submit button is not displayed.");
							}
						}
						else
						{
							Fail( "The Pin Field are not displayed.");
						}
					}
					else
					{
						Fail("The Credit Card text field is not displayed.");
					}
				}
				else
				{
					Fail("The Credit Card field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 757 Issue ." + e.getMessage());
				Exception(" BRM - 757 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 759 Guest Checkout � Verify that clicking on cross mark should close the expanded B&N Gift cards.*/
	public void payInfoCCGiftCardClose()
	{
		ChildCreation("BRM - 759 - Guest Checkout - Verify that clicking on cross mark should close the expanded B&N Gift cards.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
				{
					Pass("The Gift Card Container is Active and the fields are displayed.");
					if(BNBasicfeature.isElementPresent(giftCardsOpenTrigger))
					{
						jsclick(giftCardsOpenTrigger);
						Thread.sleep(1000);
						if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
						{
							Fail("The Gift Cards Container is still active.");
						}
						else
						{
							Pass("The Gift Cards Container is not active.");
						}
					}
					else
					{
						Fail("The Gift Cards Close trigger is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Cards Container is not active.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 759 Issue ." + e.getMessage());
				Exception(" BRM - 759 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 760 Guest Checkout � Verify that clicking on + in Coupon code should display a input field and apply button.*/
	public void payInfoCCGiftCouponCodeFields()
	{
		ChildCreation("BRM - 760 - Guest Checkout - Verify that clicking on + in Coupon code should display a input field and apply button.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(couponCouponOpenTrigger))
				{
					jsclick(couponCouponOpenTrigger);
					Thread.sleep(1000);
					String expColor = BNBasicfeature.getExcelNumericVal("BRM760", sheet, 11);
					String cellVal = BNBasicfeature.getExcelNumericVal("BRM760", sheet, 12);
					String[] Val = cellVal.split("\n");
					if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
					{
						if(BNBasicfeature.isElementPresent(couponCodeApplyContainer))
						{
							Pass("The Coupon Code text field is displayed.");
						}
						else
						{
							Fail("The Coupon Code text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeDefaultText))
						{
							Pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default Text was : " + Val[1]);
							String dfltText = couponCodeDefaultText.getText();
							log.add("The actual default text is : " + dfltText);
							if((Val[1].contains(dfltText)))
							{
								Pass("The Default text matches the expected content.",log);
							}
							else
							{
								Fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							Fail("The Default text is not displayed for the Coupon field");
						}
						
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							Pass("The Coupon Code Apply button is displayed.");
							String caption = couponCodeApplyButton.getAttribute("value");
							log.add("The expected value was : " + Val[0]);
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[0]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
							
							String csVal = couponCodeApplyButton.getCssValue("background").substring(0,16);
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color of the button is : " + expColor);
							log.add("The actual color of the button is : " + hexCode);
							if(hexCode.equals(expColor))
							{
								Pass("The Apply button color matches the expected one.",log);
							}
							else
							{
								Fail("The Apply button color does not matches the expected one.",log);
							}
						}
						else
						{
							Fail("The Coupon Code Apply button is not displayed.");
						}
					}
					else
					{
						Fail("The Loyalty Container list is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon Code Container expand + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 760 Issue ." + e.getMessage());
				Exception(" BRM - 760 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 762 Guest Checkout � Verify that if a user enter a invalid Coupon code and clicking on apply, coupon should not applied and alert must me displayed. Default text should be displayed for input field.*/
	public void payInfoCCGiftCouponCodeInvalidValidation()
	{
		ChildCreation(" BRM - 762 - Guest Checkout - Verify that if a user enter a invalid Coupon code and clicking on apply, coupon should not applied and alert must me displayed. Default text should be displayed for input field.");
		if(shipPage==true)
		{
			try
			{
				String cpnCode = BNBasicfeature.getExcelNumericVal("BRM762", sheet, 42);
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM760", sheet, 12);
				String[] defText = cellVal.split("\n");
				String cpnAlert = BNBasicfeature.getExcelVal("BRM762", sheet, 10);
				String expColor = BNBasicfeature.getExcelVal("BRM762", sheet, 11);
				if(BNBasicfeature.isElementPresent(couponCodeTextField))
				{
					Pass("The Coupon Code Container is displayed.");
					if(BNBasicfeature.isElementPresent(couponCodeTextFieldActive))
					{
						couponCodeTextFieldActive.click();
						couponCodeTextFieldActive.clear();
						/*Actions act = new Actions(driver);
						act.sendKeys(cpnCode).build().perform();*/
						couponCodeTextFieldActive.sendKeys(cpnCode);
						if(BNBasicfeature.isElementPresent(couponCodeApplyButton))
						{
							jsclick(couponCodeApplyButton);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(couponCouponError, "aria-atomic", "true"));
							Thread.sleep(500);
							log.add("The Expected alert was : " + cpnAlert);
							//String actAlert = couponCouponError.findElement(By.xpath("//*[@class='err']")).getText();
							String actAlert = couponCouponError.getText();
							log.add("The actual alert is : " +actAlert);
							if(actAlert.contains("unexpected"))
							{
								Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + actAlert);
							}
							else if(cpnAlert.contains(actAlert))
							{
								Pass("The expected alert and actual alert match.",log);
							}
							else
							{
								Fail("The expected alert and actual alert does not match.",log);
							}
					
							String csVal = couponCodeTextFieldActive.getCssValue("border-color");
							Color colorhxcnvt = Color.fromString(csVal);
							String hexCode = colorhxcnvt.asHex();
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Coupon Code field is highlighted.",log);
							}
							else
							{
								Fail("The Coupon Code field is not highlighted.",log);
							}
							
							if(BNBasicfeature.isElementPresent(couponCodeDefaultText))
							{
								Pass("The Default text is displayed for the Coupon field");
								log.add("The Expected Default Text was : " + defText[1]);
								String dfltText = couponCodeDefaultText.getText();
								log.add("The actual default text is : " + dfltText);
								if((defText[1].contains(dfltText)))
								{
									Pass("The Default text matches the expected content.",log);
								}
								else
								{
									Fail("The Default text does not match the expected content.",log);
								}
							}
							else
							{
								Fail("The Default text is not displayed for the Coupon field");
							}
						}
						else
						{
							Fail("The Apply Button in the Coupon field is not displayed.");
						}
					}
					else
					{
						Fail("The text field is not displayed for the Coupon field");
					}
				}
				else
				{
					Fail("The Coupon Code Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 762 Issue ." + e.getMessage());
				Exception(" BRM - 762 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 763 Guest Checkout � Verify that clicking on cross mark closes the expanded Coupon code.*/
	public void payInfoCCGiftCouponCodeClose()
	{
		ChildCreation(" BRM - 763 - Guest Checkout - Verify that clicking on cross mark closes the expanded Coupon code.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
				{
					Pass("The Gift Card Container is Active and the fields are displayed.");
					if(BNBasicfeature.isElementPresent(couponCouponOpenTrigger))
					{
						jsclick(couponCouponOpenTrigger);
						Thread.sleep(500);
						if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
						{
							Fail("The Coupon Code Container is not closed.");
						}
						else
						{
							Pass("The Coupon Code Container is closed.");
						}
					}
					else
					{
						Fail("The Coupon Code Close trigger is not displayed.");
					}
				}
				else
				{
					Fail("The Coupon Code Container is not active.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 763 Issue ." + e.getMessage());
				Exception(" BRM - 763 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 764 Guest Checkout � Verify that clicking on + in Book Fair ID should display a input field and apply button.*/
	public void payInfoCCBookfairContainer()
	{
		ChildCreation(" BRM - 764 - Guest Checkout - Verify that clicking on + in Book Fair ID should display a input field and apply button.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(bookFairOpenTrigger))
				{
					jsclick(bookFairOpenTrigger);
					Thread.sleep(250);
					String cellVal = BNBasicfeature.getExcelNumericVal("BRM764", sheet, 12);
					String[] Val = cellVal.split("\n");
					if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
					{
						if(BNBasicfeature.isElementPresent(bookfairApplyContainer))
						{
							Pass("The Book Fair ID Code text field is displayed.");
						}
						else
						{
							Fail("The Book Fair ID text field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(bookFairDefaultText))
						{
							Pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default text was : " + Val[1]);
							String title = bookFairDefaultText.getText();
							log.add("The actual Default text is : " + bookFairDefaultText.getText());
							if(Val[1].contains(title))
							{
								Pass("The Default text matches the expected content.",log);
							}
							else
							{
								Fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							Fail("The Default text is not displayed for the Coupon field");
						}
						
						if(BNBasicfeature.isElementPresent(bookfairApplyButton))
						{
							Pass("The Book Fair ID Apply button is displayed.");
							String caption = bookfairApplyButton.getAttribute("value");
							log.add("The actual val is : " + caption);
							if(caption.equals(Val[0]))
							{
								Pass("The Submit button caption matches.",log);
							}
							else
							{
								Fail("The Submit button caption does not matches.");
							}
						}
						else
						{
							Fail("The Book Fair ID Apply button is not displayed.");
						}
					}
					else
					{
						Fail("The Loyalty Container list is not displayed.");
					}
				}
				else
				{
					Fail("The Book Fair Container expand + is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 764 Issue ." + e.getMessage());
				Exception(" BRM - 764 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 766 Guest Checkout � Verify that if a user enter a invalid Book Fair ID and clicking on apply, coupon should not applied and alert must me displayed.*/
	public void payInfoCCBookfairValidation()
	{
		ChildCreation(" BRM - 766 - Guest Checkout - Verify that if a user enter a invalid Book Fair ID and clicking on apply, coupon should not applied and alert must me displayed.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					String expAlert = BNBasicfeature.getExcelVal("BRM766", sheet, 10);
					String expColor = BNBasicfeature.getExcelVal("BRM766", sheet, 11);
					String bookFairID = BNBasicfeature.getExcelNumericVal("BRM766", sheet, 42);
					if(BNBasicfeature.isElementPresent(bookFairTextField))
					{
						Pass("The Apply button is displayed in the Book Fair ID section.");
						bookFairTextField.click();
						bookFairTextField.clear();
						bookFairTextField.sendKeys(bookFairID);
						if(BNBasicfeature.isElementPresent(bookfairApplyButton))
						{
							jsclick(bookfairApplyButton);
							Thread.sleep(500);
							wait.until(ExpectedConditions.attributeContains(couponBookFairError, "aria-atomic", "true"));
							Thread.sleep(500);
							log.add("The Expected alert was : " + expAlert);
							String actAlert = couponBookFairError.getText();
							log.add("The actual alert was : " + actAlert);
							if(actAlert.contains("unexpected"))
							{
								Skip(" There was some issue in applying the Coupon Code. The displayed alert was : " + actAlert);
							}
							else if(expAlert.contains(actAlert))
							{
								Pass("The raised alert match the expected content.",log);
							}
							else
							{
								Fail("The raised alert does not match the expected content.",log);
							}
							
							//String csVal = bookFairTextField.getCssValue("border").substring(0,25);
							String csVal = bookFairTextField.getCssValue("border");
							//String csVal = bookFairTextField.getCssValue("border");
							String hexCode = BNBasicfeature.colorfinder(csVal);
							log.add("The expected color was : " + expColor);
							log.add("The actual color is : " + hexCode);
							if(hexCode.contains(expColor))
							{
								Pass("The Book Fair ID number field is highlighted.",log);
							}
							else
							{
								Fail("The Book Fair ID field is not highlighted.",log);
							}
						}
						else
						{
							Fail("The Apply button is not displayed in the Book Fair ID section.");
						}
					}
					else
					{
						Fail("The  bookfair text field is not displayed in the Book Fair ID section.");
					}
				}
				else
				{
					Fail("The bookfair text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 766 Issue ." + e.getMessage());
				Exception(" BRM - 766 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 767  Guest Checkout � Verify that Book Fair ID input field should accept maximum of 12 characters and default text should be displayed for input field.*/
	public void payInfoCCBookfairLengthValidation()
	{
		ChildCreation(" BRM - 767 - Guest Checkout - Verify that Book Fair ID input field should accept maximum of 12 characters and default text should be displayed for input field.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(loyaltyContainerList))
				{
					
					String cellVal = BNBasicfeature.getExcelNumericVal("BRM767", sheet, 42);
					String[] bookFairID = cellVal.split("\n");
					String cellVal1 = BNBasicfeature.getExcelNumericVal("BRM764", sheet, 12);
					String[] Val = cellVal1.split("\n");
					if(BNBasicfeature.isElementPresent(bookFairTextField))
					{
						Pass("The Apply button is displayed in the Book Fair ID section.");
						for(int i = 0; i<bookFairID.length;i++)
						{
							bookFairTextField.click();
							bookFairTextField.clear();
							bookFairTextField.sendKeys(bookFairID[i]);
							int curlen = bookFairTextField.getAttribute("value").length();
							log.add("The Value from the File was : " + bookFairID[i] + " and its length is  : " + bookFairID[i].length());
							log.add("The Current Value is : " + curlen);
							if(curlen<=12)
							{
								Pass("The Length does not exceeds the specified limit.",log);
							}
							else
							{
								Fail("The Length exceeds the specified limit.",log);
							}
						}
						
						if(BNBasicfeature.isElementPresent(bookFairDefaultText))
						{
							Pass("The Default text is displayed for the Coupon field");
							log.add("The Expected Default text was : " + Val[1]);
							String title = bookFairDefaultText.getText();
							log.add("The actual Default text is : " + bookFairDefaultText.getText());
							if(Val[1].contains(title))
							{
								Pass("The Default text matches the expected content.",log);
							}
							else
							{
								Fail("The Default text does not match the expected conten.",log);
							}
						}
						else
						{
							Fail("The Default text is not displayed for the Coupon field");
						}
					}
					else
					{
						Fail("The Apply button is not displayed in the Book Fair ID section.");
					}
				}
				else
				{
					Fail("The bookfair text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 767 Issue ." + e.getMessage());
				Exception(" BRM - 767 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 768 Guest Checkout � Verify that clicking on cross mark should close the expanded Book Fair ID.*/
	public void payInfoCCGiftBookFairClose()
	{
		ChildCreation("BRM - 768 - Guest Checkout - Verify that clicking on cross mark should close the expanded Book Fair ID.");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
				{
					Pass("The Gift Card Container is Active and the fields are displayed.");
					if(BNBasicfeature.isElementPresent(couponCouponOpenTrigger))
					{
						jsclick(bookFairOpenTrigger);
						Thread.sleep(1000);
						if(BNBasicfeature.isElementPresent(couponGiftCardsActive))
						{
							Fail("The Book Fair Container is not closed.");
						}
						else
						{
							Pass("The Book Fair Container is closed.");
						}
					}
					else
					{
						Fail("The Book Fair Close trigger is not displayed.");
					}
				}
				else
				{
					Fail("The Book Fair Conatainer is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 768 Issue ." + e.getMessage());
				Exception(" BRM - 768 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 769 Guest Checkout � Verify that subtotal (number of items) and the total amount is displayed below the Book fair ID.*/
	public void payInfoCCGiftSummaryContainerSubtotal()
	{
		ChildCreation("BRM - 769 - Guest Checkout - Verify that subtotal (number of items) and the total amount is displayed below the Book fair ID.");
		if(shipPage==true)
		{
			try
			{
				String title = BNBasicfeature.getExcelNumericVal("BRM769", sheet, 12);
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					BNBasicfeature.scrolldown(orderSummaryContainer, driver);
					String label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[1]//*[@class])[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[1]//*[@class])[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						Pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						Fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						Pass(" The Price is displayed.",log);
					}
					else
					{
						Fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 769 Issue ." + e.getMessage());
				Exception(" BRM - 769 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 770 Guest Checkout � Verify that Estimated shipping and the amount is displayed below the subtotal*/
	public void payInfoCCGiftSummaryContainerEstimatedShip()
	{
		ChildCreation("BRM - 770 - Guest Checkout - Verify that Estimated shipping and the amount is displayed below the subtotal.");
		if(shipPage==true)
		{
			try
			{
				String title = BNBasicfeature.getExcelNumericVal("BRM770", sheet, 12);
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[2]//*[@class])[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[2]//*[@class])[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						Pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						Fail("The Expected label was not displayed.",log);
					}
					
					try
					{
						Float val = Float.parseFloat(details.getText().replace("$", ""));
						log.add("The displayed price was : " + details.getText());
						if(val>=0)
						{
							Pass(" The Price is displayed.",log);
						}
						else
						{
							Fail("There is something wrong. Please Check.",log);
						}
					}
					catch(Exception e)
					{
						String text = details.getText();
						log.add("The displayed value was : " + details.getText());
						if(text.equalsIgnoreCase("FREE"))
						{
							Pass(" The Value is displayed.",log);
						}
						else
						{
							Fail("There is something wrong. Please Check.",log);
						}
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 770 Issue ." + e.getMessage());
				Exception(" BRM - 770 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 771 Guest Checkout � Verify that estimated tax and the amount is displayed below the estimated shipping.*/
	public void payInfoCCGiftSummaryContainerEstimatedTax()
	{
		ChildCreation(" BRM - 771 - Guest Checkout - Verify that estimated tax and the amount is displayed below the estimated shipping.");
		if(shipPage==true)
		{
			try
			{
				String title = BNBasicfeature.getExcelNumericVal("BRM771", sheet, 12);
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[3]//*[@class])[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[3]//*[@class])[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.contains(title))
					{
						Pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						Fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						Pass(" The Price is displayed.",log);
					}
					else
					{
						Fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 771 Issue ." + e.getMessage());
				Exception(" BRM - 771 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 772 Guest Checkout � Verify that ORDER TOTAL and its amount is displayed above the Submit order.*/
	public void payInfoCCGiftSummaryContainerOrderTotal()
	{
		ChildCreation(" BRM - 772 - Guest Checkout - Verify that ORDER TOTAL and its amount is displayed above the Submit order.");
		if(shipPage==true)
		{
			try
			{
				String title = BNBasicfeature.getExcelNumericVal("BRM772", sheet, 12);
				if(BNBasicfeature.isElementPresent(orderSummaryContainer))
				{
					Pass("The Summary Container is displayed.");
					String label = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[4]//*[@class])[1]")).getText();
					WebElement details = driver.findElement(By.xpath("((//*[@class='order-summary']//*[@class='order-summary-table__row'])[4]//*[@class])[2]"));
					log.add("The Expected title was : " + title);
					log.add("The Actual title is : " + label);
					if(label.equalsIgnoreCase(title))
					{
						Pass("The " + label+ " line is found and it is displayed.",log);
					}
					else
					{
						Fail("The Expected label was not displayed.",log);
					}
					
					Float val = Float.parseFloat(details.getText().replace("$", ""));
					log.add("The displayed price was : " + details.getText());
					if(val>=0)
					{
						Pass(" The Price is displayed.",log);
					}
					else
					{
						Fail("There is something wrong. Please Check.",log);
					}
				}
				else
				{
					Fail("The Summary Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 772 Issue ." + e.getMessage());
				Exception(" BRM - 772 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 779 Guest Checkout � Verify that clicking on �Other pay options� should display the other pay options (Check out with PayPal).*/
	public void payInfoCCPaypalLink()
	{
		ChildCreation(" BRM - 779 - Guest Checkout - Verify that clicking on �Other pay options� should display the other pay options (Check out with PayPal).");
		if(shipPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					BNBasicfeature.scrollup(guestPaymentTab, driver);
					Pass("The Guest Payment tab is dispalyed.");
					if(BNBasicfeature.isElementPresent(guestPaypalNotActiveTab))
					{
						Pass("The Guest Paypal Payment Tab is displayed.");
						jsclick(guestPaypalNotActiveTab);
						wait.until(ExpectedConditions.attributeContains(guestCCContainerActiveTab, "style", "none;"));
						Thread.sleep(500);
						if(BNBasicfeature.isElementPresent(guestCCContainerActiveTab))
						{
							boolean active = isAttribtuePresent(guestPaypalContainerActiveTab, "style");
							if(active==true)
							{
								Pass("The Gusest other payment Tab is active");
							}
							else
							{
								Fail("The Gusest other payment Tab is not active");
							}
							/*guestPaypalContainerActiveTab.getAttribute("style");
							if(active.contains("display: block;"))
							{
								Pass("The Gusest other payment Tab is active");
							}
							else
							{
								Fail("The Gusest other payment Tab is not active");
							}*/
							
							if(BNBasicfeature.isElementPresent(guestPaypalButton))
							{
								Pass("The Paypal button is displayed.");
							}
							else
							{
								Fail("The Paypal button is not displayed.");
							}
						}
						else
						{
							Fail("The Gusest other payment Tab is not active");
						}
					}
					else
					{
						Fail("The Guest Paypal payment tab is not displayed.");
					}
				}
				else
				{
					Fail("The Guest Payment tab is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 779 Issue ." + e.getMessage());
				Exception(" BRM - 779 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 944 Guest Checkout � Verify that on selecting Other Pay Options tab it should be highlighted*/
	public void payInfoCCOtherPayTabHighlight()
	{
		ChildCreation(" BRM - 944 - Guest Checkout - Verify that on selecting Other Pay Options tab it should be highlighted.");
		if(shipPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM943", sheet, 11);
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					if(BNBasicfeature.isElementPresent(guestPaypalActiveTab))
					{
						Pass("The Other PaymentTab is active.");
						if(BNBasicfeature.isElementPresent(guestPaypalActiveTab))
						{
							Pass("The credit card payment tab is displayed.");
							String ColorName = guestPaypalActiveTab.getCssValue("color");
							Color colorhxcnvt = Color.fromString(ColorName);
							String hexCode = colorhxcnvt.asHex();
							log.add("The Expected Color was : " + expColor);
							log.add("The Actual Color is : " + hexCode);
							if(expColor.equals(hexCode))
							{
								Pass("The Tab is highlighted with the expected color.",log);
							}
							else
							{
								Fail("The Tab is not highlighted with the expected color.",log);
							}
						}
						else
						{
							Fail("The Other Payment tab is not displayed.");
						}
					}
					else
					{
						Fail("The Other Payment Tab is not active");
					}
				}
				else
				{
					Fail("The Guest Paypal payment tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 944 Issue ." + e.getMessage());
				Exception(" BRM - 944 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 781 Guest Checkout � Verify that clicking on �Check out with PayPal� should redirect to the PayPal page.*/
	public void payInfoCCPaypalLinkNavigation() throws Exception
	{
		ChildCreation(" BRM - 781 - Guest Checkout - Verify that clicking on �Check out with PayPal� should redirect to the PayPal page.");
		if(shipPage==true)
		{
			try
			{
				String currUrl = driver.getCurrentUrl();
				String newUrl = "";
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					if(BNBasicfeature.isElementPresent(guestPaypalActiveTab))
					{
						Pass("The Other Pay Option Tab is Active");
						if(BNBasicfeature.isElementPresent(guestPaypalButton))
						{
							Pass("The Paypal button is displayed.");
							guestPaypalButton.click();
							Thread.sleep(5000);
							newUrl = driver.getCurrentUrl();
							if(newUrl.contains("paypal"))
							{
								Pass("The User is navigated to the expected page (i.e) PAYPAL.");
							}
							else
							{
								Fail("The User is not navigated to the expected page (i.e) PAYPAL.");
							}
							
							if(!newUrl.equals(currUrl))
							{
								driver.navigate().back();
								driver.navigate().refresh();
							}
						}
						else
						{
							Fail("The Paypal button is not displayed.");
						}
					}
					else
					{
						Fail("The Guest other payment Tab is not active");
					}
				}
				else
				{
					Fail("The Guest Paypal payment tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 781 Issue ." + e.getMessage());
				Exception(" BRM - 781 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
	/* BRM - 793 Guest Checkout - Verify that on updating all the payment details and clicking on Submit order should proceed to the Review order page.*/
	public void payInfoCCReviewOrderPageNavigation()
	{
		ChildCreation("BRM - 793 Guest Checkout - Verify that on updating all the payment details and clicking on Submit order should proceed to the Review order page.");
		if(shipPage==true)
		{
			try
			{
				String ccNum = BNBasicfeature.getExcelNumericVal("BRM793", sheet, 38);
				String ccNm = BNBasicfeature.getExcelVal("BRM793", sheet, 41);
				String cvv = BNBasicfeature.getExcelNumericVal("BRM793", sheet, 42);
				String email = BNBasicfeature.getExcelVal("BRM793", sheet, 45);
				wait.until(ExpectedConditions.visibilityOf(guestPaymentTab));
				Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(guestPaymentTab))
				{
					Pass("The Payment Tab is displayed.");
					boolean active = guestCCPaymentTab.isDisplayed();
					if(active==true)
					{
						BNBasicfeature.scrolldown(ccNumber, driver);
						Select mntsel = new Select(ccMonth);
						Select yrsel = new Select(ccYear);
						if(BNBasicfeature.isElementPresent(ccNumber))
						{
							Pass("The Credit Card Number is displayed.");
							ccNumber.clear();
							ccNumber.sendKeys(ccNum);
						}
						else
						{
							Fail("The Credit Card Number is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(ccName))
						{
							Pass("The Credit Card Name is displayed.");
							ccName.clear();
							ccName.sendKeys(ccNm);
						}
						else
						{
							Fail("The Credit Card Name is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(ccMonth))
						{
							Pass("The Credit Card Month drop down is displayed.");
							mntsel.selectByIndex(6);
						}
						else
						{
							Fail("The Credit Card Month drop down is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(ccYear))
						{
							Pass("The Credit Card Year drop down field is displayed.");
							yrsel.selectByIndex(6);
						}
						else
						{
							Fail("The Credit Card Year drop down field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(ccCsv))
						{
							Pass("The Credit Card Security Field CSV is displayed.");
							ccCsv.clear();
							ccCsv.sendKeys(cvv);
						}
						else
						{
							Fail("The Credit Card Security Field CSV field is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(guestEmailAddress))
						{
							BNBasicfeature.scrolldown(guestEmailAddress, driver);
							Pass("The Email Address Field is dispalyed.");
							guestEmailAddress.clear();
							guestEmailAddress.sendKeys(email);
						}
						else
						{
							Fail("The Email Address Field is not dispalyed.");
						}
						
						if(BNBasicfeature.isElementPresent(orderContinueBtn))
						{
							Pass("The Order Continue button is displayed.");
							jsclick(orderContinueBtn);
							Thread.sleep(3000);
							/*boolean noError = false;
							do
							{
								BNBasicfeature.scrolldown(summaryContainer, driver);
								orderContinueBtn.click();
								Thread.sleep(3000);
								if(BNBasicfeature.isElementPresent(guestError))
								{
									noError = true;
								}
								else
								{
									noError = false;
								}
							}while(!noError==true);*/
							
							if(BNBasicfeature.isElementPresent(guestError))
							{
								Skip("The Order is not processed due to Error. The duisplayed error was : " + guestError.getText());
							}
							else
							{
								wait.until(ExpectedConditions.visibilityOf(guestreviewHeader));
								if(BNBasicfeature.isElementPresent(guestreviewHeader))
								{
									Pass("The User is navigated to the Guest Review Order Page.");
								}
								else
								{
									Fail("The User is not navigated to the Guest Review Order Page.");
								}
							}
						}
						else
						{
							Fail("The Order Continue button is not displayed.");
						}
					}
					else
					{
						Skip("The Credit Container is not active.");
					}
				}
				else
				{
					Fail("The Guest Paypal payment tab is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 793 Issue ." + e.getMessage());
				Exception(" BRM - 793 Issue ." + e.getMessage());
			}
		}
		else
		{
			Skip("The user is not navigated to the Guest User Payments Page.");
		}
	}
	
}
