package bnPageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNPLPSearchStories;
import bnConfig.BNXpaths;

public class BNPLPSearchObj extends BNPLPSearchStories implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static ArrayList<String> facetList;
	static String searchTextResult = "", searchContentName = "", searchPrdtLimit = "",selFacVal = "", setFacVal = "", originalUrl = "", navigatedUrl = ""; 
	int searchPrdtTotalCount = 0;
	boolean facetFound = false, srcPage = false, facetRmv = false;
	
	public BNPLPSearchObj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, timeout);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+searchContainerr+"")
	WebElement searchContainer;
	
	@FindBy(xpath = ""+searchContainerCancell+"")
	WebElement searchContainerCancel;
	
	@FindBy(xpath = ""+searchResultsContainerr+"")
	WebElement searchResultsContainer;
	
	@FindBy(xpath = ""+homepageHeaderr+"")
	WebElement homepageHeader;
	
	@FindBy(xpath = ""+plpNoProductsFoundd+"")
	WebElement plpNoProductsFound;
	
	@FindBy(xpath = ""+plpSearchPageFilterContainerr+"")
	WebElement plpSearchPageFilterContainer;
	
	@FindBy(xpath = ""+searchResultss+"")
	WebElement searchResults;
	
	@FindBy(xpath = ""+noSearchResultss+"")
	WebElement noSearchResults;
	
	@FindBy(xpath = ""+searchPagePopUpMaskk+"")
	WebElement searchPagePopUpMask;
	
	@FindBy(xpath = ""+searchPageFilterSortMaskk+"")
	WebElement searchPageFilterSortMask;
	
	@FindBy (xpath = ""+pdpPageAddToWishListContainerr+"")
	WebElement pdpPageAddToWishListContainer;
	
	@FindBy(xpath = ""+productIdd+"")
	WebElement productId;
	
	@FindBy(xpath = ""+pdpPageATBBtnn+"")
	WebElement pdpPageATBBtn;
	
	@FindBy(xpath = ""+pdpPageProductNameContainerr+"")
	WebElement pdpPageProductNameContainer;
	
	@FindBy (xpath = ""+searchPageFilterButtonn+"")
	WebElement searchPageFilterButton;
	
	@FindBy (xpath = ""+searchPageSortButtonn+"")
	WebElement searchPageSortButton;
	
	@FindBy (xpath = ""+searchPageSortOptionFacett+"")
	WebElement searchPageSortOptionFacet;
	
	@FindBy (xpath = ""+searchPageFilterTabCaptionn+"")
	WebElement searchPageFilterTabCaption;
	
	@FindBy (xpath = ""+searchPageSortTabCaptionn+"")
	WebElement searchPageSortTabCaption;
	
	@FindBy (xpath = ""+searchPageFilterFacetss+"")
	WebElement searchPageFilterFacets;
	
	@FindBy (xpath = ""+searchPageFilterFacetsEnabledd+"")
	WebElement searchPageFilterFacetsEnabled;
	
	@FindBy (xpath = ""+searchPageProductCountContainerr+"")
	WebElement searchPageProductCountContainer;
	
	@FindBy (xpath = ""+gridViewListt+"")
	WebElement gridViewList;
	
	@FindBy (xpath = ""+searchPageFilterTabContainerr+"")
	WebElement searchPageFilterTabContainer;
	
	@FindBy (xpath = ""+searchPageSearchTermm+"")
	WebElement searchPageSearchTerm;
	
	@FindBy (xpath = ""+listViewListt+"")
	WebElement listViewList;

	@FindBy (xpath = ""+gridViewIconn+"")
	WebElement gridViewIcon;
	
	@FindBy (xpath = ""+listViewIconn+"")
	WebElement listViewIcon;
	
	@FindBy (xpath = ""+searchPagePromoBannerr+"")
	WebElement searchPagePromoBanner;
	
	@FindBy (xpath = ""+shareFBB+"")
	WebElement shareFB;
	
	@FindBy (xpath = ""+shareTwitterr+"")
	WebElement shareTwitter;
	
	@FindBy (xpath = ""+shareInstaa+"")
	WebElement shareInsta;
	
	@FindBy (xpath = ""+sharePinrestt+"")
	WebElement sharePinrest;
	
	@FindBy (xpath = ""+searchPagePaginationContainerr+"")
	WebElement searchPagePaginationContainer;
	
	@FindBy (xpath = ""+noShowBacktoTopp+"")
	WebElement noShowBacktoTop;
	
	@FindBy (xpath = ""+showBacktoTopp+"")
	WebElement showBacktoTop;
	
	@FindBy (xpath = ""+footerSectionContainerr+"")
	WebElement footerSectionContainer;
	
	@FindBy (xpath = ""+footerSectionMailContainerr+"")
	WebElement footerSectionMailContainer;
	
	@FindBy (xpath = ""+footerSectionMailContainerTextt+"")
	WebElement footerSectionMailContainerText;
	
	@FindBy (xpath = ""+footerSectionMailSubmitButtonn+"")
	WebElement footerSectionMailSubmitButton;
	
	@FindBy (xpath = ""+footerSectionMailEmaill+"")
	WebElement footerSectionMailEmail;
	
	@FindBy (xpath = ""+footerSectionMyAccountt+"")
	WebElement footerSectionMyAccount;
	
	@FindBy (xpath = ""+footerSectionSignInn+"")
	WebElement footerSectionSignIn;
	
	@FindBy (xpath = ""+footerSectionCustomerServicee+"")
	WebElement footerSectionCustomerService;
	
	@FindBy (xpath = ""+footerSectionStoress+"")
	WebElement footerSectionStores;
	
	@FindBy (xpath = ""+footerSectionTermsandUsee+"")
	WebElement footerSectionTermsandUse;
	
	@FindBy (xpath = ""+footerSectionPrivacyy+"")
	WebElement footerSectionPrivacy;
	
	@FindBy (xpath = ""+footerSectionCopyrightt+"")
	WebElement footerSectionCopyright;
	
	@FindBy (xpath = ""+footerSectionTermsandUsee1+"")
	WebElement footerSectionTermsandUse1;
	
	@FindBy (xpath = ""+footerSectionFullSitee+"")
	WebElement footerSectionFullSite;
	
	@FindBy (xpath = ""+searchPagePaginationElipsess+"")
	WebElement searchPagePaginationElipses;
	
	@FindBy (xpath = ""+searchPagePaginationContainerCurrentPagess+"")
	WebElement searchPagePaginationContainerCurrentPages;
	
	@FindBy (xpath = ""+searchPagePaginationRightArroww+"")
	WebElement searchPagePaginationRightArrow;
	
	@FindBy (xpath = ""+searchPagePaginationLeftArroww+"")
	WebElement searchPagePaginationLeftArrow;
	
	@FindBy (xpath = ""+searchMisSpellSuggestionn+"")
	WebElement searchMisSpellSuggestion;
	
	@FindBy (xpath = ""+searchMisSpellSuggestionTextt+"")
	WebElement searchMisSpellSuggestionText;
	
	@FindBy (xpath = ""+searchPageFilterTabOpenCaptionn+"")
	WebElement searchPageFilterTabOpenCaption;
	
	@FindBy (xpath = ""+plpMenuu+"")
	WebElement plpMenu;
	
	@FindBy (xpath = ""+plpCategoryy+"")
	WebElement plpCategory;
	
	@FindBy (xpath = ""+plpSubCategoryy+"")
	WebElement plpSubCategory;
	
	@FindBy (xpath = ""+storeTitlee+"")
	WebElement storeTitle;
	
	@FindBy (xpath = ""+plpMenuu1+"")
	WebElement plpMenu1;
	
	@FindBy (xpath = ""+plpCategoryy1+"")
	WebElement plpCategory1;
	
	@FindBy (xpath = ""+plpSubCategoryy1+"")
	WebElement plpSubCategory1;
	
	@FindBy (xpath = ""+browseSectionn+"")
	WebElement browseSection;
	
	@FindBy (xpath = ""+newToOldd+"")
	WebElement newToOld;
	
	@FindBy (xpath = ""+titleZAA+"")
	WebElement titleZA;
	
	@FindBy (xpath = ""+oldToNeww+"")
	WebElement oldToNew;
	
	@FindBy (xpath = ""+highToloww+"")
	WebElement highTolow;
	
	@FindBy (xpath = ""+ltoHH+"")
	WebElement ltoH;
	
	@FindBy (xpath = ""+highlyRatedd+"")
	WebElement highlyRated;
	
	@FindBy (xpath = ""+titleAZZ+"")
	WebElement titleAZ;

	@FindBy (xpath = ""+selectedSortt+"")
	WebElement selectedSort;
	
	@FindBy (xpath = ""+bestSellerss+"")
	WebElement bestSellers;
	
	@FindBy (xpath = ""+accordFilterr+"")
	WebElement accordFilter;
	
	@FindBy (xpath = ""+filterClosee+"")
	WebElement filterClose;
	
	@FindBy (xpath = ""+facetss+"")
	WebElement facets;
	
	@FindBy (xpath = ""+accordd+"")
	WebElement accord;
	
	@FindBy (xpath = ""+accordd1+"")
	WebElement accord1;
	
	//aa
	
	@FindBy(how = How.XPATH, using = ""+searchResultsListss+"")
	List<WebElement> searchResultsLists;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterFacetsListss+"")
	List<WebElement> searchPageFilterFacetsLists;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterFacetsNamee+"")
	List<WebElement> searchPageFilterFacetsName;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterFacetsExpandIconn+"")
	List<WebElement> searchPageFilterFacetsExpandIcon;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterFacetsValuee+"")
	List<WebElement> searchPageFilterFacetsValue;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterFacetsNameExpandedd+"")
	List<WebElement> searchPageFilterFacetsNameExpanded;
	
	@FindBy(how = How.XPATH, using = ""+searchPageProductContainerResultsListt+"")
	List<WebElement> searchPageProductContainerResultsList;
	
	@FindBy(how = How.XPATH, using = ""+searchPageProductListss+"")
	List<WebElement> searchPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+searchPageshareIconListt+"")
	List<WebElement> searchPageshareIconList;
	
	@FindBy(how = How.XPATH, using = ""+searchPageSortFacetsListss+"")
	List<WebElement> searchPageSortFacetsLists;
	
	@FindBy(how = How.XPATH, using = ""+searchPagePaginationContainerPagess+"")
	List<WebElement> searchPagePaginationContainerPages;
	
	@FindBy(how = How.XPATH, using = ""+searchPageFilterSelectedFacetsValuee+"")
	List<WebElement> searchPageFilterSelectedFacetsValue;
	
	@FindBy(how = How.XPATH, using = ""+searchMisSpellSuggestionListss+"")
	List<WebElement> searchMisSpellSuggestionLists;
	
	@FindBy(how = How.XPATH, using = ""+searchMisSpellSuggestionNavigationLinkk+"")
	List<WebElement> searchMisSpellSuggestionNavigationLink;
	
	@FindBy(how = How.XPATH, using = ""+searchResultListProductNamee+"")
	List<WebElement> searchResultListProductName;
	
	@FindBy(how = How.XPATH, using = ""+pdpPageATBBtnss+"")
	List<WebElement> pdpPageATBBtns;
	
	@FindBy(how = How.XPATH, using = ""+searchResultListAuthorNamee+"")
	List<WebElement> searchResultListAuthorName;
	
	@FindBy(how = How.XPATH, using = ""+featuredContentLinkss+"")
	List<WebElement> featuredContentLinks;
	
	@FindBy(how = How.XPATH, using = ""+plpPageProductListss+"")
	List<WebElement> plpPageProductLists;
	
	@FindBy(how = How.XPATH, using = ""+plpPageProductNamee+"")
	List<WebElement> plpPageProductName;
	
	//aa
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
		
	/* Search No Product */
	public boolean nosearchPrdt(String srctVal)
	{
		boolean pgOk = false;
		String cellVal = srctVal;
		String[] searchcont = cellVal.split("\n");
		for(int i = 0;i<searchcont.length;i++)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				//Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					Actions act = new Actions(driver);
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
					act.moveToElement(searchContainer).click().sendKeys(searchcont[i]).sendKeys(Keys.ENTER).build().perform();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(homepageHeader));
					if(BNBasicfeature.isElementPresent(plpNoProductsFound))
					{
						pgOk=true;
						break;
					}
					else if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
					{
						pgOk = false;	
						continue;
					}
					else
					{
						pgOk = false;	
						continue;
					}
				}
				else
				{
					System.out.println("The Search Icon is not found.Please Check.");
				}
			}
			catch (Exception e)
			{
				driver.navigate().refresh();
				if(BNBasicfeature.isElementPresent(plpNoProductsFound))
				{
					pgOk=false;
					continue;
				}
				else
				{
					wait.until(ExpectedConditions.visibilityOf(plpSearchPageFilterContainer));
					pgOk = true;	
					break;
				}
			}
		}
		return pgOk;
	}
	
	/* Search Product */
	public boolean searchPrdt(String srctVal)
	{
		boolean pgOk = false;
		String cellVal = srctVal;
		String[] searchcont = cellVal.split("\n");
		for(int i = 0;i<searchcont.length;i++)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(searchResults))
				{
					if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
					{
						jsclick(searchResults);
						BNBasicfeature.scrollup(header, driver);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
					}
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
				{
					jsclick(searchPageFilterButton);
					BNBasicfeature.scrollup(header, driver);
					Thread.sleep(50);
				}
				
				wait.until(ExpectedConditions.visibilityOf(header));
				//Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(searchIcon));
				wait.until(ExpectedConditions.elementToBeClickable(searchIcon));
				//Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					//searchIcon.click();
					jsclick(searchIcon);
					Actions act = new Actions(driver);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
					act.moveToElement(searchContainer).click().sendKeys(searchcont[i]).sendKeys(Keys.ENTER).build().perform();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(homepageHeader));
					if(BNBasicfeature.isElementPresent(plpNoProductsFound))
					{
						pgOk=false;
						continue;
					}
					else if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
					{
						pgOk=false;
						continue;
					}
					else if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
					{
						pgOk = true;	
						break;
					}
					else
					{
						pgOk=false;
						continue;
					}
				}
				else
				{
					System.out.println("The Search Icon is not found.Please Check.");
				}
			}
			catch (Exception e)
			{
				driver.navigate().refresh();
				if(BNBasicfeature.isElementPresent(plpNoProductsFound))
				{
					pgOk=false;
					continue;
				}
				else if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
				{
					pgOk = true;	
					break;
				}
				else
				{
					pgOk=false;
					continue;
				}
			}
		}
		return pgOk;
	}
	
	// Get Filter Value Size
	public List<WebElement> getfilterValues(int i)
	{
		List<WebElement> ele = null;
		try
		{
			ele = driver.findElements(By.xpath("(//*[contains(@class,'facetOptions')]//*[contains(@class,'plpFacetValueCont')])["+(i+1)+"]//*[@class]"));
		}
		catch(Exception e)
		{
			System.out.println(" Error in getting filter facet value.");
		}
		return ele;
	}
	
	/* Get Results Page Container Details */
	public void resultContainerDetails()
	{
		try
		{
			//BNBasicfeature.scrollup(header, driver);
			wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
			Thread.sleep(100);
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{
				searchTextResult = searchPageProductCountContainer.getText();
			}
			boolean detDisp = searchPageProductCountContainer.getAttribute("style").contains("block");
			if(detDisp==true)
			{
				wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductContainerResultsList));
				if(BNBasicfeature.isListElementPresent(searchPageProductContainerResultsList))
				{
					Pass("The Product Count Container List is displayed.");
					searchPrdtLimit = searchPageProductContainerResultsList.get(0).getText();
					searchPrdtTotalCount = Integer.parseInt(searchPageProductContainerResultsList.get(1).getText().replace(",", ""));
					searchContentName = searchPageProductContainerResultsList.get(2).getText();
				}
				else
				{
					Fail("The Product Count Container list is not displayed.");
				}
			}
			else
			{
				Fail("The Search Term Result Contianer is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
	}
	
	// Filter Enable
	public void filterEnable()
	{
		try
		{
			boolean enabled = false;
			WebDriverWait w1 = new WebDriverWait(driver, 1);
			do
			{
				try
				{
					jsclick(searchPageFilterButton);
					w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					enabled = true;
				}
				catch(Exception e)
				{
					enabled = false;
					enabled = searchPageFilterFacets.getAttribute("style").contains("block");
				}
			}while(enabled==false);
			wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
		}
		catch(Exception e)
		{
			System.out.println(" Issue in Enabling the filter." );
			Exception("Issue in Enabling the filter.");
		}
	}
	
	// Sort Enable
	public void enableSort()
	{
		Actions act = new Actions(driver);
		try
		{
			boolean enabled = false;
			WebDriverWait w1 = new WebDriverWait(driver, 1);
			do
			{
				try
				{
					act.moveToElement(searchPageSortButton).click().build().perform();
					w1.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
					enabled = true;
				}
				catch(Exception e)
				{
					enabled = false;
					enabled = searchPageSortOptionFacet.getAttribute("style").contains("block");
				}
			}while(enabled==false);
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
		}
		catch (Exception e) 
		{
			System.out.println("Issue in Enabling the Sort Button");
			Exception("Issue in Enabling the Sort Button.");
		}
	}
	
	/* Get Product Id */
	public ArrayList<String> getprdtId() throws Exception
	{
		ArrayList<String> modifiedList = new ArrayList<>();
		String selPrdtid = "";
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				//System.out.println(searchPageProductLists.size());
				for(int j = 1; j<=searchPageProductLists.size();j++)
				{
					Thread.sleep(15);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(j-1)));
					BNBasicfeature.scrolldown(searchPageProductLists.get(j-1), driver);
					Thread.sleep(10);
					WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+j+"]"));
					selPrdtid = prdtlist.getAttribute("identifier").toString();
					modifiedList.add(selPrdtid);
					log.add("The Selected Product ID is  : " + selPrdtid);
				}
				BNBasicfeature.scrollup(header, driver);
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedList;
	}
	
	/* Get Prdt All Details */
	public List<List<String>> pdtNameDetails()
	{
		ArrayList<String> modifiedPrdtTitle = new ArrayList<>();
		ArrayList<String> modifiedAuthorTitle = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					if(BNBasicfeature.isElementPresent(searchResultListProductName.get(i-1)))
					{
						String prdtname = searchResultListProductName.get(i-1).getText();
						//System.out.println(prdtname);
						if(prdtname.isEmpty())
						{
							Fail("The Product Name is not displayed.");
							modifiedPrdtTitle.add("NULL");
						}
						else
						{
							log.add("The displayed product name is  : " + prdtname);
							Pass("The Product Name is displayed.",log);
							modifiedPrdtTitle.add(prdtname);
						}
					}
					else
					{
						Fail("The Product Name is not displayed.");
					}
					
					if(BNBasicfeature.isListElementPresent(searchPageProductLists))
					{
						try
						{
							WebElement authorName = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'authorName')]"));
							String authName = authorName.getText();
							//System.out.println(authName);
							if(authName.isEmpty())
							{
								Fail("The Author Name is not displayed.");
								modifiedAuthorTitle.add("NULL");
							}
							else
							{
								log.add("The displayed Author name is  : " + authName);
								Pass("The Author Name is displayed.",log);
								modifiedAuthorTitle.add(authName);
							}
						}
						catch(Exception e)
						{
							Pass("No Author Name is displayed for the product.");
						}
					}
					else
					{
						Fail("The Search result Page results are not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		
		List<List<String>> result = new ArrayList<List<String>>();
		result.add(modifiedPrdtTitle);
		result.add(modifiedAuthorTitle);
		return result; 
	}
	
	/* Get Prdt All Details */
	public List<List<Float>> pdtPriceDetails()
	{
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		ArrayList<Float> regPdtPrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement priceContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							size = size-1;
						}
						if(size<=1)
						{
							size=2;
						}
						
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							if(priceLabel.isEmpty())
							{
								Fail("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									String salePrice =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listSalePrice')]")).get(j-1).getText();
									if(!salePrice.contains("FREE"))
									{
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										//System.out.println(sPrice);
										if(sPrice>=0)
										{
											Pass("The Sale Price is displayed for the Product " + searchResultListProductName.get(i-1).getText()+ " .The displayed Sale price is : " + sPrice);
											modifiedPrice.add(sPrice);
										}
										else
										{
											Fail("The Sale Price is not displayed for the Product " +searchResultListProductName.get(i-1).getText() + ". The displayed Sale price is : " + sPrice);
											modifiedPrice.add(0f);
										}
									}
									else
									{
										modifiedPrice.add(0f);
									}
									
									try
									{
										WebElement regPrice =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listReg')]")).get(j-1);
										//regPrice.click();
										String saleregPrice = regPrice.getText();
										Float sPrice = Float.parseFloat(saleregPrice.replace("$", ""));
										if(sPrice>0)
										{
											Pass("The Regular Price is displayed for the Product : " + searchResultListProductName.get(i-1).getText()+ ". The displayed Regular price is : " + sPrice);
											regPdtPrice.add(sPrice);
										}
										else
										{
											Fail("The Regular Price is not displayed. The displayed Regular price is : " + sPrice);
											regPdtPrice.add(0f);
										}
									}
									catch(Exception e)
									{
										Skip("The Reg Price is not displayed for the product " + searchResultListProductName.get(i-1).getText());
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		
		List<List<Float>> result = new ArrayList<List<Float>>();
		result.add(modifiedPrice);
		result.add(regPdtPrice);
		return result; 
	}
	
	/* Get Product Price */
	public ArrayList<Float> getPrdSalePrice()
	{
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				//System.out.println(searchPageProductLists.size());
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement priceContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							size = size-1;
						}
						if(size<=1)
						{
							size=2;
						}
						
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							if(priceLabel.isEmpty())
							{
								Fail("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									String salePrice =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listSalePrice')]")).get(j-1).getText();
									if(!salePrice.contains("FREE"))
									{
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										//System.out.println(sPrice);
										if(sPrice>=0)
										{
											Pass("The Sale Price is displayed for the Product " + searchResultListProductName.get(i-1).getText()+ " .The displayed Sale price is : " + sPrice);
											modifiedPrice.add(sPrice);
										}
										else
										{
											Fail("The Sale Price is not displayed for the Product " +searchResultListProductName.get(i-1).getText() + ". The displayed Sale price is : " + sPrice);
											modifiedPrice.add(0f);
										}
									}
									else
									{
										modifiedPrice.add(0f);
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedPrice;
	}
	
	/* Get Product Reg Price */
	public ArrayList<Float> getPrdRegPrice()
	{
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement priceContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@class='productlistTab']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							size = size-1;
						}
						if(size<=1)
						{
							size=2;
						}
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@class='productlistTab']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							if(priceLabel.isEmpty())
							{
								Fail("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									try
									{
										WebElement regPrice =  driver.findElements(By.xpath("(//*[@class='productlistTab']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listReg')]")).get(j-1);
										//regPrice.click();
										String salePrice = regPrice.getText();
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										if(sPrice>0)
										{
											Pass("The Regular Price is displayed for the Product : " + searchResultListProductName.get(i-1).getText()+ ". The displayed Regular price is : " + sPrice);
											modifiedPrice.add(sPrice);
										}
										else
										{
											Fail("The Regular Price is not displayed. The displayed Regular price is : " + sPrice);
											modifiedPrice.add(0f);
										}
									}
									catch(Exception e)
									{
										Skip("The Reg Price is not displayed for the product " + searchResultListProductName.get(i-1).getText());
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedPrice;
	}
	
	/* Get Product Rating */
	public ArrayList<Float> getPrdRating()
	{
		ArrayList<Float> modifiedRating = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement reviewContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
					BNBasicfeature.scrollup(searchPageProductLists.get(i-1), driver);
					if(BNBasicfeature.isElementPresent(reviewContainer))
					{
						Pass("The review container is displayed.");
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							String reviewCount = reviewCnt.getAttribute("title");
							Float revCnt = Float.parseFloat(reviewCount);
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
								modifiedRating.add(revCnt);
							}
						}
						catch(Exception e)
						{
							Float rating = 0.0f;
							modifiedRating.add(rating);
						}
					}
					else
					{
						Fail("The review container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedRating;
	}
	
	/* Get Product Name */
	public ArrayList<String> getPrdTitle()
	{
		ArrayList<String> modifiedTitle = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					if(BNBasicfeature.isElementPresent(searchResultListProductName.get(i-1)))
					{
						wait.until(ExpectedConditions.visibilityOf(searchResultListProductName.get(i-1)));
						String prdtname = searchResultListProductName.get(i-1).getText();
						//System.out.println(prdtname);
						if(prdtname.isEmpty())
						{
							Fail("The Product Name is not displayed.");
							modifiedTitle.add("NULL");
						}
						else
						{
							log.add("The displayed product name is  : " + prdtname);
							Pass("The Product Name is displayed.",log);
							modifiedTitle.add(prdtname);
						}
					}
					else
					{
						Fail("The Product Name is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedTitle;
	}
	
	/* Get Product Price */
	public ArrayList<Float> getPLPPrdSalePrice()
	{
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				//System.out.println(productLists.size());
				for(int i = 1; i<=plpPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(plpPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement priceContainer = plpPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							size = size-1;
						}
						if(size<=1)
						{
							size=2;
						}
						
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							if(priceLabel.isEmpty())
							{
								Fail("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									String salePrice =  driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listSalePrice')]")).get(j-1).getText();
									if(!salePrice.contains("FREE"))
									{
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										//System.out.println(sPrice);
										if(sPrice>=0)
										{
											Pass("The Sale Price is displayed for the Product " + plpPageProductName.get(i-1).getText()+ " .The displayed Sale price is : " + sPrice);
											modifiedPrice.add(sPrice);
										}
										else
										{
											Fail("The Sale Price is not displayed for the Product " +plpPageProductName.get(i-1).getText() + ". The displayed Sale price is : " + sPrice);
											modifiedPrice.add(0f);
										}
									}
									else
									{
										modifiedPrice.add(0f);
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedPrice;
	}
	
	/* Get PLP Page Product Reg Price */
	public ArrayList<Float> getPLPPrdRegPrice()
	{
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				for(int i = 1; i<=plpPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(plpPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement priceContainer = plpPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							size = size-1;
						}
						if(size<=1)
						{
							size=2;
						}
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							if(priceLabel.isEmpty())
							{
								Fail("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									try
									{
										WebElement regPrice =  driver.findElements(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listReg')]")).get(j-1);
										//regPrice.click();
										String salePrice = regPrice.getText();
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										if(sPrice>0)
										{
											Pass("The Regular Price is displayed for the Product : " + plpPageProductName.get(i-1).getText()+ ". The displayed Regular price is : " + sPrice);
											modifiedPrice.add(sPrice);
										}
										else
										{
											Fail("The Regular Price is not displayed. The displayed Regular price is : " + sPrice);
											modifiedPrice.add(0f);
										}
									}
									catch(Exception e)
									{
										Skip("The Reg Price is not displayed for the product " + plpPageProductName.get(i-1).getText());
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedPrice;
	}
	
	/* Sort Selection */
	public boolean sortSelection(String val)
	{
		boolean srtSet = false;
		WebElement srtVal = null;
		Actions act = new Actions(driver);
		try
		{
			if(BNBasicfeature.isElementPresent(searchResults))
			{
				if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
				{
					jsclick(searchResults);
					BNBasicfeature.scrollup(header, driver);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
				}
			}
			
			if(BNBasicfeature.isElementPresent(searchPageSortButton))
			{
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(100);
				Pass("The Sort button is displayed.");
				boolean enabled = false;
				WebDriverWait w1 = new WebDriverWait(driver, 1);
				do
				{
					try
					{
						act.moveToElement(searchPageSortButton).click().build().perform();
						w1.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
						enabled = true;
					}
					catch(Exception e)
					{
						enabled = false;
						enabled = searchPageSortOptionFacet.getAttribute("style").contains("block");
					}
				}while(enabled==false);
				/*searchPageSortButton.click();*/
				wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
				//Thread.sleep(500);
				if(BNBasicfeature.isListElementPresent(searchPageSortFacetsLists))
				{
					for(int j=1; j<=searchPageSortFacetsLists.size();j++)
					{
						srtVal = searchPageSortFacetsLists.get(j-1);
						String srchName = searchPageSortFacetsLists.get(j-1).getText();
						if(srchName.contains(val))
						{
							srtSet = true;
							log.add("The Searching Value is : " + srchName);
							break;
						}
						else
						{
							srtSet = false;
							continue;
						}
					}
				}
				else
				{
					Fail("The Sorting Facets are not displayed.");
				}
			}
			else
			{
				Fail("The Sort Button is not displayed.");
			}
			
			if(srtSet==true)
			{
				Pass("The Searched Sort Value is displayed.");
				jsclick(srtVal);
				//Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
			}
			else
			{
				Fail("The Searched Sort Value is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return srtSet;
	}
	
	/* Get Product Name */
	public ArrayList<String> getPLPPrdTitle()
	{
		ArrayList<String> modifiedTitle = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				for(int i = 1; i<=plpPageProductLists.size();i++)
				{
					BNBasicfeature.scrolldown(plpPageProductLists.get(i-1), driver);
					if(BNBasicfeature.isElementPresent(plpPageProductName.get(i-1)))
					{
						String prdtname = plpPageProductName.get(i-1).getText();
						//System.out.println(prdtname);
						if(prdtname.isEmpty())
						{
							Fail("The Product Name is not displayed.");
							modifiedTitle.add("NULL");
						}
						else
						{
							log.add("The displayed product name is  : " + prdtname);
							Pass("The Product Name is displayed.",log);
							modifiedTitle.add(prdtname);
						}
					}
					else
					{
						Fail("The Product Name is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result Page results are not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return modifiedTitle;
	}
	
	/* Apply Filter */
	public boolean applyFilter()
	{
		boolean facetloaded = false;
		facetList = new ArrayList<>();
		originalUrl = "";
		try
		{
			if(BNBasicfeature.isElementPresent(searchResults))
			{
				if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
				{
					jsclick(searchResults);
					BNBasicfeature.scrollup(header, driver);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
				}
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			
			do
			{
				originalUrl = driver.getCurrentUrl();
				wait.until(ExpectedConditions.visibilityOf(header));
				String prevUrl = driver.getCurrentUrl();
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					resultContainerDetails();
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
							{
								for(int j = 0; j<searchPageFilterFacetsNameExpanded.size(); j++)
								{
									jsclick(searchPageFilterFacetsNameExpanded.get(j));
									j--;
								}
							}
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facets List is displayed.");
								for(int i = 0; i<searchPageFilterFacetsLists.size(); i++)
								{
									String val = searchPageFilterFacetsLists.get(i).getAttribute("fname");
									facetList.add(val);
								}
								Random fr = new Random();
								int fsel = fr.nextInt(searchPageFilterFacetsLists.size());
								jsclick(searchPageFilterFacetsLists.get(fsel));
								if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
								{
									wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(fsel), "style", "block"));
									boolean facetVal = searchPageFilterFacetsValue.get(fsel).getAttribute("style").contains("block");
									if(facetVal==true)
									{
										List<WebElement> filterFaceVal = getfilterValues(fsel);
										Random r = new Random();
										int sel = r.nextInt(filterFaceVal.size());
										if(sel<1)
										{
											sel = 1;
										}
										selFacVal = filterFaceVal.get(sel).getText();
										jsclick(filterFaceVal.get(sel));
										wait.until(ExpectedConditions.and(
												ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@id,'popupMask')]")),
												ExpectedConditions.visibilityOf(header)
													)
												);
										
										wait.until(ExpectedConditions.visibilityOf(header));
										if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
										{
											facetloaded=false;
											driver.get(prevUrl);
											continue;
										}
										else
										{
											log.add("The Selected Facet is : " + selFacVal);
											facetloaded=true;
											continue;
										}
									}
									else
									{
										Fail("The Facet Val was not opened.");
									}
								}
								else
								{
									Fail("The Facets Value is not displayed.");
								}
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}while(facetloaded!=true);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return facetloaded;
	}
	
	/* Check If Facet Applied */
	public boolean facetApplied() throws Exception
	{
		facetFound = false;
		navigatedUrl = "";
		try
		{
			if(BNBasicfeature.isElementPresent(searchResults))
			{
				if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
				{
					jsclick(searchResults);
					BNBasicfeature.scrollup(header, driver);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
				}
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			int prevCount = searchPrdtTotalCount;
			resultContainerDetails();
			int newCount = searchPrdtTotalCount;
			navigatedUrl = driver.getCurrentUrl();
			if(newCount<prevCount)
			{
				facetFound = true;
			}
			else
			{
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						Pass("The Facets is opened.");
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							log.add("The filter facets are displayed.");
							ArrayList<String> currFac = new ArrayList<>();
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								log.add("The Facets List is displayed.");
								for(int i = 0; i<searchPageFilterFacetsLists.size(); i++)
								{
									currFac.add(searchPageFilterFacetsLists.get(i).getAttribute("fname"));
								}
							}
							else
							{
								Fail( "The Filter Facet List is not displayed.");
							}
							
							if(currFac.equals(facetList))
							{
								facetFound = false;
							}
							else
							{
								facetFound = true;
							}
							jsclick(searchPageFilterButton);
							wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "none"));
						}
						else
						{
							Fail("The Search Page Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return facetFound;
	}
	
	/* Remove Facets from the Filter*/
	public boolean removeFilters()
	{
		ArrayList<String> currFac = null;
		facetRmv = false;
		try
		{
			if(facetFound==true)
			{
				int currCount = searchPrdtTotalCount;
				resultContainerDetails();
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						Pass("The Facets is opened.");
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							log.add("The filter facets are displayed.");
							currFac = new ArrayList<>();
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								log.add("The Facets List is displayed.");
								for(int i = 0; i<searchPageFilterFacetsLists.size(); i++)
								{
									currFac.add(searchPageFilterFacetsLists.get(i).getAttribute("fname"));
								}
								if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
								{
									jsclick(searchPageFilterButton);
								}
							}
							else
							{
								Fail( "The Filter Facet List is not displayed.");
							}
						}
						else
						{
							Fail("The Search Page Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
				
				driver.navigate().back();
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
				resultContainerDetails();
				int newCount = searchPrdtTotalCount;
				if(newCount>currCount)
				{
					facetRmv = true;
				}
				else
				{
					if(BNBasicfeature.isElementPresent(searchPageFilterButton))
					{
						Pass("The Filter button is displayed.");
						boolean enabled = false;
						WebDriverWait w1 = new WebDriverWait(driver, 1);
						do
						{
							try
							{
								jsclick(searchPageFilterButton);
								w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
								enabled = true;
							}
							catch(Exception e)
							{
								enabled = false;
								enabled = searchPageFilterFacets.getAttribute("style").contains("block");
							}
						}while(enabled==false);
						//jsclick(searchPageFilterButton);
						wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
						boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
						if(facetOpen==true)
						{
							Pass("The Facets is opened.");
							if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
							{
								log.add("The filter facets are displayed.");
								facetList = new ArrayList<>();
								if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
								{
									log.add("The Facets List is displayed.");
									for(int i = 0; i<searchPageFilterFacetsLists.size(); i++)
									{
										currFac.add(searchPageFilterFacetsLists.get(i).getAttribute("fname"));
									}
								}
								else
								{
									Fail( "The Filter Facet List is not displayed.");
								}
								
								if(currFac.equals(facetList))
								{
									facetRmv = false;
								}
								else
								{
									facetRmv = true;
								}
							}
							else
							{
								Fail("The Search Page Facets is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not opened.");
						}
					}
					else
					{
						Fail("The Filter button is not displayed.");
					}
				}
				
				if(BNBasicfeature.isElementPresent(searchResults))
				{
					if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
					{
						jsclick(searchResults);
						BNBasicfeature.scrollup(header, driver);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
					}
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
				{
					jsclick(searchPageFilterButton);
				}
			}
			else
			{
				Fail( "The Facet was not applied to remove.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
		return facetRmv;
	}
	
	/*********************************************** Search Result Page Implementation **********************************************/
	/* BRM - 1847 Verify that When there are no products found for the given search term then the alert message "We found 0 results for asdfasdfadsf. Please try another search or browse our recommendations below." must be displayed matching the creative*/
	public void searchPageNoResult()
	{
		ChildCreation(" BRM - 1847 Verify that When there are no products found for the given search term then the alert message We found 0 results for asdfasdfadsf. Please try another search or browse our recommendations below. must be displayed matching the creative.");
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM1847", sheet, 3);
			String[] bookVal = book.split("\n");
			String cellVal = BNBasicfeature.getExcelVal("BRM1847", sheet, 6);
			String[] err = cellVal.split("\n");
			boolean bkOk = nosearchPrdt(book); 
			if(bkOk==true)
			{
				if(BNBasicfeature.isElementPresent(plpNoProductsFound))
				{
					String actAlert = plpNoProductsFound.getText();
					log.add("The expected alert was : " + cellVal);
					log.add("The actual alert is : " + actAlert);
					if(actAlert.contains(err[0].concat(bookVal[0]))||actAlert.contains(err[0].concat(bookVal[1]))&&(actAlert.contains(err[1])))
					{
						Pass("There is no mismatch in the displayed alert.",log);
					}
					else
					{
						Fail("There is mismatch in the displayed alert.",log);
					}
				}
				else
				{
					Fail("Products were displayed for the searched products.");
				}
			}
			else
			{
				Fail("Failed to load the Search Result page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1847 Issue ." + e.getMessage());
			Exception(" BRM - 1847 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 921 On selecting show more filter option in dropdown, then the additional filters must be displayed*/
	/* BRM - 1898 Verify that close X mark must be shown near the selected filters.*/
	public void searchPageFilterShowMore()
	{
		ChildCreation("BRM - 921 On selecting show more filter option in dropdown, then the additional filters must be displayed");
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM921", sheet, 3);
			String expVal = BNBasicfeature.getExcelVal("BRM921", sheet, 6);
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(100);
			}
			
			boolean bkOk = searchPrdt(book); 
			if(bkOk==true)
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				wait.until(ExpectedConditions.elementToBeClickable(searchPageFilterButton));
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					wait.until(ExpectedConditions.elementToBeClickable(searchPageFilterButton));
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					/*Thread.sleep(250);
					jsclick(searchPageFilterButton);*/
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
					{
						log.add( "The Filter Facet is enabled.");
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facets List is displayed.");
								for(int i = 0;i<searchPageFilterFacetsLists.size();i++)
								{
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
									{
										for(int j = 0; j<searchPageFilterFacetsNameExpanded.size(); j++)
										{
											jsclick(searchPageFilterFacetsNameExpanded.get(j));
											j--;
										}
									}
									jsclick(searchPageFilterFacetsLists.get(i));
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
									{
										wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "block"));
										boolean facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("block");
										if(facetVal==true)
										{
											//List<WebElement> filterFaceVal = driver.findElements(By.xpath("(//*[contains(@class,'facetOptions')]//*[contains(@class,'plpFacetValueCont')])["+(i+1)+"]//*[@class]"));
											List<WebElement> filterFaceVal = getfilterValues(i);
											int size = filterFaceVal.size();
											BNBasicfeature.scrolldown(filterFaceVal.get(size-1), driver);
											String currVal = filterFaceVal.get(size-1).getText();
											if(currVal.equals(expVal))
											{
												Pass("The expected value match the actual content.");
												BNBasicfeature.scrolldown(searchPageFilterButton, driver);
												jsclick(searchPageFilterFacetsNameExpanded.get(0));
												break;
											}
											else
											{
												Skip("The expected value does not match the actual content.");
												BNBasicfeature.scrolldown(searchPageFilterButton, driver);
												jsclick(searchPageFilterFacetsNameExpanded.get(0));
											}
										}
										else
										{
											Fail("The Facet Val was not opened.");
										}
									}
									else
									{
										Fail("The Facets Value is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}
			else
			{
				Fail("Failed to load the Search Result page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 921 Issue ." + e.getMessage());
			Exception(" BRM - 921 Issue." + e.getMessage());
		}
		
		/* BRM - 1898 Verify that close X mark must be shown near the selected filters.*/
		ChildCreation(" BRM - 1898 Verify that close X mark must be shown near the selected filters.");
		try
		{
			if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
			{
				Pass("The Filter Section Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					if(!searchPageFilterButton.getAttribute("class").contains("opened"))
					{
						//jsclick(searchPageFilterButton);
						boolean enabled = false;
						WebDriverWait w1 = new WebDriverWait(driver, 1);
						do
						{
							try
							{
								jsclick(searchPageFilterButton);
								w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
								enabled = true;
							}
							catch(Exception e)
							{
								enabled = false;
								enabled = searchPageFilterFacets.getAttribute("style").contains("block");
							}
						}while(enabled==false);
						//Thread.sleep(1000);
					}
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					//Thread.sleep(500);
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						Pass("The Facets is opened.");
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							log.add("The filter facets are displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterSelectedFacetsValue))
							{
								String val = "return window.getComputedStyle(document.querySelector('.skMob_plpSelFacet'),':before').getPropertyValue('content')";
								JavascriptExecutor js = (JavascriptExecutor)driver;
								String content = (String) js.executeScript(val);
								log.add("The Facet was Selected and the displayed value near the facet is : " +content);
								if(content.contains("X"))
								{
									Pass("The X mark is displayed.",log);
								}
								else
								{
									Fail("The Facet was selected but there is mismatch in the symbol selected near the facet.",log);
								}
							}
						}
						else
						{
							Fail("The Filter Applied Facets is not displayed.");
						}
					}
					else
					{
						Fail("The filter facets are not opened.");
					}
				}
				else
				{
					Fail("The Search Button is not displayed.");
				}
			}
			else
			{
				Fail("The Filter Selection Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			Exception("There is somthing wrong.Please Check." + e.getMessage());
		}
	}
	
	/* BRM - 915 Narrow filters should be displayed for more than one criteria.*/
	public void searchpageNarrowFilterSelection()
	{
		ChildCreation("BRM - 915 Narrow filters should be displayed for more than one criteria.");
		try
		{
			int oCount = 0,fAppliedCount = 0,fReaplCount = 0;
			resultContainerDetails();
			oCount = searchPrdtTotalCount;
			boolean applyFilt = applyFilter();
			if(applyFilt==true)
			{
				boolean facetApplied = facetApplied();
				if(facetApplied==true)
				{
					resultContainerDetails();
					fAppliedCount = searchPrdtTotalCount;
					log.add("The Original Count before applying the filter was : " + oCount);
					log.add("The Count before applying the filter is : " + fAppliedCount);
					if(oCount>=fAppliedCount)
					{
						Pass("The Filter Count after applying the filter is less than the original count.",log);
					}
					else
					{
						Fail("The Filter Count after applying the filter is not less than the original count.",log);
					}
					
					applyFilt = applyFilter();
					if(applyFilt==true)
					{
						resultContainerDetails();
						fReaplCount = searchPrdtTotalCount;
						log.add("The Count before applying the narrow filter was : " + fAppliedCount);
						log.add("The Count before applying the narrow filter is : " + fReaplCount);
						if(fAppliedCount>=fReaplCount)
						{
							Pass("The Filter Count after applying the narrow filter is less than the original count.",log);
						}
						else
						{
							Fail("The Filter Count after applying the narrow filter is not less than the original count.",log);
						}
					}
					else
					{
						Fail("The User was not able to apply the filter.");
					}
				}
				else
				{
					Fail("The Facet is not applied.",log);
				}
			}
			else
			{
				Fail("The Facets is not selected.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 915 Issue ." + e.getMessage());
			Exception(" BRM - 915 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1835 Verify that on selecting search icon in header, search textbox should be opened.*/
	public void searchFieldContainer()
	{
		ChildCreation("BRM - 1835 Verify that on selecting search icon in header, search textbox should be opened.");
		try
		{
			String expText = BNBasicfeature.getExcelVal("BRM1835", sheet, 6);
			if(BNBasicfeature.isElementPresent(searchResults))
			{
				if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
				{
					jsclick(searchResults);
					BNBasicfeature.scrollup(header, driver);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
				}
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				log.add("The Search Icon is displayed.");
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				Thread.sleep(100);
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					act.moveToElement(searchContainer).click().build().perform();
					log.add("The Search icon is Clicked and the Search Field is displayed.");
					if(BNBasicfeature.isElementPresent(searchContainerCancel))
					{
						Pass("The Cancel Button is displayed.");
						log.add( "The Expected Value was : " + expText);
						String actVal = searchContainerCancel.getText();
						log.add( "The Actual Value is : " + actVal);
						if(actVal.equalsIgnoreCase(expText))
						{
							Pass("The Cancel Button text matches.",log);
						}
						else
						{
							Fail("The Cancel Button text does not matches.",log);
						}
					}
					else
					{
						Fail("The Cancel Button is not displayed.",log);
					}
				}
				else
				{
					Fail("The Search icon is Clicked and the Search Container is not displayed.",log);
				}
			}
			else
			{
				Fail("The Search Icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1835 Issue ." + e.getMessage());
			Exception(" BRM - 1835 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1837 Verify that search suggestions displayed on entering the keyword in search textbox.*/
	public void searchSuggestionResults()
	{
		ChildCreation(" BRM - 1837 Verify that search suggestions displayed on entering the keyword in search textbox.");
		try
		{
			String srchText = BNBasicfeature.getExcelVal("BRM1837", sheet, 6);
			log.add("The Entered Search Keyword was : " + srchText);
			if(!BNBasicfeature.isElementPresent(searchContainer))
			{
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
			}
			if(BNBasicfeature.isElementPresent(searchContainer))
			{
				log.add("The Search icon is Clicked and the Search Field is displayed.");
				if(BNBasicfeature.isElementPresent(searchContainerCancel))
				{
					Actions act = new Actions(driver);
					act.moveToElement(searchContainer).click().sendKeys(srchText).build().perform();
					Thread.sleep(1000);
					try
					{
						Thread.sleep(1000);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(searchResultsContainer),
								//ExpectedConditions.stalenessOf(searchResultsContainer),
								ExpectedConditions.visibilityOfAllElements(searchResultsLists)
									)
								);
					}
					catch(Exception e)
					{
						Thread.sleep(1000);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(searchResultsContainer),
								//ExpectedConditions.stalenessOf(searchResultsContainer),
								ExpectedConditions.visibilityOfAllElements(searchResultsLists)
									)
								);
					}
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
					wait.until(ExpectedConditions.visibilityOf(searchResultsContainer));
					if(BNBasicfeature.isListElementPresent(searchResultsLists))
					{
						for(WebElement srchedText : searchResultsLists)
						{
							String text = srchedText.getText();
							if(text.isEmpty())
							{
								Fail("The Searched Text results is not displayed.");
							}
							else
							{
								Pass("The Searched Text results is displayed.");
							}
						}
					}
					else if(BNBasicfeature.isElementPresent(noSearchResults))
					{
						Skip("No Value was returned for the searched products. The displayed value was : " + noSearchResults.getText());
					}
					else
					{
						Fail("The Search Results are not displayed and No Search Suggestion as well not displayed.");
					}
				}
				else
				{
					Fail("The Search icon is Clicked and the Search Container is not displayed.",log);
				}
			}
			else
			{
				Fail("The Search Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1837 Issue ." + e.getMessage());
			Exception(" BRM - 1837 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1838 Verify that If user selects the search suggestion list then the search operation should be performed.*/
	/* BRM - 884 Number of items found in the page should be displayed correctly. */
	/* BRM - 849 Verify that the grid view is enabled and grid view icon is highlighted on the search result page.*/
	/* BRM - 901 Verify that the filter title is aligned as per the creative.*/
	/* BRM - 865 Verify that the searched key word should be displayed as per the creative. */
	/* BRM - 1860 Verify that the searched keyword, total result pages and total search results count should be highlighted as per the creative.*/
	/* BRM - 866 Verify that the Searched term should be displayed above the results with total count.*/
	public void searchSuggestionSelection()
	{
		ChildCreation(" BRM - 1838 Verify that If user selects the search suggestion list then the search operation should be performed.");
		boolean pgOk = false;
		String originaltext = "";
		try
		{
			String srchText = BNBasicfeature.getExcelVal("BRM1837", sheet, 6);
			log.add("The Entered Search Keyword was : " + srchText);
			if(!BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				if(BNBasicfeature.isElementPresent(searchResults))
				{
					if(!BNBasicfeature.isElementPresent(searchPagePopUpMask))
					{
						jsclick(searchResults);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
					}
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
				{
					jsclick(searchPageFilterSortMask);
					//BNBasicfeature.scrollup(header, driver);
					Thread.sleep(100);
				}
				
				wait.until(ExpectedConditions.visibilityOf(header));
				//Thread.sleep(250);
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					jsclick(searchContainerCancel);
				}
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					log.add("The Search Icon is displayed.");
					jsclick(searchIcon);
					wait.until(ExpectedConditions.visibilityOf(searchContainer));
					Actions act = new Actions(driver);
					if(BNBasicfeature.isElementPresent(searchContainer))
					{
						act.moveToElement(searchContainer).click().build().perform();
						log.add("The Search icon is Clicked and the Search Field is displayed.");
						if(BNBasicfeature.isElementPresent(searchContainerCancel))
						{
							act.moveToElement(searchContainer).click().sendKeys(srchText).build().perform();
							
							wait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(searchResultsContainer),
									ExpectedConditions.stalenessOf(searchResultsContainer),
									ExpectedConditions.visibilityOfAllElements(searchResultsLists)
										)
									);
							wait.until(ExpectedConditions.visibilityOfAllElements(searchResultsLists));
							
							if(BNBasicfeature.isListElementPresent(searchResultsLists))
							{
								for(WebElement srchedText : searchResultsLists)
								{
									String text = srchedText.getText();
									if(text.isEmpty())
									{
										Fail("The Searched Text results is not displayed.");
									}
									else
									{
										Pass("The Searched Text results is displayed.");
									}
								}
							}
							else if(BNBasicfeature.isElementPresent(noSearchResults))
							{
								Skip("No Value was returned for the searched products. The displayed value was : " + noSearchResults.getText());
							}
							else
							{
								Fail("The Search Results are not displayed and No Search Suggestion as well not displayed.");
							}
						}
						else
						{
							Fail("The Search icon is Clicked and the Search Container is not displayed.",log);
						}
					}
					else
					{
						Fail("The Search Container is not displayed.");
					}
				}
				else
				{
					Fail( "The Search icon is not displayed.");
				}
			}
			if(BNBasicfeature.isListElementPresent(searchResultsLists))
			{
				Random r = new Random();
				int sel = r.nextInt(searchResultsLists.size());
				if(sel>=0)
				{
					sel = 1;
				}
				Actions act = new Actions(driver);
				String selText = searchResultsLists.get(sel-1).getText();
				act.moveToElement(searchResultsLists.get(sel-1)).click().build().perform();
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(1000);
				wait.until(ExpectedConditions.visibilityOf(homepageHeader));
				if(BNBasicfeature.isElementPresent(plpNoProductsFound))
				{
					pgOk=false;
				}
				else if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
				{
					wait.until(ExpectedConditions.visibilityOf(pdpPageProductNameContainer));
					originaltext = pdpPageProductNameContainer.getText();
					pgOk = true;
				}
				else if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
				{
					originaltext = searchPageProductCountContainer.getText();
					pgOk = true;
				}
				else
				{
					pgOk = false;	
				}
				
				if(pgOk==true)
				{
					log.add("The expected page was : " + selText);
					log.add("The actual page is : " + originaltext);
					if(originaltext.contains(selText))
					{
						Pass("The User is redirected to the expected page.",log);
						srcPage = true;
					}
					else
					{
						Fail("The User is not redirected to the expected page.",log);
						srcPage = false;
					}
				}
				else
				{
					Fail("The Search Results page is not loaded.");
				}
			}
			else if(BNBasicfeature.isElementPresent(noSearchResults))
			{
				Skip("No Value was returned for the searched products. The displayed value was : " + noSearchResults.getText());
			}
			else
			{
				Fail("The Search Results are not displayed and No Search Suggestion as well not displayed.");
			}
			
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1838 Issue ." + e.getMessage());
			Exception(" BRM - 1838 Issue." + e.getMessage());
		}
		
		/* BRM - 884 Number of items found in the page should be displayed correctly. */
		ChildCreation(" BRM - 884 Number of items found in the page should be displayed correctly.");
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductContainerResultsList))
			{
				Pass("The Product Count Container List is displayed.");
				resultContainerDetails();
				String currVal = searchPrdtLimit;
				String[] val = currVal.split("-");
				int totalPrdtCount = Integer.parseInt(val[1]);
				log.add("The Total Product found for the Search term is : " + totalPrdtCount);
				if(BNBasicfeature.isListElementPresent(searchPageProductLists))
				{
					int prdtCount = searchPageProductLists.size();
					log.add("The Product displayed in the page is : " + prdtCount);
					if(prdtCount==totalPrdtCount)
					{
						Pass("The Product Count matches.");
					}
					else
					{
						Fail("The Product Count does not matches.");
					}
				}
				else
				{
					Fail("The Search Page is empty and no results is displayed.");
				}
			}
			else
			{
				Fail("The Product Count Container list is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 884 Issue ." + e.getMessage());
			Exception(" BRM - 884 Issue." + e.getMessage());
		}
		
		/* BRM - 849 Verify that the grid view is enabled and grid view icon is highlighted on the search result page. */
		ChildCreation(" BRM - 849 Verify that the grid view is enabled and grid view icon is highlighted on the search result page.");
		try
		{
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				Pass("The Grid view is Active.");
			}
			else
			{
				Fail("The Grid view is not Active.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 849 Issue ." + e.getMessage());
			Exception(" BRM - 849 Issue." + e.getMessage());
		}
		
		/* BRM - 901 Verify that the filter title is aligned as per the creative. */
		ChildCreation(" BRM - 901 Verify that the filter title is aligned as per the creative.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM901", sheet, 6);
			if(BNBasicfeature.isElementPresent(searchPageFilterTabContainer))
			{
				Pass("The Filter Tab Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter Button is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageFilterTabCaption))
					{
						Pass("The Filter Caption is displayed.");
						log.add("The Expected Caption was : " + cellVal);
						String actVal = searchPageFilterTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(cellVal.contains(actVal))
						{
							Pass("The Filter Caption matches the expected one.",log);
						}
						else
						{
							Fail("The Filter Caption does not mathces the expected one.",log);
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
					}
				}
				else
				{
					Fail("The Filter Button is not displayed.");
				}
			}
			else
			{
				Fail("The Filter Tab Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 901 Issue ." + e.getMessage());
			Exception(" BRM - 901 Issue." + e.getMessage());
		}
		
		/* BRM - 865 Verify that the searched key word should be displayed as per the creative. */
		ChildCreation(" BRM - 865 Verify that the searched key word should be displayed as per the creative.");
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM849", sheet, 3);
			String expColor = BNBasicfeature.getExcelVal("BRM865", sheet, 5);
			if(BNBasicfeature.isElementPresent(searchPageSearchTerm))
			{
				BNBasicfeature.scrollup(header, driver);
				Pass("The Search Term is displayed.");
				String srchTermText = searchPageSearchTerm.getText();
				if(book.contains(srchTermText))
				{
					Pass("The Search Term text matches the expected one.");
				}
				else
				{
					Fail("The Search Term does not match the expected one.");
				}
				
				log.add("The Expected color is : " + expColor);
				String fontColor = searchPageSearchTerm.getCssValue("color");
				Color colCnvt = Color.fromString(fontColor);
				String actColor = colCnvt.asHex();
				log.add("The Actual Color is : " + actColor);
				if(expColor.contains(actColor))
				{
					Pass("The Search Term font color does matches.",log);
				}
				else
				{
					Fail("The Search Term font color does not matches.",log);
				}
			}
			else
			{
				Fail("The Search Term is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 865 Issue ." + e.getMessage());
			Exception(" BRM - 865 Issue." + e.getMessage());
		}
		
		/* BRM - 1860 Verify that the searched keyword, total result pages and total search results count should be highlighted as per the creative.*/
		ChildCreation(" BRM - 1860 Verify that the searched keyword, total result pages and total search results count should be highlighted as per the creative.");
		try
		{
			String expColor = BNBasicfeature.getExcelVal("BRM865", sheet, 5);
			if(BNBasicfeature.isElementPresent(searchPageSearchTerm))
			{
				log.add("The Expected color is : " + expColor);
				String fontColor = searchPageSearchTerm.getCssValue("color");
				Color colCnvt = Color.fromString(fontColor);
				String actColor = colCnvt.asHex();
				log.add("The Actual Color is : " + actColor);
				if(expColor.contains(actColor))
				{
					Pass("The Search Term font color does matches.",log);
				}
				else
				{
					Fail("The Search Term font color does not matches.",log);
				}
			}
			else
			{
				Fail("The Search Term is not displayed.");
			}
			
			if(BNBasicfeature.isListElementPresent(searchPageProductContainerResultsList))
			{
				Pass("The Search page Containser list is displayed.");
				for(int i = 0; i<searchPageProductContainerResultsList.size();i++)
				{
					log.add("The Expected color is : " + expColor);
					String val = searchPageProductContainerResultsList.get(i).getText();
					log.add("The Selected Search Term Value is : " + val);
					String fontColor = searchPageProductContainerResultsList.get(i).getCssValue("color");
					Color colCnvt = Color.fromString(fontColor);
					String actColor = colCnvt.asHex();
					log.add("The Actual Color is : " + actColor);
					if(expColor.contains(actColor))
					{
						Pass("The Search font color matches.",log);
					}
					else
					{
						Fail("The Search font color does not matches.",log);
					}
				}
			}
			else
			{
				Fail("The Search Page Container list is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1860 Issue ." + e.getMessage());
			Exception(" BRM - 1860 Issue." + e.getMessage());
		}
		
		/* BRM - 866 Verify that the Searched term should be displayed above the results with total count.*/
		ChildCreation(" BRM - 866 Verify that the Searched term should be displayed above the results with total count.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{
				resultContainerDetails();
				Pass("The Search Term Container is displayed.");
				if(searchPrdtTotalCount>=0)
				{
					Pass("The Total Count is displayed.",log);
				}
				else
				{
					Fail("The Total Count is not displayed.",log);
				}
			}
			else
			{
				Fail("The Search Term Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 866 Issue ." + e.getMessage());
			Exception(" BRM - 866 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 863 Once the user enters the term and selecting the search icon the search result page should be displayed as per the creative.*/
	/* BRM - 1841 - Verify that the search result page should be matched with the creative. */
	/* BRM - 1851 - Verify that the page must be similar to the product list page. */ 
	/* BRM - 1856 Verify that the Products List page should match the creative.*/
	/* BRM - 1915 Verify that the footer section is displaying in the PLP page/search result page.*/
	public void searchPriceSearchResultView() throws Exception
	{
		boolean brm1841 = false;  
		boolean brm1851 = false;
		boolean brm1856 = false;
		boolean brm1915 = false;
			
		ChildCreation("BRM - 863 Once the user enters the term and selecting the search icon the search result page should be displayed as per the creative.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM863", sheet, 6);
			String[] Val = cellVal.split("\n");
			String book = BNBasicfeature.getExcelVal("BRM1837", sheet, 6);
			Integer count = null;
			if(srcPage==true)
			{
				if(BNBasicfeature.isElementPresent(listViewList))
				{
					jsclick(gridViewIcon);
					wait.until(ExpectedConditions.visibilityOf(gridViewList));
					Thread.sleep(100);
				}
				
				if(BNBasicfeature.isElementPresent(searchPagePromoBanner))
				{
					Pass("The Promo Banner is displayed in the Search Result page.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Skip("The Promo Banner is not displayed in the Search Result page.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
					
				if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
				{
					Pass("The Search page filter section is displayed.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Fail("The Search Page filter section is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
				{
					Pass("The Product Count Container is displayed.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Fail("The Product Count Container is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
					
				if(BNBasicfeature.isElementPresent(searchPageSearchTerm))
				{
					Pass("The Search Term is displayed.");
					String srchTermText = searchPageSearchTerm.getText();
					if(srchTermText.contains(book))
					{
						Pass("The Search Term text matches the expected one.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Search Term does not match the expected one.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					Fail("The Search Term is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(listViewIcon))
				{
					Pass("The List View is displayed.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Fail("The List View icon is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(gridViewIcon))
				{
					Pass("The Grid View is displayed.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Fail("The Grid View icon is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(gridViewList))
				{
					Pass("The Page is loaded in Grid View List as expected.");
					brm1841 = true;
					brm1851 = true;
					brm1856 = true;
					brm1915 = true;
				}
				else
				{
					Fail("The Page is not loaded in the expected Grid View.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterTabContainer))
				{
					Pass("The Filter Tab Container is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageFilterTabCaption))
					{
						Pass("The Filter Caption is displayed.");
						log.add("The Expected Caption was : " + Val[0]);
						String actVal = searchPageFilterTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(Val[0].contains(actVal))
						{
							Pass("The Filter Caption matches the expected one.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Filter Caption does not mathces the expected one.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
					{
						Pass("The Sort Caption is displayed.");
						log.add("The Expected Caption was : " + Val[1]);
						String actVal = searchPageSortTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(Val[1].contains(actVal))
						{
							Pass("The Sort Caption matches the expected one.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Sort Caption does not mathces the expected one.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					Fail("The Filter Tab Container is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isListElementPresent(searchPageshareIconList))
				{
					if(BNBasicfeature.isElementPresent(shareFB))
					{
						BNBasicfeature.scrolldown(shareFB, driver);
						Pass("The Social Media Facebook icon is displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Social Media Facebook icon is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(shareTwitter))
					{
						Pass("The Social Media Twitter icon is displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Social Media Twitter icon is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(sharePinrest))
					{
						Pass("The Social Media Pinrest icon is displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Social Media Pinrest icon is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(shareInsta))
					{
						Pass("The Social Media Instagram icon is displayed.");	
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Social Media Instagram icon is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					Fail("The Social Media Share Icon list is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				resultContainerDetails();
				count = searchPrdtTotalCount;
				log.add("The Total Product Count is : " + count);
				if(count>20)
				{
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						String attr = searchPagePaginationContainer.getAttribute("style");
						if(attr.contains("block"))
						{
							Pass("The Pagination is enabled.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Pagination is not enabled.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						try
						{
							String attr = searchPagePaginationContainer.getAttribute("style");
							if(attr.contains("block"))
							{
								Fail("The Pagination is enabled when the product count is less than or equal to 20.",log);
								brm1841 = false;
								brm1851 = false;
								brm1856 = false;
								brm1915 = false;
							}
							else
							{
								Pass("The Pagination is not enabled.",log);
								brm1841 = true;
								brm1851 = true;
								brm1856 = true;
								brm1915 = true;
							}
						}
						catch(Exception e)
						{
							Pass("The Pagination is not enabled.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
					}
					else
					{
						Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				
				if(BNBasicfeature.isElementPresent(header))
				{
					BNBasicfeature.scrolldown(header, driver);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(noShowBacktoTop))
					{
						Pass("The Show To Top button is not displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Show To Top button is displayed when the Page is already scrolled up.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(count>6)
					{
						if(BNBasicfeature.isElementPresent(footerSectionContainer))
						{
							BNBasicfeature.scrolldown(footerSectionContainer, driver);
							Thread.sleep(250);
							if(BNBasicfeature.isElementPresent(showBacktoTop))
							{
								Pass("The Show To Top button is displayed.");
								brm1841 = true;
								brm1851 = true;
								brm1856 = true;
								brm1915 = true;
							}
							else
							{
								Fail("The Show To Top button is not displayed when the Page is scrolled down fully.");
								brm1841 = false;
								brm1851 = false;
								brm1856 = false;
								brm1915 = false;
							}
						}
						else
						{
							Skip("The Back to top cotainer counld not verified since the count is less than the expected one.");
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
					}
					else
					{
						Skip("The Back to top cotainer counld not verified since the count is less than the expected one.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
				}
				else
				{
					Fail("The Page Could not be scrolled up.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(footerSectionMailContainer))
				{
					Pass("The Mail Container is displayed.");
					if(BNBasicfeature.isElementPresent(footerSectionMailContainerText))
					{
						String actVal = footerSectionMailContainerText.getText();
						log.add("The Expected value is : " + Val[11]);
						log.add("The Actual value is : " + actVal);
						if(Val[11].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Mail Container default text is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionMailSubmitButton))
					{
						Pass("The Submit button is displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Submit button is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionMailEmail))
					{
						Pass("The Email field is displayed.");
						brm1841 = true;
						brm1851 = true;
						brm1856 = true;
						brm1915 = true;
					}
					else
					{
						Fail("The Email field is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					Fail("The Mail Container is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
				
				if(BNBasicfeature.isElementPresent(footerSectionContainer))
				{
					BNBasicfeature.scrolldown(footerSectionContainer, driver);
					if(BNBasicfeature.isElementPresent(footerSectionMyAccount))
					{
						Pass("The My Account Link is displayed.");
						String actVal = footerSectionMyAccount.getText();
						log.add("The Expected value is : " + Val[2]);
						log.add("The Actual value is : " + actVal);
						if(Val[2].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The My Account Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionSignIn))
					{
						Pass("The Sign In Link is displayed.");
						String actVal = footerSectionSignIn.getText();
						log.add("The Expected value is : " + Val[3]);
						log.add("The Actual value is : " + actVal);
						if(Val[3].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Sign In Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionCustomerService))
					{
						Pass("The Customer Service Link is displayed.");
						String actVal = footerSectionCustomerService.getText();
						log.add("The Expected value is : " + Val[4]);
						log.add("The Actual value is : " + actVal);
						if(Val[4].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Customer Service Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionStores))
					{
						Pass("The Stores Link is displayed.");
						String actVal = footerSectionStores.getText();
						log.add("The Expected value is : " + Val[5]);
						log.add("The Actual value is : " + actVal);
						if(Val[5].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Store Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionTermsandUse))
					{
						Pass("The Terms of Use Link is displayed.");
						String actVal = footerSectionTermsandUse.getText();
						log.add("The Expected value is : " + Val[6]);
						log.add("The Actual value is : " + actVal);
						if(Val[6].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Terms and Use Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionPrivacy))
					{
						Pass("The Privacy Policy Link is displayed.");
						String actVal = footerSectionPrivacy.getText();
						log.add("The Expected value is : " + Val[8]);
						log.add("The Actual value is : " + actVal);
						if(Val[8].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Privacy Policy Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionCopyright))
					{
						Pass("The Copyright Link is displayed.");
						String actVal = footerSectionCopyright.getText();
						log.add("The Expected value is : " + Val[7]);
						log.add("The Actual value is : " + actVal);
						if(Val[7].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Copyright Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionTermsandUse1))
					{
						Pass("The Terms and Use Link is displayed.");
						String actVal = footerSectionTermsandUse1.getText();
						log.add("The Expected value is : " + Val[9]);
						log.add("The Actual value is : " + actVal);
						if(actVal.contains(Val[9]))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Terms and Use Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionFullSite))
					{
						Pass("The Full Site Link is displayed.");
						String actVal = footerSectionFullSite.getText();
						log.add("The Expected value is : " + Val[10]);
						log.add("The Actual value is : " + actVal);
						if(Val[10].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
							brm1841 = true;
							brm1851 = true;
							brm1856 = true;
							brm1915 = true;
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
							brm1841 = false;
							brm1851 = false;
							brm1856 = false;
							brm1915 = false;
						}
					}
					else
					{
						Fail("The Full Site Link is not displayed.");
						brm1841 = false;
						brm1851 = false;
						brm1856 = false;
						brm1915 = false;
					}
				}
				else
				{
					Fail("The Footer Section Container is not displayed.");
					brm1841 = false;
					brm1851 = false;
					brm1856 = false;
					brm1915 = false;
				}
			}
			else
			{
				Fail( "The Search page failed to load.");
				brm1841 = false;
				brm1851 = false;
				brm1856 = false;
				brm1915 = false;
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 863 Issue ." + e.getMessage());
			Exception(" BRM - 863 Issue." + e.getMessage());
			brm1841 = false;
			brm1851 = false;
			brm1856 = false;
			brm1915 = false;
		}
		
		/* BRM - 1841 Verify that the search result page should be matched with the creative. */
		ChildCreation(" BRM - 1841 Verify that the search result page should be matched with the creative. ");
		try
		{
			log.add("  Refer BRM - 1841 for detailed log repot. ");
			if(brm1841==true)
			{
				Pass(" The Search Page reults matches the creative. ",log);
			}
			else
			{
				Fail( " There is some mismatch in the creative.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1841 Issue ." + e.getMessage());
			Exception(" BRM - 1841 Issue." + e.getMessage());
		}
		
		/* BRM - 1851 - Verify that the page must be similar to the product list page. */
		ChildCreation(" BRM - 1851 - Verify that the page must be similar to the product list page. ");
		try
		{
			log.add("  Refer BRM - 863 for detailed log repot. ");
			if(brm1851==true)
			{
				Pass(" The Search Page reults matches the creative. ",log);
			}
			else
			{
				Fail( " There is some mismatch in the creative.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1851 Issue ." + e.getMessage());
			Exception(" BRM - 1851 Issue." + e.getMessage());
		}
		
		/* BRM - 1856 Verify that the Products List page should match the creative. */
		ChildCreation(" BRM - 1856 Verify that the Products List page should match the creative. ");
		try
		{
			log.add("  Refer BRM - 863 for detailed log repot. ");
			if(brm1856==true)
			{
				Pass(" The Search Page reults matches the creative. ",log);
			}
			else
			{
				Fail( " There is some mismatch in the creative.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1856 Issue ." + e.getMessage());
			Exception(" BRM - 1856 Issue." + e.getMessage());
		}
		
		/* BRM - 1915 Verify that the footer section is displaying in the PLP page/search result page. */
		ChildCreation(" BRM - 1915 Verify that the footer section is displaying in the PLP page/search result page. ");
		try
		{
			log.add("  Refer BRM - 863 for detailed log repot. ");
			if(brm1915==true)
			{
				Pass(" The Search Page reults matches the creative. ",log);
			}
			else
			{
				Fail( " There is some mismatch in the creative.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1915 Issue ." + e.getMessage());
			Exception(" BRM - 1915 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1858 Verify that the "sort" and the "filter" options are displayed below the header.*/
	public void searchPageFilterSort()
	{
		ChildCreation("BRM - 1858 Verify that the sort and the filter options are displayed below the header.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM863", sheet, 6);
			String[] Val = cellVal.split("\n");
			if(BNBasicfeature.isElementPresent(searchPageFilterTabContainer))
			{
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
				Pass("The Filter and Sort Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter Tab is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageFilterTabCaption))
					{
						Pass("The Filter Caption is displayed.");
						log.add("The Expected Caption was : " + Val[0]);
						String actVal = searchPageFilterTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(Val[0].contains(actVal))
						{
							Pass("The Filter Caption matches the expected one.",log);
						}
						else
						{
							Fail("The Filter Caption does not mathces the expected one.",log);
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
					}
				}
				else
				{
					Fail("The Filter Tab is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(searchPageSortButton))
				{
					Pass("The Sort button is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
					{
						Pass("The Sort Caption is displayed.");
						log.add("The Expected Caption was : " + Val[1]);
						String actVal = searchPageSortTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(Val[1].contains(actVal))
						{
							Pass("The Sort Caption matches the expected one.",log);
						}
						else
						{
							Fail("The Sort Caption does not mathces the expected one.",log);
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
					}
				}
				else
				{
					Fail("The Sort Tab is not displayed.");
				}
			}
			else
			{
				Fail("The Filter Tab Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1858 Issue ." + e.getMessage());
			Exception(" BRM - 1858 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1875 Verify that the "sort" must be displayed below the header with arrow pointing down as per the creative.*/
	public void searchPageSortVisible()
	{
		ChildCreation(" BRM - 1875 Verify that the sort must be displayed below the header.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM863", sheet, 6);
			String[] Val = cellVal.split("\n");
			if(BNBasicfeature.isElementPresent(searchPageFilterTabContainer))
			{
				BNBasicfeature.scrollup(header, driver);
				Pass("The Filter and Sort Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageSortButton))
				{
					Pass("The Sort button is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
					{
						Pass("The Sort Caption is displayed.");
						log.add("The Expected Caption was : " + Val[1]);
						String actVal = searchPageSortTabCaption.getText();
						log.add("The Actual Caption was : " + actVal);
						if(Val[1].contains(actVal))
						{
							Pass("The Sort Caption matches the expected one.",log);
						}
						else
						{
							Fail("The Sort Caption does not mathces the expected one.",log);
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
					}
				}
				else
				{
					Fail("The Sort Tab is not displayed.");
				}
			}
			else
			{
				Fail("The Filter Tab Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1875 Issue ." + e.getMessage());
			Exception(" BRM - 1875 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1876 Verify that on selecting sorting button the below listed sorting options must be displayed in the overlay.*/
	/* BRM - 1877 Verify that the Top matches option should be selected by default.*/
	public void searchPageSortOptions()
	{
		ChildCreation(" BRM - 1876 Verify that on selecting sorting button the below listed sorting options must be displayed in the overlay.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM885", sheet, 6);
			String[] Val = cellVal.split("\n");
			boolean srtFound = false;
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(searchPagePopUpMask))
			{
				jsclick(searchPagePopUpMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageSortButton))
			{
				Pass("The Sort button is displayed.");
				boolean enabled = false;
				WebDriverWait w1 = new WebDriverWait(driver, 1);
				do
				{
					try
					{
						act.moveToElement(searchPageSortButton).click().build().perform();
						w1.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
						enabled = true;
					}
					catch(Exception e)
					{
						enabled = false;
						enabled = searchPageSortOptionFacet.getAttribute("style").contains("block");
					}
				}while(enabled==false);
				
				/*searchPageSortButton.click();
				wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));*/
				wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
				Thread.sleep(100);
				if(BNBasicfeature.isListElementPresent(searchPageSortFacetsLists))
				{
					Pass("The Sort List Facets is displayed.");
					srtFound = false;
					for(int i = 0; i<Val.length;i++)
					{
						log.add("The Searching Value is : " + Val[i]);
						for(int j=1; j<=searchPageSortFacetsLists.size();j++)
						{
							String srchName = searchPageSortFacetsLists.get(j-1).getText();
							
							if(Val[i].contains(srchName))
							{
								srtFound = true;
								log.add("The Searching Value is : " + srchName);
								break;
							}
							else
							{
								srtFound = false;
								continue;
							}
						}
						
						if(srtFound==true)
						{
							Pass("The Searched value is found.",log);
						}
						else
						{
							Fail("The Searched value is not found.",log);
						}
					}
				}
				else
				{
					Fail("The Sort Facets List is not displayed.");
				}
			}
			else
			{
				Fail("The Sort button is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1876 Issue ." + e.getMessage());
			Exception(" BRM - 1876 Issue." + e.getMessage());
		}
		
		/* BRM - 1877 Verify that the Top matches option should be selected by default.*/
		ChildCreation(" BRM - 1877 Verify that the Top matches option should be selected by default.");
		try
		{
			String expColor = BNBasicfeature.getExcelVal("BRM1877", sheet, 5);
			String actColor = "";
			boolean srtFound = false;
			boolean sel = false;
			String cellVal = BNBasicfeature.getExcelVal("BRM1877", sheet, 6);
			if(BNBasicfeature.isListElementPresent(searchPageSortFacetsLists))
			{
				Pass("The Sort List Facets is displayed.");
				srtFound = false;
				for(int j=1; j<=searchPageSortFacetsLists.size();j++)
				{
					String srchName = searchPageSortFacetsLists.get(j-1).getText();
					actColor = searchPageSortFacetsLists.get(j-1).getCssValue("color");
					sel = searchPageSortFacetsLists.get(j-1).getAttribute("selected").contains("selected");
					if(cellVal.contains(srchName))
					{
						srtFound = true;
						sel = true;
						log.add("The Searching Value is : " + srchName);
						break;
					}
					else
					{
						srtFound = false;
						continue;
					}
				}
					
				if(srtFound==true)
				{
					Pass("The Searched value is found.",log);
					Color colorhxCnvt = Color.fromString(actColor);
					String hexCode = colorhxCnvt.asHex();
					
					if(sel==true)
					{
						Pass("The Top Matches is selected by default.");
						log.add("The Expected Color was : " + expColor);
						log.add("The Actual Color was : " + hexCode);
						if(expColor.equals(hexCode))
						{
							Pass("The Top Matches is highlighted by default.");
						}
						else
						{
							Fail("The Top Matches is not highlighted by default.");	
						}
					}
					else
					{
						Fail("The Top Matches is not selected by default.");
					}
				}
				else
				{
					Fail("The Searched value is not found.",log);
				}
				searchPageSortButton.click();
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1877 Issue ." + e.getMessage());
			Exception(" BRM - 1877 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1874 Verify that the selected page should be underlined in pagination.*/
	/* BRM - 889 Ellipses added after 5 as per the creative if the results found more than 5 pages.*/
	/* BRM - 924  Verify that the pagination arrows are working and navigating to the next/prev pages*/
	public void searchPageCurrentPageUnderline()
	{
		ChildCreation(" BRM - 889 Ellipses added after 5 as per the creative if the results found more than 5 pages.");
		try
		{
			int val = 0;
			String cellval = BNBasicfeature.getExcelVal("BRM889", sheet, 6);
			val = searchPrdtTotalCount;
			if(val>100)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
				{
					Pass("The Pagingation Continer is displayed.");
					if(BNBasicfeature.isElementPresent(searchPagePaginationElipses))
					{
						String currVal = searchPagePaginationElipses.getText();
						if(cellval.equals(currVal))
						{
							Pass("The Elipses is displayed.");
						}
						else
						{
							Fail("The Elipses is not displayed.");
						}
					}
					else
					{
						Fail("The Elipses pagination is not displayed.");
					}
				}
				else
				{
					Fail("The Pagination continaer is not set displayed.");
				}
			}
			else
			{
				Skip("The Search Total Products is less than 20. So Pagination is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 889 Issue ." + e.getMessage());
			Exception(" BRM - 889 Issue." + e.getMessage());
		}
		
		/* BRM - 1874 Verify that the selected page should be underlined in pagination.*/
		ChildCreation(" BRM - 1874 Verify that the selected page should be underlined in pagination.");
		try
		{
			int currPage = 0;
			String currText = "";
			int val = 0;
			resultContainerDetails();
			val = searchPrdtTotalCount;
			if(val>20)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainerCurrentPages))
				{
					currPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
					log.add("The Current page before selection is : " + currPage);
					currText = searchPagePaginationContainerCurrentPages.getCssValue("text-decoration");
					if(currText.contains("underline"))
					{
						Pass("The text is underlined.");
					}
					else
					{
						Fail("The Text is not underlined.");
					}
				}
				else
				{
					Fail("The Current Page is not displayed when there are more than 20 Products.");
				}
			}
			else
			{
				Skip("No pagination is displayed since the total Product count is less than 20.The total product count is : " + val);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1874 Issue ." + e.getMessage());
			Exception(" BRM - 1874 Issue." + e.getMessage());
		}
		
		/* BRM - 924  Verify that the pagination arrows are working and navigating to the next/prev pages*/
		ChildCreation(" BRM - 924  Verify that the pagination arrows are working and navigating to the next/prev pages.");
		try
		{
			int currPage = 0,newCurrPage = 0;
			int val = 0;
			
			if(BNBasicfeature.isElementPresent(searchPagePopUpMask))
			{
				//Thread.sleep(500);
				jsclick(searchPagePopUpMask);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(100);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			resultContainerDetails();
			val = searchPrdtTotalCount;
			if(val>100)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainerCurrentPages))
				{
					wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
					currPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
					log.add("The Current page before selection is : " + currPage);
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						BNBasicfeature.scrolldown(searchPagePaginationContainer, driver);
						if(BNBasicfeature.isElementPresent(searchPagePaginationRightArrow))
						{
							Pass("The Pagination Container is displayed and the Right Arrow aswell displayed.");
							jsclick(searchPagePaginationRightArrow);
							wait.until(ExpectedConditions.visibilityOf(header));
							BNBasicfeature.scrolldown(searchPagePaginationContainer, driver);
							wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
							newCurrPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
							log.add("The Current page after selection is : " + newCurrPage);
							if((currPage<newCurrPage)&&(currPage!=newCurrPage))
							{
								Pass("The user is navigated to the new Page.",log);
								if(BNBasicfeature.isElementPresent(searchPagePaginationLeftArrow))
								{
									Pass("The Left Arrow is displayed for the user to navigate backwards.");
									currPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
									jsclick(searchPagePaginationLeftArrow);
									wait.until(ExpectedConditions.visibilityOf(header));
									BNBasicfeature.scrolldown(searchPagePaginationContainer, driver);
									wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
									newCurrPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
									log.add("The Current page after selection is : " + newCurrPage);
									if((currPage>newCurrPage)&&(currPage!=newCurrPage))
									{
										Pass("The user is navigated to the new Page backwards successfully.",log);
									}
									else
									{
										int pgval = 0; 
										if(BNBasicfeature.isListElementPresent(searchPagePaginationContainerPages))
										{
											pgval = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
										}
										log.add("The Current page is : " + pgval);
										Fail("The Left Arrow is not displayed for the user to navigate backwards.");
									}
								}
								else
								{
									int size = 0,pgval = 0; 
									if(BNBasicfeature.isListElementPresent(searchPagePaginationContainerPages))
									{
										size = searchPagePaginationContainerPages.size();
										pgval = Integer.parseInt(searchPagePaginationContainerPages.get(size).getText());
									}
									log.add("The Total number of page in the Pagination list container is  : " + pgval);
									Fail("The Left Arrow is not displayed for the user to navigate backwards.");
								}
							}
							else
							{
								Fail("The user is not navigated to the new Page.",log);
							}
						}
						else
						{
							Fail("No Right arrow is displayed for the user to navigate.");
						}
					}
					else
					{
						Fail("The Pagination Container is not displayed.");
					}
				}
				else
				{
					Fail("The Current page is not set displayed.");
				}
			}
			else
			{
				Skip("The Search Total Products is less than 20. So Pagination is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 924 Issue ." + e.getMessage());
			Exception(" BRM - 924 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 888 Pagination should be used to navigate to view all products.*/
	public void searchPaginationPageNavigation()
	{
		ChildCreation(" BRM - 888 Pagination should be used to navigate to view all products. ");
		try
		{
			int currPage = 0,newCurrPage = 0;
			int val = 0;
			ArrayList<String> selPrdtid = new ArrayList<>();
			//ArrayList<String> selprdtname = new ArrayList<>();
			resultContainerDetails();
			val = searchPrdtTotalCount;
			if(val>20)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainerCurrentPages))
				{
					wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
					currPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
					log.add("The Current page before selection is : " + currPage);
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						BNBasicfeature.scrolldown(searchPagePaginationContainer, driver);
						Pass("The Pagination Container is displayed.");
						if(BNBasicfeature.isListElementPresent(searchPagePaginationContainerPages))
						{
							Random r =  new Random();
							int sel = 0;
							do
							{
								wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
								sel = r.nextInt(searchPagePaginationContainerPages.size());
								if(sel<1)
								{
									sel = 1;
								}
							}while(sel<=currPage);
							
							jsclick(searchPagePaginationContainerPages.get(sel));
							//searchPagePaginationContainerPages.get(sel).click();
							//Thread.sleep(2000);
							wait.until(ExpectedConditions.visibilityOf(header));
							//Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOfAllElements(searchPagePaginationContainerPages));
							newCurrPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
							log.add("The Current page after selection is : " + newCurrPage);
							if(currPage==newCurrPage)
							{
								Fail("The User is not Navigated to the New page.");
							}
							else
							{
								Pass("The User is navigated to the New page.");
								if(BNBasicfeature.isListElementPresent(searchPageProductLists))
								{
									selPrdtid = getprdtId();
									//selprdtname = getPrdTitle();
									log.add("The Selected Product ID is  : " + selPrdtid);
									log.add("Selected Product ID : " + selPrdtid);
									
									for(int i = 0; i<selPrdtid.size(); i++)
									{
										if(selPrdtid.isEmpty())
										{
											Fail("The Product Id is empty.",log);
										}
										else
										{
											Pass("The Product Id is displayed.",log);
										}
										
										/*if(selprdtname.isEmpty())
										{
											Fail("The Product Name is empty.",log);
										}
										else
										{
											Pass("The Product Name is displayed.",log);
										}*/
									}
								}
								else
								{
									Fail("The Product list is not displayed.");
								}	
							}
						}
						else
						{
							Fail("The Search Container Page list is not displayed.");
						}
					}
					else
					{
						Fail("The Pagination Container is not displayed.");
					}
				}
				else
				{
					Fail("The Current page is not set displayed.");
				}
				BNBasicfeature.scrolldown(searchPagePaginationContainer, driver);
				//Thread.sleep(1000);
				jsclick(searchPagePaginationContainerPages.get(currPage));
				//searchPagePaginationContainerPages.get(currPage).click();
				//Thread.sleep(2000);
				wait.until(ExpectedConditions.visibilityOf(header));
				//Thread.sleep(1000);
			}
			else
			{
				Skip("The Search Total Products is less than 20. So Pagination is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 888 Issue ." + e.getMessage());
			Exception(" BRM - 888 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1903 Verify that Filters,sort, list,grid views are displayed in same line as per the creative. */
	public void searchPageFilteSortIconElements()
	{
		ChildCreation("BRM - 1903 Verify that Filters,sort, list,grid views are displayed in same line as per the creative. ");
		try
		{
			if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
			{
				//BNBasicfeature.scrollup(header, driver);
				Pass("The Search page filter section is displayed.");
			}
			else
			{
				Fail("The Search Page filter section is not displayed.");
			}

			if(BNBasicfeature.isElementPresent(listViewIcon))
			{
				Pass("The List View is displayed.");
			}
			else
			{
				Fail("The List View icon is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(gridViewIcon))
			{
				Pass("The Grid View is displayed.");
			}
			else
			{
				Fail("The Grid View icon is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				Pass("The Page is loaded in Grid View List as expected.");
			}
			else
			{
				Fail("The Page is not loaded in the expected Grid View.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1903 Issue ." + e.getMessage());
			Exception(" BRM - 1903 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1857 Verify that the total count per page from the total products ,the searched category name, products and product details are displayed correctly as per the classic site*/
	public void searchResultHeaderValidation()
	{
		ChildCreation(" BRM - 1857 Verify that the total count per page from the total products ,the searched category name, products and product details are displayed correctly as per the classic site.");
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductContainerResultsList));
			Thread.sleep(500);
			if(BNBasicfeature.isListElementPresent(searchPageProductContainerResultsList))
			{
				String originText = searchPageProductCountContainer.getText();
				log.add("The Searched Term Displayed result is : " + originText);
				String text = searchPageProductContainerResultsList.get(2).getText();
				log.add("The Searched Term displayed is : " + text);
				String expval = BNBasicfeature.getExcelVal("BRM1841", sheet, 3);
				String[] val = expval.split("\n");
				log.add("The Expected value was : " + expval);
				resultContainerDetails();
				Integer  filteredCount = searchPrdtTotalCount;
				
				if(filteredCount>0)
				{
					Pass("The Filtered Count is displayed .",log);
				}
				else
				{
					Fail("The Filtered Count is not displayed .",log);
				}
				
				boolean found = false;
				log.add("The Searched Term Displayed result is : " + originText);
				log.add("The Searched Term displayed is : " + text);
				log.add("The Expected value was : " + expval);
				for(int i = 0;i<val.length;i++)
				{
					if(originText.contains(val[i])||(text.equalsIgnoreCase(val[i])))
					{
						log.add("The Expected value was : " + val[i]);
						found = true;
						break;
					}
					else
					{
						continue;
					}
				}
				
				if(found==true)
				{
					Pass("The Searched Product Search Term is displayed. The displayed title is : " + originText + " and the searched term is : " + text,log);
				}
				else
				{
					Fail("The Searched Product Search Term is not displayed. The displayed title is : " + originText,log);
				}
			}
			else
			{
				Fail("The Searched Products List Details are not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1857 Issue ." + e.getMessage());
			Exception(" BRM - 1857 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 870 Verify that the Results should be shown for common synonyms or misspellings.*/
	/* BRM - 1859 Verify that the pagination should not be shown if the results not more than one page.*/
	/* BRM - 1864 Verify that the product price is displayed with currency symbol for each product as per the creative.*/
	public void searchMisspellA()
	{
		ChildCreation("BRM - 870 Verify that the Results should be shown for common synonyms or misspellings.");
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM870A", sheet, 3);
			boolean bkOk = searchPrdt(book); 
			if(bkOk==true)
			{
				Pass("The Searched Content was loaded successfully.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
				{
					Pass("The Search Misspell suggestion is displayed.");
					if(BNBasicfeature.isElementPresent(searchMisSpellSuggestionText))
					{
						Pass("The Misspell suggestion text is displayed.");
						String text = searchMisSpellSuggestionText.getText();
						log.add("The Actual result is : " + text);
						if(text.isEmpty())
						{
							Fail("The Misspelled suggestion text seems to be empty.");
						}
						else
						{
							Pass("The Misspelled suggestion text is displayed. The displayed text is : " + text);
						}
					}
					else
					{
						Fail("The Misspell Suggestion text is not displayed.");
					}
				}
				else
				{
					Fail("The Search Misspell suggestion is not displayed.");
				}
			}
			else
			{
				Fail("The Searched Page Failed to Load.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 870 Issue ." + e.getMessage());
			Exception(" BRM - 870 Issue." + e.getMessage());
		}
		
		/* BRM - 1859 Verify that the pagination should not be shown if the results not more than one page.*/
		ChildCreation(" BRM - 1859 Verify that the pagination should not be shown if the results not more than one page.");
		try
		{
			//Thread.sleep(500);
			String originalText = searchPageProductCountContainer.getText();
			log.add("The displayed Search Terms results is : " + originalText);
			resultContainerDetails();
			Integer prdtCount = searchPrdtTotalCount;
			log.add("The Total Product Count is : " + prdtCount);
			if(prdtCount<20)
			{
				BNBasicfeature.scrolldown(footerSectionContainer, driver);
				Thread.sleep(100);
				if(BNBasicfeature.isListElementPresent(searchPagePaginationContainerPages))
				{
					Fail("The Pagination is displayed for the Searched Products eventhough the Total Product Count is less than 20.",log);
				}
				else
				{
					Pass("The Pagination is not displayed for the Searched Products.",log);
				}
			}
			else
			{
				Skip(" The Product Count is not greater than 20 so pagination is not displayed The Product Count in the page is  : " + prdtCount);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1859 Issue ." + e.getMessage());
			Exception(" BRM - 1859 Issue." + e.getMessage());
		}
		
		/* BRM - 1864 Verify that the product price is displayed with currency symbol for each product as per the creative.*/
		ChildCreation(" BRM - 1864 Verify that the product price is displayed with currency symbol for each product as per the creative.");
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				Pass("The Search Content Result Container is displayed.");
				for(int i=1;i<searchPageProductLists.size();i++)
				{
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					WebElement priceContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
					if(BNBasicfeature.isElementPresent(priceContainer))
					{
						Pass("The Price container is displayed.");
						List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
						int size = prdtType.size();
						if(size==1)
						{
							size=2;
						}
						for(int j = 1;j<=size-1;j++)
						{
							String priceLabel =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
							//System.out.println(priceLabel);
							if(priceLabel.isEmpty())
							{
								Pass("The price label is not displayed.");
							}
							else
							{
								log.add("The displayed price label is : " + priceLabel);
								Pass("The price label is displayed.");
								if(priceLabel.contains("Market"))
								{
									continue;
								}
								else
								{
									String salePrice =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listSalePrice')]")).get(j-1).getText();
									log.add("The displayed Sale Price is : " + salePrice);
									if(salePrice.contains("$")||salePrice.contains("FREE"))
									{
										Pass("The $ Symbol is displayed in the Sale Price.",log);
									}
									else
									{
										Fail("The $ Symbol is not displayed in the Sale Price.",log);
									}
									
									if(!salePrice.contains("FREE"))
									{
										Float sPrice = Float.parseFloat(salePrice.replace("$", "").replace(",", ""));
										//System.out.println(sPrice);
										if(sPrice>=0)
										{
											Pass("The Sale Price is displayed. The displayed Sale price is : " + sPrice);	
										}
										else
										{
											Fail("The Sale Price is not displayed. The displayed Sale price is : " + sPrice);
										}
									}
								}
							}
						}
					}
					else
					{
						Fail("The Price container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search Content Result Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1864 Issue ." + e.getMessage());
			Exception(" BRM - 1864 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 879 Verify that Selected filters should be displayed on top.*/
	/* BRM - 880 Verify that User can able to deselect the filters . */
	/* BRM - 882 Verify that If we clear all selected filters it should go to the default search result page. */
	/* BRM - 908 User can able to remove the selected filters by using close button X.*/
	/* BRM - 881 Verify that the deselected filters should be removed from the top.*/
	/* BRM - 912 Selected filters should be displayed on top.*/
	/* BRM - 1845 Verify that Total number of products found must be displayed.*/
	/* BRM - 931 Verify that the keyword suggestions in the search results page must be displayed with the total count text.*/
	/* BRM - 933 Now on selecting the filter button, already selcted filter option must be displayed above the filter options.*/
	/* BRM - 935 Verify that the page is refreshed again with selected filter options if we select any filter. */
	/* BRM - 932 Only the filtered attribute products must be displayed after refeshing the page once the filter selected.*/
	/* BRM - 1863 Verify that the total number of products available in the selected category is displayed.*/
	/* BRM - 1861 Verify that the category title matches the selected category. */
	/* BRM - 1891 Verify that on selecting the close symbol then the filter page must be closed and the product list page must be displayed.*/
	/* BRM - 1897 Verify that selected options are shown when we select filters.*/
	/* BRM - 1893 Verify that the filter options are working fine.*/
	public void searchPageFilterApplyRemove()
	{
		ChildCreation(" BRM - 879 Verify that Selected filters should be displayed on top.");
		boolean brm908 = false;
		boolean brm881 = false;
		boolean brm912 = false;
		boolean brm1845 = false;
		boolean brm933 = false;
		boolean brm1861 = false;
		boolean brm1891 = false;
		boolean brm1897 = false;
		String befRefresText = "",afterRefreshText = "";
		Integer initalCount = 0;
		Integer modifiedCount = 0;
		Integer newCount = 0;
		ArrayList<String> orPrdId = new ArrayList<>();
		ArrayList<String> mrPrdId = new ArrayList<>();
		ArrayList<String> nrPrdId = new ArrayList<>();
		Integer ocount = null;
		boolean facetApplied = false;
		try
		{
			resultContainerDetails();
			ocount = searchPrdtTotalCount;
			orPrdId = getprdtId();
			
			boolean applyFilter = applyFilter();
			if(applyFilter==true)
			{
				facetApplied = facetApplied();
				if(facetApplied==true)
				{
					befRefresText = setFacVal;
					initalCount = searchPrdtTotalCount;
					log.add("The Selected Facet Name was : " + selFacVal);
					log.add("The Set Facet Name is : " + setFacVal);
					if(facetFound==true)
					{
						Pass("The Set facet is displayed.",log);
						brm912 = true;
						brm933 = true;
						brm1897 = true;
					}
					else
					{
						Fail("The Set facet is not displayed.",log);
						brm912 = false;
						brm933 = false;
						brm1897 = false;
					}
					
					resultContainerDetails();
					modifiedCount = searchPrdtTotalCount;
					log.add("The Modified filtered Count is  : " + modifiedCount);
					log.add("The Set Facet Value was : " + selFacVal);
					
					if(ocount>=modifiedCount)
					{
						Pass("The Filtered Count is updated correctly.",log);
					}
					else
					{
						Fail("The Filtered Count is not updated correctly.",log);
					}
				}
				else
				{
					Fail("The Facet is not applied.",log);
				}
			}
			else
			{
				Fail("The Facets is not selected.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 879 Issue ." + e.getMessage());
			Exception(" BRM - 879 Issue." + e.getMessage());
		}
		
		/* BRM - 912 Selected filters should be displayed on top.*/
		ChildCreation("BRM - 912 Selected filters should be displayed on top.");
		try
		{
			if(brm912==true)
			{
				Pass("The Set facet is displayed.",log);
			}
			else
			{
				Fail("The Set facet is not displayed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 912 Issue ." + e.getMessage());
			Exception(" BRM - 912 Issue." + e.getMessage());
		}
		
		/* BRM - 933 Now on selecting the filter button, already selcted filter option must be displayed above the filter options.*/
		ChildCreation(" BRM - 933 Now on selecting the filter button, already selcted filter option must be displayed above the filter options.");
		try
		{
			if(brm933==true)
			{
				Pass("The Set facet is displayed.",log);
			}
			else
			{
				Fail("The Set facet is not displayed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 933 Issue ." + e.getMessage());
			Exception(" BRM - 933 Issue." + e.getMessage());
		}
		
		
		/* BRM - 931 Verify that the keyword suggestions in the search results page must be displayed with the total count text.*/
		ChildCreation("BRM - 931 Verify that the keyword suggestions in the search results page must be displayed with the total count text.");
		try
		{
			//resultContainerDetails();
			int filteredCount = searchPrdtTotalCount;
			log.add("The Set Facet Value was : " + selFacVal);
			log.add("The Filtered Total Product Count : " + filteredCount);
			
			if(filteredCount>0)
			{
				Pass("The Filtered Count is displayed .",log);
				brm1845 = true;
			}
			else
			{
				Fail("The Filtered Count is not displayed .",log);
				brm1845 = false;
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 931 Issue ." + e.getMessage());
			Exception(" BRM - 931 Issue." + e.getMessage());
		}
		
		/* BRM - 1863 Verify that the total number of products available in the selected category is displayed.*/
		ChildCreation(" BRM - 1863 Verify that the total number of products available in the selected category is displayed.");
		try
		{
			Integer  filteredCount = searchPrdtTotalCount;
			log.add("The Filtered product count is : " + filteredCount);
			
			if(filteredCount>0)
			{
				Pass("The Filtered Count is displayed .",log);
			}
			else
			{
				Fail("The Filtered Count is not displayed .",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1863 Issue ." + e.getMessage());
			Exception(" BRM - 1863 Issue." + e.getMessage());
		}
		
		/* BRM - 1845 Verify that Total number of products found must be displayed.*/
		ChildCreation(" BRM - 1845 Verify that Total number of products found must be displayed.");
		try
		{
			if(brm1845==true)
			{
				Pass("The Filtered Count is displayed.");
			}
			else
			{
				Fail("The Filtered Count is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1845 Issue ." + e.getMessage());
			Exception(" BRM - 1845 Issue." + e.getMessage());
		}
		
		/* BRM - 935 Verify that the page is refreshed again with selected filter options if we select any filter. */
		ChildCreation(" BRM - 935 Verify that the page is refreshed again with selected filter options if we select any filter.");
		try
		{
			driver.navigate().refresh();
			wait.until(ExpectedConditions.visibilityOf(header));
			//Thread.sleep(1000);
			resultContainerDetails();
			modifiedCount = searchPrdtTotalCount;
			facetApplied = facetApplied();
			afterRefreshText = setFacVal;
			log.add("The Selected facet was : " + selFacVal);
			log.add("The Set facet is : " + setFacVal);
			log.add("The Selected facet before Refreshing the page was : " + befRefresText);
			log.add("The Selected facet after Refreshing the page is : " + afterRefreshText);
			if(befRefresText.equals(afterRefreshText))
			{
				Pass("The Filtered Facet is displayed after refreshing the page.",log);
			}
			else
			{
				Fail("The Filtered Facet is not displayed after refreshing the page.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 935 Issue ." + e.getMessage());
			Exception(" BRM - 935 Issue." + e.getMessage());
		}
		
		/* BRM - 932 Only the filtered attribute products must be displayed after refeshing the page once the filter selected.*/
		ChildCreation(" BRM - 932 Only the filtered attribute products must be displayed after refeshing the page once the filter selected.");
		try
		{
			log.add("The Modified filtered Count is  : " + modifiedCount);
			log.add("The Set Facet Value was : " + selFacVal);
					
			if(initalCount>=modifiedCount)
			{
				Pass("The Filtered Count is updated correctly.",log);
			}
			else
			{
				Fail("The Filtered Count is not updated correctly.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 932 Issue ." + e.getMessage());
			Exception(" BRM - 932 Issue." + e.getMessage());
		}
		
		/* BRM - 882 Verify that If we clear all selected filters it should go to the default search result page.*/
		ChildCreation("BRM - 882 Verify that If we clear all selected filters it should go to the default search result page.");
		try
		{
			if(facetApplied==true)
			{
				mrPrdId = getprdtId();
				log.add("The Selected Facet Name was : " + selFacVal);
				log.add("The Set Facet Name is : " + setFacVal);
				resultContainerDetails();
				Integer fcount = searchPrdtTotalCount;
				log.add("The Total Product Count after applying the filter is : " + fcount);
				if(ocount>=fcount)
				{
					Pass("The Original Count is greater than the Filtered Total Count.");
				}
				else
				{
					Fail("After applying the filter the product Count has increased.");
				}
				
				if(orPrdId.equals(mrPrdId))
				{
					Fail("The Page is refreshed and the modified Filtered Results are still displaying the old results.");
					Fail("The Original Product Id before applying the filter was : " + orPrdId);
					Fail("The Product Id after applying the filter was : " + mrPrdId);
				}
				else
				{
					Pass("The Page is refreshed and the Filtered Results are displayed.");
					Pass("The Original Product Id before applying the filter was : " + orPrdId);
					Pass("The Product Id after applying the filter was : " + mrPrdId);
				}
				
				if(facetFound==true)
				{
					Pass("The Set facet is displayed.",log);
				}
				else
				{
					Fail("The Set facet is not displayed.",log);
				}
			}
			else
			{
				Fail("The Facet is not applied.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 882 Issue ." + e.getMessage());
			Exception(" BRM - 882 Issue." + e.getMessage());
		}
		
		/*BRM - 880 Verify that User can able to deselect the filters*/
		ChildCreation("BRM - 880 Verify that User can able to deselect the filters.");
		try
		{
			boolean filterRmv = removeFilters();
			log.add("The Selected Facet Name was : " + selFacVal);
			log.add("The Set Facet Name is : " + setFacVal);
			
			if(filterRmv==true)
			{
				Pass("The User was able to remove the filter successfully.",log);
				nrPrdId = getprdtId();
				resultContainerDetails();
				newCount = searchPrdtTotalCount;
				Integer ncount = searchPrdtTotalCount;
				log.add("The Total Product Count after removing the filter is : " + ncount);
				if(ocount.equals(ncount))
				{
					Pass("The Original Count and the Count after removing the filter are same as expected.");
					brm908 = true;
					brm881 = true;
				}
				else
				{
					Fail(" The Original Count and the new Count after removing the filter are same as expected.");
					brm908 = false;
					brm881 = false;
				}
			}
			else
			{
				Fail("The Filter was failed to be removed.",log);
			}
			
			if(orPrdId.equals(nrPrdId))
			{
				Pass("The Page is refreshed and the Filtered Results are displayed.");
				Pass("The Original Product Id before applying the filter was : " + orPrdId);
				Pass("The Product Id after removing the filter is : " + mrPrdId);
				brm908 = true;
				brm881 = true;
			}
			else
			{
				Fail("The Page is refreshed and the modified Filtered Results are still displaying the old results.");
				Fail("The Original Product Id before applying the filter was : " + orPrdId);
				Fail("The Product Id after removing the filter is : " + nrPrdId);
				brm908 = false;
				brm881 = false;
			}
			
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				Pass("The Search Result Page is displayed after the filter is removed.");
			}
			else
			{
				Fail("The Search Result Page is not displayed after the filter is removed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 880 Issue ." + e.getMessage());
			Exception(" BRM - 880 Issue." + e.getMessage());
		}
		
		/* BRM - 908 User can able to remove the selected filters by using close button X.*/
		ChildCreation(" BRM - 908 User can able to remove the selected filters by using close button X.");
		try
		{
			if(brm908==true)
			{
				Pass("The Search Result Page is displayed after the filter is removed.");
			}
			else
			{
				Fail("The Search Result Page is not displayed after the filter is removed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 908 Issue ." + e.getMessage());
			Exception(" BRM - 908 Issue." + e.getMessage());
		}
		
		/* BRM - 881 Verify that the deselected filters should be removed from the top.*/
		ChildCreation(" BRM - 881 Verify that the deselected filters should be removed from the top.");
		try
		{
			if(brm881==true)
			{
				Pass("The Factes are removed and the Result Page is displayed.");
				brm1861 = true;
				brm1891 = true;
			}
			else
			{
				Fail("The Factes are not removed removed.");
				brm1861 = false;
				brm1891 = false;
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 881 Issue ." + e.getMessage());
			Exception(" BRM - 881 Issue." + e.getMessage());
		}
		
		/* BRM - 1861 Verify that the category title matches the selected category. */
		ChildCreation(" BRM - 1861 Verify that the category title matches the selected category.");
		try
		{
			if(brm1861==true)
			{
				Pass("The User was able to remove the filter successfully.",log);
			}
			else
			{
				Fail("The Filter was failed to be removed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1861 Issue ." + e.getMessage());
			Exception(" BRM - 1861 Issue." + e.getMessage());
		}
		
		/* BRM - 1891 Verify that on selecting the close symbol then the filter page must be closed and the product list page must be displayed. */
		ChildCreation(" BRM - 1861 Verify that the category title matches the selected category.");
		try
		{
			if(brm1891==true)
			{
				Pass("The User was able to remove the filter successfully.",log);
			}
			else
			{
				Fail("The Filter was failed to be removed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1891 Issue ." + e.getMessage());
			Exception(" BRM - 1891 Issue." + e.getMessage());
		}
		
		/* BRM - 1897 Verify that selected options are shown when we select filters.*/
		ChildCreation(" BRM - 1897 Verify that selected options are shown when we select filters.");
		try
		{
			if(brm1897==true)
			{
				Pass("The User was able to remove the filter successfully.",log);
			}
			else
			{
				Fail("The Filter was failed to be removed.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1897 Issue ." + e.getMessage());
			Exception(" BRM - 1897 Issue." + e.getMessage());
		}
		
		/* BRM - 1893 Verify that the filter options are working fine.*/
		ChildCreation(" BRM - 1893 Verify that the filter options are working fine.");
		try
		{
			if(newCount.equals(ocount))
			{
				Pass("The Filter has been removed and the Count is displayed which is equal to the original count.");
			}
			else
			{
				Fail("The Filter has been removed but the Total Count of the Products is still same.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1893 Issue ." + e.getMessage());
			Exception(" BRM - 1893 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 870 Verify that the Results should be shown for common synonyms or misspellings.*/
	/* BRM - 938 Verify that the "did u mean"text should come with all the incorrect search suggestions as per the creative.*/
	/* BRM - 939 Verify that the related search results and suggestions should be displayed for mismatched/incorrect keyword with the text "did u mean"*/
	public void searchMisspellSuggestion()
	{
		ChildCreation(" BRM - 870 Verify that the Results should be shown for common synonyms or misspellings.");
		boolean bkOk = false; 
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM870B", sheet, 3);
			bkOk = searchPrdt(book); 
			if(bkOk==true)
			{
				Pass("The Searched Content was loaded successfully.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
				{
					Pass("The Search Misspell suggestion is displayed.");
					if(BNBasicfeature.isElementPresent(searchMisSpellSuggestionText))
					{
						Pass("The Misspell suggestion text is displayed.");
						String text = searchMisSpellSuggestionText.getText();
						log.add("The displayed suggestion text is : " + text);
						if(text.isEmpty())
						{
							Fail("The Misspelled suggestion text seems to be empty.",log);
						}
						else
						{
							Pass("The Misspelled suggestion text is displayed.",log);
						}
						
						if(BNBasicfeature.isListElementPresent(searchMisSpellSuggestionLists))
						{
							Pass("The Misspell suggestion List is displayed.");
							for(int i = 0;i<searchMisSpellSuggestionLists.size();i++)
							{
								text = searchMisSpellSuggestionLists.get(i).getText();
								log.add("The displayed suggestion text is : " + text);
								if(text.isEmpty())
								{
									Fail("The Misspelled suggestion list text seems to be empty.",log);
								}
								else
								{
									Pass("The Misspelled suggestion list text is displayed.",log);
								}
							}
						}
						else
						{
							Fail("No Misspell Suggestion is displayed.");
						}
					}
					else
					{
						Fail("The Misspell Suggestion text is not displayed.");
					}
				}
				else
				{
					Fail("The Search Misspell suggestion is not displayed.");
				}
			}
			else
			{
				Fail("The Searched Page Failed to Load.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 870 Issue ." + e.getMessage());
			Exception(" BRM - 870 Issue." + e.getMessage());
		}
		
		/* BRM - 938 Verify that the "did u mean"text should come with all the incorrect search suggestions as per the creative*/
		ChildCreation(" BRM - 938 Verify that the did u mean text should come with all the incorrect search suggestions as per the creative.");
		String text = "";
		try
		{
			String expColor = BNBasicfeature.getExcelVal("BRM938", sheet, 5);
			if(bkOk==true)
			{
				Pass("The Searched Content was loaded successfully.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
				{
					Pass("The Search Misspell suggestion is displayed.");
					if(BNBasicfeature.isListElementPresent(searchMisSpellSuggestionLists))
					{
						Pass("The Misspell suggestion List is displayed.");
						for(int i = 0;i<searchMisSpellSuggestionLists.size();i++)
						{
							text = searchMisSpellSuggestionNavigationLink.get(i).getText();
							log.add("The displayed suggestion text is : " + text);
							if(text.isEmpty())
							{
								Fail("The Misspelled suggestion list text seems to be empty.",log);
							}
							else
							{
								Pass(" The Misspelled suggestion list text is displayed.",log);
								String actClor = searchMisSpellSuggestionNavigationLink.get(i).getCssValue("color");
								Color colhxv = Color.fromString(actClor);
								String actColor = colhxv.asHex();
								log.add("The Expected Color was : " + expColor);
								log.add("The Actual Color is : " + actColor);
								if(expColor.equals(actColor))
								{
									Pass("The Suggestion Text is highlighted as per the expected value.",log);
								}
								else
								{
									Fail("The Suggestion Text is not highlighted as per the expected value.",log);
								}
							}
						}
					}
					else
					{
						Fail("No Misspell Suggestion is displayed.");
					}
				}
				else
				{
					Fail("The Search Misspell suggestion is not displayed.");
				}
			}
			else
			{
				Fail("The Searched Page Failed to Load.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 938 Issue ." + e.getMessage());
			Exception(" BRM - 938 Issue." + e.getMessage());
		}
		
		/* BRM - 939 Verify that the related search results and suggestions should be displayed for mismatched/incorrect keyword with the text "did u mean".*/
		ChildCreation("BRM - 939 Verify that the related search results and suggestions should be displayed for mismatched/incorrect keyword with the text did u mean.");
		text = "";
		try
		{
			String expval = BNBasicfeature.getExcelVal("BRM939", sheet, 6);
			if(bkOk==true)
			{
				Pass("The Searched Content was loaded successfully.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
				{
					Pass("The Search Misspell suggestion is displayed.");
					if(BNBasicfeature.isListElementPresent(searchMisSpellSuggestionLists))
					{
						Pass("The Misspell suggestion List is displayed.");
						for(int i = 0;i<searchMisSpellSuggestionLists.size();i++)
						{
							text = searchMisSpellSuggestionNavigationLink.get(i).getText();
							log.add("The displayed suggestion text is : " + text);
							if(text.isEmpty())
							{
								Fail("The Misspelled suggestion list text seems to be empty.",log);
							}
							else
							{
								Pass("The Misspelled suggestion list text is displayed.",log);
								text = searchMisSpellSuggestionLists.get(i).getText();
								log.add("The displayed suggestion text is : " + text);
								log.add("The Expected Value was : " + expval);
								log.add("The Actual Value is : " + text);
								if(text.contains(expval))
								{
									Pass("The Suggestion Text Did you mean is displayed.",log);
								}
								else
								{
									Fail("The Suggestion Text Did you mean is not displayed or the value mismatches.",log);
								}
							}
						}
					}
					else
					{
						Fail("No Misspell Suggestion is displayed.");
					}
				}
				else
				{
					Fail("The Search Misspell suggestion is not displayed.");
				}
			}
			else
			{
				Fail("The Searched Page Failed to Load.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 939 Issue ." + e.getMessage());
			Exception(" BRM - 939 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1848 Verify that Onselecting the item from suggestion list, the respected products should be displayed in the page*/
	public void searchPageMisspellSuggestionSelection()
	{
		ChildCreation(" BRM - 1848 Verify that Onselecting the item from suggestion list, the respected products should be displayed in the page.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
			{
				Pass("The Search Misspell suggestion is displayed.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestionText))
				{
					Pass("The Misspell suggestion text is displayed.");
					String text = searchMisSpellSuggestionText.getText();
					log.add("The Actual result is : " + text);
					if(text.isEmpty())
					{
						Fail("The Misspelled suggestion text seems to be empty.",log);
					}
					else
					{
						Pass("The Misspelled suggestion text is displayed.",log);
					}
					
					if(BNBasicfeature.isListElementPresent(searchMisSpellSuggestionNavigationLink))
					{
						Pass("The Navigation Link text is displayed.");
						Random r = new Random();
						int sel = r.nextInt(searchMisSpellSuggestionNavigationLink.size());
						String suggText = searchMisSpellSuggestionNavigationLink.get(sel).getText();
						log.add("The Selected Suggestion Results are : " + suggText);
						searchMisSpellSuggestionNavigationLink.get(sel).click();
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
						String filterText = searchPageProductCountContainer.getText();
						log.add("The Filtered Suggestion Results are : " + filterText);
						if(filterText.contains(suggText))
						{
							Pass("The Selected Suggestion Results are displayed.",log);
						}
						else
						{
							Fail("There  is some mismatch in the Selected Suggestion Results and displayed results.",log);
						}
					}
					else
					{
						Fail("The Navigation Link text is not displayed.");
					}
				}
				else
				{
					Fail("The Misspell Suggestion text is not displayed.");
				}
			}
			else
			{
				Fail("The Search Misspell suggestion is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1848 Issue ." + e.getMessage());
			Exception(" BRM - 1848 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 891 Product details in grid/list view should be displayed with following details.*/
	/* BRM - 860 Verify that the Add to bag button is not displayed in the grid /list view.*/
	/* BRM - 851 Verify that the product image, product title, author name, format ,price and ratings are displayed in the list view.*/
	/* BRM - 854 Verify that the review count must be displayed below the stars in list view. */
	/* BRM - 859 Verify that the review count is not displayed in the grid view.*/
	/* BRM - 862 Verify that the regular price and the sale prices are displayed in both list and grid views.*/
	/* BRM - 856 Verify that on selecting the grid view icon the products are displayed in grid view ( two products in each row) as per the creative.*/
	/* BRM - 1865 Verify that product ratings and reviews are displayed for each product, as per the classic site*/
	/* BRM - 1849 Verify that the product image, product title, price must be displayed as per the creative in plp page.*/
	/* BRM - 1862 Verify that the product title and product image are displayed for each product.*/
	public void searchPageGridListDetails()
	{
		ChildCreation(" BRM - 891 Product details in grid/list view should be displayed with following details.");
		boolean brm851 = false;
		boolean brm854 = false;
		boolean brm859 = false;
		boolean brm860 = false;
		boolean brm862 = false;
		boolean brm856 = false;
		boolean brm1865 = false;
		boolean brm1849 = false;
		boolean brm1862 = false;
		try
		{
			int count = 0;
			do
			{
				if(BNBasicfeature.isElementPresent(gridViewList))
				{
					Pass("The Grid View list is displayed.");
					if(BNBasicfeature.isElementPresent(listViewIcon))
					{
						Pass("The List View icon is displayed.");
						jsclick(listViewIcon);
						wait.until(ExpectedConditions.visibilityOf(listViewList));
						//Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(listViewList))
						{
							Pass("The List View is loaded.");
						}
						else
						{
							Fail("The List view is not loaded.");
						}
						//Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductLists));
						if(BNBasicfeature.isListElementPresent(searchPageProductLists))
						{
							Pass("The Result Content is displayed.");
							List<List<String>> res = pdtNameDetails();
							List<List<Float>> pri = pdtPriceDetails();
							
							if(res.get(0).contains("NULL"))
							{
								Fail("The Product Name is displayed. But some Product Name is not displayed : " + res.get(0));
								brm1849 = false;
								brm1862 = false;
							}
							else
							{
								Pass("The Product Name is displayed. The displayed Product Name is : " + res.get(0));
								brm1849 = true;
								brm1862 = true;
							}
							
							if(res.get(1).contains("NULL"))
							{
								Fail("The Product Author Name is displayed. But some Product Author Name is not displayed : " + res.get(1));
							}
							else
							{
								Pass("The Product Author Name is displayed. The displayed Product Author Name is : " + res.get(1));
							}
							
							if(pri.get(0).contains(0))
							{
								Fail("The Product Sale Price is displayed. But some Product Sale Price is not displayed : " + pri.get(0));
								brm862 = false;
								brm1849 = false;
							}
							else
							{
								Pass("The Product Sale Price is displayed. The displayed Product Sale Price is : " + pri.get(0));
								brm862 = true;
								brm1849 = true;
							}
							
							Pass("The Product Regular Price is displayed. The displayed Product Regular Price is : " + pri.get(1));
							
							
							for(int i = 1;i<=searchPageProductLists.size();i++)
							{
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
								
								WebElement img = searchPageProductLists.get(i-1).findElement(By.xpath("//*[@class='skMob_productImgDiv']//img"));
								if(BNBasicfeature.isElementPresent(img))
								{
									Pass("The Image is displayed.");
									brm1862 = true;
								}
								else
								{
									Fail("The Image is not displayed.");
									brm1862 = false;
								}
								/*int resp = BNBasicfeature.imageBroken(img, log);
								if(resp==200)
								{
									Pass("The Image is not broken.");
									brm1862 = true;
								}
								else
								{
									Fail("The Image is broken.");
									brm1862 = false;
								}*/
								
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
								try
								{
									WebElement addtoBagBtn = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'addToBagBtn')]"));
									String prevUrl = driver.getCurrentUrl();
									addtoBagBtn.click();
									String url = driver.getCurrentUrl();
									if(prevUrl.equals(url))
									{
										Fail("The Add to Bag button was displayed in the Grid View Container for the Product.");
										brm860 = false;
									}
								}
								catch(Exception e1)
								{
									Pass("The Add to Bag button is not displayed for the Product.");
									brm860 = true;
								}
								
								WebElement reviewContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
								if(BNBasicfeature.isElementPresent(reviewContainer))
								{
									Pass("The review container is displayed.");
									try
									{
										WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
										String reviewCount = reviewCnt.getAttribute("title");
										if(reviewCount.isEmpty())
										{
											Fail("The review count is not displayed.");
											brm854 = false;
										}
										else
										{
											log.add("The displayed review count is : " + reviewCount);
											Pass("The review count is displayed.",log);
											brm854 = true;
										}
									}
									catch(Exception e)
									{
										Pass("No Rating has been displayed for the products.");
										brm854 = true;
									}
									
									try
									{
										WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
										String totalreviewContainer = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'ratingContainer')]//*[contains(@class,'RatingCount')]")).getText();
										String reviewCount = reviewCnt.getAttribute("title");
										if(reviewCount.isEmpty())
										{
											Fail("The review count is not displayed.");
											brm854 = false;
											brm1865 = false;
										}
										else
										{
											log.add("The displayed review count is : " + reviewCount);
											Pass("The review count is displayed.",log);
											brm854 = true;
											brm1865 = true;
										}
										
										if(totalreviewContainer.isEmpty())
										{
											System.out.println("In List View Section.");
											Fail("The Total Review for the product is not displayed / empty.");
											brm854 = false;
											brm1865 = false;
										}
										else
										{
											Pass("The Total Review for the product is displayed. The displayed review count is : " + totalreviewContainer);
											brm854 = true;
											brm1865 = true;
										}
									}
									catch(Exception e)
									{
										Pass("No Rating has been displayed for the products.");
										brm854 = true;
										brm1865 = true;
									}
								}
								else
								{
									Fail("The review container is not displayed.");
									brm854 = false;
									brm1865 = false;
								}
							}
						}
						else
						{
							Fail("The Result Content is not displayed.");
							brm854 = false;
						}
					}
					else
					{
						Fail("The List view icon is not displayed.");
						brm854 = false;
					}
				}
				else if(BNBasicfeature.isElementPresent(listViewList))
				{
					Pass("The List View list is displayed.");
					if(BNBasicfeature.isElementPresent(gridViewIcon))
					{
						Pass("The Grid View icon is displayed.");
						jsclick(gridViewIcon);
						wait.until(ExpectedConditions.visibilityOf(gridViewList));
						//Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							Pass("The Grid View is loaded.");
							brm851 = true;
						}
						else
						{
							Fail("The Grid view is not loaded.");
							brm851 = false;
						}
						wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductLists));
						Thread.sleep(100);
						if(BNBasicfeature.isListElementPresent(searchPageProductLists))
						{
							Pass("The Result Content is displayed.");
							Pass("The Result Content is displayed.");
							List<List<String>> res = pdtNameDetails();
							List<List<Float>> pri = pdtPriceDetails();
							
							if(res.get(0).contains("NULL"))
							{
								Fail("The Product Name is displayed. But some Product Name is not displayed : " + res.get(0));
								brm851 = false;
								brm856 = false;
							}
							else
							{
								Pass("The Product Name is displayed. The displayed Product Name is : " + res.get(0));
								brm851 = true;
								brm856 = true;
							}
							
							if(res.get(1).contains("NULL"))
							{
								Fail("The Product Author Name is displayed. But some Product Author Name is not displayed : " + res.get(1));
								brm851 = false;
								brm856 = false;
							}
							else
							{
								Pass("The Product Author Name is displayed. The displayed Product Author Name is : " + res.get(1));
								brm851 = true;
								brm856 = true;
							}
							
							if(pri.get(0).contains(0))
							{
								Fail("The Product Sale Price is displayed. But some Product Sale Price is not displayed : " + pri.get(0));
								brm851 = false;
								brm862 = false;
								brm856 = false;
							}
							else
							{
								Pass("The Product Sale Price is displayed. The displayed Product Sale Price is : " + pri.get(0));
								brm851 = true;
								brm862 = true;
								brm856 = true;
							}
							
							Pass("The Product Regular Price is displayed. The displayed Product Regular Price is : " + pri.get(1));
							
							for(int i = 1;i<=searchPageProductLists.size();i++)
							{
								wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
								WebElement img = searchPageProductLists.get(i-1).findElement(By.xpath("//*[@class='skMob_productImgDiv']//img"));
								if(BNBasicfeature.isElementPresent(img))
								{
									Pass("The Image is displayed.");
								}
								else
								{
									Fail("The Image is not displayed.");
								}
								/*int resp = BNBasicfeature.imageBroken(img, log);
								if(resp==200)
								{
									Pass("The Image is not broken.");
								}
								else
								{
									Fail("The Image is broken.");
								}*/
								
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
								try
								{
									WebElement addtoBagBtn = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'addToBagBtn')]"));
									String prevUrl = driver.getCurrentUrl();
									addtoBagBtn.click();
									//Thread.sleep(1000);
									String url = driver.getCurrentUrl();
									if(prevUrl.equals(url))
									{
										Fail("The Total is displayed in the Grid View Container for the Product.");
										brm860 = false;
									}
								}
								catch(Exception e1)
								{
									Pass("The Add to Bag button is not displayed for the Product.");
									brm860 = true;
								}

								WebElement reviewContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
								if(BNBasicfeature.isElementPresent(reviewContainer))
								{
									Pass("The review container is displayed.");
									try
									{
										WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
										String reviewCount = reviewCnt.getAttribute("title");
										if(reviewCount.isEmpty())
										{
											Fail("The review count is not displayed.");
											brm851 = false;
											brm856 = false;
										}
										else
										{
											log.add("The displayed review count is : " + reviewCount);
											Pass("The review count is displayed.",log);
											brm851 = true;
											brm856 = true;
										}
									}
									catch(Exception e)
									{
										Pass("No Rating has been displayed for the products.");
										brm851 = true;
										brm856 = true;
									}
									
									BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
									try
									{
										WebElement totalreviewContainer = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'ratingContainer')]"));
										String prevUrl = driver.getCurrentUrl();
										totalreviewContainer.click();
										//Thread.sleep(1000);
										String url = driver.getCurrentUrl();
										if(prevUrl.equals(url))
										{
											Fail("The Total is displayed in the Grid View Container for the Product.");
											brm859 = false;
										}
									}
									catch(Exception e1)
									{
										Pass("The Review is not displayed for the Product when viewed in the Grid Section.");
										brm859 = true;
									}
								}
								else
								{
									Fail("The review container is not displayed.");
									brm851 = false;
									brm856 = false;
								}
							}
						}
						else
						{
							Fail("The Result Content is not displayed.");
							brm851 = false;
							brm856 = false;
						}
					}
					else
					{
						Fail("The Grid View Icon is not displayed.");
						brm851 = false;
						brm856 = false;
					}
				}
				else
				{
					Fail("Both the Grid View and List View Icon is not displayed.");
					brm851 = false;
					brm856 = false;
				}
				count++;
			}while(count<2);
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 891 Issue ." + e.getMessage());
			Exception(" BRM - 891 Issue." + e.getMessage());
		}
		
		/* BRM - 851 Verify that the product image, product title, author name, format ,price and ratings are displayed in the list view.*/
		ChildCreation(" BRM - 851 Verify that the product image, product title, author name, format ,price and ratings are displayed in the list view.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm851==true)
			{
				Pass( " The product image, product title, author name, format ,price and ratings are displayed in the list view.",log);
			}
			else
			{
				Fail( " The product image, product title, author name, format ,price and ratings are not displayed in the list view.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 851 Issue ." + e.getMessage());
			Exception(" BRM - 851 Issue." + e.getMessage());
		}
		
		/* BRM - 854 Verify that the review count must be displayed below the stars in list view. */
		ChildCreation(" BRM - 854 Verify that the review count must be displayed below the stars in list view.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm854==true)
			{
				Pass( " The review count is displayed below the stars in list view.",log);
			}
			else
			{
				Fail( " The review count is not displayed below the stars in list view.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 854 Issue ." + e.getMessage());
			Exception(" BRM - 854 Issue." + e.getMessage());
		}
		
		/* BRM - 859 Verify that the review count is not displayed in the grid view.*/
		ChildCreation(" BRM - 859 Verify that the review count is not displayed in the grid view.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm859==true)
			{
				Pass( " The review count is not displayed below the stars in grid view.",log);
			}
			else
			{
				Fail( " The review count is displayed below the stars in grid view.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 859 Issue ." + e.getMessage());
			Exception(" BRM - 859 Issue." + e.getMessage());
		}
		
		/* BRM - 860 Verify that the Add to bag button is not displayed in the grid /list view.*/
		ChildCreation(" BRM - 860 Verify that the Add to bag button is not displayed in the grid /list view..");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm860==true)
			{
				Pass( " The review count is not displayed below the stars in grid view.",log);
			}
			else
			{
				Fail( " The review count is displayed below the stars in grid view.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 860 Issue ." + e.getMessage());
			Exception(" BRM - 860 Issue." + e.getMessage());
		}
		
		/* BRM - 862 Verify that the regular price and the sale prices are displayed in both list and grid views.*/
		ChildCreation(" BRM - 862 Verify that the regular price and the sale prices are displayed in both list and grid views.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm862==true)
			{
				Pass( " The regular price and the sale prices are displayed in both list and grid views.",log);
			}
			else
			{
				Fail( " There is some issue in the regular price and the sale prices when viewed in the list and grid views.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 862 Issue ." + e.getMessage());
			Exception(" BRM - 862 Issue." + e.getMessage());
		}
		
		/* BRM - 856 Verify that on selecting the grid view icon the products are displayed in grid view ( two products in each row) as per the creative.*/
		ChildCreation(" BRM - 856 Verify that on selecting the grid view icon the products are displayed in grid view ( two products in each row) as per the creative.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm856==true)
			{
				Pass( " The products are listed in grid views as expected.",log);
			}
			else
			{
				Fail( " There is some issue in the listed products when viewed in grid view .",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 856 Issue ." + e.getMessage());
			Exception(" BRM - 856 Issue." + e.getMessage());
		}
		
		/* BRM - 1865 Verify that product ratings and reviews are displayed for each product, as per the classic site*/
		ChildCreation(" BRM - 1865 Verify that product ratings and reviews are displayed for each product, as per the classic site.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm1865==true)
			{
				Pass("The Total Review for the product is displayed.");
			}
			else
			{
				Fail("The Total Review for the product is not displayed / empty.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1865 Issue ." + e.getMessage());
			Exception(" BRM - 1865 Issue." + e.getMessage());
		}
		
		/* BRM - 1849 Verify that the product image, product title, price must be displayed as per the creative in plp page.*/
		ChildCreation(" BRM - 1849 Verify that the product image, product title, price must be displayed as per the creative in plp page.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm1849==true)
			{
				Pass(" The Product Title, Image and Price is displayed.",log);
			}
			else
			{
				Fail("There is some issue in displaying the Product Name, Image or Price.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1849 Issue ." + e.getMessage());
			Exception(" BRM - 1849 Issue." + e.getMessage());
		}
		
		/* BRM - 1862 Verify that the product title and product image are displayed for each product.*/
		ChildCreation(" BRM - 1862 Verify that the product title and product image are displayed for each product.");
		try
		{
			log.add("REFER BRM - 891 for the detailed report.");
			if(brm1862==true)
			{
				Pass(" The Product Title, Image is displayed.",log);
			}
			else
			{
				Fail("There is some issue in displaying the Product Name, Image.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1862 Issue ." + e.getMessage());
			Exception(" BRM - 1862 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 876 Verify that the On selecting the product image or product title the pdp page must be displayed.*/
	/* BRM - 1852 Verify that the On selecting the product image or product title the pdp page must be displayed.*/
	/* BRM - 1854 Verify that on selecting product title, it should open the PDP page with product details.*/
	/* BRM - 1870 Verify that when the product is selected, the corresponding pdp is displayed.*/
	public void searchpdpPageNavigation()
	{
		ChildCreation("BRM - 1852 Verify that the On selecting the product image or product title the pdp page must be displayed.");
		boolean pdpLoaded = false;
		try
		{
			boolean atbfound = false;
			String selprdtname = "",selPrdtid = "";
			String currUrl = driver.getCurrentUrl();
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int j = 1; j<=searchPageProductLists.size();j++)
				{
					Thread.sleep(100);
					int count=1;
					atbfound = false;
					do
					{
						Random r = new Random();
						int sel = r.nextInt(searchPageProductLists.size());
						if(sel<1)
						{
							sel = 1;
						}
						
						BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
						WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
						selprdtname = prdtlist.getText();
						log.add("The Selected Product Name is  : " + selprdtname);
						log.add("The Selected Product Name is : " + selprdtname);
						selPrdtid = prdtlist.getAttribute("identifier").toString();
						log.add("The Selected Product ID is  : " + selPrdtid);
						log.add("Selected Product ID : " + selPrdtid);
						atbfound = false;
						jsclick(prdtlist);
						wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
						
						if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
						{
							atbfound = true;
							break;
						}
						else
						{
							count++;
							driver.get(currUrl);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							continue;
						}
					}while(atbfound==true||count<10);
				}
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
						
			if(atbfound==true)
			{
				if(BNBasicfeature.isElementPresent(productId))
				{
					String id = productId.getAttribute("identifier").toString();
					log.add("The Actual Product ID is  : " + selPrdtid);
					if(id.equals(selPrdtid))
					{
						Pass("The User is navigated to the expected page.");
						pdpLoaded = true;
					}
					else
					{
						Fail("The User is not navigated to the expected page.");
						pdpLoaded = false;
					}
				}
				else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					boolean idFound = false;
					for(int i = 0;i<pdpPageATBBtns.size();i++)
					{
						String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							idFound = true;
							break;
						}
						else
						{
							idFound = false;
							continue;
						}
					}
						
					if(idFound==true)
					{
						pdpLoaded = true;
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						pdpLoaded = false;
						Fail("The User is not redirected to the user intented page.",log);
					}
				}
				else
				{
					Fail("The product id is not displayed in the Product Page.");
				}
			}
			else
			{
				Fail("The Add to back button is not displayed in the pdpPage.");
			}
			
			driver.get(currUrl);
			//Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1852 Issue ." + e.getMessage());
			Exception(" BRM - 1852 Issue." + e.getMessage());
		}
		
		/* BRM - 876 Verify that the On selecting the product image or product title the pdp page must be displayed.*/
		ChildCreation(" BRM - 876 Verify that the On selecting the product image or product title the pdp page must be displayed.");
		if(pdpLoaded==true)
		{
			Pass("The User is redirected to the user intented page.",log);	
		}
		else
		{
			Fail("The User is not redirected to the user intented page.",log);
		}
		
		/* BRM - 1854 Verify that on selecting product title, it should open the PDP page with product details.*/
		ChildCreation(" BRM - 1854 Verify that on selecting product title, it should open the PDP page with product details.");
		if(pdpLoaded==true)
		{
			Pass("The User is redirected to the user intented page.",log);	
		}
		else
		{
			Fail("The User is not redirected to the user intented page.",log);
		}
		
		/* BRM - 1870 Verify that when the product is selected, the corresponding pdp is displayed.*/
		ChildCreation(" BRM - 1870 Verify that when the product is selected, the corresponding pdp is displayed.");
		if(pdpLoaded==true)
		{
			Pass("The User is redirected to the user intented page.",log);	
		}
		else
		{
			Fail("The User is not redirected to the user intented page.",log);
		}
	}
	
	/* BRM - 883 Make sure that the List view/Grid views are working.*/
	public void searchPageViewSelection()
	{
		ChildCreation("BRM - 883 Make sure that the List view/Grid views are working.");
		try
		{
			int count = 0;
			do
			{
				if(BNBasicfeature.isElementPresent(gridViewList))
				{
					Pass("The Grid View list is displayed.");
					Thread.sleep(100);
					wait.until(ExpectedConditions.elementToBeClickable(listViewIcon));
					if(BNBasicfeature.isElementPresent(listViewIcon))
					{
						Pass("The List View icon is displayed.");
						wait.until(ExpectedConditions.elementToBeClickable(listViewIcon));
						Thread.sleep(200);
						jsclick(listViewIcon);
						Thread.sleep(100);
						wait.until(ExpectedConditions.visibilityOf(listViewList));
						if(BNBasicfeature.isElementPresent(listViewList))
						{
							Pass("The List View is loaded.");
						}
						else
						{
							Fail("The List view is not loaded.");
						}
						//Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductLists));
						if(BNBasicfeature.isListElementPresent(searchPageProductLists))
						{
							Pass("The Result Content is displayed.");
							for(int i = 1;i<=searchPageProductLists.size();i++)
							{
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
							}
							
							for(int i = searchPageProductLists.size();i>=1;i--)
							{
								BNBasicfeature.scrollup(searchPageProductLists.get(i-1), driver);
							}
						}
						else
						{
							Fail("The Result Content is not displayed.");
						}
					}
					else
					{
						Fail("The List view icon is not displayed.");
					}
				}
				else if(BNBasicfeature.isElementPresent(listViewList))
				{
					Pass("The List View list is displayed.");
					wait.until(ExpectedConditions.elementToBeClickable(gridViewIcon));
					if(BNBasicfeature.isElementPresent(gridViewIcon))
					{
						Pass("The Grid View icon is displayed.");
						wait.until(ExpectedConditions.elementToBeClickable(gridViewIcon));
						Thread.sleep(200);
						jsclick(gridViewIcon);
						wait.until(ExpectedConditions.visibilityOf(gridViewList));
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							Pass("The Grid View is loaded.");
						}
						else
						{
							Fail("The Grid view is not loaded.");
						}
						//Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOfAllElements(searchPageProductLists));
						if(BNBasicfeature.isListElementPresent(searchPageProductLists))
						{
							Pass("The Result Content is displayed.");
							for(int i = 1;i<=searchPageProductLists.size();i++)
							{
								BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
							}
							
							for(int i = searchPageProductLists.size();i>=1;i--)
							{
								BNBasicfeature.scrollup(searchPageProductLists.get(i-1), driver);
							}
						}
						else
						{
							Fail("The Result Content is not displayed.");
						}
					}
					else
					{
						Fail("The Grid View Icon is not displayed.");
					}
				}
				else
				{
					Fail("Both the Grid View and List View Icon is not displayed.");
				}
				count++;
			}while(count<2);
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				Pass("The Grid View list is displayed.");
				wait.until(ExpectedConditions.elementToBeClickable(listViewIcon));
				if(BNBasicfeature.isElementPresent(listViewIcon))
				{
					Pass("The List View icon is displayed.");
					Thread.sleep(100);
					wait.until(ExpectedConditions.elementToBeClickable(listViewIcon));
					Thread.sleep(200);
					jsclick(listViewIcon);
					wait.until(ExpectedConditions.visibilityOf(listViewList));
					//Thread.sleep(500);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 883 Issue ." + e.getMessage());
			Exception(" BRM - 883 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1878 Verify that on selecting the "Oldest to Newest" sort option, the old products are displayed first in the list.*/
	/* BRM - 1879 Verify that on selecting the Newest to Oldest sort option, the newly arrived products are displayed first in the list. */
	/* BRM - 1880 Verify that on selecting the Price High-Low option, the products with the high price are displayed first in the list, followed by the products with the low price. */
	/* BRM - 1881 Verify that on selecting the Price Low-High option, the products with the low price are displayed first followed by the products with the high price. */
	/* BRM - 1885 Verify that on selecting the Highly Rated option, the top rated products are displayed first in the list. */
	/* BRM - 1886 Verify that on selecting the A-Z option, the products are displayed in the ascending order. */
	/* BRM - 1887 Verify that on selecting the Z-A option, the products are displayed in the descending order. */
	
	public void searchPageSorting(String tc) throws Exception
	{
		String tcId = "";
		ArrayList<String> originalList = new ArrayList<>();
		ArrayList<String> originalPrdtName = new ArrayList<>();
		ArrayList<String> modifiedPrdtName = new ArrayList<>();
		ArrayList<Float> originalPrice = new ArrayList<>();
		ArrayList<Float> originalRating = new ArrayList<>();
		ArrayList<Float> modifiedRating = new ArrayList<>();
		ArrayList<Float> modifiedPrice = new ArrayList<>();
		ArrayList<String> modifiedList = new ArrayList<>();
		boolean brm885 = false;
		String cellVal = "";
		String[] Val = null;
		String selSortList = "";
		if(tc=="BRM-1878")
		{
			tcId= "BRM - 1878 Verify that on selecting the Oldest to Newest sort option, the old products are displayed first in the list.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[0];
		}
		else if(tc=="BRM-1879")
		{
			tcId= "BRM - 1879 Verify that on selecting the Newest to Oldest sort option, the newly arrived products are displayed first in the list.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[1];
		}
		else if(tc=="BRM-1880")
		{
			tcId= " BRM - 1880 Verify that on selecting the Price High-Low option, the products with the high price are displayed first in the list, followed by the products with the low price..";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[2];
		}
		else if(tc=="BRM-1881")
		{
			tcId= " BRM - 1881 Verify that on selecting the Price Low-High option, the products with the low price are displayed first followed by the products with the high price.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[3];
		}
		else if(tc=="BRM-1885")
		{
			tcId= " BRM - 1885 Verify that on selecting the Highly Rated option, the top rated products are displayed first in the list.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[4];
		}
		else if(tc=="BRM-1886")
		{
			tcId= " BRM - 1886 Verify that on selecting the A-Z option, the products are displayed in the ascending order.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[5];
		}
		else
		{
			tcId= " BRM - 1887 Verify that on selecting the Z-A option, the products are displayed in the descending order.";
			cellVal = BNBasicfeature.getExcelVal("BRM1878", sheet, 6);
			Val = cellVal.split("\n");
			selSortList = Val[6];
		}
			
		ChildCreation(tcId);
		try
		{
			originalList = getprdtId();
			if((tc=="BRM-1880")||(tc=="BRM-1881"))
			{
				originalPrice = getPrdSalePrice();
				log.add("The original price before changing the price is : " + originalPrice);
			}
			
			if(tc=="BRM-1885")
			{
				originalRating = getPrdRating();
				log.add("The Original Product Rating list was : " + originalRating);
			}
			
			if((tc=="BRM-1886")||(tc=="BRM-1887"))
			{
				originalPrdtName = getPrdTitle();
				log.add("The Original Product Title list was : " + originalPrdtName);
			}
			
			log.add("The Original Product id list was : " + originalList);
			log.add("The Sort Option Selected was  : " + selSortList);
			boolean srtSelect = sortSelection(selSortList);
			if(srtSelect==true)
			{
				modifiedList = getprdtId();
				log.add("The Modified Product id list is : " + modifiedList);
				if(modifiedList.equals(originalList))
				{
					Fail("The Modified Product List and the Original Product List are same.",log);
					brm885 = false;
				}
				else
				{
					Pass("The Modified Product List and the Original Product List are not same.",log);
					brm885 = true;
				}
				
				if((tc=="BRM-1880")||(tc=="BRM-1881"))
				{
					modifiedPrice = getPrdSalePrice();
					//Thread.sleep(1000);
					log.add("The modified price after changing the price is : " + modifiedPrice);
					if(modifiedPrice.equals(originalPrice))
					{
						Fail("The Modified Product List and the Original Product List are same.",log);
						brm885 = false;
					}
					else
					{
						Pass("The Modified Product List and the Original Product List are not same.",log);
						brm885 = true;
					}
				}
				
				if((tc=="BRM-1885"))
				{
					modifiedRating = getPrdRating();
					//Thread.sleep(1000);
					log.add("The modified Rating after Setting the " + selSortList + " is : " + modifiedRating);
					if(originalRating.equals(modifiedRating))
					{
						Fail("The Modified Product List and the Original Product List are same.",log);
						brm885 = false;
					}
					else
					{
						Pass("The Modified Product List and the Original Product List are not same.",log);
						brm885 = true;
					}
				}
				
				if((tc=="BRM-1886")||(tc=="BRM-1887"))
				{
					modifiedPrdtName = getPrdTitle();
					//Thread.sleep(1000);
					log.add("The modified Title after Setting the Sort " + selSortList + "  is : " + modifiedPrdtName);
					if(originalPrdtName.equals(modifiedPrdtName))
					{
						Fail("The Modified Product List and the Original Product List are same.",log);
						brm885 = false;
					}
					else
					{
						Pass("The Modified Product List and the Original Product List are not same.",log);
						brm885 = true;
					}
				}
			}
			else
			{
				Fail("The User was not able to select the desired sort.");
				brm885 = false;
			}
		}
		catch(Exception e)
		{
			System.out.println(tcId + "  Issue ." + e.getMessage());
			Exception(tcId  + "  Issue." + e.getMessage());
		}
		
		if(tcId=="BRM-1887")
		{
			ChildCreation(" BRM - 885 User can able to sort the products with the types of  Top Matches, Best Sellers, Newest to Oldest, Oldest to Newest, Highly Rated Price,Low to High Price,High to Low Title,A to Z Title,Z to A.");
			try
			{
				if(brm885==true)
				{
					Pass(" The Sort Function works as expected.");
				}
				else
				{
					Fail("There is some issue in the Sort Function.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1887 Issue ." + e.getMessage());
				Exception(" BRM - 1887 Issue." + e.getMessage());
			}
		}
	}
	
	/* BRM - 858 Verify that on changing the list/grid views, an animation should not played, then the view is changed.*/
	public void searchViewChange()
	{
		ChildCreation(" BRM - 858 Verify that on changing the list/grid views, an animation should not played, then the view is changed.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				//BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				Pass("The Grid View list is displayed.");
				if(BNBasicfeature.isElementPresent(listViewIcon))
				{
					BNBasicfeature.scrollup(header, driver);
					Pass("The List View icon is displayed.");
					jsclick(listViewIcon);
					Thread.sleep(50);
					wait.until(ExpectedConditions.visibilityOf(listViewList));
					if(BNBasicfeature.isElementPresent(listViewList))
					{
						Pass("The List View is loaded.");
					}
					else
					{
						Fail("The List view is not loaded.");
					}
					if(BNBasicfeature.isElementPresent(gridViewIcon))
					{
						Pass("The Grid View Icon is displayed.");
						jsclick(gridViewIcon);
						Thread.sleep(50);
						wait.until(ExpectedConditions.visibilityOf(gridViewList));
						if(BNBasicfeature.isElementPresent(gridViewList))
						{
							Pass("The Grid View is loaded.");
						}
						else
						{
							Fail("The Grid view is not loaded.");
						}
					}
					else
					{
						Fail("The Grid View icon is not displayed.");
					}
				}
				else
				{
					Fail("The List view icon is not displayed.");
				}
			}
			else if(BNBasicfeature.isElementPresent(listViewList))
			{
				Pass("The List View list is displayed.");
				if(BNBasicfeature.isElementPresent(gridViewIcon))
				{
					Pass("The Grid View icon is displayed.");
					jsclick(gridViewIcon);
					Thread.sleep(50);
					wait.until(ExpectedConditions.visibilityOf(gridViewList));
					if(BNBasicfeature.isElementPresent(gridViewList))
					{
						Pass("The Grid View is loaded.");
					}
					else
					{
						Fail("The Grid view is not loaded.");
					}
					
					if(BNBasicfeature.isElementPresent(listViewIcon))
					{
						Pass("The List View Icon is displayed.");
						jsclick(listViewIcon);
						Thread.sleep(50);
						wait.until(ExpectedConditions.visibilityOf(listViewList));
						if(BNBasicfeature.isElementPresent(listViewList))
						{
							Pass("The List View is loaded.");
						}
						else
						{
							Fail("The List view is not loaded.");
						}
					}
					else
					{
						Fail("The List View icon is not displayed.");
					}
				}
				else
				{
					Fail("The Grid View Icon is not displayed.");
				}
			}
			else
			{
				Fail("Both the Grid View and List View Icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 858 Issue ." + e.getMessage());
			Exception(" BRM - 858 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 892 In List view, the Regular price should be striked out and saved % should be shown like classic site.*/
	/* BRM - 893 Verify that In List view, the Regular price should be striked out and saved % should be shown like classic site.*/
	/* BRM - 1866 Verify that if the product has multiple price, the regular and the sale price are displayed.*/
	public void searchSavePriceDisplayListView(String tc)
	{
		String tcId = "";
		boolean brm893 = false;
		if(tc=="BRM-892")
		{
			tcId = "BRM - 892 In List view, the Regular price should be striked out and saved % should be shown like classic site.";
		}
		else
		{
			tcId = " BRM - 1866 Verify that if the product has multiple price, the regular and the sale price are displayed.";
		}
		ChildCreation(tcId);
		try
		{
			ArrayList<String> prdtName = new ArrayList<>();
			//ArrayList<String> prdtAuthorName = new ArrayList<>();
			ArrayList<Float> prdtSalePrice = new ArrayList<>();
			ArrayList<Float> prdtRegPrice = new ArrayList<>();
			
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				BNBasicfeature.scrollup(header, driver);
				//Thread.sleep(1000);
				jsclick(listViewIcon);
				Thread.sleep(50);
				wait.until(ExpectedConditions.visibilityOf(listViewList));
				Thread.sleep(100);
			}
			
			
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				Pass("The Search Result Content is displayed.");
				
				prdtName = getPrdTitle();
				if(prdtName.contains("NULL"))
				{
					Fail("The Product Name is displayed. But some Product Name is not displayed : " + prdtName);
					brm893 = false;
				}
				else
				{
					Pass("The Product Name is displayed. The displayed Product Name is : " + prdtName);
					brm893 = true;
				}
				
				//prdtAuthorName = getPrdAuthor();
				/*if(prdtAuthorName.contains("NULL"))
				{
					Fail("The Product Author Name is displayed. But some Product Author Name is not displayed : " + prdtAuthorName);
				}
				else
				{
					Pass("The Product Author Name is displayed. The displayed Product Author Name is : " + prdtAuthorName);
				}*/
				
				if(tcId.contains("BRM - 1866"))
				{
					prdtSalePrice = getPrdSalePrice();
					if(prdtSalePrice.contains(0))
					{
						Fail("The Product Sale Price is displayed. But some Product Sale Price is not displayed : " + prdtSalePrice);
					}
					else
					{
						Pass("The Product Sale Price is displayed. The displayed Product Sale Price is : " + prdtSalePrice);
					}
				}
				
				prdtRegPrice = getPrdRegPrice();
				Pass("The Product Regular Price is displayed. The displayed Product Regular Price is : " + prdtRegPrice);
				
				if(tcId.contains("BRM - 892")||(tcId.contains("BRM - 893")))
				{
					for(int i = 1; i<=searchPageProductLists.size();i++)
					{
						Thread.sleep(25);
						wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
						BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
						WebElement priceContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'PriceContainer')]"));
						if(BNBasicfeature.isElementPresent(priceContainer))
						{
							Pass("The Price container is displayed.");
							List<WebElement> prdtType = driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'priceContainer')]"));
							int size = prdtType.size();
							if(size==1)
							{
								size=2;
							}
							for(int j = 1;j<=size-1;j++)
							{
								String priceLabel =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listpricelabel')]")).get(j-1).getText();
								if(priceLabel.isEmpty())
								{
									Fail("The price label is not displayed.");
									brm893 = false;
								}
								else
								{
									log.add("The displayed price label is : " + priceLabel);
									Pass("The price label is displayed.");
									if(priceLabel.contains("Market"))
									{
										continue;
									}
									else
									{
										try
										{
											WebElement sveDiscount =  driver.findElements(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'listDisccount')]")).get(j-1);
											sveDiscount.click();
											String salePrice = sveDiscount.getText();
											//System.out.println(salePrice);
											Float sPrice = Float.parseFloat(salePrice.replaceAll("[^\\d.]", ""));
											if(sPrice>0)
											{
												Pass("The Save Discount is displayed. The displayed Save Discount is : " + sPrice);
												brm893 = true;
											}
											else
											{
												Fail("The Save Discount is not displayed. The displayed Save Discount is : " + sPrice);
												brm893 = false;
											}
										}
										catch(Exception e)
										{
											Skip("The Reg Price is not displayed for the product.");
											brm893 = true;
										}
									}
								}
							}
						}
						else
						{
							Fail("The Price container is not displayed.");
							brm893 = false;
						}
					}
				}
			}
			else
			{
				Fail("The Search Result Content is not displayed.");
				brm893 = false;
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 892 Issue ." + e.getMessage());
			Exception(" BRM - 892 Issue." + e.getMessage());
		}
		
		if(tcId=="BRM-892")
		{
			/* BRM - 893 Verify that In List view, the Regular price should be striked out and saved % should be shown like classic site.*/
			ChildCreation(" BRM - 893 Verify that In List view, the Regular price should be striked out and saved % should be shown like classic site.");
			try
			{
				log.add(" Refer BRM - 892 for detailed report.");
				if(brm893==true)
				{
					Pass(" In List view, the Regular price should be striked out and saved % should be shown like classic site and it is displayed.",log);
				}
				else
				{
					Fail(" In List view, the Regular price should be striked out and saved % should be shown like classic site and it is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 893 Issue ." + e.getMessage());
				Exception(" BRM - 893 Issue." + e.getMessage());
			}
		}
	}
	
	/* BRM - 855 Verify that when there are no reviews available, review option should not be shown.*/
	public void searchNoReviewCount()
	{
		ChildCreation(" BRM - 855 Verify that when there are no reviews available, review option should not be shown.");
		try
		{
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				Pass("The Search Result Content is displayed.");
				for(int i = 1; i<=searchPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(searchPageProductLists.get(i-1)));
					BNBasicfeature.scrolldown(searchPageProductLists.get(i-1), driver);
					if(BNBasicfeature.isElementPresent(searchResultListProductName.get(i-1)))
					{
						String prdtname = searchResultListProductName.get(i-1).getText();
						//System.out.println(prdtname);
						if(prdtname.isEmpty())
						{
							Fail("The Product Name is not displayed.");
						}
						else
						{
							log.add("The displayed product name is  : " + prdtname);
							Pass("The Product Name is displayed.",log);
						}
					}
					else
					{
						Fail("The Product Name is not displayed.");
					}
					
					WebElement reviewContainer = searchPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
					if(BNBasicfeature.isElementPresent(reviewContainer))
					{
						Pass("The review container is displayed.");
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							String reviewCount = reviewCnt.getAttribute("title");
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
							}
						}
						catch(Exception e)
						{
							Pass(" No Rating has been displayed for the products.");
							try
							{
								WebElement totalreviewContainer = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'ratingContainer')]"));
								if(BNBasicfeature.isElementPresent(totalreviewContainer))
								{
									Fail("The Total review Container is displayed for the product which has NO REVIEW.");
								}
							}
							catch(Exception e1)
							{
								Pass("The Review is not displayed for the Product which has no REVIEW.");
							}
						}
					}
					else
					{
						Fail("The review container is not displayed.");
					}
				}
			}
			else
			{
				Fail("The Search result page is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 855 Issue ." + e.getMessage());
			Exception(" BRM - 855 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 872 Verify that if more number of filter options shown in filter popup, user should allow to scroll the popup to view all the options.*/
	public void searchFilterOption()
	{
		ChildCreation(" BRM - 872 Verify that if more number of filter options shown in filter popup, user should allow to scroll the popup to view all the options.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterButton))
			{
				Pass("The Filter button is displayed.");
				BNBasicfeature.scrollup(searchPageFilterButton, driver);
				//Thread.sleep(500);
				boolean enabled = false;
				WebDriverWait w1 = new WebDriverWait(driver, 1);
				do
				{
					try
					{
						jsclick(searchPageFilterButton);
						w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
						enabled = true;
					}
					catch(Exception e)
					{
						enabled = false;
						enabled = searchPageFilterFacets.getAttribute("style").contains("block");
					}
				}while(enabled==false);
				/*jsclick(searchPageFilterButton);*/
				wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
				Thread.sleep(100);
				boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
				if(facetOpen==true)
				{
					if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
					{
						Pass("The Filter Facets is displayed.");
						if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
						{
							Random r = new Random();
							int sel = r.nextInt(searchPageFilterFacetsLists.size());
							Pass("The Facets List is displayed.");
							jsclick(searchPageFilterFacetsLists.get(sel));
							//searchPageFilterFacetsLists.get(sel).click();
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
							{
								wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(sel), "style", "block"));
								Thread.sleep(100);
								boolean facetVal = searchPageFilterFacetsValue.get(sel).getAttribute("style").contains("block");
								if(facetVal==true)
								{
									List<WebElement> filterFaceVal = driver.findElements(By.xpath("(//*[contains(@class,'skMob_facetOptions')]//*[@class='skMob_plpFacetValueCont'])["+(sel+1)+"]//*[@class='skMob_plpFacetValue']"));
									for(int j= 1;j<=filterFaceVal.size();j++)
									{
										BNBasicfeature.scrolldown(filterFaceVal.get(j-1), driver);
										String text = filterFaceVal.get(j-1).getText();
										if(text.isEmpty())
										{
											Fail("The Facets is not Scrolled and the value is not fetched.");
										}
										else
										{
											Pass(" The Facets was Scrolled and the value is fetched. The facet value is : " + text);
										}
									}
									facetVal = false;
									BNBasicfeature.scrollup(header, driver);
									Thread.sleep(100);
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
									{
										for(int i = 0; i<searchPageFilterFacetsNameExpanded.size();i++)
										{
											jsclick(searchPageFilterFacetsNameExpanded.get(i));
											//searchPageExpandedFilterFacetsLists.get(i).click();
											wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(sel), "style", "none"));
											Thread.sleep(100);
											facetVal = searchPageFilterFacetsValue.get(sel).getAttribute("style").contains("none");
											if(facetVal==true)
											{
												Pass("The Opened Facet was closed.");
											}
											else
											{
												Fail("The Opened Facet was not closed.");
											}
											i--;
										}
									}
									else
									{
										Fail("The Expanded Facets list is not displayed.");
									}
								}
								else
								{
									Fail("The Facet Val was not opened.");
								}
							}
							else
							{
								Fail("The Facets Value is not displayed.");
							}
						}
						else
						{
							Fail("The Facets List is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not displayed.");
					}
				}
				else
				{
					Fail("The Filter Facets is not opened.");
				}
			}
			else
			{
				Fail("The Filter button is not displayed.");
			}

			/*if(BNBasicfeature.isElementPresent(searchPagePopUpMask))
			{
				searchPagePopUpMask.click();
				Thread.sleep(1000);
			}*/
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 872 Issue ." + e.getMessage());
			Exception(" BRM - 872 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 925 Verify that the filter dropdown background color is matched with the creative.*/
	/* "BRM - 1896 Verify that Filters should be in dropdown as per the creative." */
	public void searchFilterBackground()
	{
		boolean brm1896 = false;
		ChildCreation( " BRM - 925 Verify that the filter dropdown background color is matched with the creative.");
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRM925", sheet, 5);
			String[] expVal = cellVal.split("\n");
			boolean facetloaded = false;
			
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				//BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			do
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					if(BNBasicfeature.isElementPresent(searchPageFilterTabCaption))
					{
						Pass("The Filter Caption is displayed.");
						//searchPageFilterButton.click();
						String atColor = searchPageFilterTabCaption.getCssValue("color");
						Color colhxcnvt = Color.fromString(atColor);
						String actColor = colhxcnvt.asHex();
						log.add("The expected color is : " + expVal[0]);
						log.add("The actual color was : " + actColor);
						if(actColor.equals(expVal[0]))
						{
							Pass("The Filter Tab Text Color before clicking the filter button match the expected one.",log);
							brm1896 = true;
						}
						else
						{
							Fail("The Filter Tab Text Color before clicking the filter button does not match the expected one.",log);
							brm1896 = false;
						}
					}
					else
					{
						Fail("The Filter Caption is not displayed.");
					}
					
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						facetloaded = true;
						if(BNBasicfeature.isElementPresent(searchPageFilterTabOpenCaption))
						{
							Pass("The Filter Tab is opened.");
							String atColor = searchPageFilterTabOpenCaption.getCssValue("color");
							Color colhxcnvt = Color.fromString(atColor);
							String actColor = colhxcnvt.asHex();
							log.add("The expected color is : " + expVal[1]);
							log.add("The actual color was : " + actColor);
							if(actColor.equals(expVal[1]))
							{
								Pass("The Filter Tab Text Color after clicking the filter button match the expected one.",log);
								brm1896 = true;
							}
							else
							{
								Fail("The Filter Tab Text Color after clicking the filter button does not match the expected one.",log);
								brm1896 = false;
							}
							
							String bkColor = searchPageFilterTabOpenCaption.getCssValue("background-color");
							colhxcnvt = Color.fromString(bkColor);
							actColor = colhxcnvt.asHex();
							log.add("The expected color is : " + expVal[2]);
							log.add("The actual color was : " + actColor);
							if(actColor.equals(expVal[2]))
							{
								Pass("The Filter Tab Background Color after clicking the filter button match the expected one.",log);
								brm1896 = true;
							}
							else
							{
								Fail("The Filter Tab Background Color after clicking the filter button does not match the expected one.",log);
								brm1896 = false;
							}
						}
						else
						{
							Fail("The Filter Tab is not opened.");
						}
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								for(int i =0;i<searchPageFilterFacetsLists.size();i++)
								{
									String bkColor = searchPageFilterFacetsLists.get(i).getCssValue("background").substring(0, 18);
									Color colhxcnvt = Color.fromString(bkColor);
									String actColor = colhxcnvt.asHex();
									log.add("The expected color is : " + expVal[3]);
									log.add("The actual color was : " + actColor);
									if(actColor.equalsIgnoreCase(expVal[3]))
									{
										Pass("The Filter Tab Background Color after clicking the filter button match the expected one.",log);
										brm1896 = true;
									}
									else
									{
										Fail("The Filter Tab Background Color after clicking the filter button does not match the expected one.",log);
										brm1896 = false;
									}
										
									String fctColorColor = searchPageFilterFacetsName.get(i).getCssValue("color");
									colhxcnvt = Color.fromString(fctColorColor);
									actColor = colhxcnvt.asHex();
									log.add("The expected color is : " + expVal[4]);
									log.add("The actual color was : " + actColor);
									if(actColor.equals(expVal[4]))
									{
										Pass("The Facet Background Color after clicking the filter button match the expected one.",log);
										brm1896 = true;
									}
									else
									{
										Fail("The Facet Background Color after clicking the filter button does not match the expected one.",log);
										brm1896 = false;
									}
										
									if(BNBasicfeature.isElementPresent(searchPageFilterFacetsExpandIcon.get(i)))
									{
										Pass("The Facet Expand Icon is displayed.");
										brm1896 = true;
									}
									else
									{
										Fail("The Facet Expand Icon is not displayed.");
										brm1896 = false;
									}
								}
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						facetloaded = false;
						Fail("The Filter Facets is not opened.");
						break;
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
				{
					jsclick(searchPageFilterButton);
					Thread.sleep(50);
				}
				
			}while(facetloaded!=true);
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 925 Issue ." + e.getMessage());
			Exception(" BRM - 925 Issue." + e.getMessage());
		}
		
		/* "BRM - 1896 Verify that Filters should be in dropdown as per the creative." */
		ChildCreation("BRM - 1896 Verify that Filters should be in dropdown as per the creative.");
		try
		{
			log.add( " Kindly Refer - BRM - 925 for detailed report.");
			if(brm1896==true)
			{
				Pass( " The creative mmatches the expected content.",log);
			}
			else
			{
				Fail( " There seems to be some mismmatch in the creative.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1896 Issue ." + e.getMessage());
			Exception(" BRM - 1896 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 894 Onselecting product image in product list page, it should navigate to product detail page.*/
	/* BRM - 1872 Verify that on selecting "back" button the previously visited page is displayed.*/
	public void searchImagepdpPageNavigation()
	{
		ChildCreation(" BRM - 894 Onselecting product image in product list page, it should navigate to product detail page.");
		boolean idFound = false;
		String origUrl = driver.getCurrentUrl();
		try
		{
			boolean atbfound = false;
			String selprdtname = "",selPrdtid = "";
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				for(int j = 1; j<=searchPageProductLists.size();j++)
				{
					//Thread.sleep(500);
					int count=1;
					atbfound = false;
					do
					{
						Random r = new Random();
						int sel = r.nextInt(searchPageProductLists.size());
						if(sel<1)
						{
							sel = 1;
						}
						
						BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
						WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
						selprdtname = prdtlist.getText();
						WebElement img = driver.findElement(By.xpath("(//*[@class='skMob_productImgDiv']//img)["+sel+"]"));
						log.add("The Selected Product Name is  : " + selprdtname);
						log.add("The Selected Product Name is : " + selprdtname);
						selPrdtid = prdtlist.getAttribute("identifier").toString();
						log.add("The Selected Product ID is  : " + selPrdtid);
						log.add("Selected Product ID : " + selPrdtid);
						atbfound = false;
						jsclick(img);
						wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
						//Thread.sleep(1500);
						//wait.until(ExpectedConditions.visibilityOf(pdpPageProductName));
						
						if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
						{
							atbfound = true;
							break;
						}
						else
						{
							count++;
							driver.navigate().back();
							continue;
						}
					}while(atbfound==true||count<10);
				}
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
						
			if(atbfound==true)
			{
				if(BNBasicfeature.isElementPresent(productId))
				{
					String id = productId.getAttribute("identifier").toString();
					log.add("The Actual Product ID is  : " + selPrdtid);
					idFound = false;
					if(id.equals(selPrdtid))
					{
						Pass("The User is navigated to the expected page.");
						idFound = true;
					}
					else
					{
						Fail("The User is not navigated to the expected page.");
						idFound = false;
					}
				}
				else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					
					for(int i = 0;i<pdpPageATBBtns.size();i++)
					{
						String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							idFound = true;
							break;
						}
						else
						{
							idFound = false;
							continue;
						}
					}
						
					if(idFound==true)
					{
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						Fail("The User is not redirected to the user intented page.",log);
					}
				}
				else
				{
					Fail("The product id is not displayed in the Product Page.");
				}
			}
			else
			{
				Fail("The Add to back button is not displayed in the pdpPage.");
			}
			//driver.navigate().back();
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 894 Issue ." + e.getMessage());
			Exception(" BRM - 894 Issue." + e.getMessage());
		}
		
		/* BRM - 1872 Verify that on selecting back button the previously visited page is displayed. */
		ChildCreation(" BRM - 1872 Verify that on selecting back button the previously visited page is displayed.");
		if(idFound==true)
		{
			try
			{
				String currUrl = driver.getCurrentUrl();
						
				if(origUrl.equals(currUrl))
				{
					Fail("The User is not navigated to the Intended page.");
				}
				else
				{
					Pass("The User is not in the Same Page.");
				}
					
				driver.navigate().back();
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(100);
				currUrl = driver.getCurrentUrl();
					
				log.add("The Navigated back url is : " + origUrl);
				log.add("The Navigated back url is : " + currUrl);
				if(origUrl.equals(currUrl))
				{
					Pass("The User is redirected to the original url.");
				}
				else
				{
					Fail("The User is not redirected to the original url.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 1872 Issue ." + e.getMessage());
				Exception(" BRM - 1872 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the respective Page.");
		}
	}
	
	/* BRM - 911 User can able to expand and close the filters*/
	public void searchpageRemoveFilterExpandClose()
	{
		ChildCreation(" BRM - 911 User can able to expand and close the filters.");
		try
		{
			boolean facetVal = false;
			do
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				BNBasicfeature.scrollup(header, driver);
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facets List is displayed.");
								for(int i = 0;i<searchPageFilterFacetsLists.size();i++)
								{
									jsclick(searchPageFilterFacetsLists.get(i));
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
									{
										wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "block"));
										Thread.sleep(100);
										facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("block");
										if(facetVal==true)
										{
											Pass("The Facet is opened in the filter drop down.");
											for(int j = 0; j<searchPageFilterFacetsNameExpanded.size();j++)
											{
												jsclick(searchPageFilterFacetsNameExpanded.get(j));
												wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "none"));
												j--;
											}
											facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("none");
											if(facetVal==true)
											{
												Pass("The Facet is closed.");
											}
											else
											{
												Fail("The Facet is not closed.");
											}
										}
										else
										{
											Fail("The Facet Val was not opened.");
										}
									}
									else
									{
										Fail("The Facets Value is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}while(facetVal!=true);
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 911 Issue ." + e.getMessage());
			Exception(" BRM - 911 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 929 Verify that On selecting the new filter option the already expanded/opened filter does not close*/
	public void searchFilterNoCollapse()
	{
		ChildCreation("BRM - 929 Verify that On selecting the new filter option the already expanded/opened filter does not close.");
		int prevSize = 0,preSize = 0;
		int openCurrSize = 0;
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
			{
				Pass("The Filter Section Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facets List is displayed.");
								do
								{
									preSize = searchPageFilterFacetsName.size();
									for(int i = 0;i<searchPageFilterFacetsName.size();i++)
									{
										String facetName = searchPageFilterFacetsName.get(i).getText();
										jsclick(searchPageFilterFacetsName.get(i));
										if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
										{
											wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "block"));
											Thread.sleep(100);
											boolean facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("block");
											Thread.sleep(100);
											if(facetVal==true)
											{
												Pass("The "+ (i+1) +" Facet is opened. The Selected facet was : " + facetName);
												prevSize++;
												break;
											}
											else
											{
												Fail("The "+ (i+1) +" Facet was not opened. The Selected facet was : " + facetName);
												break;
											}
										}
										else
										{
											Fail("The Facets Value is not displayed.");
										}
									}
								}while(preSize>1);
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
							
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
							{
								Pass("The Expanded Filters is displayed.");
								openCurrSize = searchPageFilterFacetsNameExpanded.size();
								for(int i = 0;i<searchPageFilterFacetsNameExpanded.size();i++)
								{
									String facetName = searchPageFilterFacetsNameExpanded.get(i).getText();
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
									{
										wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "block"));
										//Thread.sleep(1000);
										boolean facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("block");
										//Thread.sleep(1000);
										if(facetVal==true)
										{
											Pass("The "+ (i+1) +" Facet is opened. The Selected facet was : " + facetName);
										}
										else
										{
											Fail("The "+ (i+1) +" Facet was not opened. The Selected facet was : " + facetName);
										}
									}
									else
									{
										Fail("The Facets Value is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Expanded filter are not displayed.");
							}
							
							if(prevSize==openCurrSize)
							{
								Pass("The Total number of filters before expanding was : " + prevSize + " and the number of expanded filter was : " + openCurrSize + " and it matches.");
							}
							else
							{
								Fail("The Total number of filters before expanding was : " + prevSize + " and the number of expanded filter was : " + openCurrSize + " and it does not matches.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}
			else
			{
				Fail("The Filter Section Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 929 Issue ." + e.getMessage());
			Exception(" BRM - 929 Issue." + e.getMessage());
		}
		driver.navigate().refresh();
	}
	
	/* BRM - 1846 Verify that the Results should be shown for common synonyms or misspellings i.e if User enters "banres" instead of "barnes" the related search results should be displayed with the message "Your search for banres was adjusted to barnes"*/
	public void searchPageMisspellSuggestion()
	{
		ChildCreation("BRM - 1846 Verify that the Results should be shown for common synonyms or misspellings i.e if User enters banres instead of barnes the related search results should be displayed with the message Your search for banres was adjusted to barnes");
		try
		{
			String book = BNBasicfeature.getExcelVal("BRM1846", sheet, 3);
			String expRes = BNBasicfeature.getExcelVal("BRM1846", sheet, 6);
			
			wait.until(ExpectedConditions.visibilityOf(header));
			wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
			
			if(BNBasicfeature.isElementPresent(searchPageFilterFacetsEnabled))
			{
				jsclick(searchPageFilterButton);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(50);
			}
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			boolean bkOk = searchPrdt(book); 
			if(bkOk==true)
			{
				Pass("The Searched Content was loaded successfully.");
				if(BNBasicfeature.isElementPresent(searchMisSpellSuggestion))
				{
					Pass("The Search Misspell suggestion is displayed.");
					if(BNBasicfeature.isElementPresent(searchMisSpellSuggestionText))
					{
						Pass("The Misspell suggestion text is displayed.");
						String text = searchMisSpellSuggestionText.getText();
						log.add("The Expected result was : " + expRes);
						log.add("The Actual result is : " + text);
						if(text.isEmpty() && (text.contains(expRes)))
						{
							Fail("The Misspelled suggestion text seems to be empty.",log);
						}
						else
						{
							Pass("The Misspelled suggestion text is displayed.",log);
						}
					}
					else
					{
						Fail("The Misspell Suggestion text is not displayed.");
					}
				}
				else
				{
					Fail("The Search Misspell suggestion is not displayed.");
				}
			}
			else
			{
				Fail("The Searched Page Failed to Load.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1846 Issue ." + e.getMessage());
			Exception(" BRM - 1846 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1850 Verify that the list view and the grid view options must be displayed.*/
	public void searchResultGridListOptions()
	{
		ChildCreation("BRM - 1850 Verify that the list view and the grid view options must be displayed.");
		try
		{
			if(BNBasicfeature.isElementPresent(listViewIcon))
			{
				Pass("The List View icon is displayed.");
			}
			else
			{
				Fail("The List View Icon is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(gridViewIcon))
			{
				Pass("The Grid View Icon is displayed.");
			}
			else
			{
				Fail("The Grid View icon is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1850 Issue ." + e.getMessage());
			Exception(" BRM - 1850 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1873 Verify that on_clicking reviews count link, it should redirect to product details page.*/
	public void searchFilterReviewCountClickPDPPageNavigation()
	{
		ChildCreation("BRM - 1873 Verify that on_clicking reviews count link, it should redirect to product details page.");
		String origUrl = "", currUrl = "";
		try
		{
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				BNBasicfeature.scrollup(header, driver);
				//Thread.sleep(1000);
				jsclick(listViewIcon);
				wait.until(ExpectedConditions.visibilityOf(listViewList));
				Thread.sleep(50);
			}
			
			boolean atbfound = false;
			boolean rvCont = false;
			String selprdtname = "",selPrdtid = "";
			if(BNBasicfeature.isListElementPresent(searchPageProductLists))
			{
				//Thread.sleep(500);
				int count=1;
				atbfound = false;
				do
				{
					origUrl = driver.getCurrentUrl();
					Random r = new Random();
					int sel = r.nextInt(searchPageProductLists.size());
					if(sel<1)
					{
						sel = 1;
					}
					
					BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
					WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
					selprdtname = prdtlist.getText();
					log.add("The Selected Product Name is  : " + selprdtname);
					log.add("The Selected Product Name is : " + selprdtname);
					selPrdtid = prdtlist.getAttribute("identifier").toString();
					log.add("The Selected Product ID is  : " + selPrdtid);
					log.add("Selected Product ID : " + selPrdtid);
					atbfound = false;
					rvCont = false;
					WebElement reviewContainer = searchPageProductLists.get(sel).findElement(By.xpath("//*[contains(@class,'reviews')]"));
					if(BNBasicfeature.isElementPresent(reviewContainer))
					{
						Pass("The review container is displayed.");
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+sel+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							WebElement totalreviewContainer = driver.findElement(By.xpath("(//*[@id='searchpage']//*[@class='skMob_productListItemOuterCont'])["+sel+"]//*[contains(@class,'ratingContainer')]//*[contains(@class,'RatingCount')]"));
							String reviewCount = reviewCnt.getAttribute("title");
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								rvCont = true;
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
								jsclick(totalreviewContainer);
								//Thread.sleep(2000);
								wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
								Thread.sleep(100);
								if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
								{
									atbfound = true;
									break;
								}
								else
								{
									atbfound = false;
									count++;
									driver.navigate().back();
									continue;
								}
							}
						}
						catch(Exception e)
						{
							atbfound = false;
							rvCont = false;
							count++;
							//driver.navigate().back();
							continue;
						}
					}
					else
					{
						Fail("The review container is not displayed.");
					}
				}while(atbfound==true||count<10);
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
			if(rvCont==true)
			{
				if(atbfound==true)
				{
					boolean idFound = false;
					if(BNBasicfeature.isElementPresent(productId))
					{
						String id = productId.getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							currUrl = driver.getCurrentUrl();
							idFound = true;
						}
						else
						{
							Fail("The User is not navigated to the expected page.");
							idFound = false;
						}
						
						if(idFound==true)
						{
							Pass("The User is redirected to the user intented page.",log);	
						}
						else
						{
							Fail("The User is not redirected to the user intented page.",log);
						}
						
						log.add("The Navigated back url is : " + origUrl);
						log.add("The Navigated New url is : " + currUrl);
						if(origUrl.equals(currUrl))
						{
							Fail("The User is not navigated to the Intended page.");
						}
						else
						{
							Pass("The User is not in the Same Page.");
						}
						
						driver.navigate().back();
						//Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						//Thread.sleep(1000);
						currUrl = driver.getCurrentUrl();
						
						log.add("The Navigated back url is : " + origUrl);
						log.add("The Navigated back url is : " + currUrl);
						if(origUrl.equals(currUrl))
						{
							Pass("The User is redirected to the original url.",log);
						}
						else
						{
							Fail("The User is not redirected to the original url.",log);
						}
					}
					else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
					{
						idFound = false;
						for(int i = 0;i<pdpPageATBBtns.size();i++)
						{
							String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
							log.add("The Actual Product ID is  : " + selPrdtid);
							if(id.equals(selPrdtid))
							{
								Pass("The User is navigated to the expected page.");
								idFound = true;
								currUrl = driver.getCurrentUrl();
								break;
							}
							else
							{
								idFound = false;
								continue;
							}
						}
							
						if(idFound==true)
						{
							Pass("The User is redirected to the user intented page.",log);	
						}
						else
						{
							Fail("The User is not redirected to the user intented page.",log);
						}
						
						log.add("The Navigated back url is : " + origUrl);
						log.add("The Navigated New url is : " + currUrl);
						if(origUrl.equals(currUrl))
						{
							Fail("The User is not navigated to the Intended page.");
						}
						else
						{
							Pass("The User is not in the Same Page.");
						}
						
						driver.navigate().back();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(1000);
						currUrl = driver.getCurrentUrl();
						
						log.add("The Navigated back url is : " + origUrl);
						log.add("The Navigated back url is : " + currUrl);
						if(origUrl.equals(currUrl))
						{
							Pass("The User is redirected to the original url.",log);
						}
						else
						{
							Fail("The User is not redirected to the original url.",log);
						}
						
					}
					else
					{
						Fail("The product id is not displayed in the Product Page.");
					}
				}
				else
				{
					Fail("The Add to back button is not displayed in the pdpPage.");
				}
			}
			else
			{
				Skip( "No Review Container is displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1873 Issue ." + e.getMessage());
			Exception(" BRM - 1873 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1899 Verify that all the subcategories in filters are shown one by one as accordians with + option.*/
	/* BRM - 1900 Verify that on clicking + option, the options from the selected category must be expanded.*/
	/* BRM - 1901 Verify that on clicking - option, the expanded options must be closed */
	/* BRM - 1904 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown*/
	public void searchPageFilterAccordians()
	{
		ChildCreation("BRM - 1899 Verify that all the subcategories in filters are shown one by one as accordians with + option.");
		int i = 0,fsel = 0,prevSize = 0,currSize = 0;
		boolean facetOpen = false,facetVal = false;
		try
		{
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				BNBasicfeature.scrollup(header, driver);
				//Thread.sleep(1000);
				jsclick(listViewIcon);
				wait.until(ExpectedConditions.visibilityOf(listViewList));
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
			{
				Pass("The Filter Section Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					BNBasicfeature.scrollup(header, driver);
					boolean enabled = false;
					WebDriverWait w1 = new WebDriverWait(driver, 1);
					do
					{
						try
						{
							jsclick(searchPageFilterButton);
							w1.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
							enabled = true;
						}
						catch(Exception e)
						{
							enabled = false;
							enabled = searchPageFilterFacets.getAttribute("style").contains("block");
						}
					}while(enabled==false);
					//jsclick(searchPageFilterButton);
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								prevSize = searchPageFilterFacetsLists.size();
								Pass("The Facet list is displayed.");
								for(i = 0; i<searchPageFilterFacetsLists.size();i++)
								{
									String facetName = searchPageFilterFacetsName.get(i).getText();
									if(facetName.isEmpty())
									{
										Fail("The Facet Name is not displayed.");
									}
									else
									{
										log.add("The found facet is : " + facetName);
										Pass("The Facet Name is displayed.",log);
									}
									
									if(BNBasicfeature.isElementPresent(searchPageFilterFacetsExpandIcon.get(i)))
									{
										Pass("The Facet Expand Icon is displayed.");
									}
									else
									{
										Fail("The Facet Expand Icon is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Facet list is not displayed in the filter.");
							}
						}
						else
						{
							Fail("The Facet list Container is not displayed in the filter.");
						}
					}
					else
					{
						Fail("The Facet is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}
			else
			{
				Fail("The Search Filter Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1899 Issue ." + e.getMessage());
			Exception(" BRM - 1899 Issue." + e.getMessage());
		}
		
		/* BRM - 1904 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown*/
		ChildCreation("BRM - 1904 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				Pass("The Search Mask is displayed.");
				//Thread.sleep(100);
			}
			else
			{
				Fail("No Search Pop Up Mask is displayed when search is enabled.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1904 Issue ." + e.getMessage());
			Exception(" BRM - 1904 Issue." + e.getMessage());
		}
		
		
		/* BRM - 1900 Verify that on clicking + option, the options from the selected category must be expanded.*/
		ChildCreation("BRM - 1900 Verify that on clicking + option, the options from the selected category must be expanded.");
		try
		{
			if(facetOpen==true)
			{
				if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
				{
					Pass("The Filter Facets is displayed.");
					if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
					{
						Pass("The Facets List is displayed.");
						Random fr = new Random();
						fsel = fr.nextInt(searchPageFilterFacetsLists.size());
						jsclick(searchPageFilterFacetsLists.get(fsel));
						if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
						{
							wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(fsel), "style", "block"));
							Thread.sleep(100);
							facetVal = searchPageFilterFacetsValue.get(fsel).getAttribute("style").contains("block");
							Thread.sleep(100);
							if(facetVal==true)
							{
								Pass("The Accordian is opened.");
							}
							else
							{
								Fail("The Accordian is not opened.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Facets is not displayed.");
					}
				}
				else
				{
					Fail("The Filter Facets is not displayed.");
				}
			}
			else
			{
				Fail("The Filter is not opened.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1900 Issue ." + e.getMessage());
			Exception(" BRM - 1900 Issue." + e.getMessage());
		}
		
		/* BRM - 1901 Verify that on clicking - option, the expanded options must be closed */
		ChildCreation("BRM - 1901 Verify that on clicking - option, the expanded options must be closed .");
		try
		{
			if(facetVal==true)
			{
				Pass("The Accordian is opened.");
				if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
				{
					/*for(i = 0; i<searchPageExpandedFilterFacetsLists.size();i++)
					{
						searchPageExpandedFilterFacetsLists.get(i).click();
						Thread.sleep(500);
					}*/
					int size = 0;
					do
					{
						for(i = 0; i<searchPageFilterFacetsNameExpanded.size();i++)
						{
							jsclick(searchPageFilterFacetsNameExpanded.get(i));
							Thread.sleep(250);
						}
						size = searchPageFilterFacetsNameExpanded.size();
					}while(size>1);
				}
				
				if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsNameExpanded))
				{
					Fail("Expanded Container is Still displayed.");
				}
				else
				{
					currSize = searchPageFilterFacetsLists.size();
					if(prevSize==currSize)
					{
						Pass("No Expanded Container is displayed.");
					}
					
					for(i = 0; i<searchPageFilterFacetsValue.size();i++)
					{
						Thread.sleep(250);
						facetVal = searchPageFilterFacetsValue.get(fsel).getAttribute("style").contains("none");
						Thread.sleep(200);
						if(facetVal==true)
						{
							Pass("The Accordian is not opened.");
						}
						else
						{
							Fail("The Accordian is opened.");
						}
					}
				}
				//Thread.sleep(1000);
			}
			else
			{
				Fail("The Accordian is not opened.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1901 Issue ." + e.getMessage());
			Exception(" BRM - 1901 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1906 Verify that paginations should be shown on bottom as per the creative.*/
	public void searchPageHavePagination()
	{
		ChildCreation("BRM - 1906 Verify that paginations should be shown on bottom as per the creative.");
		int count = 0;
		try
		{
			if(BNBasicfeature.isElementPresent(gridViewList))
			{
				BNBasicfeature.scrollup(header, driver);
				//Thread.sleep(1000);
				jsclick(listViewIcon);
				wait.until(ExpectedConditions.visibilityOf(listViewList));
				Thread.sleep(50);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			resultContainerDetails();
			count = searchPrdtTotalCount;
			log.add("The Total Product Count is : " + count);
			if(count>20)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
				{
					String attr = searchPagePaginationContainer.getAttribute("style");
					if(attr.contains("block"))
					{
						Pass("The Pagination is enabled.",log);
					}
					else
					{
						Fail("The Pagination is not enabled.",log);
					}
				}
				else
				{
					Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
				}
			}
			else if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
			{
				try
				{
					String attr = searchPagePaginationContainer.getAttribute("style");
					if(attr.contains("block"))
					{
						Fail("The Pagination is enabled when the product count is less than or equal to 20.",log);
					}
					else
					{
						Pass("The Pagination is not enabled.",log);
					}
				}
				catch(Exception e)
				{
					Pass("The Pagination is not enabled.",log);
				}
			}
			else
			{
				Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1906 Issue ." + e.getMessage());
			Exception(" BRM - 1906 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1912 Verify that "Back to Top" button is displaying while scroll down the products in product list page.*/
	public void searchPageBacktoTop() throws Exception
	{
		ChildCreation(" BRM - 1912 Verify that Back to Top button is displaying while scroll down the products in product list page.");
		int count = 0;
		try
		{
			resultContainerDetails();
			count = searchPrdtTotalCount;
			if(BNBasicfeature.isElementPresent(header))
			{
				BNBasicfeature.scrolldown(header, driver);
				if(BNBasicfeature.isElementPresent(noShowBacktoTop))
				{
					Pass("The Show To Top button is not displayed.");
				}
				else
				{
					Fail("The Show To Top button is displayed when the Page is already scrolled up.");
				}
				
				if(count>6)
				{
					if(BNBasicfeature.isElementPresent(footerSectionContainer))
					{
						BNBasicfeature.scrolldown(footerSectionContainer, driver);
						wait.until(ExpectedConditions.visibilityOf(showBacktoTop));
						if(BNBasicfeature.isElementPresent(showBacktoTop))
						{
							Pass("The Show To Top button is displayed.");
						}
						else
						{
							Fail("The Show To Top button is not displayed when the Page is scrolled down fully.");
						}
					}
					else
					{
						Skip("The Back to top cotainer counld not verified since the count is less than the expected one.");
					}
				}
				else
				{
					Skip("The Back to top cotainer counld not verified since the count is less than the expected one.");
				}
			}
			else
			{
				Fail("The Page Could not be scrolled up.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 1912 Issue ." + e.getMessage());
			Exception(" BRM - 1912 Issue." + e.getMessage());
		}
		
		if(BNBasicfeature.isElementPresent(listViewList))
		{
			Pass("The List View list is displayed.");
			if(BNBasicfeature.isElementPresent(gridViewIcon))
			{
				Pass("The Grid View icon is displayed.");
				jsclick(gridViewIcon);
				wait.until(ExpectedConditions.visibilityOf(gridViewList));
			}
		}
	}
	
	/* BRM - 857 Verify that on navigating to other pages, and coming back, the selected option is maintained.*/
	public void searchGridViewPersistent()
	{
		ChildCreation("BRM - 857 Verify that on navigating to other pages, and coming back, the selected option is maintained.");
		int mrno,cmrno = 0,scno; 
		try
		{
			BNBasicfeature.scrollup(header, driver);
			Thread.sleep(100);
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id='sk_mobCategory_id']//*[@class='sk_mobCategoryItem']")));
				Thread.sleep(1000);
				List<WebElement> menuele = driver.findElements(By.xpath("//*[@id='sk_mobCategory_id']//*[@class='sk_mobCategoryItem']"));
				Random r = new Random();
				mrno = r.nextInt(menuele.size());
				if(mrno<1)
				{
					mrno = 1;
				}
				WebElement mName = driver.findElement(By.xpath("//*[@class='sk_mobCategoryItem']//*[@id='sk_mobCategoryItemCont_id_"+mrno+"_0']"));
				BNBasicfeature.scrolldown(mName, driver);
				jsclick(mName);
				Thread.sleep(1500);
				BNBasicfeature.scrollup(mName, driver);
				Thread.sleep(500);
				List<WebElement> catele = driver.findElements(By.xpath("//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@type='category']"));
				cmrno = r.nextInt(catele.size());
				if(cmrno<=1)
				{
					cmrno = 1;
				}
				WebElement cName = driver.findElement(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@type='category'])["+cmrno+"]"));
				BNBasicfeature.scrolldown(cName, driver);
				jsclick(cName);
				Thread.sleep(1000);
				List<WebElement> subcateele = driver.findElements(By.xpath("(//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel_"+mrno+"_0']//*[@class='sk_mobSubCategory'])["+cmrno+"]//*[@level]"));
				scno = r.nextInt(subcateele.size());
				if(scno<=1)
				{
					scno = 1;
				}
				WebElement subcName = driver.findElement(By.xpath("((//*[@id='sk_mobCategoryItem_id_"+mrno+"_0']//*[@id='sk_mobSublevel__1'])["+cmrno+"]//*[@class='sk_mobCategoryItemTxt'])["+scno+"]"));
				BNBasicfeature.scrolldown(subcName, driver);
				jsclick(subcName);
				Thread.sleep(500);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(500);
				driver.navigate().back();
				Thread.sleep(500);
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(500);
				if(BNBasicfeature.isElementPresent(gridViewList))
				{
					Pass("The Gird View List is maintained when user navigate and come back.");
				}
				else
				{
					Fail("The Grid view is not maintained when user navigate and come back.");
				}
			}
			else
			{
				Fail("The Menu is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 857 Issue ." + e.getMessage());
			Exception(" BRM - 857 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2897 verify whether clicking on the author name in PLP page, loads PLP with book list written by corresponding author.*/
	public void searchPageAuthorNameFilter()
	{
		ChildCreation("BRM - 2897 verify whether clicking on the author name in PLP page, loads PLP with book list written by corresponding author.");
		String selAuthorName = "",setAuthorName = "";
		boolean pdpPage = false;
		try
		{
			do
			{
				if(BNBasicfeature.isListElementPresent(searchPageProductLists))
				{
					if(BNBasicfeature.isListElementPresent(searchResultListAuthorName))
					{
						Pass("The Author Name is displayed.");
						Random r = new Random();
						int sel = r.nextInt(searchResultListAuthorName.size());
						if(sel<1)
						{
							sel = 1;
						}
						BNBasicfeature.scrolldown(searchResultListAuthorName.get(sel), driver);
						selAuthorName = searchResultListAuthorName.get(sel).getText().replace("by", "");
						log.add("The Selected Author Name was : " + selAuthorName);
						jsclick(searchResultListAuthorName.get(sel));
						if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
						{
							pdpPage = true;
							driver.navigate().back();
							wait.until(ExpectedConditions.visibilityOf(header));
						}
						else
						{
							pdpPage = false;
						}
					}
					else
					{
						Fail("The Author Name is not displayed.");	
					}
				}
				else
				{
					Fail("The Search result Page results are not displayed.");
				}
			}while(!pdpPage==false);
			
			if(pdpPage==false)
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				resultContainerDetails();
				setAuthorName = searchContentName.replace("\"", "");
				log.add("The Set Author Name is : " + setAuthorName);
				if(selAuthorName.contains(setAuthorName))
				{
					Pass("The user were able to filter the products with the Author Name.",log);
				}
				else
				{
					Fail("The user were not able to filter the products by the Author Name.",log);
				}
			}
			else
			{
				Fail("The user were not able to filter the products by the Author Name.",log);
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2897 Issue ." + e.getMessage());
			Exception(" BRM - 2897 Issue." + e.getMessage());
		}
	}
	
	/*********************************************** Product List Page Implementation **********************************************/
	
	/*BRM - 1934 Verify that on Clicking categories/sub categories in pancake, the related plp page should be displayed*/
	public void pancakeCatSubcatToPLP()
	{
		ChildCreation("BRM - 1934 Verify that onclicking categories/sub categories in pancake, the related plp page should be displayed");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			wait.until(ExpectedConditions.visibilityOf(menu));
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(plpMenu));
				if(BNBasicfeature.isElementPresent(plpMenu))
				{
					plpMenu.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(plpCategory));
					if(BNBasicfeature.isElementPresent(plpCategory))
					{
						plpCategory.click();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpSubCategory));
						if(BNBasicfeature.isElementPresent(plpSubCategory))
						{
							plpSubCategory.click();
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							wait.until(ExpectedConditions.visibilityOfAllElements(featuredContentLinks));
							//System.out.println(featuredContentLinks.size());
							if(BNBasicfeature.isListElementPresent(featuredContentLinks))
							{
								Random r = new Random();
								int sel = r.nextInt(featuredContentLinks.size());
								//if(sel<1)
								if(sel>=0)
								{
									sel = 1;
								}
								jsclick(featuredContentLinks.get(sel));
								//action.moveToElement(Contentlinks.get(sel)).click().build().perform();
								wait.until(ExpectedConditions.visibilityOf(header));
								Thread.sleep(100);
								wait.until(ExpectedConditions.visibilityOf(homepageHeader));
								wait.until(ExpectedConditions.visibilityOfAllElements(plpPageProductLists));
								if(BNBasicfeature.isListElementPresent(plpPageProductLists))
								{										
									Pass("Onclicking categories/sub categories in pancake, the related plp page displayed");
								}
								else
								{
									Fail("Onclicking categories/sub categories in pancake, the related plp page not displayed");
								}
							}
							else
							{
								Fail("BNClassics_Contentlinks not displayed");
							}								
						}
						else
						{
							Fail("BNClassics category not displayed");
						}		
					}
					else
					{
						Fail("CustomerFavorites category not displayed");
					}		
				}
				else
				{
					Fail("Books category not displayed");
				}	
			}
			else
			{
				Fail( "The Menu is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1934 Issue ." + e.getMessage());
			Exception(" BRM - 1934 Issue." + e.getMessage());
		}				
	}
	
	/*BRM - 1940 Verify that Pancake menu must be closed and the respective category pages must be displayed on selecting the categories in menu*/
	public void respectivePlp()
	{
		ChildCreation(" BRM - 1940 Verify that Pancake menu must be closed and the respective category pages must be displayed on selecting the categories in menu");
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElements(plpPageProductLists));
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				if(BNBasicfeature.isElementPresent(storeTitle))
				{
					String act_str = storeTitle.getText();
					if(act_str.equalsIgnoreCase("Barnes & Noble Classics"))
					{
						Pass("Respective category page displayed on selecting the categories in menu");
					}
					else
					{
						Fail("Respective category page not displayed on selecting the categories in menu");						
					}
				}
				else
				{
					Fail( " The Store Title is not displayed.");
				}
			}
			else
			{
				Fail( "The Store Title is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1940 Issue ." + e.getMessage());
			Exception(" BRM - 1940 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 1943 Verify that the "sort" and the "filter" options are displayed below the header.*/
	/* BRM - 1988 Verify that the "sort" must be displayed below the header with arrow pointing down as per the creative*/
	/* BRM - 2016 Verify that Filters,sort, list,grid views are displayed in same line as per the creative*/
	public void plpFilterSort() 
	{
		ChildCreation(" BRM - 1943 Verify that the 'sort' and the 'filter' options are displayed below the header.");
		boolean brm2016 = false;
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageFilterTabCaption));
			if((BNBasicfeature.isElementPresent(searchPageSortButton))&&(BNBasicfeature.isElementPresent(searchPageFilterButton)))
			{						
				Pass("'sort' and the 'filter' options are displayed below the header");
				brm2016 =  true;
			}
			else
			{
				Fail("'sort' and the 'filter' options are not displayed below the header");
				brm2016 = false;
			}				
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1943 Issue ." + e.getMessage());
			Exception(" BRM - 1943 Issue." + e.getMessage());
		}
		
		/* BRM - 1988 Verify that the "sort" must be displayed below the header with arrow pointing down as per the creative*/
		ChildCreation("BRM - 1988 Verify that the 'sort' must be displayed below the header with arrow pointing down as per the creative");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageSortTabCaption));
			if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
			{						
				Pass("'sort' displayed below the header with arrow pointing down as per the creative");
			}
			else
			{
				Fail("'sort'not displayed below the header with arrow pointing down as per the creative");
			}				
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1988 Issue ." + e.getMessage());
			Exception(" BRM - 1988 Issue." + e.getMessage());
		}
		
		/* BRM - 2016 Verify that Filters,sort, list,grid views are displayed in same line as per the creative*/
		ChildCreation("BRM - 2016 Verify that Filters,sort, list,grid views are displayed in same line as per the creative");
		try
		{
			if(brm2016==true)
			{
				Pass( "The Filter and Sort button is displayed.");
			}
			else
			{
				Fail( "The Filter and Sort button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2016 Issue ." + e.getMessage());
			Exception(" BRM - 2016 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 1944 Verify that the category title matches the selected category.*/
	public void categoryTitle()
	{
		ChildCreation("BRM - 1944 Verify that the category title matches the selected category");
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElements(plpPageProductLists));
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				//Thread.sleep(500);
				String act_str = storeTitle.getText();
				if(act_str.equalsIgnoreCase("Barnes & Noble Classics"))
				{
					Pass("Category title matches the selected category");
				}
				else
				{
					Fail("Category title not matches the selected category");						
				}
			}
			else
			{
				Fail( "The Product List Page is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1944 Issue ." + e.getMessage());
			Exception(" BRM - 1944 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1945 Verify that the total number of products available in the selected category is displayed.*/
	public void productsTotal()
	{
		ChildCreation("BRM - 1945 Verify that the total number of products available in the selected category is displayed");
		try
		{
			resultContainerDetails();
			int prdtCount = searchPrdtTotalCount;
			if(prdtCount>0)
			{
				Pass("Total number of products available in the selected category is displayed"+ prdtCount);
			}
			else
			{
				Fail("Total number of products available in the selected category is not displayed.");
			}				
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1945 Issue ." + e.getMessage());
			Exception(" BRM - 1945 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 1941 Verify that the selected sort options should be BOLD in sort dropdown*/
	public void sortBold() throws Exception
	{
		ChildCreation(" BRM - 1941 Verify that the selected sort options should be BOLD in sort dropdown.");
		String color = BNBasicfeature.getExcelVal("BRM-1941", sheet, 2);
		boolean srtFound = false;
		boolean sel = false;
		String actColor = "",currSelColor = "", newColor = "",deSelectedcolor = "",select = "";
		String hexCode = "",chexCode = "",dhexCode = "",nhexCode = "";
		String prevName = "",newSrtName = "", currSrtName = "",deSelectedSrtName;
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageSortTabCaption));
			wait.until(ExpectedConditions.elementToBeClickable(searchPageSortTabCaption));
			if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
			{			
				enableSort();
				wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
				wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
				if(BNBasicfeature.isListElementPresent(searchPageSortFacetsLists))
				{
					Pass("The Sort List Facets is displayed.");
					srtFound = false;
					for(int j=1; j<=searchPageSortFacetsLists.size();j++)
					{
						wait.until(ExpectedConditions.visibilityOf(searchPageSortFacetsLists.get(j-1)));
						String srchName = searchPageSortFacetsLists.get(j-1).getText();
						try
						{
							select = searchPageSortFacetsLists.get(j-1).getAttribute("selected");
							actColor = searchPageSortFacetsLists.get(j-1).getCssValue("color");
							prevName = searchPageSortFacetsLists.get(j-1).getText();
							if(select.equals("true"))
							{
								srtFound = true;
								sel = true;
								log.add("The Searching Value is : " + srchName);
								break;
							}
							else
							{
								srtFound = false;
								continue;
							}
						}
						catch(Exception e)
						{
							continue;
						}
					}
						
					if(srtFound==true)
					{
						Pass("The Searched value is found.",log);
						Color colorhxCnvt = Color.fromString(actColor);
						hexCode = colorhxCnvt.asHex();
						
						if(sel==true)
						{
							Pass("The Top Matches is selected by default.");
							log.add("The Expected Color was : " + color);
							log.add("The Actual Color was : " + hexCode);
							if(color.equals(hexCode))
							{
								Pass(" Selected sort option is BOLD . ");
							}
							else
							{
								Fail("Selected sort option is not BOLD.");	
							}
						}
						else
						{
							Fail("The Top Matches is not selected by default.");
						}
					}
					else
					{
						Fail(" No Option was Selected by Default.",log);
					}
				}
			}	
			else
			{
				Fail( " The Sort Option is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1941 Issue ." + e.getMessage());
			Exception(" BRM - 1941 Issue." + e.getMessage());
		}
		
		ChildCreation("BRM - 1942 Verify that the selected sort options should be highlighted and the previous selected option must be deselected.");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPagePopUpMask))
			{
				jsclick(searchPagePopUpMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			for(int j=1; j<=searchPageSortFacetsLists.size();j++)
			{
				newSrtName = searchPageSortFacetsLists.get(j-1).getText();
				if(!prevName.contains(newSrtName))
				{
					try
					{
						sel = searchPageSortFacetsLists.get(j-1).getAttribute("selected").contains("selected");
						if(sel==true)
						{
							srtFound = false;
							continue;
						}
					}
					catch(Exception e)
					{
						srtFound = true;
						currSelColor = searchPageSortFacetsLists.get(j-1).getCssValue("color");
						Color colorhxCnvt = Color.fromString(currSelColor);
						chexCode = colorhxCnvt.asHex();
						log.add( "The Selected Sort Name is  : " + searchPageSortFacetsLists.get(j-1).getText());
						jsclick(searchPageSortFacetsLists.get(j-1));
						Thread.sleep(10);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(searchPageSortButton));
						//act.moveToElement(searchPageSortFacetsLists.get(j-1)).click().build().perform();
						break;
					}
				}
				else
				{
					continue;
				}
			}
			
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			
			for(int j=1; j<=searchPageSortFacetsLists.size();j++)
			{
				currSrtName = searchPageSortFacetsLists.get(j-1).getText();
				if(currSrtName.equals(newSrtName))
				{
					newColor = searchPageSortFacetsLists.get(j-1).getCssValue("color");
					Color colorhxCnvt = Color.fromString(newColor);
					nhexCode = colorhxCnvt.asHex();
					break;
				}
				else
				{
					continue;
				}
			}
		
			for(int j=1; j<=searchPageSortFacetsLists.size();j++)
			{
				deSelectedSrtName = searchPageSortFacetsLists.get(j-1).getText();
				if(deSelectedSrtName.equals(prevName))
				{
					deSelectedcolor = searchPageSortFacetsLists.get(j-1).getCssValue("color");
					Color colorhxCnvt = Color.fromString(deSelectedcolor);
					dhexCode = colorhxCnvt.asHex();
					break;
				}
				else
				{
					continue;
				}
			}
			
			log.add ( "The Original Selected Sort Was : " + prevName);
			log.add ( "The Original Selected Sort Color Was : " + actColor);
			log.add ( "The Newly selected Sort Color before Selection was : " + chexCode);
			log.add(" The Newly Selected Sort is  : " + newSrtName);
			log.add(" The Newly Selected SortColor is  : " + nhexCode);
			log.add(" The Deselected SortColor is  : " + dhexCode);
			
			if(nhexCode.equals(dhexCode))
			{
				Fail( "The Newly Selected Sort Color and the Deselected Color are not as expected.",log);
			}
			else
			{
				Pass( "The Newly Selected Sort Color and the Deselected Color are as expected.",log);
			}
			
			log.add ( "The Newly selected Sort Color before Selection was : " + chexCode);
			log.add(" The Deselected SortColor is  : " + dhexCode);
			if(chexCode.equals(dhexCode))
			{
				Pass( "The Newly deSelected Sort Color and the Deselected Color are as expected.",log);
			}
			else
			{
				Fail( "The Previuosly DeSelected Sort Color and the Deselected Color are not as expected.",log);		
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1942 Issue ." + e.getMessage());
			Exception(" BRM - 1942 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1946 Verify that the product price is displayed with currency symbol for each product.*/
	public void productPrice()
	{
		ChildCreation(" BRM - 1946 Verify that the product price is displayed with currency symbol for each product.");
		ArrayList<Float> salePrice = new ArrayList<>();
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			wait.until(ExpectedConditions.visibilityOf(listViewIcon));
			if(BNBasicfeature.isElementPresent(listViewIcon))
			{			
				jsclick(listViewIcon);
				Thread.sleep(10);
				wait.until(ExpectedConditions.visibilityOf(listViewList));
				//Thread.sleep(100);
				salePrice = getPLPPrdSalePrice();
				log.add( "The Displayed Sale Price is  : " + salePrice);
			}
			else
			{
				Fail( "The list view icon is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1946 Issue ." + e.getMessage());
			Exception(" BRM - 1946 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1979 Verify that if the product has multiple price, the regular and the sale price are displayed.*/
	public void regular_sale_price()
	{
		ChildCreation("BRM - 1979 Verify that if the product has multiple price, the regular and the sale price are displayed");
		try
		{
			ArrayList<Float> prdtRegPrice = new ArrayList<>();
			ArrayList<Float> salePrice = new ArrayList<>();
			prdtRegPrice = getPLPPrdRegPrice();
			log.add("The Product Regular Price is displayed. The displayed Product Regular Price is : " + prdtRegPrice);
			salePrice = getPLPPrdSalePrice();
			log.add( "The Displayed Sale Price is  : " + salePrice);
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1979 Issue ." + e.getMessage());
			Exception(" BRM - 1979 Issue." + e.getMessage());
		}				
	}
	
	/*BRM - 1947 -  Verify that product ratings and reviews are displayed for each product, as per the classic site*/
	public void productRatingReviews()
	{
		ChildCreation("BRM - 1947 Verify that product ratings and reviews are displayed for each product, as per the classic site");
		try
		{
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				for(int i = 1; i<=plpPageProductLists.size();i++)
				{
					Thread.sleep(10);
					wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
					Thread.sleep(10);
					BNBasicfeature.scrolldown(plpPageProductLists.get(i-1), driver);
					Thread.sleep(5);
					WebElement reviewContainer = plpPageProductLists.get(i-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
					if(BNBasicfeature.isElementPresent(reviewContainer))
					{
						Pass("The review container is displayed.");
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							String reviewCount = reviewCnt.getAttribute("title");
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
							}
						}
						catch(Exception e)
						{
							Pass("No Rating has been displayed for the products.");
						}
						
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							String totalreviewContainer = driver.findElement(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[contains(@class,'ratingContainer')]//*[contains(@class,'RatingCount')]")).getText();
							String reviewCount = reviewCnt.getAttribute("title");
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
							}
							
							if(totalreviewContainer.isEmpty())
							{
								System.out.println("In List View Section.");
								Fail("The Total Review for the product is not displayed / empty.");
							}
							else
							{
								Pass("The Total Review for the product is displayed. The displayed review count is : " + totalreviewContainer);
							}
						}
						catch(Exception e)
						{
							Pass("No Rating has been displayed for the products.");
						}
					}
					else
					{
						Fail( "The Review Container is not displayed.");
					}
				}
			}
			else
			{
				Fail ( "The Product List is not displayed.");
			}
			BNBasicfeature.scrollup(header, driver);
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1947 Issue ." + e.getMessage());
			Exception(" BRM - 1947 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1949 Verify that the total count and category title are displayed as per the creative*/
	public void category_Count_Title() throws Exception
	{
		ChildCreation("BRM - 1949 Verify that the total count and category title are displayed as per the creative");
		String expected_str = BNBasicfeature.getExcelVal("BRM-1949", sheet, 3);
		try
		{
			wait.until(ExpectedConditions.visibilityOfAllElements(plpPageProductLists));
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				if(BNBasicfeature.isElementPresent(storeTitle))
				{
					String act_str = storeTitle.getText();
					if(act_str.equalsIgnoreCase(expected_str))
					{
						resultContainerDetails();
						wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
						if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
						{
							Pass("Total count and category title are displayed as per the creative");
							if(searchPrdtTotalCount>=0)
							{
								Pass("The Total Count is displayed.",log);
							}
							else
							{
								Fail("The Total Count is not displayed.",log);
							}
						}
						else
						{
							Fail("Total count and category title are not displayed as per the creative");						
						}
					}
					else
					{
						Fail( " There is mimatch in th expected value and the actual value. ");
					}
				}
				else
				{
					Fail( "The Store Title is not displayed.");
				}
			}
			else
			{
				Fail( "The Item Count is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1949 Issue ." + e.getMessage());
			Exception(" BRM - 1949 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1961 Verify that User should be able to view the products by 20 per page */
	public void productPerPage() throws Exception
	{
		ChildCreation("BRM - 1961 Verify that User should be able to view the products by 20 per page");
		String size = BNBasicfeature.getExcelVal("BRM-1961", sheet, 3);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
			resultContainerDetails();
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{					
				String currVal = searchPrdtLimit;
				String[] val = currVal.split("-");
				int totalCount = Integer.parseInt(val[1]);
				if(totalCount>=20)
				{
					String vall = searchPageProductCountContainer.getText();
					if(vall.contains(size))
					{
						Pass("User able to view the products by 20 per page");
					}
					else
					{
						Fail("User not able to view the products by 20 per page");
					}
				}
				else
				{
					Fail("The Total Count is less than 20.");
				}
			}
			else
			{
				Fail("The Product Count is  not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1961 Issue ." + e.getMessage());
			Exception(" BRM - 1961 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 1963 Verify that Total number of products found must be displayed.*/
	public void ProductDisplayed()
	{
		ChildCreation("BRM - 1963 Verify that Total number of products found must be displayed.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
			resultContainerDetails();
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{					
				int totalCount = searchPrdtTotalCount;
				if(totalCount>0)
				{
					Pass(" The Total Products found is displayed.");
				}
				else
				{
					Fail("The Total Product Count is empty or less than 0.");
				}
			}
			else
			{
				Fail("The Product Count is  not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1963 Issue ." + e.getMessage());
			Exception(" BRM - 1963 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 1959 Verify that Number of items found in the page should be displayed correctly.*/
	public void ItemsFound()
	{
		ChildCreation("BRM - 1959 Verify that Number of items found in the page should be displayed correctly");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
			resultContainerDetails();
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{					
				int totalCount = searchPrdtTotalCount;
				if(totalCount>0)
				{
					Pass(" The Total Products found is displayed.");
				}
				else
				{
					Fail("The Total Product Count is empty or less than 0.");
				}
			}
			else
			{
				Fail("The Product Count is  not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1959 Issue ." + e.getMessage());
			Exception(" BRM - 1959 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1967 Verify that the list view and the grid view options must be displayed. */
	public void grid_List_View()
	{
		ChildCreation("BRM - 1967 Verify that the list view and the grid view options must be displayed.");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(gridViewIcon));
			if((BNBasicfeature.isElementPresent(gridViewIcon)&&(BNBasicfeature.isElementPresent(listViewIcon))))
			{		
				Pass(" Both the list view and the grid view options are displayed");
			}
			else
			{
				Fail(" List view and the grid view options are not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1967 Issue ." + e.getMessage());
			Exception(" BRM - 1967 Issue." + e.getMessage());
		}				
	}
	
	/*BRM - 1975 Verify that the product title and product image are displayed for each product.*/
	public void prodTitleImage()
	{
		ChildCreation("BRM - 1975 Verify that the product title and product image are displayed for each product.");
		try
		{
			ArrayList<String> originalPrdtName = new ArrayList<>();
			for(int i = 1;i<=plpPageProductLists.size();i++)
			{
				BNBasicfeature.scrolldown(plpPageProductLists.get(i-1), driver);
				WebElement img = plpPageProductLists.get(i-1).findElement(By.xpath("//*[@class='skMob_productImgDiv']//img"));
				if(BNBasicfeature.isElementPresent(img))
				{
					Pass("The Image is displayed.");
				}
				else
				{
					Fail("The Image is not displayed.");
				}
				/*int resp = BNBasicfeature.imageBroken(img, log);
				if(resp==200)
				{
					Pass("The Image is not broken.");
				}
				else
				{
					Fail("The Image is broken.");
				}*/
			}
			
			originalPrdtName = getPLPPrdTitle();
			log.add("The Original Product Title list was : " + originalPrdtName);
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1975 Issue ." + e.getMessage());
			Exception(" BRM - 1975 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1969 Verify that the On selecting the product image or product title the pdp page must be displayed.*/
	/*BRM - 1970 - Verify that on selecting product title, it should open the PDP page with product details */
	/*BRM - 1985 Verify that on selecting "back" button the previously visited page is displayed. */
	public void plp_To_pdp()
	{
		ChildCreation("BRM - 1969 Verify that the On selecting the product image or product title the pdp page must be displayed.");
		boolean pdpLoaded = false;
		String currUrl = "";
		try
		{
			boolean atbfound = false;
			String selprdtname = "",selPrdtid = "";
			currUrl = driver.getCurrentUrl();
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				for(int j = 1; j<=plpPageProductLists.size();j++)
				{
					Thread.sleep(100);
					int count=1;
					atbfound = false;
					do
					{
						Random r = new Random();
						int sel = r.nextInt(plpPageProductLists.size());
						if(sel<1)
						{
							sel = 1;
						}
						
						BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
						Thread.sleep(10);
						WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
						selprdtname = prdtlist.getText();
						log.add("The Selected Product Name is  : " + selprdtname);
						log.add("The Selected Product Name is : " + selprdtname);
						selPrdtid = prdtlist.getAttribute("identifier").toString();
						log.add("The Selected Product ID is  : " + selPrdtid);
						log.add("Selected Product ID : " + selPrdtid);
						atbfound = false;
						jsclick(prdtlist);
						wait.until(ExpectedConditions.visibilityOf(pdpPageProductNameContainer));
						
						if(BNBasicfeature.isElementPresent(pdpPageProductNameContainer))
						{
							atbfound = true;
							break;
						}
						else
						{
							count++;
							driver.get(currUrl);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							continue;
						}
					}while(atbfound==true||count<10);
				}
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
						
			if(atbfound==true)
			{
				if(BNBasicfeature.isElementPresent(productId))
				{
					String id = productId.getAttribute("identifier").toString();
					log.add("The Actual Product ID is  : " + selPrdtid);
					if(id.equals(selPrdtid))
					{
						Pass("The User is navigated to the expected page.");
						pdpLoaded = true;
					}
					else
					{
						Fail("The User is not navigated to the expected page.");
						pdpLoaded = false;
					}
				}
				else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					boolean idFound = false;
					for(int i = 0;i<pdpPageATBBtns.size();i++)
					{
						String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							idFound = true;
							break;
						}
						else
						{
							idFound = false;
							continue;
						}
					}
						
					if(idFound==true)
					{
						pdpLoaded = true;
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						pdpLoaded = false;
						Fail("The User is not redirected to the user intented page.",log);
					}
				}
				else
				{
					Fail("The product id is not displayed in the Product Page.");
				}
			}
			else
			{
				Fail("The Add to back button is not displayed in the pdpPage.");
			}
			
			/*driver.get(currUrl);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));*/
		}
		catch(Exception e)
		{
			driver.get(currUrl);
			System.out.println(" BRM - 1969 Issue ." + e.getMessage());
			Exception(" BRM - 1969 Issue." + e.getMessage());
		}
		
		/*BRM - 1970 - Verify that on selecting product title, it should open the PDP page with product details */
		ChildCreation(" BRM - 1970 - Verify that on selecting product title, it should open the PDP page with product details.");
		try
		{
			if(pdpLoaded==true)
			{
				Pass( "The PDP Page is loaded successfully.");
			}
			else
			{
				Fail( "The PDP Page is not loaded.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1970 Issue ." + e.getMessage());
			Exception(" BRM - 1970 Issue." + e.getMessage());
		}	
		
		/*BRM - 1985 Verify that on selecting "back" button the previously visited page is displayed. */
		ChildCreation("BRM - 1985 Verify that on selecting back button the previously visited page is displayed.");
		try
		{
			driver.navigate().back();
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));
			wait.until(ExpectedConditions.visibilityOf(storeTitle));
			if(BNBasicfeature.isElementPresent(storeTitle))
			{
				Pass("selecting back button the previously visited page is displayed");
			}
			else
			{
				Fail("selecting back button the previously visited page is not displayed");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1985 Issue ." + e.getMessage());
			Exception(" BRM - 1985 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1965 Verify that on selecting the sub category in the pancake it should navigate to product list page*/
	public void SubCategory_To_PLP()
	{
		ChildCreation("BRM - 1965 Verify that on selecting the subcategory in the pancake it should navigate to product list page");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(header));
			wait.until(ExpectedConditions.elementToBeClickable(menu));
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(menu));
			if(BNBasicfeature.isElementPresent(menu))
			{
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.visibilityOf(plpMenu1));
				if(BNBasicfeature.isElementPresent(plpMenu1))
				{
					plpMenu1.click();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(plpCategory1));
					if(BNBasicfeature.isElementPresent(plpCategory1))
					{
						plpCategory1.click();
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(plpSubCategory1));
						if(BNBasicfeature.isElementPresent(plpSubCategory1))
						{
							plpSubCategory1.click();
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							wait.until(ExpectedConditions.visibilityOfAllElements(featuredContentLinks));
							if(BNBasicfeature.isListElementPresent(featuredContentLinks))
							{
								Random r = new Random();
								int sel = r.nextInt(featuredContentLinks.size());
								//if(sel<1)
								if(sel>=0)
								{
									sel = 1;
								}
								wait.until(ExpectedConditions.visibilityOf(featuredContentLinks.get(sel)));
								Thread.sleep(100);
								jsclick(featuredContentLinks.get(sel));
								wait.until(ExpectedConditions.visibilityOf(header));
								wait.until(ExpectedConditions.visibilityOf(searchPageProductCountContainer));
								if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
								{
									Pass("On selecting the subcategory in the pancake successfully navigate to product list page");
								}
								else
								{
									Fail("On selecting the subcategory in the pancake not navigate to product list page");
								}
							}
							else
							{
								Fail("Nook_Contentlinks not displayed");
							}								
						}
						else
						{
								Fail("Business category not displayed");
						}		
					}
					else
					{
						Fail("Subjects category not displayed");
					}		
				}
				else
				{
					Fail("Nookbook category not displayed");
				}		
			}
			else
			{
				Fail(" The Menu is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1965 Issue ." + e.getMessage());
			Exception(" BRM - 1965 Issue." + e.getMessage());
		}				
	}
	
	/*BRM - 1937 Verify that PLP page must be displayed as per the classic site/creative */
	/*BRM - 2019 Verify that paginations should be shown on bottom as per the creative*/
	/*BRM - 2025 Verify that Back to Top button is displaying while scroll down the products in product list page*/
	/*BRM - 1984 Verify that on scrolling the upward arrow , the page is scrolled up.*/
	public void PLP_Creative()
	{
		ChildCreation("BRM - 1937 Verify that PLP page must be displayed as per the classic site/creative");
		boolean pagFlag = false;
		boolean backTop = false;
		try
		{
			wait.until(ExpectedConditions.visibilityOf(storeTitle));
			if(BNBasicfeature.isElementPresent(storeTitle))
			{
				Pass("PLP title present");
			}
			else
			{
				Fail("PLP title is not present");
			}					
			
			if(BNBasicfeature.isElementPresent(browseSection))
			{
				Pass("Browse_section present");
			}
			else
			{
				Fail("Browse_section not present");
			}
			
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{
				Pass("product Count details present");
			}
			else
			{
				Fail("product Count details not present");
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterTabCaption))
			{
				Pass("Filter option present");
			}
			else
			{
				Fail("Filter option not present");
			}	
			
			if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
			{
				Pass("Sort option present");									
			}
			else
			{
				Fail("Sort option not present");									
			}
					
			if(BNBasicfeature.isElementPresent(gridViewIcon))
			{
				Pass("GridView option present");
			}
			else
			{
				Fail("GridView option not present");
			}
					
			if(BNBasicfeature.isElementPresent(listViewIcon))
			{
				Pass("ListView option present");
			}
			else
			{
				Fail("ListView option not present");
			}								
			resultContainerDetails();
			int count = searchPrdtTotalCount;
			if(BNBasicfeature.isElementPresent(searchPageProductCountContainer))
			{
				if(count>6)
				{
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(footerSectionContainer))
					{
						BNBasicfeature.scrolldown(footerSectionContainer, driver);
						Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(showBacktoTop))
						{
							Pass("The Show To Top button is displayed.");
							backTop = true;
						}
						else
						{
							Fail("The Show To Top button is not displayed when the Page is scrolled down fully.");
							backTop = false;
						}
					}
					else
					{
						Skip("The Back to top cotainer counld not verified since the count is less than the expected one.");
						backTop = true;
					}
				}
				
				if(count>20)
				{
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						String attr = searchPagePaginationContainer.getAttribute("style");
						if(attr.contains("block"))
						{
							Pass("The Pagination is enabled.",log);
							pagFlag = true;
						}
						else
						{
							Fail("The Pagination is not enabled.",log);
							pagFlag = false;
						}
					}
					else
					{
						Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
						pagFlag = false;
					}
				}
				else
				{
					if(BNBasicfeature.isElementPresent(searchPagePaginationContainer))
					{
						try
						{
							String attr = searchPagePaginationContainer.getAttribute("style");
							if(attr.contains("block"))
							{
								Fail("The Pagination is enabled when the product count is less than or equal to 20.",log);
								pagFlag = false;
							}
							else
							{
								Pass("The Pagination is not enabled.",log);
								pagFlag = false;
							}
						}
						catch(Exception e)
						{
							Pass("The Pagination is not enabled.",log);
							pagFlag = true;
						}
					}
					else
					{
						Fail("No Pagination is enabled eventhough the Product Count is more than 20.");
						pagFlag = false;
					}
				}
			}
									
			if(BNBasicfeature.isElementPresent(footerSectionMailContainer))
			{
				Pass("The Mail Container is displayed.");
				if(BNBasicfeature.isElementPresent(footerSectionMailContainerText))
				{
					String actVal = footerSectionMailContainerText.getText();
					log.add("The Actual value is : " + actVal);
					Pass("The Expected Value matches.",log);
				}
				else
				{
					Fail("The Expected Value does not matches.",log);
				}
			}
			else
			{
				Fail("The Mail Container default text is not displayed.");
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionMailSubmitButton))
			{
				Pass("The Submit button is displayed.");
			}
			else
			{
				Fail("The Submit button is not displayed.");
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionMailEmail))
			{
				Pass("The Email field is displayed.");
			}
			else
			{
				Fail("The Email field is not displayed.");
			}
			
			if(BNBasicfeature.isElementPresent(footerSectionContainer))
			{
				BNBasicfeature.scrolldown(footerSectionContainer, driver);
				if(BNBasicfeature.isElementPresent(footerSectionMyAccount))
				{
					Pass("The My Account Link is displayed.");
					String actVal = footerSectionMyAccount.getText();
					log.add("The Actual value is : " + actVal);
					Pass("The Expected Value matches.",log);
				}
				else
				{
					Fail("The Expected Value does not matches.",log);
				}
			}
			else
			{
				Fail("The My Account Link is not displayed.");
			}
					
			if(BNBasicfeature.isElementPresent(footerSectionSignIn))
			{
				Pass("The Sign In Link is displayed.");
				String actVal = footerSectionSignIn.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionCustomerService))
			{
				Pass("The Customer Service Link is displayed.");
				String actVal = footerSectionCustomerService.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
					
			if(BNBasicfeature.isElementPresent(footerSectionStores))
			{
				Pass("The Stores Link is displayed.");
				String actVal = footerSectionStores.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionTermsandUse))
			{
				Pass("The Terms of Use Link is displayed.");
				String actVal = footerSectionTermsandUse.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionPrivacy))
			{
				Pass("The Privacy Policy Link is displayed.");
				String actVal = footerSectionPrivacy.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
			
			if(BNBasicfeature.isElementPresent(footerSectionCopyright))
			{
				Pass("The Copyright Link is displayed.");
				String actVal = footerSectionCopyright.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
			
			if(BNBasicfeature.isElementPresent(footerSectionTermsandUse1))
			{
				Pass("The Terms and Use Link is displayed.");
				String actVal = footerSectionTermsandUse1.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
				
			if(BNBasicfeature.isElementPresent(footerSectionFullSite))
			{
				Pass("The Full Site Link is displayed.");
				String actVal = footerSectionFullSite.getText();
				log.add("The Actual value is : " + actVal);
				Pass("The Expected Value matches.",log);
			}
			else
			{
				Fail("The Expected Value does not matches.",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1937 Issue ." + e.getMessage());
			Exception(" BRM - 1937 Issue." + e.getMessage());
		}
		
		/*BRM - 2019 Verify that paginations should be shown on bottom as per the creative*/
		ChildCreation("BRM - 2019 Verify that paginartions should be shown on bottom as per the creative");
		try
		{
			log.add (" Refer : 1937 For Log Reports on Pagintion.");
			if(pagFlag==true)
			{
				Pass( "The Pagination Flag is displayed.");
			}
			else
			{
				Fail( "The Pagination Flag is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2019 Issue ." + e.getMessage());
			Exception(" BRM - 2019 Issue." + e.getMessage());
		}	
	
		/*BRM - 1987 Verify that the selected page should be underlined in pagination*/
		ChildCreation("BRM - 1987 Verify that the selected page should be underlined in pagination");
		try
		{
			int currPage = 0;
			String currText = "";
			int val = 0;
			resultContainerDetails();
			val = searchPrdtTotalCount;
			if(val>20)
			{
				if(BNBasicfeature.isElementPresent(searchPagePaginationContainerCurrentPages))
				{
					currPage = Integer.parseInt(searchPagePaginationContainerCurrentPages.getText());
					log.add("The Current page before selection is : " + currPage);
					currText = searchPagePaginationContainerCurrentPages.getCssValue("text-decoration");
					if(currText.contains("underline"))
					{
						Pass("The text is underlined.");
					}
					else
					{
						Fail("The Text is not underlined.");
					}
				}
				else
				{
					Fail("The Current Page is not displayed when there are more than 20 Products.");
				}
			}
			else
			{
				Skip("No pagination is displayed since the total Product count is less than 20.The total product count is : " + val);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1987 Issue ." + e.getMessage());
			Exception(" BRM - 1987 Issue." + e.getMessage());
		}
		
		/*BRM - 2025 Verify that Back to Top button is displaying while scroll down the products in product list page*/
		ChildCreation("BRM - 2025 Verify that Back to Top button is displaying while scroll down the products in product list page");
		try
		{
			if(backTop==true)
			{
				Pass( "The Back To Top button is displayed.");
			}
			else
			{
				Fail( "The Back To Top button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2025 Issue ." + e.getMessage());
			Exception(" BRM - 2025 Issue." + e.getMessage());
		}
		
		/* BRM - 1984 Verify that on scrolling the upward arrow , the page is scrolled up.*/
		ChildCreation(" BRM - 1984 Verify that on scrolling the upward arrow , the page is scrolled up.");
		try
		{
			Actions action  = new Actions(driver);
			BNBasicfeature.scrolldown(footerSectionContainer, driver);
			Thread.sleep(250);
			for(int i=0;i<150;i++)
			{
				action.sendKeys(Keys.ARROW_UP).build().perform();
				try
				{
					wait.until(ExpectedConditions.visibilityOf(storeTitle));		
				}
				catch(Exception e)
				{
					continue;
				}					
			}
			
			if(BNBasicfeature.isElementPresent(storeTitle))
			{
				Pass("On scrolling the upward arrow , the page is scrolled up");
			}
			else
			{
				Fail("On scrolling the upward arrow , the page is not scrolled up");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1984 Issue ." + e.getMessage());
			Exception(" BRM - 1984 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1983 Verify that when the product is selected, the corresponding pdp is displayed. */
	public void product_to_pdp()
	{
		ChildCreation("BRM - 1983 Verify that when the product is selected, the corresponding pdp is displayed");
		String currUrl = "";
		try
		{
			boolean atbfound = false;
			String selprdtname = "",selPrdtid = "";
			currUrl = driver.getCurrentUrl();
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				for(int j = 1; j<=plpPageProductLists.size();j++)
				{
					Thread.sleep(100);
					int count=1;
					atbfound = false;
					do
					{
						Random r = new Random();
						int sel = r.nextInt(plpPageProductLists.size());
						if(sel<1)
						{
							sel = 1;
						}
						
						BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
						Thread.sleep(10);
						WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
						selprdtname = prdtlist.getText();
						log.add("The Selected Product Name is  : " + selprdtname);
						log.add("The Selected Product Name is : " + selprdtname);
						selPrdtid = prdtlist.getAttribute("identifier").toString();
						log.add("The Selected Product ID is  : " + selPrdtid);
						log.add("Selected Product ID : " + selPrdtid);
						atbfound = false;
						jsclick(prdtlist);
						wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
						
						if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
						{
							atbfound = true;
							break;
						}
						else
						{
							count++;
							driver.get(currUrl);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.visibilityOf(header));
							continue;
						}
					}while(atbfound==true||count<10);
				}
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
						
			if(atbfound==true)
			{
				if(BNBasicfeature.isElementPresent(productId))
				{
					String id = productId.getAttribute("identifier").toString();
					log.add("The Actual Product ID is  : " + selPrdtid);
					if(id.equals(selPrdtid))
					{
						Pass("The User is navigated to the expected page.");
					}
					else
					{
						Fail("The User is not navigated to the expected page.");
					}
				}
				else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					boolean idFound = false;
					for(int i = 0;i<pdpPageATBBtns.size();i++)
					{
						String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							idFound = true;
							break;
						}
						else
						{
							idFound = false;
							continue;
						}
					}
						
					if(idFound==true)
					{
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						Fail("The User is not redirected to the user intented page.",log);
					}
				}
				else
				{
					Fail("The product id is not displayed in the Product Page.");
				}
			}
			else
			{
				Fail("The Add to back button is not displayed in the pdpPage.");
			}
			
			driver.get(currUrl);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(header));
		}
		catch(Exception e)
		{
			driver.get(currUrl);
			System.out.println(" BRM - 1983 Issue ." + e.getMessage());
			Exception(" BRM - 1983 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 1986 Verify that on_clicking reviews count link, it should redirect to product details page */
	public void review_to_pdp() throws Exception
	{
		ChildCreation("BRM - 1986 Verify that on_clicking reviews count link, it should redirect to product details page");
		String origUrl = "", currUrl = "";
		if(BNBasicfeature.isElementPresent(gridViewList))
		{
			BNBasicfeature.scrollup(header, driver);
			//Thread.sleep(1000);
			jsclick(listViewIcon);
			wait.until(ExpectedConditions.visibilityOf(listViewList));
			Thread.sleep(10);
		}
		
		boolean atbfound = false;
		String selprdtname = "",selPrdtid = "";
		try
		{
			if(BNBasicfeature.isListElementPresent(plpPageProductLists))
			{
				//Thread.sleep(500);
				int count=1;
				atbfound = false;
				do
				{
					origUrl = driver.getCurrentUrl();
					Random r = new Random();
					int sel = r.nextInt(plpPageProductLists.size());
					if(sel<1)
					{
						sel = 1;
					}
					
					BNBasicfeature.scrolldown(driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]")), driver);
					Thread.sleep(10);
					WebElement prdtlist = driver.findElement(By.xpath("(//*[@class='skMob_productTitle'])["+sel+"]"));
					selprdtname = prdtlist.getText();
					log.add("The Selected Product Name is  : " + selprdtname);
					selPrdtid = prdtlist.getAttribute("identifier").toString();
					log.add("The Selected Product ID is  : " + selPrdtid);
					atbfound = false;
					//WebElement reviewContainer = plpPageProductLists.get(sel-1).findElement(By.xpath("//*[contains(@class,'reviews')]"));
					WebElement reviewContainer = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+sel+"]//*[contains(@class,'reviews')]"));
					if(BNBasicfeature.isElementPresent(reviewContainer))
					{
						Pass("The review container is displayed.");
						try
						{
							WebElement reviewCnt = driver.findElement(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+sel+"]//*[contains(@id,'ZeroRatingStar')]//*[contains(@id,'fullRating')]"));
							WebElement totalreviewContainer = driver.findElement(By.xpath("(//*[@id='productlist']//*[@class='skMob_productListItemOuterCont'])["+sel+"]//*[contains(@class,'ratingContainer')]//*[contains(@class,'RatingCount')]"));
							String reviewCount = reviewCnt.getAttribute("title");
							if(reviewCount.isEmpty())
							{
								Fail("The review count is not displayed.");
							}
							else
							{
								log.add("The displayed review count is : " + reviewCount);
								Pass("The review count is displayed.",log);
								jsclick(totalreviewContainer);
								//Thread.sleep(2000);
								wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
								Thread.sleep(100);
								if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
								{
									atbfound = true;
									break;
								}
								else
								{
									atbfound = false;
									count++;
									driver.navigate().back();
									continue;
								}
							}
						}
						catch(Exception e)
						{
							atbfound = false;
							count++;
							//driver.navigate().back();
							continue;
						}
					}
					else
					{
						Fail("The review container is not displayed.");
					}
				}while(atbfound==true||count<10);
			}
			else
			{
				Fail("The Product list is not displayed.");
			}			
						
			if(atbfound==true)
			{
				boolean idFound = false;
				if(BNBasicfeature.isElementPresent(productId))
				{
					String id = productId.getAttribute("identifier").toString();
					log.add("The Actual Product ID is  : " + selPrdtid);
					if(selPrdtid.contains(id))
					{
						Pass("The User is navigated to the expected page.");
						currUrl = driver.getCurrentUrl();
						idFound = true;
					}
					else
					{
						currUrl = driver.getCurrentUrl();
						if(currUrl.equals(origUrl))
						{
							idFound = false;
						}
						else
						{
							idFound = true;
						}
						
					}
					
					if(idFound==true)
					{
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						Fail("The User is not redirected to the user intented page.",log);
					}
					
					log.add("The Navigated back url is : " + origUrl);
					log.add("The Navigated New url is : " + currUrl);
					if(origUrl.equals(currUrl))
					{
						Fail("The User is not navigated to the Intended page.");
					}
					else
					{
						Pass("The User is not in the Same Page.");
					}
					
					driver.navigate().back();
					//Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
					currUrl = driver.getCurrentUrl();
					
					log.add("The Navigated back url is : " + origUrl);
					log.add("The Navigated back url is : " + currUrl);
					if(origUrl.equals(currUrl))
					{
						Pass("The User is redirected to the original url.",log);
					}
					else
					{
						Fail("The User is not redirected to the original url.",log);
					}
				}
				else if(BNBasicfeature.isListElementPresent(pdpPageATBBtns))
				{
					idFound = false;
					for(int i = 0;i<pdpPageATBBtns.size();i++)
					{
						String id = pdpPageATBBtns.get(i).getAttribute("identifier").toString();
						log.add("The Actual Product ID is  : " + selPrdtid);
						if(id.equals(selPrdtid))
						{
							Pass("The User is navigated to the expected page.");
							idFound = true;
							currUrl = driver.getCurrentUrl();
							break;
						}
						else
						{
							idFound = false;
							continue;
						}
					}
						
					if(idFound==true)
					{
						Pass("The User is redirected to the user intented page.",log);	
					}
					else
					{
						Fail("The User is not redirected to the user intented page.",log);
					}
					
					log.add("The Navigated back url is : " + origUrl);
					log.add("The Navigated New url is : " + currUrl);
					if(origUrl.equals(currUrl))
					{
						Fail("The User is not navigated to the Intended page.");
					}
					else
					{
						Pass("The User is not in the Same Page.");
					}
					
					driver.navigate().back();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
					currUrl = driver.getCurrentUrl();
					
					log.add("The Navigated back url is : " + origUrl);
					log.add("The Navigated back url is : " + currUrl);
					if(origUrl.equals(currUrl))
					{
						Pass("The User is redirected to the original url.",log);
					}
					else
					{
						Fail("The User is not redirected to the original url.",log);
					}
				}
				else
				{
					Fail("The product id is not displayed in the Product Page.");
				}
			}
			else
			{
				Fail("The Add to back button is not displayed in the pdpPage.");
			}
		}
		catch(Exception e)
		{
			driver.get(origUrl);
			System.out.println(" BRM - 1986 Issue ." + e.getMessage());
			Exception(" BRM - 1986 Issue." + e.getMessage());
		}
	}
	
	/*BRM - 1989 Verify that while selecting sort button the sorting options must be displayed in the overlay*/
	public void sortOverlay() throws Exception
	{
		ChildCreation("BRM - 1989 Verify that while selecting sort button the sorting options must be displayed in the overlay");
		String sort = BNBasicfeature.getExcelVal("BRM-1989", sheet, 4);		
		String[] sortOptions = sort.split("\n");
		try
		{
			BNBasicfeature.scrollup(header, driver);
			Thread.sleep(100);
			wait.until(ExpectedConditions.visibilityOf(searchPageSortTabCaption));
			if(BNBasicfeature.isElementPresent(searchPageSortTabCaption))
			{
				Thread.sleep(10);
				enableSort();
				wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
				wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
				Thread.sleep(100);
				boolean optionfound = true;
				wait.until(ExpectedConditions.visibilityOf(searchPageSortOptionFacet));
				if(BNBasicfeature.isElementPresent(searchPageSortOptionFacet))
				{	
					for(int i=1;i<=8;i++) 
					{
						WebElement sort_Overlay = driver.findElement(By.xpath("//*[@class='skMob_sortByOptions']//*[@class='skMob_sortListVal']["+i+"]"));
						String sort_Option = sort_Overlay.getText();
						for(int j =0; j<=sortOptions.length;j++ )
						{
							if(sort_Option.contains(sortOptions[j]))
							{
								Pass("The Sort Overlay sort option is  : " + sort_Option);
								optionfound = true;
								break;
							}
							else
							{
								optionfound = false;
								continue;
							}		
						}
					}
					if(optionfound == true)
					{
						Pass("selecting sort button the sorting options displayed in the overlay");
					}
					else
					{
						Fail("selecting sort button the sorting options not displayed in the overlay");
					}
				}
				else
				{
					Fail( " The Search Facets is not displayed.");
				}
			}
			else
			{
				Fail( "The Search Sort Button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1989 Issue ." + e.getMessage());
			Exception(" BRM - 1989 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1992 Verify that on selecting the 'Newest to Oldest' sort option, the newly arrived products are displayed first in the list. */
	public void sort_new_To_old()
	{
		ChildCreation("BRM - 1992 Verify that on selecting the 'Newest to Oldest' sort option, the newly arrived products are displayed first in the list.");
		try
		{
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(100);
			jsclick(newToOld);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_newOld = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_newOld[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_newOld.toString());
			if(productArray==productArray_newOld)
			{
				Fail("selecting the 'Newest to Oldest' sort option, the newly arrived products are not displayed first in the list",log);
			}
			else
			{
				Pass("selecting the 'Newest to Oldest' sort option, the newly arrived products are displayed first in the list",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1992 Issue ." + e.getMessage());
			Exception(" BRM - 1992 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1991 Verify that on selecting the 'Oldest to Newest' sort option, the newly old  products are displayed first in the list.*/
	public void sort_old_To_new()
	{
		ChildCreation("BRM - 1991 Verify that on selecting the 'Oldest to Newest' sort option, the newly old  products are displayed first in the list.");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				Thread.sleep(5);
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			Thread.sleep(100);
			jsclick(oldToNew);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_oldNew = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));	
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_oldNew[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_oldNew.toString());
			if(productArray==productArray_oldNew)
			{
				Fail("selecting the 'Oldest to Newest' sort option, the old products are not displayed first in the list",log);
			}
			else
			{
				Pass("selecting the 'Oldest to Newest' sort option, the old products are displayed first in the list",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1991 Issue ." + e.getMessage());
			Exception(" BRM - 1991 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 1993 Verify that on selecting the "Price High-Low" option, the products with the high price are displayed first in the list, followed by the products with the low price.*/
	public void sort_high_To_low()
	{
		ChildCreation("BRM - 1993 Verify that on selecting the 'Price High-Low' option, the products with the high price are displayed first in the list, followed by the products with the low price.");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			jsclick(highTolow);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_highLow = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_highLow[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_highLow.toString());
			if(productArray==productArray_highLow)
			{
				Fail("selecting the 'Price High-Low' option, the products with the high price are not displayed first in the list, followed by the products with the low price",log);
			}
			else
			{
				Pass("selecting the 'Price High-Low' option, the products with the high price are displayed first in the list, followed by the products with the low price",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1993 Issue ." + e.getMessage());
			Exception(" BRM - 1993 Issue." + e.getMessage());
		}	
	}

	/*BRM - 1994 Verify that on selecting the "Price Low-High" option, the products with the low price are displayed first followed by the products with the high price*/
	public void sort_low_To_high()
	{	
		ChildCreation("BRM - 1994 Verify that on selecting the 'Price Low-High' option, the products with the low price are displayed first followed by the products with the high price");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			jsclick(ltoH);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_lowHigh = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_lowHigh[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_lowHigh.toString());
			if(productArray==productArray_lowHigh)
			{
				Fail("selecting the 'Price Low-High' option, the products with the low price are not displayed first followed by the products with the high price",log);
			}
			else
			{
				Pass("selecting the 'Price Low-High' option, the products with the low price are displayed first followed by the products with the high price",log);
			}			
		}	
		catch (Exception e)
		{
			System.out.println(" BRM - 1994 Issue ." + e.getMessage());
			Exception(" BRM - 1994 Issue." + e.getMessage());
		}		
	}

	/*BRM - 1998 Verify that on selecting the "Highly Rated" option, the top rated products are displayed first in the list.*/
	public void sort_highlyRated()
	{
		ChildCreation("BRM - 1998 Verify that on selecting the 'Highly Rated' option, the top rated products are displayed first in the list");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			jsclick(highlyRated);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_highlyRated = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_highlyRated[i-1] = product_aftr;
			}	
			log.add( "The Product list after applying the sort was : " + productArray_highlyRated.toString());
			if(productArray==productArray_highlyRated)
			{
				Fail("selecting the 'Highly Rated' option, the top rated products are not displayed first in the list",log);
			}
			else
			{
				Pass("selecting the 'Highly Rated' option, the top rated products are displayed first in the list",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1998 Issue ." + e.getMessage());
			Exception(" BRM - 1998 Issue." + e.getMessage());
		}		
	}

	/*BRM - 1999 Verify that on selecting the "A-Z" option, the products are displayed in the ascending order*/
	public void sort_A_Z()
	{
		ChildCreation("BRM - 1999 Verify that on selecting the 'A-Z' option, the products are displayed in the ascending order");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			jsclick(titleAZ);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_AZ = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_AZ[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_AZ.toString());
			if(productArray==productArray_AZ)
			{
				Fail("selecting the 'A-Z' option, the products are not displayed in the ascending order",log);
			}
			else
			{
				Pass("selecting the 'A-Z' option, the products are displayed in the ascending order",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1999 Issue ." + e.getMessage());
			Exception(" BRM - 1999 Issue." + e.getMessage());
		}		
	}

	/*BRM - 2000 Verify that on selecting the 'Z-A' option, the products are displayed in the descending order*/
	public void sort_Z_A()
	{
		ChildCreation("BRM - 2000 Verify that on selecting the 'Z-A' option, the products are displayed in the descending order");
		try
		{
			//action.moveToElement(searchPageSortButton).click().build().perform();	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			log.add( "The Product list before applying the sort was : " + productArray.toString());
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			jsclick(titleZA);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			int aftr_size = plpPageProductLists.size();
			String[] productArray_ZA = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				wait.until(ExpectedConditions.visibilityOf(plpPageProductLists.get(i-1)));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")));
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont'])["+i+"]//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_ZA[i-1] = product_aftr;
			}
			log.add( "The Product list after applying the sort was : " + productArray_ZA.toString());
			if(productArray==productArray_ZA)
			{
				Fail("selecting the 'Z-A' option, the products are not displayed in the descending order",log);
			}
			else
			{
				Pass("selecting the 'Z-A' option, the products are displayed in the descending order",log);
			}			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2000 Issue ." + e.getMessage());
			Exception(" BRM - 2000 Issue." + e.getMessage());
		}		
	}
	
	/*BRM - 2001 Verify that on navigating to other pages and coming back, the selected option is maintained on the Product List page/search result page */
	public void navigate_back()
	{
		ChildCreation("BRM - 2001 Verify that on navigating to other pages and coming back, the selected option is maintained on the Product List page/search result page");
		try
		{
			Thread.sleep(10);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			
			String old_selection = selectedSort.getText();
			jsclick(bestSellers);
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(100);
			
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(header));
			Thread.sleep(500);
			enableSort();
			wait.until(ExpectedConditions.attributeContains(searchPageSortOptionFacet, "style", "block"));
			wait.until(ExpectedConditions.visibilityOfAllElements(searchPageSortFacetsLists));
			String current_selection = selectedSort.getText();
			log.add( "The old Selection was : " + old_selection);
			log.add( "The new Selection is : " + current_selection);
			if(old_selection.equalsIgnoreCase(current_selection))
			{
				Pass("On navigating to other pages and coming back, the selected option is maintained on the Product List page/search result page",log);
			}
			else
			{
				Fail("On navigating to other pages and coming back, the selected option is not maintained on the Product List page/search result page",log);
			}
			
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2001 Issue ." + e.getMessage());
			Exception(" BRM - 2001 Issue." + e.getMessage());
		}			
	}
	
	/*BRM - 2009 Verify that Filters should be in dropdown as per the creative*/
	public void filterCreative()
	{
		ChildCreation("BRM - 2009 Verify that Filters should be in dropdown as per the creative");
		try
		{
			String color = BNBasicfeature.getExcelVal("BRM-2009", sheet, 2);
			String[] colorOptions = color.split("\n");
			wait.until(ExpectedConditions.visibilityOf(searchPageFilterButton));		
			if(BNBasicfeature.isElementPresent(searchPageFilterButton))		
			{
				filterEnable();
				wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
				Thread.sleep(100);
				String filter_color = driver.findElement(By.xpath("//*[@class='skMob_filterBtnText opened']")).getCssValue("background-color").replace("1px solid", "");
				Color colorhxcnvt = Color.fromString(filter_color);
				String hexCode = colorhxcnvt.asHex();
				if(hexCode.equals(colorOptions[0]))
				{						
					Pass("Filter background is matches the creative");
				}
				else
				{
					Fail("Filter background is not matches the creative");
				}		
				Thread.sleep(100);
				String text_color = driver.findElement(By.xpath("//*[@class='skMob_plpSelFacetsLabel']")).getCssValue("color").replace("1px solid", "");
				Color colorcnvt = Color.fromString(text_color);
				String hxCode = colorcnvt.asHex();
				if(hxCode.equals(colorOptions[1]))
				{						
					Pass("Filter text color is matches the creative");
				}
				else
				{
					Fail("Filter text color  is not matches the creative");
				}	
				Thread.sleep(100);	
				String accord_color = accordFilter.getCssValue("color").replace("1px solid", "");
				Color colorhcnvt = Color.fromString(accord_color);
				String hxCodecolor = colorhcnvt.asHex();
				if(hxCodecolor.equals(colorOptions[2]))
				{						
					Pass("Filter Accordian color  is matches the creative");
				}
				else
				{
					Fail("Filter Accordian color is not matches the creative");
				}	
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2009 Issue ." + e.getMessage());
			Exception(" BRM - 2009 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 2012 Verify that all the subcategories in filters are shown one by one as accordians with + option */
	public void filterAccord()
	{
		ChildCreation("BRM - 2012 Verify that all the subcategories in filters are shown one by one as accordians with + option");
		int i = 0;
		boolean facetOpen = false;
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			wait.until(ExpectedConditions.visibilityOf(header));
			if(BNBasicfeature.isElementPresent(plpSearchPageFilterContainer))
			{
				Pass("The Filter Section Container is displayed.");
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					filterEnable();
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facet list is displayed.");
								for(i = 0; i<searchPageFilterFacetsLists.size();i++)
								{
									String facetName = searchPageFilterFacetsName.get(i).getText();
									if(facetName.isEmpty())
									{
										Fail("The Facet Name is not displayed.");
									}
									else
									{
										log.add("The found facet is : " + facetName);
										Pass("The Facet Name is displayed.",log);
									}
									
									if(BNBasicfeature.isElementPresent(searchPageFilterFacetsExpandIcon.get(i)))
									{
										Pass("The Facet Expand Icon is displayed.");
									}
									else
									{
										Fail("The Facet Expand Icon is not displayed.");
									}
								}
							}
							else
							{
								Fail("The Facet list is not displayed in the filter.");
							}
						}
						else
						{
							Fail("The Facet list Container is not displayed in the filter.");
						}
					}
					else
					{
						Fail("The Facet is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}
			else
			{
				Fail("The Search Filter Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2012 Issue ." + e.getMessage());
			Exception(" BRM - 2012 Issue." + e.getMessage());
		}
	}
	
	/* BRM - 2013 Verify that on clicking + option, the options from the selected category must be expanded*/
	/* BRM - 2014 Verify that on clicking - option, the expanded options must be closed	*/
	/* BRM - 1955 Verify that on selecting the category the accordion must be expanded and the subcategories must be displayed in the filter*/
	/* BRM - 2017 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown*/
	public void filterAccordExpand()
	{
		ChildCreation("BRM - 2013 Verify that on clicking + option, the options from the selected category must be expanded");
		boolean brm2014 = false;
		boolean brm1955 = false;
		boolean brm2017 = false;
		try
		{
			boolean facetVal = false;
			do
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				BNBasicfeature.scrollup(header, driver);
				
				if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
				{
					jsclick(searchPageFilterSortMask);
					BNBasicfeature.scrollup(header, driver);
					brm2017 = true;
					Thread.sleep(10);
				}
				
				if(BNBasicfeature.isElementPresent(searchPageFilterButton))
				{
					Pass("The Filter button is displayed.");
					filterEnable();
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
					Thread.sleep(100);
					boolean facetOpen = searchPageFilterFacets.getAttribute("style").contains("block");
					if(facetOpen==true)
					{
						if(BNBasicfeature.isElementPresent(searchPageFilterFacets))
						{
							Pass("The Filter Facets is displayed.");
							if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsLists))
							{
								Pass("The Facets List is displayed.");
								for(int i = 0;i<searchPageFilterFacetsLists.size();i++)
								{
									jsclick(searchPageFilterFacetsLists.get(i));
									if(BNBasicfeature.isListElementPresent(searchPageFilterFacetsValue))
									{
										wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "block"));
										facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("block");
										//Thread.sleep(500);
										if(facetVal==true)
										{
											Pass("The Facet is opened in the filter drop down.");
											brm1955 = true;
											for(int j = 0; j<searchPageFilterFacetsNameExpanded.size();j++)
											{
												jsclick(searchPageFilterFacetsNameExpanded.get(j));
												wait.until(ExpectedConditions.attributeContains(searchPageFilterFacetsValue.get(i), "style", "none"));
											}
											facetVal = searchPageFilterFacetsValue.get(i).getAttribute("style").contains("none");
											Thread.sleep(100);
											if(facetVal==true)
											{
												Pass("The Facet is closed.");
												brm2014 = true;
											}
											else
											{
												Fail("The Facet is not closed.");
												brm2014 = false;
											}
										}
										else
										{
											Fail("The Facet Val was not opened.");
											brm2014 = false;
											brm1955 = false;
										}
									}
									else
									{
										Fail("The Facets Value is not displayed.");
										brm2014 = false;
										brm1955 = false;
									}
								}
							}
							else
							{
								Fail("The Facets List is not displayed.");
							}
						}
						else
						{
							Fail("The Filter Facets is not displayed.");
						}
					}
					else
					{
						Fail("The Filter Facets is not opened.");
					}
				}
				else
				{
					Fail("The Filter button is not displayed.");
				}
			}while(facetVal!=true);
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2013 Issue ." + e.getMessage());
			Exception(" BRM - 2013 Issue." + e.getMessage());
		}
		
		/*BRM-1955 Verify that on selecting the category the accordion must be expanded and the subcategories must be displayed in the filter*/
		ChildCreation(" BRM - 1955 Verify that on selecting the category the accordion must be expanded and the subcategories must be displayed in the filter");
		try
		{
			log.add( " Refer : BRM - 2013 Log for details.");
			if(brm1955==true)
			{
				Pass( " The Subcategories in the Facets are displayed.",log);
			}
			else
			{
				Fail( " The Subcategories in the Facets are not displayed.",log);
			}
			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 1955 Issue ." + e.getMessage());
			Exception(" BRM - 1955 Issue." + e.getMessage());
		}
		
		/* BRM - 2014 Verify that on clicking - option, the expanded options must be closed	*/
		ChildCreation("BRM - 2014 Verify that on clicking - option, the expanded options must be closed");
		try
		{
			log.add( " Refer : BRM - 2013 Log for details.");
			if(brm2014==true)
			{
				Pass( " The Facets are Closed.",log);
			}
			else
			{
				Fail( " The Facets are not Closed.",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2014 Issue ." + e.getMessage());
			Exception(" BRM - 2014 Issue." + e.getMessage());
		}
		
		/* BRM - 2017 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown*/
		ChildCreation("BRM - 2017 Verify that when we enable Filter dropdown the background mask must be shown behind the filter dropdown");
		try
		{
			if(brm2017==true)
			{
				Pass( " The Mask is displayed when filter tab is enabled.",log);
			}
			else
			{
				Fail( " The Mask is not displayed when filter tab is enabled..",log);
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2017 Issue ." + e.getMessage());
			Exception(" BRM - 2017 Issue." + e.getMessage());
		}
	}
	
	/*BRM - 2003 Verify that on selecting close button then the selected attributes in the filter should be cleared */
	public void filterClear()
	{
		ChildCreation("BRM - 2003 Verify that on selecting close button then the selected attributes in the filter should be cleared");
		try
		{
			wait.until(ExpectedConditions.visibilityOf(filterClose));
			if(BNBasicfeature.isElementPresent(filterClose))		
			{
				//String facet = facets.getText();
				jsclick(facets);
				/*action.moveToElement(facets).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(searchPageFilterButton));
				jsclick(searchPageFilterButton);
				String facet_new =facets.getText();			
				if(!facet.equalsIgnoreCase(facet_new))
				{
					Pass("on selecting close button the selected attributes in the filter has cleared");
				}
				else
				{
					Fail("on selecting close button the selected attributes in the filter has not cleared");
				}*/
				wait.until(ExpectedConditions.visibilityOfAllElements(featuredContentLinks));
				if(BNBasicfeature.isListElementPresent(featuredContentLinks))
				{
					Pass( "The User was navigated to the Feature Link Page.");
				}
				else
				{
					Fail( "The User was not navigated to the featured link page.");
				}
				//jsclick(filterClose);
			}
			else
			{
				Fail( "The Close Button in the Filter is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2003 Issue ." + e.getMessage());
			Exception(" BRM - 2003 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 2004 Verify that on selecting the close symbol then the filter page must be closed and the product list page must be refreshed again with updated details */
	public void filterRefresh()
	{
		ChildCreation("BRM - 2004 Verify that on selecting the close symbol then the filter page must be closed and the product list page must be refreshed again with updated details");
		try
		{
			/*Actions action = new Actions(driver);	
			int size = plpPageProductLists.size();
			String[] productArray = new String[size];
			for(int i=1;i<=size;i++)
			{
				String product = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray[i-1] = product;				
			}
			action.moveToElement(searchPageFilterButton).click().build().perform();	
			int size1 = searchPageFilterSelectedFacetsValue.size();
			Random rand = new Random();			
			int j = rand.nextInt(size1);			
			if(j==0)
			{
				j=j+1;
			}
			WebElement facet_remove = driver.findElement(By.xpath("(//*[@class='skMob_plpSelFacet'])["+(j+1)+"]"));
			action.moveToElement(facet_remove).click().build().perform();
			wait.until(ExpectedConditions.visibilityOf(searchPageFilterButton));
			int aftr_size = plpPageProductLists.size();
			String[] productArray_ZA = new String[aftr_size];
			for(int i=1;i<=aftr_size;i++)
			{
				String product_aftr = driver.findElement(By.xpath("(//*[@class='skMob_productListItemOuterCont']["+i+"])//*[@sctype='scLink']")).getAttribute("identifier");
				productArray_ZA[i-1] = product_aftr;
			}
			if(productArray==productArray_ZA)
			{
				Fail("on selecting the close symbol the filter page not closed and the product list page not refreshed again with updated details");
			}
			else
			{
				Pass("on selecting the close symbol the filter page closed and the product list page refreshed again with updated details");
			}*/
			wait.until(ExpectedConditions.visibilityOfAllElements(featuredContentLinks));
			if(BNBasicfeature.isListElementPresent(featuredContentLinks))
			{
				Pass( "The User was navigated to the Feature Link Page.");
			}
			else
			{
				Fail( "The User was not navigated to the featured link page.");
			}
			
			driver.navigate().back();
			wait.until(ExpectedConditions.visibilityOf(header));
			wait.until(ExpectedConditions.visibilityOf(plpSearchPageFilterContainer));
			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2004 Issue ." + e.getMessage());
			Exception(" BRM - 2004 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 2007 Verify that Apply the multiple filters in mobile web & cross check the product count with classic site*/
	public void filterApply()
	{
		ChildCreation("BRM - 2007 Verify that Apply the multiple filters in mobile web & cross check the product count with classic site");
		try
		{
			Integer modifiedCount = 0;
			Integer ocount = null;
			resultContainerDetails();
			ocount = searchPrdtTotalCount;
			boolean facetApplied = false;
			boolean applyFilter = applyFilter();
			if(applyFilter==true)
			{
				facetApplied = facetApplied();
				if(facetApplied==true)
				{
					log.add("The Selected Facet Name was : " + selFacVal);
					log.add("The Set Facet Name is : " + setFacVal);
					if(facetFound==true)
					{
						Pass("The Set facet is displayed.",log);
					}
					else
					{
						Fail("The Set facet is not displayed.",log);
					}
					
					resultContainerDetails();
					modifiedCount = searchPrdtTotalCount;
					log.add("The Modified filtered Count is  : " + modifiedCount);
					log.add("The Set Facet Value was : " + selFacVal);
					
					log.add("The Original Count before applying the filter is  : " + ocount);
					log.add("The Set Facet Value was : " + selFacVal);
					
					if(ocount>=modifiedCount)
					{
						Pass("The Filtered Count is updated correctly.",log);
					}
					else
					{
						Fail("The Filtered Count is not updated correctly.",log);
					}
				}
				else
				{
					Fail("The Facet is not applied.",log);
				}
			}
			else
			{
				Fail("The Facets is not selected.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2007 Issue ." + e.getMessage());
			Exception(" BRM - 2007 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 2011 Verify that close X mark must be shown near the selected filters*/
	public void filterClose()
	{
		ChildCreation(" BRM - 2011 Verify that close X mark must be shown near the selected filters");
		try
		{
			String close = BNBasicfeature.getExcelVal("BRM-2011", sheet, 3);
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			wait.until(ExpectedConditions.visibilityOf(searchPageFilterButton));
			if(BNBasicfeature.isElementPresent(searchPageFilterButton))		
			{
				filterEnable();
				wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));
				//jsclick(searchPageFilterButton);
				WebElement switchLabel = driver.findElement(By.cssSelector(".skMob_plpSelFacet"));
				String script = ((JavascriptExecutor)driver).executeScript("return window.getComputedStyle(arguments[0], '::before').getPropertyValue('content');",switchLabel).toString();
				if(script.contains(close))
				{
					Pass("close X mark shown near the selected filters");
				}
				else
				{
					Fail("close X mark not shown near the selected filters");
				}
			}
			else
			{
				Fail( " The Filter Button is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2011 Issue ." + e.getMessage());
			Exception(" BRM - 2011 Issue." + e.getMessage());
		}	
	}
	
	/*BRM - 2018 Verify that make sure the Filter and sort buttons are in green color as per the creative*/
	public void flterSortColor()
	{
		ChildCreation("BRM - 2018 Verify that make sure the Filter and sort buttons are in green color as per the creative");
		try
		{
			String color = BNBasicfeature.getExcelVal("BRM-2018", sheet, 2);
			String filter_color = searchPageFilterButton.getCssValue("color").replace("1px solid", "");
			Color colorhcnvt = Color.fromString(filter_color);
			String hxCodecolor = colorhcnvt.asHex();
			if(hxCodecolor.equals(color))
			{						
				Pass("Filter button in green color as per the creative");
			}
			else
			{
				Fail("Filter button is not in green color as per the creative");
			}	
			/*String sort_color = searchPageSortButton.getCssValue("color").replace("1px solid", "");
			Color colorcnvt = Color.fromString(sort_color);
			String hxCodecolour = colorcnvt.asHex();
			if(hxCodecolour.equals(color))
			{						
				Pass("sort button in green color as per the creative");
			}
			else
			{
				Fail("sort button is not in green color as per the creative");
			}	*/			
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2018 Issue ." + e.getMessage());
			Exception(" BRM - 2018 Issue." + e.getMessage());
		}	
	}
	
	/* BRM - 2094 Respective PDP page is  getting redirected, if there is no more filter results for the selected filter*/
	public void filterToPDP()
	{
		ChildCreation("BRM - 2094 Respective PDP page is  getting redirected, if there is no more filter results for the selected filter");
		try
		{
			if(BNBasicfeature.isElementPresent(searchPageFilterSortMask))
			{
				jsclick(searchPageFilterSortMask);
				BNBasicfeature.scrollup(header, driver);
				Thread.sleep(10);
			}
			
			String keyword = BNBasicfeature.getExcelVal("BRM-2094", sheet, 1);
			//Actions action = new Actions(driver);	
			wait.until(ExpectedConditions.visibilityOf(searchIcon));
			if(BNBasicfeature.isElementPresent(searchIcon))
			{
				jsclick(searchIcon);
				wait.until(ExpectedConditions.visibilityOf(searchContainer));
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(searchContainer))
				{
					act.moveToElement(searchContainer).click().sendKeys(keyword).sendKeys(Keys.ENTER).build().perform();
					Thread.sleep(1000);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
					filterEnable();
					wait.until(ExpectedConditions.attributeContains(searchPageFilterFacets, "style", "block"));	
					wait.until(ExpectedConditions.visibilityOf(accord));
					jsclick(accord);
					wait.until(ExpectedConditions.visibilityOf(accord1));
					jsclick(accord1);
					wait.until(ExpectedConditions.visibilityOf(pdpPageAddToWishListContainer));
					if(BNBasicfeature.isElementPresent(pdpPageAddToWishListContainer))
					{	
						Pass("PDP page is getting redirected, if there is no more filter results for the selected filter");
					}
					else
					{
						Fail("PDP page is not getting redirected, if there is no more filter results for the selected filter");
					}
				}
				else
				{
					Fail( "The Filter Button is not displayed.");		
				}
			}
			else
			{
				Fail( "The Search Icon is not displayed.");
			}
		}
		catch (Exception e)
		{
			System.out.println(" BRM - 2094 Issue ." + e.getMessage());
			Exception(" BRM - 2094 Issue." + e.getMessage());
		}	
	}
}
	
