package bnPageObjects;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.jsoup.helper.StringUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import commonFiles.BNBasicfeature;
import barnesnoble.BNMyAccount;
import bnConfig.BNXpaths;

public class BNMyAccObj extends BNMyAccount implements BNXpaths
{
	WebDriver driver;
	WebDriverWait wait;
	HSSFSheet sheet;
	static boolean manageAcc = false, signedIn = false, noOrder = false, orderPresent = false, upperCase = false, membershipPresent = false, orderHistoryLoaded = false, myAccntPage = false, manageAccountLoaded = false, titFound = false, manageAccNameUpdt = false, contentUpdated = false, paymetsPageLoaded = false, addNewPay = false, shipPageLoaded = false, addNewShip = false, gftCardPage = false;
	static int getFormCnt = 0;            
	static String actTitle = "", myAccountUrl = "", manageAccUrl = "";
	static WebElement accElement = null;
	
	public BNMyAccObj(WebDriver driver,HSSFSheet sheet)
	{
		this.driver = driver;
		this.sheet = sheet;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, timeout);
	}
	
	@FindBy(xpath = ""+headerr+"")
	WebElement header;
	
	@FindBy(xpath = ""+menuu+"")
	WebElement menu;
	
	@FindBy(xpath = ""+searchIconn+"")
	WebElement searchIcon;
	
	@FindBy(xpath = ""+bnLogoo+"")
	WebElement bnLogo;
	
	@FindBy(xpath = ""+bagIconn+"")
	WebElement bagIcon;
	
	@FindBy(xpath =""+storeLocatorr+"")
	WebElement storeLocator;
	
	@FindBy(xpath = ""+promoCarousell+"")
	WebElement promoCarousel;
	
	@FindBy (xpath = ""+shareFBB+"")
	WebElement shareFB;
	
	@FindBy (xpath = ""+shareTwitterr+"")
	WebElement shareTwitter;
	
	@FindBy (xpath = ""+shareInstaa+"")
	WebElement shareInsta;
	
	@FindBy (xpath = ""+sharePinrestt+"")
	WebElement sharePinrest;
	
	@FindBy (xpath = ""+myAccountFooterContainerr+"")
	WebElement myAccountFooterContainer;
	
	@FindBy (xpath = ""+footerSectionMailContainerr+"")
	WebElement footerSectionMailContainer;
	
	@FindBy (xpath = ""+footerSectionMailContainerTextt+"")
	WebElement footerSectionMailContainerText;
	
	@FindBy (xpath = ""+footerSectionMailSubmitButtonn+"")
	WebElement footerSectionMailSubmitButton;
	
	@FindBy (xpath = ""+footerSectionMailEmaill+"")
	WebElement footerSectionMailEmail;
	
	@FindBy (xpath = ""+myAccountFooterMyAccountt+"")
	WebElement myAccountFooterMyAccount;
	
	@FindBy (xpath = ""+footerSectionSignInn+"")
	WebElement footerSectionSignIn;
	
	@FindBy (xpath = ""+footerSectionCustomerServicee+"")
	WebElement footerSectionCustomerService;
	
	@FindBy (xpath = ""+footerSectionStoress+"")
	WebElement footerSectionStores;
	
	@FindBy (xpath = ""+footerSectionTermsandUsee+"")
	WebElement footerSectionTermsandUse;
	
	@FindBy (xpath = ""+footerSectionPrivacyy+"")
	WebElement footerSectionPrivacy;
	
	@FindBy (xpath = ""+footerSectionCopyrightt+"")
	WebElement footerSectionCopyright;
	
	@FindBy (xpath = ""+footerSectionTermsandUsee1+"")
	WebElement footerSectionTermsandUse1;
	
	@FindBy (xpath = ""+footerSectionFullSitee+"")
	WebElement footerSectionFullSite;
	
	@FindBy(xpath = ""+menuListss+"")
	WebElement menuLists;

	@FindBy(xpath = ""+myAccountt+"")
	WebElement myAccount;
	
	@FindBy(xpath = ""+manageAccountt+"")
	WebElement manageAccount;
	
	@FindBy(xpath = ""+myAccountPageTitlee+"")
	WebElement myAccountPageTitle;
	
	@FindBy(xpath = ""+myAccountMyOrderPageTitlee+"")
	WebElement myAccountMyOrderPageTitle;
	
	@FindBy(xpath = ""+myAccountAccountSettingsPageTitlee+"")
	WebElement myAccountAccountSettingsPageTitle;
	
	@FindBy(xpath = ""+myAccountPageTitlee1+"")
	WebElement myAccountPageTitle1;
	
	@FindBy(xpath = ""+orderContainerTitlee+"")
	WebElement orderContainerTitle;
	
	@FindBy(xpath = ""+emailAddresss+"")
	WebElement emailAddress;
	
	@FindBy(xpath = ""+emailAddressTxtt+"")
	WebElement emailAddressTxt;
	
	@FindBy(xpath = ""+orderNumberr+"")
	WebElement orderNumber;
	
	@FindBy(xpath = ""+orderNumberTxtt+"")
	WebElement orderNumberTxt;
	
	@FindBy(xpath = ""+trackOrderBtnn+"")
	WebElement trackOrderBtn;
	
	@FindBy(xpath = ""+orderErrorr+"")
	WebElement orderError;
	
	@FindBy(xpath = ""+signInToAccountLinkk+"")
	WebElement signInToAccountLink;
	
	@FindBy(xpath = ""+pgetitlee+"")
	WebElement pgetitle;
	
	@FindBy(xpath = ""+myAccSignInPageframee+"") 
	WebElement myAccSignInPageframe;
	
	@FindBy(xpath = ""+signInToAccountLinkk1+"")
	WebElement signInToAccountLink1;
	
	@FindBy(xpath = ""+emailIdd+"")
	WebElement emailId;
	
	@FindBy(xpath = ""+passwordd+"")
	WebElement password;
	
	@FindBy(xpath = ""+secureSignInn+"")
	WebElement secureSignIn;
	
	@FindBy(xpath = ""+footerSignInn+"")
	WebElement footerSignIn;
	
	@FindBy(xpath = ""+myAccFormContainerr+"")
	WebElement myAccFormContainer;
	
	@FindBy(xpath = ""+myAccountMembershipp+"")
	WebElement myAccountMembership;
	
	@FindBy(xpath = ""+myAccountMembershipp1+"")
	WebElement myAccountMembership1;
	
	@FindBy(xpath = ""+orderContainerOrderSortt+"")
	WebElement orderContainerOrderSort;
	
	@FindBy(xpath = ""+orderContainerFilterSearchContainerr+"")
	WebElement orderContainerFilterSearchContainer;
	
	@FindBy(xpath = ""+orderContainerFilterSearchContainerDefaultTextt+"")
	WebElement orderContainerFilterSearchContainerDefaultText;
	
	@FindBy(xpath = ""+orderContainerFilterSearchContainerFindBtnn+"")
	WebElement orderContainerFilterSearchContainerFindBtn;
	
	@FindBy(xpath = ""+orderContainerNoOrderss+"")
	WebElement orderContainerNoOrders;
	
	@FindBy(xpath = ""+orderContainerOrderss+"")
	WebElement orderContainerOrders;
	
	@FindBy(xpath = ""+orderContainerOrderNumberr+"")
	WebElement orderContainerOrderNumber;
	
	@FindBy(xpath = ""+accountSettingsChangeNameFNamee+"")
	WebElement accountSettingsChangeNameFName;
	
	@FindBy(xpath = ""+accountSettingsChangeNameFNameDefaultTxtt+"")
	WebElement accountSettingsChangeNameFNameDefaultTxt;
	
	@FindBy(xpath = ""+accountSettingsChangeNameLNamee+"")
	WebElement accountSettingsChangeNameLName;
	
	@FindBy(xpath = ""+accountSettingsChangeNameLNameDefaultTxtt+"")
	WebElement accountSettingsChangeNameLNameDefaultTxt;
	
	@FindBy(xpath = ""+accountSettingsChangeNameSubmitBtnn+"")
	WebElement accountSettingsChangeNameSubmitBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeNameCancelBtnn+"")
	WebElement accountSettingsChangeNameCancelBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeNameErrorr+"")
	WebElement accountSettingsChangeNameError;
	
	@FindBy(xpath = ""+accountSettingsChangeNameSuccessPageTitlee+"")
	WebElement accountSettingsChangeNameSuccessPageTitle;
	
	@FindBy(xpath = ""+accountSettingsChangeNameSuccessPagee+"")
	WebElement accountSettingsChangeNameSuccessPage;
	
	@FindBy(xpath = ""+accountSettingsChangeNameSuccessCloseBtnn+"")
	WebElement accountSettingsChangeNameSuccessCloseBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeNewEmaill+"")
	WebElement accountSettingsChangeNewEmail;
	
	@FindBy(xpath = ""+accountSettingsChangeCnfEmaill+"")
	WebElement accountSettingsChangeCnfEmail;
	
	@FindBy(xpath = ""+accountSettingsChangePasss+"")
	WebElement accountSettingsChangePass;
	
	@FindBy(xpath = ""+accountSettingsChangeEmailSubmitBtnn+"")
	WebElement accountSettingsChangeEmailSubmitBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeEmailCancelBtnn+"")
	WebElement accountSettingsChangeEmailCancelBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeCurrentEmailContainerr+"")
	WebElement accountSettingsChangeCurrentEmailContainer;
	
	@FindBy(xpath = ""+accountSettingsChangeUpdateEmailAlertt+"")
	WebElement accountSettingsChangeUpdateEmailAlert;
	
	@FindBy(xpath = ""+accountSettingsChangeUpdateEmailTitlee+"")
	WebElement accountSettingsChangeUpdateEmailTitle;
	
	@FindBy(xpath = ""+accountSettingsChangeEmailChangeSubmitBtnn+"")
	WebElement accountSettingsChangeEmailChangeSubmitBtn;
	
	@FindBy(xpath = ""+accountSettingsChangeUpdatePasswordAlertt+"")
	WebElement accountSettingsChangeUpdatePasswordAlert;
	
	@FindBy(xpath = ""+changeCurrPasss+"")
	WebElement changeCurrPass;
	
	@FindBy(xpath = ""+changeCurrPassTextt+"")
	WebElement changeCurrPassText;
	
	@FindBy(xpath = ""+contenPassFormm+"")
	WebElement contenPassForm;
	
	@FindBy(xpath = ""+changeNewPasss+"")
	WebElement changeNewPass;

	@FindBy(xpath = ""+changeNewPassTextt+"")
	WebElement changeNewPassText;
	
	@FindBy(xpath = ""+changeConfPasss+"")
	WebElement changeConfPass;
	
	@FindBy(xpath = ""+changeConfPassTextt+"")
	WebElement changeConfPassText;
	
	@FindBy(xpath = ""+changePassSubmitBtnn+"")
	WebElement changePassSubmitBtn;
	
	@FindBy(xpath = ""+changePassCancelBtnn+"")
	WebElement changePassCancelBtn;
	
	@FindBy(xpath = ""+changePassRuless+"")
	WebElement changePassRules;
	
	@FindBy(xpath = ""+changePassRulesExpandd+"")
	WebElement changePassRulesExpand;
	
	@FindBy(xpath = ""+changePassErrorContainerr+"")
	WebElement changePassErrorContainer;
	
	@FindBy(xpath = ""+changeSecQuesDDD+"")
	WebElement changeSecQuesDD;
	
	@FindBy(xpath = ""+changeSecQuesTextt+"")
	WebElement changeSecQuesText;
	
	@FindBy(xpath = ""+changeSecQuesAnss+"")
	WebElement changeSecQuesAns;
	
	@FindBy(xpath = ""+changeSecAnsTextt+"")
	WebElement changeSecAnsText;
	
	@FindBy(xpath = ""+changeSecQuesSaveBtnn+"")
	WebElement changeSecQuesSaveBtn;
	
	@FindBy(xpath = ""+changeSecQuesCancelBtnn+"")
	WebElement changeSecQuesCancelBtn;
	
	@FindBy(xpath = ""+changeSecQuesErrorContainerr+"")
	WebElement changeSecQuesErrorContainer;
	
	@FindBy(xpath = ""+contenSettingsFormm+"")
	WebElement contenSettingsForm;
	
	@FindBy(xpath = ""+changeContentSettingSubmitBtnn+"")
	WebElement changeContentSettingSubmitBtn;
	
	@FindBy(xpath = ""+changeContentSettingCancelBtnn+"")
	WebElement changeContentSettingCancelBtn;
	
	@FindBy(xpath = ""+instantPurchaseSettingsFormm+"")
	WebElement instantPurchaseSettingsForm;
	
	@FindBy(xpath = ""+changeInstantPurchaseSubmitBtnn+"")
	WebElement changeInstantPurchaseSubmitBtn;
	
	@FindBy(xpath = ""+changeInstantPurchaseCancelBtnn+"")
	WebElement changeInstantPurchaseCancelBtn;
	
	@FindBy(xpath = ""+defaultPaymentFormm+"")
	WebElement defaultPaymentForm;
	
	@FindBy(xpath = ""+editPayButtonss+"")
	WebElement editPayButtons;
	
	@FindBy(xpath = ""+editAddressButtonss+"")
	WebElement editAddressButtons;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentt+"")
	WebElement paymentsDefaultPayment;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentAddressEditt+"")
	WebElement paymentsDefaultPaymentAddressEdit;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentAddressRemovee+"")
	WebElement paymentsDefaultPaymentAddressRemove;
	
	@FindBy(xpath = ""+paymentsDefaultAddressDeletee+"")
	WebElement paymentsDefaultAddressDelete;
	
	@FindBy(xpath = ""+changeDefaultPayRemoveConfirmTextt+"")
	WebElement changeDefaultPayRemoveConfirmText;
	
	@FindBy(xpath = ""+changeDefaultPayRemoveCancelBtnn+"")
	WebElement changeDefaultPayRemoveCancelBtn;
	
	@FindBy(xpath = ""+paymentsMethodAddNewPaymentt+"")
	WebElement paymentsMethodAddNewPayment;
	
	@FindBy(xpath = ""+paymentsMethodAddNewPaymentPagee+"")
	WebElement paymentsMethodAddNewPaymentPage;
	
	@FindBy(xpath = ""+newccNumberr+"")
	WebElement newccNumber;
	
	@FindBy(xpath = ""+newccNumberDefTextt+"")
	WebElement newccNumberDefText;
	
	@FindBy(xpath = ""+newccNamee+"")
	WebElement newccName;
	
	@FindBy(xpath = ""+newccNameDefTextt+"")
	WebElement newccNameDefText;
	
	@FindBy(xpath = ""+newccMonthh+"")
	WebElement newccMonth;
	
	@FindBy(xpath = ""+newccMonthDefTextt+"")
	WebElement newccMonthDefText;
	
	@FindBy(xpath = ""+newccYearr+"")
	WebElement newccYear;
	
	@FindBy(xpath = ""+newccYearDefTextt+"")
	WebElement newccYearDefText;
	
	@FindBy(xpath = ""+countrySelectionn+"")
	WebElement countrySelection;
	
	@FindBy(xpath = ""+countryDefTextt+"")
	WebElement countryDefText;
	
	@FindBy(xpath = ""+fNamee+"")
	WebElement fName;
	
	@FindBy(xpath = ""+lNamee+"")
	WebElement lName;
	
	@FindBy(xpath = ""+stAddresss+"")
	WebElement stAddress;
	
	@FindBy(xpath = ""+aptSuiteAddresss+"")
	WebElement aptSuiteAddress;
	
	@FindBy(xpath = ""+cityy+"")
	WebElement city;
	
	@FindBy(xpath = ""+statee+"")
	WebElement state;
	
	@FindBy(xpath = ""+zipCodee+"")
	WebElement zipCode;
	
	@FindBy(xpath = ""+contactNoo+"")
	WebElement contactNo;
	
	@FindBy(xpath = ""+companyNamee+"")
	WebElement companyName;
	
	@FindBy(xpath = ""+savePaymentt+"")
	WebElement savePayment;
	
	@FindBy(xpath = ""+cancelPaymentt+"")
	WebElement cancelPayment;
	
	@FindBy(xpath = ""+fNametxtt+"")
	WebElement fNametxt;
	
	@FindBy(xpath = ""+lNametxtt+"")
	WebElement lNametxt;
	
	@FindBy(xpath = ""+stAddresstxtt+"")
	WebElement stAddresstxt;
	
	@FindBy(xpath = ""+aptSuiteAddresstxtt+"")
	WebElement aptSuiteAddresstxt;
	
	@FindBy(xpath = ""+citytxtt+"")
	WebElement citytxt;
	
	@FindBy(xpath = ""+statetxtt+"")
	WebElement statetxt;
	
	@FindBy(xpath = ""+statetxtt1+"")
	WebElement statetxt1;
	
	@FindBy(xpath = ""+zipcodetxtt+"")
	WebElement zipcodetxt;
	
	@FindBy(xpath = ""+contactNotxtt+"")
	WebElement contactNotxt;
	
	@FindBy(xpath = ""+companyNametxtt+"")
	WebElement companyNametxt;
	
	@FindBy(xpath = ""+addPayDisclaimerMessagee+"")
	WebElement addPayDisclaimerMessage;
	
	@FindBy(xpath = ""+addPayDefaultPaymentt+"")
	WebElement addPayDefaultPayment;
	
	@FindBy(xpath = ""+addPayErrorr+"")
	WebElement addPayError;
	
	@FindBy(xpath = ""+addPayAddBillingAddressTitlee+"")
	WebElement addPayAddBillingAddressTitle;
	
	@FindBy(xpath = ""+addPayAddBillingSavedAddressLinkk+"")
	WebElement addPayAddBillingSavedAddressLink;
	
	@FindBy(xpath = ""+addPaySavedAddresss+"")
	WebElement addPaySavedAddress;
	
	@FindBy(xpath = ""+addPaySavedAddressTitlee+"")
	WebElement addPaySavedAddressTitle;
	
	@FindBy(xpath = ""+addPayDefaultPaymentCheckboxx+"")
	WebElement addPayDefaultPaymentCheckbox;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentCCExpMonthh+"")
	WebElement paymentsDefaultPaymentCCExpMonth;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentCCExpYearr+"")
	WebElement paymentsDefaultPaymentCCExpYear;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentCCFirstNamee+"")
	WebElement paymentsDefaultPaymentCCFirstName;
	
	@FindBy(xpath = ""+paymentsDefaultPaymentCCLastNamee+"")
	WebElement paymentsDefaultPaymentCCLastName;
	
	@FindBy(xpath = ""+editPaymentsPagee+"")
	WebElement editPaymentsPage;
	
	@FindBy(xpath = ""+paymentsEditPaymentCCExpMonthh+"")
	WebElement paymentsEditPaymentCCExpMonth;
	
	@FindBy(xpath = ""+paymentsEditPaymentCCExpYearr+"")
	WebElement paymentsEditPaymentCCExpYear;
	
	@FindBy(xpath = ""+editfNamee+"")
	WebElement editfName;
	
	@FindBy(xpath = ""+editlNamee+"")
	WebElement editlName;
	
	@FindBy(xpath = ""+editstAddresss+"")
	WebElement editstAddress;
	
	@FindBy(xpath = ""+editcityy+"")
	WebElement editcity;
	
	@FindBy(xpath = ""+editSubmitt+"")
	WebElement editSubmit;
	
	@FindBy(xpath = ""+editCancell+"")
	WebElement editCancel;
	
	@FindBy(xpath = ""+editPaymentCancell+"")
	WebElement editPaymentCancel;
	
	@FindBy(xpath = ""+defaultAddressFormm+"")
	WebElement defaultAddressForm;
	
	@FindBy(xpath = ""+addressDefaultAddressDeletee+"")
	WebElement addressDefaultAddressDelete;
	
	@FindBy(xpath = ""+changeDefaultAddressRemoveCancelBtnn+"")
	WebElement changeDefaultAddressRemoveCancelBtn;
	
	@FindBy(xpath = ""+addressAddNewShippingg+"")
	WebElement addressAddNewShipping;
	
	@FindBy(xpath = ""+addressAddNewShippingPagee+"")
	WebElement addressAddNewShippingPage;
	
	@FindBy(xpath = ""+addPayAddShippingAddressTitlee+"")
	WebElement addPayAddShippingAddressTitle;
	
	@FindBy(xpath = ""+addAddressSavee+"")
	WebElement addAddressSave;
	
	@FindBy(xpath = ""+addAddressCancell+"")
	WebElement addAddressCancel;
	
	@FindBy(xpath = ""+poBoxtxtt1+"")
	WebElement poBoxtxt1;
	
	@FindBy(xpath = ""+addPayDefaultShippingg+"")
	WebElement addPayDefaultShipping;
	
	@FindBy(xpath = ""+addAddressSaveErrorr+"")
	WebElement addAddressSaveError;
	
	@FindBy(xpath = ""+editShipPageTitlee+"")
	WebElement editShipPageTitle;
	
	@FindBy(xpath = ""+giftCardTitlee+"")
	WebElement giftCardTitle;
	
	@FindBy(xpath = ""+addgiftCardNumberr+"")
	WebElement addgiftCardNumber;
	
	@FindBy(xpath = ""+addgiftCardPinNumberr+"")
	WebElement addgiftCardPinNumber;
	
	@FindBy(xpath = ""+addCardSubmitBtnn+"")
	WebElement addCardSubmitBtn;
	
	@FindBy(xpath = ""+addCardPinOptionalTextt+"")
	WebElement addCardPinOptionalText;
	
	@FindBy(xpath = ""+addCardErrorr+"")
	WebElement addCardError;
	
	@FindBy(xpath = ""+giftCardBalanceSectionn+"")
	WebElement giftCardBalanceSection;
	
	@FindBy(xpath = ""+giftCardBalanceTitlee+"")
	WebElement giftCardBalanceTitle;
	
	@FindBy(xpath = ""+giftCardNumberFieldd+"")
	WebElement giftCardNumberField;
	
	@FindBy(xpath = ""+giftCardPinFieldd+"")
	WebElement giftCardPinField;
	
	@FindBy(xpath = ""+giftCardSubmitBtnn+"")
	WebElement giftCardSubmitBtn;
	
	@FindBy(xpath = ""+giftCardErrorr+"")
	WebElement giftCardError;
	
	@FindBy(xpath = ""+giftCardAbtPinn+"")
	WebElement giftCardAbtPin;
	
	@FindBy(xpath = ""+wishListDDD+"")
	WebElement wishListDD;
	
	@FindBy(xpath = ""+wishListContainerr+"")
	WebElement wishListContainer;
	
	@FindBy(xpath = ""+wishListViewWishlistBtnn+"")
	WebElement wishListViewWishlistBtn;
	
	@FindBy(xpath = ""+wishListCreateWishlistBtnn+"")
	WebElement wishListCreateWishlistBtn;
	
	@FindBy(xpath = ""+textBookRentalTabss+"")
	WebElement textBookRentalTabs;
	
	//aa
	
	@FindBy(how = How.XPATH,using = ""+individualOrderErrorr+"")
	List<WebElement> individualOrderError;
	
	@FindBy(how = How.XPATH, using = ""+searchPageshareIconListt+"")
	List<WebElement> searchPageshareIconList;
	
	@FindBy(how = How.XPATH, using = ""+myAccFormAccModuless+"")
	List<WebElement> myAccFormAccModules;
	
	@FindBy(how = How.XPATH, using = ""+myAccFormAccModulesTitlee+"")
	List<WebElement> myAccFormAccModulesTitle;
	
	@FindBy(how = How.XPATH, using = ""+myAccFormAccModulesTitlee1+"")
	List<WebElement> myAccFormAccModulesTitle1;
	
	@FindBy(how = How.XPATH, using = ""+myAccFormLandingPageOrderModuleDett+"")
	List<WebElement> myAccFormLandingPageOrderModuleDet;
	
	@FindBy(how = How.XPATH, using = ""+myAccFormLandingPageOrderModuleDett1+"")
	List<WebElement> myAccFormLandingPageOrderModuleDet1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountOrderDetailsTextt+"")
	List<WebElement> myAccountOrderDetailsText;
	
	@FindBy(how = How.XPATH, using = ""+myAccountOrderDetailsTextt1+"")
	List<WebElement> myAccountOrderDetailsText1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAccountDetailsTextt+"")
	List<WebElement> myAccountAccountDetailsText;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAccountDetailsTextt1+"")
	List<WebElement> myAccountAccountDetailsText1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountPaymentDetailsTextt+"")
	List<WebElement> myAccountPaymentDetailsText;
	
	@FindBy(how = How.XPATH, using = ""+myAccountPaymentDetailsTextt1+"")
	List<WebElement> myAccountPaymentDetailsText1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAddressDetailsTextt+"")
	List<WebElement> myAccountAddressDetailsText;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAddressDetailsTextt1+"")
	List<WebElement> myAccountAddressDetailsText1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountMembershipDetailsTextt+"")
	List<WebElement> myAccountMembershipDetailsText;
	
	@FindBy(how = How.XPATH, using = ""+myAccountMembershipDetailsTextt1+"")
	List<WebElement> myAccountMembershipDetailsText1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountSideArroww+"")
	List<WebElement> myAccountSideArrow;
	
	@FindBy(how = How.XPATH, using = ""+myAccountMembershipDetailss+"")
	List<WebElement> myAccountMembershipDetails;
	
	@FindBy(how = How.XPATH, using = ""+orderContainerOrderListt+"")
	List<WebElement> orderContainerOrderList;
	
	@FindBy(how = How.XPATH, using = ""+orderContainerOrderNoListss+"")
	List<WebElement> orderContainerOrderNoLists;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAccountSettingsPageSubTitlee+"")
	List<WebElement> myAccountAccountSettingsPageSubTitle;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAccountSettingsPageSubTitlee1+"")
	List<WebElement> myAccountAccountSettingsPageSubTitle1;
	
	@FindBy(how = How.XPATH, using = ""+myAccountAccountSettingsPageSubTitlee2+"")
	List<WebElement> myAccountAccountSettingsPageSubTitle2;
	
	@FindBy(how = How.XPATH, using = ""+accountSettingsChangeNameIndividualErrorr+"")
	List<WebElement> accountSettingsChangeNameIndividualError;
	
	/*@FindBy(how = How.XPATH, using = ""+accountSettingsChangeNameSuccessPagee+"")
	List<WebElement> accountSettingsChangeNameSuccessPage;*/
	
	@FindBy(how = How.XPATH, using = ""+accountSettingsChangeUpdateIndividualPasswordAlertt+"")
	List<WebElement> accountSettingsChangeUpdateIndividualPasswordAlert;
	
	@FindBy(how = How.XPATH, using = ""+changePassErrorr+"")
	List<WebElement> changePassError;
	
	@FindBy(how = How.XPATH, using = ""+changeSecQuesErrorr+"")
	List<WebElement> changeSecQuesError;
	
	@FindBy(how = How.XPATH, using = ""+changeContentSettingss+"")
	List<WebElement> changeContentSettings;
	
	@FindBy(how = How.XPATH, using = ""+changeContentSettingsRadioo+"")
	List<WebElement> changeContentSettingsRadio;
	
	@FindBy(how = How.XPATH, using = ""+changeContentSettingsRadioo1+"")
	List<WebElement> changeContentSettingsRadio1;
	
	@FindBy(how = How.XPATH, using = ""+changeInstantPurchaseRadioo+"")
	List<WebElement> changeInstantPurchaseRadio;
	
	@FindBy(how = How.XPATH, using = ""+changeInstantPurchaseRadioo1+"")
	List<WebElement> changeInstantPurchaseRadio1;
	
	@FindBy(how = How.XPATH, using = ""+changeInstantPurchaseRadioTextt+"")
	List<WebElement> changeInstantPurchaseRadioText;
	
	@FindBy(how = How.XPATH, using = ""+changeDefaulSpeedOptionss+"")
	List<WebElement> changeDefaulSpeedOptions;
	
	@FindBy(how = How.XPATH, using = ""+changeInstantSpeedRadioBtnn+"")
	List<WebElement> changeInstantSpeedRadioBtn;
	
	@FindBy(how = How.XPATH, using = ""+changeDefaultPayRemoveCancelBtnn+"")
	List<WebElement> changeDefaultPayRemoveCancelBtn1;
	
	@FindBy(how = How.XPATH, using = ""+changeDefaultAddressRemoveCancelBtnn+"")
	List<WebElement> changeDefaultAddressRemoveCancelBtn1;
	
	@FindBy(how = How.XPATH, using = ""+individualAddPayErrorr+"")
	List<WebElement> individualAddPayError;
	
	@FindBy(how = How.XPATH, using = ""+addPaySavedAddressRadioButtonn+"")
	List<WebElement> addPaySavedAddressRadioButton;
	
	@FindBy(how = How.XPATH, using = ""+addressBookAddressLists+"")
	List<WebElement> addressBookAddressList;
	
	@FindBy(how = How.XPATH, using = ""+individualAddAddressSaveErrorr+"")
	List<WebElement> individualAddAddressSaveError;
	
	//a
	
	
	
	// JavaScript Executer Click
	public void jsclick(WebElement ele)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ele);
	}
	
	//Clear all the fields in the createAccountPage
	public void clearAll()
	{
		try
		{
			if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password)))
			{
				emailId.clear();
				password.clear();
			}
			else
			{
				Fail( "The Email and Password field is not displayed to clear.");
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in clearing the Email and Password.");
			Exception("Error in clearing the Email and Password.");
		}
	}
	
	// Navigate to My Account Page
	public boolean navigatetoMyAccount()
	{
		boolean pgOk = false;
		myAccntPage = false;
		manageAccountLoaded = false;
		try
		{
			String currUrl = driver.getCurrentUrl();
			if(!currUrl.equals(myAccountUrl))
			{
				driver.get(myAccountUrl);
				wait.until(ExpectedConditions.or(
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(myAccountPageTitle),
						ExpectedConditions.visibilityOf(myAccFormAccModules.get(0))
						)
				);
				currUrl = driver.getCurrentUrl();
				if(currUrl.equals(myAccountUrl))
				{
					pgOk = true;
				}
			}
			
			if(currUrl.equals(myAccountUrl))
			{
				pgOk = true;
			}
			
			if(!BNBasicfeature.isListElementPresent(myAccFormAccModules))
			{
				driver.get(myAccountUrl);
				wait.until(ExpectedConditions.or(
						ExpectedConditions.visibilityOf(header),
						ExpectedConditions.visibilityOf(myAccountPageTitle),
						ExpectedConditions.visibilityOf(myAccFormAccModules.get(0))
						)
				);
				
				currUrl = driver.getCurrentUrl();
				if(currUrl.equals(myAccountUrl))
				{
					pgOk = true;
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(" Issue in Navigating to the My Account page." );
			Exception("Issue in Navigating to the My Account page.");
		}
		return pgOk;
	}
	
	 // Find the Landing Page Form Value
	 public boolean getLandingPageFormValue(String val)
	 {
		boolean titleFound = false;
		actTitle = "";
		try
		{
			if(BNBasicfeature.isElementPresent(myAccFormContainer))
			{
				Pass( "The My Account Form Container is displayed.");
				wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
				if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
				{
					Pass( " The Account modules are displayed.");
					titleFound = false;
					if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
					{
						String actTitle = "";
						String expTitle = val;
						for(int j = 0; j<myAccFormAccModulesTitle.size(); j++)
						{
							//BNBasicfeature.scrolldown(myAccFormAccModulesTitle.get(j), driver);
							accElement = myAccFormAccModulesTitle.get(j);
							//accElement = driver.findElement(By.xpath("(//*[contains(@class,'account-settings-form')]//*[@class='account-module']//h2)["+(j+1)+"]"));
							actTitle = myAccFormAccModulesTitle.get(j).getText();
							upperCase = false;
							if(actTitle.equalsIgnoreCase(expTitle))
							{
								titleFound = true;
								upperCase = myAccFormAccModulesTitle.get(j).getCssValue("text-transform").contains("uppercase");
								getFormCnt = j;
								break;
							}
							else
							{
								continue;
							}
						}
							
						if(titleFound==false)
						{
							for(int l = 0; l<myAccFormAccModulesTitle1.size(); l++)
							{
								//BNBasicfeature.scrolldown(myAccFormAccModulesTitle1.get(l), driver);
								accElement = myAccFormAccModulesTitle1.get(l);
								actTitle = myAccFormAccModulesTitle1.get(l).getText();
								upperCase = false;
								if(actTitle.equalsIgnoreCase(expTitle))
								{
									titleFound = true;
									getFormCnt = l;
									upperCase = myAccFormAccModulesTitle1.get(l).getCssValue("text-transform").contains("uppercase");
									break;
								}
								else
								{
									continue;
								}
							}
						}
					}
					else
					{
						Fail( " The Account modules are not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container List is not displayed.");
				}
			}
			else
			{
				Fail( "The My Account Form Container is not displayed.");
			}
		}
		catch(Exception e)
		{
			System.out.println("Issue in Getting the Landing Page Value."+e.getMessage());
			Exception("Issue in Getting the Landing Page Value.");
		}
		return titleFound;
	}
	
	 // Get Text Color for the Landing Page
	 public boolean getLandingPageTextColor(List<WebElement> ele, List<WebElement> ele1,String expColor)
	 {
		boolean coloFound = false;
		try
		{
			if(BNBasicfeature.isListElementPresent(ele))
			{
				String csvalue = "";
				
				for(int i = 0; i<ele.size(); i++)
				{
					csvalue = ele.get(i).getCssValue("color");
					String menuhex = BNBasicfeature.colorfinder(csvalue);
					if(menuhex.contains(expColor))
					{
						coloFound = true;
						break;
					}
					else
					{
						continue;
					}
				}
			}
			else if(BNBasicfeature.isListElementPresent(ele1))
			{
				String csvalue = "";
				for(int i = 0; i<ele1.size(); i++)
				{
					csvalue = ele1.get(i).getCssValue("color");
					String menuhex = BNBasicfeature.colorfinder(csvalue);
					if(menuhex.contains(expColor))
					{
						coloFound = true;
						break;
					}
					else
					{
						continue;
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in Getting the Landing Page Color Value."+e.getMessage());
			Exception("Error in Getting the Landing Page Color Value.");
		}
		return coloFound;
	}
	 
	// Get Manage Account Page Subtitles
	 public boolean getManageAccountPageTitles(String expTitle)
	 {
		titFound = false;
		String actTitle = "";
		try
		{
			if(BNBasicfeature.isListElementPresent(myAccountAccountSettingsPageSubTitle))
			{
				//Pass("The Account Container Link are displayed.");
				titFound = false;
				log.add("The Expected Title was : " + expTitle);
				for(int i = 0; i<myAccountAccountSettingsPageSubTitle.size();i++)
				{
					actTitle = myAccountAccountSettingsPageSubTitle.get(i).getText();
					if(actTitle.contains(expTitle))
					{
						BNBasicfeature.scrolldown(myAccountAccountSettingsPageSubTitle.get(i), driver);
						log.add("The Actual Title is : " + expTitle);
						titFound = true;
						break;
					}
					else
					{
						titFound = false;
						continue;
					}
				}
			}
			else if(BNBasicfeature.isListElementPresent(myAccountAccountSettingsPageSubTitle1))
			{
				//Pass("The Account Container Link are displayed.");
				titFound = false;
				log.add("The Expected Title was : " + expTitle);
				for(int i = 0; i<myAccountAccountSettingsPageSubTitle1.size();i++)
				{
					actTitle = myAccountAccountSettingsPageSubTitle1.get(i).getText();
					if(expTitle.contains(expTitle))
					{
						BNBasicfeature.scrolldown(myAccountAccountSettingsPageSubTitle1.get(i), driver);
						log.add("The Actual Title is : " + expTitle);
						titFound = true;
						break;
					}
					else
					{
						titFound = false;
						continue;
					}
				}
			}
			else
			{
				Fail( " The Subtitles in the Manage Account Page is not dispalyed.");
			}
			
			if(titFound==false)
			{
				if(BNBasicfeature.isListElementPresent(myAccountAccountSettingsPageSubTitle1))
				{
					Pass("The Account Container Link are displayed.");
					titFound = false;
					log.add("The Expected Title was : " + expTitle);
					for(int i = 0; i<myAccountAccountSettingsPageSubTitle1.size();i++)
					{
						actTitle = myAccountAccountSettingsPageSubTitle.get(i).getText();
						if(expTitle.contains(expTitle))
						{
							BNBasicfeature.scrolldown(myAccountAccountSettingsPageSubTitle1.get(i), driver);
							log.add("The Actual Title is : " + expTitle);
							titFound = true;
							break;
						}
						else
						{
							titFound = false;
							continue;
						}
					}
				}
				
				if(titFound==false)
				{
					if(BNBasicfeature.isListElementPresent(myAccountAccountSettingsPageSubTitle2))
					{
						//Pass("The Account Container Link are displayed.");
						titFound = false;
						log.add("The Expected Title was : " + expTitle);
						for(int i = 0; i<myAccountAccountSettingsPageSubTitle2.size();i++)
						{
							actTitle = myAccountAccountSettingsPageSubTitle2.get(i).getText();
							if(expTitle.contains(expTitle))
							{
								log.add("The Actual Title is : " + expTitle);
								titFound = true;
								break;
							}
							else
							{
								titFound = false;
								continue;
							}
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("Issue in getting the Sub Title from My Account page.");
			Exception("Issue in getting the Sub Title from My Account page.");
		}
		return titFound;
	 }
		
	/* BRM - 2677 Verify that, in "My Account" page the following options should be shown : "Orders", "Payment", and so on and it should match with the creative.*/
	/* BRM - 5405 Guest User: Verify whether the My Account page is displaying before the user sign in */
	/* BRM - 5408 Guest User: Verify the My Account title is displaying as per the creative*/
	/* BRM - 5409 Guest User: Verify the ORDER HISTORY label is displaying as per the creative*/
	public void myAccountBefLogin()
	{
		ChildCreation("BRM - 2677 Verify that, in My Account page the following options should be shown : Orders, Payment, and so on and it should match with the creative.");
		boolean BRM5405 = false, BRM5408 = false, BRM5409 = false;
		try
		{
			String cellVal = BNBasicfeature.getExcelVal("BRMOrderPageNavigation", sheet, 5);
			String expTitle = BNBasicfeature.getExcelVal("BRM5409", sheet, 2);
			String title = "";
			wait.until(ExpectedConditions.visibilityOf(header));
			Actions act = new Actions(driver);
			if(BNBasicfeature.isElementPresent(menu))
			{
				Thread.sleep(200);
				wait.until(ExpectedConditions.elementToBeClickable(menu));
				jsclick(menu);
				wait.until(ExpectedConditions.visibilityOf(menuLists));
				wait.until(ExpectedConditions.elementToBeClickable(myAccount));
				Thread.sleep(100);
				act.moveToElement(myAccount).click().build().perform();
				wait.until(ExpectedConditions.visibilityOf(manageAccount));
				Thread.sleep(100);
				act.moveToElement(manageAccount).click().build().perform();
				Thread.sleep(200);
				wait.until(ExpectedConditions.or
						(
							ExpectedConditions.visibilityOf(myAccountPageTitle),
							ExpectedConditions.visibilityOf(myAccountPageTitle1)
						)
				);
				
				if(BNBasicfeature.isElementPresent(myAccountPageTitle))
				{
					Pass("The user is redirected to the My Account Page.");
					log.add("The Expected Title was : " + cellVal);
					title = myAccountPageTitle.getText();
					log.add("The Actual Title is : " + title);
					if(cellVal.contains(title))
					{
						Pass("The My Account title match the expected content.",log);
						manageAcc = true;
					}
					else
					{
						Fail("The My Account title does not match the expected content.",log);
					}
				}
				else if(BNBasicfeature.isElementPresent(myAccountPageTitle1))
				{
					Pass("The user is redirected to the My Account Page.");
					log.add("The Expected Title was : " + cellVal);
					title = myAccountPageTitle1.getText();
					log.add("The Actual Title is : " + title);
					if(cellVal.contains(title))
					{
						Pass("The My Account title match the expected content.",log);
						manageAcc = true;
					}
					else
					{
						Fail("The My Account title does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The User is not navigated to the My Account Page.");
				}
			}
			
			if(manageAcc==true)
			{
				BRM5405 = true;
				BRM5408 = true;
				if(BNBasicfeature.isElementPresent(orderContainerTitle))
				{
					Pass("The Order Container is displayed.");
					log.add("The Expected value is : " + expTitle);
					title = orderContainerTitle.getText();
					if(title.equalsIgnoreCase(expTitle))
					{
						Pass("The Value matches the expected content.",log);
						BRM5409 = true;
					}
					else
					{
						Fail("The Value does not matches the expected content.",log);
					}
				}
				else
				{
					Fail("The Order Container title is not displayed.");
				}
			}
			else
			{
				Fail("The User is not navigated to the My Account Page.");
			}
		}
		catch(Exception e)
		{
			System.out.println(" BRM - 2677 Issue ." + e.getMessage());
			Exception(" BRM - 2677 Issue." + e.getMessage());
		}
		
		/* BRM - 5405 Guest User: Verify whether the My Account page is displaying before the user sign in */
		ChildCreation("BRM - 5405 Guest User: Verify whether the My Account page is displaying before the user sign in");
		if(manageAcc==true)
		{
			if(BRM5405==true)
			{
				Pass("The My Account title match the expected content.",log);
			}
			else
			{
				Fail("The My Account title does not match the expected content.",log);
			}
		}
		else
		{
			Skip("The User is not navigated to the My Account Page.");
		}
		
		/* BRM - 5408 Guest User: Verify the My Account title is displaying as per the creative*/
		ChildCreation(" BRM - 5408 Guest User: Verify the My Account title is displaying as per the creative.");
		if(manageAcc==true)
		{
			if(BRM5408==true)
			{
				Pass("The My Account title match the expected content.",log);
			}
			else
			{
				Fail("The My Account title does not match the expected content.",log);
			}
		}
		else
		{
			Skip("The User is not navigated to the My Account Page.");
		}
		
		/* BRM - 5409 Guest User: Verify the ORDER HISTORY label is displaying as per the creative*/
		ChildCreation(" BRM - 5409 Guest User: Verify the ORDER HISTORY label is displaying as per the creative.");
		if(manageAcc==true)
		{
			if(BRM5409==true)
			{
				Pass("The Order History title match the expected content.",log);
			}
			else
			{
				Fail("The Order History title does not match the expected content.",log);
			}
		}
		else
		{
			Skip("The User is not navigated to the My Account Page.");
		}
	}
	
	/* BRM - 2681 Verify that the expanded "Orders" option should have the followings options and should match with the creatives.*/
	/* BRM - 5406 Guest User: Verify the fields displaying in My Account landing page when the user not signed in*/
	/* BRM - 2687 Verify that Track Order button in Orders option should be highlighted in Green color.*/
	/* BRM - 5410 Guest User: Verify the inner text displaying in Email address and Order number text box fields*/
	public void myAccountOrderBefLoginDet()
	{
		ChildCreation("BRM - 2681 Verify that the expanded Orders option should have the followings options and should match with the creatives.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2681", sheet, 5);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					BNBasicfeature.scrollup(myAccountPageTitle, driver);
					Pass("The email address field is displayed.");
					if(BNBasicfeature.isElementPresent(emailAddressTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = emailAddressTxt.getText();
						log.add("The Expected Value was : " + val[0]);
						if(actVal.contains(val[0]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderNumber))
				{
					Pass("The Order Number field is displayed.");
					if(BNBasicfeature.isElementPresent(orderNumberTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = orderNumberTxt.getText();
						log.add("The Expected Value was : " + val[1]);
						if(actVal.contains(val[1]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The Order Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2681 Issue ." + e.getMessage());
				Exception(" BRM - 2681 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5410 Guest User: Verify the inner text displaying in Email address and Order number text box fields*/
		ChildCreation("BRM - 5410 Guest User: Verify the inner text displaying in Email address and Order number text box fields");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2681", sheet, 5);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					if(BNBasicfeature.isElementPresent(emailAddressTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = emailAddressTxt.getText();
						log.add("The Expected Value was : " + val[0]);
						if(actVal.contains(val[0]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderNumber))
				{
					Pass("The Order Number field is displayed.");
					if(BNBasicfeature.isElementPresent(orderNumberTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = orderNumberTxt.getText();
						log.add("The Expected Value was : " + val[1]);
						if(actVal.contains(val[1]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The Order Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5410 Issue ." + e.getMessage());
				Exception(" BRM - 5410 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 2687 Verify that Track Order button in Orders option should be highlighted in Green color. */
		ChildCreation(" BRM - 2687 Verify that Track Order button in Orders option should be highlighted in Green color.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2687", sheet, 6);
				if(BNBasicfeature.isElementPresent(trackOrderBtn))
				{
					Pass("The Track Order button is displayed.");
					String btnColor = trackOrderBtn.getCssValue("background-color");
					Color colcnvt = Color.fromString(btnColor);
					String trkBtncolor = colcnvt.asHex();
					log.add("The Expected Color was : " + cellVal);
					log.add("The Actual Color is  : " + trkBtncolor);
					if(cellVal.contains(trkBtncolor))
					{
						Pass("The Button Color match the expected content.",log);
					}
					else
					{
						Fail("The Button Color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Track Order button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2687 Issue ." + e.getMessage());
				Exception(" BRM - 2687 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5406 Guest User: Verify the fields displaying in My Account landing page when the user not signed in*/
		ChildCreation("BRM - 5406 Guest User: Verify the fields displaying in My Account landing page when the user not signed in");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2681", sheet, 5);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					if(BNBasicfeature.isElementPresent(emailAddressTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = emailAddressTxt.getText();
						log.add("The Expected Value was : " + val[0]);
						if(actVal.contains(val[0]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderNumber))
				{
					Pass("The Order Number field is displayed.");
					if(BNBasicfeature.isElementPresent(orderNumberTxt))
					{
						Pass("The Email Address Default Text is displayed.");
						String actVal = orderNumberTxt.getText();
						log.add("The Expected Value was : " + val[1]);
						if(actVal.contains(val[1]))
						{
							Pass("The Value mathces.",log);
						}
						else
						{
							Fail("The Value mismathces.",log);
						}
					}
					else
					{
						Fail("The Email Address Default Text is not displayed.");
					}
				}
				else
				{
					Fail("The Order Number field is not displayed.");
				}
				
				cellVal = BNBasicfeature.getExcelVal("BRM2687", sheet, 6);
				if(BNBasicfeature.isElementPresent(trackOrderBtn))
				{
					Pass("The Track Order button is displayed.");
					String btnColor = trackOrderBtn.getCssValue("background-color");
					Color colcnvt = Color.fromString(btnColor);
					String trkBtncolor = colcnvt.asHex();
					log.add("The Expected Color was : " + cellVal);
					log.add("The Actual Color is  : " + trkBtncolor);
					if(cellVal.contains(trkBtncolor))
					{
						Pass("The Button Color match the expected content.",log);
					}
					else
					{
						Fail("The Button Color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Track Order button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5406 Issue ." + e.getMessage());
				Exception(" BRM - 5406 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 5407 Guest User: Verify the header and footer is displaying in My Account landing page when the user not signed in*/
	public void myAccountPageHeaderFooterDet()
	{
		ChildCreation("BRM - 5407 Guest User: Verify the header and footer is displaying in My Account landing page when the user not signed in");
		if(manageAcc==true)
		{
			try
			{
			
				String cellVal = BNBasicfeature.getExcelVal("BRM5407", sheet, 2);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(menu))
				{
					Pass("The Pancake Menu is displayed / present.");
				}
				else
				{
					Fail("The Pancake Menu is not displayed / present.");
				}
				
				if(BNBasicfeature.isElementPresent(searchIcon))
				{
					Pass("The Search Icon is displayed / present.");
				}
				else
				{
					Fail("The Search Icon is displayed / present.");
				}
					
				if(BNBasicfeature.isElementPresent(bnLogo))
				{
					Pass("The Barnes and Noble logo is displayed / present.");
				}
				else
				{
					Fail("The Barnes and Noble logo is not displayed / present.");
				}
					
				if(BNBasicfeature.isElementPresent(storeLocator))
				{
					Pass("The Store Locator icon is displayed / present.");
				}
				else
				{
					Fail("The Store Locator icon is not displayed / present.");
				}
					
				if(BNBasicfeature.isElementPresent(bagIcon))
				{
					Pass("The Shopping Bag icon is displayed / present.");	
				}
				else
				{
					Fail("The Shopping Bag icon is not displayed / present.");
				}
					
				if(BNBasicfeature.isElementPresent(promoCarousel))
				{
					Pass("The Promo Carousel icon is displayed / present.");
				}
				else
				{
					Fail("The Promo Carousel is not displayed / present.");
				}
				
				if(BNBasicfeature.isListElementPresent(searchPageshareIconList))
				{
					if(BNBasicfeature.isElementPresent(shareFB))
					{
						BNBasicfeature.scrolldown(shareFB, driver);
						Pass("The Social Media Facebook icon is displayed.");
					}
					else
					{
						Fail("The Social Media Facebook icon is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(shareTwitter))
					{
						Pass("The Social Media Twitter icon is displayed.");
					}
					else
					{
						Fail("The Social Media Twitter icon is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(sharePinrest))
					{
						Pass("The Social Media Pinrest icon is displayed.");
					}
					else
					{
						Fail("The Social Media Pinrest icon is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(shareInsta))
					{
						Pass("The Social Media Instagram icon is displayed.");	
					}
					else
					{
						Fail("The Social Media Instagram icon is not displayed.");
					}
				}
				else
				{
					Fail("The Social Media Share Icon list is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(footerSectionMailContainer))
				{
					Pass("The Mail Container is displayed.");
					if(BNBasicfeature.isElementPresent(footerSectionMailContainerText))
					{
						String actVal = footerSectionMailContainerText.getText();
						log.add("The Expected value is : " + Val[11]);
						log.add("The Actual value is : " + actVal);
						if(Val[11].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Mail Container default text is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionMailSubmitButton))
					{
						Pass("The Submit button is displayed.");
					}
					else
					{
						Fail("The Submit button is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionMailEmail))
					{
						Pass("The Email field is displayed.");
					}
					else
					{
						Fail("The Email field is not displayed.");
					}
				}
				else
				{
					Fail("The Mail Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(myAccountFooterContainer))
				{
					BNBasicfeature.scrolldown(myAccountFooterContainer, driver);
					if(BNBasicfeature.isElementPresent(myAccountFooterMyAccount))
					{
						Pass("The My Account Link is displayed.");
						String actVal = myAccountFooterMyAccount.getText();
						log.add("The Expected value is : " + Val[2]);
						log.add("The Actual value is : " + actVal);
						if(Val[2].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The My Account Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionSignIn))
					{
						Pass("The Sign In Link is displayed.");
						String actVal = footerSectionSignIn.getText();
						log.add("The Expected value is : " + Val[3]);
						log.add("The Actual value is : " + actVal);
						if(Val[3].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Sign In Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionCustomerService))
					{
						Pass("The Customer Service Link is displayed.");
						String actVal = footerSectionCustomerService.getText();
						log.add("The Expected value is : " + Val[4]);
						log.add("The Actual value is : " + actVal);
						if(Val[4].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Customer Service Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionStores))
					{
						Pass("The Stores Link is displayed.");
						String actVal = footerSectionStores.getText();
						log.add("The Expected value is : " + Val[5]);
						log.add("The Actual value is : " + actVal);
						if(Val[5].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Store Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionTermsandUse))
					{
						Pass("The Terms of Use Link is displayed.");
						String actVal = footerSectionTermsandUse.getText();
						log.add("The Expected value is : " + Val[6]);
						log.add("The Actual value is : " + actVal);
						if(Val[6].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Terms and Use Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionPrivacy))
					{
						Pass("The Privacy Policy Link is displayed.");
						String actVal = footerSectionPrivacy.getText();
						log.add("The Expected value is : " + Val[8]);
						log.add("The Actual value is : " + actVal);
						if(Val[8].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Privacy Policy Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionCopyright))
					{
						Pass("The Copyright Link is displayed.");
						String actVal = footerSectionCopyright.getText();
						log.add("The Expected value is : " + Val[7]);
						log.add("The Actual value is : " + actVal);
						if(Val[7].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Copyright Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionTermsandUse1))
					{
						Pass("The Terms and Use Link is displayed.");
						String actVal = footerSectionTermsandUse1.getText();
						log.add("The Expected value is : " + Val[9]);
						log.add("The Actual value is : " + actVal);
						if(actVal.contains(Val[9]))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Terms and Use Link is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(footerSectionFullSite))
					{
						Pass("The Full Site Link is displayed.");
						String actVal = footerSectionFullSite.getText();
						log.add("The Expected value is : " + Val[10]);
						log.add("The Actual value is : " + actVal);
						if(Val[10].contains(actVal))
						{
							Pass("The Expected Value matches.",log);
						}
						else
						{
							Fail("The Expected Value does not matches.",log);
						}
					}
					else
					{
						Fail("The Full Site Link is not displayed.");
					}
				}
				else
				{
					Fail("The Footer Section Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5407 Issue ." + e.getMessage());
				Exception(" BRM - 5407 Issue." + e.getMessage());
			}
		}
		else 
		{
			Skip("The User is not navigated to the My Account Page.");
		}
		
	}
	
	/* BRM - 2682 Verify that "Email Address" textbox should accept maximum of 40 characters*/
	public void myAccountOrderEmailLimit()
	{
		ChildCreation(" BRM - 2682 Verify that Email Address textbox should accept maximum of 40 characters.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2682", sheet, 5);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					for(int i = 0; i<val.length;i++)
					{
						emailAddress.clear();
						Thread.sleep(500);
						emailAddress.sendKeys(val[i]);
						int currlen = emailAddress.getAttribute("value").length();
						log.add("The Value from the file was : " + val[i] + " and its length is : " + val[i].length());
						if(currlen<=40)
						{
							Pass("The Current Length does not exceeds the specified limit.",log);
						}
						else
						{
							Fail("The Current Length exceeds the specified limit.",log);
						}
					}
					emailAddress.clear();
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2682 Issue ." + e.getMessage());
				Exception(" BRM - 2682 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2683 Verify that when the fields left empty and "Track Order" button is clicked then alert message should be displayed.*/
	/* BRM - 5411 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button before entering the valid inputs.*/
	public void myAccountEmptyValidation()
	{
		ChildCreation("BRM - 2683 Verify that when the fields left empty and Track Order button is clicked then alert message should be displayed.");
		boolean BRM5411 = false;
		if(manageAcc==true)
		{
			try
			{
				//*[@id='orderErr']
				String cellVal = BNBasicfeature.getExcelVal("BRM2683", sheet, 5);
				if(BNBasicfeature.isElementPresent(trackOrderBtn))
				{
					Pass("The Track Order button is displayed.");
					jsclick(trackOrderBtn);
					wait.until(ExpectedConditions.visibilityOfAllElements(individualOrderError));
					Thread.sleep(100);
					String actErr = orderError.getText();
					log.add("The Actual expected error was : " + cellVal);
					log.add("The Actual error is : " + actErr);
					if(cellVal.contains(actErr))
					{
						Pass("The Raised error match the expected content.",log);
						BRM5411 = true;
					}
					else
					{
						Fail("The Raised error does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Track Order button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2683 Issue ." + e.getMessage());
				Exception(" BRM - 2683 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5411 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button before entering the valid inputs.*/
		ChildCreation("BRM - 5411 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button before entering the valid inputs.");
		if(manageAcc==true)
		{
			if(BRM5411==true)
			{
				Pass("The Raised error match the expected content.",log);
			}
			else
			{
				Fail("The Raised error does not match the expected content.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 5412 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button without entering the valid email address and valid order number*/
	/* BRM - 5414 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and invalid email address*/
	public void myAccountInvalidDataValidation()
	{
		ChildCreation("BRM - 5412 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button without entering the valid email address and valid order number.");
		boolean BRM5414 = false;
		String email = "", odid = "", expAlert = "", actErr = "";
		if(manageAcc==true)
		{
			try
			{
				email = BNBasicfeature.getExcelVal("BRM5412", sheet, 2);
				odid = BNBasicfeature.getExcelVal("BRM5412", sheet, 3);
				expAlert = BNBasicfeature.getExcelVal("BRM5412", sheet, 4);
				if(BNBasicfeature.isElementPresent(emailAddress)&&(BNBasicfeature.isElementPresent(orderNumber)))
				{
					Pass("The email address field is displayed.");
					emailAddress.clear();
					orderNumber.clear();
					emailAddress.sendKeys(email);
					orderNumber.sendKeys(odid);
					jsclick(trackOrderBtn);
					Thread.sleep(100);
					actErr = orderError.getText();
					log.add("The entered email address was : " + email);
					log.add("The entered order number is : " + odid);
					log.add("The Expected error is : " + expAlert);
					log.add("The Actual error is : " + actErr);
					if(expAlert.contains(actErr))
					{
						Pass("The Expected alert and the actual alert matches.",log);
						BRM5414 = true;
					}
					else
					{
						Fail("The Expected alert and the actual alert does not matches.",log);
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5412 Issue ." + e.getMessage());
				Exception(" BRM - 5412 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5414 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and invalid email address*/
		ChildCreation(" BRM - 5414 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and invalid email address.");
		if(manageAcc==true)
		{
			log.add("The entered email address was : " + email);
			log.add("The entered order number is : " + odid);
			log.add("The Expected error is : " + expAlert);
			log.add("The Actual error is : " + actErr);
			if(BRM5414==true)
			{
				Pass("The Raised error match the expected content.",log);
			}
			else
			{
				Fail("The Raised error does not match the expected content.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2684 Verify that when special characters and numbers are typed in "Email Address" field it should not throw alert message.*/
	public void myAccountSpecialCharEmailAdd()
	{
		ChildCreation("BRM - 2684 Verify that when special characters and numbers are typed in Email Address field it should not throw alert message.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2684", sheet, 7);
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					emailAddress.clear();
					emailAddress.sendKeys(cellVal);
					jsclick(trackOrderBtn);
					Thread.sleep(100);
					String actErr = orderError.getText();
					log.add("The entered email address was : " + cellVal);
					log.add("The Actual error is : " + actErr);
					if((actErr.contains("Email"))||(actErr.contains("Email")))
					{
						Fail("The alert was raised when Special Character was entered in the email address field.",log);
					}
					else
					{
						Pass("The alert was not raised when Special Character was entered in the email address field.",log);
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2684 Issue ." + e.getMessage());
				Exception(" BRM - 26884 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2685 Verify that when Email address is not valid or invalid format then alert message should be thrown.*/
	public void myAccountInvalidEmailId()
	{
		ChildCreation(" BRM - 2685 Verify that when Email address is not valid or invalid format then alert message should be thrown..");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2685", sheet, 7);
				String expErr = BNBasicfeature.getExcelVal("BRM2685", sheet, 5);
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					emailAddress.clear();
					emailAddress.sendKeys(cellVal);
					jsclick(trackOrderBtn);
					Thread.sleep(100);
					String actErr = orderError.getText();
					log.add("The entered email address was : " + cellVal);
					log.add("The Expected error was : " + expErr);
					log.add("The Actual error is : " + actErr);
					if(actErr.contains(expErr))
					{
						Pass("The Raised error match the expected content.",log);
					}
					else
					{
						Fail("The Raised error does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2685 Issue ." + e.getMessage());
				Exception(" BRM - 2685 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2686 Verify that when the special character "@" is not present in the "Email Address", field then it should display the alert message.*/
	public void myAccountInvalidEmailId1()
	{
		ChildCreation(" BRM - 2686 Verify that when the special character @ is not present in the Email Address, field then it should display the alert message.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2686", sheet, 7);
				String expErr = BNBasicfeature.getExcelVal("BRM2686", sheet, 5);
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					emailAddress.clear();
					emailAddress.sendKeys(cellVal);
					jsclick(trackOrderBtn);
					Thread.sleep(100);
					String actErr = orderError.getText();
					log.add("The entered email address was : " + cellVal);
					log.add("The Expected error was : " + expErr);
					log.add("The Actual error is : " + actErr);
					if(actErr.contains(expErr))
					{
						Pass("The Raised error match the expected content.",log);
					}
					else
					{
						Fail("The Raised error does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2686 Issue ." + e.getMessage());
				Exception(" BRM - 2686 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2688 Verify that on entering Characters or Special Characters in "Order Number" field, it should thrown alert message.*/
	public void myAccountInvalidOrderNumber()
	{
		ChildCreation(" BRM - 2688 Verify that on entering Characters or Special Characters in Order Number field, it should thrown alert message..");
		if(manageAcc==true)
		{
			try
			{
				String emailAdd = BNBasicfeature.getExcelVal("BRM2688", sheet, 7);
				String orderNu = BNBasicfeature.getExcelVal("BRM2688", sheet, 8);
				String expErr = BNBasicfeature.getExcelVal("BRM2688", sheet, 5);
				if(BNBasicfeature.isElementPresent(emailAddress))
				{
					Pass("The email address field is displayed.");
					emailAddress.clear();
					emailAddress.sendKeys(emailAdd);
					if(BNBasicfeature.isElementPresent(orderNumber))
					{
						Pass("The Order Number field is displayed.");
						orderNumber.clear();
						orderNumber.sendKeys(orderNu);
						jsclick(trackOrderBtn);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOf(header));
						wait.until(ExpectedConditions.visibilityOf(orderError));
						Thread.sleep(250);
						String actErr = orderError.getText();
						log.add("The entered order number was : " + orderNu);
						log.add("The Expected error was : " + expErr);
						log.add("The Actual error is : " + actErr);
						if(expErr.contains(actErr))
						{
							Pass("The Raised error match the expected content.",log);
						}
						else
						{
							Fail("The Raised error does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The Order Number field is not displayed.");
					}
					
				}
				else
				{
					Fail("The email address field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2688 Issue ." + e.getMessage());
				Exception(" BRM - 2688 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2689 Verify that, when the Order Number is not valid, then it should shown alert message.*/
	/* BRM - 5413 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and valid email address*/
	public void myAccountNoResOrderNumber()
	{
		ChildCreation(" BRM - 2689 Verify that, when the Order Number is not valid, then it should shown alert message.");
		boolean BRM5413 = false; 
		String orderNu = "", expErr = "",actErr = "";
		if(manageAcc==true)
		{
			try
			{
				wait.until(ExpectedConditions.visibilityOf(header));
				Thread.sleep(500);
				orderNu = BNBasicfeature.getExcelNumericVal("BRM2689", sheet, 8);
				expErr = BNBasicfeature.getExcelVal("BRM2689", sheet, 5);
				if(BNBasicfeature.isElementPresent(orderNumber))
				{
					Pass("The Order Number field is displayed.");
					wait.until(ExpectedConditions.visibilityOf(orderNumber));
					Thread.sleep(250);
					orderNumber.clear();
					Thread.sleep(250);
					wait.until(ExpectedConditions.visibilityOf(orderNumber));
					orderNumber.sendKeys(orderNu);
					wait.until(ExpectedConditions.visibilityOf(trackOrderBtn));
					Thread.sleep(1000);
					//Actions act = new Actions(driver);
					//act.moveToElement(trackOrderBtn).click().build().perform();
					jsclick(trackOrderBtn);
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(header));
					wait.until(ExpectedConditions.visibilityOfAllElements(individualOrderError));
					actErr = individualOrderError.get(0).getText();
					log.add("The entered order number was : " + orderNu);
					log.add("The Expected error was : " + expErr);
					log.add("The Actual error is : " + actErr);
					if(expErr.contains(actErr))
					{
						Pass("The Raised error match the expected content.",log);
						BRM5413 = true;
					}
					else
					{
						Fail("The Raised error does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Order Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2689 Issue ." + e.getMessage());
				Exception(" BRM - 2689 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5413 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and valid email address*/
		ChildCreation("BRM - 5413 Guest User: Verify whether is it displaying the validation alert if the user clicks Find button with entering the invalid order number and valid email address");
		if(manageAcc==true)
		{
			log.add("The entered order number was : " + orderNu);
			log.add("The Expected error was : " + expErr);
			log.add("The Actual error is : " + actErr);
			if(BRM5413==true)
			{
				Pass("The Raised error match the expected content.",log);
			}
			else
			{
				Fail("The Raised error does not match the expected content.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2678 Verify that, when the user is not Signed In, then "Please Sign in to your account" link should be displayed.*/
	public void myAccountSignInLink()
	{
		ChildCreation("BRM - 2678 Verify that, when the user is not Signed In, then Please Sign in to your account link should be displayed.");
		if(manageAcc==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2678", sheet, 5);
				if(BNBasicfeature.isElementPresent(signInToAccountLink))
				{
					String txt = signInToAccountLink.getText();
					log.add("The Expected value was : " + cellVal);
					log.add("The Actual Value is : " + txt);
					if(cellVal.contains(txt))
					{
						Pass("The Sign In to your account text mathces.",log);
					}
					else
					{
						if(BNBasicfeature.isElementPresent(signInToAccountLink1))
						{
							txt = signInToAccountLink1.getText();
							log.add("The Expected value was : " + cellVal);
							log.add("The Actual Value is : " + txt);
							if(cellVal.contains(txt))
							{
								Pass("The Sign In to your account text mathces.",log);
							}
							else
							{
								Fail("The Sign In to your account text mismatches.",log);
							}
						}
						else
						{
							Fail("The Sign In you Your Account Link is not found.");
						}
					}
				}
				else
				{
					Fail("The Sign In you Your Account Link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2678 Issue ." + e.getMessage());
				Exception(" BRM - 2678 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
	}
	
	/* BRM - 2691 Verify that when the user is registered user, then the user name text should be displayed as : Hi,XXXXX */
	/* BRM - 5417 Guest User: Verify is it displaying sign in overlay by clicking the link 'Please sign into your account' in my account landing page*/
	/* BRM - 5418 Verify whether is it able to view the My account landing page after sign in.*/
	/* BRM - 5420 Verify the My Account title is displaying as per the creative after sign in.*/
	public void myAccountsignInUserName()
	{
		ChildCreation("BRM - 2691 Verify that when the user is registered user, then the user name text should be displayed as : Hi,XXXXX .");
		boolean BRM5417 = false, BRM5418 = false, BRM5420 = true;
		if(manageAcc==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRMSIgnIn", sheet, 2);
				String passval = BNBasicfeature.getExcelVal("BRMSIgnIn", sheet, 3);
				String pgTitle = BNBasicfeature.getExcelVal("BRM5417", sheet, 2);
				String cellVal = BNBasicfeature.getExcelVal("BRMOrderPageNavigation", sheet, 5);
				if(BNBasicfeature.isElementPresent(signInToAccountLink))
				{
					jsclick(signInToAccountLink);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(myAccSignInPageframe));
					Thread.sleep(500);
					wait.until(ExpectedConditions.visibilityOf(pgetitle));
					
					if(BNBasicfeature.isElementPresent(pgetitle))
					{
						String actTitle = pgetitle.getText();
						log.add( "The Expected title was : " + pgTitle);
						log.add( "The actual title is : " + actTitle);
						if(pgTitle.contains(actTitle))
						{
							Pass( "The User is redirected to the expected page.");
							BRM5417 = true;
							if(BNBasicfeature.isElementPresent(emailId)&&(BNBasicfeature.isElementPresent(password)))
							{
								clearAll();
								log.add(" The user is navigated and they are in the Sign In Page.");
								emailId.sendKeys(emailidval);
								password.sendKeys(passval);
								if(BNBasicfeature.isElementPresent(secureSignIn))
								{
									jsclick(secureSignIn);
									wait.until(ExpectedConditions.and(
											ExpectedConditions.visibilityOf(header),
											ExpectedConditions.visibilityOf(menu),
											ExpectedConditions.textToBePresentInElement(footerSignIn, "Sign Out"),
											ExpectedConditions.visibilityOf(myAccountPageTitle)
											)
									);
									
									wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
									if(BNBasicfeature.isElementPresent(myAccountPageTitle))
									{
										Pass("The user is redirected to the My Account Page.");
										log.add("The Expected Title was : " + cellVal);
										String title = myAccountPageTitle.getText();
										log.add("The Actual Title is : " + title);
										if(cellVal.contains(title))
										{
											Pass("The My Account title match the expected content.",log);
											myAccountUrl = driver.getCurrentUrl();
											signedIn = true;
											BRM5418 = true;
											BRM5420 = true;
										}
										else
										{
											Fail("The My Account title does not match the expected content.",log);
										}
									}
									else
									{
										Fail("The User is not navigated to the My Accounts Page.");
									}
								}
								else
								{
									Fail(" The Sign In button is not displayed.");
								}
							}
							else
							{
								Fail( "The Email and Password field is not displayed.");
							}
						}
						else
						{
							Fail( "The Page title does not matches.");
						}
					}
					else
					{
						Fail( "The User is not redirected to the expected page.");
					}
				}
				else
				{
					Fail("The Sign In you Your Account Link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2691 Issue ." + e.getMessage());
				Exception(" BRM - 2691 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5417 Guest User: Verify is it displaying sign in overlay by clicking the link 'Please sign into your account' in my account landing page*/
		ChildCreation("BRM - 5417 Guest User: Verify is it displaying sign in overlay by clicking the link 'Please sign into your account' in my account landing page");
		if(manageAcc==true)
		{
			if(BRM5417==true)
			{
				Pass( "The User is redirected to the expected Sign In page.");
			}
			else
			{
				Fail( "The User was not redirected to the expected Sign In page.");
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page.");
		}
		
		/* BRM - 5418 Verify whether is it able to view the My account landing page after sign in.*/
		ChildCreation(" BRM - 5418 Verify whether is it able to view the My account landing page after sign in.");
		if(signedIn==true)
		{
			if(BRM5418==true)
			{
				Pass( "The User is redirected to the expected My Account page after signing In.");
			}
			else
			{
				Fail( "The User was not redirected to the expected My Account page after signing In.");
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page after sign In.");
		}
		
		/* BRM - 5420 Verify the My Account title is displaying as per the creative after sign in.*/
		ChildCreation("BRM - 5420 Verify the My Account title is displaying as per the creative after sign in.");
		if(signedIn==true)
		{
			if(BRM5420==true)
			{
				Pass( "The User is redirected to the expected My Account page after signing In.");
			}
			else
			{
				Fail( "The User was not redirected to the expected My Account page after signing In.");
			}
		}
		else
		{
			Skip( "The User failed to load the manage account page after sign In.");
		}
	}
	
	/* BRM - 5419 Verify whether after sign in the My account page is displaying as per the creative.*/
	public void myAccountLandingPageDetails()
	{
		ChildCreation("BRM - 5419 Verify whether after sign in the My account page is displaying as per the creative.");
		if(signedIn==true)
		{
			try
			{
				String pgTitle = BNBasicfeature.getExcelVal("BRM5419", sheet, 2); 
				String[] formCaption = BNBasicfeature.getExcelVal("BRM5419", sheet, 3).split("\n");
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				
				if(BNBasicfeature.isElementPresent(myAccountPageTitle))
				{
					Pass("The user is redirected to the My Account Page.");
					log.add("The Expected Title was : " + pgTitle);
					String title = myAccountPageTitle.getText();
					log.add("The Actual Title is : " + title);
					if(pgTitle.contains(title))
					{
						Pass("The My Account title match the expected content.",log);
					}
					else
					{
						Fail("The My Account title does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The User is not navigated to the My Accounts Page.");
				}
				
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						for(int i = 0; i<formCaption.length; i++)
						{
							String expTitle = formCaption[i].toString();
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
							}
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5419 Issue ." + e.getMessage());
				Exception(" BRM - 5419 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5421 Verify the ORDER HISTORY label is displaying as per the creative after sign in.*/
	public void myAccountLandingPageOrderDetails()
	{
		ChildCreation(" BRM - 5421 Verify the ORDER HISTORY label is displaying as per the creative after sign in.");
		if(signedIn==true)
		{
			try
			{
				String exTitle = BNBasicfeature.getExcelVal("BRM5421", sheet, 3); 
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							String expTitle = exTitle;
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5421 Issue ." + e.getMessage());
				Exception(" BRM - 5421 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5422 Verify whether is it able to view all the fields displaying in the My account landing page by scrolling it down and upwards.*/
	public void myAccountLandingPageScroll()
	{
		ChildCreation("BRM - 5422 Verify whether is it able to view all the fields displaying in the My account landing page by scrolling it down and upwards.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(myAccountFooterContainer))
				{
					BNBasicfeature.scrolldown(myAccountFooterContainer, driver);
					Pass( "The user was able to scroll down to the footer section.");
				}
				else
				{
					Fail( "The Footer Section Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(header))
				{
					BNBasicfeature.scrolldown(header, driver);
					Pass( "The user was able to scroll up to the header section.");
				}
				else
				{
					Fail( "The Header Section Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5422 Issue ." + e.getMessage());
				Exception(" BRM - 5422 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5445 Verify on clicking the Wishlist section has wishlist drop-down, view wishlist button and Create wishlist link.*/
	public void myAccountLandingPageWishlist()
	{
		ChildCreation("BRM - 5445 Verify on clicking the Wishlist section has wishlist drop-down, view wishlist button and Create wishlist link.");
		if(signedIn==true)
		{
			try
			{
				String exTitle = BNBasicfeature.getExcelVal("BRM5445", sheet, 2); 
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							String expTitle = exTitle;
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								if(BNBasicfeature.isElementPresent(wishListContainer))
								{
									Pass( "The Wishlist Container is displayed.");
									if(BNBasicfeature.isElementPresent(wishListDD))
									{
										Pass( "The Wishlist drop down is displayed.");
									}
									else
									{
										Fail( "The Wishlist drop down is not displayed.");
									}
									
									if(BNBasicfeature.isElementPresent(wishListViewWishlistBtn))
									{
										Pass( "The View Wishlist button is displayed.");
									}
									else
									{
										Fail( "The View Wishlist button is not displayed.");
									}
									
									if(BNBasicfeature.isElementPresent(wishListCreateWishlistBtn))
									{
										Pass( "The Create Wishlist button is displayed.");
									}
									else
									{
										Fail( "The Create Wishlist button is not displayed.");
									}
								}
								else
								{
									Fail( "The Wishlist Container is not displayed.");
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5445 Issue ." + e.getMessage());
				Exception(" BRM - 5445 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5423 Verify whether all the fields labels are displaying in uppercase in my account landing page as per the creative.*/
	public void myAccountLandingPageCaseSensitive()
	{
		ChildCreation(" BRM - 5423 Verify whether all the fields labels are displaying in uppercase in my account landing page as per the creative.");
		if(signedIn==true)
		{
			try
			{
				String[] formCaption = BNBasicfeature.getExcelVal("BRM5419", sheet, 3).split("\n");
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						for(int i = 0; i<formCaption.length; i++)
						{
							String expTitle = formCaption[i].toString();
							boolean titleFound = getLandingPageFormValue(formCaption[i]);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								if(upperCase==true)
								{
									Pass( "The Expected title is displayed in the Upper case value.",log);
								}
								else
								{
									Fail( "The Expected title is not displayed in the Upper case value.",log);
								}
							}
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5423 Issue ." + e.getMessage());
				Exception(" BRM - 5423 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5424 Verify whether the recent order is displaying in Order History section in My account landing page.*/
	public void myAccountLandingPageOrderHistoryDisplay()
	{
		ChildCreation("BRM - 5424 Verify whether the recent order is displaying in Order History section in My account landing page.");
		if(signedIn==true)
		{
			String val = "";
			try
			{
				String noOrdVal = BNBasicfeature.getExcelVal("BRM5424", sheet, 2);
				if(BNBasicfeature.isListElementPresent(myAccFormLandingPageOrderModuleDet))
				{
					val = "";
					for(int i = 0; i<myAccFormLandingPageOrderModuleDet.size(); i++)
					{
						val = myAccFormLandingPageOrderModuleDet.get(i).getText();
						if(val.isEmpty())
						{
							continue;
						}
						if(noOrdVal.contains(val))
						{
							noOrder = true;
							break;
						}
						else if(val.contains("$"))
						{
							orderPresent = true;
							break;
						}
						else
						{
							continue;
						}
					}
				}
				else if(BNBasicfeature.isListElementPresent(myAccFormLandingPageOrderModuleDet1))
				{
					val = "";
					for(int i = 0; i<myAccFormLandingPageOrderModuleDet.size(); i++)
					{
						val = myAccFormLandingPageOrderModuleDet.get(i).getText();
						if(noOrdVal.contains(val))
						{
							noOrder = true;
							break;
						}
						else if(val.contains("$"))
						{
							orderPresent = true;
							break;
						}
						else
						{
							continue;
						}
					}
				}
				else
				{
					Fail(" The Order Title list is not displayed.");
				}
				
					
				if((noOrder==true))
				{
					log.add( "The Expected alert was : " + noOrdVal);
					Pass( "The Order is not done for the account and the appropriate alert is displayed.",log);
				}
				else if(orderPresent==true)
				{
					log.add( "The Expected alert was : " + val);
					Pass( "The Order is done for the account and the appropriate alert is displayed.",log);
				}
				else
				{
					Fail( "The No Order History and Order Present alert is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5424 Issue ." + e.getMessage());
				Exception(" BRM - 5424 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}

	/* BRM - 5427 Verify whether values against all the fields is displaying in green colored text in my account landing page.*/
	public void myAccountLandingPageTextColor()
	{
		ChildCreation("BRM - 5427 Verify whether values against all the fields is displaying in green colored text in my account landing page.");
		if(signedIn==true)
		{
			try
			{
				boolean coloFound = false;
				String expColor = BNBasicfeature.getExcelVal("BRM5427", sheet, 2);
				if(orderPresent==true)
				{
					if((BNBasicfeature.isListElementPresent(myAccountOrderDetailsText))||(BNBasicfeature.isListElementPresent(myAccountOrderDetailsText1)))
					{
						coloFound = getLandingPageTextColor(myAccountOrderDetailsText, myAccountOrderDetailsText1,expColor);
						if(coloFound==true)
						{
							Pass( "The Order History Color is displayed in the expected color.");
						}
						else
						{
							Fail( "The Order History Color is not displayed in the expected color.");
						}
					}
					else
					{
						Fail( "The Order History text is not displayed.");
					}
				}
				else
				{
					Skip( "The Order is not present for the account .");
				}
				
				if((BNBasicfeature.isListElementPresent(myAccountAccountDetailsText))||(BNBasicfeature.isListElementPresent(myAccountAccountDetailsText1)))
				{
					coloFound = getLandingPageTextColor(myAccountAccountDetailsText, myAccountAccountDetailsText1,expColor);
					if(coloFound==true)
					{
						Pass( "The My Account text Color is displayed in the expected color.");
					}
					else
					{
						Fail( "The My Account text is not displayed in the expected color.");
					}
				}
				else
				{
					Fail( "The My Account text is not displayed.");
				}
				
				if((BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText))||(BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText1)))
				{
					coloFound = getLandingPageTextColor(myAccountPaymentDetailsText, myAccountPaymentDetailsText1,expColor);
					if(coloFound==true)
					{
						Pass( "The Payment Header text Color is displayed in the expected color.");
					}
					else
					{
						Fail( "The Payment Header text is not displayed in the expected color.");
					}
				}
				else
				{
					Fail( "The Payment text is not displayed.");
				}
				
				if((BNBasicfeature.isListElementPresent(myAccountAddressDetailsText))||(BNBasicfeature.isListElementPresent(myAccountAddressDetailsText1)))
				{
					coloFound = getLandingPageTextColor(myAccountAddressDetailsText, myAccountAddressDetailsText1,expColor);
					if(coloFound==true)
					{
						Pass( "The Address Desc text Color is displayed in the expected color.");
					}
					else
					{
						Fail( "The Address Desc text is not displayed in the expected color.");
					}
				}
				else
				{
					Fail( "The Address Desc  is not displayed.");
				}
				
				if((BNBasicfeature.isElementPresent(myAccountMembership)||(BNBasicfeature.isElementPresent(myAccountMembership1))))
				{
					membershipPresent = true;
					if((BNBasicfeature.isListElementPresent(myAccountMembershipDetailsText))||(BNBasicfeature.isListElementPresent(myAccountMembershipDetailsText1)))
					{
						coloFound = getLandingPageTextColor(myAccountMembershipDetailsText, myAccountMembershipDetailsText1,expColor);
						if(coloFound==true)
						{
							Pass( "The Membership Desc text Color is displayed in the expected color.");
						}
						else
						{
							Fail( "The Membership Desc text is not displayed in the expected color.");
						}
					}
					else
					{
						Fail( "The Membership Desc  is not displayed.");
					}
				}
				else
				{
					Skip( "The Membership is not added to the page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5427 Issue ." + e.getMessage());
				Exception(" BRM - 5427 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}

	/* BRM - 5428 Verify whether is it displaying > arrows for each section with vertically aligned to the field content.*/
	public void myAccountLandinPageSideArrow()
	{
		ChildCreation("BRM - 5428 Verify whether is it displaying > arrows for each section with vertically aligned to the field content.");
		if(signedIn==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
				{
					if(BNBasicfeature.isListElementPresent(myAccountSideArrow))
					{
						for(int i = 0; i<myAccountSideArrow.size(); i++)
						{
							if(BNBasicfeature.isElementPresent(myAccountSideArrow.get(i)))
							{
								BNBasicfeature.scrolldown(myAccountSideArrow.get(i), driver);
								Pass( "The Navigating arrow is displayed.");
							}
							else
							{
								Fail( "The My Account Navigating Arrow is not dispalyed.");
							}
						}
						BNBasicfeature.scrollup(header, driver);
					}
					else
					{
						Fail( "The My Account Navigating Arrow is not dispalyed.");
					}
				}
				else
				{
					Fail( "The MyAccount modules are not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5428 Issue ." + e.getMessage());
				Exception(" BRM - 5428 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}

	/* BRM - 5430 Verify the ellipsis is displaying for the payment method field if the content is lengthy as per the creative*/
	/* BRM - 5431 Verify the ellipsis is displaying for the Shipping Method field if the content is lengthy as per the creative.*/
	public void myAccountLandingPageAddressOverflow()
	{
		ChildCreation(" BRM - 5430 Verify the ellipsis is displaying for the payment method field if the content is lengthy as per the creative.");
		boolean BRM5431 = false;
		String txt = "";
		if(signedIn==true)
		{
			boolean overFlow = false;
			try
			{
				if((BNBasicfeature.isListElementPresent(myAccountAddressDetailsText))||(BNBasicfeature.isListElementPresent(myAccountAddressDetailsText1)))
				{
					for(int i = 0; i<myAccountAddressDetailsText.size(); i++)
					{
						overFlow = myAccountAddressDetailsText.get(i).getCssValue("text-overflow").contains("ellipsis");
						txt = myAccountAddressDetailsText.get(i).getText();
						log.add( "The Displayed address is : " + txt);
						if(overFlow==true)
						{
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(overFlow==false)
					{
						for(int i = 0; i<myAccountAddressDetailsText1.size(); i++)
						{
							overFlow = myAccountAddressDetailsText1.get(i).getCssValue("text-overflow").contains("ellipsis");
							txt = myAccountAddressDetailsText1.get(i).getText();
							log.add( "The Displayed address is : " + txt);
							if(overFlow==true)
							{
								break;
							}
							else
							{
								continue;
							}
						}
					}
					
					if(overFlow==true)
					{
						Pass( "The text overflow is set to ellipses when the address field is more.",log);
						BRM5431 = true;
					}
					else
					{
						Skip( "The text overflow is not set to ellipses when the address field is more or the address length is small. The displayed address is : " + txt);
					}
				}
				else
				{
					Fail( "The Address Desc  is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5430 Issue ." + e.getMessage());
				Exception(" BRM - 5430 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
		
		/* BRM - 5431 Verify the ellipsis is displaying for the Shipping Method field if the content is lengthy as per the creative.*/
		ChildCreation(" BRM - 5431 Verify the ellipsis is displaying for the Shipping Method field if the content is lengthy as per the creative.");
		if(signedIn==true)
		{
			if(BRM5431==true)
			{
				Pass( "The text overflow is set to ellipses when the address field is more.",log);
			}
			else
			{
				Skip( "The text overflow is not set to ellipses when the address field is more or the address length is small. The displayed address is : " + txt);
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5432 Verify whether is it displaying the available gift cards in Gift cards section.*/
	public void myAccoundLandingPageGiftCards()
	{
		ChildCreation(" BRM - 5432 Verify whether is it displaying the available gift cards in Gift cards section.");
		if(signedIn==true)
		{
			try
			{
				String expTitle = BNBasicfeature.getExcelVal("BRM5432", sheet, 3);
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						boolean titleFound = getLandingPageFormValue(expTitle);
						log.add( "The Expected title was : " + expTitle);
						log.add("The actual title is : " + actTitle);
						if(titleFound==false)
						{
							Fail( "The Expected title and the actual title does not matches.",log);
						}
						else
						{
							Pass( "The Expected title and the actual title matches.",log);
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5432 Issue ." + e.getMessage());
				Exception(" BRM - 5432 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5433 Verify whether is it displaying the available Membership details in Membership section.*/
	public void myAccoundLandingPageMembershipCards()
	{
		ChildCreation(" BRM - 5433 Verify whether is it displaying the available Membership details in Membership section.");
		if(signedIn==true)
		{
			try
			{
				String exTitle = BNBasicfeature.getExcelVal("BRM5433", sheet, 3);
				boolean titleFound = false;
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							String expTitle = exTitle;
							titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
				
				if(titleFound==true)
				{
					if(BNBasicfeature.isElementPresent(myAccFormContainer))
					{
						Pass( "The My Account Form Container is displayed.");
						if(membershipPresent==true)
						{
							if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
							{
								if(BNBasicfeature.isListElementPresent(myAccountMembershipDetails))
								{
									log.add( "The Membership Details is displayed.");
									for(int i = 0; i<myAccountMembershipDetails.size(); i++)
									{
										if(BNBasicfeature.isElementPresent(myAccountMembershipDetails.get(i)))
										{
											Pass( "The Membership Details is displayed.");
										}
										else
										{
											Fail( "The Membership details is not displayed.");
										}
									}
								}
								else 
								{
									Fail( "The Membership details is not displayed.");
								}
							}
							else if(BNBasicfeature.isElementPresent(myAccountMembership1))
							{
								log.add( "The Membership Details is displayed.");
								for(int i = 0; i<myAccountMembershipDetails.size(); i++)
								{
									if(BNBasicfeature.isElementPresent(myAccountMembershipDetails.get(i)))
									{
										Pass( "The Membership Details is displayed.");
									}
									else
									{
										Fail( "The Membership details is not displayed.");
									}
								}
							}
							else
							{
								Skip( "The Membership details is not added.");
							}
						}
						else if((BNBasicfeature.isElementPresent(myAccountMembership))||(BNBasicfeature.isElementPresent(myAccountMembership1)))
						{
							membershipPresent = true;
							if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
							{
								if(BNBasicfeature.isElementPresent(myAccountMembership))
								{
									membershipPresent = true;
									log.add("The Membership Details is displayed.");
									if(BNBasicfeature.isListElementPresent(myAccountMembershipDetails))
									{
										log.add( "The Membership Details is displayed.");
										for(int i = 0; i<myAccountMembershipDetails.size(); i++)
										{
											if(BNBasicfeature.isElementPresent(myAccountMembershipDetails.get(i)))
											{
												Pass( "The Membership Details is displayed.");
											}
											else
											{
												Fail( "The Membership details is not displayed.");
											}
										}
									}
									else 
									{
										Fail( "The Membership details is not displayed.");
									}
								}
								else if(BNBasicfeature.isElementPresent(myAccountMembership1))
								{
									membershipPresent = true;
									log.add( "The Membership Details is displayed.");
									for(int i = 0; i<myAccountMembershipDetails.size(); i++)
									{
										if(BNBasicfeature.isElementPresent(myAccountMembershipDetails.get(i)))
										{
											Pass( "The Membership Details is displayed.");
										}
										else
										{
											Fail( "The Membership details is not displayed.");
										}
									}
								}
								else
								{
									Skip( "The Membership details is not added.");
								}
							}
							else
							{
								Fail( "The My Account Form Container List is not displayed.");
							}		
						}
						else
						{
							Skip( "The Membership details is not added.");
						}
					}
					else
					{
						Fail( "The My Account Form Container is not displayed.");
					}
				}
				else
				{
					Fail( "The Membership Title is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5433 Issue ." + e.getMessage());
				Exception(" BRM - 5433 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
		
	/* BRM - 5434 Verify the details displaying in the Membership section is displaying as per the creative.*/
	public void myAccountLandingPageMembeshipTextColor()
	{
		ChildCreation("BRM - 5434 Verify the details displaying in the Membership section is displaying as per the creative.");
		if(signedIn==true)
		{
			try
			{
				String[] expColor = BNBasicfeature.getExcelVal("BRM5434", sheet, 2).split("\n");
				if(membershipPresent==true)
				{
					Pass( "The Membership is displayed.");
					if(BNBasicfeature.isListElementPresent(myAccountMembershipDetails))
					{
						log.add( "The Membership Details is displayed.");
						for(int i = 0; i<myAccountMembershipDetails.size(); i++)
						{
							if(BNBasicfeature.isElementPresent(myAccountMembershipDetails.get(i)))
							{
								Pass( "The Membership Details is displayed.");
								
								String colVal =  myAccountMembershipDetails.get(i).getCssValue("color");
								String actCol = BNBasicfeature.colorfinder(colVal);
								if(actCol.equals(expColor[0]))
								{
									log.add("The expected color was : " + expColor[0].toString());
									log.add("The actual color is : " + actCol);
									Pass( "The Membership Color match with the expected color.",log);
								}
								else if(actCol.equals(expColor[1]))
								{
									log.add("The expected color was : " + expColor[1].toString());
									log.add("The actual color is : " + actCol);
									Pass( "The Membership Color match with the expected color.",log);
								}
								else
								{
									log.add("The expected color was : " + expColor[0].toString());
									log.add("The expected color was : " + expColor[1].toString());
									log.add("The actual color is : " + actCol);
									Fail( "The Membership Color does not match with the expected color.");
								}
							}
							else
							{
								Fail( "The Membership details is not displayed.");
							}
						}
					}
					else 
					{
						Fail( "The Membership details is not displayed.");
					}
				}
				else
				{
					Skip( "The Membership details is not added.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5434 Issue ." + e.getMessage());
				Exception(" BRM - 5434 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5436 Verify on clicking the Order number link in Order History section is it redirecting to the Order details page.*/
	public void myAccountMyOrderNavigation()
	{
		ChildCreation(" BRM - 5436 Verify on clicking the Order number link in Order History section is it redirecting to the Order details page.");
		//signedIn = true;
		if(signedIn==true)
		{
			try
			{
				orderHistoryLoaded = false;
				String exTitle = BNBasicfeature.getExcelVal("BRM5421", sheet, 3);
				String orderTitle = BNBasicfeature.getExcelVal("BRM5436", sheet, 2); 
				wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							String expTitle = exTitle;
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountMyOrderPageTitle),
										ExpectedConditions.visibilityOf(orderContainerOrderSort)
									)
								);
								
								if((BNBasicfeature.isElementPresent(myAccountMyOrderPageTitle))||(BNBasicfeature.isElementPresent(myAccountPageTitle))||(BNBasicfeature.isElementPresent(orderContainerOrderSort)))
								{
									Pass( "The User is navigated to the My Orders Page.");
									log.add("The Expected tile is  : " + orderTitle);
									try
									{
										String actTitle = myAccountMyOrderPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(orderTitle.contains(actTitle))
										{
											orderHistoryLoaded = true;
										}
										else
										{
											orderHistoryLoaded = false;
										}
									}
									catch(Exception e)
									{
										String actTitle = myAccountPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(orderTitle.contains(actTitle))
										{
											orderHistoryLoaded = true;
										}
										else
										{
											orderHistoryLoaded = false;
										}
									}
								}
								else if(BNBasicfeature.isElementPresent(myAccountPageTitle))
								{
									log.add("The Expected tile is  : " + orderTitle);
									try
									{
										String actTitle = myAccountPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(orderTitle.contains(actTitle))
										{
											orderHistoryLoaded = true;
										}
										else
										{
											orderHistoryLoaded = false;
										}
									}
									catch(Exception e)
									{
										String actTitle = myAccountMyOrderPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(orderTitle.contains(actTitle))
										{
											orderHistoryLoaded = true;
										}
										else
										{
											orderHistoryLoaded = false;
										}
									}
								}
								else
								{
									Fail( "The User is not navigated to the Order History Page.");
								}
								
								if(orderHistoryLoaded==true)
								{
									Pass("The Order History Page is loaded.",log);
								}
								else
								{
									Fail( "The User is not navigated to the Order History page.",log);
									if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
									{
										orderHistoryLoaded = true;
									}
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5436 Issue ." + e.getMessage());
				Exception(" BRM - 5436 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 5449 Order History: Verify the fields available in Order history page.*/
	public void myAccountOrderHistoryFields()
	{
		ChildCreation("BRM - 5449 Order History: Verify the fields available in Order history page.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
				{
					Pass("The Order Sort Drop down is displayed.");
				}
				else
				{
					Fail( "The Order Sort drop down is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
				{
					Pass("The Find Button in the Search Container is displayed.");
				}
				else
				{
					Fail("The Find Button in the Search Container is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
				{
					Pass("The Order Conatiner filter search container is displayed.");
				}
				else
				{
					Fail("The Order Conatiner filter search container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5449 Issue ." + e.getMessage());
				Exception(" BRM - 5449 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to Sign In into the My Account Page.");
		}
	}
	
	/* BRM - 2692 Verify that for registered user, the expanded "Orders" option should display following options and should match with the creatives.*/
	/* BRM - 5455 Order History: Verify that "Find" button should be highlighted in Green color.*/
	public void myAccountOrderPageOrderDetails()
	{
		ChildCreation("BRM - 2692 Verify that for registered user, the expanded Orders option should display following options and should match with the creatives.");
		boolean BRM5455 = false;
		Actions act = new Actions(driver);
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMOrderSortDropDown", sheet, 5);
				String[] Val = cellVal.split("\n");
				String cellVal1 = BNBasicfeature.getExcelVal("BRMOrderSearchContainer", sheet, 5);
				String[] Val1 = cellVal1.split("\n");
				String expColor = BNBasicfeature.getExcelVal("BRMOrderSearchContainer", sheet, 6);
				if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
				{
					Pass("The Order Sort Drop down is displayed.");
					Select sel = new Select(orderContainerOrderSort);
					String defSel = sel.getFirstSelectedOption().getText();
					log.add("The Expected Default Selection Value was: " + Val[0]);
					log.add("The Actual Default Selection Value is : " + defSel);
					
					if(Val[0].contains(defSel))
					{
						Pass("The Default Selection is as Expected.",log);
					}
					else
					{
						Fail("The Default Selection is as not Expected.",log);
					}
					
					act.moveToElement(orderContainerOrderSort).click().build().perform();
					//orderContainerOrderSort.click();
					Thread.sleep(100);
					int size = sel.getOptions().size();
					for(int i = 1; i<=size;i++)
					{
						String srtType = sel.getOptions().get(i-1).getText();
						log.add("The expected sort type is : " + Val[i-1]);
						log.add("The actual sort type is : " + srtType);
						if(Val[i-1].contains(srtType))
						{
							Pass("The sort option is as expected.",log);
						}
						else
						{
							Fail("There is mismatch in the sort option.",log);
						}
					}
					
					act.moveToElement(orderContainerOrderSort).click().build().perform();
					//orderContainerOrderSort.click();
					Thread.sleep(100);
					
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
					{
						Pass("The Order Conatiner filter search container is displayed.");
						if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerDefaultText))
						{
							Pass("The Default text in the Search Container is displayed.");
							log.add("The Expected Value was : " + Val1[0]);
							String title = orderContainerFilterSearchContainerDefaultText.getText();
							log.add("The Actual Value is : " + title);
							if(Val1[0].contains(title))
							{
								Pass("The Default Text match the expected content.",log);
							}
							else
							{
								Fail("The Default Text does not match the expected content.",log);
							}
						}
						else
						{
							Fail("The Default text in the Search Container is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
						{
							Pass("The Find Button in the Search Container is displayed.");
							log.add("The Expected Value was : " + Val1[1]);
							String title = orderContainerFilterSearchContainerFindBtn.getAttribute("value");
							log.add("The Actual Value is : " + title);
							if(Val1[1].contains(title))
							{
								Pass("The Find Button Caption match the expected content.",log);
							}
							else
							{
								Fail("The Find Button Caption does not match the expected content.",log);
							}
							
							String csVal = orderContainerFilterSearchContainerFindBtn.getCssValue("background").substring(0, 16);
							Color colhex = Color.fromString(csVal);
							String hexCode = colhex.asHex();
							log.add("The expected button color is : " + expColor);
							log.add("The actual button color is : " + hexCode);
							if(expColor.equals(hexCode))
							{
								Pass("The button color matches.",log);
								BRM5455 = true;
							}
							else
							{
								Fail("The button color does not matches.",log);
							}
						}
						else
						{
							Fail("The Find Button in the Search Container is not displayed.");
						}
						
						if(orderPresent==false)
						{
							if(BNBasicfeature.isElementPresent(orderContainerNoOrders))
							{
								Pass("The No Order Alert for the account is displayed.");
								log.add("The Expected alert was : " + Val[2]);
								String alert = orderContainerNoOrders.getText();
								if(Val1[2].contains(alert))
								{
									Pass("The alert matches the expected content .",log);
								}
								else
								{
									Fail("There is mismatch in the alert.",log);
								}
							}
							else if(BNBasicfeature.isElementPresent(orderContainerOrders))
							{
								Pass("The Order Details for the account is displayed.");
								if(BNBasicfeature.isListElementPresent(orderContainerOrderList))
								{
									Pass("The Order List details is displayed.");
									for(int i = 0; i<orderContainerOrderList.size();i++)
									{
										String details = orderContainerOrderList.get(i).getText();
										if(details.isEmpty())
										{
											log.add("The Order Details are : " + details);
											Fail("The displayed Order details is empty :",log);
										}
										else
										{
											log.add("The Order Details are : " + details);
											Pass("The displayed Order details are :",log);
										}
									}
								}
								else
								{
									Fail("The Order List details is not displayed.");
								}
							}
							else
							{
								Fail("There is something wrong.No Order alert is not displayed or Order details is not displayed.");
							}
						}
						else
						{
							if(BNBasicfeature.isElementPresent(orderContainerOrders))
							{
								Pass("The Order Details for the account is displayed.");
								if(BNBasicfeature.isListElementPresent(orderContainerOrderList))
								{
									Pass("The Order List details is displayed.");
									for(int i = 0; i<orderContainerOrderList.size();i++)
									{
										String details = orderContainerOrderList.get(i).getText();
										if(details.isEmpty())
										{
											log.add("The Order Details are : " + details);
											Fail("The displayed Order details is empty :",log);
										}
										else
										{
											log.add("The Order Details are : " + details);
											Pass("The displayed Order details are :",log);
										}
									}
								}
								else
								{
									Fail("The Order List details is not displayed.");
								}
							}
							else
							{
								Fail( "The Orders is not displayed.");
							}
						}
					}
					else
					{
						Fail("The Order Conatiner filter search container is not displayed.");
					}
				}
				else
				{
					Fail("The Order Sort Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2692 Issue ." + e.getMessage());
				Exception(" BRM - 2692 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the Order History Page from the My Account Page.");
		}
	
		/* BRM - 5455 Order History: Verify that Find button should be highlighted in Green color.*/
		ChildCreation(" BRM - 5455 Order History: Verify that Find button should be highlighted in Green color.");
		if(orderHistoryLoaded==true)
		{
			if(BRM5455==true)
			{
				Pass("The Find button color matches.",log);
				BRM5455 = true;
			}
			else
			{
				Fail("The Find button color does not matches.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 5452 Order History: Verify that Search Order Number inner text is displaying inside the Search order number text field.*/
	public void myAccountSearchDefaultText()
	{
		ChildCreation("BRM - 5452 Order History: Verify that Search Order Number inner text is displaying inside the Search order number text field.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal1 = BNBasicfeature.getExcelVal("BRMOrderSearchContainer", sheet, 5);
				String[] Val1 = cellVal1.split("\n");
				if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
				{
					Pass("The Order Conatiner filter search container is displayed.");
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerDefaultText))
					{
						Pass("The Default text in the Search Container is displayed.");
						log.add("The Expected Value was : " + Val1[0]);
						String title = orderContainerFilterSearchContainerDefaultText.getText();
						log.add("The Actual Value is : " + title);
						if(Val1[0].contains(title))
						{
							Pass("The Default Text match the expected content.",log);
						}
						else
						{
							Fail("The Default Text does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The Default text in the Search Container is not displayed.");
					}
				}
				else
				{
					Fail("The Default text in the Search Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5452 Issue ." + e.getMessage());
				Exception(" BRM - 5452 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 2693 Verify that, Dropdown menu should have maximum of 3 options.*/
	public void myAccountOrderDropDown(String tc)
	{
		String tcId = null;
		if(tc.equals("BRM2693"))
		{
			tcId = "BRM - 2693 Verify that, Dropdown menu should have maximum of 3 options.";
		}
		else if(tc.equals("BRM5451"))
		{
			tcId = " BRM - 5451 Order History: Verify that, Dropdown menu should have maximum of 3 options.";
		}
		ChildCreation(tcId);
		Actions act = new Actions(driver);
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMOrderSortDropDown", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
				{
					Pass("The Order Sort Drop down is displayed.");
					Select sel = new Select(orderContainerOrderSort);
					String defSel = sel.getFirstSelectedOption().getText();
					log.add("The Expected Default Selection Value was: " + Val[0]);
					log.add("The Actual Default Selection Value is : " + defSel);
					
					if(Val[0].contains(defSel))
					{
						Pass("The Default Selection is as Expected.",log);
					}
					else
					{
						Fail("The Default Selection is as not Expected.",log);
					}
					
					act.moveToElement(orderContainerOrderSort).click().build().perform();
					//orderContainerOrderSort.click();
					Thread.sleep(100);
					
					int size = sel.getOptions().size();
					for(int i = 1; i<=size;i++)
					{
						String srtType = sel.getOptions().get(i-1).getText();
						log.add("The expected sort type is : " + Val[i-1]);
						log.add("The actual sort type is : " + srtType);
						if(Val[i-1].contains(srtType))
						{
							Pass("The sort option is as expected.",log);
						}
						else
						{
							Fail("There is mismatch in the sort option.",log);
						}
					}
					
					act.moveToElement(orderContainerOrderSort).click().build().perform();
					//orderContainerOrderSort.click();
					Thread.sleep(100);
				}
				else
				{
					Fail("The Order Sort Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 2694 Verify that Dropdown should be shown in Past 30 Days option by default.*/
	public void myAccountOrderDDDefault()
	{
		ChildCreation(" BRM - 2694 Verify that Dropdown should be shown in Past 30 Days option by default.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMOrderSortDropDown", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
				{
					Pass("The Order Sort Drop down is displayed.");
					Select sel = new Select(orderContainerOrderSort);
					String defSel = sel.getFirstSelectedOption().getText();
					log.add("The Expected Default Selection Value was: " + Val[0]);
					log.add("The Actual Default Selection Value is : " + defSel);
					
					if(Val[0].contains(defSel))
					{
						Pass("The Default Selection is as Expected.",log);
					}
					else
					{
						Fail("The Default Selection is as not Expected.",log);
					}
				}
				else
				{
					Fail("The Order Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2694 Issue ." + e.getMessage());
				Exception(" BRM - 2694 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 5450 Order History: Verify that Dropdown should be shown in "Past 30 Days" option by default*/
	public void myAccountOrderDDDefaultValue()
	{
		ChildCreation(" BRM - 5450 Verify that Dropdown should be shown in Past 30 Days option by default.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMOrderSortDropDown", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(orderContainerOrderSort))
				{
					Pass("The Order Sort Drop down is displayed.");
					Select sel = new Select(orderContainerOrderSort);
					String defSel = sel.getFirstSelectedOption().getText();
					log.add("The Expected Default Selection Value was: " + Val[0]);
					log.add("The Actual Default Selection Value is : " + defSel);
					
					if(Val[0].contains(defSel))
					{
						Pass("The Default Selection is as Expected.",log);
					}
					else
					{
						Fail("The Default Selection is as not Expected.",log);
					}
				}
				else
				{
					Fail("The Order Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5450 Issue ." + e.getMessage());
				Exception(" BRM - 5450 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 2696 Verify that when "Search Order Number" field is left empty & "Find" button is clicked then it should display the orders.*/
	public void myAccountNOOrderSrch()
	{
		ChildCreation(" BRM - 2696 Verify that when Search Order Number field is left empty & Find button is clicked then it should display the orders.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				if(noOrder==true)
				{
					String expErr = BNBasicfeature.getExcelVal("BRM2696", sheet, 5);
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
					{
						Pass("The Order Conatiner filter search container is displayed.");
						orderContainerOrderNumber.clear();
						Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
						{
							jsclick(orderContainerFilterSearchContainerFindBtn);
							Thread.sleep(1000);
							log.add("The Expected alert was : " + expErr);
							String actAlrt = orderContainerNoOrders.getText();
							log.add("The Actual alert is : " + actAlrt);
							if(expErr.contains(actAlrt))
							{
								Pass("The Alert matches the expected content.",log);
							}
							else
							{
								Fail("The Alert mismatches the expected content.",log);
							}
						}
						else
						{
							Fail("The Find Button in the Search Container is not displayed.");
						}
					}
					else
					{
						Fail("The Search Container is not displayed.");
					}
				}
				else
				{
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
					{
						Pass("The Order Conatiner filter search container is displayed.");
						orderContainerOrderNumber.clear();
						Thread.sleep(100);
						jsclick(orderContainerFilterSearchContainerFindBtn);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.visibilityOfAllElements(orderContainerOrderList));
						if(BNBasicfeature.isListElementPresent(orderContainerOrderList))
						{
							for(int i = 0; i<orderContainerOrderList.size(); i++)
							{
								String val = orderContainerOrderList.get(i).getAttribute("id");
								log.add( "The Displayed order id is : " + val);
								if(val.isEmpty())
								{
									Fail( "The Id Attribute is not displayed.",log);
								}
								else
								{
									Pass( "The Order is displayed. The displayed order is.",log);
								}
							}
						}
						else
						{
							Fail( "The Order List is not dispalyed.");
						}
					}
					else
					{
						Fail("The Search Container is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2696 Issue ." + e.getMessage());
				Exception(" BRM - 2696 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 2695 Verify that when invalid format order number is entered in "Search Order Number" field then it should thrown alert message.*/
	/* BRM - 5454 Order History: Verify that on entering Characters or Special Characters in Order Number field, it should thrown alert message.*/
	public void myAccountInvalidONOSrch()
	{
		ChildCreation("BRM - 2695 Verify that when invalid format order number is entered in Search Order Number field then it should thrown alert message.");
		boolean BRM5454 = false;
		String expErr = "";
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2695", sheet, 8);
				expErr = BNBasicfeature.getExcelVal("BRM2695", sheet, 5);
				if(BNBasicfeature.isElementPresent(orderContainerOrderNumber))
				{
					Pass("The Order Conatiner filter search container is displayed.");
					orderContainerOrderNumber.clear();
					orderContainerOrderNumber.sendKeys(cellVal);
					Thread.sleep(500);
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
					{
						jsclick(orderContainerFilterSearchContainerFindBtn);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(header),
								ExpectedConditions.visibilityOf(orderContainerNoOrders)
								)
						);
						Thread.sleep(100);
						log.add("The Expected alert was : " + expErr);
						String actAlrt = orderContainerNoOrders.getText();
						log.add("The Actual alert is : " + actAlrt);
						if(expErr.contains(actAlrt))
						{
							Pass("The Alert matches the expected content.",log);
							BRM5454 = true;
						}
						else
						{
							Fail("The Alert mismatches the expected content.",log);
						}
					}
					else
					{
						Fail("The Find Button in the Search Container is not displayed.");
					}
				}
				else
				{
					Fail("The Search Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2695 Issue ." + e.getMessage());
				Exception(" BRM - 2695 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
		
		/* BRM - 5454 Order History: Verify that on entering Characters or Special Characters in Order Number field, it should thrown alert message.*/
		ChildCreation(" BRM - 5454 Order History: Verify that on entering Characters or Special Characters in Order Number field, it should thrown alert message.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				log.add("The Expected alert was : " + expErr);
				String actAlrt = orderContainerNoOrders.getText();
				log.add("The Actual alert is : " + actAlrt);
				if(BRM5454==true)
				{
					Pass("The Alert matches the expected content.",log);
				}
				else
				{
					Fail("The Alert mismatches the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5454 Issue ." + e.getMessage());
				Exception(" BRM - 5454 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 5453 Order History: Verify that, when the Order Number is not valid, then it should shown alert message.*/
	public void myAccountInvalidONOSrch1()
	{
		ChildCreation(" BRM - 5453 Order History: Verify that, when the Order Number is not valid, then it should shown alert message.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM5453", sheet, 7);
				String expErr = BNBasicfeature.getExcelVal("BRM2695", sheet, 5);
				if(BNBasicfeature.isElementPresent(orderContainerOrderNumber))
				{
					Pass("The Order Conatiner filter search container is displayed.");
					orderContainerOrderNumber.clear();
					orderContainerOrderNumber.sendKeys(cellVal);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
					{
						jsclick(orderContainerFilterSearchContainerFindBtn);
						Thread.sleep(1000);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(header),
								ExpectedConditions.visibilityOf(orderContainerNoOrders)
								)
						);
						Thread.sleep(100);
						log.add("The Expected alert was : " + expErr);
						String actAlrt = orderContainerNoOrders.getText();
						log.add("The Actual alert is : " + actAlrt);
						if(expErr.contains(actAlrt))
						{
							Pass("The Alert matches the expected content.",log);
						}
						else
						{
							Fail("The Alert mismatches the expected content.",log);
						}
					}
					else
					{
						Fail("The Find Button in the Search Container is not displayed.");
					}
				}
				else
				{
					Fail("The Search Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5453 Issue ." + e.getMessage());
				Exception(" BRM - 5453 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 2697 Verify that, when valid order number is entered, then it should display the order details in page wise.*/
	public void myAccountOrderNoFilter()
	{
		ChildCreation("BRM - 2697 Verify that, when valid order number is entered, then it should display the order details in page wise.");
		if(orderHistoryLoaded==true)
		{
			try
			{
				//orderPresent=true;
				if(orderPresent==true)
				{
					if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainer))
					{
						orderContainerOrderNumber.clear();
						if(BNBasicfeature.isElementPresent(orderContainerFilterSearchContainerFindBtn))
						{
							jsclick(orderContainerFilterSearchContainerFindBtn);
							Thread.sleep(1000);
							wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOfAllElements(orderContainerOrderList),
									ExpectedConditions.visibilityOfAllElements(orderContainerOrderNoLists)
									)
							);
							
							if(BNBasicfeature.isListElementPresent(orderContainerOrderNoLists))
							{
								Pass( "The Order Number is displayed.");
								Random r = new Random();
								int sel = r.nextInt(orderContainerOrderNoLists.size());
								//String odId = orderContainerOrderNoLists.get(sel).getText();
								String odId = orderContainerOrderNoLists.get(sel).getText();
								log.add( "The Selected order Id  : " + odId);
								orderContainerOrderNumber.clear();
								orderContainerOrderNumber.sendKeys(odId);
								jsclick(orderContainerFilterSearchContainerFindBtn);
								Thread.sleep(1000);
								wait.until(ExpectedConditions.and(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOfAllElements(orderContainerOrderList),
										ExpectedConditions.visibilityOfAllElements(orderContainerOrderNoLists)
										)
								);
								
								if(BNBasicfeature.isListElementPresent(orderContainerOrderList))
								{
									Pass( "The filtered order list is displayed.");
									boolean odFiltered = false;
									log.add( "The Expected order id is  : " + odId);
									String currId = "";
									for( int i = 0; i<orderContainerOrderNoLists.size(); i++)
									{
										currId = orderContainerOrderNoLists.get(i).getText();
										log.add( "The Current Filtered Order Id is  : " + currId);
										if(odId.equals(currId))
										{
											odFiltered = true;
											break;
										}
										else
										{
											continue;
										}
									}
									
									if(odFiltered==true)
									{
										Pass( "The Filtered Ordered Id is displayed.",log);
									}
									else
									{
										Fail( "The Filtered Ordered Id is not displayed.",log);
									}
								}
								else
								{
									Fail( "The filtered order list is not displayed.");
								}
							}
							else
							{
								Fail( "The order number list is not displayed.");
							}
						}
						else
						{
							Fail("The Find Button in the Search Container is not displayed.");
						}
					}
					else
					{
						Fail("The Search Container is not displayed.");
					}
				}
				else
				{
					Skip( " There was no order to filter and process.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2697 Issue ." + e.getMessage());
				Exception(" BRM - 2697 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Order History Page from the My Account Page.");
		}
	}
	
	/* BRM - 5437 Verify on clicking the Name section is it redirecting to the Account settings page and displaying the options to edit the Name. */
	/* BRM - 2514 Verify that in "My Account" page the account settings option should be displayed along with the "+" icon.*/
	/* BRM - 2518 Verify that on selecting Manage Account Settings option in My Account page,it should redirect to Account settings page*/
	public void myAccountNamePageNavigation()
	{
		ChildCreation("BRM - 5437 Verify on clicking the Name section is it redirecting to the Account settings page and displaying the options to edit the Name. ");
		boolean BRM2514 = false, BRM2518 = false;
		/*signedIn = true;
		myAccountUrl = driver.getCurrentUrl();*/
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5437", sheet, 2);
					String expVal = BNBasicfeature.getExcelVal("BRM5437", sheet, 3);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(accountSettingsChangeNameFName)
									)
								);
								
								if(BNBasicfeature.isElementPresent(myAccountAccountSettingsPageTitle)||(BNBasicfeature.isElementPresent(myAccountPageTitle))||(BNBasicfeature.isElementPresent(accountSettingsChangeNameFName)))
								{
									Pass( "The User is navigated to the My Orders Page.");
									log.add("The Expected tile is  : " + expTitle);
									try
									{
										wait.until(ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle));
										String actTitle = myAccountAccountSettingsPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(expVal.contains(actTitle))
										{
											manageAccountLoaded = true;
											BRM2514 = true;
											BRM2518 = true;
										}
										else
										{
											manageAccountLoaded = false;
										}
									}
									catch(Exception e)
									{
										String actTitle = myAccountPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(expVal.contains(actTitle))
										{
											manageAccountLoaded = true;
											BRM2514 = true;
											BRM2518 = true;
										}
										else
										{
											manageAccountLoaded = false;
										}
									}
								}
								else if(BNBasicfeature.isElementPresent(myAccountPageTitle))
								{
									log.add("The Expected tile is  : " + expTitle);
									try
									{
										String actTitle = myAccountPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(expVal.contains(actTitle))
										{
											manageAccountLoaded = true;
											BRM2514 = true;
											BRM2518 = true;
										}
										else
										{
											manageAccountLoaded = false;
										}
									}
									catch(Exception e)
									{
										String actTitle = myAccountAccountSettingsPageTitle.getText();
										log.add("The Actual title is  : " + actTitle);
										if(expVal.contains(actTitle))
										{
											manageAccountLoaded = true;
											BRM2514 = true;
											BRM2518 = true;
										}
										else
										{
											manageAccountLoaded = false;
										}
									}
								}
								else
								{
									Fail( "The User is not navigated to the Manage Account page.");
								}
								
								if(manageAccountLoaded==true)
								{
									Pass("The Manage Account Page is loaded.",log);
									if(BNBasicfeature.isElementPresent(accountSettingsChangeNameFName))
									{
										Pass( "The First Name Change Field is displayed.");
									}
									else
									{
										Fail( "The First Name Change Field is not displayed.");
									}
									
									if(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))
									{
										Pass( "The Last Name Change Field is displayed.");
									}
									else
									{
										Fail( "The Last Name Change Field is not displayed.");
									}
								}
								else
								{
									Fail( "The User is not navigated to the Manage Account page.",log);
									if(BNBasicfeature.isElementPresent(accountSettingsChangeNameFName))
									{
										manageAccountLoaded = true;
									}
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5437 Issue ." + e.getMessage());
				Exception(" BRM - 5437 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in into the My Accounts Page.");
		}
		
		/* BRM - 2514 Verify that in "My Account" page the account settings option should be displayed along with the "+" icon.*/
		ChildCreation(" BRM - 2514 Verify that on selecting Account Settings option in My Account page,it should get redirect to Account Settings page.");
		if(manageAccountLoaded==true)
		{
			if(BRM2514==true)
			{
				Pass("The Manage Account Page is loaded.",log);
			}
			else
			{
				Fail( "The User is not navigated to the Manage Account page.");
			}
		}
		else
		{
			Fail( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2518 Verify that on selecting Manage Account Settings option in My Account page,it should redirect to Account settings page */
		ChildCreation(" BRM - 2518 Verify that on selecting Manage Account Settings option in My Account page,it should redirect to Account settings page .");
		if(manageAccountLoaded==true)
		{
			if(BRM2518==true)
			{
				Pass("The Manage Account Page is loaded.",log);
			}
			else
			{
				Fail( "The User is not navigated to the Manage Account page.");
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2516 Verify that when Change your name option is available in Account Settings page. */
	/* BRM - 2519 Verify that Account Settings page should have the following options : Change Your Name, Change Your Email Address, and so on.*/
	/* BRM - 2520 Verify that Save Changes and cancel button is available under "Change your name" section in "Account Settings" page.*/
	/* BRM - 2521 Verify that first name and last name text fields are available under change your name section.*/
	/* BRM - 2525 Verify that "Save Changes" button in "Change your name" option should be highlighted in Green color.*/
	/* BRM - 2526 verify that first and last name fields under Change your name section has default text.*/
	public void myAccountManageAccountSettingsDetails()
	{
		ChildCreation(" BRM - 2516 Verify that when Change your name option is available in Account Settings page. ");
		if(manageAccountLoaded==true)
		{
			boolean titFound = false;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2516", sheet, 5);
				String[] Val = cellVal.split("\n");
				String expTitle = Val[1];
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2516 Issue ." + e.getMessage());
				Exception(" BRM - 2516 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2519 Verify that Account Settings page should have the following options : Change Your Name, Change Your Email Address, and so on.*/
		ChildCreation(" BRM - 2519 Verify that Account Settings page should have the following options : Change Your Name, Change Your Email Address, and so on.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2516", sheet, 5);
				String[] Val = cellVal.split("\n");
				log.add("The Expected Title was : " + Val[1]);
				String expTitle = Val[1];
				titFound = false;
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
				
				titFound = false;
				expTitle = Val[2];
				titFound = getManageAccountPageTitles(expTitle);
				
				if(titFound==true)
				{
					Pass("The Change your Email Address is found on the Account Settings Page.",log);
				}
				else
				{
					Fail("The Change your Email Address is not found on the Account Settings Page..",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2519 Issue ." + e.getMessage());
				Exception(" BRM - 2519 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2521 Verify that first name and last name text fields are available under change your name section.*/
		ChildCreation("BRM - 2521 Verify that first name and last name text fields are available under change your name section.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameFName))
				{
					Pass("The First Name textbox in the Change Your Name section is displayed.");
				}
				else
				{
					Fail("The First Name textbox in the Change Your Name section is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))
				{
					Pass("The Last Name textbox in the Change Your Name section is displayed.");
				}
				else
				{
					Fail("The  Last Name textbox in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2521 Issue ." + e.getMessage());
				Exception(" BRM - 2521 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2526 verify that first and last name fields under Change your name section has default text.*/
		ChildCreation("BRM - 2526 verify that first and last name fields under Change your name section has default text.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameFNameDefaultTxt))
				{
					Pass("The First Name Default Text in the Change Your Name section is displayed.");
					if(accountSettingsChangeNameFNameDefaultTxt.getText().isEmpty())
					{
						Fail("The First Name Default Text is empty.");
					}
					else
					{
						Pass("The Default Text is not empty.The displayed default text is : " + accountSettingsChangeNameFNameDefaultTxt.getText());
					}
				}
				else
				{
					Fail("The First Name Default Text in the Change Your Name section is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameLNameDefaultTxt))
				{
					Pass("The Last Name Default Text in the Change Your Name section is displayed.");
					if(accountSettingsChangeNameLNameDefaultTxt.getText().isEmpty())
					{
						Fail("The Last Name Default Text is empty.");
					}
					else
					{
						Pass("The Default Text is not empty.The displayed default text is : " + accountSettingsChangeNameLNameDefaultTxt.getText());
					}
				}
				else
				{
					Fail("The  Last Name Default Text in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2526 Issue ." + e.getMessage());
				Exception(" BRM - 2526 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2520 Verify that Save Changes and cancel button is available under "Change your name" section in "Account Settings" page.*/
		ChildCreation("BRM - 2520 Verify that Save Changes and cancel button is available under Change your name section in Account Settings page.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSubmitBtn))
				{
					Pass("The Save Change button in the Change Your Name section is displayed.");
				}
				else
				{
					Fail("The Save Change button in the Change Your Name section is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameCancelBtn))
				{
					Pass("The Cancel button in the Change Your Name section is displayed.");
				}
				else
				{
					Fail("The Cancel button in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2520 Issue ." + e.getMessage());
				Exception(" BRM - 2520 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2525 Verify that "Save Changes" button in "Change your name" option should be highlighted in Green color.*/
		ChildCreation("BRM - 2525 Verify that Save Changes button in Change your name option should be highlighted in Green color.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2525", sheet, 6);
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSubmitBtn))
				{
					Pass("The Save Change button in the Change Your Name section is displayed.");
					String btnColor = accountSettingsChangeNameSubmitBtn.getCssValue("background-color");
					Color colcnvt = Color.fromString(btnColor);
					String trkBtncolor = colcnvt.asHex();
					log.add("The Expected Color was : " + expColor);
					log.add("The Actual Color is  : " + trkBtncolor);
					if(expColor.contains(trkBtncolor))
					{
						Pass("The Button Color match the expected content.",log);
					}
					else
					{
						Fail("The Button Color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Save Change button in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2525 Issue ." + e.getMessage());
				Exception(" BRM - 2525 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2522 Verify that "First Name" and the "Last Name" textbox should accept maximum of 40 characters.*/
	public void myAccountFLNameLimit()
	{
		ChildCreation(" BRM - 2522 Verify that First Name and the Last Name textbox should accept maximum of 40 characters.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2522", sheet, 5);
				String[] Val = cellVal.split("\n");
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameFName))
					{
						Pass("The First Name textbox in the Change Your Name section is displayed.");
						accountSettingsChangeNameFName.clear();
						accountSettingsChangeNameFName.sendKeys(Val[i]);
						int len = accountSettingsChangeNameFName.getAttribute("value").length();
						log.add("The value from the file was : " + Val[i] + " and its length was : " + Val[i].length());
						log.add("The actual value is : " + accountSettingsChangeNameFName.getAttribute("value") + " and its length was : " + len);
						if(len<=40)
						{
							Pass("The First name field accepts character less than 40.",log);
						}
						else
						{
							Fail("The First name field accepts character more than 40.",log);
						}
					}
					else
					{
						Fail("The First Name textbox in the Change Your Name section is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))
					{
						Pass("The Last Name textbox in the Change Your Name section is displayed.");
						accountSettingsChangeNameLName.clear();
						accountSettingsChangeNameLName.sendKeys(Val[i]);
						int len = accountSettingsChangeNameLName.getAttribute("value").length();
						log.add("The value from the file was : " + Val[i] + " and its length was : " + Val[i].length());
						log.add("The actual value is : " + accountSettingsChangeNameLName.getAttribute("value") + " and its length was : " + len);
						if(len<=40)
						{
							Pass("The First name field accepts character less than 40.",log);
						}
						else
						{
							Fail("The First name field accepts character more than 40.",log);
						}
					}
					else
					{
						Fail("The  Last Name textbox in the Change Your Name section is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2522 Issue ." + e.getMessage());
				Exception(" BRM - 2522 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2523 Verify that when "First Name" and "Last Name" textbox is left empty and the "Save Changes" button is clicked, then alert message should be displayed.*/
	public void myAccountFLEmptyValidation()
	{
		ChildCreation(" BRM - 2523 Verify that when First Name and Last Name textbox is left empty and the Save Changes button is clicked, then alert message should be displayed.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expAlert = BNBasicfeature.getExcelVal("BRM2523", sheet, 5);
				if((BNBasicfeature.isElementPresent(accountSettingsChangeNameFName)&&(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))))
				{
					Pass("The First Name and Last Name textbox in the Change Your Name section is displayed.");
					accountSettingsChangeNameFName.clear();
					accountSettingsChangeNameLName.clear();
					Thread.sleep(100);
					jsclick(accountSettingsChangeNameSubmitBtn);
					wait.until(ExpectedConditions.attributeContains(accountSettingsChangeNameError, "aria-atomic", "true"));
					log.add("The Expected alert was : " + expAlert);
					String actAlert = accountSettingsChangeNameError.getText();
					if(expAlert.contains(actAlert))
					{
						Pass("The expected and actual alert matches.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and actual alert.",log);
					}
				}
				else
				{
					Fail("The First Name or Last Name textbox in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2523 Issue ." + e.getMessage());
				Exception(" BRM - 2522 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2524 Verify that when special characters and numbers are typed at first in "First Name" and "Last Name" field then it should display the alert message.*/
	public void myAccountFLInvalidValidation()
	{
		ChildCreation(" BRM - 2524 Verify that when special characters and numbers are typed at first in First Name and Last Name field then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expAlert = BNBasicfeature.getExcelVal("BRM2524", sheet, 5);
				String fN = BNBasicfeature.getExcelVal("BRM2524", sheet, 11);
				String lN = BNBasicfeature.getExcelVal("BRM2524", sheet, 12);
				if((BNBasicfeature.isElementPresent(accountSettingsChangeNameFName)&&(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))))
				{
					Pass("The First Name and Last Name textbox in the Change Your Name section is displayed.");
					accountSettingsChangeNameFName.clear();
					accountSettingsChangeNameFName.sendKeys(fN);
					accountSettingsChangeNameLName.clear();
					accountSettingsChangeNameLName.sendKeys(lN);
					Thread.sleep(100);
					jsclick(accountSettingsChangeNameSubmitBtn);
					wait.until(ExpectedConditions.attributeContains(accountSettingsChangeNameError, "aria-atomic", "true"));
					log.add("The Expected alert was : " + expAlert);
					String actAlert = accountSettingsChangeNameError.getText();
					if(expAlert.contains(actAlert))
					{
						Pass("The expected and actual alert matches.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and actual alert.",log);
					}
				}
				else
				{
					Fail("The First Name or Last Name textbox in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2524 Issue ." + e.getMessage());
				Exception(" BRM - 2524 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2530 Verify that on clicking "Cancel" button in "Change your name" option the data in "First name" and "Last Name" field should be cleared and should show the saved field names.*/
	public void myAccountChangeNameCancelButton()
	{
		ChildCreation("BRM - 2530 Verify that on clicking Cancel button in Change your name option the data in First name and Last Name field should be cleared and should show the saved field names.");
		if(manageAccountLoaded==true)
		{
			String FNcurrTxt = "";
			String LNcurrTxt = "";
			String FNOrigTxt = "", LNOrigTxt = "";
			try
			{
				if((BNBasicfeature.isElementPresent(accountSettingsChangeNameFName)&&(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))))
				{
					Pass("The First Name and Last Name textbox in the Change Your Name section is displayed.");
					FNcurrTxt = accountSettingsChangeNameFName.getAttribute("value");
					log.add("The Current First Name is : " + FNOrigTxt);
					LNcurrTxt = accountSettingsChangeNameLName.getAttribute("value");
					log.add("The Current Last Name is : " + LNcurrTxt);
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameCancelBtn))
					{
						Pass("The Cancel button in the Change Your Name section is displayed.");
						jsclick(accountSettingsChangeNameCancelBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.or(
								ExpectedConditions.visibilityOf(header),
								ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeNameIndividualError)
								)
						);
						wait.until(ExpectedConditions.visibilityOf(header));
						Thread.sleep(100);
						FNOrigTxt = accountSettingsChangeNameFName.getAttribute("value");
						log.add("The Original First Name was : " + FNOrigTxt);
						LNOrigTxt = accountSettingsChangeNameLName.getAttribute("value");
						log.add("The Original Last Name was : " + LNOrigTxt);
						if(((FNcurrTxt.equals(FNOrigTxt))&&(LNcurrTxt.equals(LNOrigTxt))))
						{
							Fail("The First Name and the Last Name Value are Same and they are not resetted.",log);
						}
						else
						{
							Pass("The First Name and the Last Name Value are different and they are resetted.",log);
						}
					}
					else
					{
						Fail("The Cancel button in the Change Your Name section is not displayed.");
					}
				}
				else
				{
					Fail("The First Name or Last Name field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2530 Issue ." + e.getMessage());
				Exception(" BRM - 2530 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2527 Verify that when "Save Changes" button is clicked on entering the valid name the "We've Updated Your Name" overlay should be displayed.*/
	public void myAccountUpdatefname()
	{
		ChildCreation("BRM - 2527 Verify that when Save Changes button is clicked on entering the valid name the We've Updated Your Name overlay should be displayed.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String firstName = BNBasicfeature.getExcelVal("BRM2527", sheet, 11);
				String lastName = BNBasicfeature.getExcelVal("BRM2527", sheet, 12);
				String successAlert = BNBasicfeature.getExcelVal("BRM2527", sheet, 5);
				boolean nameChangeSuccess = false;
				if((BNBasicfeature.isElementPresent(accountSettingsChangeNameFName)&&(BNBasicfeature.isElementPresent(accountSettingsChangeNameLName))))
				{
					Pass("The First Name and Last Name textbox in the Change Your Name section is displayed.");
					accountSettingsChangeNameFName.clear();
					accountSettingsChangeNameFName.sendKeys(firstName);
					accountSettingsChangeNameLName.clear();
					accountSettingsChangeNameLName.sendKeys(lastName);
					//Thread.sleep(500);
					jsclick(accountSettingsChangeNameSubmitBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.or(
							//ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage.get(0)),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessCloseBtn)
							)
					);
					
					Thread.sleep(100);
					log.add("The Expected value was : " + successAlert);
					//if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPageTitle))
					{
						String actAlert = accountSettingsChangeNameSuccessPageTitle.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							nameChangeSuccess = true;
						}
					}
					else if(BNBasicfeature.isElementPresent(pgetitle))
					{
						//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
						String actAlert = accountSettingsChangeNameSuccessPage.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							nameChangeSuccess = true;
						}
					}
					else
					{
						Fail( "The Success Alert Title is not displayed.");
					}
					
					if(nameChangeSuccess==true)
					{
						Pass("The expected and actual alert matches.",log);
						manageAccNameUpdt = true;
					}
					else
					{
						 if(BNBasicfeature.isElementPresent(pgetitle))
						 {
							//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
							 String actAlert = accountSettingsChangeNameSuccessPage.getText();
							log.add("The Actual value is : " + actAlert);
							if(successAlert.contains(actAlert))
							{
								Pass("The expected and actual alert matches.",log);
								manageAccNameUpdt = true;
							}
							else
							{
								Fail("There is some mismatch in the expected and actual alert.",log);
							}
						 }
					}
					
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						manageAccNameUpdt = true;
					}
				}
				else
				{
					Fail("The First Name or Last Name textbox in the Change Your Name section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2527 Issue ." + e.getMessage());
				Exception(" BRM - 2527 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2528 Verify that "We've Updated Your Name" page should be displayed with "Close" button.*/
	public void myAccountNameSuccessCloseButton()
	{
		ChildCreation("BRM - 2528 Verify that We've Updated Your Name page should be displayed with Close button.");
		if(manageAccNameUpdt==true)
		{
			try
			{
				//if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else
				{
					Fail("The User is not in the Change Name Success Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2528 Issue ." + e.getMessage());
				Exception(" BRM - 2528 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User Failed to update the name in the Manage Account page.");
		}
	}
	
	/* BRM - 2529 Verify that, when the "Close" button in "We've Updated Your Name" is cliked then the should be closed displaying previous page.*/
	public void myAccountNameSuccessOverlayClose()
	{
		ChildCreation(" BRM - 2529 Verify that, when the Close button in We've Updated Your Name is cliked then the should be closed displaying previous page.");
		if(manageAccNameUpdt==true)
		{
			String title = "";
			boolean navigatedbck = false;
			int i = 1;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2516", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
				{
					Pass("The Close button in the Success page is displayed.");
					jsclick(accountSettingsChangeNameSuccessCloseBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							//ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@data-modal-name,'success')]")),
							ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle)
							)
					);
					ExpectedConditions.visibilityOf(accountSettingsChangeNameFName);
					
					if(BNBasicfeature.isElementPresent(myAccountAccountSettingsPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
						
					}
					else if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
					}
					else
					{
						Fail("The Page Title is not displayed.");
					}
					
					if(navigatedbck==true)
					{
						Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
						manageAccNameUpdt = false;
					}
					else
					{
						if(BNBasicfeature.isElementPresent(myAccountPageTitle))
						{
							Pass("The user is redirected to the Add New Payments Page.");
							log.add("The Expected Title was : " + Val[i-1]);
							title = myAccountAccountSettingsPageTitle.getText();
							log.add("The Actual Title is : " + title);
							if(Val[0].contains(title))
							{
								Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
								manageAccNameUpdt = true;
							}
							else
							{
								Fail("The Page title does not match the expected content.",log);
							}
						}
					}
				}
				else
				{
					Fail("The Close button in the Success page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2529 Issue ." + e.getMessage());
				Exception(" BRM - 2529 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the name in the Manage Account page.");
		}
	}
	
	/* BRM - 5438 Verify on clicking the Email address section is it redirecting to the Account settings page and displaying the options to edit the email address.*/
	public void myAccountEmailPageNavigation()
	{
		ChildCreation("BRM - 5438 Verify on clicking the Email address section is it redirecting to the Account settings page and displaying the options to edit the email address.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5438", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(accountSettingsChangeNewEmail)
									)
								);
								manageAccountLoaded = true;
								String exppTitle = BNBasicfeature.getExcelVal("BRM2531", sheet, 5);
								log.add("The Expected title was : " + exppTitle);
								titleFound = getManageAccountPageTitles(exppTitle);
								if(titFound==true)
								{
									Pass("The Change your name is found on the Account Settings Page.",log);
								}
								else
								{
									Fail(" The Change your name is not found on the Account Settings Page.",log);
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5438 Issue ." + e.getMessage());
				Exception(" BRM - 5438 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the name in the Manage Account page.");
		}
	}
	
	/* BRM - 2531 Verify that when "Change Your Email Address" option is available in "Account Settings" page, below change your name option.*/
	public void myAccountChangeEmail()
	{
		ChildCreation("BRM - 2531 Verify that when Change Your Email Address option is available in Account Settings page, below change your name option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				/*String currUrll = driver.getCurrentUrl();
				if(!currUrll.equals(manageAccUrl))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}*/
				
				ExpectedConditions.visibilityOf(header);
				String expTitle = BNBasicfeature.getExcelVal("BRM2531", sheet, 5);
				log.add("The Expected title was : " + expTitle);
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2531 Issue ." + e.getMessage());
				Exception(" BRM - 2531 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2532 Verify that the "Change Your Email Address" section in Account settings page should have the certain option that matches with the classic site.*/
	/* BRM - 2538 Verify that "Save Changes" button in "Change Your Email Address" option should be highlighted in Green color */
	/* BRM - 2539 Verify whether current email address is displayed under "Change Your Email Address" option.*/
	public void myAccountEmailDetails()
	{
		ChildCreation("BRM - 2532 Verify that the Change Your Email Address section in Account settings page should have the certain option that matches with the classic site.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNewEmail))
				{
					Pass("The New Email Address text field is displayed.");
				}
				else
				{
					Fail("The New Email Address text field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeCnfEmail))
				{
					Pass("The New Confirm Email Address text field is displayed.");
				}
				else
				{
					Fail("The New Confirm Email Address text field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangePass))
				{
					Pass("The Password text field is displayed.");
				}
				else
				{
					Fail("The Password text field is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeEmailSubmitBtn))
				{
					Pass("The Change Email Submit Button is displayed.");
				}
				else
				{
					Fail("The Change Email Submit Button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeEmailCancelBtn))
				{
					Pass("The Change Email Cancel Button is displayed.");
				}
				else
				{
					Fail("The Change Email Cancel button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2532 Issue ." + e.getMessage());
				Exception(" BRM - 2532 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2538 Verify that "Save Changes" button in "Change Your Email Address" option should be highlighted in Green color.*/
		ChildCreation(" BRM - 2538 Verify that Save Changes button in Change Your Email Address option should be highlighted in Green color");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2538", sheet, 6);
				if(BNBasicfeature.isElementPresent(accountSettingsChangeEmailSubmitBtn))
				{
					Pass("The Change Email Submit Button is displayed.");
					String csVal = accountSettingsChangeEmailSubmitBtn.getCssValue("background-color");
					Color colhex = Color.fromString(csVal);
					String hexCode = colhex.asHex();
					log.add("The expected button color is : " + expColor);
					log.add("The actual button color is : " + hexCode);
					if(expColor.contains(hexCode))
					{
						Pass("The button color matches.",log);
					}
					else
					{
						Fail("The button color does not matches.",log);
					}
				}
				else
				{
					Fail("The Change Email Submit Button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2538 Issue ." + e.getMessage());
				Exception(" BRM - 2538 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2539 Verify whether current email address is displayed under "Change Your Email Address" option.*/
		ChildCreation("BRM - 2539 Verify whether current email address is displayed under Change Your Email Address option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String emailidval = BNBasicfeature.getExcelVal("BRMSIgnIn", sheet, 2);
				if(BNBasicfeature.isElementPresent(accountSettingsChangeCurrentEmailContainer))
				{
					Pass("The Current Email Container is displayed.");
					String actText = accountSettingsChangeCurrentEmailContainer.getText();
					if(actText.contains(emailidval))
					{
						Pass("The Current Email Address is displayed.");
					}
					else
					{
						Fail("The Current Email Address is not displayed.");
					}
				}
				else
				{
					Fail("The Current Email Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2539 Issue ." + e.getMessage());
				Exception(" BRM - 2539 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2533 Verify that "New Email Address", "Confirm New Email Address" textbox should accept maximum of 40 characters.*/
	public void myAccEmailLengthValidation()
	{
		ChildCreation("BRM - 2533 Verify that New Email Address, Confirm New Email Address textbox should accept maximum of 40 characters.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2533", sheet, 5);
				String[] Val = cellVal.split("\n");
				
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNewEmail))
					{
						BNBasicfeature.scrolldown(accountSettingsChangeNewEmail, driver);
						Thread.sleep(100);
						Pass("The New Email Address text field is displayed.");
						accountSettingsChangeNewEmail.clear();
						accountSettingsChangeNewEmail.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = accountSettingsChangeNewEmail.getAttribute("value").length();
						log.add("The Current Value in the New Email text field is : " + accountSettingsChangeNewEmail.getAttribute("value") + " and its length is : " + len);
						if(len<=40)
						{
							Pass("The New Email Field accept character less than 40 only.",log);
						}
						else
						{
							Fail("The New Email Field accept characted more than 40.",log);
						}
					}
					else
					{
						Fail("The New Email Address text field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(accountSettingsChangeCnfEmail))
					{
						Pass("The New Confirm Email Address text field is displayed.");
						accountSettingsChangeCnfEmail.clear();
						accountSettingsChangeCnfEmail.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = accountSettingsChangeCnfEmail.getAttribute("value").length();
						log.add("The Current Value in the New Confirm Email text field is : " + accountSettingsChangeCnfEmail.getAttribute("value") + " and its length is : " + len);
						if(len<=40)
						{
							Pass("The New Confirm Email Field accept character less than 40 only.",log);
						}
						else
						{
							Fail("The New Confirm Email Field accept characted more than 40.",log);
						}
					}
					else
					{
						Fail("The New Confirm Email Address text field is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2533 Issue ." + e.getMessage());
				Exception(" BRM - 2533 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2534 Verify that Password field should accept maximum of 15 characters..*/
	public void myAccPassLengthValidation()
	{
		ChildCreation(" BRM - 2534 Verify that Password field should accept maximum of 15 characters.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2534", sheet, 5);
				String[] Val = cellVal.split("\n");
				
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(accountSettingsChangePass))
					{
						Pass("The Password text field is displayed.");
						accountSettingsChangePass.clear();
						accountSettingsChangePass.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = accountSettingsChangePass.getAttribute("value").length();
						log.add("The Current Value in the New Email text field is : " + accountSettingsChangePass.getAttribute("value") + " and its length is : " + len);
						if(len<=15)
						{
							Pass("The Password Field accept character less than 15 only.",log);
						}
						else
						{
							Fail("The Password Field accept characted more than 15.",log);
						}
					}
					else
					{
						Fail("The Password text field is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2534 Issue ." + e.getMessage());
				Exception(" BRM - 2534 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2544 Verify that on clicking Cancel button in Change your Email Address option the entered characters in all the fields should be cleared.*/
	/* BRM - 2540 Verify that whenever the Save Changes button is clicked after entering the valid email address the Update Email Warning page should be shown.*/
	/* BRM - 2537 Verify that when the special character "@" is not present in Email Address fields then it should display the alert message.*/
	public void myAccountNewEmailValidation()
	{
		ChildCreation(" BRM - 2540 Verify that whenever the Save Changes button is clicked after entering the valid email address the Update Email Warning page should be shown.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2537", sheet, 7);
				String expTitle = BNBasicfeature.getExcelVal("BRM2540", sheet, 5);
				boolean titleFound = false;
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNewEmail))
				{
					Pass("The New Email Address text field is displayed.");
					accountSettingsChangeNewEmail.clear();
					accountSettingsChangeNewEmail.sendKeys(cellVal);
					if(BNBasicfeature.isElementPresent(accountSettingsChangeEmailSubmitBtn))
					{
						jsclick(accountSettingsChangeEmailSubmitBtn);
						wait.until(ExpectedConditions.visibilityOf(accountSettingsChangeUpdateEmailAlert));
						wait.until(ExpectedConditions.attributeContains(accountSettingsChangeUpdateEmailAlert, "style", "fixed"));
						log.add("The expected alert was : " + expTitle);
						if(BNBasicfeature.isElementPresent(accountSettingsChangeUpdateEmailTitle))
						{
							Pass("The Update Email Warning is displayed.");
							String actTitle = accountSettingsChangeUpdateEmailTitle.getText();
							log.add("The actual title is : " + actTitle);
							if(actTitle.contains(expTitle))
							{
								titleFound = true;
							}
						}
						else if(BNBasicfeature.isElementPresent(pgetitle))
						{
							Pass("The Update Email Warning is displayed.");
							String actTitle = pgetitle.getText();
							log.add("The actual title is : " + actTitle);
							if(actTitle.contains(expTitle))
							{
								titleFound = true;
							}
						}
						else
						{
							Fail("The Update Email Warning is not displayed.",log);
						}
						
						if(titleFound==true)
						{
							Pass("The title matches the expected content.",log);
						}
						else
						{
							if(BNBasicfeature.isElementPresent(pgetitle))
							{
								Pass("The Update Email Warning is displayed.");
								String actTitle = pgetitle.getText();
								log.add("The actual title is : " + actTitle);
								if(actTitle.contains(expTitle))
								{
									Pass("The title matches the expected content.",log);
									titleFound = true;
								}
								else
								{
									Fail("The title does not matches the expected content.",log);
								}
							}
							else
							{
								Fail("The title does not matches the expected content.",log);
							}
						}
					}
					else
					{
						Fail("The Change Email Submit Button is not displayed.");
					}
				}
				else
				{
					Fail("The New Email Address text field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2540 Issue ." + e.getMessage());
				Exception(" BRM - 2540 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2537 Verify that when the special character @ is not present in Email Address fields then it should display the alert message. */
		ChildCreation(" BRM - 2537 Verify that when the special character @ is not present in Email Address fields then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2537", sheet, 7);
				String expAlrt = BNBasicfeature.getExcelVal("BRM2537", sheet, 5);
				log.add("The entered email from the excel file was : " + cellVal);
				log.add("The expected alert was : " + expAlrt);
				wait.until(ExpectedConditions.attributeContains(accountSettingsChangeUpdateEmailAlert, "style", "fixed"));
				Thread.sleep(100);
				jsclick(accountSettingsChangeEmailChangeSubmitBtn);
				wait.until(ExpectedConditions.visibilityOf(accountSettingsChangeUpdatePasswordAlert));
				Thread.sleep(100);
				String actAlert = accountSettingsChangeUpdatePasswordAlert.getText();
				if(actAlert.contains(expAlrt))
				{
					Pass("There is no mismatch in the expected and actual alert.",log);
				}
				else
				{
					Fail("There is some mismatch in the expected and actual alert.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2537 Issue ." + e.getMessage());
				Exception(" BRM - 2537 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2544 Verify that on clicking Cancel button in Change your Email Address option the entered characters in all the fields should be cleared.*/
		ChildCreation("BRM - 2544 Verify that on clicking Cancel button in Change your Email Address option the entered characters in all the fields should be cleared.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(accountSettingsChangeEmailCancelBtn))
				{
					Pass("The Change Email Cancel Button is displayed.");
					jsclick(accountSettingsChangeEmailCancelBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(myAccountPageTitle),
							ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeUpdateIndividualPasswordAlert)
						)
					);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
					String val = accountSettingsChangeNewEmail.getAttribute("value");
					if(val.isEmpty())
					{
						Pass("The New Email Address field is empty and the field is resetted.");
					}
					else
					{
						Fail("The New Email Address field is not empty.");
					}
					
					val = accountSettingsChangeCnfEmail.getAttribute("value");
					if(val.isEmpty())
					{
						Pass("The Confirm Email Address field is empty and the field is resetted.");
					}
					else
					{
						Fail("The Confirm Email Address field is not empty.");
					}
				}
				else
				{
					Fail("The Change Email Cancel button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2544 Issue ." + e.getMessage());
				Exception(" BRM - 2544 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 5439 Verify on clicking the Password section is it redirecting to the Account settings page and displaying the options to edit the Password.*/
	public void myAccountPasswordPageNavigation()
	{
		ChildCreation(" BRM - 5439 Verify on clicking the Password section is it redirecting to the Account settings page and displaying the options to edit the Password.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5439", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(contenPassForm)
									)
								);
								manageAccountLoaded = true;
								String exppTitle = BNBasicfeature.getExcelVal("BRM2545", sheet, 5);
								ExpectedConditions.visibilityOf(header);
								log.add("The Expected title was : " + exppTitle);
								titFound = getManageAccountPageTitles(exppTitle);
								if(titFound==true)
								{
									Pass("The Change your Passord is found on the Account Settings Page.",log);
								}
								else
								{
									Fail(" The Change your Passord is not found on the Account Settings Page.",log);
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5439 Issue ." + e.getMessage());
				Exception(" BRM - 5439 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the name in the Manage Account page.");
		}
	}
	
	/* BRM - 2545 Verify that when "Change Your Password" option is available below Change your email section in "Account Settings" page.*/
	public void myAccountChangePass()
	{
		ChildCreation(" BRM - 2545 Verify that when Change Your Password option is available below Change your email section in Account Settings page.");
		if(manageAccountLoaded==true)
		{
			try
			{
				/*String currUrll = driver.getCurrentUrl();
				if(!currUrll.equals(manageAccUrl))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}
				
				if(BNBasicfeature.isListElementPresent(accountSettingsChangeUpdateIndividualPasswordAlert))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}*/
				
				String expTitle = BNBasicfeature.getExcelVal("BRM2545", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + expTitle);
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2545 Issue ." + e.getMessage());
				Exception(" BRM - 2545 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2546 Verify that Save Changes and cancel buttons are available under "Change Your Password" section.*/
	/* BRM - 2547 Verify that the "Change Your Password" option should have the followings options & it should match with the classic site.*/
	/* BRM - 2555 Verify that "Save Changes" button in "Change Your Password" option should be highlighted in Green color.*/
	/* BRM - 2556 Verify that "Password Rules" link along with the "Information" icon should be shown above the "Save Changes" button*/
	/* BRM - 2557 Verify that when the "information" icon is clicked then it should display the certain rules.*/
	/* BRM - 2558 Verify that password fields in "Change Your Password" has default text.*/
	public void myAccountChangePassDet()
	{
		ChildCreation(" BRM - 2547 Verify that the Change Your Password option should have the followings options & it should match with the classic site");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeCurrPass))
				{
					Pass("The Current Password text is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The Current Password text is not displayed in the Change Your Password Section.");
				}
				
				if(BNBasicfeature.isElementPresent(changeNewPass))
				{
					Pass("The New Password text is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The New Password text is not displayed in the Change Your Password Section.");
				}
				
				if(BNBasicfeature.isElementPresent(changeConfPass))
				{
					Pass("The Confirm Password text is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The Confirm Password text is not displayed in the Change Your Password Section.");
				}
				
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
				
				if(BNBasicfeature.isElementPresent(changePassCancelBtn))
				{
					Pass("The Cancel button is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The Cancel button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2547 Issue ." + e.getMessage());
				Exception(" BRM - 2547 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2546 Verify that Save Changes and cancel buttons are available under "Change Your Password" section.*/
		ChildCreation("BRM - 2546 Verify that Save Changes and cancel buttons are available under Change Your Password section.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2546 Issue ." + e.getMessage());
				Exception(" BRM - 2546 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2555 Verify that "Save Changes" button in "Change Your Password" option should be highlighted in Green color.*/
		ChildCreation(" BRM - 2555 Verify that Save Changes button in Change Your Password option should be highlighted in Green color.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2555", sheet, 6);
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
					String csVal = changePassSubmitBtn.getCssValue("background-color");
					Color colhex = Color.fromString(csVal);
					String hexCode = colhex.asHex();
					log.add("The expected button color is : " + expColor);
					log.add("The actual button color is : " + hexCode);
					if(expColor.contains(hexCode))
					{
						Pass("The button color matches.",log);
					}
					else
					{
						Fail("The button color does not matches.",log);
					}
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2555 Issue ." + e.getMessage());
				Exception(" BRM - 2555 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2558 Verify that password fields in "Change Your Password" has default text.*/
		ChildCreation(" BRM - 2558 Verify that password fields in Change Your Password has default text.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeCurrPassText))
				{
					Pass("The Current Password Text is displayed.");
					String val = changeCurrPassText.getText();
					log.add("The displayed text is : " + val);
					if(val.isEmpty())
					{
						Fail("The Current Password Text is empty or not displayed.");
					}
					else
					{
						Pass("The Current password default text is displayed.",log);
					}
				}
				else
				{
					Fail("The Current Password Text is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeNewPassText))
				{
					Pass("The New Password Text is displayed.");
					String val = changeNewPassText.getText();
					log.add("The displayed text is : " + val);
					if(val.isEmpty())
					{
						Fail("The New Password Text is empty or not displayed.");
					}
					else
					{
						Pass("The New password default text is displayed.",log);
					}
				}
				else
				{
					Fail("The New Password Text is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeConfPassText))
				{
					Pass("The Confirm Password Text is displayed.");
					String val = changeConfPassText.getText();
					log.add("The displayed text is : " + val);
					if(val.isEmpty())
					{
						Fail("The Confirm Password Text is empty or not displayed.");
					}
					else
					{
						Pass("The Confirm password default text is displayed.",log);
					}
				}
				else
				{
					Fail("The Confirm Password Text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2558 Issue ." + e.getMessage());
				Exception(" BRM - 2558 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2556 Verify that "Password Rules" link along with the "Information" icon should be shown above the "Save Changes" button*/
		ChildCreation(" BRM - 2556 Verify that Password Rules link along with the Information icon should be shown above the Save Changes button.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changePassRules))
				{
					Pass("The Password Change Rules Icon is displayed.");	
				}
				else
				{
					Fail("The Password Change Rules Icon is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2556 Issue ." + e.getMessage());
				Exception(" BRM - 2556 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2557 Verify that when the "information" icon is clicked then it should display the certain rules.*/
		ChildCreation(" BRM - 2557 Verify that when the information icon is clicked then it should display the certain rules.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changePassRules))
				{
					Pass("The Password Change Rules Icon is displayed.");
					jsclick(changePassRules);
					wait.until(ExpectedConditions.visibilityOf(changePassRulesExpand));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(changePassRulesExpand))
					{
						Pass("The Change rules password information is displayed.");
					}
					else
					{
						Fail("The Change rules password information is not displayed.");
					}
					jsclick(changePassRules);
					
					wait.until(ExpectedConditions.visibilityOf(changePassRules));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(changePassRulesExpand))
					{
						Fail("The Change rules password information is displayed.");
					}
					else
					{
						Pass("The Change rules password information is not displayed.");
					}
				}
				else
				{
					Fail("The Password Change Rules Icon is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2557 Issue ." + e.getMessage());
				Exception(" BRM - 2557 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2548 Verify that, Password fields should accept maximum of 15 characters.*/
	public void myAccNewPassLimitValidation()
	{
		ChildCreation(" BRM - 2548 Verify that, Password fields should accept maximum of 15 characters.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2534", sheet, 5);
				String[] Val = cellVal.split("\n");
				
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(changeCurrPass))
					{
						Pass("The Current Password text is displayed in the Change Your Password Section.");
						changeCurrPass.clear();
						changeCurrPass.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = changeCurrPass.getAttribute("value").length();
						log.add("The Current Value in the Current Password text field is : " + changeCurrPass.getAttribute("value") + " and its length is : " + len);
						if(len<=15)
						{
							Pass("The Password Field accept character less than 15 only.",log);
						}
						else
						{
							Fail("The Password Field accept characted more than 15.",log);
						}
						changeCurrPass.clear();
					}
					else
					{
						Fail("The Current Password text is not displayed in the Change Your Password Section.");
					}
					
					if(BNBasicfeature.isElementPresent(changeNewPass))
					{
						Pass("The New Password text is displayed in the Change Your Password Section.");
						changeNewPass.clear();
						changeNewPass.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = changeNewPass.getAttribute("value").length();
						log.add("The Current Value in the New Password text field is : " + changeNewPass.getAttribute("value") + " and its length is : " + len);
						if(len<=15)
						{
							Pass("The Password Field accept character less than 15 only.",log);
						}
						else
						{
							Fail("The Password Field accept characted more than 15.",log);
						}
						changeNewPass.clear();
					}
					else
					{
						Fail("The New Password text is not displayed in the Change Your Password Section.");
					}
					
					if(BNBasicfeature.isElementPresent(changeConfPass))
					{
						Pass("The Confirm Password text is displayed in the Change Your Password Section.");
						changeConfPass.clear();
						changeConfPass.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = changeConfPass.getAttribute("value").length();
						log.add("The Current Value in the Confirm New Password text field is : " + changeConfPass.getAttribute("value") + " and its length is : " + len);
						if(len<=15)
						{
							Pass("The Password Field accept character less than 15 only.",log);
						}
						else
						{
							Fail("The Password Field accept characted more than 15.",log);
						}
						changeConfPass.clear();
					}
					else
					{
						Fail("The Confirm Password text is not displayed in the Change Your Password Section.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2548 Issue ." + e.getMessage());
				Exception(" BRM - 2548 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2549 Verify that when "Password" fields is left empty and the "Save Changes" button is clicked, then alert message should be displayed.*/
	public void myAccountEmptyPassValidation()
	{
		ChildCreation(" BRM - 2549 Verify that when Password fields is left empty and the Save Changes button is clicked, then alert message should be displayed.");
		if(manageAccountLoaded==true)
		{
			try
			{
				//String cellVal = BNBasicfeature.getExcelVal("BRM2549", sheet, 5);
				String cellVal = BNBasicfeature.getExcelVal("BRM2549", sheet, 6);
				String[] val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
					jsclick(changePassSubmitBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
					Thread.sleep(100);
					if(BNBasicfeature.isListElementPresent(changePassError))
					{
						for(int i = 0; i<val.length;i++)
						{
							String expVal = val[i];
							log.add("The Expected value was : " + expVal);
							String actVal = changePassError.get(i).getText();
							log.add("The Actual value is  : " + actVal);
							if(expVal.contains(actVal))
							{
								Pass("The expected alert and actual alert matches .",log);
							}
							else
							{
								Fail("There is some mismatch in the expected alert and actual alert.",log);
							}
						}
					}
					else
					{
						Fail("The Change password error is not displayed.");
					}
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2549 Issue ." + e.getMessage());
				Exception(" BRM - 2549 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2550 Verify that the password fields should contain atleast one numeric character and atleast one UPPER character.*/
	/* BRM - 2551 Verify that when the password field does not contain atleast one numeric and UPPER character then it should display the alert message. */
	/* BRM - 2552 Verify that when the password is less than 6 characters then it should display the alert message.*/
	/* BRM - 2553 Verify that in "Current Password" field when the password is typed incorrectly then it should display the alert message.*/
	/* BRM - 2554 Verify that when the New Password, and the Confirm Password fields does not match then it should display the alert message.*/
	public void myAccountPassStrucValidation()
	{
		ChildCreation(" BRM - 2550 Verify that the password fields should contain atleast one numeric character and atleast one UPPER character.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2550", sheet, 5);
				String pass = BNBasicfeature.getExcelVal("BRM2550", sheet, 7);
				changeCurrPass.clear();
				changeCurrPass.sendKeys(pass);;
				changeNewPass.clear();
				changeNewPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + pass);
				log.add("The Expected alert was : " + cellVal);
				changeConfPass.clear();
				changeConfPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + cellVal);
				jsclick(changePassSubmitBtn);
				ExpectedConditions.visibilityOf(changePassErrorContainer);
				Thread.sleep(1000);
				wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
				Thread.sleep(500);
				if(BNBasicfeature.isListElementPresent(changePassError))
				{
					String expVal = cellVal;
					log.add("The Expected value was : " + expVal);
					String actVal = changePassError.get(0).getText();
					log.add("The Actual value is  : " + actVal);
					if(expVal.contains(actVal))
					{
						Pass("The expected alert and actual alert matches .",log);
					}
					else
					{
						Fail("There is some mismatch in the expected alert and actual alert.",log);
					}
				}
				else
				{
					Fail("The Change password error is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2550 Issue ." + e.getMessage());
				Exception(" BRM - 2550 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		/* BRM - 2551 Verify that when the password field does not contain atleast one numeric and UPPER character then it should display the alert message. */
		ChildCreation(" BRM - 2551 Verify that when the password field does not contain atleast one numeric and UPPER character then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2550", sheet, 5);
				String pass = BNBasicfeature.getExcelVal("BRM2550", sheet, 7);
				changeNewPass.clear();
				changeNewPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + pass);
				log.add("The Expected alert was : " + cellVal);
				changeConfPass.clear();
				changeConfPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + cellVal);
				jsclick(changePassSubmitBtn);
				Thread.sleep(500);
				ExpectedConditions.visibilityOf(changePassErrorContainer);
				Thread.sleep(1000);
				wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
				Thread.sleep(500);
				if(BNBasicfeature.isListElementPresent(changePassError))
				{
					String expVal = cellVal;
					log.add("The Expected value was : " + expVal);
					String actVal = changePassError.get(0).getText();
					log.add("The Actual value is  : " + actVal);
					if(expVal.contains(actVal))
					{
						Pass("The expected alert and actual alert matches .",log);
					}
					else
					{
						Fail("There is some mismatch in the expected alert and actual alert.",log);
					}
				}
				else
				{
					Fail("The Change password error is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2551 Issue ." + e.getMessage());
				Exception(" BRM - 2551 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2552 Verify that when the password is less than 6 characters then it should display the alert message.*/
		ChildCreation(" BRM - 2552 Verify that when the password is less than 6 characters then it should display the alert message. ");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2552", sheet, 5);
				String[] val = cellVal.split("\n");
				String pass = BNBasicfeature.getExcelVal("BRM2552", sheet, 7);
				changeNewPass.clear();
				changeNewPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + pass);
				log.add("The Expected alert was : " + cellVal + " and its length is : "  + pass.length());
				changeConfPass.clear();
				changeConfPass.sendKeys(pass);
				log.add("The Value from the excel file was : " + cellVal + " and its length is : "  + pass.length());
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
					jsclick(changePassSubmitBtn);
					Thread.sleep(500);
					ExpectedConditions.visibilityOf(changePassErrorContainer);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
					Thread.sleep(500);
					if(BNBasicfeature.isListElementPresent(changePassError))
					{
						for(int i = 0; i<val.length;i++)
						{
							wait.until(ExpectedConditions.visibilityOf(changePassError.get(i)));
							String expVal = val[i];
							log.add("The Expected value was : " + expVal);
							String actVal = changePassError.get(i).getText();
							log.add("The Actual value is  : " + actVal);
							if(expVal.contains(actVal))
							{
								Pass("The expected alert and actual alert matches .",log);
							}
							else
							{
								Fail("There is some mismatch in the expected alert and actual alert.",log);
							}
						}
					}
					else
					{
						Fail("The Change password error is not displayed.");
					}
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2552 Issue ." + e.getMessage());
				Exception(" BRM - 2552 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2553 Verify that in "Current Password" field when the password is typed incorrectly then it should display the alert message.*/
		ChildCreation(" BRM - 2553  Verify that in Current Password field when the password is typed incorrectly then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2553", sheet, 5);
				String pass = BNBasicfeature.getExcelVal("BRM2553", sheet, 7);
				String[] val = pass.split("\n");
				changeCurrPass.clear();
				changeCurrPass.sendKeys(val[0]);
				changeNewPass.clear();
				changeNewPass.sendKeys(val[1]);
				log.add("The Value from the excel file was : " + val[1]);
				changeConfPass.clear();
				changeConfPass.sendKeys(val[1]);
				log.add("The Value from the excel file was : " + val[1]);
				log.add("The Expected alert was : " + cellVal);
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
					jsclick(changePassSubmitBtn);
					Thread.sleep(500);
					ExpectedConditions.visibilityOf(changePassErrorContainer);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
					Thread.sleep(500);
					if(BNBasicfeature.isListElementPresent(changePassError))
					{
						String expVal = cellVal;
						log.add("The Expected value was : " + expVal);
						String actVal = changePassError.get(0).getText();
						log.add("The Actual value is  : " + actVal);
						if(expVal.contains(actVal))
						{
							Pass("The expected alert and actual alert matches .",log);
						}
						else
						{
							Fail("There is some mismatch in the expected alert and actual alert.",log);
						}
					}
					else
					{
						Fail("The Change password error is not displayed.",log);
					}
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2553 Issue ." + e.getMessage());
				Exception(" BRM - 2553 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2554 Verify that when the New Password, and the Confirm Password fields does not match then it should display the alert message.*/
		ChildCreation(" BRM - 2554 Verify that when the New Password, and the Confirm Password fields does not match then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2554", sheet, 5);
				String pass = BNBasicfeature.getExcelVal("BRM2554", sheet, 7);
				String[] val = pass.split("\n");
				changeCurrPass.clear();
				changeCurrPass.sendKeys(val[0]);
				changeNewPass.clear();
				changeNewPass.sendKeys(val[1]);
				log.add("The Value from the excel file was : " + val[1]);
				changeConfPass.clear();
				changeConfPass.sendKeys(val[2]);
				log.add("The Value from the excel file was : " + val[2]);
				log.add("The Expected alert was : " + cellVal);
				if(BNBasicfeature.isElementPresent(changePassSubmitBtn))
				{
					Pass("The Save Changes button is displayed in the Change Your Password Section.");
					jsclick(changePassSubmitBtn);
					Thread.sleep(500);
					ExpectedConditions.visibilityOf(changePassErrorContainer);
					Thread.sleep(1000);
					wait.until(ExpectedConditions.attributeContains(changePassErrorContainer, "aria-atomic", "true"));
					Thread.sleep(500);
					if(BNBasicfeature.isListElementPresent(changePassError))
					{
						String expVal = cellVal;
						log.add("The Expected value was : " + expVal);
						String actVal = changePassError.get(0).getText();
						log.add("The Actual value is  : " + actVal);
						if(expVal.contains(actVal))
						{
							Pass("The expected alert and actual alert matches .",log);
						}
						else
						{
							Fail("There is some mismatch in the expected alert and actual alert.",log);
						}
					}
					else
					{
						Fail("The Change password error is not displayed.",log);
					}
				}
				else
				{
					Fail("The Save Changes button is not displayed in the Change Your Password Section.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2554 Issue ." + e.getMessage());
				Exception(" BRM - 2554 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2562 Verify that on clicking "Cancel" button in "Change your Password" option the password fields should be cleared.*/
	public void myAccountChangePassCancel()
	{
		ChildCreation("BRM - 2562 Verify that on clicking Cancel button in Change your Password option the password fields should be cleared.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changePassCancelBtn))
				{
					Pass("The Change Password Cancel Button is displayed.");
					jsclick(changePassCancelBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(myAccountPageTitle),
							ExpectedConditions.invisibilityOfAllElements(changePassError)
						)
					);
					if(BNBasicfeature.isElementPresent(changeCurrPass))
					{
						BNBasicfeature.scrolldown(changeCurrPass, driver);
					}
					String val = changeCurrPass.getAttribute("value");
					if(val.isEmpty())
					{
						Pass("The Current Password field is empty and the field is resetted.");
					}
					else
					{
						Fail("The Current Password Address field is not empty.");
					}
					
					val = changeNewPass.getAttribute("value");
					if(val.isEmpty())
					{
						Pass("The New Password field is empty and the field is resetted.");
					}
					else
					{
						Fail("The New Password field is not empty.");
					}
					
					val = changeConfPass.getAttribute("value");
					if(val.isEmpty())
					{
						Pass("The Confirm Password field is empty and the field is resetted.");
					}
					else
					{
						Fail("The Confirm Password Address field is not empty.");
					}
				}
				else
				{
					Fail("The Change Email Cancel button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2562 Issue ." + e.getMessage());
				Exception(" BRM - 2562 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2563 Verify that "Change Your Security Question" option is available in "Account Settings" page, below change your password section.*/
	public void myAccountChangeSecQuestion()
	{
		ChildCreation(" BRM - 2563 Verify that Change Your Security Question option is available in Account Settings page, below change your password section.");
		if(manageAccountLoaded==true)
		{
			try
			{
				/*String currUrll = driver.getCurrentUrl();
				if(!currUrll.equals(manageAccUrl))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}
				
				if(BNBasicfeature.isListElementPresent(accountSettingsChangeUpdateIndividualPasswordAlert))
				{
					driver.get(manageAccUrl);
					ExpectedConditions.visibilityOf(header);
					ExpectedConditions.visibilityOf(myAccountPageTitle);
				}
				*/
				String expTitle = BNBasicfeature.getExcelVal("BRM2563", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + expTitle);
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2563 Issue ." + e.getMessage());
				Exception(" BRM - 2563 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2564 Verify that "Change Your Security Question" option has Save Changes and cancel button.*/
	/* BRM - 2565 Verify that the expanded Change Your Security Question option should have the followings : Security Question Dropdown Menu & security answer fields.*/
	/* BRM - 2573 Verify that "Save Changes" button in "Change Your Security Question" option should be highlighted in Green color.*/
	/* BRM - 2574 Verify that drop-down and security answer field in "Change Your Security Question" option has default text.*/
	public void myAccountSecurityQuesDetails()
	{
		ChildCreation(" BRM - 2565 Verify that the expanded Change Your Security Question option should have the followings : Security Question Dropdown Menu & security answer fields.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeSecQuesDD))
				{
					Pass("The Security Question Drop Down is displayed.");
				}
				else
				{
					Fail("The Security Question Drop Down is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeSecQuesAns))
				{
					Pass("The Security Question Answer field is displayed.");
				}
				else
				{
					Fail("The Security Question Answer field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2565 Issue ." + e.getMessage());
				Exception(" BRM - 2565 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2564 Verify that Change Your Security Question option has Save Changes and cancel button. */
		ChildCreation("BRM - 2564 Verify that Change Your Security Question option has Save Changes and cancel button.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeSecQuesSaveBtn))
				{
					Pass("The Save Button in the Security Section is displayed.");
				}
				else
				{
					Fail("The Save Button in the Security Section is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeSecQuesCancelBtn))
				{
					Pass("The Cancel Button in the Security Section is displayed.");
				}
				else
				{
					Fail("The Cancel Button in the Security Section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2564 Issue ." + e.getMessage());
				Exception(" BRM - 2564 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2574 Verify that drop-down and security answer field in "Change Your Security Question" option has default text.*/
		ChildCreation(" BRM - 2574 Verify that drop-down and security answer field in Change Your Security Question option has default text.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeSecQuesText))
				{
					Pass("The Security Question Text is displayed.");
					String val = changeSecQuesText.getText();
					log.add("The displayed text is : " + val);
					if(val.isEmpty())
					{
						Fail("The Security Question Text is empty or not displayed.");
					}
					else
					{
						Pass("The Security Question default text is displayed.",log);
					}
				}
				else
				{
					Fail("The Securtiy Answer Default Password Text is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeSecAnsText))
				{
					Pass("The Security Answer Text is displayed.");
					String val = changeSecAnsText.getText();
					log.add("The displayed text is : " + val);
					if(val.isEmpty())
					{
						Fail("The Security Answer Text is empty or not displayed.");
					}
					else
					{
						Pass("The Security Answer default text is displayed.",log);
					}
				}
				else
				{
					Fail("The Securtiy Answer Default Text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2574 Issue ." + e.getMessage());
				Exception(" BRM - 2574 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2573 Verify that "Save Changes" button in "Change Your Security Question" option should be highlighted in Green color.*/
		ChildCreation(" BRM - 2573 Verify that Save Changes button in Change Your Security Question option should be highlighted in Green color.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2573", sheet, 6);
				if(BNBasicfeature.isElementPresent(changeSecQuesSaveBtn))
				{
					Pass("The Save Button in the Security Section is displayed.");
					String csVal = changeSecQuesSaveBtn.getCssValue("background-color");
					Color colhex = Color.fromString(csVal);
					String hexCode = colhex.asHex();
					log.add("The expected button color is : " + expColor);
					log.add("The actual button color is : " + hexCode);
					if(expColor.contains(hexCode))
					{
						Pass("The button color matches.",log);
					}
					else
					{
						Fail("The button color does not matches.",log);
					}
				}
				else
				{
					Fail("The Save Button in the Security Section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2574 Issue ." + e.getMessage());
				Exception(" BRM - 2574 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2566 Verify that, "Security Answer" field should accept maximum of 15 characters.*/
	public void myAccountSecQuesLimit()
	{
		ChildCreation("BRM - 2566 Verify that, Security Answer field should accept maximum of 15 characters.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2566", sheet, 5);
				String[] Val = cellVal.split("\n");
				
				for(int i = 0; i<Val.length;i++)
				{
					if(BNBasicfeature.isElementPresent(changeSecQuesAns))
					{
						Pass("The Security Question Answer field is displayed.");
						changeSecQuesAns.clear();
						changeSecQuesAns.sendKeys(Val[i]);
						log.add("The Value from the excel file was : " + Val[i] +" and its length is : " + Val[i].length());
						int len = changeSecQuesAns.getAttribute("value").length();
						log.add("The Current Value in the Securty Answer text field is : " + changeSecQuesAns.getAttribute("value") + " and its length is : " + len);
						if(len<=15)
						{
							Pass("The Security Answer Field accept character less than 15 only.",log);
						}
						else
						{
							Fail("The Security Answer Field accept character more than 15.",log);
						}
					}
					else
					{
						Fail("The Security Question Answer field is not displayed.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2566 Issue ." + e.getMessage());
				Exception(" BRM - 2566 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2571 Verify that when "Security Answer" field is left empty and the "Save Changes" button is clicked, then alert message should be displayed.*/
	/* BRM - 2568 Verify that when the Security Answer is less than 4 characters then it should display the alert message.*/
	public void myAccountSecQuesLimitValidation()
	{
		ChildCreation(" BRM - 2571 Verify that when Security Answer field is left empty and the Save Changes button is clicked, then alert message should be displayed.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2571", sheet, 5);
				changeSecQuesAns.clear();
				log.add("The Expected alert was : " + cellVal);
				jsclick(changeSecQuesSaveBtn);
				Thread.sleep(250);
				wait.until(ExpectedConditions.attributeContains(changeSecQuesErrorContainer, "aria-atomic", "true"));
				Thread.sleep(500);
				if(BNBasicfeature.isListElementPresent(changeSecQuesError))
				{
					String expVal = cellVal;
					log.add("The Expected value was : " + expVal);
					String actVal = changeSecQuesError.get(0).getText();
					log.add("The Actual value is  : " + actVal);
					if(expVal.contains(actVal))
					{
						Pass("The expected alert and actual alert matches .",log);
					}
					else
					{
						Fail("There is some mismatch in the expected alert and actual alert.",log);
					}
				}
				else
				{
					Fail("The Change password error is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2571 Issue ." + e.getMessage());
				Exception(" BRM - 2571 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2568 Verify that when the Security Answer is less than 4 characters then it should display the alert message.*/
		ChildCreation("BRM - 2568 Verify that when the Security Answer is less than 4 characters then it should display the alert message.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2568", sheet, 5);
				String pass = BNBasicfeature.getExcelVal("BRM2568", sheet, 7);
				changeSecQuesAns.clear();
				changeSecQuesAns.sendKeys(pass);
				log.add("The Value from the excel file was : " + pass);
				log.add("The Expected alert was : " + cellVal);
				jsclick(changeSecQuesSaveBtn);
				Thread.sleep(250);
				wait.until(ExpectedConditions.attributeContains(changeSecQuesErrorContainer, "aria-atomic", "true"));
				Thread.sleep(500);
				if(BNBasicfeature.isListElementPresent(changeSecQuesError))
				{
					String expVal = cellVal;
					log.add("The Expected value was : " + expVal);
					String actVal = changeSecQuesError.get(0).getText();
					log.add("The Actual value is  : " + actVal);
					if(expVal.contains(actVal))
					{
						Pass("The expected alert and actual alert matches .",log);
					}
					else
					{
						Fail("There is some mismatch in the expected alert and actual alert.",log);
					}
				}
				else
				{
					Fail("The Security Question error is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2568 Issue ." + e.getMessage());
				Exception(" BRM - 2568 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2578 Verify that on clicking "Cancel" button in "Change your Security Question" option the fields should be cleared and the default answer should be shown.*/
	public void myAccountSecQuesCancel()
	{
		ChildCreation(" BRM - 2578 Verify that on clicking Cancel button in Change your Security Question option the fields should be cleared and the default answer should be shown.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeSecQuesCancelBtn))
				{
					Pass("The Cancel Button in the Security Section is displayed.");
					jsclick(changeSecQuesCancelBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.visibilityOf(myAccountPageTitle),
							ExpectedConditions.invisibilityOfAllElements(changeSecQuesError)
						)
					);
					wait.until(ExpectedConditions.visibilityOf(header));
					Thread.sleep(100);
					BNBasicfeature.scrolldown(changeSecQuesDD, driver);
					String val = changeSecQuesAns.getAttribute("value");
					if(val.isEmpty())
					{
						Fail("The Security Answer field is empty.");
					}
					else
					{
						Pass("The Security Answer field is not empty and the field is resetted.");	
					}
				}
				else
				{
					Fail("The Cancel Button in the Security Section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2578 Issue ." + e.getMessage());
				Exception(" BRM - 2578 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 5440 Verify on clicking the Content Settings section is it redirecting to the Account settings page and displaying the options to edit the Content settings.*/
	public void myAccountContentPageNavigation()
	{
		ChildCreation(" BRM - 5440 Verify on clicking the Content Settings section is it redirecting to the Account settings page and displaying the options to edit the Content settings.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5440", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(contenSettingsForm)
									)
								);
								manageAccountLoaded = true;
								String exppTitle = BNBasicfeature.getExcelVal("BRM2579", sheet, 5);
								ExpectedConditions.visibilityOf(header);
								log.add("The Expected title was : " + exppTitle);
								titFound = getManageAccountPageTitles(exppTitle);
								if(titFound==true)
								{
									Pass("The Change your Passord is found on the Account Settings Page.",log);
								}
								else
								{
									Fail(" The Change your Passord is not found on the Account Settings Page.",log);
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5440 Issue ." + e.getMessage());
				Exception(" BRM - 5440 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2579 Verify that when "Content Settings" is available in "Account Settings" page*/
	public void myAccountContentSettings()
	{
		ChildCreation(" BRM - 2579 Verify that when Content Settings is available in Account Settings page.");
		if(manageAccountLoaded==true)
		{
			try
			{
				ExpectedConditions.visibilityOf(header);
				String expTitle = BNBasicfeature.getExcelVal("BRM2579", sheet, 5);
				log.add("The Expected title was : " + expTitle);
				titFound = getManageAccountPageTitles(expTitle);
				if(titFound==true)
				{
					Pass("The Change your name is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your name is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2579 Issue ." + e.getMessage());
				Exception(" BRM - 2579 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2580 Verify that Save Changes and Cancel button is available under "Content Settings" option.*/
	/* BRM - 2582 Verify that "Content Settings" option should have the default selected radio tab "Allow explicit content".*/
	/* BRM - 2584 Verify that Save changes and cancel button is available under Content Settings option.*/
	/* BRM - 2583 Verify that Save Changes button in Content Settings option should be highlighted in Green color.*/
	public void myAccountContentSettingDetails()
	{
		ChildCreation(" BRM - 2580 Verify that Save Changes and Cancel button is available under Content Settings option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeContentSettingSubmitBtn))
				{
					Pass("The Save button in the Content Setting Section is displayed.");
				}
				else
				{
					Fail("The Save Button in the Content Settings is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeContentSettingCancelBtn))
				{
					Pass("The Cancel button in the Content Setting Section is displayed.");
				}
				else
				{
					Fail("The Cancel Button in the Content Settings is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2580 Issue ." + e.getMessage());
				Exception(" BRM - 2580 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2584 Verify that Save changes and cancel button is available under Content Settings option.*/
		ChildCreation(" BRM - 2584 Verify that Save changes and cancel button is available under Content Settings option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeContentSettingSubmitBtn))
				{
					Pass("The Save button in the Content Setting Section is displayed.");
				}
				else
				{
					Fail("The Save Button in the Content Settings is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeContentSettingCancelBtn))
				{
					Pass("The Cancel button in the Content Setting Section is displayed.");
				}
				else
				{
					Fail("The Cancel Button in the Content Settings is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2584 Issue ." + e.getMessage());
				Exception(" BRM - 2584 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2583 Verify that "Save Changes" button in "Content Settings" option should be highlighted in Green color.*/
		ChildCreation(" BRM - 2583 Verify that Save Changes button in Content Settings option should be highlighted in Green color");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2583", sheet, 6);
				if(BNBasicfeature.isElementPresent(changeContentSettingSubmitBtn))
				{
					Pass("The Save button in the Content Setting Section is displayed.");
					String csVal = changeContentSettingSubmitBtn.getCssValue("background-color");
					Color colhex = Color.fromString(csVal);
					String hexCode = colhex.asHex();
					log.add("The expected button color is : " + expColor);
					log.add("The actual button color is : " + hexCode);
					if(expColor.contains(hexCode))
					{
						Pass("The button color matches.",log);
					}
					else
					{
						Fail("The button color does not matches.",log);
					}
				}
				else
				{
					Fail("The Save Button in the Content Settings is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2583 Issue ." + e.getMessage());
				Exception(" BRM - 2583 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
		
		/* BRM - 2582 Verify that "Content Settings" option should have the default selected radio tab "Allow explicit content".*/
		ChildCreation(" BRM - 2582 Verify that Content Settings option should have the default selected radio tab Allow explicit content.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2582", sheet, 5);
				log.add("The Expected Content is : " + cellVal);
				if(BNBasicfeature.isListElementPresent(changeContentSettings))
				{
					Pass("The Content Settings List is displayed.");
					boolean settfound = false;
					for(int i = 0; i<changeContentSettings.size();i++)
					{
						String val = changeContentSettings.get(i).getText();
						if(val.contains(cellVal))
						{
							log.add("The Found Section was : " + val);
							settfound = true;
							break;
						}
						else
						{
							settfound = false;
							continue;
						}
					}
					
					if(settfound==true)
					{
						Pass("The Expected Content was found in the Content Setting Section.",log);
					}
					else
					{
						Fail("The Expected Content was not found in the Content Setting Section.",log);
					}
				}
				else
				{
					Fail("The Content Settings List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2582 Issue ." + e.getMessage());
				Exception(" BRM - 2582 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2588 Verify that on clicking "Cancel" button in " Content Settings" option the selected options should be cleared and it should shown the default option.*/
	public void myAccountContentSettChangeCancel()
	{
		ChildCreation("BRM - 2588 Verify that on clicking Cancel button in Content Settings option the selected options should be cleared and it should shown the default option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				int orignialSelVal = 0;
				int newSelval = 0;
				if(BNBasicfeature.isListElementPresent(changeContentSettingsRadio))
				{
					Pass("The Content Settings Radio button is displayed.");
					String chkd = "";
					
					for(int i = 0; i<changeContentSettingsRadio.size();i++)
					{
						chkd = changeContentSettingsRadio.get(i).getAttribute("value");
						if(chkd.contains("true"))
						{
							orignialSelVal = i;
							break;
						}
						else
						{
							continue;
						}
					}
					
					
					for(int i = 0; i<changeContentSettingsRadio.size();i++)
					{
						chkd = changeContentSettingsRadio.get(i).getAttribute("value");
						if(chkd.contains("false"))
						{
							newSelval = i;
							changeContentSettingsRadio1.get(i).click();
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(orignialSelVal==newSelval)
					{
						Fail("The Radio button was not selected.");
					}
					else
					{
						Pass("The Radion button was selected.");
						if(BNBasicfeature.isElementPresent(changeContentSettingCancelBtn))
						{
							Pass("The Cancel button in the Content Setting Section is displayed.");
							jsclick(changeContentSettingCancelBtn);
							Thread.sleep(500);
							wait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
									ExpectedConditions.visibilityOf(contenSettingsForm)
								)
							);
							BNBasicfeature.scrolldown(changeContentSettingsRadio.get(0), driver);
							Thread.sleep(100);
							for(int i = 0; i<changeContentSettingsRadio.size();i++)
							{
								chkd = changeContentSettingsRadio.get(i).getAttribute("value");
								if(chkd.contains("true"))
								{
									orignialSelVal = i;
									break;
								}
								else
								{
									continue;
								}
							}
							
							if(orignialSelVal==newSelval)
							{
								Fail("The Radio button Selction was not resetted when cancel button was clicked.");
							}
							else
							{
								Pass("The Radio button Selction was resetted when cancel button was clicked.");
							}
						}
						else
						{
							Fail("The Cancel Button in the Content Settings is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Content Settings Radio button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2588 Issue ." + e.getMessage());
				Exception(" BRM - 2588 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Manage Account page.");
		}
	}
	
	/* BRM - 2585 Verify that when "Save Changes" button is clicked the "We've Updated Your Content Settings" page should be shown.*/
	public void myAccountContentSettingUpdate()
	{
		ChildCreation("BRM - 2585 Verify that when Save Changes button is clicked the We've Updated Your Content Settings page should be shown.");
		if(manageAccountLoaded==true)
		{
			try
			{
				boolean contentUpdate = false;
				String successAlert = BNBasicfeature.getExcelVal("BRM2585", sheet, 5);
				if(BNBasicfeature.isElementPresent(changeContentSettingSubmitBtn))
				{
					Pass("The Save button in the Content Setting Section is displayed.");
					Thread.sleep(250);
					jsclick(changeContentSettingSubmitBtn);
					Thread.sleep(100);
					wait.until(ExpectedConditions.and(
							//ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage.get(0)),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessCloseBtn)
							)
					);
					
					Thread.sleep(100);
					log.add("The Expected value was : " + successAlert);
					/*if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
					{
						String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();*/
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPageTitle))
					{
						String actAlert = accountSettingsChangeNameSuccessPageTitle.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							contentUpdate = true;
						}
					}
					else if(BNBasicfeature.isElementPresent(pgetitle))
					{
						//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
						String actAlert = accountSettingsChangeNameSuccessPage.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							contentUpdate = true;
						}
					}
					else
					{
						Fail( "The Success Alert Title is not displayed.");
					}
					
					if(contentUpdate==true)
					{
						Pass("The expected and actual alert matches.",log);
						contentUpdated = true;
					}
					else
					{
						 if(BNBasicfeature.isElementPresent(pgetitle))
						 {
							//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
							 String actAlert = accountSettingsChangeNameSuccessPage.getText();
							log.add("The Actual value is : " + actAlert);
							if(successAlert.contains(actAlert))
							{
								Pass("The expected and actual alert matches.",log);
								contentUpdated = true;
							}
							else
							{
								Fail("There is some mismatch in the expected and actual alert.",log);
							}
						 }
					}
					
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						contentUpdated = true;
					}
				}
				else
				{
					Fail("The Save Button in the Content Settings is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2585 Issue ." + e.getMessage());
				Exception(" BRM - 2585 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the Content in the Manage Account page.");
		}
	}
	
	/* BRM - 2586 Verify that "We've Updated Your Content Settings" overlay should have "Close" button.*/
	public void myAccountContentSettingSuccessCloseButton()
	{
		ChildCreation(" BRM - 2586 Verify that We've Updated Your Content Settings overlay should have Close button.");
		if(contentUpdated==true)
		{
			try
			{
				//if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else
				{
					Fail("The User is not in the Change Name Success Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2586 Issue ." + e.getMessage());
				Exception(" BRM - 2586 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User Failed to update the Content in the Manage Account page.");
		}
	}
	
	/* BRM - 2587 Verify that, when the "Close" button in "We've Updated Your Content Settings" page is clicked then it should be closed displaying previous page.*/
	public void myAccountContentSettSuccessOverlayClose()
	{
		ChildCreation(" BRM - 2587 Verify that, when the Close button in We've Updated Your Content Settings page is clicked then it should be closed displaying previous page.");
		if(contentUpdated==true)
		{
			String title = "";
			boolean navigatedbck = false;
			int i = 1;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2516", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
				{
					Pass("The Close button in the Success page is displayed.");
					//accountSettingsChangeNameSuccessCloseBtn.click();
					ExpectedConditions.elementToBeClickable(accountSettingsChangeNameSuccessCloseBtn);
					Thread.sleep(500);
					boolean pgLoad = false;
					WebDriverWait w1 = new WebDriverWait(driver, 3);
					do
					{
						try
						{
							jsclick(accountSettingsChangeNameSuccessCloseBtn);
							w1.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(header),
									//ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeNameSuccessPage),
									ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@data-modal-name,'success')]")),
									ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle)
									)
							);
							pgLoad = true;
						}
						catch(Exception e1)
						{
							pgLoad = false;
						}
					}while(pgLoad==false);
					
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							//ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@data-modal-name,'success')]")),
							ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle)
							)
					);
					
					ExpectedConditions.visibilityOf(contenSettingsForm);
					
					if(BNBasicfeature.isElementPresent(myAccountAccountSettingsPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
						
					}
					else if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
					}
					else
					{
						Fail("The Page Title is not displayed.");
					}
					
					if(navigatedbck==true)
					{
						Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
						manageAccNameUpdt = true;
					}
					else
					{
						if(BNBasicfeature.isElementPresent(myAccountPageTitle))
						{
							Pass("The user is redirected to the Add New Payments Page.");
							log.add("The Expected Title was : " + Val[i-1]);
							title = myAccountAccountSettingsPageTitle.getText();
							log.add("The Actual Title is : " + title);
							if(Val[0].contains(title))
							{
								Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
								manageAccNameUpdt = true;
							}
							else
							{
								Fail("The Page title does not match the expected content.",log);
							}
						}
					}
				}
				else
				{
					Fail("The Close button in the Success page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2587 Issue ." + e.getMessage());
				Exception(" BRM - 2587 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the Content in the Manage Account page.");
		}
	}
	
	/* BRM - 5441 Verify on clicking the Instant purchase Settings section is it redirecting to the Account settings page page and displaying the options to edit the Instant purchase settings.*/
	public void myAccountInstantPurchasePageNavigation()
	{
		ChildCreation(" BRM - 5441 Verify on clicking the Instant purchase Settings section is it redirecting to the Account settings page page and displaying the options to edit the Instant purchase settings.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5441", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(instantPurchaseSettingsForm)
									)
								);
								
								manageAccountLoaded = true;
								String exppTitle = BNBasicfeature.getExcelVal("BRM2589", sheet, 5);
								ExpectedConditions.visibilityOf(header);
								log.add("The Expected title was : " + exppTitle);
								titFound = getManageAccountPageTitles(exppTitle);
								if(titFound==true)
								{
									Pass("The Change your Passord is found on the Account Settings Page.",log);
								}
								else
								{
									Fail(" The Change your Passord is not found on the Account Settings Page.",log);
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5441 Issue ." + e.getMessage());
				Exception(" BRM - 5441 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2589 Verify that when "Instant Purchase Settings" is available in "Account Settings" page.*/
	public void myAccountInstantPurchaseSettings()
	{
		ChildCreation(" BRM - 2589 Verify that when Instant Purchase Settings is available in Account Settings page.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String exppTitle = BNBasicfeature.getExcelVal("BRM2589", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + exppTitle);
				titFound = getManageAccountPageTitles(exppTitle);
				if(titFound==true)
				{
					Pass("The Change your Passord is found on the Account Settings Page.",log);
				}
				else
				{
					Fail(" The Change your Passord is not found on the Account Settings Page.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2589 Issue ." + e.getMessage());
				Exception(" BRM - 2589 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2590 Verify that radio buttons are available for the options under "Instant Purchase Settings".*/
	public void myAccountInstantPurchaseRadio()
	{
		ChildCreation("BRM - 2590 Verify that radio buttons are available for the options under Instant Purchase Settings.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(changeInstantPurchaseRadio))
				{
					Pass("The Instant purchase Radion button is displayed.");
				}
				else
				{
					Fail("The Instant purchase Radion button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2590 Issue ." + e.getMessage());
				Exception(" BRM - 2590 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2591 Verify that the "Instant Purchase Settings" option should have the followings : "Allow instant purchase" & "Do not allow radio button" options.*/
	public void myAccountInstantPurchaselabel()
	{
		ChildCreation("BRM - 2591 Verify that the Instant Purchase Settings option should have the followings : Allow instant purchase & Do not allow radio button options.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2591", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isListElementPresent(changeInstantPurchaseRadioText))
				{
					Pass("The Instant purchase Radion button is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						String expVal = Val[i];
						log.add("The Expected Value was : " + expVal);
						String actVal = changeInstantPurchaseRadioText.get(i).getText();
						log.add("The Actual Value is : " + actVal);
						if(expVal.contains(actVal))
						{
							Pass("The Value matches.",log);
						}
						else
						{
							Fail("The Value does not matches.",log);
						}
					}
				}
				else
				{
					Fail("The Instant purchase Radion button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2591 Issue ." + e.getMessage());
				Exception(" BRM - 2591 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2592 Verify that "Instant Purchase Settings" option should have the default selected radio tab "allow instant purchase".*/
	public void myAccountInstantPurchaseDefSelect()
	{
		ChildCreation(" BRM - 2592 Verify that Instant Purchase Settings option should have the default selected radio tab Allow instant purchase.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expVal = BNBasicfeature.getExcelVal("BRM2592", sheet, 5);
				log.add("The Expected value to be selected by default is : " + expVal);
				
				String actVal = "";
				if(BNBasicfeature.isListElementPresent(changeInstantPurchaseRadio))
				{
					Pass("The Content Settings Radio button is displayed.");
					String chkd = "";
					
					for(int i = 0; i<changeInstantPurchaseRadio.size();i++)
					{
						chkd = changeInstantPurchaseRadio.get(i).getAttribute("value");
						if(chkd.contains("true"))
						{
							actVal = changeInstantPurchaseRadioText.get(i).getText();
							log.add("The Actual value selected by default is : " + expVal);
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(expVal.contains(actVal))
					{
						Pass("The Expected Option is selected by default.",log);
					}
					else
					{
						Fail("The Expected option is not selected by default.",log);
					}
				}
				else
				{
					Fail("The Content Settings Radio button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2592 Issue ." + e.getMessage());
				Exception(" BRM - 2592 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2594 Verify that Save Changes and Cancel button is available ubder"Instant Purchase Settings" option.*/
	/* BRM - 2593 Verify that "Save Changes" button in "Instant Purchase Settings" option should be highlighted in Green color.*/
	public void myAccountInstantPurchaseDetails()
	{
		ChildCreation("BRM - 2594 Verify that Save Changes and Cancel button is available under Instant Purchase Settings option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeInstantPurchaseSubmitBtn))
				{
					Pass("The Save button in the Instant Purchase Section is displayed.");
				}
				else
				{
					Fail("The Save Button in the Instant Purchase is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(changeInstantPurchaseCancelBtn))
				{
					Pass("The Cancel button in the Instant Purchase Section is displayed.");
				}
				else
				{
					Fail("The Cancel Button in the Instant Purchase is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2594 Issue ." + e.getMessage());
				Exception(" BRM - 2594 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
		
		/* BRM - 2593 Verify that "Save Changes" button in "Instant Purchase Settings" option should be highlighted in Green color.*/
		ChildCreation("BRM - 2593 Verify that Save Changes button in Instant Purchase Settings option should be highlighted in Green color.");
		if(manageAccountLoaded==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2593", sheet, 6);
				if(BNBasicfeature.isElementPresent(changeInstantPurchaseSubmitBtn))
				{
					Pass("The Save button in the Instant Purchase Setting Section is displayed.");
					String csVal = changeInstantPurchaseSubmitBtn.getCssValue("background-color");
					Color colhex = Color.fromString(csVal);
					String hexCode = colhex.asHex();
					log.add("The expected button color is : " + expColor);
					log.add("The actual button color is : " + hexCode);
					if(expColor.contains(hexCode))
					{
						Pass("The button color matches.",log);
					}
					else
					{
						Fail("The button color does not matches.",log);
					}
				}
				else
				{
					Fail("The Save Button in the Instant Purchase is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2593 Issue ." + e.getMessage());
				Exception(" BRM - 2593 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2598 Verify that on clicking "Cancel" button in " Instant Purchase" option the selected options should be cleared and it should shown the default option.*/
	public void myAccountInstantPurchaseChangeCancel()
	{
		ChildCreation("BRM - 2598 Verify that on clicking Cancel button in Instant Purchase option the selected options should be cleared and it should shown the default option.");
		if(manageAccountLoaded==true)
		{
			try
			{
				int orignialSelVal = 0;
				int newSelval = 0;
				if(BNBasicfeature.isListElementPresent(changeInstantPurchaseRadio))
				{
					Pass("The Instant Purchase Radio button is displayed.");
					String chkd = "";
					
					for(int i = 0; i<changeInstantPurchaseRadio.size();i++)
					{
						chkd = changeInstantPurchaseRadio.get(i).getAttribute("value");
						if(chkd.contains("true"))
						{
							orignialSelVal = i;
							break;
						}
						else
						{
							continue;
						}
					}
					
					
					for(int i = 0; i<changeInstantPurchaseRadio.size();i++)
					{
						chkd = changeInstantPurchaseRadio.get(i).getAttribute("value");
						if(chkd.contains("false"))
						{
							newSelval = i;
							changeInstantPurchaseRadio1.get(i).click();
							break;
						}
						else
						{
							continue;
						}
					}
					
					if(orignialSelVal==newSelval)
					{
						Fail("The Radio button was not selected.");
					}
					else
					{
						Pass("The Radio button was selected.");
						if(BNBasicfeature.isElementPresent(changeInstantPurchaseCancelBtn))
						{
							Pass("The Cancel button in the Instant Purchase Section is displayed.");
							BNBasicfeature.scrolldown(changeInstantPurchaseCancelBtn, driver);
							jsclick(changeInstantPurchaseCancelBtn);
							Thread.sleep(500);
							wait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
									ExpectedConditions.visibilityOf(instantPurchaseSettingsForm)
								)
							);
							wait.until(ExpectedConditions.visibilityOf(header));
							BNBasicfeature.scrolldown(changeInstantPurchaseRadio.get(0), driver);
							Thread.sleep(500);
							for(int i = 0; i<changeInstantPurchaseRadio.size();i++)
							{
								chkd = changeInstantPurchaseRadio.get(i).getAttribute("value");
								if(chkd.contains("true"))
								{
									orignialSelVal = i;
									break;
								}
								else
								{
									continue;
								}
							}
							
							if(orignialSelVal==newSelval)
							{
								Fail("The Radio button Selction was not resetted when cancel button was clicked.");
							}
							else
							{
								Pass("The Radio button Selction was resetted when cancel button was clicked.");
							}
						}
						else
						{
							Fail("The Cancel Button in the Content Settings is not displayed.");
						}
					}
				}
				else
				{
					Fail("The Content Settings Radio button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2598 Issue ." + e.getMessage());
				Exception(" BRM - 2598 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2595 Verify that when "Save Changes" button is clicked the "We've Updated Your Account Settings" page should be shown.*/
	public void myAccountInstantPurchaseUpdate()
	{
		ChildCreation("BRM - 2595 Verify that when Save Changes button is clicked the We've Updated Your Account Settings page should be shown.");
		if(manageAccountLoaded==true)
		{
			try
			{
				boolean contentUpdate = false;
				contentUpdated = false;
				String successAlert = BNBasicfeature.getExcelVal("BRM2595", sheet, 5);
				if(BNBasicfeature.isElementPresent(changeInstantPurchaseSubmitBtn))
				{
					Pass("The Save button in the Insant Purchase Section is displayed.");
					BNBasicfeature.scrolldown(changeInstantPurchaseSubmitBtn, driver);
					jsclick(changeInstantPurchaseSubmitBtn);
					
					Thread.sleep(100);
					wait.until(ExpectedConditions.or(
							//ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage.get(0)),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.visibilityOf(accountSettingsChangeNameSuccessCloseBtn)
							)
					);
					
					Thread.sleep(100);
					log.add("The Expected value was : " + successAlert);
					/*if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
					{
						String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();*/
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPageTitle))
					{
						String actAlert = accountSettingsChangeNameSuccessPageTitle.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							contentUpdate = true;
						}
					}
					else if(BNBasicfeature.isElementPresent(pgetitle))
					{
						//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
						String actAlert = accountSettingsChangeNameSuccessPage.getText();
						log.add("The Actual value is : " + actAlert);
						if(successAlert.contains(actAlert))
						{
							contentUpdate = true;
						}
					}
					else
					{
						Fail( "The Success Alert Title is not displayed.");
					}
					
					if(contentUpdate==true)
					{
						Pass("The expected and actual alert matches.",log);
						contentUpdated = true;
					}
					else
					{
						 if(BNBasicfeature.isElementPresent(pgetitle))
						 {
							//String actAlert = accountSettingsChangeNameSuccessPage.get(0).getText();
							String actAlert = accountSettingsChangeNameSuccessPage.getText();
							log.add("The Actual value is : " + actAlert);
							if(successAlert.contains(actAlert))
							{
								Pass("The expected and actual alert matches.",log);
								contentUpdated = true;
							}
							else
							{
								Fail("There is some mismatch in the expected and actual alert.",log);
							}
						 }
					}
					
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						contentUpdated = true;
					}
				}
				else
				{
					Fail("The Save Button in the Content Settings is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2595 Issue ." + e.getMessage());
				Exception(" BRM - 2595 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to load the Sign In to the Account page.");
		}
	}
	
	/* BRM - 2596 Verify that "We've Updated Your Account Settings " overlay should have "Close" button.*/
	public void myAccountInstantPurchaseSuccessCloseButton()
	{
		ChildCreation(" BRM - 2596 Verify that We've Updated Your Account Settings overlay should have Close button.");
		if(contentUpdated==true)
		{
		
			try
			{
				//if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage.get(0)))
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessPage))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else if(BNBasicfeature.isElementPresent(pgetitle))
				{
					Pass("The User is in the Change Name Success Page.");
					if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
					{
						Pass("The Close button in the Success page is displayed.");
					}
					else
					{
						Fail("The Close button in the Success page is not displayed.");
					}
				}
				else
				{
					Fail("The User is not in the Change Name Success Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2596 Issue ." + e.getMessage());
				Exception(" BRM - 2596 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User Failed to update the Instant Purchase in the Manage Account page.");
		}
	}
	
	/* BRM - 2597 Verify that, when the "Close" button in "We've Updated Your Account Settings  " page is clicked then it should be closed displaying previous page.*/
	public void myAccountInsantPurchaseSuccessOverlayClose()
	{
		ChildCreation(" BRM - 2597 Verify that, when the Close button in We've Updated Your Account Settings page is clicked then it should be closed displaying previous page.");
		if(contentUpdated==true)
		{
			String title = "";
			boolean navigatedbck = false;
			int i = 1;
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRM2516", sheet, 5);
				String[] Val = cellVal.split("\n");
				
				if(BNBasicfeature.isElementPresent(accountSettingsChangeNameSuccessCloseBtn))
				{
					Pass("The Close button in the Success page is displayed.");
					jsclick(accountSettingsChangeNameSuccessCloseBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							//ExpectedConditions.invisibilityOfAllElements(accountSettingsChangeNameSuccessPage),
							ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[contains(@data-modal-name,'success')]")),
							ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle)
							)
					);
					
					ExpectedConditions.visibilityOf(instantPurchaseSettingsForm);
					
					if(BNBasicfeature.isElementPresent(myAccountAccountSettingsPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
						
					}
					else if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("The user is redirected to the Add New Payments Page.");
						log.add("The Expected Title was : " + Val[i-1]);
						title = myAccountAccountSettingsPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(Val[0].contains(title))
						{
							navigatedbck = true;
						}
					}
					else
					{
						Fail("The Page Title is not displayed.");
					}
					
					if(navigatedbck==true)
					{
						Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
						manageAccNameUpdt = true;
					}
					else
					{
						if(BNBasicfeature.isElementPresent(myAccountPageTitle))
						{
							Pass("The user is redirected to the Add New Payments Page.");
							log.add("The Expected Title was : " + Val[i-1]);
							title = myAccountAccountSettingsPageTitle.getText();
							log.add("The Actual Title is : " + title);
							if(Val[0].contains(title))
							{
								Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
								manageAccNameUpdt = true;
							}
							else
							{
								Fail("The Page title does not match the expected content.",log);
							}
						}
					}
				}
				else
				{
					Fail("The Close button in the Success page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2597 Issue ." + e.getMessage());
				Exception(" BRM - 2597 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User Failed to update the Content in the Manage Account page.");
		}
	}
	
	/* BRM - 5442 Verify on clicking the Payment method section is it redirecting to the Account settings page and displaying the options to edit the Payment method settings.*/
	/* BRM - 2704 Verify that when "Manage Default Payment" link is clicked, then it should navigate to "Manage Default Payment" page.*/
	public void myAccountPaymentsPageNavigation()
	{
		ChildCreation(" BRM - 5442 Verify on clicking the Payment method section is it redirecting to the Account settings page and displaying the options to edit the Payment method settings.");
		boolean BRM2704 = false, BRM2705 = false;
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5442", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountAccountSettingsPageTitle),
										ExpectedConditions.visibilityOf(defaultPaymentForm)
									)
								);
								
								manageAccountLoaded = true;
								String exppTitle = BNBasicfeature.getExcelVal("BRM5442", sheet, 3);
								ExpectedConditions.visibilityOf(header);
								log.add("The Expected title was : " + exppTitle);
								titFound = getManageAccountPageTitles(exppTitle);
								if(titFound==true)
								{
									Pass("The Default payment is found on the Account Settings Page.",log);
									paymetsPageLoaded = true;
									BRM2704 = true;
									BRM2705 = true;
								}
								else
								{
									if(BNBasicfeature.isElementPresent(myAccountPageTitle))
									{
										String pgeTitle = BNBasicfeature.getExcelVal("BRMPayNavigation", sheet, 5);
										if(pgeTitle.contains(myAccountPageTitle.getText()))
										{
											paymetsPageLoaded = true;
											BRM2704 = true;
											BRM2705 = true;
										}
										else
										{
											Fail(" The Default payment is not found on the Account Settings Page.",log);
										}
									}
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5442 Issue ." + e.getMessage());
				Exception(" BRM - 5442 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the payments page.");
		}
		
		/* BRM - 2704 Verify that when "Manage Default Payment" link is clicked, then it should navigate to "Manage Default Payment" page.*/
		ChildCreation("BRM - 2704 Verify that when Manage Default Payment link is clicked, then it should navigate to Manage Default Payment page.");
		if(paymetsPageLoaded==true)
		{
			if(BRM2704==true)
			{
				Pass("The Default payment is found on the Account Settings Page.",log);
			}
			else
			{
				Fail(" The Default payment is not found on the Account Settings Page.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the payments page.");
		}
		
		/* BRM - 2705 Verify that when "Manage Payment options" link is clicked, then it should navigate to "Manage Payment options" page.*/
		ChildCreation("BRM - 2705 Verify that when Manage Payment options link is clicked, then it should navigate to Manage Payment options page.");
		if(paymetsPageLoaded==true)
		{
			if(BRM2705==true)
			{
				Pass("The Default payment is found on the Account Settings Page.",log);
			}
			else
			{
				Fail(" The Default payment is not found on the Account Settings Page.",log);
			}
		}
		else
		{
			Skip( "The User failed to load the payments page.");
		}
		
	}
	
	/* BRM - 2674 Verify that "Default Delivery Speed" text should be shown with three Radio tabs.*/
	/* BRM - 2676 Verify that selected radio button should be highlighted in bold in Default Delivery Speed radio text. */
	public void myAccountDeliverySpeed()
	{
		ChildCreation(" BRM - 2674 Verify that Default Delivery Speed text should be shown with three Radio tabs.");
		boolean lnkFound = false;
		if(manageAccountLoaded==true)
		{
			try
			{
				String speedOption = BNBasicfeature.getExcelVal("BRM2674", sheet, 6);
				String[] Val = speedOption.split("\n");
				
				int i = 0;
				String exppTitle = BNBasicfeature.getExcelVal("BRM2674", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + exppTitle);
				titFound = getManageAccountPageTitles(exppTitle);
				if(titFound==true)
				{
					Pass("The Default Delivery Speed title is found on the Account Settings Page.",log);
					lnkFound = true;
				}
				else
				{
					Fail(" The Default Delivery Speed title is not found on the Account Settings Page.",log);
				}
				
				if(lnkFound==true)
				{
					if(BNBasicfeature.isListElementPresent(changeDefaulSpeedOptions))
					{
						Pass("The Change Speed Option is displayed.");
						for(i = 0; i<Val.length;i++)
						{
							log.add("The Expected value was : " + Val[i]);
							String actVal = changeDefaulSpeedOptions.get(i).getText();
							log.add("The Actual Speed Delivery Value is : " +actVal);
							if(actVal.contains(Val[i]))
							{
								Pass("The Value Matches the expected content.",log);
							}
							else
							{
								Fail("The Value does not Matches the expected content.",log);
							}
						}
					}
					else
					{
						Fail("The Change Speed Option is not displayed.");
					}
				}
				else
				{
					Fail("The Default Delivery Speed is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2674 Issue ." + e.getMessage());
				Exception(" BRM - 2674 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the manage account settings page.");
		}
		
		/* BRM - 2676 Verify that selected radio button should be highlighted in bold in Default Delivery Speed radio text. */
		ChildCreation(" BRM - 2676 Verify that selected radio button should be highlighted in bold in Default Delivery Speed radio text.");
		if(manageAccountLoaded==true)
		{
			try
			{
				int i = 0;
				String expColor = BNBasicfeature.getExcelVal("BRM2676", sheet, 6);
				boolean chkd = false;
				if(lnkFound==true)
				{
					Pass("The Change Speed Option is  displayed.");
					if(BNBasicfeature.isListElementPresent(changeInstantSpeedRadioBtn))
					{
						Pass("The Speed option radio button is displayed.");
						for(i = 0; i<changeInstantSpeedRadioBtn.size();i++)
						{
							chkd = changeInstantSpeedRadioBtn.get(i).isSelected();
							if(chkd==true)
							{
								chkd=true;
								break;
							}
							else
							{
								continue;
							}
						}
						
						if(chkd==true)
						{
							Pass("The Speed Option is selected.");
							String speedCol = changeDefaulSpeedOptions.get(i).getCssValue("color");
							Color colHx = Color.fromString(speedCol);
							String actColor = colHx.asHex();
							log.add("The Expected Color was : " + expColor);
							log.add("The Actual Color is : " + actColor);
							if(expColor.contains(actColor))
							{
								Pass("The Speed Option is selected and they are highlighted.",log);
							}
							else
							{
								Fail("The Speed Option is not selected and they are not highlighted.",log);
							}
						}
						else
						{
							Skip("The Speed Option is not selected.");
						}
					}
					else
					{
						Fail("The Speed option radio button is not displayed.");
					}
					
				}
				else
				{
					Fail("The Default Delivery Speed is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2676 Issue ." + e.getMessage());
				Exception(" BRM - 2676 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the manage account settings page.");
		}
	}
	
	/* BRM - 2599 Verify that when "Manage Default Payment" link is selected it should navigate to "Manage Payment Methods" page.*/
	public void myAccountManageDefPayNavigation()
	{
		ChildCreation("BRM - 2599 Verify that when Manage Default Payment link is selected it should navigate to Manage Payment Methods page.");
		if(manageAccountLoaded==true)
		{
			try
			{
				//String cellVal = BNBasicfeature.getExcelVal("BRM2599", sheet, 5);
				String pgeTitle = BNBasicfeature.getExcelVal("BRMPayNavigation", sheet, 5);
				boolean lnkFound = false;
				//int i = 0;
				
				String exppTitle = BNBasicfeature.getExcelVal("BRM2674", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + exppTitle);
				titFound = getManageAccountPageTitles(exppTitle);
				if(titFound==true)
				{
					Pass("The Default Delivery Speed title is found on the Account Settings Page.",log);
					lnkFound = true;
				}
				else
				{
					Fail(" The Default Delivery Speed title is not found on the Account Settings Page.",log);
				}
				
				if(lnkFound==true)
				{
					if(BNBasicfeature.isElementPresent(editPayButtons))
					{
						jsclick(editPayButtons);
						Thread.sleep(100);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(header),
								ExpectedConditions.visibilityOf(myAccountPageTitle),
								ExpectedConditions.visibilityOf(defaultPaymentForm)
							)
						);
						
						if(BNBasicfeature.isElementPresent(myAccountPageTitle))
						{
							Pass("The user is redirected to the Payments Page.");
							log.add("The Expected Title was : " + pgeTitle);
							String title = myAccountPageTitle.getText();
							log.add("The Actual Title is : " + title);
							if(title.contains(pgeTitle))
							{
								Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
							}
							else
							{
								Fail("The Payment Page title does not match the expected content.",log);
							}
						}
						else
						{
							Fail("The user is not redirected to the Payments Page.");
						}
					}
					else
					{
						Fail( "The Payment page is not displayed.");
					}
				}
				else
				{
					Fail("The Manage Default Payment Link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2599 Issue ." + e.getMessage());
				Exception(" BRM - 2599 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the payments page.");
		}
	}
	
	/* BRM - 2600  Verify that "Manage Payment Methods" page should display Default Payment text with "Edit" and "Remove" link.*/
	public void myAccountManagePayPageDet()
	{
		ChildCreation(" BRM - 2600  Verify that Manage Payment Methods page should display Default Payment text with Edit and Remove link. ");
		if(paymetsPageLoaded==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMPayPageDefDet", sheet, 5);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(defaultPaymentForm))
				{
					Pass("The Payment Method is displayed.");
					if(BNBasicfeature.isElementPresent(paymentsDefaultPayment))
					{
						Pass("The Default Payment is set for the logged in user.");
						String exppTitle = Val[0];
						ExpectedConditions.visibilityOf(header);
						log.add("The Expected title was : " + exppTitle);
						Pass("The Default Payment title is displayed.");
						titFound = getManageAccountPageTitles(exppTitle);
						if(titFound==true)
						{
							Pass("The Default payment is found on the Account Settings Page.",log);
						}
						else
						{
							Fail("The Title does not matches the expected content.",log);
						}
						
						if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentAddressEdit))
						{
							Pass("The Default Payment Edit button is displayed.");
							String title = paymentsDefaultPaymentAddressEdit.getText();
							log.add("The Expected title was : " + Val[1]);
							log.add("The Actual title is : " + title);
							if(title.contains(Val[1]))
							{
								Pass("The Title matches the expected content.",log);
							}
							else
							{
								Fail("The Title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Default Payment Edit button is not dispalyed.");
						}
						
						if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentAddressRemove))
						{
							Pass("The Default Payment Edit button is displayed.");
							String title = paymentsDefaultPaymentAddressRemove.getText();
							log.add("The Expected title was : " + Val[2]);
							log.add("The Actual title is : " + title);
							if(Val[2].contains(title))
							{
								Pass("The Title matches the expected content.",log);
							}
							else
							{
								Fail("The Title does not matches the expected content.",log);
							}
						}
						else
						{
							Fail("The Default Payment Remove button is not dispalyed.");
						}
					}
					else
					{
						Fail("The Default Payment is not set for the logged in user.");
					}
				}
				else
				{
					Fail("The Payment Method is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2600 Issue ." + e.getMessage());
				Exception(" BRM - 2600 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Manage Payment Page.");
		}
	}
	
	/* BRM - 2602 Verify that when "Remove" link is clicked then "Remove this Payment Method" overlay should be shown.*/
	/* BRM - 2603 Verify that Remove this Payment Method overlay should display an alert message.*/
	/* BRM - 2605 Verify that when Cancel button is clicked then it should navigate back to the previous page.*/
	public void myAccountRemovePayOverlay()
	{
		ChildCreation("BRM - 2602 Verify that when Remove link is clicked then Remove this Payment Method overlay should be shown.");
		if(paymetsPageLoaded==true)
		{
			try
			{
				String successAlert = BNBasicfeature.getExcelVal("BRM2602", sheet, 5);
				if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentAddressRemove))
				{
					Pass("The Default Payment Edit button is displayed.");
					jsclick(paymentsDefaultPaymentAddressRemove);
					
					Thread.sleep(100);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(paymentsDefaultAddressDelete),
							ExpectedConditions.visibilityOf(pgetitle)
							)
					);
					
					Thread.sleep(100);
					
					log.add("The Expected value was : " + successAlert);
					String actAlert = pgetitle.getText();
					log.add("The Actual value is : " + actAlert);
					if(successAlert.contains(actAlert))
					{
						Pass("The expected and actual alert matches.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and actual alert.",log);
					}
				}
				else
				{
					Fail("The Default Payment Remove button is not dispalyed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2602 Issue ." + e.getMessage());
				Exception(" BRM - 2602 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Manage Payment Page.");
		}
		
		/* BRM - 2603 Verify that Remove this Payment Method overlay should display an alert message.*/
		ChildCreation(" BRM - 2603 Verify that Remove this Payment Method overlay should display an alert message.");
		if(paymetsPageLoaded==true)
		{
			try
			{
				String successAlert = BNBasicfeature.getExcelVal("BRM2603", sheet, 5);
				if(BNBasicfeature.isElementPresent(changeDefaultPayRemoveConfirmText))
				{
					Pass("The Payment Remove Confirm text is displayed.");
					log.add("The Expected value was : " + successAlert);
					String actAlert = changeDefaultPayRemoveConfirmText.getText();
					log.add("The Actual value is : " + actAlert);
					if(successAlert.contains(actAlert))
					{
						Pass("The expected and actual alert matches.",log);
					}
					else
					{
						Fail("There is some mismatch in the expected and actual alert.",log);
					}
				}
				else
				{
					Fail("The Payment Remove Confirm text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2603 Issue ." + e.getMessage());
				Exception(" BRM - 2603 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Manage Payment Page.");
		}
		
		/* BRM - 2605 Verify that when Cancel button is clicked then it should navigate back to the previous page.*/
		ChildCreation(" BRM - 2605 Verify that when Cancel button is clicked then it should navigate back to the previous page.");
		if(paymetsPageLoaded==true)
		{
			try
			{
				String pggeTitle = BNBasicfeature.getExcelVal("BRMPayNavigation", sheet, 5);
				if(BNBasicfeature.isElementPresent(changeDefaultPayRemoveCancelBtn))
				{
					Pass("The Cancel button in the Remove Payments Page is displayed.");
					jsclick(changeDefaultPayRemoveCancelBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.invisibilityOfAllElements(changeDefaultPayRemoveCancelBtn1),
							ExpectedConditions.visibilityOf(myAccountPageTitle)
							)
					);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("The user is redirected to the Payments Page.");
						log.add("The Expected Title was : " + pggeTitle);
						String title = myAccountPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(title.contains(pggeTitle))
						{
							Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
						}
						else
						{
							Fail("The Payment Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The user is not redirected to the Payments Page.");
					}
				}
				else
				{
					Fail("The Cancel button in the Remove Payments Page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2605 Issue ." + e.getMessage());
				Exception(" BRM - 2605 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Manage Payment Page.");
		}
	}
	
	/* BRM - 2607 Verify that "Add New Payment Method" page should have the following : "Card Number", "Name on the Card" & so on...*/
	/* BRM - 2609 Verify that, below Add New payment Method text, the message should be displayed.*/
	public void myAccountNewPayDet()
	{
		ChildCreation("BRM - 2607 Verify that Add New Payment Method page should have the following : Card Number, Name on the Card & so on...");
		if(paymetsPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(paymentsMethodAddNewPayment))
				{
					Pass("The Add New Payments button is displayed.");
					jsclick(paymentsMethodAddNewPayment);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(paymentsMethodAddNewPaymentPage));
					addNewPay = true;
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(newccNumber))
					{
						Pass("The Credit Card field is present and visible.");
					}
					else
					{
						Fail("The Credit Card text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccNumberDefText))
					{
						if(newccNumberDefText.getText().isEmpty())
						{
							Fail("The Credit Card Number field Default text is not present and visible.");
						}
						else
						{
							Pass("The Credit Card Number field Default text is present and visible.");
						}
					}
					else
					{
						Fail("The Credit Card Number field Default text is not present and visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccName))
					{
						Pass("The Credit Card Name field is present and visible.");
					}
					else
					{
						Fail("The Credit Card Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccNameDefText))
					{
						if(newccNameDefText.getText().isEmpty())
						{
							Fail("The Credit Card Name field Default text is not present and visible.");
						}
						else
						{
							Pass("The Credit Card Name field Default text is present and visible.");
						}
					}
					else
					{
						Fail("The Credit Card Name field Default text is not present and visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccMonth))
					{
						Pass("The Credit Card Month Expiration field is present and visible.");
					}
					else
					{
						Fail("The Credit Card Month Expiration field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccMonthDefText))
					{
						if(newccMonthDefText.getText().isEmpty())
						{
							Fail("The Credit Card Expiration Month field Default text is not present and visible.");
						}
						else
						{
							Pass("The Credit Card Expiration Month field Default text is present and visible.");
						}
					}
					else
					{
						Fail("The Credit Card Expiration Month field Default text is not present and visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccYear))
					{
						Pass("The Credit Card Year Expiration field is present and visible.");
					}
					else
					{
						Fail("The Credit Card Year Expiration field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(newccYearDefText))
					{
						if(newccYearDefText.getText().isEmpty())
						{
							Fail("The Credit Card Expiration Year field Default text is not present and visible.");
						}
						else
						{
							Pass("The Credit Card Expiration Year field Default text is present and visible.");
						}
					}
					else
					{
						Fail("The Credit Card Expiration Year field Default text is not present and visible.");
					}
					
					if(BNBasicfeature.isElementPresent(fName))
					{

						BNBasicfeature.scrolldown(fName, driver);
						Pass("The First Name text field is present and visible.");
					}
					else
					{
						Fail("The First Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(lName))
					{
						Pass("The Last Name text field is present and visible.");
					}
					else
					{
						Fail("The Last Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(stAddress))
					{
						Pass("The Stree Address text field is present and visible.");
					}
					else
					{
						Fail("The Stree Address text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(aptSuiteAddress))
					{
						BNBasicfeature.scrolldown(aptSuiteAddress, driver);
						Pass("The Apt/Suite text field is present and visible.");
					}
					else
					{
						Fail("The Apt/Suite text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(city))
					{
						Pass("The City text field is present and visible.");
					}
					else
					{
						Fail("The City text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(state))
					{
						Pass("The State selection drop down is present and visible.");
					}
					else
					{
						Fail("The State selection drop down is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(zipCode))
					{
						Pass("The Zipcode text field is present and visible.");
					}
					else
					{
						Fail("The Zipcode text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(contactNo))
					{
						Pass("The Phone Number text field is present and visible.");
					}
					else
					{
						Fail("The Phone Number text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(companyName))
					{
						Pass("The Company Name text field is present and visible.");
					}
					else
					{
						Fail("The Company Name text field is not present / visible.");
					}
					
					if(BNBasicfeature.isElementPresent(savePayment))
					{
						Pass(" The Save Payment button is displayed.");
					}
					else
					{
						Fail(" The Save Payment button is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(cancelPayment))
					{
						Pass(" The Cancel Payment button is displayed.");
					}
					else
					{
						Fail(" The Cancel Payment button is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(fNametxt))
					{
						if(fNametxt.getText().isEmpty())
						{
							Fail("The First Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The First Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The First Name text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(lNametxt))
					{
						if(lNametxt.getText().isEmpty())
						{
							Fail("The Last Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Last Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Last Name text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(stAddresstxt))
					{
						if(stAddresstxt.getText().isEmpty())
						{
							Fail("The Street Address text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Street Address text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Street Address text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(aptSuiteAddresstxt))
					{
						if(aptSuiteAddresstxt.getText().isEmpty())
						{
							Fail("The Apt / Suite Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Apt / Suite Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Apt / Suite Name text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(citytxt))
					{
						if(citytxt.getText().isEmpty())
						{
							Fail("The City Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The City Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The City Name text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(statetxt1))
					{
						if(statetxt1.getText().isEmpty())
						{
							Fail("The State Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The State Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The State Name text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(zipcodetxt))
					{
						if(zipcodetxt.getText().isEmpty())
						{
							Fail("The Zip Code text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Zip Code text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Zip Code text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(contactNotxt))
					{
						if(contactNotxt.getText().isEmpty())
						{
							Fail("The Contact Number text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Contact Number text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Contact Number text is missing or the value does not matches with the expected one.");
					}
					
					if(BNBasicfeature.isElementPresent(companyNametxt))
					{
						if(contactNotxt.getText().isEmpty())
						{
							Fail("The Company Name text is missing or the value does not matches with the expected one.");
						}
						else
						{
							Pass("The Company Name text is present and the value matches with the expected one.");
						}
					}
					else
					{
						Fail("The Company Name text is missing or the value does not matches with the expected one.");
					}
				}
				else
				{
					Fail("The Add New Payments Page is displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2607 Issue ." + e.getMessage());
				Exception(" BRM - 2607 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Manage Payment Page.");
		}
		
		/*BRM - 2609 Verify that, below Add New payment Method text, the message should be displayed.*/
		ChildCreation(" BRM - 2609 Verify that, below Add New payment Method text, the message should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(addPayDisclaimerMessage))
				{
					Pass("The Disclaimer message is displayed in the Add Payment Page.");
				}
				else
				{
					Fail("The Disclaimer message is not displayed in the Add Payment Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2609 Issue ." + e.getMessage());
				Exception(" BRM - 2609 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2630 Verify that in "Add a New Billing Address" link the country should be in default option "United States"*/
	public void myAccountAddShippingDefaultCountrySelection(String tc)
	{
		String tcId= "";
		if(tc=="BRM2630")
		{
			tcId = "BRM - 2630 Verify that in Add a New Billing Address link the country should be in default option United States.";
		}
		else if(tc=="BRM2657")
		{
			tcId = "BRM - 2657 Verify that in Add a New Shipping Address page the country should be in default option United States.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				String cntry = BNBasicfeature.getExcelVal("BRM2630", sheet, 5);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Thread.sleep(100);
					Select cntrysel = new Select(countrySelection);
					String selctedcntry = cntrysel.getFirstSelectedOption().getText();
					Thread.sleep(100);
					if(selctedcntry.equals(cntry))
					{
						Pass("The default selected Country is : " + selctedcntry + " and it is as Expected.");
					}
					else
					{
						Fail("The default selected Country is : " + selctedcntry + " and it is not as Expected. The expected Country was " + cntry);
					}
				}
				else
				{
					Fail("The Country Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2637 Verify that "State" dropdown menu should be selected in Empty option in default.*/
	public void myAccountStateDefSelection(String tc)
	{
		String tcId = "";
		if(tc=="BRM2637")
		{
			tcId = "BRM - 2637 Verify that State dropdown menu should be selected in Empty option in default.";
		}
		else
		{
			tcId = "BRM - 2665 Verify that State dropdown menu should be selected in Empty option in default.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					BNBasicfeature.scrollup(stAddress,driver);
					Thread.sleep(10);
				}
				if(BNBasicfeature.isElementPresent(state))
				{
					Select cntrysel = new Select(state);
					String selctedState = cntrysel.getFirstSelectedOption().getText();
					//System.out.println(selctedState.length());
					Thread.sleep(10);
					if(selctedState.contains(""))
					{
						Pass("The default selected State empty is and it is as Expected.");
					}
					else
					{
						Fail("The default selected Country is not empty. The Selected State is : " + selctedState + " and it is not as Expected.");
					}
				}
				else
				{
					Fail("The Country Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2627 Verify that "Add a New Billing Address" link should has the certain options and it should match with the creatives.*/
	/* BRM - 2642 Verify that Set as a default Payment Method checklist should be unticked in default. */
	public void myAccountBillingSectionDet()
	{
		ChildCreation("BRM - 2627 Verify that Add a New Billing Address link should has the certain options and it should match with the creatives.");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(fName))
				{
					BNBasicfeature.scrolldown(fName, driver);
					Pass("The First Name text field is present and visible.");
				}
				else
				{
					Fail("The First Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name text field is present and visible.");
				}
				else
				{
					Fail("The Last Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Stree Address text field is present and visible.");
				}
				else
				{
					Fail("The Stree Address text field is not present / visible.");
				}
				
				BNBasicfeature.scrolldown(aptSuiteAddress, driver);
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					Pass("The Apt/Suite text field is present and visible.");
				}
				else
				{
					Fail("The Apt/Suite text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City text field is present and visible.");
				}
				else
				{
					Fail("The City text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State selection drop down is present and visible.");
				}
				else
				{
					Fail("The State selection drop down is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					Pass("The Zipcode text field is present and visible.");
				}
				else
				{
					Fail("The Zipcode text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Phone Number text field is present and visible.");
				}
				else
				{
					Fail("The Phone Number text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name text field is present and visible.");
				}
				else
				{
					Fail("The Company Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(savePayment))
				{
					Pass(" The Save Payment button is displayed.");
				}
				else
				{
					Fail(" The Save Payment button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(cancelPayment))
				{
					Pass(" The Cancel Payment button is displayed.");
				}
				else
				{
					Fail(" The Cancel Payment button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2627 Issue ." + e.getMessage());
				Exception(" BRM - 2627 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
		
		/* BRM - 2642 Verify that Set as a default Payment Method checklist should be unticked in default. */
		ChildCreation(" BRM - 2642 Verify that Set as a default Payment Method checklist should be unticked in default.");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(addPayDefaultPayment))
				{
					Pass("The Set as Default Payment Method checkbox is displayed.");
					boolean chkd = addPayDefaultPayment.getAttribute("aria-checked").contains("true");
					if(chkd==true)
					{
						Fail("The Set as Default Payment Method Checkbox is checked by default.");
					}
					else
					{
						Pass("The Set as Default Payment Method Checkbox is not checked by default.");
					}
				}
				else
				{
					Fail("The Set as Default Payment Method checkbox is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2642 Issue ." + e.getMessage());
				Exception(" BRM - 2642 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2608 Verify that when the "Save" button is clicked without filling the fields in "Add New Payment Method" page the alert message should be displayed.*/
	public void myAccountAddpayEmptyValidation()
	{
		ChildCreation("BRM - 2608 Verify that when the Save button is clicked without filling the fields in Add New Payment Method page the alert message should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String expalert = BNBasicfeature.getExcelVal("BRM2608", sheet, 5);
				log.add("The Expected alert was : " + expalert);
				if(BNBasicfeature.isElementPresent(savePayment))
				{
					Pass(" The Save Payment button is displayed.");
					jsclick(savePayment);
					wait.until(ExpectedConditions.visibilityOf(addPayError));
					Thread.sleep(100);
					String actAlert = addPayError.getText();
					log.add("The Actual alert is : " + actAlert);
					if(expalert.contains(actAlert))
					{
						Pass("The appropriate error is raised when user tries to save the Shipping Address without values .",log);
					}
					else
					{
						Fail("The appropriate error is raised when user tries to save the Shipping Address without values ,but there is mismatch in the alert.",log);
					}
				}
				else
				{
					Fail(" The Save Payment button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2608 Issue ." + e.getMessage());
				Exception(" BRM - 2608 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2610 Verify that "Card Number" text box should accept a maximum of 16 digits.*/
	public void myAccountAddPayCCLimit()
	{
		ChildCreation("BRM - 2610 Verify that Card Number text box should accept a maximum of 16 digits.");
		if(addNewPay==true)
		{
			try
			{
				String ccNum = BNBasicfeature.getExcelNumericVal("BRM2609", sheet, 13);
				String[] val = ccNum.split("\n");
				if(BNBasicfeature.isElementPresent(newccNumber))
				{
					Pass("The Credit Card field is present and visible.");
					BNBasicfeature.scrolldown(newccNumber, driver);
					Thread.sleep(10);
					try
					{
						for(int i = 0;i<val.length;i++)
						{
							newccNumber.clear();
							newccNumber.sendKeys(val[i]);
							Thread.sleep(100);
							if(newccNumber.getAttribute("value").length()<=16)
							{
								log.add("The entered value from the Excel file is " + val[i] + " and its length is " + val[i].length() + ". The current value in the Credit Card Number field is " + newccNumber.getAttribute("value").toString() + " and its length is " + newccNumber.getAttribute("value").length());
								Pass("The lenght of the Credit Card Number field is less than 16.",log);
							}
							else
							{
								log.add("The entered value from the Excel file is " + val[i] + " and its length is " + val[i].length() + ". The current value in the Credit Card Number field is " + newccNumber.getAttribute("value").toString() + " and its length is " + newccNumber.getAttribute("value").length());
								Fail("The lenght of the Credit Card Number field is more than 16.",log);
							}
						}
						
					}
					catch(Exception e)
					{
						Exception(" There is something wrong. Please Check. " + e.getMessage());
					}
				}
				else
				{
					Fail("The Credit Card text field is not present / visible.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2610 Issue ." + e.getMessage());
				Exception(" BRM - 2610 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2612 Verify that "Expiration Month" drop down menu has a maximum of 12 values.*/
	public void myAccountAddPayMonth()
	{
		ChildCreation(" BRM - 2612 Verify that Expiration Month drop down menu has a maximum of 12 values.");
		if(addNewPay==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(newccMonth))
				{
					log.add("The Month field is displayed.");
					Select mnthsel = new Select(newccMonth);
					act.moveToElement(newccMonth).click().build().perform();
					int size = mnthsel.getOptions().size();
					if(size==13)
					{
						for(int i = 1; i <size; i++)
						{
							String mnthname = mnthsel.getOptions().get(i).getText();
							if(mnthname.isEmpty())
							{
								Fail("The Month Name is not displayed.The Month Count is : " + i);
							}
							else
							{
								log.add("The Month list includes " + mnthname);
							}
						}
						Pass("The Month Size is : " + size,log);
					}
					else
					{
						Fail("The Month size is less than 12. Please Check");
					}
				}
				else
				{
					Fail("The Month field is not displayed.");
				}
				act.moveToElement(newccMonth).click().build().perform();
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2612 Issue ." + e.getMessage());
				Exception(" BRM - 2612 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2615  Verify that "Expiration Year" drop down menu has a maximum of 12 values.*/
	public void myAccountAddPayYearList()
	{
		ChildCreation("BRM - 2615  Verify that Expiration Year drop down menu has a maximum of 12 values.");
		if(addNewPay==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(newccYear))
				{
					log.add("The year field is displayed.");
					Select mnthsel = new Select(newccYear);
					act.moveToElement(newccYear).click().build().perform();
					int size = mnthsel.getOptions().size();
					if(size==12)
					{
						for(int i = 1; i <size; i++)
						{
							String mnthname = mnthsel.getOptions().get(i).getText();
							if(mnthname.isEmpty())
							{
								Fail("The Year is not displayed.The Year Count is : " + i);
							}
							else
							{
								log.add("The Year list includes " + mnthname);
							}
						}
						Pass("The Year Size is : " + size,log);
					}
					else
					{
						Fail("The Year size is less than 12. Please Check");
					}
				}
				else
				{
					Fail("The Year field is not displayed.");
				}
				act.moveToElement(newccYear).click().build().perform();
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2615 Issue ." + e.getMessage());
				Exception(" BRM - 2615 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2611 Verify that, Expiration Year text box should have year starting of Current Year.*/
	public void myAccountAddPayCurrYear()
	{
		ChildCreation(" BRM - 2611 Verify that, Expiration Year text box should have year starting of Current Year.");
		if(addNewPay==true)
		{
			try
			{
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(newccYear))
				{
					log.add("The Year field is displayed.");
					Select yrsel = new Select(newccYear);
					act.moveToElement(newccYear).click().build().perform();
					if(yrsel.getOptions().size()>0)
					{
						Pass("The Year drop down is clicked and it is not empty");
						int year = Calendar.getInstance().get(Calendar.YEAR);
						String yr = Integer.toString(year);
						if(yr.equals(yrsel.getOptions().get(1).getText()))
						{
							log.add("The current year is " + yr);
							log.add("The first value in the year drop down is " + yrsel.getOptions().get(1).getText());
							Pass("The Current year and the drop down first value matches.",log);
						}
						else
						{
							log.add("The current year is " + yr);
							log.add("The first value in the year drop down is " + yrsel.getOptions().get(1).getText());
							Fail("The Current year and the drop down first value down not matches.",log);
						}
					}
					else
					{
						Fail("The Month size is less than 12. Please Check");
					}
				}
				else
				{
					Fail("The Month field is not displayed.");
				}
				act.moveToElement(newccYear).click().build().perform();
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2611 Issue ." + e.getMessage());
				Exception(" BRM - 2611 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2632 Verify that "State" dropdown menu should be changed to "State" text box, when other than "United States", "Canada" country is selected.*/
	/* BRM - 2660 Verify that State dropdown menu should be changed to State text box, when other than United States, Canada country is selected.*/
	public void myAccountCountryTextBox(String tc)
	{
		String tcId = "";
		if(tc=="BRM2632")
		{
			tcId = "BRM - 2632 Verify that State dropdown menu should be changed to State text box, when other than United States, Canada country is selected.";
		}
		else if(tc=="BRM2660")
		{
			tcId = " BRM - 2660 Verify that State dropdown menu should be changed to State text box, when other than United States, Canada country is selected.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				Actions act = new Actions(driver);
				String countryname = BNBasicfeature.getExcelVal("BRM2632", sheet, 5);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Select cntrysel = new Select(countrySelection);
					act.moveToElement(countrySelection).click().build().perform();
					Thread.sleep(100);
					cntrysel.selectByVisibleText(countryname);
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(state))
					{
						BNBasicfeature.scrolldown(state,driver);
					}
					String role = state.getAttribute("role");
					if(role.contains("text"))
					{
						Pass("The State Text field is changed to Text Box.");
					}
					else
					{
						Fail( "The State Text field is not changed to Text Box.");
					}
				}
				else
				{
					Fail("The Country Selection is not displayed.");
				}
				act.moveToElement(countrySelection).click().build().perform();
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2631 Verify that when the country is changed the state should be also changed*/
	/* BRM - 2659 Verify that when the country is changed the state should be also changed.*/
	public void myAccountAddShippingStateListingValidation(String tc)
	{
		String tcId = "";
		if(tc=="BRM2631")
		{
			tcId = "BRM - 2631 Verify that when the country is changed the state should be also changed.";
		}
		else if(tc=="BRM2659")
		{
			tcId = "BRM - 2659 Verify that when the country is changed the state should be also changed.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			int brmkey = sheet.getLastRowNum();
			for(int i = 0; i <= brmkey; i++)
			{
				String tcid = "BRM2631";
				String cellCont = sheet.getRow(i).getCell(0).getStringCellValue().toString();
				if(cellCont.equals(tcid))
				{
					String countryname = sheet.getRow(i).getCell(5).getStringCellValue().toString();
					String stateName =  sheet.getRow(i).getCell(17).getStringCellValue().toString();
					boolean statefound = false;
					try
					{
						if(BNBasicfeature.isElementPresent(countrySelection))
						{
							BNBasicfeature.scrollup(countrySelection,driver);
							Actions act = new Actions(driver);
							Select cntrysel = new Select(countrySelection);
							act.moveToElement(countrySelection).click().build().perform();
							Thread.sleep(100);
							cntrysel.selectByVisibleText(countryname);
							Thread.sleep(100);
							BNBasicfeature.scrolldown(state,driver);
							Select statesel = new Select(state);
							act.moveToElement(state).click().build().perform();
							for(int j = 1; j<statesel.getOptions().size(); j++)
							{
								if(statesel.getOptions().get(j).getText().contains(stateName))
								{
									statefound = true;
									break;
								}
								else
								{
									statefound = false;
									continue;
								}
							}
							
							if(statefound == true)
							{
								Pass("The State : " + stateName + " is present for the selected Country " + countryname);
							}
							else
							{
								Fail("The State : " + stateName + " is not present for the selected Country " + countryname);
							}
							act.moveToElement(state).click().build().perform();
						}
						else
						{
							Fail("The Country Selection Container is not visible / present.");
						}
					}
					catch(Exception e)
					{
						System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
						Exception(" BRM - " + tcId + " Issue." + e.getMessage());
					}
				}
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2640 Verify that for "Canada" the zip code should contain alphanumerics,if only the numbers then the alert message should be displayed.*/
	public void myAccountBillingCanadaZipValidation()
	{
		ChildCreation(" BRM - 2640 Verify that for Canada the zip code should contain alphanumerics,if only the numbers then the alert message should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2640", sheet, 20);
				String[] zip = cellVal.split("\n");
				String alert = BNBasicfeature.getExcelVal("BRM2639", sheet, 5);
				
				String cntry = BNBasicfeature.getExcelVal("BRM2640", sheet, 6);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Thread.sleep(100);
					Select cntrysel = new Select(countrySelection);
					cntrysel.selectByValue("CA");
					String selctedcntry = cntrysel.getFirstSelectedOption().getText();
					Thread.sleep(100);
					if(selctedcntry.equals(cntry))
					{
						for(int i = 0; i<zip.length;i++)
						{
							BNBasicfeature.scrolldown(fName,driver);
							zipCode.clear();
							zipCode.sendKeys(zip[i]);
							jsclick(savePayment);
							wait.until(ExpectedConditions.visibilityOf(addPayError));
							Thread.sleep(100);
							boolean isNumeric = StringUtil.isNumeric(zip[i]);
							if(isNumeric==true)
							{
								if(addPayError.getText().contains(alert))
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + driver.findElement(By.xpath("//*[@id='addPaymentErr']")).getText());
									Pass("The alert for the invalid Zip Code field is raised and it matches the expected content.");
								}
								else
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + driver.findElement(By.xpath("//*[@id='addPaymentErr']")).getText());
									Fail("The alert for the invalid Zip Code field is raised and it does not match the expected content.",log);
								}
							}
							else
							{
								if(addPayError.getText().contains(alert))
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + driver.findElement(By.xpath("//*[@id='addPaymentErr']/p[8]")).getText());
									Fail("The alert for the valid Zip Code field is raised and it should not have been raised.",log);
								}
								else
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									Pass("No alert for the valid Zip Code field is raised.",log);
								}
							}
						}
					}
					else
					{
						Skip("The Selected Default Country is not UNITED STATES. ");
					}
				}
				else
				{
					Fail(" The Country Selection drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2640 Issue ." + e.getMessage());
				Exception(" BRM - 2640 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2628 Verify that when the "Save" button is clicked without filling all the fields in "Add a New Billing Address" link then it should display the alert message.*/
	public void myAccountBillValidation()
	{
		ChildCreation(" BRM - 2628 Verify that when the Save button is clicked without filling all the fields in Add a New Billing Address link then it should display the alert message.");
		if(addNewPay==true)
		{
			try
			{
				String ccNum = BNBasicfeature.getExcelNumericVal("BRM2628", sheet, 13);
				String expalert  = BNBasicfeature.getExcelNumericVal("BRM2628", sheet, 5);
				String ccName = BNBasicfeature.getExcelVal("BRM2628", sheet, 14);
				if(BNBasicfeature.isElementPresent(newccNumber))
				{
					BNBasicfeature.scrollup(newccNumber, driver);
				}
				newccNumber.clear();
				newccNumber.sendKeys(ccNum);
				newccName.clear();
				newccName.sendKeys(ccName);
				Select mntsel = new Select(newccMonth);
				mntsel.selectByIndex(10);
				Select yrsel = new Select(newccYear);
				yrsel.selectByIndex(5);
				if(BNBasicfeature.isElementPresent(city))
				{
					BNBasicfeature.scrolldown(city, driver);
				}
				
				zipCode.clear();
				jsclick(savePayment);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				Thread.sleep(100);
				String actAlert = addPayError.getText();
				log.add("The Actual alert is : " + actAlert);
				if(expalert.contains(actAlert))
				{
					Pass("The appropriate error is raised when user tries to save the Shipping Address without values .",log);
				}
				else
				{
					Fail("The appropriate error is raised when user tries to save the Shipping Address without values ,but there is mismatch in the alert.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2628 Issue ." + e.getMessage());
				Exception(" BRM - 2628 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2629 Verify that when the numbers are entered in "First Name", "Last Name" field then it should not display the alert message.*/
	public void myAccountBillFNLNValidation()
	{
		ChildCreation(" BRM - 2629 Verify that when the numbers are entered in First Name, Last Name field then it should not display the alert message.");
		if(addNewPay==true)
		{
			try
			{
				String fstName = BNBasicfeature.getExcelNumericVal("BRM2629", sheet, 15);
				String expalert  = BNBasicfeature.getExcelNumericVal("BRM2629", sheet, 5);
				String lstname = BNBasicfeature.getExcelVal("BRM2629", sheet, 16);
				BNBasicfeature.scrollup(countrySelection, driver);
				Thread.sleep(100);
				fName.clear();
				fName.sendKeys(fstName);
				lName.clear();
				lName.sendKeys(lstname);
				jsclick(savePayment);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				Thread.sleep(100);
				String actAlert = addPayError.getText();
				log.add("The Actual alert is : " + actAlert);
				if(actAlert.contains(expalert))
				{
					Pass("The appropriate error is raised when user tries to save the Shipping Address with invalid First Name and Last Name values  .",log);
				}
				else
				{
					Fail("The appropriate error is raised when user tries to save the Shipping Address with invalid First Name and Last Name values ,but there is mismatch in the alert.",log);
				}
				
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2629 Issue ." + e.getMessage());
				Exception(" BRM - 2629 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2638 Verify that for "United States" the zip code should accept a maximum of 10 digits.*/
	public void myAccountAddBillingZipCodeLimit(String tc)
	{
		String tcId = "";
		if(tc=="BRM2638")
		{
			tcId = "BRM - 2638 Verify that for United States the zip code should accept a maximum of 10 digits.";
		}
		else if(tc=="BRM2641")
		{
			tcId = " BRM - 2641 Verify that zip code should accept a maximum of 10 digits.";
		}
		else if(tc=="BRM2666")
		{
			tcId= " BRM - 2666 Verify that for United States the zip code should accept a maximum of 10 digits.";
		}
		else
		{
			tcId = " BRM - 2669 Verify that zip code should accept a maximum of 10 digits.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2638", sheet, 20);
				String[] Zip = cellVal.split("\n");
				String cntry = BNBasicfeature.getExcelVal("BRM2630", sheet, 5);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Thread.sleep(100);
					Select cntrysel = new Select(countrySelection);
					cntrysel.selectByValue("US");
					String selctedcntry = cntrysel.getFirstSelectedOption().getText();
					Thread.sleep(100);
					if(selctedcntry.equals(cntry))
					{
						if(BNBasicfeature.isElementPresent(stAddress))
						{
							BNBasicfeature.scrollup(stAddress,driver);
						}
						Thread.sleep(100);
						if(BNBasicfeature.isElementPresent(zipCode))
						{
							for(int i = 0; i<Zip.length; i++)
							{
								zipCode.clear();
								zipCode.sendKeys(Zip[i]);
								Thread.sleep(100);
								int len = zipCode.getAttribute("value").length();
								log.add(" The Value from the excel file was : " + Zip[i] + " and its length was : " + Zip[i].length());
								log.add(" The Current Value in the Zipcode field is : " + zipCode.getAttribute("value") + " and its length was : " + len);
								if(len<=10)
								{
									Pass("The Zipcode value is less than 10 only.",log);
								}
								else
								{
									Fail("The Zipcode Value is more than 10.",log);
								}
							}
						}
						else
						{
							Fail("The Zipcode text field is not displayed.");
						}
					}
					else
					{
						Skip("The Default Country is not United States.");
					}
				}
				else
				{
					Fail("The Country Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2639 Verify that when zip code contains characters or alphanumerics for "United States" then alert message should be thrown.)*/
	public void myAccountAddBillingCCBillingZipcodeValidation()
	{
		ChildCreation(" BRM-2639 Verify that when zip code contains characters or alphanumerics for United States then alert message should be thrown.");
		if(addNewPay==true)
		{
			try
			{
				String stadd = BNBasicfeature.getExcelVal("BRM2639", sheet, 16); 
				String zpcode = BNBasicfeature.getExcelVal("BRM2639", sheet, 20);
				String alert = BNBasicfeature.getExcelVal("BRM2639", sheet, 5);
				
				if(BNBasicfeature.isElementPresent(fName))
				{
					BNBasicfeature.scrolldown(fName,driver);
				}
				stAddress.clear();
				stAddress.sendKeys(stadd);
				zipCode.clear();
				zipCode.sendKeys(zpcode);
				BNBasicfeature.scrolldown(savePayment,driver);
				jsclick(savePayment);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				Thread.sleep(100);
				if(addPayError.getText().contains(alert))
				{
					log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(5).getText());
					Pass("The alert for the invalid Zip Code field is raised and it matches the expected content.");
				}
				else
				{
					log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(5).getText());
					Fail("The alert for the invalid Zip Code field is raised and it does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2639 Issue ." + e.getMessage());
				Exception(" BRM - 2639 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2633 Verify that when characters are entered in "Phone Number" field it should thrown an alert message*/
	/* BRM - 2661 Verify that when characters are entered in "Phone Number" field it should thrown an alert message.*/
	public void myAccountAddBillingCCBillingPhoneNoValidation(String tc)
	{
		String tcId = "";
		if(tc=="BRM2633")
		{
			tcId = " BRM - 2633 Verify that when characters are entered in Phone Number field it should thrown an alert message.";
		}
		else if(tc == "BRM2661")
		{
			tcId = "BRM - 2661 Verify that when characters are entered in Phone Number field it should thrown an alert message.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2633", sheet, 5);
				String cntno = BNBasicfeature.getExcelVal("BRM2633", sheet, 21);
				String cty = BNBasicfeature.getExcelVal("BRM2633", sheet, 19);
				BNBasicfeature.scrolldown(lName,driver);
				city.clear();
				city.sendKeys(cty);
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(savePayment);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				wait.until(ExpectedConditions.visibilityOfAllElements(individualAddPayError));
				Thread.sleep(100);
				if(addPayError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Pass("The alert for the invalid Contact No field is raised and it matches the expected content.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Fail("The alert for the invalid Contact No field is raised and it does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2636 Verify that "Phone Number" field should accept only one special character in the beginning.*/
	public void myAccountAddBillingSpecialCharPhoneNo()
	{
		ChildCreation("BRM - 2664 Verify that Phone Number field should accept only one special character in the beginning.");
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2634", sheet, 5);
				String cntno = BNBasicfeature.getExcelNumericVal("BRM2636", sheet, 21);
				if(BNBasicfeature.isElementPresent(lName))
				{
					BNBasicfeature.scrolldown(lName,driver);
				}
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(savePayment);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				Thread.sleep(100);
				if(addPayError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Fail(" The Special Character at the beginning of the Contact Number when used alert is raised.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Pass("The Special Character at the beginning of the Contact Number when used no alert is raised.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2664 Issue ." + e.getMessage());
				Exception(" BRM - 2664 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2634 Verify that when characters are entered in "Phone Number" field it should thrown an alert message*/
	public void myAccountAddBillingCCBillingPhoneNoValidation1()
	{
		ChildCreation(" BRM - 2633 Verify that when characters are entered in Phone Number field it should thrown an alert message.");
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2634", sheet, 5);
				String cntno = BNBasicfeature.getExcelNumericVal("BRM2634", sheet, 21);
				if(BNBasicfeature.isElementPresent(lName))
				{
					BNBasicfeature.scrolldown(lName,driver);
				}
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(savePayment);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addPayError));
				Thread.sleep(100);
				if(addPayError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Pass("The alert for the invalid Contact No field is raised and it matches the expected content.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddPayError.get(4).getText());
					Fail("The alert for the invalid Contact No field is raised and it does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2634 Issue ." + e.getMessage());
				Exception(" BRM - 2634 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2635 Verify that "Phone Number field" should accept a maximum of 20 digits*/
	public void myAccountPhoneNumberLengthValidation(String tc)
	{
		String tcId = "";
		if(tc=="BRM2635")
		{
			tcId = " BRM - 2635 Verify that Phone Number field should accept a maximum of 20 digits.";
		}
		else if(tc=="BRM2663")
		{
			tcId = " BRM - 2663 Verify that Phone Number field should accept a maximum of 20 digits.";
		}
		ChildCreation(tcId);
		if(addNewPay==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2635", sheet, 21);
				String[] contactNumber = cellVal.split("\n");
				for(int i  = 0; i<contactNumber.length; i++)
				{
					BNBasicfeature.scrolldown(lName,driver);
					contactNo.clear();
					contactNo.sendKeys(contactNumber[i]);
					if(contactNo.getAttribute("value").length()<=60)
					{
						log.add("The entered Contact Number from the excel file was " + contactNumber[i] + " and its length is " + contactNumber[i].length());
						log.add("The Current Value in the Contact Number field is " + contactNo.getAttribute("value") + " and its length is " + contactNo.getAttribute("value").length());
						Pass("The accepted lenght of the phone number is less than or equal to 60.",log);
					}
					else
					{
						log.add("The entered Contact Number from the excel file was " + contactNumber[i] + " and its length is " + contactNumber[i].length());
						log.add("The Current Value in the Contact Number field is " + contactNo.getAttribute("value") + " and its length is " + contactNo.getAttribute("value").length());
						Fail("The accepted lenght of the phone number is greater than or equal to 60.",log);
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2621 Verify that "Add a New Billing Address" link should be highlighted in bold and should be selected in default.*/
	public void myAccountAddPayAddBillingAddress()
	{
		ChildCreation("BRM - 2621 Verify that Add a New Billing Address link should be highlighted in bold and should be selected in default.");
		if(addNewPay==true)
		{
			try
			{
				String expVal = BNBasicfeature.getExcelVal("BRM2621", sheet, 5);
				String expcolor = BNBasicfeature.getExcelVal("BRM2621", sheet, 6);
				log.add("The Expected Value was : " + expVal);
				if(BNBasicfeature.isElementPresent(addPayAddBillingAddressTitle))
				{
					BNBasicfeature.scrolldown(addPayAddBillingAddressTitle, driver);
					Thread.sleep(100);
					Pass("The Add a New Billing Address is displayed.");
					String actVal = addPayAddBillingAddressTitle.getText();
					if(actVal.contains(expVal))
					{
						Pass( "The Value matches the expected one.",log);
					}
					else
					{
						Fail( "The Value does not matches the expected one.",log);
					}
					
					log.add("The Expected color was : " + expcolor);
					String val = addPayAddBillingAddressTitle.getCssValue("color");
					Color colhxv = Color.fromString(val);
					String hexColor =colhxv.asHex();
					if(expcolor.contains(hexColor))
					{
						Pass("The Add a New billing address is highlighted in the expected color.",log);
					}
					else
					{
						Fail("The Add a New billing address is not highlighted in the expected color.",log);
					}
				}
				else
				{
					Fail("The Add a New Billing Address is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2621 Issue ." + e.getMessage());
				Exception(" BRM - 2621 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2622 Verify that when "Select a Saved Billing Address" link is clicked then it should swap the links and should be highlighted.*/
	public void myAccountSelectSavedbillingAdd()
	{
		ChildCreation("BRM - 2622 Verify that when Select a Saved Billing Address link is clicked then it should swap the links and should be highlighted.");
		if(addNewPay==true)
		{
			try
			{
				String expVal = BNBasicfeature.getExcelVal("BRM2622", sheet, 5);
				String expcolor = BNBasicfeature.getExcelVal("BRM2622", sheet, 6);
				log.add("The Expected Value was : " + expVal);
				if(BNBasicfeature.isElementPresent(addPayAddBillingSavedAddressLink))
				{
					jsclick(addPayAddBillingSavedAddressLink);
					Thread.sleep(100);
					wait.until(ExpectedConditions.attributeContains(addPaySavedAddress, "aria-hidden", "false"));
					Thread.sleep(100);
					Pass("The Select a Saved Billing Address is displayed.");
					String actVal = addPaySavedAddressTitle.getText();
					if(actVal.contains(expVal))
					{
						Pass( "The Value matches the expected one.",log);
					}
					else
					{
						Fail( "The Value does not matches the expected one.",log);
					}
					
					log.add("The Expected color was : " + expcolor);
					String val = addPaySavedAddressTitle.getCssValue("color");
					Color colhxv = Color.fromString(val);
					String hexColor =colhxv.asHex();
					if(expcolor.contains(hexColor))
					{
						Pass("The Add a New billing address is highlighted in the expected color.",log);
					}
					else
					{
						Fail("The Add a New billing address is not highlighted in the expected color.",log);
					}
				}
				else
				{
					Fail("The Add a New Billing Address is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2622 Issue ." + e.getMessage());
				Exception(" BRM - 2622 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2623 Verify that on selecting "Select a Saved Billing Address" link, it should display the Saved Address along with radio button and "Set as a default Payment Method" checklist button along with "Save" and "Cancel" button.*/
	public void myAccountSavedAddressDet()
	{
		ChildCreation("BRM - 2623 Verify that on selecting Select a Saved Billing Address link, it should display the Saved Address along with radio button and Set as a default Payment Method checklist button along with Save and Cancel button.");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isListElementPresent(addPaySavedAddressRadioButton))
				{
					Pass("The Select Address Radio button is displayed.");
				}
				else
				{
					Fail("The Select Address Radio button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(savePayment))
				{
					Pass(" The Save Payment button is displayed.");
				}
				else
				{
					Fail(" The Save Payment button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(cancelPayment))
				{
					Pass(" The Cancel Payment button is displayed.");
				}
				else
				{
					Fail(" The Cancel Payment button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(savePayment))
				{
					Pass(" The Save Payment button is displayed.");
				}
				else
				{
					Fail(" The Save Payment button is not displayed.");
				}
				
				if(BNBasicfeature.isElementPresent(addPayDefaultPaymentCheckbox))
				{
					Pass(" The Set as Default Payment Checkbox is displayed.");
				}
				else
				{
					Fail(" The Set as Default Payment Checkbox is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2623 Issue ." + e.getMessage());
				Exception(" BRM - 2623 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2645 Verify that when "Cancel" button is clicked in "Add a New Billing Address" link then the entries in all the fields should be cleared and previous page should be displayed*/
	public void myAccountAddBillingCancelNavigation()
	{
		ChildCreation(" BRM - 2645 Verify that when Cancel button is clicked in Add a New Billing Address link then the entries in all the fields should be cleared and previous page should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRMPayNavigation", sheet, 5);
				Actions act = new Actions(driver);
				if(BNBasicfeature.isElementPresent(cancelPayment))
				{
					BNBasicfeature.scrolldown(cancelPayment, driver);
					Pass("The Cancel button in the Remove Payments Page is displayed.");
					boolean pgLoad = false;
					WebDriverWait w1 = new WebDriverWait(driver, 3);
					do
					{
						try
						{
							act.moveToElement(cancelPayment).doubleClick().build().perform();
							w1.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(myAccountPageTitle),
									ExpectedConditions.invisibilityOfAllElements(addPaySavedAddressRadioButton)
									)
							);
							pgLoad = true;
						}
						catch(Exception e1)
						{
							pgLoad = false;
						}
					}while(pgLoad==false);
					
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						addNewPay = false;
						Pass("The user is redirected to the Payments Page.");
						log.add("The Expected Title was : " + pgeTitle);
						String title = myAccountPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(title.contains(pgeTitle))
						{
							Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
						}
						else
						{
							Fail("The Payment Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The user is not redirected to the Payments Page.");
					}
				}
				else
				{
					Fail("The Cancel button in the Remove Payments Page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2645 Issue ." + e.getMessage());
				Exception(" BRM - 2645 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add New Payment Page.");
		}
	}
	
	/* BRM - 2601 Verify that when "Edit" link is clicked then "Edit this Payment Method" overlay should be shown.*/
	public void myAccountpaymentPageEditPayment()
	{
		ChildCreation(" BRM - 2601 Verify that when Edit link is clicked then Edit this Payment Method overlay should be shown.");
		if(addNewPay==false)
		{
			String ccExpMonth = "",ccExpYear = "",firstName = "",lastName = "",editExpMonth,editExpYear,editfirstName,editlastName;
			try
			{
				String expTitle = BNBasicfeature.getExcelVal("BRMPayEditPage", sheet, 5);
				if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentAddressEdit))
				{
					Pass("The Edit option is displayed in the Default Payment Section. ");
					if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentCCExpMonth))
					{
						if(paymentsDefaultPaymentCCExpMonth.getText().isEmpty())
						{
							Fail("The Credit Card Expiration Month is not displayed.It seems to be empty.");
						}
						else
						{
							ccExpMonth = paymentsDefaultPaymentCCExpMonth.getText();
						}
					}
					else
					{
						Fail("The Credit Card Expiration Month is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentCCExpYear))
					{
						if(paymentsDefaultPaymentCCExpYear.getText().isEmpty())
						{
							Fail("The Credit Card Expiration Year is not displayed.It seems to be empty.");
						}
						else
						{
							ccExpYear =  paymentsDefaultPaymentCCExpYear.getText();
						}
					}
					else
					{
						Fail("The Credit Card Expiration Year is not displayed.");
					}
					
					/*if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentCCNumber))
					{
						if(paymentsDefaultPaymentCCNumber.getText().isEmpty())
						{
							Fail("The Credit Card Number is not displayed.It seems to be empty.");
						}
						else
						{
							ccNumber = paymentsDefaultPaymentCCNumber.getText();
						}
					}
					else
					{
						Fail("The Credit Card Number is not displayed.");
					}*/
					
					if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentCCFirstName))
					{
						if(paymentsDefaultPaymentCCFirstName.getText().isEmpty())
						{
							Fail("The First Name is not displayed.It seems to be empty.");
						}
						else
						{
							firstName = paymentsDefaultPaymentCCFirstName.getText();
						}
					}
					else
					{
						Fail("The First Name is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(paymentsDefaultPaymentCCLastName))
					{
						if(paymentsDefaultPaymentCCLastName.getText().isEmpty())
						{
							Fail("The Last Name is not displayed.It seems to be empty.");
						}
						else
						{
							lastName = paymentsDefaultPaymentCCLastName.getText();
						}
					}
					else
					{
						Fail("The Last Name is not displayed.");
					}
					
					jsclick(paymentsDefaultPaymentAddressEdit);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(editPaymentsPage),
							ExpectedConditions.visibilityOf(pgetitle)
							)
					);
					addNewPay = true;
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(pgetitle))
					{
						Pass("The user is navigated to the Edit payment page.");
						log.add("The expected title was : " + expTitle);
						String title = pgetitle.getText();
						if(expTitle.contains(title))
						{
							Pass("The edit page title matches.",log);
						}
						else
						{
							Fail("The edit page title mismatches.",log);
						}
						
						/*if(BNBasicfeature.isElementPresent(paymentsEditPaymentCCNumber))
						{
							if(paymentsEditPaymentCCNumber.getText().isEmpty())
							{
								Fail("The Credit Card Number is not displayed.It seems to be empty.");
							}
							else
							{
								editccNumber = paymentsEditPaymentCCNumber.getText();
								log.add("The Expected Number was : " + ccNumber);
								log.add("The Actual Number is : " + editccNumber);
								if(ccNumber.equals(editccNumber))
								{
									Pass("The Edit Payments Page for the Selected Credit Card is displayed and the number matches the expected one.",log);
								}
								else
								{
									Fail("There is some mismatch in the expected and actual value.",log);
								}
							}
						}
						else
						{
							Fail("The Credit Card Number is not displayed.");
						}*/
						
						if(BNBasicfeature.isElementPresent(paymentsEditPaymentCCExpMonth))
						{
							Select sel = new Select(paymentsEditPaymentCCExpMonth);
							editExpMonth = sel.getFirstSelectedOption().getText();
							log.add("The Expected Expiry Month was : " + ccExpMonth);
							log.add("The Actual Number is : " + editExpMonth);
							if(editExpMonth.contains(ccExpMonth))
							{
								Pass("The Expiry Month matches with the expected one.",log);
							}
							else
							{
								Fail("There is some mismatch in the selected Expiry Month and the actual / displayed expiry month.",log);
							}
						}
						else
						{
							Fail("The Expiry Month drop down is not displayed in the Edit Payment Page.");
						}
						
						if(BNBasicfeature.isElementPresent(paymentsEditPaymentCCExpYear))
						{
							Select sel = new Select(paymentsEditPaymentCCExpYear);
							editExpYear = sel.getFirstSelectedOption().getText();
							log.add("The Expected Expiry year was : " + ccExpYear);
							log.add("The Actual year is : " + editExpYear);
							if(editExpYear.contains(ccExpYear))
							{
								Pass("The Expiry Year matches with the expected one.",log);
							}
							else
							{
								Fail("There is some mismatch in the selected Expiry Year and the actual / displayed expiry Year.",log);
							}
						}
						else
						{
							Fail("The Expiry Year drop down is not displayed in the Edit Payment Page.");
						}
						
						if(BNBasicfeature.isElementPresent(editfName))
						{
							Pass("The First Name field in the edit page is displayed.");
							if(editfName.getAttribute("value").isEmpty())
							{
								Fail("The First Name field is empty.");
							}
							else
							{
								editfirstName = editfName.getAttribute("value").toString();
								log.add("The Expected First Name was : " + firstName);
								log.add("The Actual First Name is : " + editfirstName);
								if(firstName.equals(editfirstName))
								{
									Pass("The First Name name in the edit payment page matches with the expected one.",log);
								}
								else
								{
									Fail("There is some mismatch in the expected and actual value.",log);
								}
							}
						}
						else
						{
							Fail("The First Name field in the edit page is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(editlName))
						{
							Pass("The Last Name field in the edit page is displayed.");
							if(editlName.getAttribute("value").isEmpty())
							{
								Fail("The Last Name field is empty.");
							}
							else
							{
								editlastName = editlName.getAttribute("value").toString();
								log.add("The Expected Last Name was : " + lastName);
								log.add("The Actual Last Name is : " + editlastName);
								if(lastName.equals(editlastName))
								{
									Pass("The First Name name in the edit payment page matches with the expected one.",log);
								}
								else
								{
									Fail("There is some mismatch in the expected and actual value.",log);
								}
							}
						}
						else
						{
							Fail("The Last Name field in the edit page is not displayed.");
						}
					}
					else
					{
						Fail("The user is not in the edit payments page.");
					}
				}
				else
				{
					Fail("No Edit option is not displayed in the Default payment Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2601 Issue ." + e.getMessage());
				Exception(" BRM - 2601 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to navigate to the Manage Payment Page.");
		}
	}
	
	/* BRM - 5444 Verify on clicking the Shipping Method section in the Account settings page , should redirect to shipping address overlay and should display the options to edit the Shipping Method.*/
	public void myAccountShippingAddressPageNavigation()
	{
		ChildCreation(" BRM - 5444 Verify on clicking the Shipping Method section in the Account settings page , should redirect to shipping address overlay and should display the options to edit the Shipping Method.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5443", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.and(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(myAccountPageTitle),
										ExpectedConditions.visibilityOf(defaultAddressForm)
									)
								);
								
								manageAccountLoaded = true;
								String pgeTitle = BNBasicfeature.getExcelVal("BRM5443", sheet, 3);
								if(BNBasicfeature.isElementPresent(myAccountPageTitle))
								{
									Pass("The user is redirected to the Payments Page.");
									log.add("The Expected Title was : " + pgeTitle);
									String title = myAccountPageTitle.getText();
									log.add("The Actual Title is : " + title);
									if(pgeTitle.contains(title))
									{
										Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
										shipPageLoaded = true;
									}
									else
									{
										Fail("The Payment Page title does not match the expected content.",log);
									}
								}
								else
								{
									Fail("The user is not redirected to the Payments Page.");
								}
								
								if(BNBasicfeature.isElementPresent(defaultAddressForm))
								{
									shipPageLoaded = true;
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5444 Issue ." + e.getMessage());
				Exception(" BRM - 5444 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the payments page.");
		}
	}
	
	/* BRM - 2646 Verify that when "Manage Default Address" link is clicked then it should display "Manage Address Book" Page.*/
	/* BRM - 5443 Verify on clicking the Shipping address section in the Manage Address Book page,Should redirect to Shipping Address Overlay and display the options to edit the Shipping address.*/
	public void myAccountManageAddressBookNavigation()
	{
		ChildCreation(" BRM - 2646 Verify that when Manage Default Address link is clicked then it should display Manage Address Book Page.");
		boolean BRM5443 = false;
		if(manageAccountLoaded==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRM2646", sheet, 6);
				boolean lnkFound = false;
				//int i = 0;
				
				String exppTitle = BNBasicfeature.getExcelVal("BRM2646", sheet, 5);
				ExpectedConditions.visibilityOf(header);
				log.add("The Expected title was : " + exppTitle);
				titFound = getManageAccountPageTitles(exppTitle);
				if(titFound==true)
				{
					Pass("The Default Delivery Speed title is found on the Account Settings Page.",log);
					lnkFound = true;
				}
				else
				{
					Fail(" The Default Delivery Speed title is not found on the Account Settings Page.",log);
				}
				
				if(lnkFound==true)
				{
					if(BNBasicfeature.isElementPresent(editAddressButtons))
					{
						jsclick(editAddressButtons);
						Thread.sleep(100);
						wait.until(ExpectedConditions.and(
								ExpectedConditions.visibilityOf(header),
								ExpectedConditions.visibilityOf(myAccountPageTitle),
								ExpectedConditions.visibilityOf(defaultAddressForm)
							)
						);
						
						if(BNBasicfeature.isElementPresent(myAccountPageTitle))
						{
							Pass("The user is redirected to the Payments Page.");
							log.add("The Expected Title was : " + pgeTitle);
							String title = myAccountPageTitle.getText();
							log.add("The Actual Title is : " + title);
							if(pgeTitle.contains(title))
							{
								Pass("The Address Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
								BRM5443 = true;
							}
							else
							{
								Fail("The Address Page title does not match the expected content.",log);
							}
						}
						else
						{
							Fail("The user is not redirected to the Payments Page.");
						}
					}
					else
					{
						Fail( "The Payment page is not displayed.");
					}
				}
				else
				{
					Fail("The Manage Default Payment Link is not found.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2646 Issue ." + e.getMessage());
				Exception(" BRM - 2646 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the payments page.");
		}
		
		/* BRM - 5443 Verify on clicking the Shipping address section in the Manage Address Book page,Should redirect to Shipping Address Overlay and display the options to edit the Shipping address.*/
		ChildCreation("BRM - 5443 Verify on clicking the Shipping address section in the Manage Address Book page,Should redirect to Shipping Address Overlay and display the options to edit the Shipping address.");
		if(manageAccountLoaded==true)
		{
			if(BRM5443==true)
			{
				Pass("The Address Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
			}
			else
			{
				Fail("The Address Page title does not match the expected content.",log);
			}
		}
		else
		{
			Fail( "The User failed to load the Manage Accounts page.");
		}
	}
	
	/* BRM - 2647 Verify that when Manage Address Book page has a default address then it should display along with Edit and Remove link.*/
	public void myAccountDefaultAddressDetails()
	{
		ChildCreation(" BRM - 2647 Verify that when Manage Address Book page has a default address then it should display along with Edit and Remove link.");
		if(shipPageLoaded==true)
		{
			try
			{
				int i;
				String cellVal = BNBasicfeature.getExcelVal("BRMDefShipAddBtns", sheet, 5);
				String[] Val = cellVal.split("\n");
				boolean defaulShip = false;
				if(BNBasicfeature.isListElementPresent(addressBookAddressList))
				{
					Pass("The Shippment Address List is displayed.");
					for(i = 1 ;i <=addressBookAddressList.size(); i++)
					{
						String defShip = driver.findElement(By.xpath("(//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]")).getAttribute("data-itemdefault");
						if(defShip.contains("shipping"))
						{
							defaulShip = true;
							break;
						}
						else
						{
							defaulShip = false;
							continue;
						}
					}
					
					if(defaulShip==true)
					{
						Pass("There was a default shipping address set for the Address.");
						String fName = driver.findElement(By.xpath("((//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//span[@data-itemprop])[1]")).getText();
						String lName = driver.findElement(By.xpath("((//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//span[@data-itemprop])[2]")).getText();
						String stAdd = driver.findElement(By.xpath("((//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//span[@data-itemprop])[3]")).getText();
						log.add("First Name : " + fName);
						log.add("First Name : " + lName);
						log.add("First Name : " + stAdd);
						if((fName.isEmpty())&&(lName.isEmpty())&&(stAdd.isEmpty()))
						{
							Fail("The First Name, Last Name and Street Address is not displayed or empty.",log);
						}
						else
						{
							Pass("The First Name, Last Name and Street Address is displayed.",log);
						}
					}
					else
					{
						Fail("There was no default shipping address set for the Address.");
					}
					
					WebElement edit = driver.findElement(By.xpath("(//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//*[@data-itemeditor]"));
					WebElement remove = driver.findElement(By.xpath("(//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//*[@data-itemdelete]"));
					if(BNBasicfeature.isElementPresent(edit))
					{
						Pass("The Edit button is displayed for the default shipping.");
						String title = edit.getText();
						log.add("The expected value was : " + Val[0]);
						log.add("The actual value is : " + title);
						if(title.contains(Val[0]))
						{
							Pass("The value matches the expected content.",log);
						}
						else
						{
							Fail("The value does not matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Edit button is not displayed for the default shipping.");
					}
					
					if(BNBasicfeature.isElementPresent(remove))
					{
						Pass("The Remove button is displayed for the default shipping.");
						String title = remove.getText();
						log.add("The expected value was : " + Val[1]);
						log.add("The actual value is : " + title);
						if(Val[1].contains(title))
						{
							Pass("The value matches the expected content.",log);
						}
						else
						{
							Fail("The value does not matches the expected content.",log);
						}
					}
					else
					{
						Fail("The Remove button is not displayed for the default shipping.");
					}
				}
				else
				{
					Fail("The Shippment Address List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2647 Issue ." + e.getMessage());
				Exception(" BRM - 2647 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the address book page.");
		}
	}
	
	/* BRM - 2649 Verify that, when "Remove" link is clicked in "Manage Address Book" page then it should display "Remove Shipping Address" overlay.*/
	/* BRM - 2650 Verify that Remove this Payment Method overlay should display an alert message.*/
	/* BRM - 2652 Verify that when "Cancel" button is clicked then it should navigate back to the previous page*/
	public void myAccountDefaultAddressRemoveLink()
	{
		ChildCreation(" BRM - 2649 Verify that, when Remove link is clicked in Manage Address Book page then it should display Remove Shipping Address overlay.");
		if(shipPageLoaded==true)
		{
			try
			{
				int i;
				boolean defaulShip = false;
				String successAlert = BNBasicfeature.getExcelVal("BRM2649", sheet, 5);
				if(BNBasicfeature.isListElementPresent(addressBookAddressList))
				{
					Pass("The Shippment Address List is displayed.");
					for(i = 1 ;i <=addressBookAddressList.size(); i++)
					{
						String defShip = driver.findElement(By.xpath("(//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]")).getAttribute("data-itemdefault");
						if(defShip.contains("shipping"))
						{
							defaulShip = true;
							break;
						}
						else
						{
							defaulShip = false;
							continue;
						}
					}
					
					if(defaulShip==true)
					{
					
						WebElement remove = driver.findElement(By.xpath("(//*[@data-itemlist='PostalAddress']//*[@data-itemscope='PostalAddress'])["+i+"]//*[@data-itemdelete]"));
						if(BNBasicfeature.isElementPresent(remove))
						{
							jsclick(remove);
							Thread.sleep(100);
							wait.until(ExpectedConditions.and(
									ExpectedConditions.visibilityOf(addressDefaultAddressDelete),
									ExpectedConditions.visibilityOf(pgetitle)
									)
							);
							
							Thread.sleep(100);
							
							log.add("The Expected value was : " + successAlert);
							String actAlert = pgetitle.getText();
							log.add("The Actual value is : " + actAlert);
							if(successAlert.contains(actAlert))
							{
								Pass("The expected and actual alert matches.",log);
							}
							else
							{
								Fail("There is some mismatch in the expected and actual alert.",log);
							}
						}
						else
						{
							Fail("The Remove button is not displayed for the default shipping.");
						}
					}
					else
					{
						Fail("The Shippment Address List is not displayed.");
					}
				}
				else
				{
					Fail("The Default Address list is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2649 Issue ." + e.getMessage());
				Exception(" BRM - 2649 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the address book page.");
		}
		
		/* BRM - 2650 Verify that Remove this Payment Method overlay should display an alert message.*/
		ChildCreation(" BRM - 2650 Verify that Remove this Payment Method overlay should display an alert message.");
		if(shipPageLoaded==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(changeDefaultPayRemoveConfirmText))
				{
					Pass("The Payment Remove Confirm text is displayed.");
					String actAlert = changeDefaultPayRemoveConfirmText.getText();
					log.add("The Actual value is : " + actAlert);
					if(actAlert.isEmpty())
					{
						Fail("The alert is empty or not displayed.",log);
					}
					else
					{
						Pass("The actual alert is displayed.",log);
					}
				}
				else
				{
					Fail("The Payment Remove Confirm text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2650 Issue ." + e.getMessage());
				Exception(" BRM - 2650 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the address book page.");
		}
		
		/*BRM - 2652 Verify that when Cancel button is clicked then it should navigate back to the previous page.*/
		ChildCreation(" BRM - 2652 Verify that when Cancel button is clicked then it should navigate back to the previous page.");
		if(shipPageLoaded==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRM2652", sheet, 5);
				if(BNBasicfeature.isElementPresent(changeDefaultAddressRemoveCancelBtn))
				{
					Pass("The Cancel button in the Remove Address Page is displayed.");
					jsclick(changeDefaultAddressRemoveCancelBtn);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(header),
							ExpectedConditions.invisibilityOfAllElements(changeDefaultAddressRemoveCancelBtn1)
							)
					);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
					Thread.sleep(250);
					if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						Pass("The user is redirected to the Default Address Page.");
						log.add("The Expected Title was : " + pgeTitle);
						String title = myAccountPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(pgeTitle.contains(title))
						{
							Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
						}
						else
						{
							Fail("The Payment Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The user is not redirected to the Payments Page.");
					}
				}
				else
				{
					Fail("The Cancel button in the Remove Payments Page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2652 Issue ." + e.getMessage());
				Exception(" BRM - 2652 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the address book page.");
		}
	}
	
	/* BRM - 2653 Verify that on selecting Add a New Shipping Address button then it should display Add a New Shipping Address page.*/
	public void myAccountAddShippingAddressNavigation()
	{
		ChildCreation(" BRM - 2653 Verify that on selecting Add a New Shipping Address button then it should display Add a New Shipping Address page.");
		if(shipPageLoaded==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRM2653", sheet, 5);
				if(BNBasicfeature.isElementPresent(addressAddNewShipping))
				{
					Pass("The Add a New payment button is displayed.");
					jsclick(addressAddNewShipping);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(addressAddNewShippingPage),
							ExpectedConditions.visibilityOf(addPayAddShippingAddressTitle)
							)
					);
					addNewShip = true;
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(addPayAddShippingAddressTitle));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(addPayAddShippingAddressTitle))
					{
						Pass("The user is redirected to the Payments Page.");
						log.add("The Expected Title was : " + pgeTitle);
						String title = addPayAddShippingAddressTitle.getText();
						log.add("The Actual Title is : " + title);
						if(pgeTitle.contains(title))
						{
							Pass("The Payment Page title match the expected content and the user is navigated to the Manage Payments Page.",log);
						}
						else
						{
							Fail("The Payment Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The user is not redirected to the Payments Page.");
					}
				}
				else
				{
					Fail("The add a Shipping button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2653 Issue ." + e.getMessage());
				Exception(" BRM - 2653 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2654 Verify that "Add a New Shipping Address" page should has the following : "First Name", "Country" & so on...*/
	/* BRM - 2670 Verify that Set as a default Shipping Address and Address can't be serviced by UPS (P.O.Box, APO or FPO) checklist should be unticked in default.*/
	public void myAccountAddShippingDetails()
	{
		ChildCreation("BRM - 2654 Verify that Add a New Shipping Address page should has the following : First Name, Country & so on...");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					Pass("The Country Selection Drop Down is displayed.");
				}
				else
				{
					Fail("The Country Selection Drop Down is not displayed.");
				}
				if(BNBasicfeature.isElementPresent(fName))
				{
					Pass("The First Name text field is present and visible.");
				}
				else
				{
					Fail("The First Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(lName))
				{
					Pass("The Last Name text field is present and visible.");
				}
				else
				{
					Fail("The Last Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddress))
				{
					Pass("The Stree Address text field is present and visible.");
				}
				else
				{
					Fail("The Stree Address text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddress))
				{
					BNBasicfeature.scrolldown(aptSuiteAddress, driver);
					Pass("The Apt/Suite text field is present and visible.");
				}
				else
				{
					Fail("The Apt/Suite text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(city))
				{
					Pass("The City text field is present and visible.");
				}
				else
				{
					Fail("The City text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(state))
				{
					Pass("The State selection drop down is present and visible.");
				}
				else
				{
					Fail("The State selection drop down is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(zipCode))
				{
					BNBasicfeature.scrolldown(zipCode, driver);
					Pass("The Zipcode text field is present and visible.");
				}
				else
				{
					Fail("The Zipcode text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNo))
				{
					Pass("The Phone Number text field is present and visible.");
				}
				else
				{
					Fail("The Phone Number text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(companyName))
				{
					Pass("The Company Name text field is present and visible.");
				}
				else
				{
					Fail("The Company Name text field is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(addAddressSave))
				{
					Pass("The Save Button in the Add Shipping Address Page is displayed.");
				}
				else
				{
					Fail("The Save Button in the Add Shipping Address Page is not displayed..");
				}
				
				if(BNBasicfeature.isElementPresent(addAddressCancel))
				{
					Pass("The Cancel Button in the Add Shipping Address Page is displayed.");
				}
				else
				{
					Fail("The Cancel Button in the Add Shipping Address Page is not displayed..");
				}
				
				if(BNBasicfeature.isElementPresent(fNametxt))
				{
					if(fNametxt.getText().isEmpty())
					{
						Fail("The First Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The First Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The First Name text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(lNametxt))
				{
					if(lNametxt.getText().isEmpty())
					{
						Fail("The Last Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Last Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Last Name text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(stAddresstxt))
				{
					if(stAddresstxt.getText().isEmpty())
					{
						Fail("The Street Address text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Street Address text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Street Address text is missing or the value does not matches with the expected one. ");
				}
				
				if(BNBasicfeature.isElementPresent(aptSuiteAddresstxt))
				{
					if(aptSuiteAddresstxt.getText().isEmpty())
					{
						Fail("The Apt / Suite Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Apt / Suite Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Apt / Suite Name text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(citytxt))
				{
					if(citytxt.getText().isEmpty())
					{
						Fail("The City Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The City Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The City Name text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(statetxt))
				{
					if(statetxt.getText().isEmpty())
					{
						Fail("The State Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The State Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The State Name text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(zipcodetxt))
				{
					if(zipcodetxt.getText().isEmpty())
					{
						Fail("The Zip Code text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Zip Code text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Zip Code text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(contactNotxt))
				{
					if(contactNotxt.getText().isEmpty())
					{
						Fail("The Contact Number text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Contact Number text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Contact Number text is missing or the value does not matches with the expected one.");
				}
				
				if(BNBasicfeature.isElementPresent(companyNametxt))
				{
					if(companyNametxt.getText().isEmpty())
					{
						Fail("The Company Name text is missing or the value does not matches with the expected one.");
					}
					else
					{
						Pass("The Company Name text is present and the value matches with the expected one.");
					}
				}
				else
				{
					Fail("The Company Name text is missing or the value does not matches with the expected one.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2654 Issue ." + e.getMessage());
				Exception(" BRM - 2654 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
		
		/* BRM - 2670 Verify that Set as a default Shipping Address and Address can't be serviced by UPS (P.O.Box, APO or FPO) checklist should be unticked in default.*/
		ChildCreation(" BRM - 2670 Verify that Set as a default Shipping Address and Address can't be serviced by UPS (P.O.Box, APO or FPO) checklist should be unticked in default.");
		if(addNewPay==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(poBoxtxt1))
				{
					Pass("The PO Check Box field is present and visible.");
				}
				else
				{
					Fail("The PO Check Box is not present / visible.");
				}
				
				if(BNBasicfeature.isElementPresent(addPayDefaultShipping))
				{
					Pass("The Set as Default Shipping Address is present and visible.");
				}
				else
				{
					Fail("The Set as Default Shipping Address is not present / visible.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2670 Issue ." + e.getMessage());
				Exception(" BRM - 2670 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2655 Verify that when the "Save" button is clicked without filling all the fields in "Add a New Shipping Address" page then it should display the alert message.*/
	public void myAccountAddShippingEmptyValidation()
	{
		ChildCreation("BRM - 2655 Verify that when the Save button is clicked without filling all the fields in Add a New Shipping Address page then it should display the alert message.");
		if(addNewPay==true)
		{
			try
			{
				String expalert = BNBasicfeature.getExcelVal("BRM2655", sheet, 5);
				if(BNBasicfeature.isElementPresent(addAddressSave))
				{
					Pass("The Save Button in the Add Shipping Address Page is displayed.");
					jsclick(addAddressSave);
					Thread.sleep(100);
					wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
					Thread.sleep(100);
					String actAlert = addAddressSaveError.getText();
					log.add("The Expected alert was : " + expalert);
					log.add("The Actual alert is : " + actAlert);
					if(actAlert.contains(expalert))
					{
						Pass("The Expected and Actual Alert matches.",log);
					}
					else
					{
						Fail("The Expected and Actual Alert does not matches.",log);
					}
				}
				else
				{
					Fail("The Save Button in the Add Shipping Address Page is not displayed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2655 Issue ." + e.getMessage());
				Exception(" BRM - 2655 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2656 Verify that when the numbers or Special Characters are entered in "First Name", "Last Name" field then it should not display the alert message*/
	public void myAccountAddShipFNLNValidation()
	{
		ChildCreation("BRM - 2656 Verify that when the numbers or Special Characters are entered in First Name, Last Name field then it should not display the alert message.");
		if(addNewPay==true)
		{
			try
			{
				String expalert = BNBasicfeature.getExcelVal("BRM2656", sheet, 5);
				String fstName = BNBasicfeature.getExcelNumericVal("BRM2629", sheet, 15);
				String lstname = BNBasicfeature.getExcelVal("BRM2629", sheet, 16);
				BNBasicfeature.scrollup(countrySelection, driver);
				Thread.sleep(100);
				fName.clear();
				fName.sendKeys(fstName);
				lName.clear();
				lName.sendKeys(lstname);
				BNBasicfeature.scrolldown(city, driver);
				jsclick(addAddressSave);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
				Thread.sleep(100);
				String actAlert = addAddressSaveError.getText();
				log.add("The Actual alert is : " + actAlert);
				if(actAlert.contains(expalert))
				{
					Pass("The appropriate error is raised when user tries to save the Shipping Address invalid First Name and Last Name values .",log);
				}
				else
				{
					Fail("The appropriate error is raised when user tries to save the Shipping Address invalid First Name and Last Name values ,but there is mismatch in the alert.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2656 Issue ." + e.getMessage());
				Exception(" BRM - 2656 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2661 Verify that when characters are entered in "Phone Number" field it should thrown an alert message.*/
	public void myAccountAddShipPhoneNoValidation()
	{
		ChildCreation("BRM - 2661 Verify that when characters are entered in Phone Number field it should thrown an alert message.");
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2633", sheet, 5);
				String cntno = BNBasicfeature.getExcelVal("BRM2633", sheet, 21);
				String cty = BNBasicfeature.getExcelVal("BRM2633", sheet, 19);
				BNBasicfeature.scrolldown(lName,driver);
				city.clear();
				city.sendKeys(cty);
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(addAddressSave);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
				Thread.sleep(200);
				if(addAddressSaveError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddAddressSaveError.get(4).getText());
					Pass("The alert for the invalid Contact No field is raised and it matches the expected content.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddAddressSaveError.get(4).getText());
					Fail("The alert for the invalid Contact No field is raised and it does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2661 Issue ." + e.getMessage());
				Exception(" BRM - 2661 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2664 Verify that Phone Number field should accept only one special character in the beginning.*/
	public void myAccountAddShippingSpecialCharPhoneNo()
	{
		ChildCreation(" BRM - 2664 Verify that Phone Number field should accept only one special character in the beginning.");
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2634", sheet, 5);
				String cntno = BNBasicfeature.getExcelNumericVal("BRM2636", sheet, 21);
				BNBasicfeature.scrolldown(lName,driver);
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(addAddressSave);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
				Thread.sleep(200);
				if(addAddressSaveError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddAddressSaveError.get(4).getText());
					Fail(" The Special Character at the beginning of the Contact Number when used alert is raised.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					Pass("The Special Character at the beginning of the Contact Number when used no alert is raised.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2664 Issue ." + e.getMessage());
				Exception(" BRM - 2664 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2662 Verify that when "Phone Number field" has less than 10 digits the alert message should be displayed.*/
	public void myAccountAddShippingPhoneNoValidation1()
	{
		ChildCreation(" BRM - 2662 Verify that when Phone Number field has less than 10 digits the alert message should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String alert = BNBasicfeature.getExcelVal("BRM2662", sheet, 5);
				String cntno = BNBasicfeature.getExcelNumericVal("BRM2662", sheet, 21);
				BNBasicfeature.scrolldown(lName,driver);
				contactNo.clear();
				contactNo.sendKeys(cntno);
				jsclick(addAddressSave);
				Thread.sleep(100);
				wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
				Thread.sleep(200);
				if(addAddressSaveError.getText().contains(alert))
				{
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddAddressSaveError.get(5).getText());
					Pass("The alert for the invalid Contact No field is raised and it matches the expected content.",log);
				}
				else
				{	
					log.add("The entered Contact No value was " + contactNo.getAttribute("value").toString());
					log.add("The raised alert was " + individualAddAddressSaveError.get(5).getText());
					Fail("The alert for the invalid Contact No field is raised and it does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2662 Issue ." + e.getMessage());
				Exception(" BRM - 2662 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2667 Verify that when zip code contains characters or alphanumerics for "United States" then alert message should be thrown.*/
	public void myAccountAddShippingCntNoValidation()
	{
		ChildCreation("BRM - 2667 Verify that when zip code contains characters or alphanumerics for United States then alert message should be thrown.");
		if(addNewPay==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2667", sheet, 20);
				String cntry = BNBasicfeature.getExcelVal("BRM2630", sheet, 5);
				String expAlert = BNBasicfeature.getExcelVal("BRM2667", sheet, 5);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Thread.sleep(100);
					Select cntrysel = new Select(countrySelection);
					String selctedcntry = cntrysel.getFirstSelectedOption().getText();
					Thread.sleep(100);
					if(selctedcntry.equals(cntry))
					{
						if(BNBasicfeature.isElementPresent(stAddress))
						{
							BNBasicfeature.scrollup(stAddress,driver);
							Thread.sleep(100);
						}
						if(BNBasicfeature.isElementPresent(zipCode))
						{
							zipCode.clear();
							zipCode.sendKeys(cellVal);
							jsclick(addAddressSave);
							Thread.sleep(100);
							wait.until(ExpectedConditions.visibilityOf(addAddressSaveError));
							Thread.sleep(200);
							log.add("The Expected alert was  : " + expAlert);
							String actAlert = addAddressSaveError.getText();
							if(actAlert.contains(expAlert))
							{
								Pass("The Alert matches the expected content.",log);
							}
							else
							{
								Fail("The Alert doesn not matche the expected content.",log);
							}
						}
						else
						{
							Fail("The Zipcode text field is not displayed.");
						}
					}
					else
					{
						Skip("The Default Country is not United States.");
					}
				}
				else
				{
					Fail("The Country Drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2667 Issue ." + e.getMessage());
				Exception(" BRM - 2667 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2668 Verify that for Canada the zip code should contain alphanumerics,if only the numbers then the alert message should be displayed.*/
	public void myAccountShippingCanadaZipValidation()
	{
		ChildCreation(" BRM - 2668 Verify that for Canada the zip code should contain alphanumerics,if only the numbers then the alert message should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2640", sheet, 20);
				String[] zip = cellVal.split("\n");
				String alert = BNBasicfeature.getExcelVal("BRM2639", sheet, 5);
				
				String cntry = BNBasicfeature.getExcelVal("BRM2640", sheet, 6);
				if(BNBasicfeature.isElementPresent(countrySelection))
				{
					BNBasicfeature.scrollup(countrySelection,driver);
					Thread.sleep(100);
					Select cntrysel = new Select(countrySelection);
					cntrysel.selectByValue("CA");
					String selctedcntry = cntrysel.getFirstSelectedOption().getText();
					Thread.sleep(100);
					if(selctedcntry.equals(cntry))
					{
						for(int i = 0; i<zip.length;i++)
						{
							BNBasicfeature.scrolldown(fName,driver);
							zipCode.clear();
							zipCode.sendKeys(zip[i]);
							BNBasicfeature.scrolldown(addAddressSave,driver);
							jsclick(addAddressSave);
							wait.until(ExpectedConditions.visibilityOf(addAddressSave));
							Thread.sleep(100);
							boolean isNumeric = StringUtil.isNumeric(zip[i]);
							if(isNumeric==true)
							{
								if(addAddressSaveError.getText().contains(alert))
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + addAddressSaveError.getText());
									Pass("The alert for the invalid Zip Code field is raised and it matches the expected content.");
								}
								else
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + addAddressSaveError.getText());
									Fail("The alert for the invalid Zip Code field is raised and it does not match the expected content.",log);
								}
							}
							else
							{
								if(addAddressSaveError.getText().contains(alert))
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									log.add("The raised alert was " + addAddressSaveError.getText());
									Fail("The alert for the valid Zip Code field is raised and it should not have been raised.",log);
								}
								else
								{
									log.add("The entered Zip Code value was " + zipCode.getAttribute("value").toString());
									Pass("No alert for the valid Zip Code field is raised.",log);
								}
							}
						}
					}
					else
					{
						Skip("The Selected Default Country is not UNITED STATES. ");
					}
				}
				else
				{
					Fail(" The Country Selection drop down is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2668 Issue ." + e.getMessage());
				Exception(" BRM - 2668 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2673 Verify that when "Cancel" button is clicked in "Add a New Shipping Address" page then the entries in all the fields should be cleared and previous page should be displayed.*/
	public void myAccountAddShippingCancelNavigation()
	{
		ChildCreation(" BRM - 2673 Verify that when Cancel button is clicked in Add a New Shipping Address page then the entries in all the fields should be cleared and previous page should be displayed.");
		if(addNewPay==true)
		{
			try
			{
				String pgeTitle = BNBasicfeature.getExcelVal("BRM2652", sheet, 5);
				if(BNBasicfeature.isElementPresent(addAddressCancel))
				{
					Pass("The Cancel button in the Remove Payments Page is displayed.");
					jsclick(addAddressCancel);
					wait.until(ExpectedConditions.and(
							ExpectedConditions.visibilityOf(myAccountPageTitle),
							ExpectedConditions.invisibilityOfAllElements(individualAddAddressSaveError)
							)
					);
					wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
					Thread.sleep(100);
					if(BNBasicfeature.isElementPresent(myAccountPageTitle))
					{
						addNewPay = false;
						Pass("The user is redirected to the Default Address Page.");
						log.add("The Expected Title was : " + pgeTitle);
						String title = myAccountPageTitle.getText();
						log.add("The Actual Title is : " + title);
						if(pgeTitle.contains(title))
						{
							Pass("The Payment Page title match the expected content and the user is navigated to the Manage Address Page.",log);
						}
						else
						{
							Fail("The Payment Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The user is not redirected to the Payments Page.");
					}
				}
				else
				{
					Fail("The Cancel button in the Remove Payments Page is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2673 Issue ." + e.getMessage());
				Exception(" BRM - 2673 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2648 Verify that, when "Edit" link is clicked in "Manage Address Book" page then it should display "Edit Shipping Address" overlay.*/
	public void myAccountPaymentPageEditShipping()
	{
		ChildCreation("BRM - 2648 Verify that, when Edit link is clicked in Manage Address Book page then it should display Edit Shipping Address overlay.");
		if(addNewPay==false)
		{
			try
			{
				int i;
				String editfirstName="",editlastName="",fName="",lName="",stAdd="",editStAddress="";
				String cellVal = BNBasicfeature.getExcelVal("BRMAddressEditPage", sheet, 5);
				String[] Val = cellVal.split("\n");
				boolean defaulShip = false;
				if(BNBasicfeature.isListElementPresent(addressBookAddressList))
				{
					Pass("The Shippment Address List is displayed.");
					for(i = 1 ;i <addressBookAddressList.size(); i++)
					{
						String defShip = driver.findElement(By.xpath("(//*[@data-itemscope='PostalAddress'])["+i+"]")).getAttribute("data-itemdefault");
						if(defShip.contains("shipping"))
						{
							defaulShip = true;
							break;
						}
						else
						{
							defaulShip = false;
							continue;
						}
					}
					
					if(defaulShip==true)
					{
						Pass("There was a default shipping address set for the Address.");
						fName = driver.findElement(By.xpath("((//*[@data-itemscope='PostalAddress'])["+i+"]//span[@data-itemprop])[1]")).getText();
						lName = driver.findElement(By.xpath("((//*[@data-itemscope='PostalAddress'])["+i+"]//span[@data-itemprop])[2]")).getText();
						stAdd = driver.findElement(By.xpath("((//*[@data-itemscope='PostalAddress'])["+i+"]//p[@data-itemprop])[1]")).getText();
						log.add("First Name : " + fName);
						log.add("First Name : " + lName);
						log.add("First Name : " + stAdd);
						if((fName.isEmpty())&&(lName.isEmpty())&&(stAdd.isEmpty()))
						{
							Fail("The First Name, Last Name and Street Address is not displayed or empty.",log);
						}
						else
						{
							Pass("The First Name, Last Name and Street Address is displayed.",log);
							WebElement edit = driver.findElement(By.xpath("(//*[@data-itemscope='PostalAddress'])["+i+"]//*[@data-itemeditor]"));
							if(BNBasicfeature.isElementPresent(edit))
							{
								Pass("The Edit button is displayed for the default shipping.");
								jsclick(edit);
								Thread.sleep(100);
								wait.until(ExpectedConditions.visibilityOf(editShipPageTitle));
								if(BNBasicfeature.isElementPresent(editShipPageTitle))
								{
									Thread.sleep(250);
									log.add("The Expected title was : " + editShipPageTitle.getText());
									Pass("The user is redirected to the Edit Shipping address Page.");
									String title = editShipPageTitle.getText();
									log.add("The Actual title is : " + title);
									if(Val[0].equals(title))
									{
										Pass("The title matches the expected content.",log);
									}
									else
									{
										Fail("The title does not matches the expected content.",log);
									}
									
									if(BNBasicfeature.isElementPresent(editfName))
									{
										Pass("The First Name field in the edit page is displayed.");
										if(editfName.getAttribute("value").isEmpty())
										{
											Fail("The First Name field is empty.");
										}
										else
										{
											editfirstName = editfName.getAttribute("value").toString();
											log.add("The Expected First Name was : " + fName);
											log.add("The Actual First Name is : " + editfirstName);
											if(fName.equals(editfirstName))
											{
												Pass("The First Name in the edit payment page matches with the expected one.",log);
											}
											else
											{
												Fail("There is some mismatch in the expected and actual value.",log);
											}
										}
									}
									else
									{
										Fail("The First Name field in the edit page is not displayed.");
									}
									
									if(BNBasicfeature.isElementPresent(editlName))
									{
										Pass("The Last Name field in the edit page is displayed.");
										if(editlName.getAttribute("value").isEmpty())
										{
											Fail("The Last Name field is empty.");
										}
										else
										{
											editlastName = editlName.getAttribute("value").toString();
											log.add("The Expected Last Name was : " + lName);
											log.add("The Actual Last Name is : " + editlastName);
											if(lName.equals(editlastName))
											{
												Pass("The last name in the edit payment page matches with the expected one.",log);
											}
											else
											{
												Fail("There is some mismatch in the expected and actual value.",log);
											}
										}
									}
									else
									{
										Fail("The Last Name field in the edit page is not displayed.");
									}
									
									if(BNBasicfeature.isElementPresent(editstAddress))
									{
										Pass("The Stree address field in the edit page is displayed.");
										if(editstAddress.getAttribute("value").isEmpty())
										{
											Fail("The Stree Address field is empty.");
										}
										else
										{
											editStAddress = editstAddress.getAttribute("value").toString();
											log.add("The Expected Street Address was : " + stAdd);
											log.add("The Actual Street Address is : " + editStAddress);
											if(stAdd.equals(editStAddress))
											{
												Pass("The Street Address in the edit payment page matches with the expected one.",log);
											}
											else
											{
												Fail("There is some mismatch in the expected and actual value.",log);
											}
										}
									}
									else
									{
										Fail("The Street Address field in the edit page is not displayed.");
									}
								}
								else
								{
									Fail("The user is not redirected to the Edit Shipping address.");
								}
							}
							else
							{
								Fail("The Edit button is not displayed for the default shipping.");
							}
						}
					}
					else
					{
						Fail("There was no default shipping address set for the Address.");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2648 Issue ." + e.getMessage());
				Exception(" BRM - 2648 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the new address book page.");
		}
	}
	
	/* BRM - 2701 Verify that expanded "payment" option should shown certain links and should match with the creatives.*/
	public void myAccPaymentExpandDet(String tc)
	{
		String tcId = "";
		/*signedIn = true;
		myAccountUrl = driver.getCurrentUrl();*/
		if(tc=="BRM-2701")
		{
			tcId = "BRM - 2701 Verify that expanded Payment option should shown certain links and should match with the creatives.";
		}
		else
		{
			tcId = "BRM - 2702 Verify that Default Payment, Payment Methods, Gift Cards texts should be shown with the seperation line.";
		}
		ChildCreation(tcId);
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM2701", sheet, 2);
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						Pass( " The Account modules are displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
							}
						}
						else
						{
							Fail( "The My Account Form Container Title List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - " + tcId +" Issue ." + e.getMessage());
				Exception(" BRM - " + tcId + " Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to sign in into the My Accounts Page.");
		}
	}
	
	/* BRM - 2703 Verify that below "Default Payment" text, the saved Card Number(Eg:XXXXX586) & the Expiration Date should be shown.*/
	public void myAccountPaymentCCDetails()
	{
		ChildCreation("BRM - 2703 Verify that below Default Payment text, the saved Card Number(Eg:XXXXX586) & the Expiration Date should be shown.");
		if(signedIn==true)
		{
			try
			{
				String expTitle = BNBasicfeature.getExcelVal("BRM2701", sheet, 2);
				if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
				{
					Pass( " The Account modules are displayed.");
					if(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle)&&(BNBasicfeature.isListElementPresent(myAccFormAccModulesTitle1)))
					{
						boolean titleFound = getLandingPageFormValue(expTitle);
						log.add( "The Expected title was : " + expTitle);
						log.add("The actual title is : " + actTitle);
						if(titleFound==false)
						{
							Fail( "The Expected title and the actual title does not matches.",log);
						}
						else
						{
							Pass( "The Expected title and the actual title matches.",log);
							if((BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText))||(BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText1)))
							{
								boolean txtFound = false;
								
								if(BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText))
								{
									for(int i = 0 ; i<myAccountPaymentDetailsText.size(); i++)
									{
										String txt = myAccountPaymentDetailsText.get(i).getText();
										if(txt.isEmpty())
										{
											Fail( "The Payments Details is empty.");
											txtFound = false;
										}
										else
										{
											Pass( "The Payments Details is not empty.The displayed text is  : " + txt);
											txtFound = true;
										}
									}
								}
								
								if(txtFound==false)
								{
									if(BNBasicfeature.isListElementPresent(myAccountPaymentDetailsText1))
									{
										for(int i = 0 ; i<myAccountPaymentDetailsText1.size(); i++)
										{
											String txt = myAccountPaymentDetailsText1.get(i).getText();
											if(txt.isEmpty())
											{
												Fail( "The Payments Details is empty.");
												txtFound = false;
											}
											else
											{
												Pass( "The Payments Details is not empty.The displayed text is  : " + txt);
											}
										}
									}
								}
							}
							else
							{
								Fail( "The Payment text is not displayed.");
							}
						}
					}
					else
					{
						Fail( "The My Account Form Container Title List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container List is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2648 Issue ." + e.getMessage());
				Exception(" BRM - 2648 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 2708 Verify that when "Manage Gift Cards & Credits for Nook Content" link is clicked, then it should navigate to respective page.*/
	public void myAccountNavigatetoGiftCardsNav()
	{
		ChildCreation("BRM - 2708 Verify that when Manage Gift Cards & Credits for Nook Content link is clicked, then it should navigate to respective page.");
		if(signedIn==true)
		{
			try
			{
				String expTitle = BNBasicfeature.getExcelVal("BRM5432", sheet, 3);
				String cellVal1 = BNBasicfeature.getExcelVal("BRMPaymentsContainerGiftCardsLinkDetails", sheet, 5);
				String[] Val1 = cellVal1.split("\n");
				if(BNBasicfeature.isElementPresent(myAccFormContainer))
				{
					Pass( "The My Account Form Container is displayed.");
					if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
					{
						boolean titleFound = getLandingPageFormValue(expTitle);
						log.add( "The Expected title was : " + expTitle);
						log.add("The actual title is : " + actTitle);
						if(titleFound==false)
						{
							Fail( "The Expected title and the actual title does not matches.",log);
						}
						else
						{
							Pass( "The Expected title and the actual title matches.",log);
							jsclick(accElement);
							wait.until(ExpectedConditions.or(
									ExpectedConditions.visibilityOf(header),
									ExpectedConditions.visibilityOfAllElements(myAccountAccountSettingsPageSubTitle),
									ExpectedConditions.visibilityOf(giftCardTitle)
								)
							);
							
							if(BNBasicfeature.isElementPresent(giftCardTitle))
							{
								Pass("The user is redirected to the Add A New Gift Card page.");
								log.add("The Expected Title was : " + Val1[0]);
								String title = giftCardTitle.getText();
								log.add("The Actual Title is : " + title);
								if(Val1[0].contains(title))
								{
									Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
									gftCardPage = true;
								}
								else
								{
									Fail("The Page title does not match the expected content.",log);
								}
							}
							else if(BNBasicfeature.isListElementPresent(myAccountAccountSettingsPageSubTitle))
							{
								Pass("The user is redirected to the Add A New Gift Card page.");
								log.add("The Expected Title was : " + Val1[0]);
								for(int i = 0; i<myAccountAccountSettingsPageSubTitle.size(); i++)
								{
									String title = giftCardTitle.getText();
									log.add("The Actual Title is : " + title);
									if(Val1[0].contains(title))
									{
										Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
										gftCardPage = true;
									}
									else
									{
										Fail("The Page title does not match the expected content.",log);
									}	
								}
							}
							else
							{
								Fail( "The User is not navigated to the Gift Card page.");
							}
						}
					}
					else
					{
						Fail( "The My Account Form Container List is not displayed.");
					}
				}
				else
				{
					Fail( "The My Account Form Container is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2708 Issue ." + e.getMessage());
				Exception(" BRM - 2708 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to sign in.");
		}
	}
	
	/* BRM - 2721 Verify that, "Add a New Gift Card" page should have certain textboxes and links.*/
	/* BRM - 2724 Verify that, below PIN* textbox, The PIN is optional for some cards text should be shown.*/
	/* BRM - 2725 Verify that, Add Gift Card button should be highlighted in green color.*/
	public void myAccAddGftCardDet()
	{
		ChildCreation("BRM - 2721 Verify that, Add a New Gift Card page should have certain textboxes and links.");
		if(gftCardPage==true)
		{
			try
			{
				if(gftCardPage==true)
				{
					if(BNBasicfeature.isElementPresent(addgiftCardNumber))
					{
						Pass("The Add a Gift Card Number field is displayed.");
					}
					else
					{
						Fail("The Add a Gift Card Number field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(addgiftCardPinNumber))
					{
						Pass("The Add a Gift Card PIN Number field is displayed.");
					}
					else
					{
						Fail("The Add a Gift Card PIN Number field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(addCardSubmitBtn))
					{
						Pass("The Add a Gift Card Submit button is displayed.");
					}
					else
					{
						Fail("The Add a Gift Card Submit button is not displayed.");
					}
				}
				else
				{
					Fail("The user is not redirected to the Add a gift Card Page Page.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2721 Issue ." + e.getMessage());
				Exception(" BRM - 2721 Issue." + e.getMessage());
			}
		}
		else
		{
			Fail( "The User failed to load the Add Gift Card Page .");
		}
		
		/* BRM - 2724 Verify that, below PIN* textbox, The PIN is optional for some cards text should be shown.*/
		ChildCreation("BRM - 2724 Verify that, below PIN* textbox, The PIN is optional for some cards text should be shown.");
		if(gftCardPage==true)
		{
			try
			{
				//String cellVal = BNBasicfeature.getExcelVal("BRM-2724", sheet, 5);
				if(BNBasicfeature.isElementPresent(addCardPinOptionalText))
				{
					Pass("The Add a Gift Card PIN Number Optional text is displayed.");
					if(addCardPinOptionalText.getText().isEmpty())
					{
						Fail("The optional text is empty or not displayed.");
					}
					else
					{
						Pass("The optional text is displayed.The displayed text is : " + addCardPinOptionalText.getText());
					}
				}
				else
				{
					Fail("The Add a Gift Card PIN Number Optional text is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2721 Issue ." + e.getMessage());
				Exception(" BRM - 2721 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
		
		/*BRM - 2725 Verify that, Add Gift Card button should be highlighted in green color.*/
		ChildCreation(" BRM - 2725 Verify that, Add Gift Card button should be highlighted in green color.");
		if(gftCardPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2717", sheet, 6);
				if(BNBasicfeature.isElementPresent(addCardSubmitBtn))
				{
					Pass("The Add a Gift Card Submit button is displayed.");
					String btnColor = addCardSubmitBtn.getCssValue("background-color");
					Color colcnvt = Color.fromString(btnColor);
					String trkBtncolor = colcnvt.asHex();
					log.add("The Expected Color was : " + expColor);
					log.add("The Actual Color is  : " + trkBtncolor);
					if(expColor.contains(trkBtncolor))
					{
						Pass("The Button Color match the expected content.",log);
					}
					else
					{
						Fail("The Button Color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Add a Gift Card Submit button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2725 Issue ." + e.getMessage());
				Exception(" BRM - 2725 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2722 Verify that, "Gift Card Number" textbox should accepts only numbers*/
	public void myAccInvalidGftCard()
	{
		ChildCreation("BRM - 2722 Verify that, Gift Card Number textbox should accepts only numbers.");
		if(gftCardPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM2722", sheet, 4);
				String cardVal = BNBasicfeature.getExcelVal("BRM2722", sheet, 9);
				if(BNBasicfeature.isElementPresent(addgiftCardNumber))
				{
					addgiftCardNumber.clear();
					//addgiftCardNumber.click();
					addgiftCardNumber.sendKeys(cardVal);
					addgiftCardPinNumber.clear();
					if(BNBasicfeature.isElementPresent(addCardSubmitBtn))
					{
						Pass("The Submit button in the Gift Card page is displayed.");
						jsclick(addCardSubmitBtn);
						Thread.sleep(100);
						wait.until(ExpectedConditions.attributeContains(addCardError, "aria-atomic", "true"));
						Thread.sleep(100);
						String actalert = addCardError.getText();
						log.add("The Expected alert was : " + error);
						if(error.contains(actalert))
						{
							Pass("The raised alert match the expected content.",log);
						}
						else
						{
							Fail("The raised alert does not match the expected content.",log);
						}
					}
					else
					{
						Fail("The Submit button in the Gift Card page is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Card Number is not displayed.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2722  Issue ." + e.getMessage());
				Exception(" BRM - 2722 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2723 Verify that, "PIN*" textbox should accepts maximum of 4 digits.*/
	public void myAccAddGiftPinLimit()
	{
		ChildCreation(" BRM - 2723 Verify that, PIN textbox should accepts maximum of 4 digits.");
		if(gftCardPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2712", sheet, 10);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(addgiftCardPinNumber))
				{
					Pass("The Gift Card Number field is displayed.");
					for(int i = 0; i<Val.length;i++)
					{
						addgiftCardPinNumber.clear();
						addgiftCardPinNumber.sendKeys(Val[i]);
						int len = addgiftCardPinNumber.getAttribute("value").length();
						log.add("The Sent Character from the file was : " + Val[i] + " and its length is : " + Val[i].length());
						log.add("The Current Character from the text field is : " + addgiftCardPinNumber.getAttribute("value") + " and its length is : " + len);
						if(len<=4)
						{
							Pass("The Add a Gift Card PIN Number field accepts character less than 4.",log);
						}
						else
						{
							Fail("The Add a Gift Card PIN Number field accepts character more than 4.",log);
						}
					}
				}
				else
				{
					Fail("The Gift Card Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2723  Issue ." + e.getMessage());
				Exception(" BRM - 2723 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2709 Verify that when "Check Balance" link is clicked, then it should navigate to "Check Your Balance" page.*/
	public void myAccChkBalNavigation()
	{
		ChildCreation("BRM - 2709 Verify that when Check Balance link is clicked, then it should navigate to Check Your Balance page.");
		if(gftCardPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelVal("BRMPaymentsContainerGiftCardsDetails", sheet, 4);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(giftCardBalanceSection))
				{
					BNBasicfeature.scrolldown(giftCardBalanceSection, driver);
					if(BNBasicfeature.isElementPresent(giftCardBalanceTitle))
					{
						Pass( "The Gift Card Balance is displaayed.");
						log.add("The Expected Title was : " + Val[0]);
						String title = giftCardBalanceTitle.getText();
						log.add("The Actual Title is : " + title);
						if(title.contains(Val[0]))
						{
							Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
						}
						else
						{
							Fail("The Page title does not match the expected content.",log);
						}
					}
					else
					{
						Fail( "The Gift Card Section title is not displayed.");	
					}
				}
				else
				{
					Fail( "The Gift Card Section is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2709  Issue ." + e.getMessage());
				Exception(" BRM - 2709 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2710 Verify that, "Check Your Balance" page should have the certain textboxs and should match with the classic site.*/
	/* BRM - 2717 Verify that, Check Balance button should be highlighted in green color */
	public void myAccCheckBalDet()
	{
		ChildCreation(" BRM - 2710 Verify that, Check Your Balance page should have the certain textboxs and should match with the classic site.");
		if(gftCardPage==true)
		{
			try
			{
				if(BNBasicfeature.isElementPresent(giftCardBalanceSection))
				{
					Pass("The page title match the expected content and the user is navigated from the Manage Payments Page.",log);
					if(BNBasicfeature.isElementPresent(giftCardNumberField))
					{
						Pass("The Gift Card Number field is displayed.");
					}
					else
					{
						Fail("The Gift Card Number field is not displayed.");
					}
					
					if(BNBasicfeature.isElementPresent(giftCardPinField))
					{
						Pass("The Gift Card PIN field is displayed.");
					}
					else
					{
						Fail("The Gift Card PIN field is not displayed.");
					}
				}
				else
				{
					Fail("The Page title does not match the expected content.",log);
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2710  Issue ." + e.getMessage());
				Exception(" BRM - 2710 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
		
		/* BRM - 2717 Verify that, Check Balance button should be highlighted in green color. */
		ChildCreation(" BRM - 2717 Verify that, Check Balance button should be highlighted in green color.");
		if(gftCardPage==true)
		{
			try
			{
				String expColor = BNBasicfeature.getExcelVal("BRM2717", sheet, 6);
				if(BNBasicfeature.isElementPresent(giftCardSubmitBtn))
				{
					Pass("The Gift Card Submit button is displayed.");
					String btnColor = giftCardSubmitBtn.getCssValue("background-color");
					Color colcnvt = Color.fromString(btnColor);
					String trkBtncolor = colcnvt.asHex();
					log.add("The Expected Color was : " + expColor);
					log.add("The Actual Color is  : " + trkBtncolor);
					if(expColor.contains(trkBtncolor))
					{
						Pass("The Button Color match the expected content.",log);
					}
					else
					{
						Fail("The Button Color does not match the expected content.",log);
					}
				}
				else
				{
					Fail("The Gift Card Submit button is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2717  Issue ." + e.getMessage());
				Exception(" BRM - 2717 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2713 Verify that, when the "Gift Card" field left empty or entered invalid number then alert message should be thrown. */
	public void myAccountChkBalEmptyVal()
	{
		ChildCreation("BRM - 2713 Verify that, when the Gift Card field left empty or entered invalid number then alert message should be thrown.");
		if(gftCardPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM2713", sheet, 4);
				if(BNBasicfeature.isElementPresent(giftCardBalanceSection))
				{
					Pass("The user is redirected to the Add New Payments Page.");
					if((BNBasicfeature.isElementPresent(giftCardNumberField))&&(BNBasicfeature.isElementPresent(giftCardPinField)))
					{
						giftCardNumberField.clear();
						giftCardPinField.clear();
						if(BNBasicfeature.isElementPresent(giftCardSubmitBtn))
						{
							Pass("The Submit button in the Gift Card page is displayed.");
							jsclick(giftCardSubmitBtn);
							wait.until(ExpectedConditions.attributeContains(giftCardError, "aria-atomic", "true"));
							Thread.sleep(250);
							String actalert = giftCardError.getText();
							log.add("The Expected alert was : " + error);
							if(error.contains(actalert))
							{
								Pass("The raised alert match the expected content.",log);
							}
							else
							{
								Fail("The raised alert does not match the expected content.",log);
							}
						}
						else
						{
							Fail("The Submit button in the Gift Card page is not displayed.");
						}
					}
					else
					{
						Fail( "The Gift Card Number or Pin field is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Card Check Balance Section is not dispalyed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2713  Issue ." + e.getMessage());
				Exception(" BRM - 2713 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2714 Verify that, when the "PIN (4 Digit - if available)" field is left empty or entered invalid number then alert message should be thrown.*/
	public void myAccountChkBalEmptyPinVal()
	{
		ChildCreation("BRM - 2714 Verify that, when the PIN (4 Digit - if available) field is left empty or entered invalid number then alert message should be thrown.");
		if(gftCardPage==true)
		{
			try
			{
				String error = BNBasicfeature.getExcelVal("BRM2713", sheet, 4);
				String giftCardnum = BNBasicfeature.getExcelNumericVal("BRM2714", sheet, 9);
				if(BNBasicfeature.isElementPresent(giftCardBalanceSection))
				{
					Pass("The user is redirected to the Add New Payments Page.");
					if((BNBasicfeature.isElementPresent(giftCardNumberField))&&(BNBasicfeature.isElementPresent(giftCardPinField)))
					{
						giftCardNumberField.clear();
						giftCardNumberField.sendKeys(giftCardnum);
						giftCardPinField.clear();
						if(BNBasicfeature.isElementPresent(giftCardSubmitBtn))
						{
							Pass("The Submit button in the Gift Card page is displayed.");
							jsclick(giftCardSubmitBtn);
							wait.until(ExpectedConditions.attributeContains(giftCardError, "aria-atomic", "true"));
							Thread.sleep(100);
							String actalert = giftCardError.getText();
							log.add("The Expected alert was : " + error);
							if(error.contains(actalert))
							{
								Pass("The raised alert match the expected content.",log);
							}
							else
							{
								Fail("The raised alert does not match the expected content.",log);
							}
						}
						else
						{
							Fail("The Submit button in the Gift Card page is not displayed.");
						}
					}
					else
					{
						Fail( "The Gift Card Number or Pin field is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Card Check Balance Section is not dispalyed..");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2714  Issue ." + e.getMessage());
				Exception(" BRM - 2714 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2711 Verify that "Gift Card or Validation Number" textbox should accept maximum of 19 characters.*/
	public void myAccGiftCardLimit()
	{
		ChildCreation("BRM - 2711 Verify that Gift Card or Validation Number textbox should accept maximum of 19 characters.");
		if(gftCardPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2711", sheet, 9);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(giftCardNumberField))
				{
					Pass("The Gift Card Number field is displayed.");
					if((BNBasicfeature.isElementPresent(giftCardNumberField))&&(BNBasicfeature.isElementPresent(giftCardPinField)))
					{
						for(int i = 0; i<Val.length;i++)
						{
							giftCardNumberField.clear();
							giftCardNumberField.sendKeys(Val[i]);
							int len = giftCardNumberField.getAttribute("value").length();
							log.add("The Sent Character from the file was : " + Val[i] + " and its length is : " + Val[i].length());
							log.add("The Current Character from the text field is : " + giftCardNumberField.getAttribute("value") + " and its length is : " + len);
							if(len<=19)
							{
								Pass("The Gift Card Number field accepts character less than 19.",log);
							}
							else
							{
								Fail("The Gift Card Number field accepts character more than 19.",log);
							}
						}
					}
					else
					{
						Fail( "The Gift Card Number or Pin field is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Card Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2711  Issue ." + e.getMessage());
				Exception(" BRM - 2711 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 2712 Verify that "PIN (4 Digit - if available)" textbox should accept maximum of 4 characters.*/
	public void myAccGiftPinLimit()
	{
		ChildCreation(" BRM - 2712 Verify that PIN (4 Digit - if available) textbox should accept maximum of 4 characters.");
		if(gftCardPage==true)
		{
			try
			{
				String cellVal = BNBasicfeature.getExcelNumericVal("BRM2712", sheet, 10);
				String[] Val = cellVal.split("\n");
				if(BNBasicfeature.isElementPresent(giftCardPinField))
				{
					Pass("The Gift Card Number field is displayed.");
					if((BNBasicfeature.isElementPresent(giftCardNumberField))&&(BNBasicfeature.isElementPresent(giftCardPinField)))
					{
						for(int i = 0; i<Val.length;i++)
						{
							giftCardPinField.clear();
							giftCardPinField.sendKeys(Val[i]);
							int len = giftCardPinField.getAttribute("value").length();
							log.add("The Sent Character from the file was : " + Val[i] + " and its length is : " + Val[i].length());
							log.add("The Current Character from the text field is : " + giftCardPinField.getAttribute("value") + " and its length is : " + len);
							if(len<=4)
							{
								Pass("The Gift Card PIN Number field accepts character less than 4.",log);
							}
							else
							{
								Fail("The Gift Card PIN Number field accepts character more than 4.",log);
							}
						}
					}
					else
					{
						Fail("The Gift Card Number field is not displayed.");
					}
				}
				else
				{
					Fail("The Gift Card Number field is not displayed.");
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 2712  Issue ." + e.getMessage());
				Exception(" BRM - 2712 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 5792 Verify whether header and footer links are navigating properly from My Account landing page.*/
	public void myAccountLandinFooterLinksNav()
	{
		ChildCreation("BRM - 5792 Verify whether header and footer links are navigating properly from My Account landing page.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String currUrl = driver.getCurrentUrl();
					String cellVal = BNBasicfeature.getExcelVal("BRM5407", sheet, 2);
					String[] Val = cellVal.split("\n");
					String[] pgeurl = BNBasicfeature.getExcelVal("BRM5792", sheet, 3).split("\n");
					String navUrl = "";
					if(BNBasicfeature.isElementPresent(myAccountFooterContainer))
					{
						BNBasicfeature.scrolldown(myAccountFooterContainer, driver);
						if(BNBasicfeature.isElementPresent(myAccountFooterMyAccount))
						{
							Pass("The My Account Link is displayed.");
							String actVal = myAccountFooterMyAccount.getText();
							log.add("The Expected value is : " + Val[2]);
							log.add("The Actual value is : " + actVal);
							if(Val[2].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The My Account Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionSignIn))
						{
							Pass("The Sign In Link is displayed.");
							String actVal = footerSectionSignIn.getText();
							log.add("The Expected value is : " + "Sign Out");
							log.add("The Actual value is : " + actVal);
							if("Sign Out".contains(actVal))
							{
								Pass("The Expected Value matches.",log);
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Sign In Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionCustomerService))
						{
							Pass("The Customer Service Link is displayed.");
							String actVal = footerSectionCustomerService.getText();
							log.add("The Expected value is : " + Val[4]);
							log.add("The Actual value is : " + actVal);
							if(Val[4].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
								Actions act = new Actions(driver);
								act.moveToElement(footerSectionCustomerService).click().build().perform();
								//jsclick(customerService);
								Thread.sleep(5000);
								navUrl = driver.getCurrentUrl();
								if(navUrl.contains(pgeurl[0].toString()))
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Customer Service Link was : " + pgeurl[0].toString());
									Pass("The user is  navigated to the Customer Service Page.",log);
								}
								else
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Customer Service Link was : " + pgeurl[0].toString());
									Fail("The user is not navigated to the Customer Service Page. Please Check.",log);
								}
								navUrl = driver.getCurrentUrl();
								
								if(!currUrl.equals(navUrl))
								{
									driver.navigate().back();
									wait.until(ExpectedConditions.visibilityOf(header));
									wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
								}
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Customer Service Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionStores))
						{
							Pass("The Stores Link is displayed.");
							String actVal = footerSectionStores.getText();
							log.add("The Expected value is : " + Val[5]);
							log.add("The Actual value is : " + actVal);
							if(Val[5].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Store Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionTermsandUse))
						{
							Pass("The Terms of Use Link is displayed.");
							String actVal = footerSectionTermsandUse.getText();
							log.add("The Expected value is : " + Val[6]);
							log.add("The Actual value is : " + actVal);
							if(Val[6].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
								jsclick(footerSectionTermsandUse);
								Thread.sleep(1000);
								navUrl = driver.getCurrentUrl();
								if(navUrl.contains(pgeurl[1].toString()))
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Terms of Use Page was : " + pgeurl[1].toString());
									Pass("The user is navigated to the Terms of Use Page.",log);
								}
								else
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Terms of Use page was : " + pgeurl[1].toString());
									Fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
								}
								
								if(!currUrl.equals(navUrl))
								{
									driver.navigate().back();
									wait.until(ExpectedConditions.visibilityOf(header));
									wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
								}
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Terms and Use Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionPrivacy))
						{
							Pass("The Privacy Policy Link is displayed.");
							String actVal = footerSectionPrivacy.getText();
							log.add("The Expected value is : " + Val[8]);
							log.add("The Actual value is : " + actVal);
							if(Val[8].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
								jsclick(footerSectionPrivacy);
								Thread.sleep(1000);
								navUrl = driver.getCurrentUrl();
								if(navUrl.contains(pgeurl[3].toString()))
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Privacy Page was : " + pgeurl[3].toString());
									Pass("The user is navigated to the Terms of Use Page.",log);
								}
								else
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Privacy page was : " + pgeurl[3].toString());
									Fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
								}
								
								if(!currUrl.equals(navUrl))
								{
									driver.navigate().back();
									wait.until(ExpectedConditions.visibilityOf(header));
									wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
								}
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Privacy Policy Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionCopyright))
						{
							Pass("The Copyright Link is displayed.");
							String actVal = footerSectionCopyright.getText();
							log.add("The Expected value is : " + Val[7]);
							log.add("The Actual value is : " + actVal);
							if(Val[7].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
								jsclick(footerSectionCopyright);
								Thread.sleep(1000);
								navUrl = driver.getCurrentUrl();
								if(navUrl.contains(pgeurl[2].toString()))
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Copyrights Page was : " + pgeurl[2].toString());
									Pass("The user is navigated to the Copyrights  Page.",log);
								}
								else
								{
									log.add("The Current URL the user is navigated to is : " + driver.getCurrentUrl());
									log.add("The Expected URL for the Terms of Use page was : " + pgeurl[2].toString());
									Fail("The user is not navigated to the Terms of Use Page. Please Check.",log);
								}
								
								if(!currUrl.equals(navUrl))
								{
									driver.navigate().back();
									wait.until(ExpectedConditions.visibilityOf(header));
									wait.until(ExpectedConditions.visibilityOf(myAccountPageTitle));
								}
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Copyright Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionTermsandUse1))
						{
							Pass("The Terms and Use Link is displayed.");
							String actVal = footerSectionTermsandUse1.getText();
							log.add("The Expected value is : " + Val[9]);
							log.add("The Actual value is : " + actVal);
							if(actVal.contains(Val[9]))
							{
								Pass("The Expected Value matches.",log);
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Terms and Use Link is not displayed.");
						}
						
						if(BNBasicfeature.isElementPresent(footerSectionFullSite))
						{
							Pass("The Full Site Link is displayed.");
							String actVal = footerSectionFullSite.getText();
							log.add("The Expected value is : " + Val[10]);
							log.add("The Actual value is : " + actVal);
							if(Val[10].contains(actVal))
							{
								Pass("The Expected Value matches.",log);
							}
							else
							{
								Fail("The Expected Value does not matches.",log);
							}
						}
						else
						{
							Fail("The Full Site Link is not displayed.");
						}
					}
					else
					{
						Fail("The Footer Section Container is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5792  Issue ." + e.getMessage());
				Exception(" BRM - 5792 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
	
	/* BRM - 5447 Verify on clicking the Textbook Rentals section is it redirecting to the textbook rental page as per the classic site.*/
	public void myAccountLandinTextBookRentalNav()
	{
		ChildCreation(" BRM - 5447 Verify on clicking the Textbook Rentals section is it redirecting to the textbook rental page as per the classic site.");
		if(signedIn==true)
		{
			try
			{
				myAccntPage = navigatetoMyAccount();
				if(myAccntPage==true)
				{
					manageAccUrl = driver.getCurrentUrl();
					wait.until(ExpectedConditions.visibilityOf(myAccFormAccModules.get(0)));
					String expTitle = BNBasicfeature.getExcelVal("BRM5447", sheet, 2);
					if(BNBasicfeature.isElementPresent(myAccFormContainer))
					{
						Pass( "The My Account Form Container is displayed.");
						if(BNBasicfeature.isListElementPresent(myAccFormAccModules))
						{
							boolean titleFound = getLandingPageFormValue(expTitle);
							log.add( "The Expected title was : " + expTitle);
							log.add("The actual title is : " + actTitle);
							if(titleFound==false)
							{
								Fail( "The Expected title and the actual title does not matches.",log);
							}
							else
							{
								Pass( "The Expected title and the actual title matches.",log);
								jsclick(accElement);
								wait.until(ExpectedConditions.or(
										ExpectedConditions.visibilityOf(header),
										ExpectedConditions.visibilityOf(textBookRentalTabs)
									)
								);
								
								if(BNBasicfeature.isElementPresent(textBookRentalTabs))
								{
									Pass(" The user is redirected to the Retnal Text Book page.");
								}
								else
								{
									Fail( "The User is not navigated to the Rental Text Book page.");
								}
							}
						}
						else
						{
							Fail( "The My Account Form Container List is not displayed.");
						}
					}
					else
					{
						Fail( "The My Account Form Container is not displayed.");
					}
				}
				else
				{
					Fail( "The user failed to navigate to My Account Page.");
					signedIn = false;
				}
			}
			catch(Exception e)
			{
				System.out.println(" BRM - 5447  Issue ." + e.getMessage());
				Exception(" BRM - 5447 Issue." + e.getMessage());
			}
		}
		else
		{
			Skip( "The User failed to load the Add Gift Card Page .");
		}
	}
}

	
