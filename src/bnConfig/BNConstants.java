package bnConfig;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BNConstants 
{
	static String filename = new SimpleDateFormat("dd-MMM-yyyy").format(new Date())+".html";
	public static String currentUsersHomeDir = System.getProperty("user.dir");
	public static String mobileSiteURL = "http://m.barnesandnoble.com";
	public static String checkoutURL =  "https://m.barnesandnoble.com/checkout/checkout.jsp";
	public static String guestcheckoutURL =  "https://mpreprod.barnesandnoble.com/checkout/shipping.jsp";
	public static String chromeexePath = currentUsersHomeDir+"\\lib\\chromedriver.exe";
	//public static String commonExcelFile = "G:\\B&N\\BarnesAndNoble\\ExcelFiles\\commonExcelData.xls";
	//public static String commonExcelFile = "G:\\B&N\\BNProdCompleteStories\\ExcelFiles\\commonExcelData.xls";
	public static String commonExcelFile = currentUsersHomeDir+"\\ExcelFiles\\commonExcelData.xls";
	public static String repFileLoc = currentUsersHomeDir+"\\Reports\\BN Automation Test Report Prod "+filename;
	public static String extentRepConfigfile = currentUsersHomeDir+"\\ExtentReportConfig.xml";
	public static String topCampaignURL = "https://m.barnesandnoble.com/skavastream/core/v5/barnesandnoble/category/top?campaignId=1";
	public static String miniCartViewBagURL = "https://m.barnesandnoble.com/skavastream/xact/v5/barnesandnobleapi/viewbag?campaignId=772&productids=";
}
